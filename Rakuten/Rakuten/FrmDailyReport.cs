﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Rakuten.DAL;
using System.IO;
using Rakuten.Report;
using Rakuten.Utility;

namespace Rakuten
{
    public partial class FrmDailyReport : Form
    {
        private int index;

        private string folderName = "";

        public FrmDailyReport()
        {
            InitializeComponent();
        }

        private void FrmDailyReport_Load(object sender, EventArgs e)
        {
        }

        public string sendReportByEmail(int index,DateTime dtStart,DateTime dtEnd)
        {
            string fileName = "";
            try
            {
                switch (index)
                {
                    case 1:
                        //สรุปประเภทการชำระเงิน
                        Report09 rpt09 = new Report09();
                        rpt09.SetDataSource(DALReport.getReport9(dtStart, dtEnd));
                        rpt09.SetParameterValue("start", dtStart);
                        rpt09.SetParameterValue("end", dtEnd);
                        fileName = folderName + "สรุปประเภทการชำระเงิน.xls";
                        rpt09.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        break;
                    case 2:
                        //สรุปจำนวนลูกค้า
                        Report12 rpt12 = new Report12();
                        rpt12.SetDataSource(DALReport.getReport12(dtStart, dtEnd));
                        rpt12.SetParameterValue("start", dtStart);
                        rpt12.SetParameterValue("end", dtEnd);
                        fileName = folderName + "สรุปจำนวนลูกค้า.xls";
                        rpt12.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        break;
                    case 3:
                        //สรุปยอดขายสมาชิก
                        Report10 rpt10 = new Report10();
                        rpt10.SetDataSource(DALReport.getReport10(dtStart, dtEnd));
                        rpt10.SetParameterValue("start", dtStart);
                        rpt10.SetParameterValue("end", dtEnd);
                        fileName = folderName + "สรุปยอดขายสมาชิก.xls";
                        rpt10.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        break;
                    case 4:
                        //สรุปยอดขายรายเดือน
                        Report20 rpt20 = new Report20();
                        rpt20.SetDataSource(DALReport.getReport20(dtStart, dtEnd));
                        rpt20.SetParameterValue("start", dtStart);
                        rpt20.SetParameterValue("end", dtEnd);
                        fileName = folderName + "สรุปยอดขายรายเดือน.xls";
                        rpt20.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        break;
                    case 5:
                        //สรุปจำนวนสมาชิก
                        Report21 rpt21 = new Report21();
                        rpt21.SetDataSource(DALReport.getReport21(dtStart, dtEnd));
                        rpt21.SetParameterValue("start", dtStart);
                        rpt21.SetParameterValue("end", dtEnd);
                        fileName = folderName + "สรุปจำนวนสมาชิก.xls";
                        rpt21.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        break;
                    case 6:
                        // สรุปจำนวนสมาชิกใหม่
                        Report22 rpt22 = new Report22();
                        rpt22.SetDataSource(DALReport.getReport22(dtStart, dtEnd));
                        rpt22.SetParameterValue("start", dtStart);
                        rpt22.SetParameterValue("end", dtEnd);
                        fileName = folderName + "สรุปจำนวนสมาชิกใหม่.xls";
                        rpt22.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        break;
                    case 7:
                        //สรุปค่ามือพนักงาน
                        Report02 rpt02 = new Report02();
                        rpt02.SetDataSource(DALReport.getReport2(dtStart, dtEnd));
                        rpt02.SetParameterValue("start", dtStart);
                        rpt02.SetParameterValue("end", dtEnd);
                        fileName = folderName + "สรุปค่ามือพนักงาน.xls";
                        rpt02.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        break;
                    case 8:
                        //สรุปการจองพนักงานนวด
                        Report18 rpt19 = new Report18();
                        rpt19.SetDataSource(DALReport.getReport19(dtStart, dtEnd));
                        rpt19.SetParameterValue("start", dtStart);
                        rpt19.SetParameterValue("end", dtEnd);
                        fileName = folderName + "สรุปการจองพนักงานนวด.xls";
                        rpt19.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        break;
                    case 9:
                        //สรุปลูกค้า
                        Report01 rpt01 = new Report01();
                        rpt01.SetDataSource(DALReport.getReport1(dtStart, dtEnd));
                        rpt01.SetParameterValue("start", dtStart);
                        rpt01.SetParameterValue("end", dtEnd);
                        fileName = folderName + "สรุปลูกค้า.xls";
                        rpt01.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        break;
                    case 10:
                        //รายงานจำนวนเชื้อชาตินี้เลือก Package การนวดแบบไหน แยกตามเพศ
                        Report06 rpt06 = new Report06();
                        rpt06.SetDataSource(DALReport.getReport6(dtStart, dtEnd));
                        fileName = folderName + "รายงานจำนวนเชื้อชาตินี้เลือกใช้บริการ.xls";
                        rpt06.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        break;
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                Console.WriteLine(ex.Message);
            }
                return fileName;
        }

        private void step1()
        {
            //----------------Create folder
            folderName = @"C:\RakutenLog\" + MyFunction.getMonth(DateTime.Now.Month) + "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + "\\";
            DirectoryInfo di = new DirectoryInfo(@"" + folderName);
            if (!di.Exists) { di.Create(); }//ถ้าไม่พบ Folder ก็ทำการสร้างมันขึ้นมา
        }

        private void step2()
        {
            //----------------Send report to email.
            List<string> data = new List<string>();
            for (int i = 1; i <= 10; i++)
            {
                string file = sendReportByEmail(i, DP_START.Value, DP_END.Value);
                data.Add(file);
            }
            //-----------------Begin Send To Email
            label1.Text = SendSMS.sendMAIL(
                            ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "TO")),//to
                            ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "FROM")),//From
                            "Rakuten spa daily report (" + DP_START.Value.ToString() + ").",//subject
                            "Rakuten spa daily report (" + DP_START.Value.ToString() + ").",//body
                            ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "USER")),//user
                            ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "PASS")),//pass
                            ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "HOST")),//stmp server
                            data
                        );
            Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Value = index;
            if (index < 110)
            {
                if (index == 10)
                {
                    step2();
                    Application.DoEvents();
                }
                index++;
                Console.WriteLine("Index:" + index);
            }
            if (index == 110)
            {
                label1.Text = "Complete";
                progressBar1.Value = 110;
                progressBar1.Enabled = false;
            }
        }

        private void CMD_SEND_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            Application.DoEvents();
            step1();
            Application.DoEvents();
            step2();
            Cursor = Cursors.Default;
            MessageBox.Show("ส่งข้อมูลเรียบร้อยแล้ว");
            Close();
        }

    }
}
