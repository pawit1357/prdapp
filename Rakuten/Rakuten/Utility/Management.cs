﻿using System;
using System.Data;
using System.Data.SqlClient;
using Rakuten.DAL;
using Rakuten.Structure;
using System.Collections.Generic;
using System.IO;
//using Microsoft.SqlServer.Management.Smo;
//using Microsoft.SqlServer.Management.Common;
namespace Rakuten.Utility
{
    class Management
    {

        public const string Insert = "0";
        public const string Update = "1";
        public const string Delete = "2";
        public const string SearchAll = "3";
        public const string SearchByID = "4";
        public const string SearchByDetail = "5";
        public const string SearchByGroup = "6";

        public const string NUMBER_ONLY = "กรอกตัวเลขได้อย่างเดียว";
        public const string NO_FOUND_DATA = "ไม่พบข้อมูล";
        public const string IS_NULL = "ยังไม่ได้ป้อนข้อมูล";
        public const string CANNOT_INITIAL = "ไม่สามารถกำหนดค่าเริ่มต้นได้";
        public const string INSERT_SUCCESS = "บันทึกข้อมูลเรียบร้อยแล้ว";
        public const string UPDATE_SUCCESS = "ปรับปรุงข้อมูเรียบร้อยแล้ว";
        public const string DELETE_SUCCESS = "ลบข้อมูเรียบร้อยแล้ว";
        public const string NOT_ENOUGH_MONEY = "จำนวนเงินไม่พอ";
        public const string NOT_ENOUGH_PACKAGE = "จำนวน Package หมดแล้ว";
        public static string USER_NOTMATCH = "รหัสผ่านไม่ตรงกัน";
        public static string LOGIN_FAIL = "บัญชีผู้ใช้ไม่ถูกต้อง";

        public static void ShowMsg(string msg)
        {
            System.Windows.Forms.MessageBox.Show(msg);
        }
        public static void WriteMsg(string msg)
        {
            Console.WriteLine(msg);
        }
        //ดูคำอธิบาย
        public static string getDetail(MasterList master, string _id)
        {
            Console.WriteLine(master.ToString() + "," + _id);
            return DALMaster.getList(master,
                            new CMaster
                            {
                                ID = _id,
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchByID//Searh Flag
                            }
                            )[0].DETAIL;
        }

        //ดูค่า Package
        public static List<CPackage> getPackage(string _id)
        {
            return DALPackage.getList(new CPackage
                                        {
                                            ID = _id,
                                            DETAIL = "",
                                            Hour = "1",
                                            Price = 1,
                                            MoneyUnit = "THB",
                                            MassageType = "",
                                            PerCourse = 1,
                                            CustomerType = "",
                                            StartDate = DateTime.Now,
                                            EndDate = DateTime.Now,
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = "4"
                                        }
                                    );
        }

        //ดูค่าห้อง
        public static string getRoom(string _id)
        {
            return DALRoom.getList(new CRoom
                    {
                        ID = _id,
                        DETAIL = "",
                        MeassageType = "",
                        KEYDATE = DateTime.Now,
                        KEYUSER = Authorize.getUser(),
                        LASTEDIT = DateTime.Now,
                        FLAG = Management.SearchByID
                    }
            )[0].DETAIL;
        }

        public static bool isDBExist()
        {
            bool bExist = true;
            using (SqlConnection conn = new SqlConnection(@"Data Source=.\SqlExpress;Initial Catalog=;Integrated Security=SSPI"))
            {
                SqlCommand cmd = new SqlCommand("select dbid from sys.sysdatabases where name ='DBRakuten'", conn);
                cmd.CommandType = CommandType.Text;
                try
                {
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "DB");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        bExist = true;
                    }
                    else
                    {
                        bExist = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    bExist = false;
                }
                finally
                {
                    cmd.Dispose();
                }
            }
            return bExist;
        }

        public static bool createScript(string filename)
        {
            bool bSuccess = false;
            //try
            //{
            //    FileInfo file = new FileInfo(filename);
            //    string script = file.OpenText().ReadToEnd();
            //    string s = Connection.ConnectionString();
            //    using (SqlConnection conn = new SqlConnection(@"Data Source=.\SqlExpress;Initial Catalog=;Integrated Security=SSPI"))
            //    {
            //        Server server = new Server(new ServerConnection(conn));
            //        server.ConnectionContext.ExecuteNonQuery(script);
            //    }
            //    bSuccess = true;
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //    bSuccess = false;
            //}
            return true;
        }
    }
}
