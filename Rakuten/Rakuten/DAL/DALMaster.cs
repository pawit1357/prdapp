﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Rakuten.Structure;
using System.Data;
using System.Data.SqlClient;
namespace Rakuten.DAL
{
    public enum MasterList
    {
        MCustomerType = 0,
        MMassageType = 1,
        MNation = 2,
        MPaymentType = 3,
        //MServiceType = 4,
        MTitle = 5,
        MTypeEmployee = 6,
        MBank = 7,
        MPaymentStatus = 8 ,
        MSex = 9,
        MHour = 10,
        MAccountCode = 11,
        MLeave = 12
    }

    public class DALMaster
    {
        //Get Data
        public static List<CMaster> getList(MasterList mList,CMaster _master)
        {
            List<CMaster> lists = new List<CMaster>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manage" + mList.ToString(), con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _master.ID));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _master.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _master.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _master.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CMaster master = new CMaster
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["DNo"]),
                        DETAIL = Convert.ToString(dr["DDetail"]),
                        KEYDATE = Convert.ToDateTime(dr["MKeyDate"]),
                        LASTEDIT = Convert.ToDateTime(dr["MLastEdit"]),
                        KEYUSER = Convert.ToString(dr["MKeyUser"])
                    };
                    running++;
                    lists.Add(master);
                }
                dr.Close();
            }
            return lists;
        }

        //Insert,Delete,Update
        public static int manageMaster(MasterList mList, CMaster _master)
        {
            int i = 0;
            using(SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manage" + mList.ToString(), con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _master.ID));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _master.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _master.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _master.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
    }
}
