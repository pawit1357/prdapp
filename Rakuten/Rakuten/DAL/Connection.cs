﻿using System.Configuration;
namespace Rakuten.DAL
{
    public class Connection
    {
        public static string ConnectionString()
        {
            return @"Data Source=PAWIT-HUI;Integrated Security=SSPI;Initial Catalog=DBRakuten";
        }
        public static string TestConnection(string server,string dbname,string user,string pass)
        {
            return "server=" + server + ";user id=" + user + ";database=" + dbname + ";password=" + pass + ";persist security info=True";
        }
        public static string TestConnection(string server,string dbname)
        {
            return @"Data Source=" + server + ";Initial Catalog=" + dbname + ";Integrated Security=True";
        }
    }
}


