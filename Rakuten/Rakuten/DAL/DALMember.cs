﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Rakuten.Structure;
using System.Data;
using System.Data.SqlClient;
namespace Rakuten.DAL
{
    public class DALMember{
    
        //Get Data
        public static List<CMember> getList(CMember _cus)
        {
            List<CMember> cuss = new List<CMember>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageCustomer", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inMno", _cus.ID));
                cmd.Parameters.Add(new SqlParameter("@inMtype", _cus.Type));
                cmd.Parameters.Add(new SqlParameter("@inMsex", _cus.Sex));
                cmd.Parameters.Add(new SqlParameter("@inMname", _cus.Name));
                cmd.Parameters.Add(new SqlParameter("@inMsurname", _cus.Surname));
                cmd.Parameters.Add(new SqlParameter("@inMnational", _cus.Nation));
                cmd.Parameters.Add(new SqlParameter("@inAge", _cus.Age));
                cmd.Parameters.Add(new SqlParameter("@inMcontactAddress", _cus.Address));
                cmd.Parameters.Add(new SqlParameter("@inMmobile", _cus.Mobile));
                cmd.Parameters.Add(new SqlParameter("@inMemail", _cus.Email));
                cmd.Parameters.Add(new SqlParameter("@inMstartDate", _cus.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inMendDate", _cus.EndDate));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _cus.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _cus.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CMember cus = new CMember
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["Mno"]),
                        Type = Convert.ToString(dr["Mtype"]),
                        Sex = Convert.ToString(dr["Msex"]),
                        Name = Convert.ToString(dr["Mname"]),
                        Surname = Convert.ToString(dr["Msurname"]),
                        Nation = Convert.ToString(dr["Mnational"]),
                        Address = Convert.ToString(dr["McontactAddress"]),
                        Mobile = Convert.ToString(dr["Mmobile"]),
                        Email = Convert.ToString(dr["Memail"]),
                        StartDate = Convert.ToDateTime(dr["MstartDate"]),
                        EndDate = Convert.ToDateTime(dr["MendDate"]),
                        KEYDATE = Convert.ToDateTime(dr["MKeyDate"]),
                        LASTEDIT = Convert.ToDateTime(dr["MLastEdit"]),
                        KEYUSER = Convert.ToString(dr["MKeyUser"]),
                        Age = Convert.ToInt16(dr["MAge"])
                    };
                    running++;
                    cuss.Add(cus);
                }
                dr.Close();
            }
            return cuss;
        }

        //Insert,Delete,Update
        public static int manageMaster(CMember _cus)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageCustomer", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inMno", _cus.ID));
                cmd.Parameters.Add(new SqlParameter("@inMtype", _cus.Type));
                cmd.Parameters.Add(new SqlParameter("@inMsex", _cus.Sex));
                cmd.Parameters.Add(new SqlParameter("@inMname", _cus.Name));
                cmd.Parameters.Add(new SqlParameter("@inMsurname", _cus.Surname));
                cmd.Parameters.Add(new SqlParameter("@inMnational", _cus.Nation));
                cmd.Parameters.Add(new SqlParameter("@inAge", _cus.Age));
                cmd.Parameters.Add(new SqlParameter("@inMcontactAddress", _cus.Address));
                cmd.Parameters.Add(new SqlParameter("@inMmobile", _cus.Mobile));
                cmd.Parameters.Add(new SqlParameter("@inMemail", _cus.Email));
                cmd.Parameters.Add(new SqlParameter("@inMstartDate", _cus.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inMendDate", _cus.EndDate));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _cus.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _cus.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }

        //เก็บข้อมูลสมาชิกสมัคร และการจ่ายตัง
        public static int manageMemberPayment(CMemberPayment _mp)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageMemberPayment", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inMno", _mp.ID));
	            cmd.Parameters.Add(new SqlParameter("@inMtype", _mp.Type));
	            cmd.Parameters.Add(new SqlParameter("@inMsex", _mp.Sex));
	            cmd.Parameters.Add(new SqlParameter("@inMname", _mp.Name));
	            cmd.Parameters.Add(new SqlParameter("@inMsurname", _mp.Surname));
	            cmd.Parameters.Add(new SqlParameter("@inMnational", _mp.Nation));
	            cmd.Parameters.Add(new SqlParameter("@inAge", _mp.Age));
	            cmd.Parameters.Add(new SqlParameter("@inMcontactAddress", _mp.Address));
	            cmd.Parameters.Add(new SqlParameter("@inMmobile", _mp.Mobile));
	            cmd.Parameters.Add(new SqlParameter("@inMemail", _mp.Email));
	            cmd.Parameters.Add(new SqlParameter("@inMstartDate", _mp.StartDate));
	            cmd.Parameters.Add(new SqlParameter("@inMendDate", _mp.EndDate));
	            ////
	            cmd.Parameters.Add(new SqlParameter("@inTTherapist", _mp.Therapist));
	            cmd.Parameters.Add(new SqlParameter("@inTPackage", _mp.Package));
	            ////
	            cmd.Parameters.Add(new SqlParameter("@inSACCode", _mp.AccountCode));
	            cmd.Parameters.Add(new SqlParameter("@inSAmount", _mp.Amount));
                cmd.Parameters.Add(new SqlParameter("@inSDiscount", _mp.Discount));
	            cmd.Parameters.Add(new SqlParameter("@inSPaytype", _mp.PaymentType));
	            cmd.Parameters.Add(new SqlParameter("@inSSlip", _mp.Slip));
	            cmd.Parameters.Add(new SqlParameter("@inSBank", _mp.Bank));
                cmd.Parameters.Add(new SqlParameter("@inMemuse", _mp.RUNNING));
	            ////manageMemberPayment
                cmd.Parameters.Add(new SqlParameter("@inSKeyUser", _mp.KEYUSER));
	            cmd.Parameters.Add(new SqlParameter("@inFlag", _mp.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }

        //ดูข้อมูล Package ที่ซื้อไป
        public static List<CMemberPayment> getMemberPackage(CMemberPayment _mp)
        {
            List<CMemberPayment> list = new List<CMemberPayment>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageMemberPayment", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inMno", _mp.ID));
                cmd.Parameters.Add(new SqlParameter("@inMtype", _mp.Type));
                cmd.Parameters.Add(new SqlParameter("@inMsex", _mp.Sex));
                cmd.Parameters.Add(new SqlParameter("@inMname", _mp.Name));
                cmd.Parameters.Add(new SqlParameter("@inMsurname", _mp.Surname));
                cmd.Parameters.Add(new SqlParameter("@inMnational", _mp.Nation));
                cmd.Parameters.Add(new SqlParameter("@inAge", _mp.Age));
                cmd.Parameters.Add(new SqlParameter("@inMcontactAddress", _mp.Address));
                cmd.Parameters.Add(new SqlParameter("@inMmobile", _mp.Mobile));
                cmd.Parameters.Add(new SqlParameter("@inMemail", _mp.Email));
                cmd.Parameters.Add(new SqlParameter("@inMstartDate", _mp.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inMendDate", _mp.EndDate));
                ////
                cmd.Parameters.Add(new SqlParameter("@inTTherapist", _mp.Therapist));
                cmd.Parameters.Add(new SqlParameter("@inTPackage", _mp.Package));
                ////
                cmd.Parameters.Add(new SqlParameter("@inSACCode", _mp.AccountCode));
                cmd.Parameters.Add(new SqlParameter("@inSAmount", _mp.Amount));
                cmd.Parameters.Add(new SqlParameter("@inSDiscount", _mp.Discount));
                cmd.Parameters.Add(new SqlParameter("@inSPaytype", _mp.PaymentType));
                cmd.Parameters.Add(new SqlParameter("@inSSlip", _mp.Slip));
                cmd.Parameters.Add(new SqlParameter("@inSBank", _mp.Bank));
                cmd.Parameters.Add(new SqlParameter("@inMemuse", _mp.RUNNING));
                ////
                cmd.Parameters.Add(new SqlParameter("@inSKeyUser", _mp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _mp.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CMemberPayment cmp = new CMemberPayment
                    {
                        RUNNING = running,
                        Therapist = Convert.ToString(dr["TTherapist"]),
                        Package = Convert.ToString(dr["MPackage"]),
                        PaymentType = Convert.ToString(dr["SPaytype"]),
                        Bank = Convert.ToString(dr["SBank"]),
                        Slip = Convert.ToString(dr["SSlip"]),
                        AccountCode = Convert.ToString(dr["SACCode"]),
                        Amount = Convert.ToDouble(dr["SAmount"]),
                        Discount = Convert.ToDouble(dr["SDiscount"]),
                    };
                    running++;
                    list.Add(cmp);
                }
                dr.Close();
            }
            return list;
        }
        //พิมพ์ใบเสร็จย้อนหลัง
        public static List<CRpt02> printInvoice(string cusID)
        {
            List<CRpt02> CRpt02 = new List<CRpt02>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("PrintMemberInvoice", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inMno", cusID));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CRpt02 cus = new CRpt02
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["MID"]),
                        Name = Convert.ToString(dr["Mname"]),
                        Surname = Convert.ToString(dr["Msurname"]),
                        Address = Convert.ToString(dr["McontactAddress"]),
                        DETAIL = Convert.ToString(dr["Pdetail"]),
                        Amount = Convert.ToDouble(dr["Pprice"]),
                    };
                    running++;
                    CRpt02.Add(cus);
                }
                dr.Close();
            }
            return CRpt02;
        }
    }
}
