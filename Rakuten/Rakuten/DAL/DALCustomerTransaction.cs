﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Rakuten.Structure;
using System.Data;
using System.Data.SqlClient;
using Rakuten.Utility;
namespace Rakuten.DAL
{
    public class DALCustomerTransaction
    {
        //Get Data
        #region
        public static List<CCustomerTransaction> getList(CCustomerTransaction _res)
        {
            List<CCustomerTransaction> rervs = new List<CCustomerTransaction>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageCustomerTransaction", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inRID", _res.ID));
                cmd.Parameters.Add(new SqlParameter("@inRmemberID", _res.CustomerID));
                cmd.Parameters.Add(new SqlParameter("@inRTherapistID", _res.TherapistID));
                cmd.Parameters.Add(new SqlParameter("@inRTherapistRevID", _res.TherapistRevID));
                cmd.Parameters.Add(new SqlParameter("@inRPackage", _res.Package));
                cmd.Parameters.Add(new SqlParameter("@inRDate", _res.Date));
                cmd.Parameters.Add(new SqlParameter("@inRTime", _res.Time));
                cmd.Parameters.Add(new SqlParameter("@inRRoom", _res.Room));
                cmd.Parameters.Add(new SqlParameter("@inRCustomerType", _res.Customertype));
                //Customer Detail
                cmd.Parameters.Add(new SqlParameter("@inName", _res.Name));
                cmd.Parameters.Add(new SqlParameter("@inSurname", _res.Surname));
                cmd.Parameters.Add(new SqlParameter("@inSex", _res.Sex));
                cmd.Parameters.Add(new SqlParameter("@inAge", _res.Age));
                cmd.Parameters.Add(new SqlParameter("@inNation", _res.Nation));
                cmd.Parameters.Add(new SqlParameter("@inMobile", _res.Mobile));
                cmd.Parameters.Add(new SqlParameter("@inAddress", _res.Address));
                //End Customer Detail
                cmd.Parameters.Add(new SqlParameter("@inScent", _res.ScentType));
                cmd.Parameters.Add(new SqlParameter("@inScentCount", _res.ScentCount));
                cmd.Parameters.Add(new SqlParameter("@inkeyUser", _res.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inStatus", _res.Status));//1 คือ จอง , 2 คือ ใช้งบริการปกติ
                cmd.Parameters.Add(new SqlParameter("@inRSpecialPackage", _res.SpecialPackage));
                cmd.Parameters.Add(new SqlParameter("@inRScent1", _res.ScentType1));
                cmd.Parameters.Add(new SqlParameter("@inRScentCount1", _res.ScentCount1));
                cmd.Parameters.Add(new SqlParameter("@inRemark", _res.Remark));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _res.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CCustomerTransaction ct = new CCustomerTransaction
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["RID"]),
                        CustomerID = Convert.ToString(dr["RMemberID"]),
                        TherapistID = Convert.ToString(dr["RTherapistID"]),
                        TherapistRevID = Convert.ToString(dr["RTherapistRevID"]),
                        Package = Convert.ToString(dr["RPackage"]),
                        Date = Convert.ToDateTime(dr["RDate"]),
                        Time = Convert.ToDateTime(dr["RTime"]),
                        DETAIL= Management.getPackage(Convert.ToString(dr["RPackage"]))[0].Hour,
                        EndDate = Convert.ToDateTime(dr["RTime"]).AddHours(Convert.ToDouble(Management.getDetail(MasterList.MHour, Management.getPackage(Convert.ToString(dr["RPackage"]))[0].Hour))),
                        Room = Convert.ToString(dr["RRoom"]),
                        Customertype = Convert.ToString(dr["RCustomerType"]),
                        Name = Convert.ToString(dr["Mname"]),
                        Surname = Convert.ToString(dr["Msurname"]),
                        Mobile = Convert.ToString(dr["Mmobile"]),
                        Type = Convert.ToString(dr["Rtype"]),
                        Nation = Convert.ToString(dr["Mnational"]),
                        Address = Convert.ToString(dr["MContactAddress"]),
                        Sex = Convert.ToString(dr["MSex"]),
                        Age = Convert.ToInt16(dr["MAge"]),
                        Remark = Convert.ToString(dr["isPlay"]),
                        SpecialPackage = Convert.ToString(dr["RSpecialPackage"]),
                        FLAG = Convert.ToString(dr["RemarkPackageID"]),
                        NickName = Convert.ToString(dr["EmpName"])
                    };
                    running++;
                    rervs.Add(ct);
                }
                dr.Close();
            }
            return rervs;
        }

        //Insert,Delete,Update
        public static int manageMaster(CCustomerTransaction _res)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageCustomerTransaction", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inRID", _res.ID));
                cmd.Parameters.Add(new SqlParameter("@inRmemberID", _res.CustomerID));
                cmd.Parameters.Add(new SqlParameter("@inRTherapistID", _res.TherapistID));
                cmd.Parameters.Add(new SqlParameter("@inRTherapistRevID", _res.TherapistRevID));
                cmd.Parameters.Add(new SqlParameter("@inRPackage", _res.Package));
                cmd.Parameters.Add(new SqlParameter("@inRDate", _res.Date));
                cmd.Parameters.Add(new SqlParameter("@inRTime", _res.Time));
                cmd.Parameters.Add(new SqlParameter("@inRRoom", _res.Room));
                cmd.Parameters.Add(new SqlParameter("@inRCustomerType", _res.Customertype));
                //Customer Detail
                cmd.Parameters.Add(new SqlParameter("@inName", _res.Name));
                cmd.Parameters.Add(new SqlParameter("@inSurname", _res.Surname));
                cmd.Parameters.Add(new SqlParameter("@inSex", _res.Sex));
                cmd.Parameters.Add(new SqlParameter("@inAge", _res.Age));
                cmd.Parameters.Add(new SqlParameter("@inNation", _res.Nation));
                cmd.Parameters.Add(new SqlParameter("@inMobile", _res.Mobile));
                cmd.Parameters.Add(new SqlParameter("@inAddress", _res.Address));

                //End Customer Detail
                cmd.Parameters.Add(new SqlParameter("@inScent", _res.ScentType));
                cmd.Parameters.Add(new SqlParameter("@inScentCount", _res.ScentCount));
                cmd.Parameters.Add(new SqlParameter("@inRSpecialPackage", _res.SpecialPackage));
                cmd.Parameters.Add(new SqlParameter("@inkeyUser", _res.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inStatus", _res.Status));//1 คือ จอง , 2 คือ ใช้งบริการปกติ
                cmd.Parameters.Add(new SqlParameter("@inRemark", _res.Remark));
                cmd.Parameters.Add(new SqlParameter("@inRScent1", _res.ScentType1));
                cmd.Parameters.Add(new SqlParameter("@inRScentCount1", _res.ScentCount1));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _res.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }

        #endregion
        //Get Last Customer Transaction
        public static string getLastCostomerID(string type)
        {
            int id = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetCustomerLastID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inCusType", type));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    id = Convert.ToInt16(dr["icount"]);
                }
            }
            return String.Format("{0:0000}", Convert.ToInt16(id));
        }

        //Get Transaction ID
        public static int getTransactionID()
        {
            int id = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetLastTranSaction", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    id = Convert.ToInt16(dr["RID"]);
                }
            }
            return id;

        }
    }
}
