﻿using System;
using System.Collections.Generic;
using System.Text;
using Rakuten.Structure;
using System.Data.SqlClient;
using System.Data;

namespace Rakuten.DAL
{
    public class DALPermission
    {
        //Get Data
        public static List<CPermission> getList(CPermission _c)
        {
            List<CPermission> cs = new List<CPermission>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetPermission", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inPUID", _c.EMPID));
                cmd.Parameters.Add(new SqlParameter("@inPFRM", _c.From));
                cmd.Parameters.Add(new SqlParameter("@inPINSERT", _c.Insert));
                cmd.Parameters.Add(new SqlParameter("@inPDELETE", _c.Delete));
                cmd.Parameters.Add(new SqlParameter("@inPUPDATE", _c.Update));
                cmd.Parameters.Add(new SqlParameter("@inPVIEW", _c.View));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _c.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    CPermission c = new CPermission
                    {
                        ID = Convert.ToString(dr["FID"]),
                        EMPID = Convert.ToString(dr["PUID"]),
                        From = Convert.ToString(dr["FNAME"]),
                        DETAIL = Convert.ToString(dr["FDESC"]),
                        Insert = Convert.ToByte(Convert.ToBoolean(Convert.ToString(dr["PINSERT"]))),
                        Delete = Convert.ToByte(Convert.ToBoolean(Convert.ToString(dr["PDELETE"]))),
                        Update = Convert.ToByte(Convert.ToBoolean(Convert.ToString(dr["PUPDATE"]))),
                        View = Convert.ToByte(Convert.ToBoolean(Convert.ToString(dr["PVIEW"]))),
                    };
                    cs.Add(c);
                }
                dr.Close();
            }
            return cs;
        }
        //Manage Data
        public static int manage(CPermission _c)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetPermission", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inPUID", _c.EMPID));
                cmd.Parameters.Add(new SqlParameter("@inPFRM", _c.From));
                cmd.Parameters.Add(new SqlParameter("@inPINSERT", _c.Insert));
                cmd.Parameters.Add(new SqlParameter("@inPDELETE", _c.Delete));
                cmd.Parameters.Add(new SqlParameter("@inPUPDATE", _c.Update));
                cmd.Parameters.Add(new SqlParameter("@inPVIEW", _c.View));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _c.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
    }
}
