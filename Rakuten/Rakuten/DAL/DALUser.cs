﻿using System;
using System.Collections.Generic;
using Rakuten.Structure;
using System.Data;
using System.Data.SqlClient;
namespace Rakuten.DAL
{
    class DALUser
    {
        //Get Data
        public static List<CUSER> GetLogin(CUSER _user)
        {
            List<CUSER> lists = new List<CUSER>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageMUser", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inUREFID", _user.USER));
                cmd.Parameters.Add(new SqlParameter("@inUPASS", _user.PASSWORD));
                cmd.Parameters.Add(new SqlParameter("@inUTYPE", _user.Type));
                cmd.Parameters.Add(new SqlParameter("@inUAUTHORIZE", _user.PERMISSION));
                cmd.Parameters.Add(new SqlParameter("@inKEYUSER", _user.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _user.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CUSER master = new CUSER
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["Eno"]),
                        Name = Convert.ToString(dr["Ename"]) + "  " + Convert.ToString(dr["Esurname"]),
                        Type = Convert.ToString(dr["DDetail"]),
                        PERMISSION = Convert.ToString(dr["UAUTHORIZE"])
                    };
                    running++;
                    lists.Add(master);
                }
                dr.Close();
            }
            return lists;
        }
        //Insert,Delete,Update
        public static int manage(CUSER _user)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageMUser", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inUREFID", _user.USER));
                cmd.Parameters.Add(new SqlParameter("@inUPASS", _user.PASSWORD));
                cmd.Parameters.Add(new SqlParameter("@inUTYPE", _user.Type));
                cmd.Parameters.Add(new SqlParameter("@inUAUTHORIZE", _user.PERMISSION));
                cmd.Parameters.Add(new SqlParameter("@inKEYUSER", _user.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _user.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
    }
}
