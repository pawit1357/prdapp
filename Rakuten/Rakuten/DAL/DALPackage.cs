﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using Rakuten.DAL;
using Rakuten.Structure;
using Rakuten.Utility;

namespace Rakuten.DAL
{
    public class DALPackage
    {
        //Get Data
        public static List<CPackage> getList(CPackage _package)
        {
            List<CPackage> packages = new List<CPackage>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("managePackage", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _package.ID));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _package.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inPhour", _package.Hour));
                cmd.Parameters.Add(new SqlParameter("@inPprice", _package.Price));
                cmd.Parameters.Add(new SqlParameter("@inPpriceforTherapist", _package.TherapistPrice));
                cmd.Parameters.Add(new SqlParameter("@inPmoneyUnit", _package.MoneyUnit));
                cmd.Parameters.Add(new SqlParameter("@inPMassageType", _package.MassageType));
                cmd.Parameters.Add(new SqlParameter("@inPpercourse", _package.PerCourse));
                cmd.Parameters.Add(new SqlParameter("@inPCustomerType", _package.CustomerType));
                cmd.Parameters.Add(new SqlParameter("@inPstart", _package.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inPend", _package.EndDate));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _package.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _package.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CPackage package = new CPackage
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["Pno"]),
                        DETAIL = Convert.ToString(dr["Pdetail"]),
                        Hour = Convert.ToString(dr["Phour"]),
                        Price = Convert.ToDouble(dr["Pprice"]),
                        MoneyUnit = Convert.ToString(dr["PmoneyUnit"]),
                        MassageType = Convert.ToString(dr["PMassageType"]),
                        PerCourse = Convert.ToInt16(dr["Ppercourse"]),
                        CustomerType = Convert.ToString(dr["PCustomerType"]),
                        TherapistPrice = Convert.ToDouble(dr["PpriceforTherapist"]),
                        StartDate = Convert.ToDateTime(dr["Pstart"]),
                        EndDate = Convert.ToDateTime(dr["Pend"]),
                        KEYDATE = Convert.ToDateTime(dr["MKeyDate"]),
                        LASTEDIT = Convert.ToDateTime(dr["MLastEdit"]),
                        KEYUSER = Convert.ToString(dr["MKeyUser"])
                    };
                    running++;
                    packages.Add(package);
                }
                dr.Close();
            }
            return packages;
        }
        //Get Data
        public static List<CPackage> getList1(CPackage _package)
        {
            List<CPackage> packages = new List<CPackage>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("managePackage", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _package.ID));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _package.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inPhour", _package.Hour));
                cmd.Parameters.Add(new SqlParameter("@inPprice", _package.Price));
                cmd.Parameters.Add(new SqlParameter("@inPpriceforTherapist", _package.TherapistPrice));
                cmd.Parameters.Add(new SqlParameter("@inPmoneyUnit", _package.MoneyUnit));
                cmd.Parameters.Add(new SqlParameter("@inPMassageType", _package.MassageType));
                cmd.Parameters.Add(new SqlParameter("@inPpercourse", _package.PerCourse));
                cmd.Parameters.Add(new SqlParameter("@inPCustomerType", _package.CustomerType));
                cmd.Parameters.Add(new SqlParameter("@inPstart", _package.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inPend", _package.EndDate));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _package.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _package.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CPackage package = new CPackage
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["MID"]),
                        MassageType = Convert.ToString(dr["PMassageType"]),
                        DETAIL = Convert.ToString(dr["Pdetail"]),
                        Hour = Convert.ToString(dr["Phour"]),
                        Price = Convert.ToDouble(dr["Pprice"]),
                        MemberUse = Convert.ToInt16(dr["MUseMember"]),
                        PerCourse = Convert.ToInt16(dr["Ppercourse"]),
                    };
                    running++;
                    packages.Add(package);
                }
                dr.Close();
            }
            return packages;
        }

        //Insert,Delete,Update
        public static int manageMaster(CPackage _package)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("managePackage", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _package.ID));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _package.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inPhour", _package.Hour));
                cmd.Parameters.Add(new SqlParameter("@inPprice", _package.Price));
                cmd.Parameters.Add(new SqlParameter("@inPpriceforTherapist", _package.TherapistPrice));
                cmd.Parameters.Add(new SqlParameter("@inPmoneyUnit", _package.MoneyUnit));
                cmd.Parameters.Add(new SqlParameter("@inPMassageType", _package.MassageType));
                cmd.Parameters.Add(new SqlParameter("@inPpercourse", _package.PerCourse));
                cmd.Parameters.Add(new SqlParameter("@inPCustomerType", _package.CustomerType));
                cmd.Parameters.Add(new SqlParameter("@inPstart", _package.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inPend", _package.EndDate));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _package.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _package.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
                cmd.Clone();
            }
            return i;
        }

        public static string getMemberPackage(string id)
        {
            string package = "";
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetMemberPackage", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inID", id));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    package = Convert.ToString(dr["Package"]);
                }
                dr.Close();
            }
            return package;
        }
        
    }
}
