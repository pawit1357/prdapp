﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using Rakuten.DAL;
using Rakuten.Structure;
using Rakuten.Utility;

namespace Rakuten.DAL
{
    class DALSchedule
    {
        public static List<CSchedule> getScheduleList(DateTime dt)
        {
            List<CSchedule> lists = new List<CSchedule>();
            //Set Header
            CSchedule schedule = new CSchedule
            {
                TimeIn = "",
                TimeOut = "",
                Number = "",
                T0030 = "",
                T0100 = "",
                T0130 = "",
                T0200 = "",
                T0230 = "",
                T0300 = "",
                T0330 = "",
                T0400 = "",
                T0430 = "",
                T0500 = "",
                T0530 = "",
                T0600 = "",
                T0630 = "",
                T0700 = "",
                T0730 = "",
                T0800 = "",
                T0830 = "",
                T0900 = "",
                T0930 = "",
                T1000 = "",
                T1030 = "",
                T1100 = "",
                T1130 = "",
                T1200 = "",
                T1230 = "",
                T1300 = "",
                T1330 = "",
                T1400 = "",
                T1430 = "",
                T1500 = "",
                T1530 = "",
                T1600 = "",
                T1630 = "",
                T1700 = "",
                T1730 = "",
                T1800 = "",
                T1830 = "",
                T1900 = "",
                T1930 = "",
                T2000 = "",
                T2030 = "",
                T2100 = "",
                T2130 = "",
                T2200 = "",
                T2230 = "",
                T2300 = "",
                T2330 = "",
                T0000 = "",
                RUNNING = 0,
                Remark = ""
            };
            lists.Add(schedule);//Add Header for show time cursor.
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSchedule", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inESno", ""));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate",dt));
                cmd.Parameters.Add(new SqlParameter("@inQ", ""));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", Authorize.getUser()));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "3"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    lists.Add(getScheduleByID(
                             Convert.ToString(dr["ESno"]),//Employee ID
                            (DBNull.Value != dr["EStartTime"]) ? Convert.ToDateTime(dr["EStartTime"]).ToShortTimeString() : "",//Time IN
                            (DBNull.Value != dr["ESEndTime"]) ? Convert.ToDateTime(dr["ESEndTime"]).ToShortTimeString() : "",dt)//Time OUT
                            );
                    running++;
                }
                dr.Close();
            }
            return lists;
        }

        public static List<CSchedule> getRoomScheduleList(DateTime dt)
        {
            List<CSchedule> lists = new List<CSchedule>();
            //Set Header
            CSchedule schedule = new CSchedule
            {
                TimeIn = "",
                TimeOut = "",
                Number = "",
                T0030 = "",
                T0100 = "",
                T0130 = "",
                T0200 = "",
                T0230 = "",
                T0300 = "",
                T0330 = "",
                T0400 = "",
                T0430 = "",
                T0500 = "",
                T0530 = "",
                T0600 = "",
                T0630 = "",
                T0700 = "",
                T0730 = "",
                T0800 = "",
                T0830 = "",
                T0900 = "",
                T0930 = "",
                T1000 = "",
                T1030 = "",
                T1100 = "",
                T1130 = "",
                T1200 = "",
                T1230 = "",
                T1300 = "",
                T1330 = "",
                T1400 = "",
                T1430 = "",
                T1500 = "",
                T1530 = "",
                T1600 = "",
                T1630 = "",
                T1700 = "",
                T1730 = "",
                T1800 = "",
                T1830 = "",
                T1900 = "",
                T1930 = "",
                T2000 = "",
                T2030 = "",
                T2100 = "",
                T2130 = "",
                T2200 = "",
                T2230 = "",
                T2300 = "",
                T2330 = "",
                T0000 = "",
            };
            lists.Add(schedule);//Add Header for show time cursor.
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetRoomSchedule", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@InRoom", ""));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", dt));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "0"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    lists.Add(getRoomScheduleByID(Convert.ToString(dr["Rno"]),dt));
                    running++;
                }
                dr.Close();
            }
            return lists;
        }

        public static CSchedule getRoomScheduleByID(string id, DateTime dt)
        {
            List<int> indexs = new List<int>();
            List<string> services = new List<string>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetRoomSchedule", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@InRoom", id));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", dt));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "1"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    indexs.Add(getTimeIndex(getTimePeriod(Convert.ToDateTime(dr["RTimeBegin"]).ToShortTimeString())));
                    indexs.Add(getTimeIndex(getTimePeriod(Convert.ToDateTime(Convert.ToDateTime(dr["RTimeBegin"]).AddHours(Convert.ToDouble(dr["RTimeEnd"]))).ToShortTimeString())));
                    services.Add(Convert.ToString(dr["Rtype"]));
                    services.Add(Convert.ToString(dr["Rtype"]));
                    running++;
                }
                dr.Close();
            }
            List<int> newIndex = genSesqunce(indexs);
            string[] newService = genSesqunce(indexs, services);
            CSchedule schedule = new CSchedule
            {
                TimeIn = "",
                TimeOut = "",
                Number = id,
                T0030 = (genSchedule(0, newIndex)) ? newService[0] : "",
                T0100 = (genSchedule(1, newIndex)) ? newService[1] : "",
                T0130 = (genSchedule(2, newIndex)) ? newService[2] : "",
                T0200 = (genSchedule(3, newIndex)) ? newService[3] : "",
                T0230 = (genSchedule(4, newIndex)) ? newService[4] : "",
                T0300 = (genSchedule(5, newIndex)) ? newService[5] : "",
                T0330 = (genSchedule(6, newIndex)) ? newService[6] : "",
                T0400 = (genSchedule(7, newIndex)) ? newService[7] : "",
                T0430 = (genSchedule(8, newIndex)) ? newService[8] : "",
                T0500 = (genSchedule(9, newIndex)) ? newService[9] : "",
                T0530 = (genSchedule(10, newIndex)) ? newService[10] : "",
                T0600 = (genSchedule(11, newIndex)) ? newService[11] : "",
                T0630 = (genSchedule(12, newIndex)) ? newService[12] : "",
                T0700 = (genSchedule(13, newIndex)) ? newService[13] : "",
                T0730 = (genSchedule(14, newIndex)) ? newService[14] : "",
                T0800 = (genSchedule(15, newIndex)) ? newService[15] : "",
                T0830 = (genSchedule(16, newIndex)) ? newService[16] : "",
                T0900 = (genSchedule(17, newIndex)) ? newService[17] : "",
                T0930 = (genSchedule(18, newIndex)) ? newService[18] : "",
                T1000 = (genSchedule(19, newIndex)) ? newService[19] : "",
                T1030 = (genSchedule(20, newIndex)) ? newService[20] : "",
                T1100 = (genSchedule(21, newIndex)) ? newService[21] : "",
                T1130 = (genSchedule(22, newIndex)) ? newService[22] : "",
                T1200 = (genSchedule(23, newIndex)) ? newService[23] : "",
                T1230 = (genSchedule(24, newIndex)) ? newService[24] : "",
                T1300 = (genSchedule(25, newIndex)) ? newService[25] : "",
                T1330 = (genSchedule(26, newIndex)) ? newService[26] : "",
                T1400 = (genSchedule(27, newIndex)) ? newService[27] : "",
                T1430 = (genSchedule(28, newIndex)) ? newService[28] : "",
                T1500 = (genSchedule(29, newIndex)) ? newService[29] : "",
                T1530 = (genSchedule(30, newIndex)) ? newService[30] : "",
                T1600 = (genSchedule(31, newIndex)) ? newService[31] : "",
                T1630 = (genSchedule(32, newIndex)) ? newService[32] : "",
                T1700 = (genSchedule(33, newIndex)) ? newService[33] : "",
                T1730 = (genSchedule(34, newIndex)) ? newService[34] : "",
                T1800 = (genSchedule(35, newIndex)) ? newService[35] : "",
                T1830 = (genSchedule(36, newIndex)) ? newService[36] : "",
                T1900 = (genSchedule(37, newIndex)) ? newService[37] : "",
                T1930 = (genSchedule(38, newIndex)) ? newService[38] : "",
                T2000 = (genSchedule(39, newIndex)) ? newService[39] : "",
                T2030 = (genSchedule(40, newIndex)) ? newService[40] : "",
                T2100 = (genSchedule(41, newIndex)) ? newService[41] : "",
                T2130 = (genSchedule(42, newIndex)) ? newService[42] : "",
                T2200 = (genSchedule(43, newIndex)) ? newService[43] : "",
                T2230 = (genSchedule(44, newIndex)) ? newService[44] : "",
                T2300 = (genSchedule(45, newIndex)) ? newService[45] : "",
                T2330 = (genSchedule(46, newIndex)) ? newService[46] : "",
                T0000 = (genSchedule(47, newIndex)) ? newService[47] : "",
            };
            return schedule;
        }

        public static int checkCount(CSchedule schedule)
        {
            int count = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSchedule", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inESno", schedule.ID));
                cmd.Parameters.Add(new SqlParameter("@inQ", schedule.Remark));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", schedule.KEYDATE));
                cmd.Parameters.Add(new SqlParameter("@inkeyUser", schedule.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", schedule.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    count = Convert.ToInt16(dr["icount"]);
                }
            }
            return count;
        }

        public static CSchedule getScheduleByID(string id, string timeIn, string timeOut,DateTime dt)
        {
            List<int> indexs = new List<int>();
            List<string> services = new List<string>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSchedule", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inESno",id));
                cmd.Parameters.Add(new SqlParameter("@inQ", ""));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", dt));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", Authorize.getUser()));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "5"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    indexs.Add(getTimeIndex(getTimePeriod(Convert.ToDateTime(dr["RTimeBegin"]).ToShortTimeString())));
                    indexs.Add(getTimeIndex(getTimePeriod(Convert.ToDateTime(Convert.ToDateTime(dr["RTimeBegin"]).AddHours(Convert.ToDouble(dr["RTimeEnd"]))).ToShortTimeString())));
                    services.Add(Convert.ToString(dr["Rtype"]));
                    services.Add(Convert.ToString(dr["Rtype"]));
                    running++;
                }
                dr.Close();
            }
            List<int> newIndex = genSesqunce(indexs);
            string[] newService = genSesqunce(indexs, services);
            CSchedule schedule = new CSchedule
            {
                TimeIn = timeIn,
                TimeOut = timeOut,
                Number = id,
                T0030 = (genSchedule(0, newIndex)) ? newService[0] : "",
                T0100 = (genSchedule(1, newIndex)) ? newService[1] : "",
                T0130 = (genSchedule(2, newIndex)) ? newService[2] : "",
                T0200 = (genSchedule(3, newIndex)) ? newService[3] : "",
                T0230 = (genSchedule(4, newIndex)) ? newService[4] : "",
                T0300 = (genSchedule(5, newIndex)) ? newService[5] : "",
                T0330 = (genSchedule(6, newIndex)) ? newService[6] : "",
                T0400 = (genSchedule(7, newIndex)) ? newService[7] : "",
                T0430 = (genSchedule(8, newIndex)) ? newService[8] : "",
                T0500 = (genSchedule(9, newIndex)) ? newService[9] : "",
                T0530 = (genSchedule(10, newIndex)) ? newService[10] : "",
                T0600 = (genSchedule(11, newIndex)) ? newService[11] : "",
                T0630 = (genSchedule(12, newIndex)) ? newService[12] : "",
                T0700 = (genSchedule(13, newIndex)) ? newService[13] : "",
                T0730 = (genSchedule(14, newIndex)) ? newService[14] : "",
                T0800 = (genSchedule(15, newIndex)) ? newService[15] : "",
                T0830 = (genSchedule(16, newIndex)) ? newService[16] : "",
                T0900 = (genSchedule(17, newIndex)) ? newService[17] : "",
                T0930 = (genSchedule(18, newIndex)) ? newService[18] : "",
                T1000 = (genSchedule(19, newIndex)) ? newService[19] : "",
                T1030 = (genSchedule(20, newIndex)) ? newService[20] : "",
                T1100 = (genSchedule(21, newIndex)) ? newService[21] : "",
                T1130 = (genSchedule(22, newIndex)) ? newService[22] : "",
                T1200 = (genSchedule(23, newIndex)) ? newService[23] : "",
                T1230 = (genSchedule(24, newIndex)) ? newService[24] : "",
                T1300 = (genSchedule(25, newIndex)) ? newService[25] : "",
                T1330 = (genSchedule(26, newIndex)) ? newService[26] : "",
                T1400 = (genSchedule(27, newIndex)) ? newService[27] : "",
                T1430 = (genSchedule(28, newIndex)) ? newService[28] : "",
                T1500 = (genSchedule(29, newIndex)) ? newService[29] : "",
                T1530 = (genSchedule(30, newIndex)) ? newService[30] : "",
                T1600 = (genSchedule(31, newIndex)) ? newService[31] : "",
                T1630 = (genSchedule(32, newIndex)) ? newService[32] : "",
                T1700 = (genSchedule(33, newIndex)) ? newService[33] : "",
                T1730 = (genSchedule(34, newIndex)) ? newService[34] : "",
                T1800 = (genSchedule(35, newIndex)) ? newService[35] : "",
                T1830 = (genSchedule(36, newIndex)) ? newService[36] : "",
                T1900 = (genSchedule(37, newIndex)) ? newService[37] : "",
                T1930 = (genSchedule(38, newIndex)) ? newService[38] : "",
                T2000 = (genSchedule(39, newIndex)) ? newService[39] : "",
                T2030 = (genSchedule(40, newIndex)) ? newService[40] : "",
                T2100 = (genSchedule(41, newIndex)) ? newService[41] : "",
                T2130 = (genSchedule(42, newIndex)) ? newService[42] : "",
                T2200 = (genSchedule(43, newIndex)) ? newService[43] : "",
                T2230 = (genSchedule(44, newIndex)) ? newService[44] : "",
                T2300 = (genSchedule(45, newIndex)) ? newService[45] : "",
                T2330 = (genSchedule(46, newIndex)) ? newService[46] : "",
                T0000 = (genSchedule(47, newIndex)) ? newService[47] : "",
                RUNNING = getCount(id, dt),
                Remark = getQuece(id, dt) + ""
            };
            return schedule;
        }

        //ดูจำนวนรอบเวลาของคน
        public static int getCount(string id,DateTime dt)
        {
            int count = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSchedule", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inESno", id));
                cmd.Parameters.Add(new SqlParameter("@inQ", ""));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", dt));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", Authorize.getUser()));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "7"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    count = int.Parse(dr["icount"].ToString());

                }
                dr.Close();
            }
            return count;
        }
        //ดู Quece ของพนักงาน
        public static int getQuece(string id, DateTime dt)
        {
            int count = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSchedule", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inESno", id));
                cmd.Parameters.Add(new SqlParameter("@inQ", ""));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", dt));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", Authorize.getUser()));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "8"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    count = int.Parse(dr["EQuece"].ToString());
                }
                dr.Close();
            }
            return count;
        }
        //Funciton
        public static string getTimePeriod(string _time)
        {
            if (_time == "") return "";
            _time = _time.Replace(' ', ':');
            string[] time = _time.Split(':');
            int hr = Convert.ToInt16(time[0]);
            int minute = Convert.ToInt16(time[1]);
            if (minute < 15)
            {
                minute = 0;
            }
            else if (minute <= 30)
            {
                minute = 30;
            }
            else if (minute <= 45)
            {
                minute = 30;
            }
            else
            {
                hr++;
                minute = 0;
            }
            if ((hr == 24) && (minute > 0))
            {
                hr = 24;
                minute = 0;
            }
            return hr.ToString("00") + ":" + minute.ToString("00");
        }

        public static int getTimeIndex(string time)
        {
            Console.WriteLine("getTimeIndex."+time);
            int index = -1;
            switch (time)
            {
                case "00:30": index = 0; break;
                case "01:00": index = 1; break;
                case "01:30": index = 2; break;
                case "02:00": index = 3; break;
                case "02:30": index = 4; break;
                case "03:00": index = 5; break;
                case "03:30": index = 6; break;
                case "04:00": index = 7; break;
                case "04:30": index = 8; break;
                case "05:00": index = 9; break;

                case "05:30": index = 10; break;
                case "06:00": index = 11; break;
                case "06:30": index = 12; break;
                case "07:00": index = 13; break;
                case "07:30": index = 14; break;
                case "08:00": index = 15; break;
                case "08:30": index = 16; break;
                case "09:00": index = 17; break;
                case "09:30": index = 18; break;
                case "10:00": index = 19; break;
                case "10:30": index = 20; break;
                case "11:00": index = 21; break;
                case "11:30": index = 22; break;
                case "12:00": index = 23; break;
                case "12:30": index = 24; break;
                case "13:00": index = 25; break;
                case "13:30": index = 26; break;
                case "14:00": index = 27; break;
                case "14:30": index = 28; break;
                case "15:00": index = 29; break;
                case "15:30": index = 30; break;
                case "16:00": index = 31; break;
                case "16:30": index = 32; break;
                case "17:00": index = 33; break;
                case "17:30": index = 34; break;
                case "18:00": index = 35; break;
                case "18:30": index = 36; break;
                case "19:00": index = 37; break;
                case "19:30": index = 38; break;
                case "20:00": index = 39; break;
                case "20:30": index = 40; break;
                case "21:00": index = 41; break;
                case "21:30": index = 42; break;
                case "22:00": index = 43; break;
                case "22:30": index = 44; break;
                case "23:00": index = 45; break;
                case "23:30": index = 46; break;
                case "00:00": index = 47; break;

                //case "12:00": index = 0; break;
                //case "12:30": index = 1; break;
                //case "13:00": index = 2; break;
                //case "13:30": index = 3; break;
                //case "14:00": index = 4; break;
                //case "14:30": index = 5; break;
                //case "15:00": index = 6; break;
                //case "15:30": index = 7; break;
                //case "16:00": index = 8; break;
                //case "16:30": index = 9; break;
                //case "17:00": index = 10; break;
                //case "17:30": index = 11; break;
                //case "18:00": index = 12; break;
                //case "18:30": index = 13; break;
                //case "19:00": index = 14; break;
                //case "19:30": index = 15; break;
                //case "20:00": index = 16; break;
                //case "20:30": index = 17; break;
                //case "21:00": index = 18; break;
                //case "21:30": index = 19; break;
                //case "22:00": index = 20; break;
                //case "22:30": index = 21; break;
                //case "23:00": index = 22; break;
                //case "23:30": index = 23; break;
                //case "24:00": index = 24; break;
                default: index = 47; break;
            }
            return index;
        }

        public static DateTime getTime(int index)
        {
            string time = DateTime.Now.ToShortTimeString();
            switch (index)
            {
                case 0: time = "00:30"; break;
                case 1: time = "01:00"; break;
                case 2: time = "01:30"; break;
                case 3: time = "02:00"; break;
                case 4: time = "02:30"; break;
                case 5: time = "03:00"; break;
                case 6: time = "03:30"; break;
                case 7: time = "04:00"; break;
                case 8: time = "04:30"; break;
                case 9: time = "05:00"; break;
                case 10: time = "05:30"; break;
                case 11: time = "06:00"; break;
                case 12: time = "06:30"; break;
                case 13: time = "07:00"; break;
                case 14: time = "07:30"; break;
                case 15: time = "08:00"; break;
                case 16: time = "08:30"; break;
                case 17: time = "09:00"; break;
                case 18: time = "09:30"; break;
                case 19: time = "10:00"; break;
                case 20: time = "10:30"; break;
                case 21: time = "11:00"; break;
                case 22: time = "11:30"; break;
                case 23: time = "12:00"; break;
                case 24: time = "12:30"; break;
                case 25: time = "13:00"; break;
                case 26: time = "13:30"; break;
                case 27: time = "14:00"; break;
                case 28: time = "14:30"; break;
                case 29: time = "15:00"; break;
                case 30: time = "15:30"; break;
                case 31: time = "16:00"; break;
                case 32: time = "16:30"; break;
                case 33: time = "17:00"; break;
                case 34: time = "17:30"; break;
                case 35: time = "18:00"; break;
                case 36: time = "18:30"; break;
                case 37: time = "19:00"; break;
                case 38: time = "19:30"; break;
                case 39: time = "20:00"; break;
                case 40: time = "20:30"; break;
                case 41: time = "21:00"; break;
                case 42: time = "21:30"; break;
                case 43: time = "22:00"; break;
                case 44: time = "22:30"; break;
                case 45: time = "23:00"; break;
                case 46: time = "23:30"; break;
                case 47: time = "00:00"; break;
                case -1: time = "00:00"; break;
            }

            return Convert.ToDateTime(time);
        }

        private static List<int> genSesqunce(List<int> indexs)
        {
            List<int> newIndex = new List<int>();
            for (int i = 0; i < indexs.Count; i++)
            {
                if (i % 2 == 1)
                {//เลขคี่
                    if ((indexs[i] - indexs[i - 1]) > 1)
                    {
                        for (int j = 1; j < (indexs[i] - indexs[i - 1]); j++)
                        {
                            newIndex.Add(indexs[i - 1] + j);
                        }
                    }
                    newIndex.Add(indexs[i]);
                }
                else
                {//เพิ่มเลขคู่
                    newIndex.Add(indexs[i]);
                }

            }
            return newIndex;
        }

        private static string[] genSesqunce(List<int> indexs, List<string> services)
        {
            string[] newService = new string[49];
            for (int i = 0; i < indexs.Count; i++)
            {
                if (i % 2 == 1)
                {//เลขคี่
                    if ((indexs[i] - indexs[i - 1]) > 1)
                    {
                        for (int j = 1; j < (indexs[i] - indexs[i - 1]); j++)
                        {
                            newService[indexs[i - 1] + j] = services[i];
                        }
                    }
                    newService[indexs[i]] = services[i];
                }
                else
                {//เพิ่มเลขคู่
                    newService[indexs[i]] = services[i];
                }

            }
            return newService;
        }

        private static bool genSchedule(int index, List<int> indexs)
        {
            if (index == -1) return false;
            bool bFound = false;
            for (int i = 0; i < indexs.Count; i++)
            {
                if (index == indexs[i])
                {
                    bFound = true;
                    break;
                }
            }
            return bFound;
        }

        //ดูคิวล่าสุด
        public static int getQ(CSchedule schedule)
        {
            int count = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSchedule", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inESno", schedule.ID));
                cmd.Parameters.Add(new SqlParameter("@inQ", schedule.Remark));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", schedule.KEYDATE));
                cmd.Parameters.Add(new SqlParameter("@inkeyUser", schedule.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", schedule.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    count = (DBNull.Value == dr["icount"]) ? 0 : Convert.ToInt16(dr["icount"]);
                }
            }
            return count;
        }
    }
}
