﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Rakuten.Structure;
using System.Data;
using System.Data.SqlClient;

namespace Rakuten.DAL
{
    public class DALInvoice
    {
        public static List<CRpt02> getInvoice(int _tranID)
        {
            List<CRpt02> cuss = new List<CRpt02>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("PrintInvoiceData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inSCusTranID", _tranID));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CRpt02 cus = new CRpt02
                    {
                        RUNNING = running,
                        ID = (Convert.ToInt16(dr["SBook"]) + "" + Convert.ToInt16(dr["SRunning"]).ToString("000")),
                        Name = Convert.ToString(dr["Mname"]),
                        Surname = Convert.ToString(dr["Msurname"]),
                        Address = Convert.ToString(dr["MContactAddress"]),
                        DETAIL = Convert.ToString(dr["RDetail"]),
                        Amount = Convert.ToDouble(dr["SAmount"]),
                        Discount = Convert.ToDouble(dr["SDiscount"]),
                        Remark = Convert.ToString(dr["Quantity"]),
                    };
                    running++;
                    cuss.Add(cus);
                }
                dr.Close();
            }
            return cuss;
        }
    }
}
