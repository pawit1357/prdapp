﻿namespace Rakuten
{
    partial class FrmOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("อีเมล์");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("อัตราค่าปรับ");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("ลบข้อมูลทั้งหมด");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("ส่งรายงานไปยัง Email");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Setting", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4});
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TXT_HOST = new System.Windows.Forms.TextBox();
            this.TXT_PASS = new System.Windows.Forms.TextBox();
            this.TXT_USER = new System.Windows.Forms.TextBox();
            this.TXT_FROM = new System.Windows.Forms.TextBox();
            this.TXT_TO = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CB_SEND = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TXT_FINE = new System.Windows.Forms.TextBox();
            this.B_SAVE = new System.Windows.Forms.Button();
            this.B_CANCEL = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DP_START = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.DP_END = new System.Windows.Forms.DateTimePicker();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TXT_HOST);
            this.groupBox2.Controls.Add(this.TXT_PASS);
            this.groupBox2.Controls.Add(this.TXT_USER);
            this.groupBox2.Controls.Add(this.TXT_FROM);
            this.groupBox2.Controls.Add(this.TXT_TO);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.CB_SEND);
            this.groupBox2.Location = new System.Drawing.Point(180, 55);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(352, 200);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ตั้งค่าส่งเมล์อัตโนมัติ";
            // 
            // TXT_HOST
            // 
            this.TXT_HOST.Location = new System.Drawing.Point(112, 132);
            this.TXT_HOST.Name = "TXT_HOST";
            this.TXT_HOST.Size = new System.Drawing.Size(143, 20);
            this.TXT_HOST.TabIndex = 5;
            this.TXT_HOST.Text = "smtp.gmail.com";
            // 
            // TXT_PASS
            // 
            this.TXT_PASS.Location = new System.Drawing.Point(112, 106);
            this.TXT_PASS.Name = "TXT_PASS";
            this.TXT_PASS.Size = new System.Drawing.Size(143, 20);
            this.TXT_PASS.TabIndex = 4;
            this.TXT_PASS.Text = "c8jlyd8o";
            this.TXT_PASS.UseSystemPasswordChar = true;
            // 
            // TXT_USER
            // 
            this.TXT_USER.Location = new System.Drawing.Point(112, 80);
            this.TXT_USER.Name = "TXT_USER";
            this.TXT_USER.Size = new System.Drawing.Size(143, 20);
            this.TXT_USER.TabIndex = 3;
            this.TXT_USER.Text = "pawitvaap@gmail.com";
            // 
            // TXT_FROM
            // 
            this.TXT_FROM.Location = new System.Drawing.Point(112, 28);
            this.TXT_FROM.Name = "TXT_FROM";
            this.TXT_FROM.Size = new System.Drawing.Size(143, 20);
            this.TXT_FROM.TabIndex = 1;
            this.TXT_FROM.Text = "report@rakutenspa.com";
            // 
            // TXT_TO
            // 
            this.TXT_TO.Location = new System.Drawing.Point(112, 53);
            this.TXT_TO.Name = "TXT_TO";
            this.TXT_TO.Size = new System.Drawing.Size(143, 20);
            this.TXT_TO.TabIndex = 2;
            this.TXT_TO.Text = "info@rakutenspa.com";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(48, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 14);
            this.label5.TabIndex = 39;
            this.label5.Text = "password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(46, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 14);
            this.label3.TabIndex = 38;
            this.label3.Text = "username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(72, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 14);
            this.label2.TabIndex = 37;
            this.label2.Text = "Host";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(87, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 14);
            this.label1.TabIndex = 36;
            this.label1.Text = "to";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(72, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 14);
            this.label4.TabIndex = 35;
            this.label4.Text = "From";
            // 
            // CB_SEND
            // 
            this.CB_SEND.AutoSize = true;
            this.CB_SEND.Location = new System.Drawing.Point(112, 167);
            this.CB_SEND.Name = "CB_SEND";
            this.CB_SEND.Size = new System.Drawing.Size(76, 17);
            this.CB_SEND.TabIndex = 34;
            this.CB_SEND.Text = "Auto Send";
            this.CB_SEND.UseVisualStyleBackColor = true;
            this.CB_SEND.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.TXT_FINE);
            this.groupBox1.Location = new System.Drawing.Point(180, 55);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(352, 200);
            this.groupBox1.TabIndex = 121;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "อัตราค่าปรับ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(43, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 14);
            this.label11.TabIndex = 35;
            this.label11.Text = "อัคราค่าปรับ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(181, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 14);
            this.label7.TabIndex = 36;
            this.label7.Text = "บาท";
            // 
            // TXT_FINE
            // 
            this.TXT_FINE.Location = new System.Drawing.Point(112, 28);
            this.TXT_FINE.Name = "TXT_FINE";
            this.TXT_FINE.Size = new System.Drawing.Size(63, 20);
            this.TXT_FINE.TabIndex = 1;
            this.TXT_FINE.Text = "5";
            this.TXT_FINE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_FINE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckIsNumber_KeyPress);
            // 
            // B_SAVE
            // 
            this.B_SAVE.Image = global::Rakuten.Properties.Resources.disk_blue_ok;
            this.B_SAVE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.B_SAVE.Location = new System.Drawing.Point(369, 261);
            this.B_SAVE.Name = "B_SAVE";
            this.B_SAVE.Size = new System.Drawing.Size(70, 36);
            this.B_SAVE.TabIndex = 6;
            this.B_SAVE.Text = "บันทึก";
            this.B_SAVE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.B_SAVE.UseVisualStyleBackColor = true;
            this.B_SAVE.Click += new System.EventHandler(this.B_SAVE_Click);
            // 
            // B_CANCEL
            // 
            this.B_CANCEL.Image = global::Rakuten.Properties.Resources.redo;
            this.B_CANCEL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.B_CANCEL.Location = new System.Drawing.Point(445, 261);
            this.B_CANCEL.Name = "B_CANCEL";
            this.B_CANCEL.Size = new System.Drawing.Size(70, 36);
            this.B_CANCEL.TabIndex = 7;
            this.B_CANCEL.Text = "ยกเลิก";
            this.B_CANCEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.B_CANCEL.UseVisualStyleBackColor = true;
            this.B_CANCEL.Click += new System.EventHandler(this.B_CANCEL_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.Green;
            this.label6.Location = new System.Drawing.Point(72, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 25);
            this.label6.TabIndex = 119;
            this.label6.Text = "ตั้งค่า";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::Rakuten.Properties.Resources.product;
            this.pictureBox1.Location = new System.Drawing.Point(10, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(56, 50);
            this.pictureBox1.TabIndex = 117;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(-2, -1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(543, 50);
            this.pictureBox2.TabIndex = 118;
            this.pictureBox2.TabStop = false;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 54);
            this.treeView1.Name = "treeView1";
            treeNode1.Checked = true;
            treeNode1.Name = "Node1";
            treeNode1.Text = "อีเมล์";
            treeNode2.Name = "Node3";
            treeNode2.Text = "อัตราค่าปรับ";
            treeNode3.Name = "Node0";
            treeNode3.Text = "ลบข้อมูลทั้งหมด";
            treeNode4.Name = "Node1";
            treeNode4.Text = "ส่งรายงานไปยัง Email";
            treeNode5.Name = "Node0";
            treeNode5.Text = "Setting";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode5});
            this.treeView1.Size = new System.Drawing.Size(159, 200);
            this.treeView1.TabIndex = 1;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.DP_START);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.DP_END);
            this.groupBox3.Location = new System.Drawing.Point(180, 56);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(351, 198);
            this.groupBox3.TabIndex = 122;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ลบข้อมูล";
            // 
            // DP_START
            // 
            this.DP_START.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DP_START.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DP_START.Location = new System.Drawing.Point(90, 43);
            this.DP_START.Name = "DP_START";
            this.DP_START.Size = new System.Drawing.Size(121, 26);
            this.DP_START.TabIndex = 135;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(25, 50);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 14);
            this.label15.TabIndex = 137;
            this.label15.Text = "วันที่เริ่มต้น";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(27, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 14);
            this.label8.TabIndex = 138;
            this.label8.Text = "วันที่สิ้นสุด";
            // 
            // DP_END
            // 
            this.DP_END.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DP_END.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DP_END.Location = new System.Drawing.Point(90, 75);
            this.DP_END.Name = "DP_END";
            this.DP_END.Size = new System.Drawing.Size(121, 26);
            this.DP_END.TabIndex = 136;
            // 
            // FrmOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 305);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.B_SAVE);
            this.Controls.Add(this.B_CANCEL);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmOption";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Option";
            this.Load += new System.EventHandler(this.FrmOption_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox CB_SEND;
        private System.Windows.Forms.Button B_SAVE;
        private System.Windows.Forms.Button B_CANCEL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TXT_HOST;
        private System.Windows.Forms.TextBox TXT_PASS;
        private System.Windows.Forms.TextBox TXT_USER;
        private System.Windows.Forms.TextBox TXT_FROM;
        private System.Windows.Forms.TextBox TXT_TO;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TXT_FINE;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker DP_START;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker DP_END;
    }
}