﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Rakuten.Structure;
using Rakuten.DAL;
using Rakuten.Utility;
using System.Collections.Generic;
using System.Reflection;

namespace Rakuten
{
    public partial class FrmEmpSummary : Form
    {
        public FrmEmpSummary()
        {
            InitializeComponent();
        }

        private void FrmEmpSummary_Load(object sender, EventArgs e)
        {
            initial();
            dataGridView1.RefreshEdit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                switch (e.ColumnIndex)
                { 
                    case 0:case 1:case 2:case 5:
                        break;
                    default:
                        if (!MyFunction.isNumber(dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()))
                        {
                            dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                        }
                        else
                        {
                            dataGridView1.Rows[e.RowIndex].Cells[10].Value = ((Convert.ToDouble(dataGridView1.Rows[e.RowIndex].Cells[6].Value) + Convert.ToDouble(dataGridView1.Rows[e.RowIndex].Cells[7].Value)) - (Convert.ToDouble(dataGridView1.Rows[e.RowIndex].Cells[8].Value) + Convert.ToDouble(dataGridView1.Rows[e.RowIndex].Cells[9].Value))).ToString();
                        }
                        break;
                }
            }
        }

        private void BProcess_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            //ตรวจสอบว่ามีการบันทึกข้อมูลไว้ก่อนหรือไม่
            bool isTranfered = DALEmployee.isTransfered(new CEmployeeSummary
            {
                ID = "",
                Income = "",
                GuestReservation = 0,
                SoldMember = "",
                CheckIN = "",
                CheckOUT = "",
                RUNNING = 0,
                Fine = "",
                Late = 0,
                ReMain = "",
                Period = getPeriod(),
                KEYDATE = DTP_DAY.Value,
                KEYUSER = "",
                FLAG = "5"
            });
            if (isTranfered)
            {
                //---มีข้อมูลสรุปค่ามือบันทึกไว้แล้ว
                switch (RD_EVERYDAY.Checked)
                { 
                    case true:
                        //กรณีได้มีการทำรายการสรุปค่ามือไว้แล้ว (แก้ไขรายการได้อย่างเดียว)
                        Management.ShowMsg("ได้มีการบันทึกข้อมูลค่ามือพนักงาน ประจำวันที่ " + DTP_DAY.Value.ToShortDateString() + " เรียบร้อยแล้ว");
                        dataGridView1.DataSource = DALEmployee.getlistEmpSummary(new CEmployeeSummary
                        {
                            ID = "",
                            Income = "",
                            GuestReservation = 0,
                            SoldMember = "",
                            Fine = "",
                            ReMain = "",
                            Late = 0,
                            CheckIN = "",
                            CheckOUT = "",
                            RUNNING = 0,
                            Period = getPeriod(),
                            KEYDATE = DTP_DAY.Value,
                            KEYUSER = "",
                            FLAG = "6"
                        });
                        if (dataGridView1.Rows.Count > 0)
                        {
                            B_SAVE.Text = "ปรับปรุงข้อมูล";
                            B_SAVE.Enabled = true;
                        }
                        break;
                    case false:
                        Management.ShowMsg("ได้มีการบันทึกข้อมูลค่ามือพนักงาน งวดที่ " + getPeriod() + " เรียบร้อยแล้ว");
                        break;
                }
            }
            else
            {
                //---ยังไม่มีข้อมูลสรุปค่ามือบันทึกไว้แล้ว
                dataGridView1.DataSource = DALEmployee.getlistEmpSummary(new CEmployeeSummary
                {
                    ID = "",
                    Income = "",
                    GuestReservation = 0,
                    SoldMember = "",
                    Fine = "",
                    ReMain = "",
                    Late = 0,
                    CheckIN = "",
                    CheckOUT = "",
                    RUNNING = 0,
                    Period = getPeriod(),
                    KEYDATE = DTP_DAY.Value,
                    KEYUSER = "",
                    FLAG = "4"
                });
                if (dataGridView1.Rows.Count > 0)
                {
                    B_SAVE.Text = "บันทึกข้อมูล";
                    B_SAVE.Enabled = true;
                    updateComission();//update Commission List
                    dataGridView1.Refresh();
                }
                else
                {
                    Management.ShowMsg("ไม่พบข้อมูลการทำรายการในช่วง "+getPeriod());
                    initial();
                }
            }
            Cursor = Cursors.Default;
        }

        private void B_SAVE_Click(object sender, EventArgs e)
        {
            int count = 0;
            switch (B_SAVE.Text)
            {
                case "บันทึกข้อมูล":
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        CEmployeeSummary es = new CEmployeeSummary
                        {
                            Remark = dataGridView1.Rows[i].Cells[1].Value.ToString(),
                            ID = dataGridView1.Rows[i].Cells[1].Value.ToString(),
                            CheckIN = Convert.ToString(dataGridView1.Rows[i].Cells[2].Value),//เข้างาน
                            RUNNING = Convert.ToInt16(dataGridView1.Rows[i].Cells[3].Value),//รอบงาน
                            GuestReservation = Convert.ToInt16(dataGridView1.Rows[i].Cells[4].Value),//แขกจอง
                            CheckOUT = Convert.ToString(dataGridView1.Rows[i].Cells[5].Value),
                            Income = dataGridView1.Rows[i].Cells[6].Value.ToString(),
                            SoldMember = dataGridView1.Rows[i].Cells[7].Value.ToString(),
                            Fine = dataGridView1.Rows[i].Cells[8].Value.ToString(),
                            Late = Convert.ToDouble(dataGridView1.Rows[i].Cells[9].Value.ToString()),
                            ReMain = ((Convert.ToDouble(dataGridView1.Rows[i].Cells[6].Value) + Convert.ToDouble(dataGridView1.Rows[i].Cells[7].Value)) - (Convert.ToDouble(dataGridView1.Rows[i].Cells[8].Value) + Convert.ToDouble(dataGridView1.Rows[i].Cells[9].Value))).ToString(),
                            Period = getPeriod(),
                            KEYDATE = DTP_DAY.Value,
                            KEYUSER = Authorize.getUser(),
                            FLAG = Management.Insert
                        };
                        count += DALEmployee.manageMaster(es);
                    }
                    break;
                case "ปรับปรุงข้อมูล":
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        CEmployeeSummary es = new CEmployeeSummary
                        {
                            Remark = dataGridView1.Rows[i].Cells[31].Value.ToString(),
                            ID = dataGridView1.Rows[i].Cells[1].Value.ToString(),
                            CheckIN = Convert.ToString(dataGridView1.Rows[i].Cells[2].Value),//เข้างาน
                            RUNNING = Convert.ToInt16(dataGridView1.Rows[i].Cells[3].Value),//รอบงาน
                            GuestReservation = Convert.ToInt16(dataGridView1.Rows[i].Cells[4].Value),//แขกจอง
                            CheckOUT = Convert.ToString(dataGridView1.Rows[i].Cells[5].Value),
                            Income = dataGridView1.Rows[i].Cells[6].Value.ToString(),
                            SoldMember = dataGridView1.Rows[i].Cells[7].Value.ToString(),
                            Fine = dataGridView1.Rows[i].Cells[8].Value.ToString(),
                            Late = Convert.ToDouble(dataGridView1.Rows[i].Cells[9].Value.ToString()),
                            ReMain = ((Convert.ToDouble(dataGridView1.Rows[i].Cells[6].Value) + Convert.ToDouble(dataGridView1.Rows[i].Cells[7].Value)) - (Convert.ToDouble(dataGridView1.Rows[i].Cells[8].Value) + Convert.ToDouble(dataGridView1.Rows[i].Cells[9].Value))).ToString(),
                            Period = getPeriod(),
                            KEYDATE = DTP_DAY.Value,
                            KEYUSER = Authorize.getUser(),
                            FLAG = Management.Update
                        };
                        count += DALEmployee.manageMaster(es);
                    }
                    break;
            }
            if (count == dataGridView1.Rows.Count)
            {
                Management.ShowMsg(Management.INSERT_SUCCESS);
                Close();
            }
        }

        private void initial()
        {
            B_SAVE.Enabled = false;
            CBO_PERIOD.SelectedIndex = 0;
            groupBox1.Visible = (RD_EVERYDAY.Checked) ? true : false;
            groupBox2.Visible = (RD_EVERY15DAY.Checked) ? true : false;
        }

        private string getPeriod()
        {
            return (CBO_PERIOD.SelectedIndex+1).ToString() +"-"+ Convert.ToInt16(DP_START.Value.Month.ToString()) +"-"+ DP_START.Value.Year.ToString("0000");
        }

        private void RD_EVERYDAY_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Visible = (RD_EVERYDAY.Checked) ? true : false;
            groupBox2.Visible = (RD_EVERY15DAY.Checked) ? true : false;
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }


        private void updateComission()
        {
            List<string> data = new List<string>();
            List<CEmployeeSummary> lists = DALEmployee.getComission(new CEmployeeSummary
                {
                    ID = "",
                    Income = "",
                    GuestReservation = 0,
                    SoldMember = "",
                    Fine = "",
                    ReMain = "",
                    Late = 0,
                    CheckIN = "",
                    CheckOUT = "",
                    RUNNING = 0,
                    Period = getPeriod(),
                    KEYDATE = DTP_DAY.Value,
                    KEYUSER = "",
                    FLAG = "7"
                });
            foreach (CEmployeeSummary c in lists)
            {
                string[] arData = c.ID.Split(',');
                for (int i = 0; i < arData.Length; i++)
                {
                    data.Add(arData[i] + "#" + c.Income);
                }
            }
            //Begin Update commissioin
            foreach (string s in data)
            { 
                string[] arCom = s.Split('#');
                updateGridViewCommission(arCom[0], Convert.ToDouble(arCom[1]));
            }
            Console.WriteLine("Result:--");
        }

        private void updateGridViewCommission(string id,double value)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[1].Value.ToString().Equals(id))
                {
                    dataGridView1.Rows[i].Cells[7].Value = (Convert.ToDouble(dataGridView1.Rows[i].Cells[7].Value) + value).ToString();
                    break;
                }
            }
        }

    }
}
