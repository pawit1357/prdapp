﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CMember : CPerson
    {
        private string _type;
        private string _package;
        private string _Therapist;
        private string _scent;

        public string Type
        {
            set { this._type = value; }
            get { return this._type; }
        }
        public string Package
        {
            set { this._package = value; }
            get { return this._package; }
        }
        public string Therapist
        {
            set { this._Therapist = value; }
            get { return this._Therapist; }
        }
        public string Scent
        {
            set { this._scent = value; }
            get { return this._scent; }
        }
    }
}
