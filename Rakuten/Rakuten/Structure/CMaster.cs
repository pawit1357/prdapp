﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CMaster
    {
        private int running;
        private string id;
        private string detail;
        private DateTime keydate;
        private DateTime lastedit;
        private string keyuser;
        private string flag;
        
        private DateTime _startDate;
        private DateTime _endDate;
        private string _remark;

        public int RUNNING
        {
            set { this.running = value; }
            get { return this.running; }
        }
        public string ID
        {
            set { this.id = value; }
            get { return this.id; }
        }
        public string DETAIL
        {
            set { this.detail = value; }
            get { return this.detail; }
        }
        public DateTime KEYDATE 
        {
            set { this.keydate = value; }
            get { return this.keydate; }
        }
        public DateTime LASTEDIT
        {
            set { this.lastedit = value; }
            get { return this.lastedit; }
        }
        public string KEYUSER
        {
            set { this.keyuser = value; }
            get { return this.keyuser; }
        }
        public string FLAG
        {
            set { this.flag = value; }
            get { return this.flag; }
        }
        public DateTime StartDate
        {
            set { this._startDate = value; }
            get { return this._startDate; }
        }
        public DateTime EndDate
        {
            set { this._endDate = value; }
            get { return this._endDate; }
        }
        public string Remark
        {
            set { this._remark = value; }
            get { return this._remark; }
        }
    }
}
