﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CScent : CMaster
    {
        private int _min;
        private int _max;
        private string _scentType;//ประเภทวัสดุ
        private int _count;//จำนวน
        private string _programType;//ประเภทรายการ

        public string ScentType
        {
            set { this._scentType = value; }
            get { return this._scentType; }
        }
        public int Count
        {
            set { this._count = value; }
            get { return this._count; }
        }
        public string ProgramType
        {
            set { this._programType = value; }
            get { return this._programType; }
        }
        public int Min
        {
            set { this._min = value; }
            get { return this._min; }
        }
        public int Max
        {
            set { this._max = value; }
            get { return this._max; }
        }
    }
}
