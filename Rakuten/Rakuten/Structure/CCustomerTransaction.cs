﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CCustomerTransaction:CPerson
    {
        private string cusID;
        private string therapistID;
        private string therapistRevID;
        private string massageType;
        private string hour;
        private DateTime date;
        private DateTime time;
        private string room;
        private string customerType;
        private string package;
        private string type;
        private string status;
        private string scent;
        private int scentCount;
        private string scent1;
        private int scentCount1;
        private string specialPackage;
        public string CustomerID
        {
            set { this.cusID = value; }
            get { return this.cusID; }
        }
        public string TherapistID 
        {
            set { this.therapistID = value; }
            get { return this.therapistID; }
        }
        public string TherapistRevID
        {
            set { this.therapistRevID = value; }
            get { return this.therapistRevID; }
        }
        public string MassageType
        {
            set { this.massageType = value; }
            get { return this.massageType; }
        }
        public string Hour 
        {
            set { this.hour = value; }
            get { return this.hour; }
        }
        public DateTime Date 
        {
            set { this.date = value; }
            get { return this.date; }
        }
        public DateTime Time 
        {
            set { this.time = value; }
            get { return this.time; }
        }
        public string Room 
        {
            set { this.room = value; }
            get { return this.room; }
        }
        public string Customertype
        {
            set { this.customerType = value; }
            get { return this.customerType; }
        }
        public string Package 
        {
            set { this.package = value; }
            get { return this.package; }
        }
        public string Type
        {
            set { this.type = value; }
            get { return this.type; }
        }
        public string Status
        {
            set { this.status = value; }
            get { return this.status; }
        }
        public string ScentType
        {
            set { this.scent = value; }
            get { return this.scent; }
        }
        public int ScentCount
        {
            set { this.scentCount = value; }
            get { return this.scentCount; }
        }
        public string ScentType1
        {
            set { this.scent1 = value; }
            get { return this.scent1; }
        }
        public int ScentCount1
        {
            set { this.scentCount1 = value; }
            get { return this.scentCount1; }
        }
        public string SpecialPackage
        {
            set { this.specialPackage = value; }
            get { return this.specialPackage; }
        }
    }
}
