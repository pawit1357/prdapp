﻿
namespace Rakuten.Structure
{
    public class CReport14:CEmployee
    {
        private double count1;
        private double count2;
        private double count3;
        public double Count1
        {
            set { this.count1 = value; }
            get { return this.count1; }
        }
        public double Count2
        {
            set { this.count2 = value; }
            get { return this.count2; }
        }
        public double Count3
        {
            set { this.count3 = value; }
            get { return this.count3; }
        }
    }
}
