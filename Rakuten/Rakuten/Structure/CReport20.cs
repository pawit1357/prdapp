﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CReport20:CMaster
    {
        private double amount;
        private double perday;
        private int count;

        public double Amount
        {
            set { this.amount = value; }
            get { return this.amount; }
        }
        public double PerDay
        {
            set { this.perday = value; }
            get { return this.perday; }
        }
        public int Count
        {
            set { this.count = value; }
            get { return this.count; }
        }
    }
}
