﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CReport1:CMember
    {

        private double _amount;//จำนวนเงิน
        private string _payType;//รูปแบบการชำระเงิน
        private string _customerType;//ประเภทสมาชิก
        //private string _therapist;//ชื่อพนักงาน
        private double _therapistCost;//ค่ามือสมาชิก

        public double Amount
        {
            set { this._amount = value; }
            get { return this._amount; }
        }
        public string PayType 
        {
            set { this._payType = value; }
            get { return this._payType; }
        }
        public string CustomerType
        {
            set { this._customerType = value; }
            get { return this._customerType; }
        }
        //public string Therapist
        //{
        //    set { this._therapist = value; }
        //    get { return this._therapist; }
        //}
        public double TherapistPrice
        {
            set { this._therapistCost = value; }
            get { return this._therapistCost; }
        }
    }
}
