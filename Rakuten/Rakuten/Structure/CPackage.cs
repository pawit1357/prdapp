﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CPackage : CMaster
    {
        private string _hour;
        private double _price;
        private string _moneyunit;
        private string _massageType;
        private int _percourse;
        private string _custype;
        private double _therapistPrice;
        private int _memberUse;
        public string Hour
        {
            set { this._hour = value; }
            get { return this._hour; }
        }
        public double Price
        {
            set { this._price = value; }
            get { return this._price; }
        }
        public double TherapistPrice
        {
            set { this._therapistPrice = value; }
            get { return this._therapistPrice; }
        }
        public string MoneyUnit
        {
            set { this._moneyunit = value; }
            get { return this._moneyunit; }
        }
        public string MassageType
        {
            set { this._massageType = value; }
            get { return this._massageType; }
        }
        public int PerCourse 
        {
            set { this._percourse = value; }
            get { return this._percourse; }
        }
        public string CustomerType 
        {
            set { this._custype = value; }
            get { return this._custype; }
        }
        public int MemberUse
        {
            set { this._memberUse = value; }
            get { return this._memberUse; }
        }
    }
}
