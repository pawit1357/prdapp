﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CPermission:CMaster
    {
        private string empID;
        private string frm;
        private byte insert;
        private byte delete;
        private byte update;
        private byte view;

        public string EMPID
        {
            set { this.empID = value; }
            get { return this.empID; }
        }
        public string From
        {
            set { this.frm = value; }
            get { return this.frm; }
        }
        public byte Insert
        {
            set { this.insert = value; }
            get { return this.insert; }
        }
        public byte Delete
        {
            set { this.delete = value; }
            get { return this.delete; }
        }
        public byte Update
        {
            set { this.update = value; }
            get { return this.update; }
        }
        public byte View
        {
            set { this.view = value; }
            get { return this.view; }
        }
    }
}
