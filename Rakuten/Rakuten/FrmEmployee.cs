﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using Rakuten.Structure;
using Rakuten.DAL;
using Rakuten.Utility;
using System.Data.SqlClient;
namespace Rakuten
{
    public partial class FrmEmployee : Form
    {
        private FormStatus formStatus = FormStatus.Normal;
        private DBStatus status = DBStatus.Insert;//Initial Status
        public List<CEmployee> lists = null;
        public int index;
        private bool bSearch = false;
        public bool bSelected = false;
        private FrmMain main;
        //Group Parameter
        public string group = "";
        public FrmEmployee(FrmMain _main)//Normal
        {
            InitializeComponent();
            this.main = _main;
        }

        public FrmEmployee(FormStatus _fStatus,FrmMain _main)//Screen
        {
            InitializeComponent();
            this.formStatus = _fStatus;
            this.main = _main;
        }

        public FrmEmployee(String _group, FormStatus _fStatus)//Screen
        {
            InitializeComponent();
            this.formStatus = _fStatus;
            this.group = _group;
        }

        private void FrmCustomer_Load(object sender, EventArgs e)
        {
            initial();

            if (main == null)//
            {
                search();
            }
            else
            {
                if (!main.Therapist.Equals("0000"))
                {
                    //ใช้กรณีมาจาก การเลือกจาก Therapist Sub Menu
                    lists = DALEmployee.getList(new CEmployee
                    {
                        ID = main.Therapist,
                        Type = "",
                        Sex = txtSearch.Text,
                        Title = txtSearch.Text,
                        Name = txtSearch.Text,
                        Surname = txtSearch.Text,
                        NickName = txtSearch.Text,
                        Nation = txtSearch.Text,
                        BirthDay = DateTime.Now,
                        Address = txtSearch.Text,
                        Mobile = txtSearch.Text,
                        Email = txtSearch.Text,
                        Status = "",
                        Period = "",
                        KEYDATE = DateTime.Now,
                        KEYUSER = Authorize.getUser(),
                        LASTEDIT = DateTime.Now,
                        FLAG = "4"//Searh Flag (6 คือ ให้แสดงเฉพาะที่เป็นหมอนวด 7 คือ แสดงเฉพาะเจ้าหน้าที่)
                    }
                                            );
                    //Check Have Data!!
                    if (lists.Count == 0)
                    {
                        dataGridView1.DataSource = null;
                        if (bSearch)
                        {
                            Management.ShowMsg(Management.NO_FOUND_DATA);
                        }
                    }
                    else
                    {
                        dataGridView1.DataSource = lists;
                    }
                    //open only therapist tab
                    tabControl1.TabPages.Remove(tabPage1);
                    tabControl1.TabPages.Add(tabPage2);
                    tabPage2.Controls.Add(panel1);
                    tabControl1.SelectTab(0);
                    //Show Data
                    status = DBStatus.Update;
                    B_SAVE.Text = "ปรับปรุง";
                    TXT_ID.Text = lists[index].ID;
                    CBO_TYPE.SelectedValue = lists[index].Type;
                    CBO_TITLE.SelectedValue = lists[index].Title;
                    TXT_NAME.Text = lists[index].Name;
                    TXT_SURNAME.Text = lists[index].Surname;
                    TXT_NICKNAME.Text = lists[index].NickName;
                    TXT_ADDR.Text = lists[index].Address;
                    TXT_TEL.Text = lists[index].Mobile;
                    TXT_EMAIL.Text = lists[index].Email;
                    CBO_PERIOD.SelectedValue = lists[index].Period;
                    CBO_WORKSTATUS.SelectedIndex = (lists[index].Status.Equals("A") ? 0 : 1);
                    TXT_BIZ.Text = lists[index].Biz.ToString();
                    TXT_HOLIDAY.Text = lists[index].Holiday.ToString();
                    TXT_SICK.Text = lists[index].Sick.ToString();
                    TXT_PASS.Text = "-";
                    TXT_PASS1.Text = "-";
                    TXT_NAME.Select();
                    TXT_ID.ReadOnly = true;
                }
                else {
                    search();
                }
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                search();
            }
        }

        private void TXT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void CheckIsNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                Management.ShowMsg(Management.NUMBER_ONLY);
            }
        }

        private void B_Search_Click(object sender, EventArgs e)
        {
            bSearch = true;
            search();
        }

        private void B_SAVE_Click(object sender, EventArgs e)
        {
            if (TXT_ID.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_ID.Focus();
            }
            else if (TXT_NAME.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_NAME.Focus();
            }
            else if (TXT_SURNAME.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_SURNAME.Focus();
            }
            else if (TXT_NICKNAME.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_NICKNAME.Focus();
            }
            else if (TXT_TEL.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_TEL.Focus();
            }
            else if (TXT_EMAIL.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_EMAIL.Focus();
            }
            else if (TXT_PASS.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_PASS.Focus();
            }
            else if (!TXT_PASS.Text.Equals(TXT_PASS1.Text))
            {
                Management.ShowMsg("รหัสผ่านไม่ตรงกัน");
                TXT_PASS.Focus();
            }
            else
            {
                switch (status)
                {
                    case DBStatus.Insert://INSERT
                        try
                        {
                            CEmployee member = new CEmployee
                            {
                                ID = TXT_ID.Text,
                                Type = CBO_TYPE.SelectedValue.ToString(),
                                Sex = (!CBO_TITLE.SelectedText.ToString().Trim().Equals("นาย")) ? "M" : "F",
                                Title = CBO_TITLE.SelectedValue.ToString(),
                                Name = TXT_NAME.Text,
                                Surname = TXT_SURNAME.Text,
                                NickName = TXT_NICKNAME.Text,
                                Address = TXT_ADDR.Text,
                                Mobile = TXT_TEL.Text,
                                Email = TXT_EMAIL.Text,
                                Period = CBO_PERIOD.SelectedValue.ToString(),
                                Status = CBO_WORKSTATUS.SelectedIndex.ToString(),
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                Biz = Convert.ToDouble(TXT_BIZ.Text),
                                Holiday = Convert.ToDouble(TXT_HOLIDAY.Text),
                                Sick = Convert.ToDouble(TXT_SICK.Text),
                                FLAG = Management.Insert//Insert
                            };
                            if (DALEmployee.manageMaster(member) > 0)
                            {
                                //Save user name
                                Console.WriteLine(DALUser.manage(
                                                    new CUSER
                                                    {
                                                        USER = TXT_ID.Text,
                                                        PASSWORD = TXT_PASS.Text,
                                                        Type = CBO_TYPE.SelectedValue.ToString(),
                                                        PERMISSION = "",
                                                        KEYUSER = Authorize.getUser(),
                                                        FLAG = "0"//Insert
                                                    }
                                                )
                                            );
                                MessageBox.Show(Management.INSERT_SUCCESS);
                                if (!main.Therapist.Equals("0000"))
                                {
                                    //ออกในกรณีที่เข้ามาด้วย therapist menu
                                    Close();
                                }
                                else
                                {
                                    initial();
                                    search();
                                }
                            }
                        }
                        catch (SqlException ex)
                        {
                            Management.ShowMsg(ex.Message);
                        }
                        break;
                    case DBStatus.Update://UPDATE
                        try
                        {
                            CEmployee member = new CEmployee
                            {
                                ID = TXT_ID.Text,
                                Type = CBO_TYPE.SelectedValue.ToString(),
                                Sex = (!CBO_TITLE.SelectedText.ToString().Trim().Equals("นาย")) ? "M" : "F",
                                Title = CBO_TITLE.SelectedValue.ToString(),
                                Name = TXT_NAME.Text,
                                Surname = TXT_SURNAME.Text,
                                NickName = TXT_NICKNAME.Text,
                                Address = TXT_ADDR.Text,
                                Mobile = TXT_TEL.Text,
                                Email = TXT_EMAIL.Text,
                                Period = CBO_PERIOD.SelectedValue.ToString(),
                                Status = CBO_WORKSTATUS.SelectedIndex.ToString(),
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                Biz = Convert.ToDouble(TXT_BIZ.Text),
                                Holiday = Convert.ToDouble(TXT_HOLIDAY.Text),
                                Sick = Convert.ToDouble(TXT_SICK.Text),
                                FLAG = Management.Update//Update
                            };
                            if (DALEmployee.manageMaster(member) > 0)
                            {
                                Console.WriteLine(DALUser.manage(
                                        new CUSER
                                        {
                                            USER = TXT_ID.Text,
                                            PASSWORD = TXT_PASS.Text,
                                            Type = CBO_TYPE.SelectedValue.ToString(),
                                            PERMISSION = "",
                                            KEYUSER = Authorize.getUser(),
                                            FLAG = "1"//Insert
                                        }
                                    )
                                );
                                MessageBox.Show(Management.UPDATE_SUCCESS);
                                if (!main.Therapist.Equals("0000"))
                                {
                                    //ออกในกรณีที่เข้ามาด้วย therapist menu
                                    Close();
                                }
                                else
                                {
                                    initial();
                                    search();
                                }
                            }
                        }
                        catch (SqlException ex)
                        {
                            Management.ShowMsg(ex.Message);
                        }
                        break;
                    case DBStatus.Delete://DELETE
                        try
                        {
                            CEmployee member = new CEmployee
                            {
                                ID = TXT_ID.Text,
                                Type = CBO_TYPE.SelectedValue.ToString(),
                                Sex = (!CBO_TITLE.SelectedText.ToString().Trim().Equals("นาย")) ? "M" : "F",
                                Title = CBO_TITLE.SelectedValue.ToString(),
                                Name = TXT_NAME.Text,
                                Surname = TXT_SURNAME.Text,
                                NickName = TXT_NICKNAME.Text,
                                Address = TXT_ADDR.Text,
                                Mobile = TXT_TEL.Text,
                                Email = TXT_EMAIL.Text,
                                Period = CBO_PERIOD.SelectedValue.ToString(),
                                Status = CBO_WORKSTATUS.SelectedIndex.ToString(),
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                Biz = Convert.ToDouble(TXT_BIZ.Text),
                                Holiday = Convert.ToDouble(TXT_HOLIDAY.Text),
                                Sick = Convert.ToDouble(TXT_SICK.Text),
                                FLAG = Management.Delete
                            };
                            if (DALEmployee.manageMaster(member) > 0)
                            {
                                MessageBox.Show(Management.DELETE_SUCCESS);
                                if (!main.Therapist.Equals("0000"))
                                {
                                    //ออกในกรณีที่เข้ามาด้วย therapist menu
                                    Close();
                                }
                                else
                                {
                                    initial();
                                    search();
                                }
                            }
                        }
                        catch (SqlException ex)
                        {
                            Management.ShowMsg(ex.Message);
                        }
                        break;
                }
            }
        }

        private void B_CANCEL_Click(object sender, EventArgs e)
        {
            initial();
            search();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
            }
            Cursor = Cursors.Default;
        }

        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.dataGridView1.HitTest(e.X, e.Y);
            if (hti.RowIndex != -1)
            {
                if (hti.Type == DataGridViewHitTestType.Cell)
                {
                    dataGridView1.ClearSelection();
                    dataGridView1.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
                    index = hti.RowIndex;
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
                bSelected = true;
                Close();
            }
            Cursor = Cursors.Default;
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.CellStyle.ForeColor = Color.Green;
            }
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (formStatus == FormStatus.Normal)
                {
                    dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "Double Click เลือกรายการที่ต้องการ \n หรือ คลิ๊กขวาเพื่อ เพิ่ม,ปรับปรุง,ลบ ข้อมูล";
                }
                else
                {
                    dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "Double Click เลือกรายการที่ต้องการ";
                }
            }
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                    initial();
                    break;
            }
        }

        private void RD_ALL_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            if (dataGridView1.Rows.Count > 0)
            {
                //Reset Old select row
                dataGridView1.Rows[index].Selected = false;
                dataGridView1.Rows[0].Selected = true;
                txtSearch.Focus();
            }
        }

        private void CMS_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem b = (ToolStripMenuItem)sender;
            tabControl1.TabPages.Add(tabPage2);
            tabPage2.Controls.Add(panel1);
            tabControl1.SelectTab(1);
            groupBox2.Enabled = false;
            RD_ALL.Checked = true;
            txtSearch.Text = "";
            switch (b.Name)
            {
                case "CMS_Insert":
                    status = DBStatus.Insert;
                    B_SAVE.Text = "เพิ่ม";
                    TXT_ID.Text = "";
                    TXT_ID.Focus();
                    break;
                case "CMS_Update":
                    status = DBStatus.Update;
                    B_SAVE.Text = "ปรับปรุง";
                    TXT_ID.Text = lists[index].ID;
                    CBO_TYPE.SelectedValue = lists[index].Type;
                    CBO_TITLE.SelectedValue = lists[index].Title;
                    TXT_NAME.Text = lists[index].Name;
                    TXT_SURNAME.Text = lists[index].Surname;
                    TXT_NICKNAME.Text = lists[index].NickName;
                    TXT_ADDR.Text = lists[index].Address;
                    TXT_TEL.Text = lists[index].Mobile;
                    TXT_EMAIL.Text = lists[index].Email;
                    CBO_PERIOD.SelectedValue = lists[index].Period;
                    CBO_WORKSTATUS.SelectedIndex = (lists[index].Status.Equals("A") ? 0 : 1);
                    TXT_BIZ.Text = lists[index].Biz.ToString();
                    TXT_HOLIDAY.Text = lists[index].Holiday.ToString();
                    TXT_SICK.Text = lists[index].Sick.ToString();
                    TXT_PASS.Text = "-";
                    TXT_PASS1.Text = "-";
                    TXT_NAME.Select();
                    TXT_ID.ReadOnly = true;
                    break;
                case "CMS_Delete":
                    status = DBStatus.Delete;
                    B_SAVE.Text = "ลบ";
                    TXT_ID.Text = lists[index].ID;
                    CBO_TYPE.SelectedValue = lists[index].Type;
                    CBO_TITLE.SelectedValue = lists[index].Title;
                    TXT_NAME.Text = lists[index].Name;
                    TXT_SURNAME.Text = lists[index].Surname;
                    TXT_NICKNAME.Text = lists[index].NickName;
                    TXT_ADDR.Text = lists[index].Address;
                    TXT_TEL.Text = lists[index].Mobile;
                    TXT_EMAIL.Text = lists[index].Email;
                    TXT_BIZ.Text = lists[index].Biz.ToString();
                    TXT_HOLIDAY.Text = lists[index].Holiday.ToString();
                    TXT_SICK.Text = lists[index].Sick.ToString();
                    CBO_PERIOD.SelectedValue = lists[index].Period;
                    CBO_WORKSTATUS.SelectedIndex = (lists[index].Status.Equals("A") ? 0 : 1);
                    TXT_PASS.Text = "-";
                    TXT_PASS1.Text = "-";
                    B_SAVE.Focus();
                    TXT_ID.ReadOnly = true;
                    TXT_NAME.ReadOnly = true;
                    break;
            }
        }

        private void B_STYPE_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            switch (b.Name)
            {
                case "B_STYPE":
                    FrmSearch search = new FrmSearch(MasterList.MTypeEmployee, FormStatus.Search);
                    search.ShowDialog();
                    if (search.bSelected)
                    {
                        CBO_TYPE.SelectedValue = search.masters[search.index].ID;
                    }
                    break;
                case "B_TITLE":
                    FrmSearch search1 = new FrmSearch(MasterList.MTitle, FormStatus.Search);
                    search1.ShowDialog();
                    if (search1.bSelected)
                    {
                        CBO_TITLE.SelectedValue = search1.masters[search1.index].ID;
                    }
                    break;
                case "B_SPERIOD":
                    FrmTimePeriod ftp = new FrmTimePeriod(FormStatus.Search);
                    ftp.ShowDialog();
                    if (ftp.bSelected)
                    {
                        CBO_PERIOD.SelectedValue = ftp.lists[ftp.index].ID;
                    }
                    break;
            }
        }

        private void search()
        {
            lists = DALEmployee.getList(new CEmployee
                                        {
                                            ID = txtSearch.Text,
                                            Type = (group.Equals("")) ? txtSearch.Text : "0003",
                                            Sex = txtSearch.Text,
                                            Title = txtSearch.Text,
                                            Name = txtSearch.Text,
                                            Surname = txtSearch.Text,
                                            NickName = txtSearch.Text,
                                            Nation = txtSearch.Text,
                                            BirthDay = DateTime.Now,
                                            Address = txtSearch.Text,
                                            Mobile = txtSearch.Text,
                                            Email = txtSearch.Text,
                                            Status = "",
                                            Period = "",
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = (group.Equals("")) ? getSearchStatus() : ((group.Equals("6")) ? "6" : "7")//Searh Flag (6 คือ ให้แสดงเฉพาะที่เป็นหมอนวด 7 คือ แสดงเฉพาะเจ้าหน้าที่)
                                        }
                                    );
            //Check Have Data!!
            if (lists == null)
            {
                dataGridView1.DataSource = null;
                if (bSearch)
                {
                    Management.ShowMsg(Management.NO_FOUND_DATA);
                }
            }
            else
            {
                dataGridView1.DataSource = lists;
                //L_TOTAL.Text = String.Format("{0:##}", lists.Count);
            }

        }

        private void initial()
        {
            groupBox2.Enabled = true;
            TXT_ID.ReadOnly = false;
            TXT_NAME.ReadOnly = false;
            TXT_ID.Text = "";
            TXT_ID.Text = "";
            TXT_NAME.Text = "";
            TXT_SURNAME.Text = "";
            TXT_NICKNAME.Text = "";
            TXT_ADDR.Text = "";
            TXT_TEL.Text = "";
            TXT_EMAIL.Text = "";
            TXT_BIZ.Text = "0";
            TXT_HOLIDAY.Text = "0";
            TXT_SICK.Text = "0";
            txtSearch.Text = "";
            txtSearch.Focus();
            tabControl1.TabPages.RemoveAt(1);
            //Customer Type
            CBO_TYPE.DataSource = DALMaster.getList(MasterList.MTypeEmployee,
                                new CMaster
                                {
                                    ID = "",
                                    DETAIL = "",
                                    KEYDATE = DateTime.Now,
                                    KEYUSER = Authorize.getUser(),
                                    LASTEDIT = DateTime.Now,
                                    FLAG = Management.SearchAll//Searh Flag
                                }
                                );
            CBO_TITLE.DataSource = DALMaster.getList(MasterList.MTitle,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );

            CBO_PERIOD.DataSource = DALTimePeriod.getList(new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                StartDate = DateTime.Now,
                                EndDate = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            CBO_WORKSTATUS.SelectedIndex = 0;//Initial Selected Index
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Set User Permission
            CMS_Insert.Enabled = (formStatus == FormStatus.Normal) ? Authorize.GetAuthorize(this.Name)[0] : false;
            CMS_Update.Enabled = ((formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[1]) && (dataGridView1.Rows.Count > 0)) ? true : false;
            CMS_Delete.Enabled = ((formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[2]) && (dataGridView1.Rows.Count > 0)) ? true : false;
        }

        //private string getIDSequence()
        //{
        //    //Reload master data
        //    RD_ALL.Checked = true;
        //    search();
        //    //Begin Gen Sequence
        //    int seq = 1;
        //    for (int i = 0; i < lists.Count; i++)
        //    {
        //        if (seq != Convert.ToInt16(lists[i].ID))
        //        {
        //            break;
        //        }
        //        else
        //        {
        //            seq++;
        //        }
        //    }
        //    return String.Format("{0:0000}", seq); 
        //}

        private string getSearchStatus()
        {
            //Search Status If
            //3:ALL
            //4:ID
            //5:NAME
            return (RD_ALL.Checked) ? Management.SearchAll : ((RD_ID.Checked) ? Management.SearchByID : Management.SearchByDetail);
        }

        private void FrmEmployee_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (main != null)
            {
                main.refreshSchedule();
            }
        }
    }
}
