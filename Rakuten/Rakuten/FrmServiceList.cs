﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Rakuten.DAL;
using Rakuten.Utility;
using Rakuten.Structure;

namespace Rakuten
{
    public partial class FrmServiceList : Form
    {
        public int index;
        public bool bSelected = false;
        public List<CPackage> list = null;
        public FrmServiceList()
        {
            InitializeComponent();
        }
        private void FrmServiceList_Load(object sender, EventArgs e)
        {
            //Massage Type
            CBO_MASSAGE_TYPE.DataSource = DALMaster.getList(MasterList.MMassageType,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            getServiceList("0001");
        }
       

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
                bSelected = true;
                Close();
            }
            Cursor = Cursors.Default;
        }


        private void getServiceList(string cusType)
        {
            //Show package detail
                list = DALPackage.getList1(new CPackage
                {
                    ID = "",
                    DETAIL = "",
                    Hour = "",
                    Price = 1,
                    MoneyUnit = "THB",
                    MassageType = CBO_MASSAGE_TYPE.SelectedValue.ToString(),
                    PerCourse = 1,
                    CustomerType = cusType,
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now,
                    KEYDATE = DateTime.Now,
                    KEYUSER = Authorize.getUser(),
                    LASTEDIT = DateTime.Now,
                    FLAG = "8"//--แสดงข้อมูล package ของสมาชิกแต่ละคน โดยแสดงเฉพาะที่ยังใช้ member ไม่หมด
                }
                );
            if (list != null && list.Count > 0)
            {
                dataGridView2.DataSource = list;
            }
            else
            {
                //Management.ShowMsg(Management.NO_FOUND_DATA);
            }

        }

        private void CBO_MASSAGE_TYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            getServiceList("0001");
        }

        private void dataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 3:
                    e.Value = Management.getDetail(MasterList.MHour, e.Value.ToString());
                    break;
            }
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }
    }
}
