﻿namespace Rakuten
{
    partial class FrmMember
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CMS_Insert = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_Update = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_MEMBER = new System.Windows.Forms.ToolStripMenuItem();
            this.CMD_PRINT = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.BSearch = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.rUNNINGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.surnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nickNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobileDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.therapistDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthDayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dETAILDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lASTEDITDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYUSERDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fLAGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cMemberBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hourDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.memberUseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perCourseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moneyUnitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MassageType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPackageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ND_AGE = new System.Windows.Forms.NumericUpDown();
            this.B_STYPE = new System.Windows.Forms.Button();
            this.CBO_TYPE = new System.Windows.Forms.ComboBox();
            this.cMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.B_SNation = new System.Windows.Forms.Button();
            this.CBO_NATION = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TXT_EMAIL = new System.Windows.Forms.TextBox();
            this.TXT_TEL = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.TXT_ADDR = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.B_SEX = new System.Windows.Forms.Button();
            this.CBO_SEX = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.TXT_SURNAME = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TXT_ID = new System.Windows.Forms.TextBox();
            this.B_CANCEL = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.TXT_NAME = new System.Windows.Forms.TextBox();
            this.B_NEXT = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.BRemoveTherapist = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B_ADDTherapist = new System.Windows.Forms.Button();
            this.B_STHERAPIST = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.CBO_THERAPIST = new System.Windows.Forms.ComboBox();
            this.cEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CBO_PACKAGE = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.B_SPACKAGE = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.CB_PRINT_2 = new System.Windows.Forms.CheckBox();
            this.CB_PRINT_1 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TXT_DISCOUNT = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.CMD_SAVE = new System.Windows.Forms.Button();
            this.TXT_TOTAL = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.TXT_RECIEVE = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.TXT_CHANGE = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.CBO_ACCOUNT_CODE = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.CBO_CASH_TYPE = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.B_CASHTYPE = new System.Windows.Forms.Button();
            this.TXT_SLIP = new System.Windows.Forms.TextBox();
            this.B_PAY_TYPE = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.CBO_BANK = new System.Windows.Forms.ComboBox();
            this.B_BANK = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CBO_TYPE1 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.CBO_PACKAGE1 = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.NUD_YEAR = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMemberBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cPackageBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ND_AGE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMasterBindingSource)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeBindingSource)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_YEAR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CMS_Insert,
            this.CMS_Update,
            this.CMS_Delete,
            this.CMS_MEMBER,
            this.CMD_PRINT});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuStrip1.Size = new System.Drawing.Size(173, 114);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // CMS_Insert
            // 
            this.CMS_Insert.Name = "CMS_Insert";
            this.CMS_Insert.Size = new System.Drawing.Size(172, 22);
            this.CMS_Insert.Text = "สมัครสมาชิก";
            this.CMS_Insert.Click += new System.EventHandler(this.CMS_Click);
            // 
            // CMS_Update
            // 
            this.CMS_Update.Name = "CMS_Update";
            this.CMS_Update.Size = new System.Drawing.Size(172, 22);
            this.CMS_Update.Text = "แก้ไขข้อมูล";
            this.CMS_Update.Click += new System.EventHandler(this.CMS_Click);
            // 
            // CMS_Delete
            // 
            this.CMS_Delete.Name = "CMS_Delete";
            this.CMS_Delete.Size = new System.Drawing.Size(172, 22);
            this.CMS_Delete.Text = "ลบข้อมูลสมาชิก";
            this.CMS_Delete.Visible = false;
            this.CMS_Delete.Click += new System.EventHandler(this.CMS_Click);
            // 
            // CMS_MEMBER
            // 
            this.CMS_MEMBER.Name = "CMS_MEMBER";
            this.CMS_MEMBER.Size = new System.Drawing.Size(172, 22);
            this.CMS_MEMBER.Text = "ซื้อเพ็คเกจ";
            this.CMS_MEMBER.Click += new System.EventHandler(this.CMS_Click);
            // 
            // CMD_PRINT
            // 
            this.CMD_PRINT.Name = "CMD_PRINT";
            this.CMD_PRINT.Size = new System.Drawing.Size(172, 22);
            this.CMD_PRINT.Text = "พิมพ์ใบเสร็จย้อนหลัง";
            this.CMD_PRINT.Visible = false;
            this.CMD_PRINT.Click += new System.EventHandler(this.CMD_PRINT_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.txtSearch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtSearch.ForeColor = System.Drawing.Color.Yellow;
            this.txtSearch.Location = new System.Drawing.Point(463, 28);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(219, 22);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.Visible = false;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // BSearch
            // 
            this.BSearch.Image = global::Rakuten.Properties.Resources.document_view;
            this.BSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BSearch.Location = new System.Drawing.Point(688, 21);
            this.BSearch.Name = "BSearch";
            this.BSearch.Size = new System.Drawing.Size(75, 35);
            this.BSearch.TabIndex = 2;
            this.BSearch.Text = "ค้นหา";
            this.BSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BSearch.UseVisualStyleBackColor = true;
            this.BSearch.Visible = false;
            this.BSearch.Click += new System.EventHandler(this.B_Search_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(19, 129);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(796, 461);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.TabStop = false;
            this.tabControl1.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(788, 435);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "แสดงรายการค้นหา";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rUNNINGDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.surnameDataGridViewTextBoxColumn,
            this.nickNameDataGridViewTextBoxColumn,
            this.ageDataGridViewTextBoxColumn,
            this.mobileDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.packageDataGridViewTextBoxColumn,
            this.therapistDataGridViewTextBoxColumn,
            this.scentDataGridViewTextBoxColumn,
            this.sexDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.nationDataGridViewTextBoxColumn,
            this.birthDayDataGridViewTextBoxColumn,
            this.addressDataGridViewTextBoxColumn,
            this.emailDataGridViewTextBoxColumn,
            this.dETAILDataGridViewTextBoxColumn,
            this.kEYDATEDataGridViewTextBoxColumn,
            this.lASTEDITDataGridViewTextBoxColumn,
            this.kEYUSERDataGridViewTextBoxColumn,
            this.fLAGDataGridViewTextBoxColumn,
            this.startDateDataGridViewTextBoxColumn,
            this.endDateDataGridViewTextBoxColumn,
            this.remarkDataGridViewTextBoxColumn});
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.DataSource = this.cMemberBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(782, 429);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.TabStop = false;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseDown);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseEnter);
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // rUNNINGDataGridViewTextBoxColumn
            // 
            this.rUNNINGDataGridViewTextBoxColumn.DataPropertyName = "RUNNING";
            this.rUNNINGDataGridViewTextBoxColumn.HeaderText = "ลำดับ";
            this.rUNNINGDataGridViewTextBoxColumn.Name = "rUNNINGDataGridViewTextBoxColumn";
            this.rUNNINGDataGridViewTextBoxColumn.ReadOnly = true;
            this.rUNNINGDataGridViewTextBoxColumn.Width = 60;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "รหัส";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "ชื่อ";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // surnameDataGridViewTextBoxColumn
            // 
            this.surnameDataGridViewTextBoxColumn.DataPropertyName = "Surname";
            this.surnameDataGridViewTextBoxColumn.HeaderText = "นามสกุล";
            this.surnameDataGridViewTextBoxColumn.Name = "surnameDataGridViewTextBoxColumn";
            this.surnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nickNameDataGridViewTextBoxColumn
            // 
            this.nickNameDataGridViewTextBoxColumn.DataPropertyName = "NickName";
            this.nickNameDataGridViewTextBoxColumn.HeaderText = "ชื่อเล่น";
            this.nickNameDataGridViewTextBoxColumn.Name = "nickNameDataGridViewTextBoxColumn";
            this.nickNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ageDataGridViewTextBoxColumn
            // 
            this.ageDataGridViewTextBoxColumn.DataPropertyName = "Age";
            this.ageDataGridViewTextBoxColumn.HeaderText = "อายุ";
            this.ageDataGridViewTextBoxColumn.Name = "ageDataGridViewTextBoxColumn";
            this.ageDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mobileDataGridViewTextBoxColumn
            // 
            this.mobileDataGridViewTextBoxColumn.DataPropertyName = "Mobile";
            this.mobileDataGridViewTextBoxColumn.HeaderText = "เบอร์ติดต่อ";
            this.mobileDataGridViewTextBoxColumn.Name = "mobileDataGridViewTextBoxColumn";
            this.mobileDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "Type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "ประเภท";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // packageDataGridViewTextBoxColumn
            // 
            this.packageDataGridViewTextBoxColumn.DataPropertyName = "Package";
            this.packageDataGridViewTextBoxColumn.HeaderText = "Package";
            this.packageDataGridViewTextBoxColumn.Name = "packageDataGridViewTextBoxColumn";
            this.packageDataGridViewTextBoxColumn.ReadOnly = true;
            this.packageDataGridViewTextBoxColumn.Visible = false;
            // 
            // therapistDataGridViewTextBoxColumn
            // 
            this.therapistDataGridViewTextBoxColumn.DataPropertyName = "Therapist";
            this.therapistDataGridViewTextBoxColumn.HeaderText = "Therapist";
            this.therapistDataGridViewTextBoxColumn.Name = "therapistDataGridViewTextBoxColumn";
            this.therapistDataGridViewTextBoxColumn.ReadOnly = true;
            this.therapistDataGridViewTextBoxColumn.Visible = false;
            // 
            // scentDataGridViewTextBoxColumn
            // 
            this.scentDataGridViewTextBoxColumn.DataPropertyName = "Scent";
            this.scentDataGridViewTextBoxColumn.HeaderText = "Scent";
            this.scentDataGridViewTextBoxColumn.Name = "scentDataGridViewTextBoxColumn";
            this.scentDataGridViewTextBoxColumn.ReadOnly = true;
            this.scentDataGridViewTextBoxColumn.Visible = false;
            // 
            // sexDataGridViewTextBoxColumn
            // 
            this.sexDataGridViewTextBoxColumn.DataPropertyName = "Sex";
            this.sexDataGridViewTextBoxColumn.HeaderText = "Sex";
            this.sexDataGridViewTextBoxColumn.Name = "sexDataGridViewTextBoxColumn";
            this.sexDataGridViewTextBoxColumn.ReadOnly = true;
            this.sexDataGridViewTextBoxColumn.Visible = false;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.ReadOnly = true;
            this.titleDataGridViewTextBoxColumn.Visible = false;
            // 
            // nationDataGridViewTextBoxColumn
            // 
            this.nationDataGridViewTextBoxColumn.DataPropertyName = "Nation";
            this.nationDataGridViewTextBoxColumn.HeaderText = "Nation";
            this.nationDataGridViewTextBoxColumn.Name = "nationDataGridViewTextBoxColumn";
            this.nationDataGridViewTextBoxColumn.ReadOnly = true;
            this.nationDataGridViewTextBoxColumn.Visible = false;
            // 
            // birthDayDataGridViewTextBoxColumn
            // 
            this.birthDayDataGridViewTextBoxColumn.DataPropertyName = "BirthDay";
            this.birthDayDataGridViewTextBoxColumn.HeaderText = "BirthDay";
            this.birthDayDataGridViewTextBoxColumn.Name = "birthDayDataGridViewTextBoxColumn";
            this.birthDayDataGridViewTextBoxColumn.ReadOnly = true;
            this.birthDayDataGridViewTextBoxColumn.Visible = false;
            // 
            // addressDataGridViewTextBoxColumn
            // 
            this.addressDataGridViewTextBoxColumn.DataPropertyName = "Address";
            this.addressDataGridViewTextBoxColumn.HeaderText = "Address";
            this.addressDataGridViewTextBoxColumn.Name = "addressDataGridViewTextBoxColumn";
            this.addressDataGridViewTextBoxColumn.ReadOnly = true;
            this.addressDataGridViewTextBoxColumn.Visible = false;
            // 
            // emailDataGridViewTextBoxColumn
            // 
            this.emailDataGridViewTextBoxColumn.DataPropertyName = "Email";
            this.emailDataGridViewTextBoxColumn.HeaderText = "Email";
            this.emailDataGridViewTextBoxColumn.Name = "emailDataGridViewTextBoxColumn";
            this.emailDataGridViewTextBoxColumn.ReadOnly = true;
            this.emailDataGridViewTextBoxColumn.Visible = false;
            // 
            // dETAILDataGridViewTextBoxColumn
            // 
            this.dETAILDataGridViewTextBoxColumn.DataPropertyName = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn.HeaderText = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn.Name = "dETAILDataGridViewTextBoxColumn";
            this.dETAILDataGridViewTextBoxColumn.ReadOnly = true;
            this.dETAILDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYDATEDataGridViewTextBoxColumn
            // 
            this.kEYDATEDataGridViewTextBoxColumn.DataPropertyName = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.HeaderText = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.Name = "kEYDATEDataGridViewTextBoxColumn";
            this.kEYDATEDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYDATEDataGridViewTextBoxColumn.Visible = false;
            // 
            // lASTEDITDataGridViewTextBoxColumn
            // 
            this.lASTEDITDataGridViewTextBoxColumn.DataPropertyName = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.HeaderText = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.Name = "lASTEDITDataGridViewTextBoxColumn";
            this.lASTEDITDataGridViewTextBoxColumn.ReadOnly = true;
            this.lASTEDITDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYUSERDataGridViewTextBoxColumn
            // 
            this.kEYUSERDataGridViewTextBoxColumn.DataPropertyName = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.HeaderText = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.Name = "kEYUSERDataGridViewTextBoxColumn";
            this.kEYUSERDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYUSERDataGridViewTextBoxColumn.Visible = false;
            // 
            // fLAGDataGridViewTextBoxColumn
            // 
            this.fLAGDataGridViewTextBoxColumn.DataPropertyName = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.HeaderText = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.Name = "fLAGDataGridViewTextBoxColumn";
            this.fLAGDataGridViewTextBoxColumn.ReadOnly = true;
            this.fLAGDataGridViewTextBoxColumn.Visible = false;
            // 
            // startDateDataGridViewTextBoxColumn
            // 
            this.startDateDataGridViewTextBoxColumn.DataPropertyName = "StartDate";
            this.startDateDataGridViewTextBoxColumn.HeaderText = "StartDate";
            this.startDateDataGridViewTextBoxColumn.Name = "startDateDataGridViewTextBoxColumn";
            this.startDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.startDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // endDateDataGridViewTextBoxColumn
            // 
            this.endDateDataGridViewTextBoxColumn.DataPropertyName = "EndDate";
            this.endDateDataGridViewTextBoxColumn.HeaderText = "EndDate";
            this.endDateDataGridViewTextBoxColumn.Name = "endDateDataGridViewTextBoxColumn";
            this.endDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.endDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // remarkDataGridViewTextBoxColumn
            // 
            this.remarkDataGridViewTextBoxColumn.DataPropertyName = "Remark";
            this.remarkDataGridViewTextBoxColumn.HeaderText = "Remark";
            this.remarkDataGridViewTextBoxColumn.Name = "remarkDataGridViewTextBoxColumn";
            this.remarkDataGridViewTextBoxColumn.ReadOnly = true;
            this.remarkDataGridViewTextBoxColumn.Visible = false;
            // 
            // cMemberBindingSource
            // 
            this.cMemberBindingSource.DataSource = typeof(Rakuten.Structure.CMember);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(788, 435);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "เพิ่ม/ลบข้อมูล";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.groupBox8);
            this.panel1.Controls.Add(this.ND_AGE);
            this.panel1.Controls.Add(this.B_STYPE);
            this.panel1.Controls.Add(this.CBO_TYPE);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.B_SNation);
            this.panel1.Controls.Add(this.CBO_NATION);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.TXT_EMAIL);
            this.panel1.Controls.Add(this.TXT_TEL);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.TXT_ADDR);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.B_SEX);
            this.panel1.Controls.Add(this.CBO_SEX);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.TXT_SURNAME);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.TXT_ID);
            this.panel1.Controls.Add(this.B_CANCEL);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.TXT_NAME);
            this.panel1.Controls.Add(this.B_NEXT);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(782, 429);
            this.panel1.TabIndex = 12;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dataGridView2);
            this.groupBox8.Location = new System.Drawing.Point(32, 231);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(724, 100);
            this.groupBox8.TabIndex = 113;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "ข้อมูล Package";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.hourDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.memberUseDataGridViewTextBoxColumn,
            this.perCourseDataGridViewTextBoxColumn,
            this.moneyUnitDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.MassageType});
            this.dataGridView2.DataSource = this.cPackageBindingSource;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(3, 16);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(718, 81);
            this.dataGridView2.TabIndex = 112;
            this.dataGridView2.TabStop = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "RUNNING";
            this.dataGridViewTextBoxColumn1.HeaderText = "ลำดับ";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 60;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn2.HeaderText = "ID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "DETAIL";
            this.dataGridViewTextBoxColumn3.HeaderText = "รายละเอียด";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 250;
            // 
            // hourDataGridViewTextBoxColumn
            // 
            this.hourDataGridViewTextBoxColumn.DataPropertyName = "Hour";
            this.hourDataGridViewTextBoxColumn.HeaderText = "ชม.";
            this.hourDataGridViewTextBoxColumn.Name = "hourDataGridViewTextBoxColumn";
            this.hourDataGridViewTextBoxColumn.ReadOnly = true;
            this.hourDataGridViewTextBoxColumn.Width = 60;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "ราคา";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            this.priceDataGridViewTextBoxColumn.Width = 60;
            // 
            // memberUseDataGridViewTextBoxColumn
            // 
            this.memberUseDataGridViewTextBoxColumn.DataPropertyName = "MemberUse";
            this.memberUseDataGridViewTextBoxColumn.HeaderText = "ใช้ไป";
            this.memberUseDataGridViewTextBoxColumn.Name = "memberUseDataGridViewTextBoxColumn";
            this.memberUseDataGridViewTextBoxColumn.ReadOnly = true;
            this.memberUseDataGridViewTextBoxColumn.Width = 60;
            // 
            // perCourseDataGridViewTextBoxColumn
            // 
            this.perCourseDataGridViewTextBoxColumn.DataPropertyName = "PerCourse";
            this.perCourseDataGridViewTextBoxColumn.HeaderText = "ทั้งหมด";
            this.perCourseDataGridViewTextBoxColumn.Name = "perCourseDataGridViewTextBoxColumn";
            this.perCourseDataGridViewTextBoxColumn.ReadOnly = true;
            this.perCourseDataGridViewTextBoxColumn.Width = 70;
            // 
            // moneyUnitDataGridViewTextBoxColumn
            // 
            this.moneyUnitDataGridViewTextBoxColumn.DataPropertyName = "MoneyUnit";
            this.moneyUnitDataGridViewTextBoxColumn.HeaderText = "MoneyUnit";
            this.moneyUnitDataGridViewTextBoxColumn.Name = "moneyUnitDataGridViewTextBoxColumn";
            this.moneyUnitDataGridViewTextBoxColumn.ReadOnly = true;
            this.moneyUnitDataGridViewTextBoxColumn.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "KEYDATE";
            this.dataGridViewTextBoxColumn4.HeaderText = "KEYDATE";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "LASTEDIT";
            this.dataGridViewTextBoxColumn5.HeaderText = "LASTEDIT";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "KEYUSER";
            this.dataGridViewTextBoxColumn6.HeaderText = "KEYUSER";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "FLAG";
            this.dataGridViewTextBoxColumn7.HeaderText = "FLAG";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "StartDate";
            this.dataGridViewTextBoxColumn8.HeaderText = "StartDate";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "EndDate";
            this.dataGridViewTextBoxColumn9.HeaderText = "EndDate";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Remark";
            this.dataGridViewTextBoxColumn10.HeaderText = "Remark";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // MassageType
            // 
            this.MassageType.DataPropertyName = "MassageType";
            this.MassageType.HeaderText = "MassageType";
            this.MassageType.Name = "MassageType";
            this.MassageType.ReadOnly = true;
            this.MassageType.Visible = false;
            // 
            // cPackageBindingSource
            // 
            this.cPackageBindingSource.DataSource = typeof(Rakuten.Structure.CPackage);
            // 
            // ND_AGE
            // 
            this.ND_AGE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ND_AGE.Location = new System.Drawing.Point(467, 80);
            this.ND_AGE.Name = "ND_AGE";
            this.ND_AGE.Size = new System.Drawing.Size(114, 26);
            this.ND_AGE.TabIndex = 5;
            this.ND_AGE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ND_AGE.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ND_AGE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // B_STYPE
            // 
            this.B_STYPE.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_STYPE.Location = new System.Drawing.Point(680, 16);
            this.B_STYPE.Name = "B_STYPE";
            this.B_STYPE.Size = new System.Drawing.Size(30, 26);
            this.B_STYPE.TabIndex = 73;
            this.B_STYPE.Text = "...";
            this.B_STYPE.UseVisualStyleBackColor = true;
            this.B_STYPE.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // CBO_TYPE
            // 
            this.CBO_TYPE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_TYPE.DataSource = this.cMasterBindingSource;
            this.CBO_TYPE.DisplayMember = "DETAIL";
            this.CBO_TYPE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_TYPE.FormattingEnabled = true;
            this.CBO_TYPE.Location = new System.Drawing.Point(467, 16);
            this.CBO_TYPE.Name = "CBO_TYPE";
            this.CBO_TYPE.Size = new System.Drawing.Size(207, 26);
            this.CBO_TYPE.TabIndex = 72;
            this.CBO_TYPE.ValueMember = "ID";
            // 
            // cMasterBindingSource
            // 
            this.cMasterBindingSource.DataSource = typeof(Rakuten.Structure.CMaster);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label24.Location = new System.Drawing.Point(389, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(72, 14);
            this.label24.TabIndex = 74;
            this.label24.Text = "ประเภทลูกค้า";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label22.Location = new System.Drawing.Point(433, 86);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 14);
            this.label22.TabIndex = 68;
            this.label22.Text = "อายุ";
            // 
            // B_SNation
            // 
            this.B_SNation.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SNation.Location = new System.Drawing.Point(323, 112);
            this.B_SNation.Name = "B_SNation";
            this.B_SNation.Size = new System.Drawing.Size(30, 26);
            this.B_SNation.TabIndex = 65;
            this.B_SNation.Text = "...";
            this.B_SNation.UseVisualStyleBackColor = true;
            this.B_SNation.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // CBO_NATION
            // 
            this.CBO_NATION.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_NATION.DataSource = this.cMasterBindingSource;
            this.CBO_NATION.DisplayMember = "DETAIL";
            this.CBO_NATION.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_NATION.FormattingEnabled = true;
            this.CBO_NATION.Location = new System.Drawing.Point(110, 112);
            this.CBO_NATION.Name = "CBO_NATION";
            this.CBO_NATION.Size = new System.Drawing.Size(207, 26);
            this.CBO_NATION.TabIndex = 6;
            this.CBO_NATION.ValueMember = "ID";
            this.CBO_NATION.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.Location = new System.Drawing.Point(61, 118);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 14);
            this.label17.TabIndex = 66;
            this.label17.Text = "เชื้อชาติ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(429, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 14);
            this.label1.TabIndex = 55;
            this.label1.Text = "อีเมล์";
            // 
            // TXT_EMAIL
            // 
            this.TXT_EMAIL.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_EMAIL.Location = new System.Drawing.Point(467, 112);
            this.TXT_EMAIL.Name = "TXT_EMAIL";
            this.TXT_EMAIL.Size = new System.Drawing.Size(207, 26);
            this.TXT_EMAIL.TabIndex = 7;
            this.TXT_EMAIL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // TXT_TEL
            // 
            this.TXT_TEL.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_TEL.Location = new System.Drawing.Point(110, 144);
            this.TXT_TEL.Name = "TXT_TEL";
            this.TXT_TEL.Size = new System.Drawing.Size(207, 26);
            this.TXT_TEL.TabIndex = 8;
            this.TXT_TEL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label23.Location = new System.Drawing.Point(73, 182);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(30, 14);
            this.label23.TabIndex = 52;
            this.label23.Text = "ที่อยู่";
            // 
            // TXT_ADDR
            // 
            this.TXT_ADDR.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_ADDR.Location = new System.Drawing.Point(110, 176);
            this.TXT_ADDR.Multiline = true;
            this.TXT_ADDR.Name = "TXT_ADDR";
            this.TXT_ADDR.Size = new System.Drawing.Size(564, 49);
            this.TXT_ADDR.TabIndex = 9;
            this.TXT_ADDR.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label21.Location = new System.Drawing.Point(56, 86);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 14);
            this.label21.TabIndex = 48;
            this.label21.Text = "นามสกุล";
            // 
            // B_SEX
            // 
            this.B_SEX.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SEX.Location = new System.Drawing.Point(323, 48);
            this.B_SEX.Name = "B_SEX";
            this.B_SEX.Size = new System.Drawing.Size(30, 26);
            this.B_SEX.TabIndex = 46;
            this.B_SEX.Text = "...";
            this.B_SEX.UseVisualStyleBackColor = true;
            this.B_SEX.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // CBO_SEX
            // 
            this.CBO_SEX.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_SEX.DataSource = this.cMasterBindingSource;
            this.CBO_SEX.DisplayMember = "DETAIL";
            this.CBO_SEX.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_SEX.FormattingEnabled = true;
            this.CBO_SEX.Location = new System.Drawing.Point(110, 48);
            this.CBO_SEX.Name = "CBO_SEX";
            this.CBO_SEX.Size = new System.Drawing.Size(207, 26);
            this.CBO_SEX.TabIndex = 2;
            this.CBO_SEX.ValueMember = "ID";
            this.CBO_SEX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label20.Location = new System.Drawing.Point(78, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(25, 14);
            this.label20.TabIndex = 47;
            this.label20.Text = "เพศ";
            // 
            // TXT_SURNAME
            // 
            this.TXT_SURNAME.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_SURNAME.Location = new System.Drawing.Point(110, 80);
            this.TXT_SURNAME.Name = "TXT_SURNAME";
            this.TXT_SURNAME.Size = new System.Drawing.Size(207, 26);
            this.TXT_SURNAME.TabIndex = 4;
            this.TXT_SURNAME.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(29, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 14);
            this.label7.TabIndex = 34;
            this.label7.Text = "เบอร์โทรศัพท์";
            // 
            // TXT_ID
            // 
            this.TXT_ID.BackColor = System.Drawing.SystemColors.MenuText;
            this.TXT_ID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_ID.ForeColor = System.Drawing.Color.Gold;
            this.TXT_ID.Location = new System.Drawing.Point(110, 19);
            this.TXT_ID.MaxLength = 25;
            this.TXT_ID.Name = "TXT_ID";
            this.TXT_ID.Size = new System.Drawing.Size(207, 23);
            this.TXT_ID.TabIndex = 1;
            this.TXT_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TXT_ID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // B_CANCEL
            // 
            this.B_CANCEL.Image = global::Rakuten.Properties.Resources.redo;
            this.B_CANCEL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.B_CANCEL.Location = new System.Drawing.Point(599, 341);
            this.B_CANCEL.Name = "B_CANCEL";
            this.B_CANCEL.Size = new System.Drawing.Size(75, 35);
            this.B_CANCEL.TabIndex = 11;
            this.B_CANCEL.Text = "ยกเลิก";
            this.B_CANCEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.B_CANCEL.UseVisualStyleBackColor = true;
            this.B_CANCEL.Click += new System.EventHandler(this.B_CANCEL_Click);
            this.B_CANCEL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(9, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Membership No.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.Location = new System.Drawing.Point(438, 54);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 14);
            this.label18.TabIndex = 12;
            this.label18.Text = "ชื่อ";
            // 
            // TXT_NAME
            // 
            this.TXT_NAME.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_NAME.Location = new System.Drawing.Point(467, 48);
            this.TXT_NAME.Name = "TXT_NAME";
            this.TXT_NAME.Size = new System.Drawing.Size(207, 26);
            this.TXT_NAME.TabIndex = 3;
            this.TXT_NAME.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // B_NEXT
            // 
            this.B_NEXT.Image = global::Rakuten.Properties.Resources.arrow_right_blue;
            this.B_NEXT.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.B_NEXT.Location = new System.Drawing.Point(518, 341);
            this.B_NEXT.Name = "B_NEXT";
            this.B_NEXT.Size = new System.Drawing.Size(75, 35);
            this.B_NEXT.TabIndex = 10;
            this.B_NEXT.Text = "ถัดไป";
            this.B_NEXT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.B_NEXT.UseVisualStyleBackColor = true;
            this.B_NEXT.Click += new System.EventHandler(this.B_NEXT_Click);
            this.B_NEXT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(788, 435);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ชำระเงิน";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox6);
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(782, 429);
            this.panel2.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox9);
            this.groupBox6.Controls.Add(this.CBO_PACKAGE);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.B_SPACKAGE);
            this.groupBox6.Location = new System.Drawing.Point(11, 15);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(498, 201);
            this.groupBox6.TabIndex = 148;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "ข้อมูล Package";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.BRemoveTherapist);
            this.groupBox9.Controls.Add(this.dataGridView3);
            this.groupBox9.Controls.Add(this.B_ADDTherapist);
            this.groupBox9.Controls.Add(this.B_STHERAPIST);
            this.groupBox9.Controls.Add(this.checkBox1);
            this.groupBox9.Controls.Add(this.CBO_THERAPIST);
            this.groupBox9.Location = new System.Drawing.Point(16, 51);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(476, 144);
            this.groupBox9.TabIndex = 137;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "รายการผู้แนะนำ";
            // 
            // BRemoveTherapist
            // 
            this.BRemoveTherapist.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.BRemoveTherapist.Location = new System.Drawing.Point(273, 83);
            this.BRemoveTherapist.Name = "BRemoveTherapist";
            this.BRemoveTherapist.Size = new System.Drawing.Size(62, 26);
            this.BRemoveTherapist.TabIndex = 139;
            this.BRemoveTherapist.TabStop = false;
            this.BRemoveTherapist.Text = "ลบ";
            this.BRemoveTherapist.UseVisualStyleBackColor = true;
            this.BRemoveTherapist.Click += new System.EventHandler(this.BRemoveTherapist_Click);
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dataGridView3.Location = new System.Drawing.Point(6, 51);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.Size = new System.Drawing.Size(261, 87);
            this.dataGridView3.TabIndex = 136;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "รหัส";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "ชื่อ";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // B_ADDTherapist
            // 
            this.B_ADDTherapist.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_ADDTherapist.Location = new System.Drawing.Point(273, 51);
            this.B_ADDTherapist.Name = "B_ADDTherapist";
            this.B_ADDTherapist.Size = new System.Drawing.Size(62, 26);
            this.B_ADDTherapist.TabIndex = 138;
            this.B_ADDTherapist.TabStop = false;
            this.B_ADDTherapist.Text = "เพิ่ม";
            this.B_ADDTherapist.UseVisualStyleBackColor = true;
            this.B_ADDTherapist.Click += new System.EventHandler(this.B_ADDTherapist_Click);
            // 
            // B_STHERAPIST
            // 
            this.B_STHERAPIST.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_STHERAPIST.Location = new System.Drawing.Point(219, 19);
            this.B_STHERAPIST.Name = "B_STHERAPIST";
            this.B_STHERAPIST.Size = new System.Drawing.Size(30, 26);
            this.B_STHERAPIST.TabIndex = 135;
            this.B_STHERAPIST.TabStop = false;
            this.B_STHERAPIST.Text = "...";
            this.B_STHERAPIST.UseVisualStyleBackColor = true;
            this.B_STHERAPIST.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(255, 23);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(87, 17);
            this.checkBox1.TabIndex = 127;
            this.checkBox1.Text = "ไม่มีผู้แนะนำ";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // CBO_THERAPIST
            // 
            this.CBO_THERAPIST.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cEmployeeBindingSource, "ID", true));
            this.CBO_THERAPIST.DataSource = this.cEmployeeBindingSource;
            this.CBO_THERAPIST.DisplayMember = "Name";
            this.CBO_THERAPIST.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_THERAPIST.FormattingEnabled = true;
            this.CBO_THERAPIST.Location = new System.Drawing.Point(6, 19);
            this.CBO_THERAPIST.Name = "CBO_THERAPIST";
            this.CBO_THERAPIST.Size = new System.Drawing.Size(207, 26);
            this.CBO_THERAPIST.TabIndex = 1;
            this.CBO_THERAPIST.TabStop = false;
            this.CBO_THERAPIST.ValueMember = "ID";
            this.CBO_THERAPIST.SelectedIndexChanged += new System.EventHandler(this.CBO_THERAPIST_SelectedIndexChanged);
            // 
            // cEmployeeBindingSource
            // 
            this.cEmployeeBindingSource.DataSource = typeof(Rakuten.Structure.CEmployee);
            // 
            // CBO_PACKAGE
            // 
            this.CBO_PACKAGE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cPackageBindingSource, "ID", true));
            this.CBO_PACKAGE.DataSource = this.cPackageBindingSource;
            this.CBO_PACKAGE.DisplayMember = "DETAIL";
            this.CBO_PACKAGE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_PACKAGE.FormattingEnabled = true;
            this.CBO_PACKAGE.Location = new System.Drawing.Point(56, 19);
            this.CBO_PACKAGE.Name = "CBO_PACKAGE";
            this.CBO_PACKAGE.Size = new System.Drawing.Size(207, 26);
            this.CBO_PACKAGE.TabIndex = 2;
            this.CBO_PACKAGE.TabStop = false;
            this.CBO_PACKAGE.ValueMember = "ID";
            this.CBO_PACKAGE.SelectedIndexChanged += new System.EventHandler(this.CBO_PACKAGE_SelectedIndexChanged);
            this.CBO_PACKAGE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label35.Location = new System.Drawing.Point(7, 25);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(43, 14);
            this.label35.TabIndex = 32;
            this.label35.Text = "เพกเก็ต";
            // 
            // B_SPACKAGE
            // 
            this.B_SPACKAGE.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SPACKAGE.Location = new System.Drawing.Point(269, 22);
            this.B_SPACKAGE.Name = "B_SPACKAGE";
            this.B_SPACKAGE.Size = new System.Drawing.Size(30, 26);
            this.B_SPACKAGE.TabIndex = 31;
            this.B_SPACKAGE.TabStop = false;
            this.B_SPACKAGE.Text = "...";
            this.B_SPACKAGE.UseVisualStyleBackColor = true;
            this.B_SPACKAGE.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.CB_PRINT_2);
            this.groupBox5.Controls.Add(this.CB_PRINT_1);
            this.groupBox5.Location = new System.Drawing.Point(11, 386);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(498, 40);
            this.groupBox5.TabIndex = 146;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Print Option";
            // 
            // CB_PRINT_2
            // 
            this.CB_PRINT_2.AutoSize = true;
            this.CB_PRINT_2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CB_PRINT_2.ForeColor = System.Drawing.Color.Maroon;
            this.CB_PRINT_2.Location = new System.Drawing.Point(105, 14);
            this.CB_PRINT_2.Name = "CB_PRINT_2";
            this.CB_PRINT_2.Size = new System.Drawing.Size(95, 18);
            this.CB_PRINT_2.TabIndex = 134;
            this.CB_PRINT_2.TabStop = false;
            this.CB_PRINT_2.Text = "พิมพ์ใบสมาชิก";
            this.CB_PRINT_2.UseVisualStyleBackColor = true;
            this.CB_PRINT_2.Visible = false;
            // 
            // CB_PRINT_1
            // 
            this.CB_PRINT_1.AutoSize = true;
            this.CB_PRINT_1.Checked = true;
            this.CB_PRINT_1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CB_PRINT_1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CB_PRINT_1.ForeColor = System.Drawing.Color.Maroon;
            this.CB_PRINT_1.Location = new System.Drawing.Point(16, 14);
            this.CB_PRINT_1.Name = "CB_PRINT_1";
            this.CB_PRINT_1.Size = new System.Drawing.Size(83, 18);
            this.CB_PRINT_1.TabIndex = 133;
            this.CB_PRINT_1.TabStop = false;
            this.CB_PRINT_1.Text = "พิมพ์ใบเสร็จ";
            this.CB_PRINT_1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TXT_DISCOUNT);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.CMD_SAVE);
            this.groupBox3.Controls.Add(this.TXT_TOTAL);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.TXT_RECIEVE);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.TXT_CHANGE);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Location = new System.Drawing.Point(515, 15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(264, 411);
            this.groupBox3.TabIndex = 147;
            this.groupBox3.TabStop = false;
            // 
            // TXT_DISCOUNT
            // 
            this.TXT_DISCOUNT.BackColor = System.Drawing.SystemColors.InfoText;
            this.TXT_DISCOUNT.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_DISCOUNT.ForeColor = System.Drawing.Color.Yellow;
            this.TXT_DISCOUNT.Location = new System.Drawing.Point(122, 63);
            this.TXT_DISCOUNT.Name = "TXT_DISCOUNT";
            this.TXT_DISCOUNT.Size = new System.Drawing.Size(125, 43);
            this.TXT_DISCOUNT.TabIndex = 2;
            this.TXT_DISCOUNT.Text = "0";
            this.TXT_DISCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_DISCOUNT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(42, 74);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 25);
            this.label15.TabIndex = 165;
            this.label15.Text = "ส่วนลด";
            // 
            // button5
            // 
            this.button5.Image = global::Rakuten.Properties.Resources.redo;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(166, 242);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(81, 40);
            this.button5.TabIndex = 6;
            this.button5.Text = "ยกเลิก";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.B_CANCEL_Click);
            // 
            // CMD_SAVE
            // 
            this.CMD_SAVE.Image = global::Rakuten.Properties.Resources.disk_blue_ok;
            this.CMD_SAVE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CMD_SAVE.Location = new System.Drawing.Point(79, 242);
            this.CMD_SAVE.Name = "CMD_SAVE";
            this.CMD_SAVE.Size = new System.Drawing.Size(81, 40);
            this.CMD_SAVE.TabIndex = 5;
            this.CMD_SAVE.Text = "บันทึก";
            this.CMD_SAVE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CMD_SAVE.UseVisualStyleBackColor = true;
            this.CMD_SAVE.Click += new System.EventHandler(this.CMD_SAVE_Click);
            // 
            // TXT_TOTAL
            // 
            this.TXT_TOTAL.BackColor = System.Drawing.SystemColors.InfoText;
            this.TXT_TOTAL.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_TOTAL.ForeColor = System.Drawing.Color.Yellow;
            this.TXT_TOTAL.Location = new System.Drawing.Point(122, 14);
            this.TXT_TOTAL.Name = "TXT_TOTAL";
            this.TXT_TOTAL.ReadOnly = true;
            this.TXT_TOTAL.Size = new System.Drawing.Size(125, 43);
            this.TXT_TOTAL.TabIndex = 1;
            this.TXT_TOTAL.Text = "0";
            this.TXT_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_TOTAL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label27.Location = new System.Drawing.Point(9, 25);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(107, 25);
            this.label27.TabIndex = 104;
            this.label27.Text = "รวมเป็นเงิน";
            // 
            // TXT_RECIEVE
            // 
            this.TXT_RECIEVE.BackColor = System.Drawing.SystemColors.InfoText;
            this.TXT_RECIEVE.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_RECIEVE.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.TXT_RECIEVE.Location = new System.Drawing.Point(122, 112);
            this.TXT_RECIEVE.Name = "TXT_RECIEVE";
            this.TXT_RECIEVE.Size = new System.Drawing.Size(125, 43);
            this.TXT_RECIEVE.TabIndex = 3;
            this.TXT_RECIEVE.Text = "0";
            this.TXT_RECIEVE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_RECIEVE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_RECIEVE_KeyDown);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label26.Location = new System.Drawing.Point(59, 123);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 25);
            this.label26.TabIndex = 122;
            this.label26.Text = "รับมา";
            // 
            // TXT_CHANGE
            // 
            this.TXT_CHANGE.BackColor = System.Drawing.SystemColors.InfoText;
            this.TXT_CHANGE.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_CHANGE.ForeColor = System.Drawing.Color.Red;
            this.TXT_CHANGE.Location = new System.Drawing.Point(122, 161);
            this.TXT_CHANGE.Name = "TXT_CHANGE";
            this.TXT_CHANGE.ReadOnly = true;
            this.TXT_CHANGE.Size = new System.Drawing.Size(125, 43);
            this.TXT_CHANGE.TabIndex = 4;
            this.TXT_CHANGE.TabStop = false;
            this.TXT_CHANGE.Text = "0";
            this.TXT_CHANGE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_CHANGE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label25.Location = new System.Drawing.Point(63, 172);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 25);
            this.label25.TabIndex = 124;
            this.label25.Text = "ทอน";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.CBO_ACCOUNT_CODE);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.CBO_CASH_TYPE);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.B_CASHTYPE);
            this.groupBox4.Controls.Add(this.TXT_SLIP);
            this.groupBox4.Controls.Add(this.B_PAY_TYPE);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.CBO_BANK);
            this.groupBox4.Controls.Add(this.B_BANK);
            this.groupBox4.Location = new System.Drawing.Point(11, 222);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(498, 154);
            this.groupBox4.TabIndex = 145;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "รูปแบบการชำระ";
            // 
            // CBO_ACCOUNT_CODE
            // 
            this.CBO_ACCOUNT_CODE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_ACCOUNT_CODE.DataSource = this.cMasterBindingSource;
            this.CBO_ACCOUNT_CODE.DisplayMember = "DETAIL";
            this.CBO_ACCOUNT_CODE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_ACCOUNT_CODE.FormattingEnabled = true;
            this.CBO_ACCOUNT_CODE.Location = new System.Drawing.Point(124, 116);
            this.CBO_ACCOUNT_CODE.Name = "CBO_ACCOUNT_CODE";
            this.CBO_ACCOUNT_CODE.Size = new System.Drawing.Size(207, 26);
            this.CBO_ACCOUNT_CODE.TabIndex = 6;
            this.CBO_ACCOUNT_CODE.TabStop = false;
            this.CBO_ACCOUNT_CODE.ValueMember = "ID";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(31, 122);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 14);
            this.label16.TabIndex = 116;
            this.label16.Text = "ประเภทการชำระ";
            // 
            // CBO_CASH_TYPE
            // 
            this.CBO_CASH_TYPE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_CASH_TYPE.DataSource = this.cMasterBindingSource;
            this.CBO_CASH_TYPE.DisplayMember = "DETAIL";
            this.CBO_CASH_TYPE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_CASH_TYPE.FormattingEnabled = true;
            this.CBO_CASH_TYPE.Location = new System.Drawing.Point(124, 20);
            this.CBO_CASH_TYPE.Name = "CBO_CASH_TYPE";
            this.CBO_CASH_TYPE.Size = new System.Drawing.Size(207, 26);
            this.CBO_CASH_TYPE.TabIndex = 3;
            this.CBO_CASH_TYPE.TabStop = false;
            this.CBO_CASH_TYPE.ValueMember = "ID";
            this.CBO_CASH_TYPE.SelectedIndexChanged += new System.EventHandler(this.CBO_CASH_TYPE_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label28.Location = new System.Drawing.Point(43, 90);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(75, 14);
            this.label28.TabIndex = 120;
            this.label28.Text = "หมายเลขสลิป";
            // 
            // B_CASHTYPE
            // 
            this.B_CASHTYPE.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_CASHTYPE.Location = new System.Drawing.Point(337, 21);
            this.B_CASHTYPE.Name = "B_CASHTYPE";
            this.B_CASHTYPE.Size = new System.Drawing.Size(30, 26);
            this.B_CASHTYPE.TabIndex = 76;
            this.B_CASHTYPE.TabStop = false;
            this.B_CASHTYPE.Text = "...";
            this.B_CASHTYPE.UseVisualStyleBackColor = true;
            this.B_CASHTYPE.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // TXT_SLIP
            // 
            this.TXT_SLIP.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_SLIP.Location = new System.Drawing.Point(124, 84);
            this.TXT_SLIP.Name = "TXT_SLIP";
            this.TXT_SLIP.Size = new System.Drawing.Size(207, 26);
            this.TXT_SLIP.TabIndex = 5;
            this.TXT_SLIP.TabStop = false;
            // 
            // B_PAY_TYPE
            // 
            this.B_PAY_TYPE.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_PAY_TYPE.Location = new System.Drawing.Point(337, 117);
            this.B_PAY_TYPE.Name = "B_PAY_TYPE";
            this.B_PAY_TYPE.Size = new System.Drawing.Size(30, 26);
            this.B_PAY_TYPE.TabIndex = 129;
            this.B_PAY_TYPE.TabStop = false;
            this.B_PAY_TYPE.Text = "...";
            this.B_PAY_TYPE.UseVisualStyleBackColor = true;
            this.B_PAY_TYPE.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label30.Location = new System.Drawing.Point(67, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 14);
            this.label30.TabIndex = 109;
            this.label30.Text = "ชำระโดย";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label29.Location = new System.Drawing.Point(74, 58);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 14);
            this.label29.TabIndex = 115;
            this.label29.Text = "ธนาคาร";
            // 
            // CBO_BANK
            // 
            this.CBO_BANK.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_BANK.DataSource = this.cMasterBindingSource;
            this.CBO_BANK.DisplayMember = "DETAIL";
            this.CBO_BANK.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_BANK.FormattingEnabled = true;
            this.CBO_BANK.Location = new System.Drawing.Point(124, 52);
            this.CBO_BANK.Name = "CBO_BANK";
            this.CBO_BANK.Size = new System.Drawing.Size(207, 26);
            this.CBO_BANK.TabIndex = 4;
            this.CBO_BANK.TabStop = false;
            this.CBO_BANK.ValueMember = "ID";
            // 
            // B_BANK
            // 
            this.B_BANK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_BANK.Location = new System.Drawing.Point(337, 53);
            this.B_BANK.Name = "B_BANK";
            this.B_BANK.Size = new System.Drawing.Size(30, 26);
            this.B_BANK.TabIndex = 92;
            this.B_BANK.TabStop = false;
            this.B_BANK.Text = "...";
            this.B_BANK.UseVisualStyleBackColor = true;
            this.B_BANK.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(56, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(401, 14);
            this.label2.TabIndex = 9;
            this.label2.Text = "การลบ เลือกทรัพย์สินที่ต้องการลบจากหน้า (แสดงรายการค้นหา) แล้ว (กดปุ่มลบ)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(56, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(195, 14);
            this.label8.TabIndex = 8;
            this.label8.Text = "การเพิ่ม พิมพ์ชื่อทรัพย์สิน แล้ว (กดเพิ่ม)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.Location = new System.Drawing.Point(24, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 14);
            this.label9.TabIndex = 7;
            this.label9.Text = "หมายเหตุ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(24, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 14);
            this.label10.TabIndex = 5;
            this.label10.Text = "ชื่อทรัพย์สิน";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(389, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "ลบ";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(94, 63);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(370, 59);
            this.textBox1.TabIndex = 4;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "XXXX";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(308, 128);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "เพิ่ม";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(56, 209);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(401, 14);
            this.label11.TabIndex = 9;
            this.label11.Text = "การลบ เลือกทรัพย์สินที่ต้องการลบจากหน้า (แสดงรายการค้นหา) แล้ว (กดปุ่มลบ)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.Location = new System.Drawing.Point(56, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(195, 14);
            this.label12.TabIndex = 8;
            this.label12.Text = "การเพิ่ม พิมพ์ชื่อทรัพย์สิน แล้ว (กดเพิ่ม)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(24, 161);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 14);
            this.label13.TabIndex = 7;
            this.label13.Text = "หมายเหตุ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.Location = new System.Drawing.Point(24, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 14);
            this.label14.TabIndex = 5;
            this.label14.Text = "ชื่อทรัพย์สิน";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(389, 128);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "ลบ";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(94, 63);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(370, 59);
            this.textBox2.TabIndex = 4;
            this.textBox2.TabStop = false;
            this.textBox2.Text = "XXXX";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(308, 128);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "เพิ่ม";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CBO_TYPE1);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(6, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(162, 47);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ประเภทลูกค้า";
            // 
            // CBO_TYPE1
            // 
            this.CBO_TYPE1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cPackageBindingSource, "ID", true));
            this.CBO_TYPE1.DataSource = this.cPackageBindingSource;
            this.CBO_TYPE1.DisplayMember = "DETAIL";
            this.CBO_TYPE1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_TYPE1.FormattingEnabled = true;
            this.CBO_TYPE1.Location = new System.Drawing.Point(6, 20);
            this.CBO_TYPE1.Name = "CBO_TYPE1";
            this.CBO_TYPE1.Size = new System.Drawing.Size(150, 21);
            this.CBO_TYPE1.TabIndex = 3;
            this.CBO_TYPE1.ValueMember = "ID";
            this.CBO_TYPE1.SelectedIndexChanged += new System.EventHandler(this.CBO_TYPE1_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox7);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.txtSearch);
            this.groupBox2.Controls.Add(this.BSearch);
            this.groupBox2.Location = new System.Drawing.Point(19, 54);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(796, 69);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "เงื่อนไขการสืบค้น";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.CBO_PACKAGE1);
            this.groupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox7.Location = new System.Drawing.Point(174, 16);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(195, 47);
            this.groupBox7.TabIndex = 23;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "ประเภท package";
            // 
            // CBO_PACKAGE1
            // 
            this.CBO_PACKAGE1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cPackageBindingSource, "ID", true));
            this.CBO_PACKAGE1.DataSource = this.cPackageBindingSource;
            this.CBO_PACKAGE1.DisplayMember = "DETAIL";
            this.CBO_PACKAGE1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_PACKAGE1.FormattingEnabled = true;
            this.CBO_PACKAGE1.Location = new System.Drawing.Point(6, 20);
            this.CBO_PACKAGE1.Name = "CBO_PACKAGE1";
            this.CBO_PACKAGE1.Size = new System.Drawing.Size(183, 21);
            this.CBO_PACKAGE1.TabIndex = 3;
            this.CBO_PACKAGE1.ValueMember = "ID";
            this.CBO_PACKAGE1.SelectedIndexChanged += new System.EventHandler(this.CBO_PACKAGE1_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label19.Location = new System.Drawing.Point(55, 53);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(87, 14);
            this.label19.TabIndex = 78;
            this.label19.Text = "ระยะเวลาสมาชิก";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(7, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 14);
            this.label5.TabIndex = 32;
            this.label5.Text = "เพกเก็ต";
            // 
            // NUD_YEAR
            // 
            this.NUD_YEAR.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.NUD_YEAR.Location = new System.Drawing.Point(149, 47);
            this.NUD_YEAR.Name = "NUD_YEAR";
            this.NUD_YEAR.Size = new System.Drawing.Size(114, 26);
            this.NUD_YEAR.TabIndex = 9;
            this.NUD_YEAR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NUD_YEAR.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.Green;
            this.label6.Location = new System.Drawing.Point(85, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 25);
            this.label6.TabIndex = 163;
            this.label6.Text = "ข้อมูลลูกค้า";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::Rakuten.Properties.Resources.product;
            this.pictureBox1.Location = new System.Drawing.Point(12, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(67, 50);
            this.pictureBox1.TabIndex = 161;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(831, 50);
            this.pictureBox2.TabIndex = 162;
            this.pictureBox2.TabStop = false;
            // 
            // FrmMember
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(831, 596);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMember";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ข้อมูลลูกค้า";
            this.Load += new System.EventHandler(this.FrmCustomer_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMemberBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cPackageBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ND_AGE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMasterBindingSource)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeBindingSource)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NUD_YEAR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BSearch;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.TextBox txtSearch;

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox TXT_NAME;
        private System.Windows.Forms.Button B_NEXT;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem CMS_Insert;
        private System.Windows.Forms.ToolStripMenuItem CMS_Update;
        private System.Windows.Forms.ToolStripMenuItem CMS_Delete;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button B_CANCEL;
        private System.Windows.Forms.TextBox TXT_ID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.BindingSource cMasterBindingSource;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TXT_SURNAME;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button B_SEX;
        private System.Windows.Forms.ComboBox CBO_SEX;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox TXT_ADDR;
        private System.Windows.Forms.TextBox TXT_TEL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TXT_EMAIL;
        private System.Windows.Forms.Button B_SNation;
        private System.Windows.Forms.ComboBox CBO_NATION;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button B_STYPE;
        private System.Windows.Forms.ComboBox CBO_TYPE;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUD_YEAR;
        private System.Windows.Forms.NumericUpDown ND_AGE;
        private System.Windows.Forms.ComboBox CBO_THERAPIST;
        private System.Windows.Forms.Button B_STHERAPIST;
        private System.Windows.Forms.ToolStripMenuItem CMS_MEMBER;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource cMemberBindingSource;
        private System.Windows.Forms.BindingSource cEmployeeBindingSource;
        private System.Windows.Forms.BindingSource cPackageBindingSource;
        private System.Windows.Forms.ComboBox CBO_PACKAGE;
        private System.Windows.Forms.Button B_SPACKAGE;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TXT_TOTAL;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox TXT_RECIEVE;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox TXT_CHANGE;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox CBO_ACCOUNT_CODE;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox CBO_CASH_TYPE;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button B_PAY_TYPE;
        private System.Windows.Forms.Button B_CASHTYPE;
        private System.Windows.Forms.TextBox TXT_SLIP;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox CBO_BANK;
        private System.Windows.Forms.Button B_BANK;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox CB_PRINT_2;
        private System.Windows.Forms.CheckBox CB_PRINT_1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button CMD_SAVE;
        private System.Windows.Forms.DataGridViewTextBoxColumn rUNNINGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn surnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nickNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobileDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn therapistDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthDayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dETAILDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lASTEDITDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYUSERDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fLAGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarkDataGridViewTextBoxColumn;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ToolStripMenuItem CMD_PRINT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox TXT_DISCOUNT;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox CBO_PACKAGE1;
        private System.Windows.Forms.ComboBox CBO_TYPE1;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn hourDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn memberUseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perCourseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn moneyUnitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn MassageType;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button B_ADDTherapist;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Button BRemoveTherapist;

    }
}