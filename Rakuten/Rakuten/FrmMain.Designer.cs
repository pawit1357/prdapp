﻿namespace Rakuten
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("เปลี่ยนรายการ", 6);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("ยกเลิกรายการ", 7);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("รวมใบเสร็จ", 10);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("ชำระเงิน", 8);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("พิมพ์ใบเสร็จย้อนหลัง", 9);
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.listView3 = new System.Windows.Forms.ListView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CMS_CHECKIN = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_CHECKOUT = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.CMS_PERIOD = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_P1 = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_P2 = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_LEAVE = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.LB_CUS_TOTAL = new System.Windows.Forms.Label();
            this.LB_SELL_TOTAL = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DTP_SERVICE_DATE = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TSM_UPDATE = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_CANCEL = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_PAY = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_PRINTINVOICE = new System.Windows.Forms.ToolStripMenuItem();
            this.listView1 = new System.Windows.Forms.ListView();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.numberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeInDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeOutDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0030DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0100DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0130DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0200DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0230DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0300DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0330DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0400DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0430DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0500DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0530DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0600DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0630DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0700DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0730DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0800DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0830DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0900DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0930DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1000DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1030DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1100DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1130DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1200DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1230DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1300DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1330DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1400DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1430DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1500DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1530DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1600DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1630DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1700DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1730DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1800DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1830DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1900DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t1930DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t2000DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t2030DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t2100DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t2130DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t2200DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t2230DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t2300DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t2330DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0000DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rUNNINGDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dETAILDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYDATEDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lASTEDITDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYUSERDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fLAGDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarkDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cScheduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rUNNINGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobileDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customertypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nickNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.therapistIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.massageTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hourDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.surnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthDayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dETAILDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lASTEDITDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYUSERDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fLAGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCustomerTransactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cScheduleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCustomerTransactionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1264, 580);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.splitContainer1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1256, 554);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ตารางพนักงาน";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer1.Size = new System.Drawing.Size(1256, 554);
            this.splitContainer1.SplitterDistance = 173;
            this.splitContainer1.TabIndex = 4;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.listView3);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.panel4);
            this.splitContainer3.Size = new System.Drawing.Size(1256, 173);
            this.splitContainer3.SplitterDistance = 1090;
            this.splitContainer3.TabIndex = 4;
            // 
            // listView3
            // 
            this.listView3.ContextMenuStrip = this.contextMenuStrip1;
            this.listView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView3.LargeImageList = this.imageList1;
            this.listView3.Location = new System.Drawing.Point(0, 0);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(1090, 173);
            this.listView3.SmallImageList = this.imageList1;
            this.listView3.TabIndex = 3;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.DoubleClick += new System.EventHandler(this.listView3_DoubleClick);
            this.listView3.Click += new System.EventHandler(this.listView3_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CMS_CHECKIN,
            this.CMS_CHECKOUT,
            this.toolStripSeparator1,
            this.CMS_PERIOD,
            this.CMS_LEAVE});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(139, 98);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // CMS_CHECKIN
            // 
            this.CMS_CHECKIN.Name = "CMS_CHECKIN";
            this.CMS_CHECKIN.Size = new System.Drawing.Size(138, 22);
            this.CMS_CHECKIN.Text = "ลงเวลาเข้า";
            this.CMS_CHECKIN.Click += new System.EventHandler(this.CMS_CHECKIN_Click);
            // 
            // CMS_CHECKOUT
            // 
            this.CMS_CHECKOUT.Name = "CMS_CHECKOUT";
            this.CMS_CHECKOUT.Size = new System.Drawing.Size(138, 22);
            this.CMS_CHECKOUT.Text = "ลงเวลาออก";
            this.CMS_CHECKOUT.Click += new System.EventHandler(this.CMS_CHECKIN_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(135, 6);
            // 
            // CMS_PERIOD
            // 
            this.CMS_PERIOD.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CMS_P1,
            this.CMS_P2});
            this.CMS_PERIOD.Name = "CMS_PERIOD";
            this.CMS_PERIOD.Size = new System.Drawing.Size(138, 22);
            this.CMS_PERIOD.Text = "กำหนดกะงาน";
            // 
            // CMS_P1
            // 
            this.CMS_P1.Name = "CMS_P1";
            this.CMS_P1.Size = new System.Drawing.Size(113, 22);
            this.CMS_P1.Text = "กะที่หนึ่ง";
            this.CMS_P1.Click += new System.EventHandler(this.CMS_CHECKIN_Click);
            // 
            // CMS_P2
            // 
            this.CMS_P2.Name = "CMS_P2";
            this.CMS_P2.Size = new System.Drawing.Size(113, 22);
            this.CMS_P2.Text = "กะที่สอง";
            this.CMS_P2.Click += new System.EventHandler(this.CMS_CHECKIN_Click);
            // 
            // CMS_LEAVE
            // 
            this.CMS_LEAVE.Name = "CMS_LEAVE";
            this.CMS_LEAVE.Size = new System.Drawing.Size(138, 22);
            this.CMS_LEAVE.Text = "ลา/ขาด";
            this.CMS_LEAVE.Click += new System.EventHandler(this.CMS_CHECKIN_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "user3.png");
            this.imageList1.Images.SetKeyName(1, "stopwatch_run.png");
            this.imageList1.Images.SetKeyName(2, "stopwatch_reset.png");
            this.imageList1.Images.SetKeyName(3, "id_card_warning.png");
            this.imageList1.Images.SetKeyName(4, "doctor.png");
            this.imageList1.Images.SetKeyName(5, "undo.png");
            this.imageList1.Images.SetKeyName(6, "note_edit.png");
            this.imageList1.Images.SetKeyName(7, "note_delete.png");
            this.imageList1.Images.SetKeyName(8, "cashier.png");
            this.imageList1.Images.SetKeyName(9, "printer2.png");
            this.imageList1.Images.SetKeyName(10, "preferences.png");
            this.imageList1.Images.SetKeyName(11, "stopwatch_stop.png");
            this.imageList1.Images.SetKeyName(12, "up1.png");
            this.imageList1.Images.SetKeyName(13, "up2.png");
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.LB_CUS_TOTAL);
            this.panel4.Controls.Add(this.LB_SELL_TOTAL);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(162, 173);
            this.panel4.TabIndex = 4;
            // 
            // LB_CUS_TOTAL
            // 
            this.LB_CUS_TOTAL.AutoSize = true;
            this.LB_CUS_TOTAL.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LB_CUS_TOTAL.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.LB_CUS_TOTAL.Location = new System.Drawing.Point(15, 48);
            this.LB_CUS_TOTAL.Name = "LB_CUS_TOTAL";
            this.LB_CUS_TOTAL.Size = new System.Drawing.Size(28, 29);
            this.LB_CUS_TOTAL.TabIndex = 3;
            this.LB_CUS_TOTAL.Text = "0";
            this.LB_CUS_TOTAL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LB_SELL_TOTAL
            // 
            this.LB_SELL_TOTAL.AutoSize = true;
            this.LB_SELL_TOTAL.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LB_SELL_TOTAL.ForeColor = System.Drawing.Color.LightGreen;
            this.LB_SELL_TOTAL.Location = new System.Drawing.Point(15, 19);
            this.LB_SELL_TOTAL.Name = "LB_SELL_TOTAL";
            this.LB_SELL_TOTAL.Size = new System.Drawing.Size(28, 29);
            this.LB_SELL_TOTAL.TabIndex = 2;
            this.LB_SELL_TOTAL.Text = "0";
            this.LB_SELL_TOTAL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.DTP_SERVICE_DATE);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(1115, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(135, 158);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "วันที่ทำรายการ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.label6.Location = new System.Drawing.Point(34, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 14);
            this.label6.TabIndex = 12;
            this.label6.Text = "ใช้บริการ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.label5.Location = new System.Drawing.Point(34, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 14);
            this.label5.TabIndex = 11;
            this.label5.Text = "จอง";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.label4.Location = new System.Drawing.Point(34, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 14);
            this.label4.TabIndex = 10;
            this.label4.Text = "ทำรายการแล้ว";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Green;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.ForeColor = System.Drawing.Color.Silver;
            this.panel3.Location = new System.Drawing.Point(7, 129);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(21, 16);
            this.panel3.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Red;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.ForeColor = System.Drawing.Color.Silver;
            this.panel2.Location = new System.Drawing.Point(7, 107);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(21, 16);
            this.panel2.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.ForeColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(7, 85);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(21, 16);
            this.panel1.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(6, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "ประเภทรายการ :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.label3.Location = new System.Drawing.Point(45, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 14);
            this.label3.TabIndex = 7;
            this.label3.Text = "ปกติ";
            // 
            // DTP_SERVICE_DATE
            // 
            this.DTP_SERVICE_DATE.CustomFormat = "dd/MM/yyyy";
            this.DTP_SERVICE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTP_SERVICE_DATE.Location = new System.Drawing.Point(37, 19);
            this.DTP_SERVICE_DATE.Name = "DTP_SERVICE_DATE";
            this.DTP_SERVICE_DATE.Size = new System.Drawing.Size(94, 20);
            this.DTP_SERVICE_DATE.TabIndex = 4;
            this.DTP_SERVICE_DATE.ValueChanged += new System.EventHandler(this.DTP_SERVICE_DATE_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "วันที่";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Info;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightSeaGreen;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.LightSeaGreen;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numberDataGridViewTextBoxColumn,
            this.timeInDataGridViewTextBoxColumn,
            this.timeOutDataGridViewTextBoxColumn,
            this.t0030DataGridViewTextBoxColumn,
            this.t0100DataGridViewTextBoxColumn,
            this.t0130DataGridViewTextBoxColumn,
            this.t0200DataGridViewTextBoxColumn,
            this.t0230DataGridViewTextBoxColumn,
            this.t0300DataGridViewTextBoxColumn,
            this.t0330DataGridViewTextBoxColumn,
            this.t0400DataGridViewTextBoxColumn,
            this.t0430DataGridViewTextBoxColumn,
            this.t0500DataGridViewTextBoxColumn,
            this.t0530DataGridViewTextBoxColumn,
            this.t0600DataGridViewTextBoxColumn,
            this.t0630DataGridViewTextBoxColumn,
            this.t0700DataGridViewTextBoxColumn,
            this.t0730DataGridViewTextBoxColumn,
            this.t0800DataGridViewTextBoxColumn,
            this.t0830DataGridViewTextBoxColumn,
            this.t0900DataGridViewTextBoxColumn,
            this.t0930DataGridViewTextBoxColumn,
            this.t1000DataGridViewTextBoxColumn,
            this.t1030DataGridViewTextBoxColumn,
            this.t1100DataGridViewTextBoxColumn,
            this.t1130DataGridViewTextBoxColumn,
            this.t1200DataGridViewTextBoxColumn,
            this.t1230DataGridViewTextBoxColumn,
            this.t1300DataGridViewTextBoxColumn,
            this.t1330DataGridViewTextBoxColumn,
            this.t1400DataGridViewTextBoxColumn,
            this.t1430DataGridViewTextBoxColumn,
            this.t1500DataGridViewTextBoxColumn,
            this.t1530DataGridViewTextBoxColumn,
            this.t1600DataGridViewTextBoxColumn,
            this.t1630DataGridViewTextBoxColumn,
            this.t1700DataGridViewTextBoxColumn,
            this.t1730DataGridViewTextBoxColumn,
            this.t1800DataGridViewTextBoxColumn,
            this.t1830DataGridViewTextBoxColumn,
            this.t1900DataGridViewTextBoxColumn,
            this.t1930DataGridViewTextBoxColumn,
            this.t2000DataGridViewTextBoxColumn,
            this.t2030DataGridViewTextBoxColumn,
            this.t2100DataGridViewTextBoxColumn,
            this.t2130DataGridViewTextBoxColumn,
            this.t2200DataGridViewTextBoxColumn,
            this.t2230DataGridViewTextBoxColumn,
            this.t2300DataGridViewTextBoxColumn,
            this.t2330DataGridViewTextBoxColumn,
            this.t0000DataGridViewTextBoxColumn,
            this.rUNNINGDataGridViewTextBoxColumn1,
            this.iDDataGridViewTextBoxColumn1,
            this.dETAILDataGridViewTextBoxColumn1,
            this.kEYDATEDataGridViewTextBoxColumn1,
            this.lASTEDITDataGridViewTextBoxColumn1,
            this.kEYUSERDataGridViewTextBoxColumn1,
            this.fLAGDataGridViewTextBoxColumn1,
            this.startDateDataGridViewTextBoxColumn1,
            this.endDateDataGridViewTextBoxColumn1,
            this.remarkDataGridViewTextBoxColumn1});
            this.dataGridView1.DataSource = this.cScheduleBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1256, 377);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.TabStop = false;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1256, 554);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "สถานะห้อง";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Info;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightSeaGreen;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.LightSeaGreen;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61,
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63});
            this.dataGridView2.DataSource = this.cScheduleBindingSource;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(1250, 548);
            this.dataGridView2.TabIndex = 4;
            this.dataGridView2.TabStop = false;
            this.dataGridView2.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1256, 554);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Transaction";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridView3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.listView1);
            this.splitContainer2.Size = new System.Drawing.Size(1256, 554);
            this.splitContainer2.SplitterDistance = 471;
            this.splitContainer2.TabIndex = 11;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToResizeColumns = false;
            this.dataGridView3.AllowUserToResizeRows = false;
            this.dataGridView3.AutoGenerateColumns = false;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rUNNINGDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.mobileDataGridViewTextBoxColumn,
            this.customertypeDataGridViewTextBoxColumn,
            this.nickNameDataGridViewTextBoxColumn,
            this.therapistIDDataGridViewTextBoxColumn,
            this.packageDataGridViewTextBoxColumn,
            this.massageTypeDataGridViewTextBoxColumn,
            this.hourDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn,
            this.timeDataGridViewTextBoxColumn,
            this.endDateDataGridViewTextBoxColumn,
            this.roomDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.sexDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.surnameDataGridViewTextBoxColumn,
            this.nationDataGridViewTextBoxColumn,
            this.birthDayDataGridViewTextBoxColumn,
            this.addressDataGridViewTextBoxColumn,
            this.emailDataGridViewTextBoxColumn,
            this.ageDataGridViewTextBoxColumn,
            this.dETAILDataGridViewTextBoxColumn,
            this.kEYDATEDataGridViewTextBoxColumn,
            this.lASTEDITDataGridViewTextBoxColumn,
            this.kEYUSERDataGridViewTextBoxColumn,
            this.fLAGDataGridViewTextBoxColumn,
            this.startDateDataGridViewTextBoxColumn,
            this.remarkDataGridViewTextBoxColumn});
            this.dataGridView3.ContextMenuStrip = this.contextMenuStrip2;
            this.dataGridView3.DataSource = this.cCustomerTransactionBindingSource;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(0, 0);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(1256, 471);
            this.dataGridView3.TabIndex = 9;
            this.dataGridView3.TabStop = false;
            this.dataGridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView3_MouseDown);
            this.dataGridView3.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView3_CellFormatting);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_UPDATE,
            this.TSM_CANCEL,
            this.TSM_PAY,
            this.TSM_PRINTINVOICE});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(170, 92);
            this.contextMenuStrip2.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip2_Opening);
            // 
            // TSM_UPDATE
            // 
            this.TSM_UPDATE.Name = "TSM_UPDATE";
            this.TSM_UPDATE.Size = new System.Drawing.Size(169, 22);
            this.TSM_UPDATE.Text = "เปลี่ยนแปลงรายการ";
            this.TSM_UPDATE.Click += new System.EventHandler(this.TSM_Click);
            // 
            // TSM_CANCEL
            // 
            this.TSM_CANCEL.Name = "TSM_CANCEL";
            this.TSM_CANCEL.Size = new System.Drawing.Size(169, 22);
            this.TSM_CANCEL.Text = "ยกเลิก";
            this.TSM_CANCEL.Click += new System.EventHandler(this.TSM_Click);
            // 
            // TSM_PAY
            // 
            this.TSM_PAY.Name = "TSM_PAY";
            this.TSM_PAY.Size = new System.Drawing.Size(169, 22);
            this.TSM_PAY.Text = "ชำระเงิน";
            this.TSM_PAY.Click += new System.EventHandler(this.TSM_Click);
            // 
            // TSM_PRINTINVOICE
            // 
            this.TSM_PRINTINVOICE.Name = "TSM_PRINTINVOICE";
            this.TSM_PRINTINVOICE.Size = new System.Drawing.Size(169, 22);
            this.TSM_PRINTINVOICE.Text = "ออกใบเสร็จย้อนหลัง";
            this.TSM_PRINTINVOICE.Click += new System.EventHandler(this.TSM_Click);
            // 
            // listView1
            // 
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5});
            this.listView1.LargeImageList = this.imageList1;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1256, 79);
            this.listView1.SmallImageList = this.imageList1;
            this.listView1.TabIndex = 10;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // numberDataGridViewTextBoxColumn
            // 
            this.numberDataGridViewTextBoxColumn.DataPropertyName = "Number";
            this.numberDataGridViewTextBoxColumn.Frozen = true;
            this.numberDataGridViewTextBoxColumn.HeaderText = "เบอร์";
            this.numberDataGridViewTextBoxColumn.Name = "numberDataGridViewTextBoxColumn";
            this.numberDataGridViewTextBoxColumn.ReadOnly = true;
            this.numberDataGridViewTextBoxColumn.Width = 40;
            // 
            // timeInDataGridViewTextBoxColumn
            // 
            this.timeInDataGridViewTextBoxColumn.DataPropertyName = "TimeIn";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle2.Format = "t";
            dataGridViewCellStyle2.NullValue = null;
            this.timeInDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.timeInDataGridViewTextBoxColumn.HeaderText = "เข้างาน";
            this.timeInDataGridViewTextBoxColumn.Name = "timeInDataGridViewTextBoxColumn";
            this.timeInDataGridViewTextBoxColumn.ReadOnly = true;
            this.timeInDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.timeInDataGridViewTextBoxColumn.Width = 55;
            // 
            // timeOutDataGridViewTextBoxColumn
            // 
            this.timeOutDataGridViewTextBoxColumn.DataPropertyName = "TimeOut";
            dataGridViewCellStyle3.Format = "t";
            dataGridViewCellStyle3.NullValue = null;
            this.timeOutDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.timeOutDataGridViewTextBoxColumn.HeaderText = "ออกงาน";
            this.timeOutDataGridViewTextBoxColumn.Name = "timeOutDataGridViewTextBoxColumn";
            this.timeOutDataGridViewTextBoxColumn.ReadOnly = true;
            this.timeOutDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.timeOutDataGridViewTextBoxColumn.Width = 55;
            // 
            // t0030DataGridViewTextBoxColumn
            // 
            this.t0030DataGridViewTextBoxColumn.DataPropertyName = "T0030";
            this.t0030DataGridViewTextBoxColumn.HeaderText = "00:30";
            this.t0030DataGridViewTextBoxColumn.Name = "t0030DataGridViewTextBoxColumn";
            this.t0030DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0030DataGridViewTextBoxColumn.Visible = false;
            this.t0030DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0100DataGridViewTextBoxColumn
            // 
            this.t0100DataGridViewTextBoxColumn.DataPropertyName = "T0100";
            this.t0100DataGridViewTextBoxColumn.HeaderText = "01:00";
            this.t0100DataGridViewTextBoxColumn.Name = "t0100DataGridViewTextBoxColumn";
            this.t0100DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0100DataGridViewTextBoxColumn.Visible = false;
            this.t0100DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0130DataGridViewTextBoxColumn
            // 
            this.t0130DataGridViewTextBoxColumn.DataPropertyName = "T0130";
            this.t0130DataGridViewTextBoxColumn.HeaderText = "01:30";
            this.t0130DataGridViewTextBoxColumn.Name = "t0130DataGridViewTextBoxColumn";
            this.t0130DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0130DataGridViewTextBoxColumn.Visible = false;
            this.t0130DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0200DataGridViewTextBoxColumn
            // 
            this.t0200DataGridViewTextBoxColumn.DataPropertyName = "T0200";
            this.t0200DataGridViewTextBoxColumn.HeaderText = "02:00";
            this.t0200DataGridViewTextBoxColumn.Name = "t0200DataGridViewTextBoxColumn";
            this.t0200DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0200DataGridViewTextBoxColumn.Visible = false;
            this.t0200DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0230DataGridViewTextBoxColumn
            // 
            this.t0230DataGridViewTextBoxColumn.DataPropertyName = "T0230";
            this.t0230DataGridViewTextBoxColumn.HeaderText = "02:30";
            this.t0230DataGridViewTextBoxColumn.Name = "t0230DataGridViewTextBoxColumn";
            this.t0230DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0230DataGridViewTextBoxColumn.Visible = false;
            this.t0230DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0300DataGridViewTextBoxColumn
            // 
            this.t0300DataGridViewTextBoxColumn.DataPropertyName = "T0300";
            this.t0300DataGridViewTextBoxColumn.HeaderText = "03:00";
            this.t0300DataGridViewTextBoxColumn.Name = "t0300DataGridViewTextBoxColumn";
            this.t0300DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0300DataGridViewTextBoxColumn.Visible = false;
            this.t0300DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0330DataGridViewTextBoxColumn
            // 
            this.t0330DataGridViewTextBoxColumn.DataPropertyName = "T0330";
            this.t0330DataGridViewTextBoxColumn.HeaderText = "03:30";
            this.t0330DataGridViewTextBoxColumn.Name = "t0330DataGridViewTextBoxColumn";
            this.t0330DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0330DataGridViewTextBoxColumn.Visible = false;
            this.t0330DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0400DataGridViewTextBoxColumn
            // 
            this.t0400DataGridViewTextBoxColumn.DataPropertyName = "T0400";
            this.t0400DataGridViewTextBoxColumn.HeaderText = "04:00";
            this.t0400DataGridViewTextBoxColumn.Name = "t0400DataGridViewTextBoxColumn";
            this.t0400DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0400DataGridViewTextBoxColumn.Visible = false;
            this.t0400DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0430DataGridViewTextBoxColumn
            // 
            this.t0430DataGridViewTextBoxColumn.DataPropertyName = "T0430";
            this.t0430DataGridViewTextBoxColumn.HeaderText = "04:30";
            this.t0430DataGridViewTextBoxColumn.Name = "t0430DataGridViewTextBoxColumn";
            this.t0430DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0430DataGridViewTextBoxColumn.Visible = false;
            this.t0430DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0500DataGridViewTextBoxColumn
            // 
            this.t0500DataGridViewTextBoxColumn.DataPropertyName = "T0500";
            this.t0500DataGridViewTextBoxColumn.HeaderText = "05:00";
            this.t0500DataGridViewTextBoxColumn.Name = "t0500DataGridViewTextBoxColumn";
            this.t0500DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0500DataGridViewTextBoxColumn.Visible = false;
            this.t0500DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0530DataGridViewTextBoxColumn
            // 
            this.t0530DataGridViewTextBoxColumn.DataPropertyName = "T0530";
            this.t0530DataGridViewTextBoxColumn.HeaderText = "05:30";
            this.t0530DataGridViewTextBoxColumn.Name = "t0530DataGridViewTextBoxColumn";
            this.t0530DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0530DataGridViewTextBoxColumn.Visible = false;
            this.t0530DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0600DataGridViewTextBoxColumn
            // 
            this.t0600DataGridViewTextBoxColumn.DataPropertyName = "T0600";
            this.t0600DataGridViewTextBoxColumn.HeaderText = "06:00";
            this.t0600DataGridViewTextBoxColumn.Name = "t0600DataGridViewTextBoxColumn";
            this.t0600DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0600DataGridViewTextBoxColumn.Visible = false;
            this.t0600DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0630DataGridViewTextBoxColumn
            // 
            this.t0630DataGridViewTextBoxColumn.DataPropertyName = "T0630";
            this.t0630DataGridViewTextBoxColumn.HeaderText = "06:30";
            this.t0630DataGridViewTextBoxColumn.Name = "t0630DataGridViewTextBoxColumn";
            this.t0630DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0630DataGridViewTextBoxColumn.Visible = false;
            this.t0630DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0700DataGridViewTextBoxColumn
            // 
            this.t0700DataGridViewTextBoxColumn.DataPropertyName = "T0700";
            this.t0700DataGridViewTextBoxColumn.HeaderText = "07:00";
            this.t0700DataGridViewTextBoxColumn.Name = "t0700DataGridViewTextBoxColumn";
            this.t0700DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0700DataGridViewTextBoxColumn.Visible = false;
            this.t0700DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0730DataGridViewTextBoxColumn
            // 
            this.t0730DataGridViewTextBoxColumn.DataPropertyName = "T0730";
            this.t0730DataGridViewTextBoxColumn.HeaderText = "07:30";
            this.t0730DataGridViewTextBoxColumn.Name = "t0730DataGridViewTextBoxColumn";
            this.t0730DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0730DataGridViewTextBoxColumn.Visible = false;
            this.t0730DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0800DataGridViewTextBoxColumn
            // 
            this.t0800DataGridViewTextBoxColumn.DataPropertyName = "T0800";
            this.t0800DataGridViewTextBoxColumn.HeaderText = "08:00";
            this.t0800DataGridViewTextBoxColumn.Name = "t0800DataGridViewTextBoxColumn";
            this.t0800DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0800DataGridViewTextBoxColumn.Visible = false;
            this.t0800DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0830DataGridViewTextBoxColumn
            // 
            this.t0830DataGridViewTextBoxColumn.DataPropertyName = "T0830";
            this.t0830DataGridViewTextBoxColumn.HeaderText = "08:30";
            this.t0830DataGridViewTextBoxColumn.Name = "t0830DataGridViewTextBoxColumn";
            this.t0830DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0830DataGridViewTextBoxColumn.Visible = false;
            this.t0830DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0900DataGridViewTextBoxColumn
            // 
            this.t0900DataGridViewTextBoxColumn.DataPropertyName = "T0900";
            this.t0900DataGridViewTextBoxColumn.HeaderText = "09:00";
            this.t0900DataGridViewTextBoxColumn.Name = "t0900DataGridViewTextBoxColumn";
            this.t0900DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0900DataGridViewTextBoxColumn.Visible = false;
            this.t0900DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0930DataGridViewTextBoxColumn
            // 
            this.t0930DataGridViewTextBoxColumn.DataPropertyName = "T0930";
            this.t0930DataGridViewTextBoxColumn.HeaderText = "09:30";
            this.t0930DataGridViewTextBoxColumn.Name = "t0930DataGridViewTextBoxColumn";
            this.t0930DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0930DataGridViewTextBoxColumn.Visible = false;
            this.t0930DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1000DataGridViewTextBoxColumn
            // 
            this.t1000DataGridViewTextBoxColumn.DataPropertyName = "T1000";
            this.t1000DataGridViewTextBoxColumn.HeaderText = "10:00";
            this.t1000DataGridViewTextBoxColumn.Name = "t1000DataGridViewTextBoxColumn";
            this.t1000DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1000DataGridViewTextBoxColumn.Visible = false;
            this.t1000DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1030DataGridViewTextBoxColumn
            // 
            this.t1030DataGridViewTextBoxColumn.DataPropertyName = "T1030";
            this.t1030DataGridViewTextBoxColumn.HeaderText = "10:30";
            this.t1030DataGridViewTextBoxColumn.Name = "t1030DataGridViewTextBoxColumn";
            this.t1030DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1030DataGridViewTextBoxColumn.Visible = false;
            this.t1030DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1100DataGridViewTextBoxColumn
            // 
            this.t1100DataGridViewTextBoxColumn.DataPropertyName = "T1100";
            this.t1100DataGridViewTextBoxColumn.HeaderText = "11:00";
            this.t1100DataGridViewTextBoxColumn.Name = "t1100DataGridViewTextBoxColumn";
            this.t1100DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1100DataGridViewTextBoxColumn.Visible = false;
            this.t1100DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1130DataGridViewTextBoxColumn
            // 
            this.t1130DataGridViewTextBoxColumn.DataPropertyName = "T1130";
            this.t1130DataGridViewTextBoxColumn.HeaderText = "11:30";
            this.t1130DataGridViewTextBoxColumn.Name = "t1130DataGridViewTextBoxColumn";
            this.t1130DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1130DataGridViewTextBoxColumn.Visible = false;
            this.t1130DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1200DataGridViewTextBoxColumn
            // 
            this.t1200DataGridViewTextBoxColumn.DataPropertyName = "T1200";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.t1200DataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.t1200DataGridViewTextBoxColumn.HeaderText = "12:00";
            this.t1200DataGridViewTextBoxColumn.Name = "t1200DataGridViewTextBoxColumn";
            this.t1200DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1200DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1230DataGridViewTextBoxColumn
            // 
            this.t1230DataGridViewTextBoxColumn.DataPropertyName = "T1230";
            this.t1230DataGridViewTextBoxColumn.HeaderText = "12:30";
            this.t1230DataGridViewTextBoxColumn.Name = "t1230DataGridViewTextBoxColumn";
            this.t1230DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1230DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1300DataGridViewTextBoxColumn
            // 
            this.t1300DataGridViewTextBoxColumn.DataPropertyName = "T1300";
            this.t1300DataGridViewTextBoxColumn.HeaderText = "13:00";
            this.t1300DataGridViewTextBoxColumn.Name = "t1300DataGridViewTextBoxColumn";
            this.t1300DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1300DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1330DataGridViewTextBoxColumn
            // 
            this.t1330DataGridViewTextBoxColumn.DataPropertyName = "T1330";
            this.t1330DataGridViewTextBoxColumn.HeaderText = "13:30";
            this.t1330DataGridViewTextBoxColumn.Name = "t1330DataGridViewTextBoxColumn";
            this.t1330DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1330DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1400DataGridViewTextBoxColumn
            // 
            this.t1400DataGridViewTextBoxColumn.DataPropertyName = "T1400";
            this.t1400DataGridViewTextBoxColumn.HeaderText = "14:00";
            this.t1400DataGridViewTextBoxColumn.Name = "t1400DataGridViewTextBoxColumn";
            this.t1400DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1400DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1430DataGridViewTextBoxColumn
            // 
            this.t1430DataGridViewTextBoxColumn.DataPropertyName = "T1430";
            this.t1430DataGridViewTextBoxColumn.HeaderText = "14:30";
            this.t1430DataGridViewTextBoxColumn.Name = "t1430DataGridViewTextBoxColumn";
            this.t1430DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1430DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1500DataGridViewTextBoxColumn
            // 
            this.t1500DataGridViewTextBoxColumn.DataPropertyName = "T1500";
            this.t1500DataGridViewTextBoxColumn.HeaderText = "15:00";
            this.t1500DataGridViewTextBoxColumn.Name = "t1500DataGridViewTextBoxColumn";
            this.t1500DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1500DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1530DataGridViewTextBoxColumn
            // 
            this.t1530DataGridViewTextBoxColumn.DataPropertyName = "T1530";
            this.t1530DataGridViewTextBoxColumn.HeaderText = "15:30";
            this.t1530DataGridViewTextBoxColumn.Name = "t1530DataGridViewTextBoxColumn";
            this.t1530DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1530DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1600DataGridViewTextBoxColumn
            // 
            this.t1600DataGridViewTextBoxColumn.DataPropertyName = "T1600";
            this.t1600DataGridViewTextBoxColumn.HeaderText = "16:00";
            this.t1600DataGridViewTextBoxColumn.Name = "t1600DataGridViewTextBoxColumn";
            this.t1600DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1600DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1630DataGridViewTextBoxColumn
            // 
            this.t1630DataGridViewTextBoxColumn.DataPropertyName = "T1630";
            this.t1630DataGridViewTextBoxColumn.HeaderText = "16:30";
            this.t1630DataGridViewTextBoxColumn.Name = "t1630DataGridViewTextBoxColumn";
            this.t1630DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1630DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1700DataGridViewTextBoxColumn
            // 
            this.t1700DataGridViewTextBoxColumn.DataPropertyName = "T1700";
            this.t1700DataGridViewTextBoxColumn.HeaderText = "17:00";
            this.t1700DataGridViewTextBoxColumn.Name = "t1700DataGridViewTextBoxColumn";
            this.t1700DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1700DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1730DataGridViewTextBoxColumn
            // 
            this.t1730DataGridViewTextBoxColumn.DataPropertyName = "T1730";
            this.t1730DataGridViewTextBoxColumn.HeaderText = "17:30";
            this.t1730DataGridViewTextBoxColumn.Name = "t1730DataGridViewTextBoxColumn";
            this.t1730DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1730DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1800DataGridViewTextBoxColumn
            // 
            this.t1800DataGridViewTextBoxColumn.DataPropertyName = "T1800";
            this.t1800DataGridViewTextBoxColumn.HeaderText = "18:00";
            this.t1800DataGridViewTextBoxColumn.Name = "t1800DataGridViewTextBoxColumn";
            this.t1800DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1800DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1830DataGridViewTextBoxColumn
            // 
            this.t1830DataGridViewTextBoxColumn.DataPropertyName = "T1830";
            this.t1830DataGridViewTextBoxColumn.HeaderText = "18:30";
            this.t1830DataGridViewTextBoxColumn.Name = "t1830DataGridViewTextBoxColumn";
            this.t1830DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1830DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1900DataGridViewTextBoxColumn
            // 
            this.t1900DataGridViewTextBoxColumn.DataPropertyName = "T1900";
            this.t1900DataGridViewTextBoxColumn.HeaderText = "19:00";
            this.t1900DataGridViewTextBoxColumn.Name = "t1900DataGridViewTextBoxColumn";
            this.t1900DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1900DataGridViewTextBoxColumn.Width = 36;
            // 
            // t1930DataGridViewTextBoxColumn
            // 
            this.t1930DataGridViewTextBoxColumn.DataPropertyName = "T1930";
            this.t1930DataGridViewTextBoxColumn.HeaderText = "19:30";
            this.t1930DataGridViewTextBoxColumn.Name = "t1930DataGridViewTextBoxColumn";
            this.t1930DataGridViewTextBoxColumn.ReadOnly = true;
            this.t1930DataGridViewTextBoxColumn.Width = 36;
            // 
            // t2000DataGridViewTextBoxColumn
            // 
            this.t2000DataGridViewTextBoxColumn.DataPropertyName = "T2000";
            this.t2000DataGridViewTextBoxColumn.HeaderText = "20:00";
            this.t2000DataGridViewTextBoxColumn.Name = "t2000DataGridViewTextBoxColumn";
            this.t2000DataGridViewTextBoxColumn.ReadOnly = true;
            this.t2000DataGridViewTextBoxColumn.Width = 36;
            // 
            // t2030DataGridViewTextBoxColumn
            // 
            this.t2030DataGridViewTextBoxColumn.DataPropertyName = "T2030";
            this.t2030DataGridViewTextBoxColumn.HeaderText = "20:30";
            this.t2030DataGridViewTextBoxColumn.Name = "t2030DataGridViewTextBoxColumn";
            this.t2030DataGridViewTextBoxColumn.ReadOnly = true;
            this.t2030DataGridViewTextBoxColumn.Width = 36;
            // 
            // t2100DataGridViewTextBoxColumn
            // 
            this.t2100DataGridViewTextBoxColumn.DataPropertyName = "T2100";
            this.t2100DataGridViewTextBoxColumn.HeaderText = "21:00";
            this.t2100DataGridViewTextBoxColumn.Name = "t2100DataGridViewTextBoxColumn";
            this.t2100DataGridViewTextBoxColumn.ReadOnly = true;
            this.t2100DataGridViewTextBoxColumn.Width = 36;
            // 
            // t2130DataGridViewTextBoxColumn
            // 
            this.t2130DataGridViewTextBoxColumn.DataPropertyName = "T2130";
            this.t2130DataGridViewTextBoxColumn.HeaderText = "21:30";
            this.t2130DataGridViewTextBoxColumn.Name = "t2130DataGridViewTextBoxColumn";
            this.t2130DataGridViewTextBoxColumn.ReadOnly = true;
            this.t2130DataGridViewTextBoxColumn.Width = 36;
            // 
            // t2200DataGridViewTextBoxColumn
            // 
            this.t2200DataGridViewTextBoxColumn.DataPropertyName = "T2200";
            this.t2200DataGridViewTextBoxColumn.HeaderText = "22:00";
            this.t2200DataGridViewTextBoxColumn.Name = "t2200DataGridViewTextBoxColumn";
            this.t2200DataGridViewTextBoxColumn.ReadOnly = true;
            this.t2200DataGridViewTextBoxColumn.Width = 36;
            // 
            // t2230DataGridViewTextBoxColumn
            // 
            this.t2230DataGridViewTextBoxColumn.DataPropertyName = "T2230";
            this.t2230DataGridViewTextBoxColumn.HeaderText = "22:30";
            this.t2230DataGridViewTextBoxColumn.Name = "t2230DataGridViewTextBoxColumn";
            this.t2230DataGridViewTextBoxColumn.ReadOnly = true;
            this.t2230DataGridViewTextBoxColumn.Width = 36;
            // 
            // t2300DataGridViewTextBoxColumn
            // 
            this.t2300DataGridViewTextBoxColumn.DataPropertyName = "T2300";
            this.t2300DataGridViewTextBoxColumn.HeaderText = "23:00";
            this.t2300DataGridViewTextBoxColumn.Name = "t2300DataGridViewTextBoxColumn";
            this.t2300DataGridViewTextBoxColumn.ReadOnly = true;
            this.t2300DataGridViewTextBoxColumn.Width = 36;
            // 
            // t2330DataGridViewTextBoxColumn
            // 
            this.t2330DataGridViewTextBoxColumn.DataPropertyName = "T2330";
            this.t2330DataGridViewTextBoxColumn.HeaderText = "23:30";
            this.t2330DataGridViewTextBoxColumn.Name = "t2330DataGridViewTextBoxColumn";
            this.t2330DataGridViewTextBoxColumn.ReadOnly = true;
            this.t2330DataGridViewTextBoxColumn.Width = 36;
            // 
            // t0000DataGridViewTextBoxColumn
            // 
            this.t0000DataGridViewTextBoxColumn.DataPropertyName = "T0000";
            this.t0000DataGridViewTextBoxColumn.HeaderText = "00:00";
            this.t0000DataGridViewTextBoxColumn.Name = "t0000DataGridViewTextBoxColumn";
            this.t0000DataGridViewTextBoxColumn.ReadOnly = true;
            this.t0000DataGridViewTextBoxColumn.Width = 36;
            // 
            // rUNNINGDataGridViewTextBoxColumn1
            // 
            this.rUNNINGDataGridViewTextBoxColumn1.DataPropertyName = "RUNNING";
            this.rUNNINGDataGridViewTextBoxColumn1.HeaderText = "รอบ";
            this.rUNNINGDataGridViewTextBoxColumn1.Name = "rUNNINGDataGridViewTextBoxColumn1";
            this.rUNNINGDataGridViewTextBoxColumn1.ReadOnly = true;
            this.rUNNINGDataGridViewTextBoxColumn1.Width = 30;
            // 
            // iDDataGridViewTextBoxColumn1
            // 
            this.iDDataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn1.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn1.Name = "iDDataGridViewTextBoxColumn1";
            this.iDDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn1.Visible = false;
            // 
            // dETAILDataGridViewTextBoxColumn1
            // 
            this.dETAILDataGridViewTextBoxColumn1.DataPropertyName = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn1.HeaderText = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn1.Name = "dETAILDataGridViewTextBoxColumn1";
            this.dETAILDataGridViewTextBoxColumn1.ReadOnly = true;
            this.dETAILDataGridViewTextBoxColumn1.Visible = false;
            // 
            // kEYDATEDataGridViewTextBoxColumn1
            // 
            this.kEYDATEDataGridViewTextBoxColumn1.DataPropertyName = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn1.HeaderText = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn1.Name = "kEYDATEDataGridViewTextBoxColumn1";
            this.kEYDATEDataGridViewTextBoxColumn1.ReadOnly = true;
            this.kEYDATEDataGridViewTextBoxColumn1.Visible = false;
            // 
            // lASTEDITDataGridViewTextBoxColumn1
            // 
            this.lASTEDITDataGridViewTextBoxColumn1.DataPropertyName = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn1.HeaderText = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn1.Name = "lASTEDITDataGridViewTextBoxColumn1";
            this.lASTEDITDataGridViewTextBoxColumn1.ReadOnly = true;
            this.lASTEDITDataGridViewTextBoxColumn1.Visible = false;
            // 
            // kEYUSERDataGridViewTextBoxColumn1
            // 
            this.kEYUSERDataGridViewTextBoxColumn1.DataPropertyName = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn1.HeaderText = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn1.Name = "kEYUSERDataGridViewTextBoxColumn1";
            this.kEYUSERDataGridViewTextBoxColumn1.ReadOnly = true;
            this.kEYUSERDataGridViewTextBoxColumn1.Visible = false;
            // 
            // fLAGDataGridViewTextBoxColumn1
            // 
            this.fLAGDataGridViewTextBoxColumn1.DataPropertyName = "FLAG";
            this.fLAGDataGridViewTextBoxColumn1.HeaderText = "FLAG";
            this.fLAGDataGridViewTextBoxColumn1.Name = "fLAGDataGridViewTextBoxColumn1";
            this.fLAGDataGridViewTextBoxColumn1.ReadOnly = true;
            this.fLAGDataGridViewTextBoxColumn1.Visible = false;
            // 
            // startDateDataGridViewTextBoxColumn1
            // 
            this.startDateDataGridViewTextBoxColumn1.DataPropertyName = "StartDate";
            this.startDateDataGridViewTextBoxColumn1.HeaderText = "StartDate";
            this.startDateDataGridViewTextBoxColumn1.Name = "startDateDataGridViewTextBoxColumn1";
            this.startDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.startDateDataGridViewTextBoxColumn1.Visible = false;
            // 
            // endDateDataGridViewTextBoxColumn1
            // 
            this.endDateDataGridViewTextBoxColumn1.DataPropertyName = "EndDate";
            this.endDateDataGridViewTextBoxColumn1.HeaderText = "EndDate";
            this.endDateDataGridViewTextBoxColumn1.Name = "endDateDataGridViewTextBoxColumn1";
            this.endDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.endDateDataGridViewTextBoxColumn1.Visible = false;
            // 
            // remarkDataGridViewTextBoxColumn1
            // 
            this.remarkDataGridViewTextBoxColumn1.DataPropertyName = "Remark";
            this.remarkDataGridViewTextBoxColumn1.HeaderText = "Q";
            this.remarkDataGridViewTextBoxColumn1.Name = "remarkDataGridViewTextBoxColumn1";
            this.remarkDataGridViewTextBoxColumn1.ReadOnly = true;
            this.remarkDataGridViewTextBoxColumn1.Width = 30;
            // 
            // cScheduleBindingSource
            // 
            this.cScheduleBindingSource.DataSource = typeof(Rakuten.Structure.CSchedule);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Number";
            this.dataGridViewTextBoxColumn1.HeaderText = "ห้อง";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 60;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "TimeIn";
            dataGridViewCellStyle7.Format = "t";
            dataGridViewCellStyle7.NullValue = null;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn2.HeaderText = "เข้างาน";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            this.dataGridViewTextBoxColumn2.Width = 65;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "TimeOut";
            dataGridViewCellStyle8.Format = "t";
            dataGridViewCellStyle8.NullValue = null;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn3.HeaderText = "ออกงาน";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            this.dataGridViewTextBoxColumn3.Width = 70;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "T0030";
            this.dataGridViewTextBoxColumn4.HeaderText = "00:30";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            this.dataGridViewTextBoxColumn4.Width = 36;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "T0100";
            this.dataGridViewTextBoxColumn5.HeaderText = "01:00";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            this.dataGridViewTextBoxColumn5.Width = 36;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "T0130";
            this.dataGridViewTextBoxColumn6.HeaderText = "01:30";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            this.dataGridViewTextBoxColumn6.Width = 36;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "T0200";
            this.dataGridViewTextBoxColumn7.HeaderText = "02:00";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Visible = false;
            this.dataGridViewTextBoxColumn7.Width = 36;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "T0230";
            this.dataGridViewTextBoxColumn8.HeaderText = "02:30";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            this.dataGridViewTextBoxColumn8.Width = 36;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "T0300";
            this.dataGridViewTextBoxColumn9.HeaderText = "03:00";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            this.dataGridViewTextBoxColumn9.Width = 36;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "T0330";
            this.dataGridViewTextBoxColumn10.HeaderText = "03:30";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            this.dataGridViewTextBoxColumn10.Width = 36;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "T0400";
            this.dataGridViewTextBoxColumn11.HeaderText = "04:00";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Visible = false;
            this.dataGridViewTextBoxColumn11.Width = 36;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "T0430";
            this.dataGridViewTextBoxColumn12.HeaderText = "04:30";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Visible = false;
            this.dataGridViewTextBoxColumn12.Width = 36;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "T0500";
            this.dataGridViewTextBoxColumn13.HeaderText = "05:00";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Visible = false;
            this.dataGridViewTextBoxColumn13.Width = 36;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "T0530";
            this.dataGridViewTextBoxColumn14.HeaderText = "05:30";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Visible = false;
            this.dataGridViewTextBoxColumn14.Width = 36;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "T0600";
            this.dataGridViewTextBoxColumn15.HeaderText = "06:00";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Visible = false;
            this.dataGridViewTextBoxColumn15.Width = 36;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "T0630";
            this.dataGridViewTextBoxColumn16.HeaderText = "06:30";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Visible = false;
            this.dataGridViewTextBoxColumn16.Width = 36;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "T0700";
            this.dataGridViewTextBoxColumn17.HeaderText = "07:00";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Visible = false;
            this.dataGridViewTextBoxColumn17.Width = 36;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "T0730";
            this.dataGridViewTextBoxColumn18.HeaderText = "07:30";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Visible = false;
            this.dataGridViewTextBoxColumn18.Width = 36;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "T0800";
            this.dataGridViewTextBoxColumn19.HeaderText = "08:00";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Visible = false;
            this.dataGridViewTextBoxColumn19.Width = 36;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "T0830";
            this.dataGridViewTextBoxColumn20.HeaderText = "08:30";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Visible = false;
            this.dataGridViewTextBoxColumn20.Width = 36;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "T0900";
            this.dataGridViewTextBoxColumn21.HeaderText = "09:00";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Visible = false;
            this.dataGridViewTextBoxColumn21.Width = 36;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "T0930";
            this.dataGridViewTextBoxColumn22.HeaderText = "09:30";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Visible = false;
            this.dataGridViewTextBoxColumn22.Width = 36;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "T1000";
            this.dataGridViewTextBoxColumn23.HeaderText = "10:00";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Visible = false;
            this.dataGridViewTextBoxColumn23.Width = 36;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "T1030";
            this.dataGridViewTextBoxColumn24.HeaderText = "10:30";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Visible = false;
            this.dataGridViewTextBoxColumn24.Width = 36;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "T1100";
            this.dataGridViewTextBoxColumn25.HeaderText = "11:00";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Visible = false;
            this.dataGridViewTextBoxColumn25.Width = 36;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "T1130";
            this.dataGridViewTextBoxColumn26.HeaderText = "11:30";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Visible = false;
            this.dataGridViewTextBoxColumn26.Width = 36;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "T1200";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn28.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn28.HeaderText = "12:00";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Width = 36;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "T1230";
            this.dataGridViewTextBoxColumn29.HeaderText = "12:30";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Width = 36;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "T1300";
            this.dataGridViewTextBoxColumn30.HeaderText = "13:00";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Width = 36;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "T1330";
            this.dataGridViewTextBoxColumn31.HeaderText = "13:30";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Width = 36;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "T1400";
            this.dataGridViewTextBoxColumn32.HeaderText = "14:00";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Width = 36;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "T1430";
            this.dataGridViewTextBoxColumn33.HeaderText = "14:30";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Width = 36;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "T1500";
            this.dataGridViewTextBoxColumn34.HeaderText = "15:00";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            this.dataGridViewTextBoxColumn34.Width = 36;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "T1530";
            this.dataGridViewTextBoxColumn35.HeaderText = "15:30";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            this.dataGridViewTextBoxColumn35.Width = 36;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "T1600";
            this.dataGridViewTextBoxColumn36.HeaderText = "16:00";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.Width = 36;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "T1630";
            this.dataGridViewTextBoxColumn37.HeaderText = "16:30";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            this.dataGridViewTextBoxColumn37.Width = 36;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "T1700";
            this.dataGridViewTextBoxColumn38.HeaderText = "17:00";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Width = 36;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "T1730";
            this.dataGridViewTextBoxColumn39.HeaderText = "17:30";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Width = 36;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.DataPropertyName = "T1800";
            this.dataGridViewTextBoxColumn40.HeaderText = "18:00";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            this.dataGridViewTextBoxColumn40.Width = 36;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.DataPropertyName = "T1830";
            this.dataGridViewTextBoxColumn41.HeaderText = "18:30";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.ReadOnly = true;
            this.dataGridViewTextBoxColumn41.Width = 36;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "T1900";
            this.dataGridViewTextBoxColumn42.HeaderText = "19:00";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            this.dataGridViewTextBoxColumn42.Width = 36;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.DataPropertyName = "T1930";
            this.dataGridViewTextBoxColumn43.HeaderText = "19:30";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Width = 36;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.DataPropertyName = "T2000";
            this.dataGridViewTextBoxColumn44.HeaderText = "20:00";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            this.dataGridViewTextBoxColumn44.Width = 36;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.DataPropertyName = "T2030";
            this.dataGridViewTextBoxColumn45.HeaderText = "20:30";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.ReadOnly = true;
            this.dataGridViewTextBoxColumn45.Width = 36;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.DataPropertyName = "T2100";
            this.dataGridViewTextBoxColumn46.HeaderText = "21:00";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            this.dataGridViewTextBoxColumn46.Width = 36;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.DataPropertyName = "T2130";
            this.dataGridViewTextBoxColumn47.HeaderText = "21:30";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.ReadOnly = true;
            this.dataGridViewTextBoxColumn47.Width = 36;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.DataPropertyName = "T2200";
            this.dataGridViewTextBoxColumn48.HeaderText = "22:00";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            this.dataGridViewTextBoxColumn48.Width = 36;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.DataPropertyName = "T2230";
            this.dataGridViewTextBoxColumn49.HeaderText = "22:30";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            this.dataGridViewTextBoxColumn49.Width = 36;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.DataPropertyName = "T2300";
            this.dataGridViewTextBoxColumn50.HeaderText = "23:00";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.ReadOnly = true;
            this.dataGridViewTextBoxColumn50.Width = 36;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.DataPropertyName = "T2330";
            this.dataGridViewTextBoxColumn51.HeaderText = "23:30";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            this.dataGridViewTextBoxColumn51.Width = 36;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.DataPropertyName = "T0000";
            this.dataGridViewTextBoxColumn52.HeaderText = "00:00";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            this.dataGridViewTextBoxColumn52.Width = 36;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.DataPropertyName = "RUNNING";
            this.dataGridViewTextBoxColumn53.HeaderText = "RUNNING";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.ReadOnly = true;
            this.dataGridViewTextBoxColumn53.Visible = false;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn54.HeaderText = "ID";
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.ReadOnly = true;
            this.dataGridViewTextBoxColumn54.Visible = false;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.DataPropertyName = "DETAIL";
            this.dataGridViewTextBoxColumn56.HeaderText = "DETAIL";
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.ReadOnly = true;
            this.dataGridViewTextBoxColumn56.Visible = false;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.DataPropertyName = "KEYDATE";
            this.dataGridViewTextBoxColumn57.HeaderText = "KEYDATE";
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.ReadOnly = true;
            this.dataGridViewTextBoxColumn57.Visible = false;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.DataPropertyName = "LASTEDIT";
            this.dataGridViewTextBoxColumn58.HeaderText = "LASTEDIT";
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.ReadOnly = true;
            this.dataGridViewTextBoxColumn58.Visible = false;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.DataPropertyName = "KEYUSER";
            this.dataGridViewTextBoxColumn59.HeaderText = "KEYUSER";
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.ReadOnly = true;
            this.dataGridViewTextBoxColumn59.Visible = false;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.DataPropertyName = "FLAG";
            this.dataGridViewTextBoxColumn60.HeaderText = "FLAG";
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.ReadOnly = true;
            this.dataGridViewTextBoxColumn60.Visible = false;
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.DataPropertyName = "StartDate";
            this.dataGridViewTextBoxColumn61.HeaderText = "StartDate";
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.ReadOnly = true;
            this.dataGridViewTextBoxColumn61.Visible = false;
            // 
            // dataGridViewTextBoxColumn62
            // 
            this.dataGridViewTextBoxColumn62.DataPropertyName = "EndDate";
            this.dataGridViewTextBoxColumn62.HeaderText = "EndDate";
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            this.dataGridViewTextBoxColumn62.ReadOnly = true;
            this.dataGridViewTextBoxColumn62.Visible = false;
            // 
            // dataGridViewTextBoxColumn63
            // 
            this.dataGridViewTextBoxColumn63.DataPropertyName = "Remark";
            this.dataGridViewTextBoxColumn63.HeaderText = "Remark";
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            this.dataGridViewTextBoxColumn63.ReadOnly = true;
            this.dataGridViewTextBoxColumn63.Visible = false;
            // 
            // rUNNINGDataGridViewTextBoxColumn
            // 
            this.rUNNINGDataGridViewTextBoxColumn.DataPropertyName = "RUNNING";
            this.rUNNINGDataGridViewTextBoxColumn.HeaderText = "ลำดับ";
            this.rUNNINGDataGridViewTextBoxColumn.Name = "rUNNINGDataGridViewTextBoxColumn";
            this.rUNNINGDataGridViewTextBoxColumn.ReadOnly = true;
            this.rUNNINGDataGridViewTextBoxColumn.Width = 60;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "รหัส";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            this.iDDataGridViewTextBoxColumn.Width = 80;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "ชื่อลูกค้า";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mobileDataGridViewTextBoxColumn
            // 
            this.mobileDataGridViewTextBoxColumn.DataPropertyName = "Mobile";
            this.mobileDataGridViewTextBoxColumn.HeaderText = "เบอร์โทรลูกค้า";
            this.mobileDataGridViewTextBoxColumn.Name = "mobileDataGridViewTextBoxColumn";
            this.mobileDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // customertypeDataGridViewTextBoxColumn
            // 
            this.customertypeDataGridViewTextBoxColumn.DataPropertyName = "Customertype";
            this.customertypeDataGridViewTextBoxColumn.HeaderText = "ประเภทลูกค้า";
            this.customertypeDataGridViewTextBoxColumn.Name = "customertypeDataGridViewTextBoxColumn";
            this.customertypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nickNameDataGridViewTextBoxColumn
            // 
            this.nickNameDataGridViewTextBoxColumn.DataPropertyName = "NickName";
            this.nickNameDataGridViewTextBoxColumn.HeaderText = "ชื่อพนักงาน";
            this.nickNameDataGridViewTextBoxColumn.Name = "nickNameDataGridViewTextBoxColumn";
            this.nickNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // therapistIDDataGridViewTextBoxColumn
            // 
            this.therapistIDDataGridViewTextBoxColumn.DataPropertyName = "TherapistID";
            this.therapistIDDataGridViewTextBoxColumn.HeaderText = "หมายเลข";
            this.therapistIDDataGridViewTextBoxColumn.Name = "therapistIDDataGridViewTextBoxColumn";
            this.therapistIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.therapistIDDataGridViewTextBoxColumn.Width = 75;
            // 
            // packageDataGridViewTextBoxColumn
            // 
            this.packageDataGridViewTextBoxColumn.DataPropertyName = "Package";
            this.packageDataGridViewTextBoxColumn.HeaderText = "นวด/ชม.";
            this.packageDataGridViewTextBoxColumn.Name = "packageDataGridViewTextBoxColumn";
            this.packageDataGridViewTextBoxColumn.ReadOnly = true;
            this.packageDataGridViewTextBoxColumn.Width = 200;
            // 
            // massageTypeDataGridViewTextBoxColumn
            // 
            this.massageTypeDataGridViewTextBoxColumn.DataPropertyName = "MassageType";
            this.massageTypeDataGridViewTextBoxColumn.HeaderText = "ประเภทการนวด";
            this.massageTypeDataGridViewTextBoxColumn.Name = "massageTypeDataGridViewTextBoxColumn";
            this.massageTypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.massageTypeDataGridViewTextBoxColumn.Visible = false;
            this.massageTypeDataGridViewTextBoxColumn.Width = 125;
            // 
            // hourDataGridViewTextBoxColumn
            // 
            this.hourDataGridViewTextBoxColumn.DataPropertyName = "Hour";
            this.hourDataGridViewTextBoxColumn.HeaderText = "ชม.";
            this.hourDataGridViewTextBoxColumn.Name = "hourDataGridViewTextBoxColumn";
            this.hourDataGridViewTextBoxColumn.ReadOnly = true;
            this.hourDataGridViewTextBoxColumn.Visible = false;
            this.hourDataGridViewTextBoxColumn.Width = 45;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            dataGridViewCellStyle12.Format = "d";
            dataGridViewCellStyle12.NullValue = null;
            this.dateDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle12;
            this.dateDataGridViewTextBoxColumn.HeaderText = "วันที่จะมาใช้บริการ";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            this.dateDataGridViewTextBoxColumn.Width = 120;
            // 
            // timeDataGridViewTextBoxColumn
            // 
            this.timeDataGridViewTextBoxColumn.DataPropertyName = "Time";
            dataGridViewCellStyle13.Format = "t";
            dataGridViewCellStyle13.NullValue = null;
            this.timeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this.timeDataGridViewTextBoxColumn.HeaderText = "เวลา";
            this.timeDataGridViewTextBoxColumn.Name = "timeDataGridViewTextBoxColumn";
            this.timeDataGridViewTextBoxColumn.ReadOnly = true;
            this.timeDataGridViewTextBoxColumn.Width = 80;
            // 
            // endDateDataGridViewTextBoxColumn
            // 
            this.endDateDataGridViewTextBoxColumn.DataPropertyName = "EndDate";
            dataGridViewCellStyle14.Format = "t";
            dataGridViewCellStyle14.NullValue = null;
            this.endDateDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle14;
            this.endDateDataGridViewTextBoxColumn.HeaderText = "ถึงเวลา";
            this.endDateDataGridViewTextBoxColumn.Name = "endDateDataGridViewTextBoxColumn";
            this.endDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.endDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // roomDataGridViewTextBoxColumn
            // 
            this.roomDataGridViewTextBoxColumn.DataPropertyName = "Room";
            this.roomDataGridViewTextBoxColumn.HeaderText = "ห้อง";
            this.roomDataGridViewTextBoxColumn.Name = "roomDataGridViewTextBoxColumn";
            this.roomDataGridViewTextBoxColumn.ReadOnly = true;
            this.roomDataGridViewTextBoxColumn.Visible = false;
            this.roomDataGridViewTextBoxColumn.Width = 70;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "Type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "ประเภทรายการ";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.ReadOnly = true;
            this.typeDataGridViewTextBoxColumn.Width = 110;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            this.statusDataGridViewTextBoxColumn.Visible = false;
            // 
            // sexDataGridViewTextBoxColumn
            // 
            this.sexDataGridViewTextBoxColumn.DataPropertyName = "Sex";
            this.sexDataGridViewTextBoxColumn.HeaderText = "Sex";
            this.sexDataGridViewTextBoxColumn.Name = "sexDataGridViewTextBoxColumn";
            this.sexDataGridViewTextBoxColumn.ReadOnly = true;
            this.sexDataGridViewTextBoxColumn.Visible = false;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.ReadOnly = true;
            this.titleDataGridViewTextBoxColumn.Visible = false;
            // 
            // surnameDataGridViewTextBoxColumn
            // 
            this.surnameDataGridViewTextBoxColumn.DataPropertyName = "Surname";
            this.surnameDataGridViewTextBoxColumn.HeaderText = "Surname";
            this.surnameDataGridViewTextBoxColumn.Name = "surnameDataGridViewTextBoxColumn";
            this.surnameDataGridViewTextBoxColumn.ReadOnly = true;
            this.surnameDataGridViewTextBoxColumn.Visible = false;
            // 
            // nationDataGridViewTextBoxColumn
            // 
            this.nationDataGridViewTextBoxColumn.DataPropertyName = "Nation";
            this.nationDataGridViewTextBoxColumn.HeaderText = "Nation";
            this.nationDataGridViewTextBoxColumn.Name = "nationDataGridViewTextBoxColumn";
            this.nationDataGridViewTextBoxColumn.ReadOnly = true;
            this.nationDataGridViewTextBoxColumn.Visible = false;
            // 
            // birthDayDataGridViewTextBoxColumn
            // 
            this.birthDayDataGridViewTextBoxColumn.DataPropertyName = "BirthDay";
            this.birthDayDataGridViewTextBoxColumn.HeaderText = "BirthDay";
            this.birthDayDataGridViewTextBoxColumn.Name = "birthDayDataGridViewTextBoxColumn";
            this.birthDayDataGridViewTextBoxColumn.ReadOnly = true;
            this.birthDayDataGridViewTextBoxColumn.Visible = false;
            // 
            // addressDataGridViewTextBoxColumn
            // 
            this.addressDataGridViewTextBoxColumn.DataPropertyName = "Address";
            this.addressDataGridViewTextBoxColumn.HeaderText = "Address";
            this.addressDataGridViewTextBoxColumn.Name = "addressDataGridViewTextBoxColumn";
            this.addressDataGridViewTextBoxColumn.ReadOnly = true;
            this.addressDataGridViewTextBoxColumn.Visible = false;
            // 
            // emailDataGridViewTextBoxColumn
            // 
            this.emailDataGridViewTextBoxColumn.DataPropertyName = "Email";
            this.emailDataGridViewTextBoxColumn.HeaderText = "Email";
            this.emailDataGridViewTextBoxColumn.Name = "emailDataGridViewTextBoxColumn";
            this.emailDataGridViewTextBoxColumn.ReadOnly = true;
            this.emailDataGridViewTextBoxColumn.Visible = false;
            // 
            // ageDataGridViewTextBoxColumn
            // 
            this.ageDataGridViewTextBoxColumn.DataPropertyName = "Age";
            this.ageDataGridViewTextBoxColumn.HeaderText = "Age";
            this.ageDataGridViewTextBoxColumn.Name = "ageDataGridViewTextBoxColumn";
            this.ageDataGridViewTextBoxColumn.ReadOnly = true;
            this.ageDataGridViewTextBoxColumn.Visible = false;
            // 
            // dETAILDataGridViewTextBoxColumn
            // 
            this.dETAILDataGridViewTextBoxColumn.DataPropertyName = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn.HeaderText = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn.Name = "dETAILDataGridViewTextBoxColumn";
            this.dETAILDataGridViewTextBoxColumn.ReadOnly = true;
            this.dETAILDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYDATEDataGridViewTextBoxColumn
            // 
            this.kEYDATEDataGridViewTextBoxColumn.DataPropertyName = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.HeaderText = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.Name = "kEYDATEDataGridViewTextBoxColumn";
            this.kEYDATEDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYDATEDataGridViewTextBoxColumn.Visible = false;
            // 
            // lASTEDITDataGridViewTextBoxColumn
            // 
            this.lASTEDITDataGridViewTextBoxColumn.DataPropertyName = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.HeaderText = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.Name = "lASTEDITDataGridViewTextBoxColumn";
            this.lASTEDITDataGridViewTextBoxColumn.ReadOnly = true;
            this.lASTEDITDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYUSERDataGridViewTextBoxColumn
            // 
            this.kEYUSERDataGridViewTextBoxColumn.DataPropertyName = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.HeaderText = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.Name = "kEYUSERDataGridViewTextBoxColumn";
            this.kEYUSERDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYUSERDataGridViewTextBoxColumn.Visible = false;
            // 
            // fLAGDataGridViewTextBoxColumn
            // 
            this.fLAGDataGridViewTextBoxColumn.DataPropertyName = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.HeaderText = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.Name = "fLAGDataGridViewTextBoxColumn";
            this.fLAGDataGridViewTextBoxColumn.ReadOnly = true;
            this.fLAGDataGridViewTextBoxColumn.Visible = false;
            // 
            // startDateDataGridViewTextBoxColumn
            // 
            this.startDateDataGridViewTextBoxColumn.DataPropertyName = "StartDate";
            this.startDateDataGridViewTextBoxColumn.HeaderText = "StartDate";
            this.startDateDataGridViewTextBoxColumn.Name = "startDateDataGridViewTextBoxColumn";
            this.startDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.startDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // remarkDataGridViewTextBoxColumn
            // 
            this.remarkDataGridViewTextBoxColumn.DataPropertyName = "Remark";
            this.remarkDataGridViewTextBoxColumn.HeaderText = "Remark";
            this.remarkDataGridViewTextBoxColumn.Name = "remarkDataGridViewTextBoxColumn";
            this.remarkDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cCustomerTransactionBindingSource
            // 
            this.cCustomerTransactionBindingSource.DataSource = typeof(Rakuten.Structure.CCustomerTransaction);
            // 
            // cEmployeeBindingSource
            // 
            this.cEmployeeBindingSource.DataSource = typeof(Rakuten.Structure.CEmployee);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 580);
            this.ControlBox = false;
            this.Controls.Add(this.tabControl1);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMain";
            this.Text = "FrmMain";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cScheduleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCustomerTransactionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.BindingSource cEmployeeBindingSource;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem CMS_CHECKIN;
        private System.Windows.Forms.ToolStripMenuItem CMS_CHECKOUT;
        private System.Windows.Forms.ToolStripMenuItem CMS_PERIOD;
        private System.Windows.Forms.ToolStripMenuItem CMS_P1;
        private System.Windows.Forms.ToolStripMenuItem CMS_P2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem CMS_LEAVE;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem TSM_UPDATE;
        private System.Windows.Forms.ToolStripMenuItem TSM_CANCEL;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.BindingSource cCustomerTransactionBindingSource;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripMenuItem TSM_PAY;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem TSM_PRINTINVOICE;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.BindingSource cScheduleBindingSource;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView listView1;
        public System.Windows.Forms.DateTimePicker DTP_SERVICE_DATE;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn rUNNINGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobileDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customertypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nickNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn therapistIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn massageTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hourDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn roomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn surnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthDayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dETAILDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lASTEDITDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYUSERDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fLAGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarkDataGridViewTextBoxColumn;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeInDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeOutDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0030DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0100DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0130DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0200DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0230DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0300DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0330DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0400DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0430DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0500DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0530DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0600DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0630DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0700DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0730DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0800DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0830DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0900DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0930DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1000DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1030DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1100DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1130DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1200DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1230DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1300DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1330DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1400DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1430DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1500DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1530DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1600DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1630DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1700DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1730DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1800DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1830DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1900DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1930DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t2000DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t2030DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t2100DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t2130DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t2200DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t2230DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t2300DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t2330DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0000DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rUNNINGDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dETAILDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYDATEDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn lASTEDITDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYUSERDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fLAGDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarkDataGridViewTextBoxColumn1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Label LB_CUS_TOTAL;
        private System.Windows.Forms.Label LB_SELL_TOTAL;
    }
}