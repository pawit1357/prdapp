﻿namespace Rakuten
{
    partial class FrmPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CMS_Insert = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_Update = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.BSearch = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.rUNNINGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dETAILDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hourDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TherapistPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moneyUnitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perCourseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lASTEDITDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYUSERDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fLAGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPackageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.DTP_START = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.DTP_END = new System.Windows.Forms.DateTimePicker();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TXT_ID = new System.Windows.Forms.TextBox();
            this.CBO_CUSTYPE = new System.Windows.Forms.ComboBox();
            this.cMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TXT_NAME = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.B_SHR = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.CBO_HR = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CBO_TYPE = new System.Windows.Forms.ComboBox();
            this.B_STYPE = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TXT_COST = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TXT_THB = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TXT_PERCOURSE = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TXT_THERAPIST_PRICE = new System.Windows.Forms.TextBox();
            this.B_CANCEL = new System.Windows.Forms.Button();
            this.B_SAVE = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.RD_ID = new System.Windows.Forms.RadioButton();
            this.RD_Name = new System.Windows.Forms.RadioButton();
            this.RD_ALL = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cPackageBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMasterBindingSource)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CMS_Insert,
            this.CMS_Update,
            this.CMS_Delete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuStrip1.Size = new System.Drawing.Size(113, 70);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // CMS_Insert
            // 
            this.CMS_Insert.Name = "CMS_Insert";
            this.CMS_Insert.Size = new System.Drawing.Size(112, 22);
            this.CMS_Insert.Text = "Insert";
            this.CMS_Insert.Click += new System.EventHandler(this.CMS_Click);
            // 
            // CMS_Update
            // 
            this.CMS_Update.Name = "CMS_Update";
            this.CMS_Update.Size = new System.Drawing.Size(112, 22);
            this.CMS_Update.Text = "Update";
            this.CMS_Update.Click += new System.EventHandler(this.CMS_Click);
            // 
            // CMS_Delete
            // 
            this.CMS_Delete.Name = "CMS_Delete";
            this.CMS_Delete.Size = new System.Drawing.Size(112, 22);
            this.CMS_Delete.Text = "Delete";
            this.CMS_Delete.Click += new System.EventHandler(this.CMS_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.txtSearch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtSearch.ForeColor = System.Drawing.Color.Yellow;
            this.txtSearch.Location = new System.Drawing.Point(251, 25);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(248, 22);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // BSearch
            // 
            this.BSearch.Image = global::Rakuten.Properties.Resources.document_view;
            this.BSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BSearch.Location = new System.Drawing.Point(505, 19);
            this.BSearch.Name = "BSearch";
            this.BSearch.Size = new System.Drawing.Size(75, 32);
            this.BSearch.TabIndex = 2;
            this.BSearch.Text = "ค้นหา";
            this.BSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BSearch.UseVisualStyleBackColor = true;
            this.BSearch.Click += new System.EventHandler(this.B_Search_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(19, 125);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(587, 508);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.TabStop = false;
            this.tabControl1.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(579, 482);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "แสดงรายการค้นหา";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rUNNINGDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn,
            this.dETAILDataGridViewTextBoxColumn,
            this.hourDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.TherapistPrice,
            this.moneyUnitDataGridViewTextBoxColumn,
            this.perCourseDataGridViewTextBoxColumn,
            this.kEYDATEDataGridViewTextBoxColumn,
            this.lASTEDITDataGridViewTextBoxColumn,
            this.kEYUSERDataGridViewTextBoxColumn,
            this.fLAGDataGridViewTextBoxColumn,
            this.startDateDataGridViewTextBoxColumn,
            this.endDateDataGridViewTextBoxColumn,
            this.remarkDataGridViewTextBoxColumn});
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.DataSource = this.cPackageBindingSource;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(573, 476);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.TabStop = false;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseDown);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseEnter);
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // rUNNINGDataGridViewTextBoxColumn
            // 
            this.rUNNINGDataGridViewTextBoxColumn.DataPropertyName = "RUNNING";
            this.rUNNINGDataGridViewTextBoxColumn.HeaderText = "ลำดับ";
            this.rUNNINGDataGridViewTextBoxColumn.Name = "rUNNINGDataGridViewTextBoxColumn";
            this.rUNNINGDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "รหัส";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Width = 80;
            // 
            // dETAILDataGridViewTextBoxColumn
            // 
            this.dETAILDataGridViewTextBoxColumn.DataPropertyName = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn.HeaderText = "รายละเอียด";
            this.dETAILDataGridViewTextBoxColumn.Name = "dETAILDataGridViewTextBoxColumn";
            this.dETAILDataGridViewTextBoxColumn.ReadOnly = true;
            this.dETAILDataGridViewTextBoxColumn.Width = 200;
            // 
            // hourDataGridViewTextBoxColumn
            // 
            this.hourDataGridViewTextBoxColumn.DataPropertyName = "Hour";
            this.hourDataGridViewTextBoxColumn.HeaderText = "ช.ม.";
            this.hourDataGridViewTextBoxColumn.Name = "hourDataGridViewTextBoxColumn";
            this.hourDataGridViewTextBoxColumn.ReadOnly = true;
            this.hourDataGridViewTextBoxColumn.Width = 80;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "ราคา";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            this.priceDataGridViewTextBoxColumn.Width = 80;
            // 
            // TherapistPrice
            // 
            this.TherapistPrice.DataPropertyName = "TherapistPrice";
            this.TherapistPrice.HeaderText = "ค่ามือพนักงาน";
            this.TherapistPrice.Name = "TherapistPrice";
            this.TherapistPrice.ReadOnly = true;
            this.TherapistPrice.Visible = false;
            // 
            // moneyUnitDataGridViewTextBoxColumn
            // 
            this.moneyUnitDataGridViewTextBoxColumn.DataPropertyName = "MoneyUnit";
            this.moneyUnitDataGridViewTextBoxColumn.HeaderText = "MoneyUnit";
            this.moneyUnitDataGridViewTextBoxColumn.Name = "moneyUnitDataGridViewTextBoxColumn";
            this.moneyUnitDataGridViewTextBoxColumn.ReadOnly = true;
            this.moneyUnitDataGridViewTextBoxColumn.Visible = false;
            // 
            // perCourseDataGridViewTextBoxColumn
            // 
            this.perCourseDataGridViewTextBoxColumn.DataPropertyName = "PerCourse";
            this.perCourseDataGridViewTextBoxColumn.HeaderText = "PerCourse";
            this.perCourseDataGridViewTextBoxColumn.Name = "perCourseDataGridViewTextBoxColumn";
            this.perCourseDataGridViewTextBoxColumn.ReadOnly = true;
            this.perCourseDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYDATEDataGridViewTextBoxColumn
            // 
            this.kEYDATEDataGridViewTextBoxColumn.DataPropertyName = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.HeaderText = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.Name = "kEYDATEDataGridViewTextBoxColumn";
            this.kEYDATEDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYDATEDataGridViewTextBoxColumn.Visible = false;
            // 
            // lASTEDITDataGridViewTextBoxColumn
            // 
            this.lASTEDITDataGridViewTextBoxColumn.DataPropertyName = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.HeaderText = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.Name = "lASTEDITDataGridViewTextBoxColumn";
            this.lASTEDITDataGridViewTextBoxColumn.ReadOnly = true;
            this.lASTEDITDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYUSERDataGridViewTextBoxColumn
            // 
            this.kEYUSERDataGridViewTextBoxColumn.DataPropertyName = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.HeaderText = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.Name = "kEYUSERDataGridViewTextBoxColumn";
            this.kEYUSERDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYUSERDataGridViewTextBoxColumn.Visible = false;
            // 
            // fLAGDataGridViewTextBoxColumn
            // 
            this.fLAGDataGridViewTextBoxColumn.DataPropertyName = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.HeaderText = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.Name = "fLAGDataGridViewTextBoxColumn";
            this.fLAGDataGridViewTextBoxColumn.ReadOnly = true;
            this.fLAGDataGridViewTextBoxColumn.Visible = false;
            // 
            // startDateDataGridViewTextBoxColumn
            // 
            this.startDateDataGridViewTextBoxColumn.DataPropertyName = "StartDate";
            this.startDateDataGridViewTextBoxColumn.HeaderText = "StartDate";
            this.startDateDataGridViewTextBoxColumn.Name = "startDateDataGridViewTextBoxColumn";
            this.startDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.startDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // endDateDataGridViewTextBoxColumn
            // 
            this.endDateDataGridViewTextBoxColumn.DataPropertyName = "EndDate";
            this.endDateDataGridViewTextBoxColumn.HeaderText = "EndDate";
            this.endDateDataGridViewTextBoxColumn.Name = "endDateDataGridViewTextBoxColumn";
            this.endDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.endDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // remarkDataGridViewTextBoxColumn
            // 
            this.remarkDataGridViewTextBoxColumn.DataPropertyName = "Remark";
            this.remarkDataGridViewTextBoxColumn.HeaderText = "ราคาค่ามือ";
            this.remarkDataGridViewTextBoxColumn.Name = "remarkDataGridViewTextBoxColumn";
            this.remarkDataGridViewTextBoxColumn.ReadOnly = true;
            this.remarkDataGridViewTextBoxColumn.Visible = false;
            // 
            // cPackageBindingSource
            // 
            this.cPackageBindingSource.AllowNew = true;
            this.cPackageBindingSource.DataSource = typeof(Rakuten.Structure.CPackage);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(579, 482);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "เพิ่ม/ลบข้อมูล";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.groupBox6);
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.B_CANCEL);
            this.panel1.Controls.Add(this.B_SAVE);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(573, 476);
            this.panel1.TabIndex = 12;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.DTP_START);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.DTP_END);
            this.groupBox6.Location = new System.Drawing.Point(11, 204);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(546, 83);
            this.groupBox6.TabIndex = 133;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "ช่วงเวลา Special Package";
            // 
            // DTP_START
            // 
            this.DTP_START.CalendarFont = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTP_START.CustomFormat = "HH:mm";
            this.DTP_START.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTP_START.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTP_START.Location = new System.Drawing.Point(115, 19);
            this.DTP_START.Name = "DTP_START";
            this.DTP_START.Size = new System.Drawing.Size(141, 22);
            this.DTP_START.TabIndex = 6;
            this.DTP_START.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label19.Location = new System.Drawing.Point(51, 51);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 14);
            this.label19.TabIndex = 42;
            this.label19.Text = "เวลาสิ้นสุด";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.Location = new System.Drawing.Point(49, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 14);
            this.label17.TabIndex = 41;
            this.label17.Text = "เวลาเริ่มต้น";
            // 
            // DTP_END
            // 
            this.DTP_END.CalendarFont = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTP_END.CustomFormat = "HH:mm";
            this.DTP_END.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTP_END.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTP_END.Location = new System.Drawing.Point(115, 47);
            this.DTP_END.Name = "DTP_END";
            this.DTP_END.Size = new System.Drawing.Size(141, 22);
            this.DTP_END.TabIndex = 7;
            this.DTP_END.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.TXT_ID);
            this.groupBox5.Controls.Add(this.CBO_CUSTYPE);
            this.groupBox5.Controls.Add(this.TXT_NAME);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.button5);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.B_SHR);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.CBO_HR);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.CBO_TYPE);
            this.groupBox5.Controls.Add(this.B_STYPE);
            this.groupBox5.Location = new System.Drawing.Point(11, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(546, 185);
            this.groupBox5.TabIndex = 132;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ข้อมูล Package";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(46, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 14);
            this.label7.TabIndex = 87;
            this.label7.Text = "จำนวน ชม.";
            // 
            // TXT_ID
            // 
            this.TXT_ID.BackColor = System.Drawing.SystemColors.MenuText;
            this.TXT_ID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_ID.ForeColor = System.Drawing.Color.Gold;
            this.TXT_ID.Location = new System.Drawing.Point(115, 19);
            this.TXT_ID.MaxLength = 10;
            this.TXT_ID.Name = "TXT_ID";
            this.TXT_ID.Size = new System.Drawing.Size(144, 23);
            this.TXT_ID.TabIndex = 1;
            this.TXT_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_ID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // CBO_CUSTYPE
            // 
            this.CBO_CUSTYPE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_CUSTYPE.DataSource = this.cMasterBindingSource;
            this.CBO_CUSTYPE.DisplayMember = "DETAIL";
            this.CBO_CUSTYPE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_CUSTYPE.FormattingEnabled = true;
            this.CBO_CUSTYPE.Location = new System.Drawing.Point(115, 113);
            this.CBO_CUSTYPE.Name = "CBO_CUSTYPE";
            this.CBO_CUSTYPE.Size = new System.Drawing.Size(141, 26);
            this.CBO_CUSTYPE.TabIndex = 4;
            this.CBO_CUSTYPE.ValueMember = "ID";
            // 
            // cMasterBindingSource
            // 
            this.cMasterBindingSource.DataSource = typeof(Rakuten.Structure.CMaster);
            // 
            // TXT_NAME
            // 
            this.TXT_NAME.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_NAME.Location = new System.Drawing.Point(115, 48);
            this.TXT_NAME.Name = "TXT_NAME";
            this.TXT_NAME.Size = new System.Drawing.Size(204, 26);
            this.TXT_NAME.TabIndex = 2;
            this.TXT_NAME.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label22.Location = new System.Drawing.Point(19, 119);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(90, 14);
            this.label22.TabIndex = 126;
            this.label22.Text = "Package สำหรับ";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button5.Location = new System.Drawing.Point(262, 113);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(30, 26);
            this.button5.TabIndex = 127;
            this.button5.Text = "...";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.Location = new System.Drawing.Point(35, 54);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 14);
            this.label18.TabIndex = 12;
            this.label18.Text = "ชื่อ/คำอธิบาย";
            // 
            // B_SHR
            // 
            this.B_SHR.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SHR.Location = new System.Drawing.Point(262, 145);
            this.B_SHR.Name = "B_SHR";
            this.B_SHR.Size = new System.Drawing.Size(30, 26);
            this.B_SHR.TabIndex = 88;
            this.B_SHR.Text = "...";
            this.B_SHR.UseVisualStyleBackColor = true;
            this.B_SHR.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(82, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "รหัส";
            // 
            // CBO_HR
            // 
            this.CBO_HR.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_HR.DataSource = this.cMasterBindingSource;
            this.CBO_HR.DisplayMember = "DETAIL";
            this.CBO_HR.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_HR.FormattingEnabled = true;
            this.CBO_HR.Location = new System.Drawing.Point(115, 145);
            this.CBO_HR.Name = "CBO_HR";
            this.CBO_HR.Size = new System.Drawing.Size(141, 26);
            this.CBO_HR.TabIndex = 5;
            this.CBO_HR.ValueMember = "ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(62, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 14);
            this.label4.TabIndex = 29;
            this.label4.Text = "ประเภท";
            // 
            // CBO_TYPE
            // 
            this.CBO_TYPE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_TYPE.DataSource = this.cMasterBindingSource;
            this.CBO_TYPE.DisplayMember = "DETAIL";
            this.CBO_TYPE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_TYPE.FormattingEnabled = true;
            this.CBO_TYPE.Location = new System.Drawing.Point(115, 80);
            this.CBO_TYPE.Name = "CBO_TYPE";
            this.CBO_TYPE.Size = new System.Drawing.Size(204, 26);
            this.CBO_TYPE.TabIndex = 3;
            this.CBO_TYPE.ValueMember = "ID";
            this.CBO_TYPE.SelectedIndexChanged += new System.EventHandler(this.CBO_TYPE_SelectedIndexChanged);
            this.CBO_TYPE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // B_STYPE
            // 
            this.B_STYPE.Location = new System.Drawing.Point(325, 82);
            this.B_STYPE.Name = "B_STYPE";
            this.B_STYPE.Size = new System.Drawing.Size(30, 23);
            this.B_STYPE.TabIndex = 28;
            this.B_STYPE.Text = "...";
            this.B_STYPE.UseVisualStyleBackColor = true;
            this.B_STYPE.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TXT_COST);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.TXT_THB);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.TXT_PERCOURSE);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.TXT_THERAPIST_PRICE);
            this.groupBox3.Location = new System.Drawing.Point(11, 293);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(546, 133);
            this.groupBox3.TabIndex = 128;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "กำหนดราคา";
            // 
            // TXT_COST
            // 
            this.TXT_COST.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_COST.Location = new System.Drawing.Point(115, 19);
            this.TXT_COST.Name = "TXT_COST";
            this.TXT_COST.Size = new System.Drawing.Size(133, 26);
            this.TXT_COST.TabIndex = 8;
            this.TXT_COST.Text = "0";
            this.TXT_COST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_COST.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            this.TXT_COST.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckIsNumber_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(78, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 14);
            this.label1.TabIndex = 31;
            this.label1.Text = "ราคา";
            // 
            // TXT_THB
            // 
            this.TXT_THB.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_THB.Location = new System.Drawing.Point(254, 19);
            this.TXT_THB.Name = "TXT_THB";
            this.TXT_THB.Size = new System.Drawing.Size(47, 26);
            this.TXT_THB.TabIndex = 6;
            this.TXT_THB.Text = "THB";
            this.TXT_THB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(38, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 14);
            this.label16.TabIndex = 36;
            this.label16.Text = "ราคาต่อ";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label21.Location = new System.Drawing.Point(257, 57);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(146, 14);
            this.label21.TabIndex = 91;
            this.label21.Text = "ราคาสำหรับคิดค่ามือพนักงาน";
            // 
            // TXT_PERCOURSE
            // 
            this.TXT_PERCOURSE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_PERCOURSE.Location = new System.Drawing.Point(115, 83);
            this.TXT_PERCOURSE.Name = "TXT_PERCOURSE";
            this.TXT_PERCOURSE.Size = new System.Drawing.Size(133, 26);
            this.TXT_PERCOURSE.TabIndex = 10;
            this.TXT_PERCOURSE.Text = "1";
            this.TXT_PERCOURSE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_PERCOURSE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            this.TXT_PERCOURSE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckIsNumber_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label20.Location = new System.Drawing.Point(76, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 14);
            this.label20.TabIndex = 90;
            this.label20.Text = "ราคา";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(257, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 14);
            this.label5.TabIndex = 38;
            this.label5.Text = "คอร์ส";
            // 
            // TXT_THERAPIST_PRICE
            // 
            this.TXT_THERAPIST_PRICE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_THERAPIST_PRICE.Location = new System.Drawing.Point(115, 51);
            this.TXT_THERAPIST_PRICE.Name = "TXT_THERAPIST_PRICE";
            this.TXT_THERAPIST_PRICE.Size = new System.Drawing.Size(133, 26);
            this.TXT_THERAPIST_PRICE.TabIndex = 9;
            this.TXT_THERAPIST_PRICE.Text = "0";
            this.TXT_THERAPIST_PRICE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // B_CANCEL
            // 
            this.B_CANCEL.Image = global::Rakuten.Properties.Resources.redo;
            this.B_CANCEL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.B_CANCEL.Location = new System.Drawing.Point(472, 432);
            this.B_CANCEL.Name = "B_CANCEL";
            this.B_CANCEL.Size = new System.Drawing.Size(75, 35);
            this.B_CANCEL.TabIndex = 12;
            this.B_CANCEL.Text = "ยกเลิก";
            this.B_CANCEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.B_CANCEL.UseVisualStyleBackColor = true;
            this.B_CANCEL.Click += new System.EventHandler(this.B_CANCEL_Click);
            this.B_CANCEL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // B_SAVE
            // 
            this.B_SAVE.Image = global::Rakuten.Properties.Resources.disk_blue_ok;
            this.B_SAVE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.B_SAVE.Location = new System.Drawing.Point(391, 432);
            this.B_SAVE.Name = "B_SAVE";
            this.B_SAVE.Size = new System.Drawing.Size(75, 35);
            this.B_SAVE.TabIndex = 11;
            this.B_SAVE.Text = "บันทึก";
            this.B_SAVE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.B_SAVE.UseVisualStyleBackColor = true;
            this.B_SAVE.Click += new System.EventHandler(this.B_SAVE_Click);
            this.B_SAVE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(56, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(401, 14);
            this.label2.TabIndex = 9;
            this.label2.Text = "การลบ เลือกทรัพย์สินที่ต้องการลบจากหน้า (แสดงรายการค้นหา) แล้ว (กดปุ่มลบ)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(56, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(195, 14);
            this.label8.TabIndex = 8;
            this.label8.Text = "การเพิ่ม พิมพ์ชื่อทรัพย์สิน แล้ว (กดเพิ่ม)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.Location = new System.Drawing.Point(24, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 14);
            this.label9.TabIndex = 7;
            this.label9.Text = "หมายเหตุ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(24, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 14);
            this.label10.TabIndex = 5;
            this.label10.Text = "ชื่อทรัพย์สิน";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(389, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "ลบ";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(94, 63);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(370, 59);
            this.textBox1.TabIndex = 4;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "XXXX";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(308, 128);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "เพิ่ม";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(56, 209);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(401, 14);
            this.label11.TabIndex = 9;
            this.label11.Text = "การลบ เลือกทรัพย์สินที่ต้องการลบจากหน้า (แสดงรายการค้นหา) แล้ว (กดปุ่มลบ)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.Location = new System.Drawing.Point(56, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(195, 14);
            this.label12.TabIndex = 8;
            this.label12.Text = "การเพิ่ม พิมพ์ชื่อทรัพย์สิน แล้ว (กดเพิ่ม)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(24, 161);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 14);
            this.label13.TabIndex = 7;
            this.label13.Text = "หมายเหตุ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.Location = new System.Drawing.Point(24, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 14);
            this.label14.TabIndex = 5;
            this.label14.Text = "ชื่อทรัพย์สิน";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(389, 128);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "ลบ";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(94, 63);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(370, 59);
            this.textBox2.TabIndex = 4;
            this.textBox2.TabStop = false;
            this.textBox2.Text = "XXXX";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(308, 128);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "เพิ่ม";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // RD_ID
            // 
            this.RD_ID.AutoSize = true;
            this.RD_ID.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RD_ID.Location = new System.Drawing.Point(77, 19);
            this.RD_ID.Name = "RD_ID";
            this.RD_ID.Size = new System.Drawing.Size(44, 17);
            this.RD_ID.TabIndex = 15;
            this.RD_ID.Text = "รหัส";
            this.RD_ID.UseVisualStyleBackColor = true;
            this.RD_ID.CheckedChanged += new System.EventHandler(this.RD_ALL_Click);
            // 
            // RD_Name
            // 
            this.RD_Name.AutoSize = true;
            this.RD_Name.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RD_Name.Location = new System.Drawing.Point(127, 20);
            this.RD_Name.Name = "RD_Name";
            this.RD_Name.Size = new System.Drawing.Size(39, 17);
            this.RD_Name.TabIndex = 16;
            this.RD_Name.Text = "ชื่อ";
            this.RD_Name.UseVisualStyleBackColor = true;
            this.RD_Name.CheckedChanged += new System.EventHandler(this.RD_ALL_Click);
            // 
            // RD_ALL
            // 
            this.RD_ALL.AutoSize = true;
            this.RD_ALL.Checked = true;
            this.RD_ALL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RD_ALL.Location = new System.Drawing.Point(12, 19);
            this.RD_ALL.Name = "RD_ALL";
            this.RD_ALL.Size = new System.Drawing.Size(59, 17);
            this.RD_ALL.TabIndex = 17;
            this.RD_ALL.TabStop = true;
            this.RD_ALL.Text = "ทั้งหมด";
            this.RD_ALL.UseVisualStyleBackColor = true;
            this.RD_ALL.CheckedChanged += new System.EventHandler(this.RD_ALL_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RD_Name);
            this.groupBox1.Controls.Add(this.RD_ID);
            this.groupBox1.Controls.Add(this.RD_ALL);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(6, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(211, 45);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "รูปแบบการค้นหา";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.txtSearch);
            this.groupBox2.Controls.Add(this.BSearch);
            this.groupBox2.Location = new System.Drawing.Point(19, 54);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(587, 69);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.Green;
            this.label6.Location = new System.Drawing.Point(85, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(157, 25);
            this.label6.TabIndex = 175;
            this.label6.Text = "ตั้งค่า Package";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::Rakuten.Properties.Resources.product;
            this.pictureBox1.Location = new System.Drawing.Point(12, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(67, 50);
            this.pictureBox1.TabIndex = 173;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(623, 50);
            this.pictureBox2.TabIndex = 174;
            this.pictureBox2.TabStop = false;
            // 
            // FrmPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(623, 638);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPackage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ตั้งค่า Package";
            this.Load += new System.EventHandler(this.FrmPackage_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cPackageBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMasterBindingSource)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BSearch;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.TextBox txtSearch;

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox TXT_NAME;
        private System.Windows.Forms.Button B_SAVE;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem CMS_Insert;
        private System.Windows.Forms.ToolStripMenuItem CMS_Update;
        private System.Windows.Forms.ToolStripMenuItem CMS_Delete;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button B_CANCEL;
        private System.Windows.Forms.TextBox TXT_ID;
        private System.Windows.Forms.RadioButton RD_ID;
        private System.Windows.Forms.RadioButton RD_Name;
        private System.Windows.Forms.RadioButton RD_ALL;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox CBO_TYPE;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button B_STYPE;
        private System.Windows.Forms.BindingSource cMasterBindingSource;
        private System.Windows.Forms.TextBox TXT_THB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TXT_COST;
        private System.Windows.Forms.TextBox TXT_PERCOURSE;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker DTP_END;
        private System.Windows.Forms.DateTimePicker DTP_START;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox CBO_HR;
        private System.Windows.Forms.Button B_SHR;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource cPackageBindingSource;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox TXT_THERAPIST_PRICE;
        private System.Windows.Forms.ComboBox CBO_CUSTYPE;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox3;

        private System.Windows.Forms.DataGridViewTextBoxColumn rUNNINGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dETAILDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hourDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TherapistPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn moneyUnitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perCourseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lASTEDITDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYUSERDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fLAGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarkDataGridViewTextBoxColumn;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;

    }
}