﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using Rakuten.Structure;
using Rakuten.DAL;
using Rakuten.Utility;
using System.Data.SqlClient;

namespace Rakuten
{
    public partial class FrmLeave : Form
    {
        private FrmMain main;
        private DBStatus status = DBStatus.Insert;//Initial Status
        private List<CEmployee> lists = null;
        private List<CLeave> lists1 = null;
        public int index = 0;
        public bool bSelected = false;
        public FrmLeave(FrmMain _main)
        {
            InitializeComponent();
            this.main = _main;
        }

        private void FrmLeave_Load(object sender, EventArgs e)
        {
            initial();
        }

        public void initial()
        {
            //Clear Value
            TXT_ID.Text = "";
            refreshData();
            TXT_ID.Focus();
            //Leave Type
            CBO_TYPE.DataSource = DALMaster.getList(MasterList.MLeave,
                                new CMaster
                                {
                                    ID = "",
                                    DETAIL = "",
                                    KEYDATE = DateTime.Now,
                                    KEYUSER = Authorize.getUser(),
                                    LASTEDIT = DateTime.Now,
                                    FLAG = Management.SearchAll//Searh Flag
                                }
                                );
            //ตรวจสอบจากการเลือก พนักงานนวดมาจาก form main ถ้ามีการเลือกมาให้แสดงข้อมูลเลย
            if (!main.Therapist.Equals("0000"))
            {
                showTherapistData(main.Therapist);
            }
            tabControl1.TabPages.RemoveAt(1);
        }

        private void CMD_SAVE_Click(object sender, EventArgs e)
        {
            if (TXT_COUNT.Text.Equals("0"))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_COUNT.Focus();
            }
            else if (TXT_REMARK.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_REMARK.Focus();
            }
            else if (!CBO_TYPE.SelectedValue.Equals("0004") && TXT_USE.Text.Equals("0"))
            {
                Management.ShowMsg("จำนวนวันลา ประเภท" + CBO_TYPE.Text + " หมดแล้ว");
            }
            else
            {
                try
                {
                    CEmployeeSummary emp = new CEmployeeSummary
                        {
                            ID = lists1[index].ID,
                            EmpID = lists1[index].EmpID,
                            Type = CBO_TYPE.SelectedValue.ToString(),
                            Income = TXT_COUNT.Text,
                            StartDate = DTP_S.Value,
                            EndDate = DTS_END.Value,
                            Remark = TXT_REMARK.Text,
                            KEYDATE = DateTime.Now,
                            KEYUSER = Authorize.getUser(),
                            FLAG = (status == DBStatus.Insert) ? Management.Insert : (status == DBStatus.Update) ? Management.Update : Management.Delete
                        };
                    if (DALEmployee.manageLeave(emp) > 0)
                    {
                        Management.ShowMsg(Management.INSERT_SUCCESS);
                        Close();
                    }
                }
                catch (SqlException ex)
                {
                    Management.ShowMsg(ex.Message);
                    Close();
                }
            }

        }

        private void CMD_CANCEL_Click(object sender, EventArgs e)
        {
            main.refreshSchedule();
            Close();
        }

        private void B_SMEMBER_Click(object sender, EventArgs e)
        {
            refreshData();//Refresh Data
            //6 คือ flag บอกว่าให้ แสดง เฉพาะ ชื่อหมอนวด (รหัสหมอนวด 0003)
            FrmEmployee employee = new FrmEmployee("6", FormStatus.Search);
            employee.ShowDialog();
            if (employee.bSelected)
            {
                lists = employee.lists;
                TXT_ID.Text = lists[employee.index].ID;
                TXT_NAME.Text = lists[employee.index].Name + "  " + lists[employee.index].Surname;
                setLeaveType();
                TXT_COUNT.Focus();
            }
        }

        private void TXT_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                refreshData();//Refresh Data
                showTherapistData(TXT_ID.Text);//Show therapist data
            }
        }

        private void CBO_TYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CBO_TYPE.SelectedValue != null)
            {
                setLeaveType();
            }
        }

        private void showTherapistData(string id)
        {
            lists = DALEmployee.getList(new CEmployee
            {
                ID = id,
                Type = "0003",
                Sex = "",
                Title = "",
                Name = "",
                Surname = "",
                NickName = "",
                Nation = "",
                BirthDay = DateTime.Now,
                Address = "",
                Mobile = "",
                Email = "",
                Status = "",
                Period = "",
                KEYDATE = DateTime.Now,
                KEYUSER = Authorize.getUser(),
                LASTEDIT = DateTime.Now,
                FLAG = "8"
            }
            );
            //Show Employee Leave history
            lists1 = DALEmployee.getLeave(new CLeave
            {
                ID = "",
                EmpID = id,
                Type = "",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                Count = 0,
                Remark = "",
                KEYDATE = DateTime.Now,
                KEYUSER = "",
                FLAG = "5"
            });
            dataGridView1.DataSource = lists1;
            //Show therapist data
            if (lists.Count > 0)
            {
                TXT_ID.Text = lists[0].ID;
                TXT_NAME.Text = lists[0].Name + "  " + lists[0].Surname;
                setLeaveType();
            }
            else
            {
                Management.ShowMsg(Management.NO_FOUND_DATA);
            }
        }

        private void setLeaveType()
        {
            //แสดงจำนวนวันทั้งหมดที่ลาได้แยกตามประเภทการลา
            if (lists != null)
            {
                switch (CBO_TYPE.SelectedValue.ToString())
                {
                    case "0001": //ติดธุระ
                        TXT_TOTAL.Text = lists[0].Biz.ToString();
                        break;
                    case "0002": //ลาพักร้อน
                        TXT_TOTAL.Text = lists[0].Holiday.ToString();
                        break;
                    case "0003": //ป่วย
                        TXT_TOTAL.Text = lists[0].Sick.ToString();
                        break;
                    default:
                        TXT_TOTAL.Text = "0";
                        TXT_USE.Text = "0";
                        TXT_COUNT.Text = "1";
                        break;
                }
                showLeaveCount();//แสดงวันที่ลาได้ตามประเภท
            }
        }

        private void refreshData()
        {
            TXT_NAME.Text = "";
            TXT_REMARK.Text = "";
            TXT_TOTAL.Text = "0";
            TXT_USE.Text = "0";
            TXT_COUNT.Text = "1";
            CBO_TYPE.SelectedValue = "0001";
        }

        private void showLeaveCount()
        {
            CEmployeeSummary emp = new CEmployeeSummary
            {
                ID = TXT_ID.Text,
                Type = CBO_TYPE.SelectedValue.ToString(),
                Income = "",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                Remark = "",
                KEYDATE = DateTime.Now,
                KEYUSER = Authorize.getUser(),
                FLAG = "4"//ตรวจสอบการใช้วันลาแยกตามประเภท
            };
            TXT_USE.Text = (Convert.ToDouble(TXT_TOTAL.Text) - DALEmployee.getEmployeeLeaveCount(emp)).ToString();
        }

        private void FrmLeave_FormClosed(object sender, FormClosedEventArgs e)
        {
            main.refreshSchedule();
        }

        private void DTS_END_ValueChanged(object sender, EventArgs e)
        {
            TXT_COUNT.Text = (MyFunction.calDay(DTS_END.Value, DTP_S.Value)+1).ToString();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 2:
                    e.Value = Management.getDetail(MasterList.MLeave, e.Value.ToString());
                    break;
            }
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }

        private void CMS_Insert_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            tabControl1.TabPages.Add(tabPage2);
            //tabPage2.Controls.Add(panel1);
            tabControl1.SelectTab(1);
            switch(tsmi.Name)
            {
                case "CMS_Insert":
                    status = DBStatus.Insert;
                    CMD_SAVE.Text = "เพิ่ม";
                    break;
                case "CMS_Update":
                    status = DBStatus.Update;
                    CMD_SAVE.Text = "ปรับปรุง";
                    CBO_TYPE.SelectedValue = lists1[index].Type;
                    DTP_S.Value = lists1[index].StartDate;
                    DTS_END.Value = lists1[index].EndDate;
                    TXT_REMARK.Text = lists1[index].Remark;
                    TXT_COUNT.Text = lists1[index].Count.ToString();
                    break;
                case "CMS_Delete":
                    status = DBStatus.Delete;
                    CMD_SAVE.Text = "ลบ";
                    CBO_TYPE.SelectedValue = lists1[index].Type;
                    DTP_S.Value = lists1[index].StartDate;
                    DTS_END.Value = lists1[index].EndDate;
                    TXT_REMARK.Text = lists1[index].Remark;
                    TXT_COUNT.Text = lists1[index].Count.ToString();
                    break;
            }
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
            }
            Cursor = Cursors.Default;
        }

        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.dataGridView1.HitTest(e.X, e.Y);
            if (hti.RowIndex != -1)
            {
                if (hti.Type == DataGridViewHitTestType.Cell)
                {
                    dataGridView1.ClearSelection();
                    dataGridView1.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
                    index = hti.RowIndex;
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
                bSelected = true;
                Close();
            }
            Cursor = Cursors.Default;
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            initial();
        }
    }
}
