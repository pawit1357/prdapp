﻿namespace Rakuten
{
    partial class FrmTimeLine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label15 = new System.Windows.Forms.Label();
            this.DTP_SERVICE_DATE = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TXT_Q = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CMD_DELETE = new System.Windows.Forms.Button();
            this.CMD_CHECKOUT = new System.Windows.Forms.Button();
            this.CMD_CHECKIN = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(73, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 14);
            this.label15.TabIndex = 130;
            this.label15.Text = "วัน";
            // 
            // DTP_SERVICE_DATE
            // 
            this.DTP_SERVICE_DATE.CustomFormat = "dd/MM/yyyy HH:mm";
            this.DTP_SERVICE_DATE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTP_SERVICE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTP_SERVICE_DATE.Location = new System.Drawing.Point(99, 19);
            this.DTP_SERVICE_DATE.Name = "DTP_SERVICE_DATE";
            this.DTP_SERVICE_DATE.Size = new System.Drawing.Size(163, 26);
            this.DTP_SERVICE_DATE.TabIndex = 1;
            this.DTP_SERVICE_DATE.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TXT_Q);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.DTP_SERVICE_DATE);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.CMD_DELETE);
            this.groupBox1.Controls.Add(this.CMD_CHECKOUT);
            this.groupBox1.Controls.Add(this.CMD_CHECKIN);
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(288, 101);
            this.groupBox1.TabIndex = 131;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "วัน/เวลา";
            // 
            // TXT_Q
            // 
            this.TXT_Q.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_Q.Location = new System.Drawing.Point(201, 53);
            this.TXT_Q.Name = "TXT_Q";
            this.TXT_Q.Size = new System.Drawing.Size(38, 33);
            this.TXT_Q.TabIndex = 135;
            this.TXT_Q.Text = "1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Rakuten.Properties.Resources.stopwatch_run;
            this.pictureBox1.Location = new System.Drawing.Point(6, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(63, 75);
            this.pictureBox1.TabIndex = 134;
            this.pictureBox1.TabStop = false;
            // 
            // CMD_DELETE
            // 
            this.CMD_DELETE.Image = global::Rakuten.Properties.Resources.stopwatch_stop;
            this.CMD_DELETE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CMD_DELETE.Location = new System.Drawing.Point(99, 51);
            this.CMD_DELETE.Name = "CMD_DELETE";
            this.CMD_DELETE.Size = new System.Drawing.Size(96, 43);
            this.CMD_DELETE.TabIndex = 133;
            this.CMD_DELETE.Text = "ลบ";
            this.CMD_DELETE.UseVisualStyleBackColor = true;
            this.CMD_DELETE.Click += new System.EventHandler(this.CMD_DELETE_Click);
            // 
            // CMD_CHECKOUT
            // 
            this.CMD_CHECKOUT.Image = global::Rakuten.Properties.Resources.CheckOut;
            this.CMD_CHECKOUT.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CMD_CHECKOUT.Location = new System.Drawing.Point(99, 51);
            this.CMD_CHECKOUT.Name = "CMD_CHECKOUT";
            this.CMD_CHECKOUT.Size = new System.Drawing.Size(96, 43);
            this.CMD_CHECKOUT.TabIndex = 3;
            this.CMD_CHECKOUT.Text = "ออกงาน";
            this.CMD_CHECKOUT.UseVisualStyleBackColor = true;
            this.CMD_CHECKOUT.Click += new System.EventHandler(this.CMD_CHECKOUT_Click);
            // 
            // CMD_CHECKIN
            // 
            this.CMD_CHECKIN.Image = global::Rakuten.Properties.Resources.CheckIn;
            this.CMD_CHECKIN.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CMD_CHECKIN.Location = new System.Drawing.Point(99, 51);
            this.CMD_CHECKIN.Name = "CMD_CHECKIN";
            this.CMD_CHECKIN.Size = new System.Drawing.Size(96, 43);
            this.CMD_CHECKIN.TabIndex = 132;
            this.CMD_CHECKIN.Text = "เข้างาน";
            this.CMD_CHECKIN.UseVisualStyleBackColor = true;
            this.CMD_CHECKIN.Click += new System.EventHandler(this.CMD_CHECKIN_Click);
            // 
            // FrmTimeLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(310, 113);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmTimeLine";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "วัน/เวลา";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker DTP_SERVICE_DATE;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button CMD_CHECKIN;
        private System.Windows.Forms.Button CMD_CHECKOUT;
        private System.Windows.Forms.Button CMD_DELETE;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox TXT_Q;
    }
}