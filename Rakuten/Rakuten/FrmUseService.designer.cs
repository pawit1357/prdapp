﻿namespace Rakuten
{
    partial class FrmUseService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.RD_04 = new System.Windows.Forms.RadioButton();
            this.RD_03 = new System.Windows.Forms.RadioButton();
            this.RD_01 = new System.Windows.Forms.RadioButton();
            this.RD_02 = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.CB_RESERV = new System.Windows.Forms.CheckBox();
            this.label26 = new System.Windows.Forms.Label();
            this.DTP_TIME_E = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.CBO_ROOM = new System.Windows.Forms.ComboBox();
            this.cMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DTP_TIME_S = new System.Windows.Forms.DateTimePicker();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DTP_SERVICE_DATE = new System.Windows.Forms.DateTimePicker();
            this.B_STHERAPIST = new System.Windows.Forms.Button();
            this.B_SROOM = new System.Windows.Forms.Button();
            this.CBO_THERAPIST = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.LSpecialPackage_Detail = new System.Windows.Forms.Label();
            this.CBO_MASSAGE_TYPE = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.B_SMASSAGE_TYPE = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.rUNNINGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dETAILDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hourDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.memberUseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perCourseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moneyUnitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lASTEDITDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYUSERDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fLAGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MassageType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPackageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.L_REMAIN = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.CBO_SCENT = new System.Windows.Forms.ComboBox();
            this.cScentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.B_SSCENT = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.NUD_SCENTCOUNT = new System.Windows.Forms.NumericUpDown();
            this.B_SAVE = new System.Windows.Forms.Button();
            this.B_CANCEL = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TXT_TEL = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.TXT_ADDR = new System.Windows.Forms.TextBox();
            this.CBO_SEX = new System.Windows.Forms.ComboBox();
            this.TXT_SURNAME = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.B_SSEX = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.CBO_NATION = new System.Windows.Forms.ComboBox();
            this.B_SNATION = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.TXT_NAME = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.ND_AGE = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CBO_TYPE = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TXT_MEMBERID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.B_SMEMBER = new System.Windows.Forms.Button();
            this.B_STYPE = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TXT_REMARK = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.L_REMAIN1 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.CBO_SCENT1 = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.NUD_SCENTCOUNT1 = new System.Windows.Forms.NumericUpDown();
            this.B_SSCENT1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeBindingSource)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMasterBindingSource)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cPackageBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cScentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_SCENTCOUNT)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ND_AGE)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_SCENTCOUNT1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // cEmployeeBindingSource
            // 
            this.cEmployeeBindingSource.DataSource = typeof(Rakuten.Structure.CEmployee);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.RD_04);
            this.groupBox6.Controls.Add(this.RD_03);
            this.groupBox6.Controls.Add(this.RD_01);
            this.groupBox6.Controls.Add(this.RD_02);
            this.groupBox6.Location = new System.Drawing.Point(13, 332);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(686, 57);
            this.groupBox6.TabIndex = 131;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "ประเภทรายการ";
            // 
            // RD_04
            // 
            this.RD_04.AutoSize = true;
            this.RD_04.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RD_04.Location = new System.Drawing.Point(250, 23);
            this.RD_04.Name = "RD_04";
            this.RD_04.Size = new System.Drawing.Size(89, 17);
            this.RD_04.TabIndex = 19;
            this.RD_04.Text = "ยกเลิกรายการ";
            this.RD_04.UseVisualStyleBackColor = true;
            this.RD_04.Visible = false;
            // 
            // RD_03
            // 
            this.RD_03.AutoSize = true;
            this.RD_03.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RD_03.Location = new System.Drawing.Point(129, 23);
            this.RD_03.Name = "RD_03";
            this.RD_03.Size = new System.Drawing.Size(115, 17);
            this.RD_03.TabIndex = 18;
            this.RD_03.Text = "เปลี่ยนแปลงรายการ";
            this.RD_03.UseVisualStyleBackColor = true;
            this.RD_03.Visible = false;
            // 
            // RD_01
            // 
            this.RD_01.AutoSize = true;
            this.RD_01.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RD_01.Location = new System.Drawing.Point(80, 23);
            this.RD_01.Name = "RD_01";
            this.RD_01.Size = new System.Drawing.Size(43, 17);
            this.RD_01.TabIndex = 15;
            this.RD_01.Text = "จอง";
            this.RD_01.UseVisualStyleBackColor = true;
            // 
            // RD_02
            // 
            this.RD_02.AutoSize = true;
            this.RD_02.Checked = true;
            this.RD_02.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RD_02.Location = new System.Drawing.Point(28, 23);
            this.RD_02.Name = "RD_02";
            this.RD_02.Size = new System.Drawing.Size(46, 17);
            this.RD_02.TabIndex = 17;
            this.RD_02.TabStop = true;
            this.RD_02.Text = "ปกติ";
            this.RD_02.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.CB_RESERV);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.DTP_TIME_E);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.CBO_ROOM);
            this.groupBox5.Controls.Add(this.DTP_TIME_S);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.DTP_SERVICE_DATE);
            this.groupBox5.Controls.Add(this.B_STHERAPIST);
            this.groupBox5.Controls.Add(this.B_SROOM);
            this.groupBox5.Controls.Add(this.CBO_THERAPIST);
            this.groupBox5.Location = new System.Drawing.Point(13, 16);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(687, 89);
            this.groupBox5.TabIndex = 130;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "วันเวลา";
            // 
            // CB_RESERV
            // 
            this.CB_RESERV.AutoSize = true;
            this.CB_RESERV.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CB_RESERV.ForeColor = System.Drawing.Color.Maroon;
            this.CB_RESERV.Location = new System.Drawing.Point(613, 57);
            this.CB_RESERV.Name = "CB_RESERV";
            this.CB_RESERV.Size = new System.Drawing.Size(46, 18);
            this.CB_RESERV.TabIndex = 134;
            this.CB_RESERV.Text = "จอง";
            this.CB_RESERV.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label26.Location = new System.Drawing.Point(164, 58);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(11, 14);
            this.label26.TabIndex = 128;
            this.label26.Text = "-";
            // 
            // DTP_TIME_E
            // 
            this.DTP_TIME_E.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTP_TIME_E.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.DTP_TIME_E.Location = new System.Drawing.Point(181, 51);
            this.DTP_TIME_E.Name = "DTP_TIME_E";
            this.DTP_TIME_E.ShowUpDown = true;
            this.DTP_TIME_E.Size = new System.Drawing.Size(63, 26);
            this.DTP_TIME_E.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(16, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 14);
            this.label15.TabIndex = 126;
            this.label15.Text = "วันที่ใช้บริการ";
            // 
            // CBO_ROOM
            // 
            this.CBO_ROOM.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_ROOM.DataSource = this.cMasterBindingSource;
            this.CBO_ROOM.DisplayMember = "DETAIL";
            this.CBO_ROOM.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_ROOM.FormattingEnabled = true;
            this.CBO_ROOM.Location = new System.Drawing.Point(389, 20);
            this.CBO_ROOM.Name = "CBO_ROOM";
            this.CBO_ROOM.Size = new System.Drawing.Size(182, 26);
            this.CBO_ROOM.TabIndex = 2;
            this.CBO_ROOM.ValueMember = "ID";
            // 
            // cMasterBindingSource
            // 
            this.cMasterBindingSource.DataSource = typeof(Rakuten.Structure.CMaster);
            // 
            // DTP_TIME_S
            // 
            this.DTP_TIME_S.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTP_TIME_S.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.DTP_TIME_S.Location = new System.Drawing.Point(95, 51);
            this.DTP_TIME_S.Name = "DTP_TIME_S";
            this.DTP_TIME_S.ShowUpDown = true;
            this.DTP_TIME_S.Size = new System.Drawing.Size(63, 26);
            this.DTP_TIME_S.TabIndex = 3;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label22.Location = new System.Drawing.Point(23, 58);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(66, 14);
            this.label22.TabIndex = 95;
            this.label22.Text = "เวลาเริ่มนวด";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label23.Location = new System.Drawing.Point(355, 26);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(28, 14);
            this.label23.TabIndex = 98;
            this.label23.Text = "ห้อง";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(315, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 14);
            this.label1.TabIndex = 67;
            this.label1.Text = "หนักงานนวด";
            // 
            // DTP_SERVICE_DATE
            // 
            this.DTP_SERVICE_DATE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTP_SERVICE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTP_SERVICE_DATE.Location = new System.Drawing.Point(95, 19);
            this.DTP_SERVICE_DATE.Name = "DTP_SERVICE_DATE";
            this.DTP_SERVICE_DATE.Size = new System.Drawing.Size(149, 26);
            this.DTP_SERVICE_DATE.TabIndex = 1;
            // 
            // B_STHERAPIST
            // 
            this.B_STHERAPIST.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_STHERAPIST.Location = new System.Drawing.Point(577, 52);
            this.B_STHERAPIST.Name = "B_STHERAPIST";
            this.B_STHERAPIST.Size = new System.Drawing.Size(30, 26);
            this.B_STHERAPIST.TabIndex = 66;
            this.B_STHERAPIST.Text = "...";
            this.B_STHERAPIST.UseVisualStyleBackColor = true;
            this.B_STHERAPIST.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // B_SROOM
            // 
            this.B_SROOM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SROOM.Location = new System.Drawing.Point(577, 20);
            this.B_SROOM.Name = "B_SROOM";
            this.B_SROOM.Size = new System.Drawing.Size(30, 26);
            this.B_SROOM.TabIndex = 97;
            this.B_SROOM.Text = "...";
            this.B_SROOM.UseVisualStyleBackColor = true;
            this.B_SROOM.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // CBO_THERAPIST
            // 
            this.CBO_THERAPIST.DataSource = this.cEmployeeBindingSource;
            this.CBO_THERAPIST.DisplayMember = "Name";
            this.CBO_THERAPIST.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_THERAPIST.FormattingEnabled = true;
            this.CBO_THERAPIST.Location = new System.Drawing.Point(389, 52);
            this.CBO_THERAPIST.Name = "CBO_THERAPIST";
            this.CBO_THERAPIST.Size = new System.Drawing.Size(182, 26);
            this.CBO_THERAPIST.TabIndex = 5;
            this.CBO_THERAPIST.ValueMember = "ID";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.LSpecialPackage_Detail);
            this.groupBox4.Controls.Add(this.CBO_MASSAGE_TYPE);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.B_SMASSAGE_TYPE);
            this.groupBox4.Controls.Add(this.dataGridView2);
            this.groupBox4.Location = new System.Drawing.Point(8, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(694, 429);
            this.groupBox4.TabIndex = 129;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ข้อมูลบริการ";
            // 
            // LSpecialPackage_Detail
            // 
            this.LSpecialPackage_Detail.AutoSize = true;
            this.LSpecialPackage_Detail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LSpecialPackage_Detail.ForeColor = System.Drawing.Color.DarkGreen;
            this.LSpecialPackage_Detail.Location = new System.Drawing.Point(319, 25);
            this.LSpecialPackage_Detail.Name = "LSpecialPackage_Detail";
            this.LSpecialPackage_Detail.Size = new System.Drawing.Size(0, 13);
            this.LSpecialPackage_Detail.TabIndex = 112;
            // 
            // CBO_MASSAGE_TYPE
            // 
            this.CBO_MASSAGE_TYPE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_MASSAGE_TYPE.DataSource = this.cMasterBindingSource;
            this.CBO_MASSAGE_TYPE.DisplayMember = "DETAIL";
            this.CBO_MASSAGE_TYPE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_MASSAGE_TYPE.FormattingEnabled = true;
            this.CBO_MASSAGE_TYPE.Location = new System.Drawing.Point(95, 19);
            this.CBO_MASSAGE_TYPE.Name = "CBO_MASSAGE_TYPE";
            this.CBO_MASSAGE_TYPE.Size = new System.Drawing.Size(182, 26);
            this.CBO_MASSAGE_TYPE.TabIndex = 1;
            this.CBO_MASSAGE_TYPE.ValueMember = "ID";
            this.CBO_MASSAGE_TYPE.SelectedIndexChanged += new System.EventHandler(this.CBO_MASSAGE_TYPE_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(5, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 14);
            this.label5.TabIndex = 89;
            this.label5.Text = "ประเภทการนวด";
            // 
            // B_SMASSAGE_TYPE
            // 
            this.B_SMASSAGE_TYPE.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SMASSAGE_TYPE.Location = new System.Drawing.Point(283, 19);
            this.B_SMASSAGE_TYPE.Name = "B_SMASSAGE_TYPE";
            this.B_SMASSAGE_TYPE.Size = new System.Drawing.Size(30, 26);
            this.B_SMASSAGE_TYPE.TabIndex = 88;
            this.B_SMASSAGE_TYPE.Text = "...";
            this.B_SMASSAGE_TYPE.UseVisualStyleBackColor = true;
            this.B_SMASSAGE_TYPE.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rUNNINGDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn,
            this.dETAILDataGridViewTextBoxColumn,
            this.hourDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.memberUseDataGridViewTextBoxColumn,
            this.perCourseDataGridViewTextBoxColumn,
            this.moneyUnitDataGridViewTextBoxColumn,
            this.kEYDATEDataGridViewTextBoxColumn,
            this.lASTEDITDataGridViewTextBoxColumn,
            this.kEYUSERDataGridViewTextBoxColumn,
            this.fLAGDataGridViewTextBoxColumn,
            this.startDateDataGridViewTextBoxColumn,
            this.endDateDataGridViewTextBoxColumn,
            this.remarkDataGridViewTextBoxColumn,
            this.MassageType});
            this.dataGridView2.DataSource = this.cPackageBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(8, 51);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(680, 371);
            this.dataGridView2.TabIndex = 111;
            this.dataGridView2.TabStop = false;
            this.dataGridView2.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellDoubleClick);
            this.dataGridView2.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView2_CellFormatting);
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // rUNNINGDataGridViewTextBoxColumn
            // 
            this.rUNNINGDataGridViewTextBoxColumn.DataPropertyName = "RUNNING";
            this.rUNNINGDataGridViewTextBoxColumn.HeaderText = "ลำดับ";
            this.rUNNINGDataGridViewTextBoxColumn.Name = "rUNNINGDataGridViewTextBoxColumn";
            this.rUNNINGDataGridViewTextBoxColumn.ReadOnly = true;
            this.rUNNINGDataGridViewTextBoxColumn.Width = 60;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // dETAILDataGridViewTextBoxColumn
            // 
            this.dETAILDataGridViewTextBoxColumn.DataPropertyName = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn.HeaderText = "รายละเอียด";
            this.dETAILDataGridViewTextBoxColumn.Name = "dETAILDataGridViewTextBoxColumn";
            this.dETAILDataGridViewTextBoxColumn.ReadOnly = true;
            this.dETAILDataGridViewTextBoxColumn.Width = 250;
            // 
            // hourDataGridViewTextBoxColumn
            // 
            this.hourDataGridViewTextBoxColumn.DataPropertyName = "Hour";
            this.hourDataGridViewTextBoxColumn.HeaderText = "ชม.";
            this.hourDataGridViewTextBoxColumn.Name = "hourDataGridViewTextBoxColumn";
            this.hourDataGridViewTextBoxColumn.ReadOnly = true;
            this.hourDataGridViewTextBoxColumn.Width = 60;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "ราคา";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            this.priceDataGridViewTextBoxColumn.Width = 60;
            // 
            // memberUseDataGridViewTextBoxColumn
            // 
            this.memberUseDataGridViewTextBoxColumn.DataPropertyName = "MemberUse";
            this.memberUseDataGridViewTextBoxColumn.HeaderText = "ใช้ไป";
            this.memberUseDataGridViewTextBoxColumn.Name = "memberUseDataGridViewTextBoxColumn";
            this.memberUseDataGridViewTextBoxColumn.ReadOnly = true;
            this.memberUseDataGridViewTextBoxColumn.Width = 60;
            // 
            // perCourseDataGridViewTextBoxColumn
            // 
            this.perCourseDataGridViewTextBoxColumn.DataPropertyName = "PerCourse";
            this.perCourseDataGridViewTextBoxColumn.HeaderText = "ทั้งหมด";
            this.perCourseDataGridViewTextBoxColumn.Name = "perCourseDataGridViewTextBoxColumn";
            this.perCourseDataGridViewTextBoxColumn.ReadOnly = true;
            this.perCourseDataGridViewTextBoxColumn.Width = 70;
            // 
            // moneyUnitDataGridViewTextBoxColumn
            // 
            this.moneyUnitDataGridViewTextBoxColumn.DataPropertyName = "MoneyUnit";
            this.moneyUnitDataGridViewTextBoxColumn.HeaderText = "MoneyUnit";
            this.moneyUnitDataGridViewTextBoxColumn.Name = "moneyUnitDataGridViewTextBoxColumn";
            this.moneyUnitDataGridViewTextBoxColumn.ReadOnly = true;
            this.moneyUnitDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYDATEDataGridViewTextBoxColumn
            // 
            this.kEYDATEDataGridViewTextBoxColumn.DataPropertyName = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.HeaderText = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.Name = "kEYDATEDataGridViewTextBoxColumn";
            this.kEYDATEDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYDATEDataGridViewTextBoxColumn.Visible = false;
            // 
            // lASTEDITDataGridViewTextBoxColumn
            // 
            this.lASTEDITDataGridViewTextBoxColumn.DataPropertyName = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.HeaderText = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.Name = "lASTEDITDataGridViewTextBoxColumn";
            this.lASTEDITDataGridViewTextBoxColumn.ReadOnly = true;
            this.lASTEDITDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYUSERDataGridViewTextBoxColumn
            // 
            this.kEYUSERDataGridViewTextBoxColumn.DataPropertyName = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.HeaderText = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.Name = "kEYUSERDataGridViewTextBoxColumn";
            this.kEYUSERDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYUSERDataGridViewTextBoxColumn.Visible = false;
            // 
            // fLAGDataGridViewTextBoxColumn
            // 
            this.fLAGDataGridViewTextBoxColumn.DataPropertyName = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.HeaderText = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.Name = "fLAGDataGridViewTextBoxColumn";
            this.fLAGDataGridViewTextBoxColumn.ReadOnly = true;
            this.fLAGDataGridViewTextBoxColumn.Visible = false;
            // 
            // startDateDataGridViewTextBoxColumn
            // 
            this.startDateDataGridViewTextBoxColumn.DataPropertyName = "StartDate";
            this.startDateDataGridViewTextBoxColumn.HeaderText = "StartDate";
            this.startDateDataGridViewTextBoxColumn.Name = "startDateDataGridViewTextBoxColumn";
            this.startDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.startDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // endDateDataGridViewTextBoxColumn
            // 
            this.endDateDataGridViewTextBoxColumn.DataPropertyName = "EndDate";
            this.endDateDataGridViewTextBoxColumn.HeaderText = "EndDate";
            this.endDateDataGridViewTextBoxColumn.Name = "endDateDataGridViewTextBoxColumn";
            this.endDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.endDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // remarkDataGridViewTextBoxColumn
            // 
            this.remarkDataGridViewTextBoxColumn.DataPropertyName = "Remark";
            this.remarkDataGridViewTextBoxColumn.HeaderText = "Remark";
            this.remarkDataGridViewTextBoxColumn.Name = "remarkDataGridViewTextBoxColumn";
            this.remarkDataGridViewTextBoxColumn.ReadOnly = true;
            this.remarkDataGridViewTextBoxColumn.Visible = false;
            // 
            // MassageType
            // 
            this.MassageType.DataPropertyName = "MassageType";
            this.MassageType.HeaderText = "MassageType";
            this.MassageType.Name = "MassageType";
            this.MassageType.ReadOnly = true;
            this.MassageType.Visible = false;
            // 
            // cPackageBindingSource
            // 
            this.cPackageBindingSource.DataSource = typeof(Rakuten.Structure.CPackage);
            // 
            // L_REMAIN
            // 
            this.L_REMAIN.AutoSize = true;
            this.L_REMAIN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.L_REMAIN.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.L_REMAIN.Location = new System.Drawing.Point(300, 65);
            this.L_REMAIN.Name = "L_REMAIN";
            this.L_REMAIN.Size = new System.Drawing.Size(16, 16);
            this.L_REMAIN.TabIndex = 127;
            this.L_REMAIN.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label25.Location = new System.Drawing.Point(282, 65);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(12, 14);
            this.label25.TabIndex = 126;
            this.label25.Text = "/";
            // 
            // CBO_SCENT
            // 
            this.CBO_SCENT.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cScentBindingSource, "ID", true));
            this.CBO_SCENT.DataSource = this.cScentBindingSource;
            this.CBO_SCENT.DisplayMember = "DETAIL";
            this.CBO_SCENT.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_SCENT.FormattingEnabled = true;
            this.CBO_SCENT.Location = new System.Drawing.Point(94, 27);
            this.CBO_SCENT.Name = "CBO_SCENT";
            this.CBO_SCENT.Size = new System.Drawing.Size(182, 26);
            this.CBO_SCENT.TabIndex = 6;
            this.CBO_SCENT.ValueMember = "ID";
            this.CBO_SCENT.SelectedIndexChanged += new System.EventHandler(this.CBO_SCENT_SelectedIndexChanged);
            // 
            // cScentBindingSource
            // 
            this.cScentBindingSource.DataSource = typeof(Rakuten.Structure.CScent);
            // 
            // B_SSCENT
            // 
            this.B_SSCENT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SSCENT.Location = new System.Drawing.Point(282, 27);
            this.B_SSCENT.Name = "B_SSCENT";
            this.B_SSCENT.Size = new System.Drawing.Size(30, 26);
            this.B_SSCENT.TabIndex = 124;
            this.B_SSCENT.Text = "...";
            this.B_SSCENT.UseVisualStyleBackColor = true;
            this.B_SSCENT.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(32, 33);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 14);
            this.label16.TabIndex = 125;
            this.label16.Text = "กลิ่นน้ำมัน";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.Location = new System.Drawing.Point(48, 65);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 14);
            this.label17.TabIndex = 122;
            this.label17.Text = "จำนวน";
            // 
            // NUD_SCENTCOUNT
            // 
            this.NUD_SCENTCOUNT.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.NUD_SCENTCOUNT.Location = new System.Drawing.Point(94, 59);
            this.NUD_SCENTCOUNT.Name = "NUD_SCENTCOUNT";
            this.NUD_SCENTCOUNT.Size = new System.Drawing.Size(182, 26);
            this.NUD_SCENTCOUNT.TabIndex = 7;
            this.NUD_SCENTCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NUD_SCENTCOUNT.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // B_SAVE
            // 
            this.B_SAVE.Image = global::Rakuten.Properties.Resources.disk_blue_ok;
            this.B_SAVE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.B_SAVE.Location = new System.Drawing.Point(526, 395);
            this.B_SAVE.Name = "B_SAVE";
            this.B_SAVE.Size = new System.Drawing.Size(81, 36);
            this.B_SAVE.TabIndex = 9;
            this.B_SAVE.Text = "บันทึก";
            this.B_SAVE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.B_SAVE.UseVisualStyleBackColor = true;
            this.B_SAVE.Click += new System.EventHandler(this.B_SAVE_Click);
            this.B_SAVE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // B_CANCEL
            // 
            this.B_CANCEL.Image = global::Rakuten.Properties.Resources.redo;
            this.B_CANCEL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.B_CANCEL.Location = new System.Drawing.Point(613, 395);
            this.B_CANCEL.Name = "B_CANCEL";
            this.B_CANCEL.Size = new System.Drawing.Size(70, 36);
            this.B_CANCEL.TabIndex = 10;
            this.B_CANCEL.Text = "ยกเลิก";
            this.B_CANCEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.B_CANCEL.UseVisualStyleBackColor = true;
            this.B_CANCEL.Click += new System.EventHandler(this.B_CANCEL_Click);
            this.B_CANCEL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TXT_TEL);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.TXT_ADDR);
            this.groupBox3.Controls.Add(this.CBO_SEX);
            this.groupBox3.Controls.Add(this.TXT_SURNAME);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.B_SSEX);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.CBO_NATION);
            this.groupBox3.Controls.Add(this.B_SNATION);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.TXT_NAME);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.ND_AGE);
            this.groupBox3.Location = new System.Drawing.Point(6, 98);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(696, 337);
            this.groupBox3.TabIndex = 128;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ข้อมูลลูกค้า";
            // 
            // TXT_TEL
            // 
            this.TXT_TEL.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_TEL.Location = new System.Drawing.Point(393, 83);
            this.TXT_TEL.Name = "TXT_TEL";
            this.TXT_TEL.Size = new System.Drawing.Size(182, 26);
            this.TXT_TEL.TabIndex = 8;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label28.Location = new System.Drawing.Point(312, 89);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(74, 14);
            this.label28.TabIndex = 127;
            this.label28.Text = "เบอร์โทรศัพท์";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label27.Location = new System.Drawing.Point(60, 121);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(30, 14);
            this.label27.TabIndex = 125;
            this.label27.Text = "ที่อยู่";
            // 
            // TXT_ADDR
            // 
            this.TXT_ADDR.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_ADDR.Location = new System.Drawing.Point(96, 115);
            this.TXT_ADDR.Multiline = true;
            this.TXT_ADDR.Name = "TXT_ADDR";
            this.TXT_ADDR.Size = new System.Drawing.Size(479, 61);
            this.TXT_ADDR.TabIndex = 9;
            // 
            // CBO_SEX
            // 
            this.CBO_SEX.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_SEX.DataSource = this.cMasterBindingSource;
            this.CBO_SEX.DisplayMember = "DETAIL";
            this.CBO_SEX.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_SEX.FormattingEnabled = true;
            this.CBO_SEX.Location = new System.Drawing.Point(95, 19);
            this.CBO_SEX.Name = "CBO_SEX";
            this.CBO_SEX.Size = new System.Drawing.Size(182, 26);
            this.CBO_SEX.TabIndex = 3;
            this.CBO_SEX.ValueMember = "ID";
            // 
            // TXT_SURNAME
            // 
            this.TXT_SURNAME.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_SURNAME.Location = new System.Drawing.Point(95, 51);
            this.TXT_SURNAME.Name = "TXT_SURNAME";
            this.TXT_SURNAME.Size = new System.Drawing.Size(182, 26);
            this.TXT_SURNAME.TabIndex = 5;
            this.TXT_SURNAME.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label21.Location = new System.Drawing.Point(41, 57);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 14);
            this.label21.TabIndex = 48;
            this.label21.Text = "นามสกุล";
            // 
            // B_SSEX
            // 
            this.B_SSEX.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SSEX.Location = new System.Drawing.Point(288, 19);
            this.B_SSEX.Name = "B_SSEX";
            this.B_SSEX.Size = new System.Drawing.Size(30, 26);
            this.B_SSEX.TabIndex = 117;
            this.B_SSEX.Text = "...";
            this.B_SSEX.UseVisualStyleBackColor = true;
            this.B_SSEX.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(64, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 14);
            this.label7.TabIndex = 120;
            this.label7.Text = "เพศ";
            // 
            // CBO_NATION
            // 
            this.CBO_NATION.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_NATION.DataSource = this.cMasterBindingSource;
            this.CBO_NATION.DisplayMember = "DETAIL";
            this.CBO_NATION.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_NATION.FormattingEnabled = true;
            this.CBO_NATION.Location = new System.Drawing.Point(393, 51);
            this.CBO_NATION.Name = "CBO_NATION";
            this.CBO_NATION.Size = new System.Drawing.Size(182, 26);
            this.CBO_NATION.TabIndex = 6;
            this.CBO_NATION.ValueMember = "ID";
            // 
            // B_SNATION
            // 
            this.B_SNATION.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SNATION.Location = new System.Drawing.Point(586, 51);
            this.B_SNATION.Name = "B_SNATION";
            this.B_SNATION.Size = new System.Drawing.Size(30, 26);
            this.B_SNATION.TabIndex = 122;
            this.B_SNATION.Text = "...";
            this.B_SNATION.UseVisualStyleBackColor = true;
            this.B_SNATION.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label20.Location = new System.Drawing.Point(362, 56);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(28, 14);
            this.label20.TabIndex = 123;
            this.label20.Text = "ชาติ";
            // 
            // TXT_NAME
            // 
            this.TXT_NAME.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_NAME.Location = new System.Drawing.Point(393, 18);
            this.TXT_NAME.Name = "TXT_NAME";
            this.TXT_NAME.Size = new System.Drawing.Size(182, 26);
            this.TXT_NAME.TabIndex = 4;
            this.TXT_NAME.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.Location = new System.Drawing.Point(364, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 14);
            this.label18.TabIndex = 12;
            this.label18.Text = "ชื่อ";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label38.Location = new System.Drawing.Point(61, 89);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(27, 14);
            this.label38.TabIndex = 119;
            this.label38.Text = "อายุ";
            // 
            // ND_AGE
            // 
            this.ND_AGE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ND_AGE.Location = new System.Drawing.Point(95, 83);
            this.ND_AGE.Name = "ND_AGE";
            this.ND_AGE.Size = new System.Drawing.Size(182, 26);
            this.ND_AGE.TabIndex = 7;
            this.ND_AGE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ND_AGE.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CBO_TYPE);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.TXT_MEMBERID);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.B_SMEMBER);
            this.groupBox2.Controls.Add(this.B_STYPE);
            this.groupBox2.Location = new System.Drawing.Point(6, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(696, 85);
            this.groupBox2.TabIndex = 127;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ประเภทสมาชิก";
            // 
            // CBO_TYPE
            // 
            this.CBO_TYPE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_TYPE.DataSource = this.cMasterBindingSource;
            this.CBO_TYPE.DisplayMember = "DETAIL";
            this.CBO_TYPE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_TYPE.FormattingEnabled = true;
            this.CBO_TYPE.Location = new System.Drawing.Point(95, 19);
            this.CBO_TYPE.Name = "CBO_TYPE";
            this.CBO_TYPE.Size = new System.Drawing.Size(182, 26);
            this.CBO_TYPE.TabIndex = 1;
            this.CBO_TYPE.ValueMember = "ID";
            this.CBO_TYPE.SelectedIndexChanged += new System.EventHandler(this.CBO_TYPE_SelectedIndexChanged);
            this.CBO_TYPE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(31, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "รหัสมาชิก";
            // 
            // TXT_MEMBERID
            // 
            this.TXT_MEMBERID.BackColor = System.Drawing.SystemColors.MenuText;
            this.TXT_MEMBERID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_MEMBERID.ForeColor = System.Drawing.Color.Gold;
            this.TXT_MEMBERID.Location = new System.Drawing.Point(95, 51);
            this.TXT_MEMBERID.Name = "TXT_MEMBERID";
            this.TXT_MEMBERID.Size = new System.Drawing.Size(182, 23);
            this.TXT_MEMBERID.TabIndex = 2;
            this.TXT_MEMBERID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TXT_MEMBERID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_KeyDown);
            this.TXT_MEMBERID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckIsNumber_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(44, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 14);
            this.label4.TabIndex = 29;
            this.label4.Text = "ประเภท";
            // 
            // B_SMEMBER
            // 
            this.B_SMEMBER.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SMEMBER.Location = new System.Drawing.Point(288, 49);
            this.B_SMEMBER.Name = "B_SMEMBER";
            this.B_SMEMBER.Size = new System.Drawing.Size(30, 26);
            this.B_SMEMBER.TabIndex = 115;
            this.B_SMEMBER.Text = "...";
            this.B_SMEMBER.UseVisualStyleBackColor = true;
            this.B_SMEMBER.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // B_STYPE
            // 
            this.B_STYPE.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_STYPE.Location = new System.Drawing.Point(288, 19);
            this.B_STYPE.Name = "B_STYPE";
            this.B_STYPE.Size = new System.Drawing.Size(30, 26);
            this.B_STYPE.TabIndex = 124;
            this.B_STYPE.Text = "...";
            this.B_STYPE.UseVisualStyleBackColor = true;
            this.B_STYPE.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TXT_REMARK);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(14, 218);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(686, 108);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "หมายเหตุกรณีมีการเปลี่ยนแปลงรายการ";
            // 
            // TXT_REMARK
            // 
            this.TXT_REMARK.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_REMARK.Location = new System.Drawing.Point(77, 24);
            this.TXT_REMARK.Multiline = true;
            this.TXT_REMARK.Name = "TXT_REMARK";
            this.TXT_REMARK.Size = new System.Drawing.Size(592, 78);
            this.TXT_REMARK.TabIndex = 8;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label24.Location = new System.Drawing.Point(17, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 14);
            this.label24.TabIndex = 126;
            this.label24.Text = "หมายเหตุ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(56, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(401, 14);
            this.label2.TabIndex = 9;
            this.label2.Text = "การลบ เลือกทรัพย์สินที่ต้องการลบจากหน้า (แสดงรายการค้นหา) แล้ว (กดปุ่มลบ)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(56, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(195, 14);
            this.label8.TabIndex = 8;
            this.label8.Text = "การเพิ่ม พิมพ์ชื่อทรัพย์สิน แล้ว (กดเพิ่ม)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.Location = new System.Drawing.Point(24, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 14);
            this.label9.TabIndex = 7;
            this.label9.Text = "หมายเหตุ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(24, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 14);
            this.label10.TabIndex = 5;
            this.label10.Text = "ชื่อทรัพย์สิน";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(389, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "ลบ";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(94, 63);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(370, 59);
            this.textBox1.TabIndex = 4;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "XXXX";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(308, 128);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "เพิ่ม";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(56, 209);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(401, 14);
            this.label11.TabIndex = 9;
            this.label11.Text = "การลบ เลือกทรัพย์สินที่ต้องการลบจากหน้า (แสดงรายการค้นหา) แล้ว (กดปุ่มลบ)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.Location = new System.Drawing.Point(56, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(195, 14);
            this.label12.TabIndex = 8;
            this.label12.Text = "การเพิ่ม พิมพ์ชื่อทรัพย์สิน แล้ว (กดเพิ่ม)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(24, 161);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 14);
            this.label13.TabIndex = 7;
            this.label13.Text = "หมายเหตุ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.Location = new System.Drawing.Point(24, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 14);
            this.label14.TabIndex = 5;
            this.label14.Text = "ชื่อทรัพย์สิน";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(389, 128);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "ลบ";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(94, 63);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(370, 59);
            this.textBox2.TabIndex = 4;
            this.textBox2.TabStop = false;
            this.textBox2.Text = "XXXX";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(308, 128);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "เพิ่ม";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::Rakuten.Properties.Resources.product;
            this.pictureBox1.Location = new System.Drawing.Point(12, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(52, 50);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 57);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(716, 467);
            this.tabControl1.TabIndex = 114;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(708, 441);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ข้อมูลลูกค้า";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(708, 441);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ข้อมูลบริการ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Controls.Add(this.B_SAVE);
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.B_CANCEL);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(708, 441);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "วันที่ใช้บริการ";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.L_REMAIN1);
            this.groupBox7.Controls.Add(this.label29);
            this.groupBox7.Controls.Add(this.CBO_SCENT1);
            this.groupBox7.Controls.Add(this.label30);
            this.groupBox7.Controls.Add(this.label31);
            this.groupBox7.Controls.Add(this.NUD_SCENTCOUNT1);
            this.groupBox7.Controls.Add(this.B_SSCENT1);
            this.groupBox7.Controls.Add(this.L_REMAIN);
            this.groupBox7.Controls.Add(this.label25);
            this.groupBox7.Controls.Add(this.CBO_SCENT);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.NUD_SCENTCOUNT);
            this.groupBox7.Controls.Add(this.B_SSCENT);
            this.groupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox7.Location = new System.Drawing.Point(13, 111);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(687, 101);
            this.groupBox7.TabIndex = 132;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "ข้อมูลน้ำหอม";
            // 
            // L_REMAIN1
            // 
            this.L_REMAIN1.AutoSize = true;
            this.L_REMAIN1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.L_REMAIN1.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.L_REMAIN1.Location = new System.Drawing.Point(595, 65);
            this.L_REMAIN1.Name = "L_REMAIN1";
            this.L_REMAIN1.Size = new System.Drawing.Size(16, 16);
            this.L_REMAIN1.TabIndex = 134;
            this.L_REMAIN1.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label29.Location = new System.Drawing.Point(577, 65);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(12, 14);
            this.label29.TabIndex = 133;
            this.label29.Text = "/";
            // 
            // CBO_SCENT1
            // 
            this.CBO_SCENT1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cScentBindingSource, "ID", true));
            this.CBO_SCENT1.DataSource = this.cScentBindingSource;
            this.CBO_SCENT1.DisplayMember = "DETAIL";
            this.CBO_SCENT1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_SCENT1.FormattingEnabled = true;
            this.CBO_SCENT1.Location = new System.Drawing.Point(389, 27);
            this.CBO_SCENT1.Name = "CBO_SCENT1";
            this.CBO_SCENT1.Size = new System.Drawing.Size(182, 26);
            this.CBO_SCENT1.TabIndex = 128;
            this.CBO_SCENT1.ValueMember = "ID";
            this.CBO_SCENT1.SelectedIndexChanged += new System.EventHandler(this.CBO_SCENT1_SelectedIndexChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label30.Location = new System.Drawing.Point(343, 65);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(40, 14);
            this.label30.TabIndex = 130;
            this.label30.Text = "จำนวน";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label31.Location = new System.Drawing.Point(327, 33);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(55, 14);
            this.label31.TabIndex = 132;
            this.label31.Text = "กลิ่นน้ำมัน";
            // 
            // NUD_SCENTCOUNT1
            // 
            this.NUD_SCENTCOUNT1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.NUD_SCENTCOUNT1.Location = new System.Drawing.Point(389, 59);
            this.NUD_SCENTCOUNT1.Name = "NUD_SCENTCOUNT1";
            this.NUD_SCENTCOUNT1.Size = new System.Drawing.Size(182, 26);
            this.NUD_SCENTCOUNT1.TabIndex = 129;
            this.NUD_SCENTCOUNT1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // B_SSCENT1
            // 
            this.B_SSCENT1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SSCENT1.Location = new System.Drawing.Point(577, 27);
            this.B_SSCENT1.Name = "B_SSCENT1";
            this.B_SSCENT1.Size = new System.Drawing.Size(30, 26);
            this.B_SSCENT1.TabIndex = 131;
            this.B_SSCENT1.Text = "...";
            this.B_SSCENT1.UseVisualStyleBackColor = true;
            this.B_SSCENT1.Click += new System.EventHandler(this.B_STYPE_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(0, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(739, 50);
            this.pictureBox2.TabIndex = 115;
            this.pictureBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.Green;
            this.label6.Location = new System.Drawing.Point(70, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 25);
            this.label6.TabIndex = 116;
            this.label6.Text = "ทำรายการ";
            // 
            // FrmUseService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(738, 536);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUseService";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ทำรายการ";
            this.Load += new System.EventHandler(this.FrmUseService_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeBindingSource)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMasterBindingSource)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cPackageBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cScentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_SCENTCOUNT)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ND_AGE)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_SCENTCOUNT1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox TXT_NAME;
        private System.Windows.Forms.Button B_SAVE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button B_CANCEL;
        private System.Windows.Forms.TextBox TXT_MEMBERID;
        private System.Windows.Forms.ComboBox CBO_TYPE;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.BindingSource cMasterBindingSource;
        //private System.Windows.Forms.DataGridViewTextBoxColumn rUNNINGDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn surnameDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn nickNameDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn sexDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn birthDayDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn nationDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn mobileDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn dETAILDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn kEYDATEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn lASTEDITDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn kEYUSERDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn fLAGDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource cEmployeeBindingSource;
        private System.Windows.Forms.ComboBox CBO_THERAPIST;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button B_STHERAPIST;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DateTimePicker DTP_TIME_S;
        private System.Windows.Forms.Button B_SROOM;
        private System.Windows.Forms.ComboBox CBO_ROOM;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button B_SMEMBER;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource cPackageBindingSource;
        private System.Windows.Forms.Button B_SSEX;
        private System.Windows.Forms.ComboBox CBO_SEX;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown ND_AGE;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.NumericUpDown NUD_SCENTCOUNT;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox CBO_SCENT;
        private System.Windows.Forms.Button B_SSCENT;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button B_SNATION;
        private System.Windows.Forms.ComboBox CBO_NATION;
        private System.Windows.Forms.Button B_STYPE;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox TXT_REMARK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton RD_01;
        private System.Windows.Forms.RadioButton RD_02;
        private System.Windows.Forms.DateTimePicker DTP_SERVICE_DATE;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TXT_SURNAME;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.BindingSource cScentBindingSource;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DateTimePicker DTP_TIME_E;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox TXT_ADDR;
        private System.Windows.Forms.CheckBox CB_RESERV;
        private System.Windows.Forms.TextBox TXT_TEL;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox CBO_MASSAGE_TYPE;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button B_SMASSAGE_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn rUNNINGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dETAILDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hourDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn memberUseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perCourseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn moneyUnitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lASTEDITDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYUSERDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fLAGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarkDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MassageType;
        private System.Windows.Forms.Label LSpecialPackage_Detail;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label L_REMAIN1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox CBO_SCENT1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.NumericUpDown NUD_SCENTCOUNT1;
        private System.Windows.Forms.Button B_SSCENT1;
        private System.Windows.Forms.Label L_REMAIN;
        private System.Windows.Forms.RadioButton RD_04;
        private System.Windows.Forms.RadioButton RD_03;

    }
}