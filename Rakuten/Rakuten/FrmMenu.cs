﻿using System;
using System.Windows.Forms;
using Rakuten.DAL;
using Rakuten.Utility;
using Rakuten.Structure;
using System.Diagnostics;
// ตั้งค่าเวลาเป็น English (United Kingdom)
namespace Rakuten
{
    public partial class FrmMenu : Form
    {
        public FrmMain main = null;

        public FrmMenu()
        {
            InitializeComponent();
        }
        private void FrmMenu_Load(object sender, EventArgs e)
        {
            //string s1 = ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "SK"));
            //string s2 = ManageLOG.GetHDDSerialNumber("C");
            //string s21 = ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "HDR"));
            //string s3 = ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "CON"));
            //string s4 = Management.isDBExist().ToString();
            //Console.WriteLine("{0}\n,{1}\n,{2}\n,{3}\n,{4}\n", s1, s2, s21, s3, s4);

            if (ManageLOG.Formula(ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "SK"))) &&//ตรวจสอบ Serial Key ว่าถูกหรือไม่
                ManageLOG.GetHDDSerialNumber("C").Equals(ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "HDR"))) &&//ดึง Serial HD มาตรวจสอบว่าตรงกันหรือไม่
               !ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "CON")).Equals("") //&&//ตรวจสอบ connection string
                //Management.isDBExist()//ตรวจสอบว่า มี DB อยุ่หรือไม่
                )
            {
                //Show Login Menu
                frmLogin login = new frmLogin();
                login.ShowDialog();
                if (login.click.Equals("OK"))
                {
                    initial();
                }
            }
            else
            {
                frmSetting setting = new frmSetting();
                setting.ShowDialog();
            }
            if (Authorize.getUser().Equals("")) Application.Exit();//if alt+f4 exit app
        }

        private void TSM_1_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            string name = "";
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem tsm = (ToolStripMenuItem)sender;
                name = tsm.Name;
            }
            if (sender is ToolStripButton)
            {
                ToolStripButton tsb = (ToolStripButton)sender;
                name = tsb.Name;
            }
            FrmSearch search = null;
            switch (name)
            {
                case "TSM_1":
                    search = new FrmSearch(MasterList.MCustomerType, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_2":
                    search = new FrmSearch(MasterList.MTypeEmployee, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_3":
                    //search = new FrmSearch(MasterList.MServiceType, FormStatus.Normal);
                    //search.ShowDialog();
                    break;
                case "TSM_4":
                    search = new FrmSearch(MasterList.MMassageType, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_5":
                    search = new FrmSearch(MasterList.MPaymentType, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_6":
                    search = new FrmSearch(MasterList.MBank, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_7":
                    search = new FrmSearch(MasterList.MNation, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_8":
                    search = new FrmSearch(MasterList.MSex, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_9":
                    FrmRoom room = new FrmRoom(FormStatus.Normal);
                    room.ShowDialog();
                    break;
                case "TSM_10":
                    FrmPackage package = new FrmPackage(FormStatus.Normal);
                    package.ShowDialog();
                    break;
                case "TSM_11":
                    FrmScent scent = new FrmScent();
                    scent.ShowDialog();
                    break;
                case "TSM_12":
                    search = new FrmSearch(MasterList.MPaymentStatus, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_13":
                    search = new FrmSearch(MasterList.MTitle, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_14":
                    search = new FrmSearch(MasterList.MHour, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_15":
                    FrmTimePeriod timePeriod = new FrmTimePeriod();
                    timePeriod.ShowDialog();
                    break;
                case "TSM_16":
                    search = new FrmSearch(MasterList.MAccountCode, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_17":
                    search = new FrmSearch(MasterList.MLeave, FormStatus.Normal);
                    search.ShowDialog();
                    break;
                case "TSM_OP8":
                case "toolStripButton5":
                    FrmMateriel materiel = new FrmMateriel();
                    materiel.ShowDialog();
                    break;
                case "TSM_OP1":
                case "toolStripButton3":
                    FrmEmployee employee = new FrmEmployee(main);
                    employee.ShowDialog();
                    break;
                case "TSM_OP2":
                case "toolStripButton4":
                    FrmMember customer = new FrmMember();
                    customer.ShowDialog();
                    break;
                case "TSM_OP3":
                case "toolStripButton7":
                    FrmPackage fpack = new FrmPackage();
                    fpack.ShowDialog();
                    break;
                case "TSM_OP4":
                    FrmUseService useService = new FrmUseService(main,DBStatus.Insert);
                    useService.ShowDialog();
                    break;
                case "TSM_OP5":
                case "toolStripButton2":
                    FrmEmpSummary empSum = new FrmEmpSummary();
                    empSum.ShowDialog();
                    break;
                case "TSM_OP05":
                    Cursor = Cursors.WaitCursor;
                    FrmDailyReport fdr = new FrmDailyReport();
                    fdr.ShowDialog();
                    Cursor = Cursors.Default;
                    break;
                case "TSM_OP6":
                    FrmLeave leave = new FrmLeave(main);
                    leave.ShowDialog();
                    break;
                case "TSM_OP7":
                    break;
                case "TSM_R1":
                    FrmReport report = new FrmReport();
                    report.ShowDialog();
                    break;
                case "toolStripButton6":
                case "TSM_F02":
                    //Cursor = Cursors.WaitCursor;
                    Application.Exit();
                    //Cursor = Cursors.Default;
                    break;
                case "TSM_F01":
                case "toolStripButton1":
                    main.Close();
                    frmLogin login = new frmLogin();
                    login.ShowDialog();
                    if (login.click.Equals("OK"))
                    {
                        initial();
                    }
                    else
                    {

                    }
                    break;
                case "TSM_MANUAL":
                    try
                    {
                        Process.Start("RakutenManual.pdf");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ไม่พบเอกสารคู่มือ");
                        Console.WriteLine(ex.Message);
                    }
                    break;
                case "TSM_OP01":
                    FrmPermission fp = new FrmPermission();
                    fp.ShowDialog();
                    break;
                case "TSM_OP02":
                    FrmOption fo = new FrmOption();
                    fo.ShowDialog();
                    break;
                case "TSM_OP03":
                    FrmDeleteTransactions fdt = new FrmDeleteTransactions(this.main);
                    fdt.ShowDialog();
                    break;
                case "TSM_OP04":
                    FrmConversion fCon = new FrmConversion();
                    fCon.ShowDialog();
                    break;
                case "toolStripButton8":
                    FrmCustomerUseDate fcud = new FrmCustomerUseDate();
                    fcud.ShowDialog();
                    break;
            }
            main.refreshSchedule();//Refresh Data
            Cursor = Cursors.Default;
        }


        private void initial()
        {
            if (Authorize.getUser().Equals("")) Application.Exit();//if alt+f4 exit app
            //Set Status
            toolStripStatusLabel1.Text = "ผู้ใช้งานปัจจุบัน: " + Authorize.getUser() + " " + Authorize.getUserName();
            toolStripStatusLabel2.Text = "สถานะ: " + Authorize.getUserPermission();

            //Open main
            main = new FrmMain();
            main.MdiParent = this;
            main.WindowState = FormWindowState.Maximized;
            main.Show();

            //update last login
            DALUser.manage(new CUSER
                    {
                        USER = Authorize.getUser(),
                        PASSWORD = "1",
                        Type = "",
                        PERMISSION = "",
                        KEYUSER = "0000",
                        FLAG = "5"
                    });
            Authorize.GetAuthorize(this.Name);
            //set Screen
            switch (Authorize.getUserPermission())
            {
                case "Admin":
                    TSM_MASTER.Visible = true;
                    TSM_OPER.Visible = true;
                    TSM_R1.Visible = true;
                    TSM_OPTION.Visible = true;
                    toolStripButton1.Visible = true;//change user
                    toolStripButton2.Visible = true;//therapist sumary
                    toolStripButton3.Visible = true;//employee data
                    toolStripButton4.Visible = true;//register memeber
                    toolStripButton5.Visible = true;//stock data
                    toolStripButton6.Visible = true;//package
                    toolStripButton7.Visible = true;//exit
                    break;
                case "Operator":
                    TSM_MASTER.Visible = true;
                    TSM_OPER.Visible = false;
                    TSM_R1.Visible = false;
                    TSM_OPTION.Visible = false;
                    toolStripButton1.Visible = true;//change user
                    toolStripButton2.Visible = true;//therapist sumary
                    toolStripButton3.Visible = false;//employee data
                    toolStripButton4.Visible = true;//register memeber
                    toolStripButton5.Visible = true;//stock data
                    toolStripButton6.Visible = true;//package
                    toolStripButton7.Visible = true;//exit
                    break;
                default:
                    break;
            }
        }

        private void FrmMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
        }
    }
}
