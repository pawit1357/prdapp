﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Rakuten.Structure;
using Rakuten.DAL;
using Rakuten.Utility;
using System.Data.SqlClient;
using System.Drawing;
namespace Rakuten
{
    /// <summary>
    /// 1.การเปลี่ยนแปลงรายการจะเป็นการ เปลี่ยนสถานะ  record เก่าเป็น 4 แล้วเพิ่ม record ใหม่
    /// 2.การยกเลิกรายการจะเป็นการเปลี่ยนสถานะเป็น ยกเลิกรายการ
    /// </summary>
    public partial class FrmUseService : Form
    {
        private DBStatus status = DBStatus.Insert;
        private FrmMain main;
        private CPackage spPackage = null;//เก็บ Temp ของ Special package
        private List<CCustomerTransaction> lists = null;
        public FrmUseService()//Normal
        {
            InitializeComponent();
        }

        public FrmUseService(FrmMain _main,DBStatus _status)//Screen
        {
            InitializeComponent();
            this.main = _main;
            this.status = _status;
            switch (status)
            { 
                case DBStatus.Update:
                    B_SAVE.Text = "เปลี่ยนรายการ";
                    break;
                case DBStatus.Delete:
                    B_SAVE.Text = "ยกเลิกรายการ";
                    break;
                case DBStatus.Insert:
                    B_SAVE.Text = "บันทึก";
                    break;
            }
        }

        private void FrmUseService_Load(object sender, EventArgs e)
        {
            initial();
            //Initial time index and therapist
            if (!main.Therapist.Equals("0000"))
            {
                DTP_TIME_S.Value = DALSchedule.getTime(main.TimePeriodIndex);
                CBO_THERAPIST.SelectedValue = main.Therapist;
            }
            DTP_SERVICE_DATE.Value = main.Date;
        }

        private void TXT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (CBO_TYPE.SelectedValue.Equals("0002"))
                {
                    List<CMember> cus = DALMember.getList(new CMember
                    {
                        ID = TXT_MEMBERID.Text,
                        Type = "",
                        Sex = "",
                        Title = "",
                        Name = "",
                        Surname = "",
                        BirthDay = DateTime.Now,
                        Address = "",
                        Mobile = "",
                        Email = "",
                        Nation = "",
                        StartDate = DateTime.Now,
                        EndDate = DateTime.Now,
                        KEYDATE = DateTime.Now,
                        KEYUSER = Authorize.getUser(),
                        LASTEDIT = DateTime.Now,
                        FLAG = Management.SearchByID//Insert
                    });
                    if (cus.Count > 0)
                    {
                        CBO_SEX.SelectedValue = cus[0].Sex;
                        TXT_MEMBERID.Text = cus[0].ID;
                        TXT_NAME.Text = cus[0].Name;
                        TXT_SURNAME.Text = cus[0].Surname;
                        ND_AGE.Value = cus[0].Age;
                        //TXT_TEL.Text = cus[0].Mobile;
                        //Set Package Detail
                        getServiceList(CBO_TYPE.SelectedValue.ToString(),"");
                    }
                    else {
                        Management.ShowMsg("ไม่พบข้อมูลสมาชิก");
                    }
                }
                else {
                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void CheckIsNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                Management.ShowMsg(Management.NUMBER_ONLY);
            }
        }

        private void B_SAVE_Click(object sender, EventArgs e)
        {
            if (TXT_MEMBERID.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                tabControl1.SelectTab(0);
                TXT_MEMBERID.Focus();
            }
            else if (TXT_NAME.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                tabControl1.SelectTab(0);
                TXT_NAME.Focus();
            }
            else if (TXT_SURNAME.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                tabControl1.SelectTab(0);
                TXT_SURNAME.Focus();
            }
            else if (dataGridView2.Rows.Count == 0)
            {
                Management.ShowMsg("ยังไม่ได้ Package.");
                tabControl1.SelectTab(1);
            }
            else if (CBO_MASSAGE_TYPE.SelectedValue.Equals("0007") && LSpecialPackage_Detail.Text.Equals(""))
            {
                Management.ShowMsg("ยังไม่ได้ Package.");
                tabControl1.SelectTab(1);
            }
            else
            {
                try
                {
                    switch (status)
                    {
                        case DBStatus.Insert:
                            CCustomerTransaction ct = new CCustomerTransaction
                            {
                                ID = main.RecID.ToString(),
                                CustomerID = TXT_MEMBERID.Text,
                                TherapistID = CBO_THERAPIST.SelectedValue.ToString(),
                                TherapistRevID = ((CB_RESERV.Checked) ? CBO_THERAPIST.SelectedValue.ToString() : "0000"),
                                MassageType = CBO_MASSAGE_TYPE.SelectedValue.ToString(),
                                Package = ((CBO_TYPE.SelectedValue.ToString().Equals("0001")) ? dataGridView2.CurrentRow.Cells[1].Value.ToString() : (spPackage == null) ? dataGridView2.CurrentRow.Cells[1].Value.ToString() : spPackage.ID),//เก็บข้อมูล flag กรณีเป็น special package
                                Date = DTP_SERVICE_DATE.Value,
                                Time = DTP_TIME_S.Value,
                                Room = CBO_ROOM.SelectedValue.ToString(),
                                Customertype = CBO_TYPE.SelectedValue.ToString(),
                                Name = TXT_NAME.Text,
                                Surname = TXT_SURNAME.Text,
                                Sex = CBO_SEX.SelectedValue.ToString(),
                                Age = Convert.ToInt16(ND_AGE.Value),
                                Nation = CBO_NATION.SelectedValue.ToString(),
                                Mobile = TXT_TEL.Text,
                                Address = TXT_ADDR.Text,
                                Status = getProgrmeType(),//1 คือ จอง , 2 คือ ใช้งบริการปกติ
                                ScentType = CBO_SCENT.SelectedValue.ToString(),
                                ScentCount = Convert.ToInt16(NUD_SCENTCOUNT.Value),
                                ScentType1 = CBO_SCENT1.SelectedValue.ToString(),
                                ScentCount1 = Convert.ToInt16(NUD_SCENTCOUNT1.Value),
                                SpecialPackage = ((spPackage == null) ? "0000" : dataGridView2.CurrentRow.Cells[1].Value.ToString()),
                                KEYUSER = Authorize.getUser(),
                                Remark = TXT_REMARK.Text,
                                FLAG = Management.Insert//Insert
                            };
                            if (DALCustomerTransaction.manageMaster(ct) > 0)
                            {
                                Management.ShowMsg(Management.INSERT_SUCCESS);
                                main.refreshSchedule();
                                Close();
                            }
                            break;
                        case DBStatus.Update:
                            if (!TXT_REMARK.Text.Equals(""))
                            {
                                CCustomerTransaction ct1 = new CCustomerTransaction
                                {
                                    ID = main.RecID.ToString(),
                                    CustomerID = TXT_MEMBERID.Text,
                                    TherapistID = CBO_THERAPIST.SelectedValue.ToString(),
                                    TherapistRevID = ((CB_RESERV.Checked) ? CBO_THERAPIST.SelectedValue.ToString() : "0000"),
                                    MassageType = CBO_MASSAGE_TYPE.SelectedValue.ToString(),
                                    Package = ((CBO_TYPE.SelectedValue.ToString().Equals("0001")) ? dataGridView2.CurrentRow.Cells[1].Value.ToString() : (spPackage == null) ? dataGridView2.CurrentRow.Cells[1].Value.ToString() : spPackage.ID),//เก็บข้อมูล flag กรณีเป็น special package
                                    Date = DTP_SERVICE_DATE.Value,
                                    Time = DTP_TIME_S.Value,
                                    Room = CBO_ROOM.SelectedValue.ToString(),
                                    Customertype = CBO_TYPE.SelectedValue.ToString(),
                                    Name = TXT_NAME.Text,
                                    Surname = TXT_SURNAME.Text,
                                    Sex = CBO_SEX.SelectedValue.ToString(),
                                    Age = Convert.ToInt16(ND_AGE.Value),
                                    Nation = CBO_NATION.SelectedValue.ToString(),
                                    Mobile = TXT_TEL.Text,
                                    Address = TXT_ADDR.Text,
                                    Status = getProgrmeType(),//1 คือ จอง , 2 คือ ใช้งบริการปกติ
                                    ScentType = CBO_SCENT.SelectedValue.ToString(),
                                    ScentCount = Convert.ToInt16(NUD_SCENTCOUNT.Value),
                                    ScentType1 = CBO_SCENT1.SelectedValue.ToString(),
                                    ScentCount1 = Convert.ToInt16(NUD_SCENTCOUNT1.Value),
                                    SpecialPackage = dataGridView2.CurrentRow.Cells[1].Value.ToString(),
                                    KEYUSER = Authorize.getUser(),
                                    Remark = TXT_REMARK.Text,
                                    FLAG = "1"
                                };
                                if (DALCustomerTransaction.manageMaster(ct1) > 0)
                                {
                                    Management.ShowMsg(Management.UPDATE_SUCCESS);
                                    main.refreshSchedule();
                                    Close();
                                }
                            }
                            else
                            {
                                Management.ShowMsg("เปลี่ยนแปลงรายการโปรดระบุเหตุผลด้วย");
                                TXT_REMARK.Focus();
                            }
                            break;
                        case DBStatus.Delete:
                            if (!TXT_REMARK.Text.Equals(""))
                            {
                                CCustomerTransaction ct2 = new CCustomerTransaction
                                {
                                    ID = main.RecID.ToString(),
                                    CustomerID = TXT_MEMBERID.Text,
                                    TherapistID = CBO_THERAPIST.SelectedValue.ToString(),
                                    TherapistRevID = ((CB_RESERV.Checked) ? CBO_THERAPIST.SelectedValue.ToString() : "0000"),
                                    MassageType = CBO_MASSAGE_TYPE.SelectedValue.ToString(),
                                    Package = ((CBO_TYPE.SelectedValue.ToString().Equals("0001")) ? dataGridView2.CurrentRow.Cells[1].Value.ToString() : (spPackage == null) ? dataGridView2.CurrentRow.Cells[1].Value.ToString() : spPackage.ID),//เก็บข้อมูล flag กรณีเป็น special package
                                    Date = DTP_SERVICE_DATE.Value,
                                    Time = DTP_TIME_S.Value,
                                    Room = CBO_ROOM.SelectedValue.ToString(),
                                    Customertype = CBO_TYPE.SelectedValue.ToString(),
                                    Name = TXT_NAME.Text,
                                    Surname = TXT_SURNAME.Text,
                                    Sex = CBO_SEX.SelectedValue.ToString(),
                                    Age = Convert.ToInt16(ND_AGE.Value),
                                    Nation = CBO_NATION.SelectedValue.ToString(),
                                    Mobile = TXT_TEL.Text,
                                    Address = TXT_ADDR.Text,
                                    Status = getProgrmeType(),//1 คือ จอง , 2 คือ ใช้งบริการปกติ
                                    ScentType = CBO_SCENT.SelectedValue.ToString(),
                                    ScentCount = Convert.ToInt16(NUD_SCENTCOUNT.Value),
                                    ScentType1 = CBO_SCENT1.SelectedValue.ToString(),
                                    ScentCount1 = Convert.ToInt16(NUD_SCENTCOUNT1.Value),
                                    SpecialPackage = dataGridView2.CurrentRow.Cells[1].Value.ToString(),
                                    KEYUSER = Authorize.getUser(),
                                    Remark = TXT_REMARK.Text,
                                    FLAG = "2"
                                };
                                if (DALCustomerTransaction.manageMaster(ct2) > 0)
                                {
                                    Management.ShowMsg(Management.DELETE_SUCCESS);
                                    main.refreshSchedule();
                                    Close();
                                }
                            }
                            else
                            {
                                Management.ShowMsg("ยกเลิกรายการโปรดระบุเหตุผลด้วย");
                                TXT_REMARK.Focus();
                            }
                            break;
                    }
                }
                catch (SqlException ex)
                {
                    Management.ShowMsg(ex.Message);
                }

            }
        }

        private void B_CANCEL_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void B_STYPE_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            switch (b.Name)
            {
                case "B_STHERAPIST":
                    //6 คือ flag บอกว่าให้ แสดง เฉพาะ ชื่อหมอนวด (รหัสหมอนวด 0003)
                    FrmEmployee employee = new FrmEmployee("6", FormStatus.Search);
                    employee.ShowDialog();
                    if (employee.bSelected)
                    {
                        CBO_THERAPIST.SelectedValue = employee.lists[employee.index].ID;
                    }
                    break;
                case "B_SMASSAGE_TYPE":
                    FrmSearch search3 = new FrmSearch(MasterList.MMassageType, FormStatus.Search);
                    search3.ShowDialog();
                    if (search3.bSelected)
                    {
                        CBO_MASSAGE_TYPE.SelectedValue = search3.masters[search3.index].ID;
                    }
                    break;
                case "B_SROOM":
                    FrmRoomStatus rStatus = new FrmRoomStatus(DTP_SERVICE_DATE.Value);
                    rStatus.ShowDialog();
                    if (rStatus.bSelected)
                    {
                        CBO_ROOM.SelectedValue = rStatus.lists[rStatus.index].Number;
                    }
                    break;
                case "B_SMEMBER":
                    FrmMember customer = new FrmMember(FormStatus.Search,CBO_TYPE.SelectedValue.ToString());
                    customer.ShowDialog();
                    if (customer.bSelected)
                    {
                        TXT_MEMBERID.Text = customer.lists[customer.index].ID;
                        TXT_NAME.Text = customer.lists[customer.index].Name;
                        TXT_SURNAME.Text = customer.lists[customer.index].Surname;
                        ND_AGE.Value = customer.lists[customer.index].Age;
                        CBO_SEX.SelectedValue = customer.lists[customer.index].Sex;
                        TXT_TEL.Text = customer.lists[customer.index].Mobile;
                        TXT_ADDR.Text = customer.lists[customer.index].Address;
                        getServiceList(CBO_TYPE.SelectedValue.ToString(),"");//Set Package for select
                    }
                    break;
                case "B_SSEX":
                    FrmSearch search9 = new FrmSearch(MasterList.MSex, FormStatus.Search);
                    search9.ShowDialog();
                    if (search9.bSelected)
                    {
                        CBO_SEX.SelectedValue = search9.masters[search9.index].ID;
                    }
                    break;
                case "B_SNATION":
                    FrmSearch search6 = new FrmSearch(MasterList.MNation, FormStatus.Search);
                    search6.ShowDialog();
                    if (search6.bSelected)
                    {
                        CBO_NATION.SelectedValue = search6.masters[search6.index].ID;
                    }
                    break;
                case "B_SSCENT":
                    FrmScent scent = new FrmScent();
                    scent.ShowDialog();
                    if (scent.bSelected)
                    {
                        CBO_SCENT.SelectedValue = scent.lists[scent.index].ID;
                    }
                    break;
                case "B_SSCENT1":
                    FrmScent scent1 = new FrmScent();
                    scent1.ShowDialog();
                    if (scent1.bSelected)
                    {
                        CBO_SCENT1.SelectedValue = scent1.lists[scent1.index].ID;
                    }
                    break;
                case "B_STYPE":
                    FrmSearch search8 = new FrmSearch(MasterList.MCustomerType, FormStatus.Search);
                    search8.ShowDialog();
                    if (search8.bSelected)
                    {
                        CBO_TYPE.SelectedValue = search8.masters[search8.index].ID;
                    }
                    break;
            }
        }

        private void initial()
        {
            TXT_MEMBERID.ReadOnly = false;
            TXT_NAME.ReadOnly = false;
            TXT_MEMBERID.Text = "";
            TXT_MEMBERID.Text = "";
            TXT_NAME.Text = "";
            TXT_SURNAME.Text = "";
            //TXT_TEL.Text = "";
            dataGridView2.DataSource = null;
            //Customer Type
            CBO_TYPE.DataSource = DALMaster.getList(MasterList.MCustomerType,
                                    new CMaster
                                    {
                                        ID = "",
                                        DETAIL = "",
                                        KEYDATE = DateTime.Now,
                                        KEYUSER = Authorize.getUser(),
                                        LASTEDIT = DateTime.Now,
                                        FLAG = Management.SearchAll//Searh Flag
                                    }
                                );
            //Employee
            CBO_THERAPIST.DataSource = DALEmployee.getList(new CEmployee
                                        {
                                            ID = "",
                                            Type = "0003",
                                            Sex = "",
                                            Title = "",
                                            Name = "",
                                            Surname = "",
                                            NickName = "",
                                            Nation = "",
                                            BirthDay = DateTime.Now,
                                            Address = "",
                                            Mobile = "",
                                            Email = "",
                                            Status = "",
                                            Period = "",
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = Management.SearchByGroup//Searh Flag
                                        }
                                    );
            //Massage Type
            CBO_MASSAGE_TYPE.DataSource = DALMaster.getList(MasterList.MMassageType,
                            new CMaster
                                {
                                    ID = "",
                                    DETAIL = "",
                                    KEYDATE = DateTime.Now,
                                    KEYUSER = Authorize.getUser(),
                                    LASTEDIT = DateTime.Now,
                                    FLAG = Management.SearchAll//Searh Flag
                                }
                            );
            //Room
            CBO_ROOM.DataSource = DALRoom.getList(new CRoom
                                        {
                                            ID = "",
                                            DETAIL = "",
                                            MeassageType = "",
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = Management.SearchAll//Searh Flag
                                        }
                                        );
            CBO_SEX.DataSource = DALMaster.getList(MasterList.MSex,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            CBO_NATION.DataSource = DALMaster.getList(MasterList.MNation,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            CBO_SCENT.DataSource = DALScent.getList(new CScent
                                        {
                                            ID = "",
                                            DETAIL = "",
                                            Min = 0,
                                            Max = 0,
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = Management.SearchAll
                                        }
                                    );
            CBO_SCENT1.DataSource = DALScent.getList(new CScent
            {
                ID = "",
                DETAIL = "",
                Min = 0,
                Max = 0,
                KEYDATE = DateTime.Now,
                KEYUSER = Authorize.getUser(),
                LASTEDIT = DateTime.Now,
                FLAG = Management.SearchAll
            }
                        );
            if (status == DBStatus.Delete)
            {
                groupBox6.Enabled = false;
                RD_04.Checked = true;//กำหนดให้ลบรายการ
            }
            switch (status)
            { 
                case DBStatus.Delete:
                case DBStatus.Update:
                    //แสดงข้อมูล Transaction
                    lists = DALCustomerTransaction.getList(
                                    new CCustomerTransaction
                                    {
                                        ID = main.RecID.ToString(),
                                        CustomerID = "",
                                        TherapistID = "",
                                        TherapistRevID = "",
                                        Package = "",
                                        MassageType = "",
                                        Hour = "",
                                        Date = main.DTP_SERVICE_DATE.Value,
                                        Time = DateTime.Now,
                                        Room = "",
                                        Customertype = "",
                                        Name = "",
                                        Surname = "",
                                        ScentType = "",
                                        ScentCount = 0,
                                        ScentType1 = "",
                                        ScentCount1 = 0,
                                        Sex = "",
                                        Age = 0,
                                        Nation = "",
                                        Mobile = "",
                                        Address = "",
                                        Status = "2",//1 คือ จอง , 2 คือ ใช้งบริการปกติ
                                        SpecialPackage = "",
                                        KEYUSER = Authorize.getUser(),
                                        Remark = "",
                                        FLAG = "5"//ดึงข้อมูลมาแสดงตาม record ID
                                    }
                            );
                    CBO_TYPE.SelectedValue = lists[0].Customertype;
                    TXT_MEMBERID.Text = lists[0].CustomerID;
                    CBO_SEX.SelectedValue = lists[0].Sex;
                    TXT_NAME.Text = lists[0].Name;
                    TXT_SURNAME.Text = lists[0].Surname;
                    ND_AGE.Value = Convert.ToInt16(lists[0].Age);
                    CBO_NATION.SelectedValue = lists[0].Nation;
                    CB_RESERV.Checked = (lists[0].TherapistRevID.Equals("0000")) ? false : true;
                    TXT_TEL.Text = lists[0].Mobile;
                    TXT_ADDR.Text = lists[0].Address;
                    CBO_MASSAGE_TYPE.SelectedValue = Management.getPackage(lists[0].Package)[0].MassageType;
                    DTP_TIME_S.Value = Convert.ToDateTime(lists[0].Time);
                    CBO_ROOM.SelectedValue = lists[0].Room;
                    CBO_THERAPIST.SelectedValue = lists[0].TherapistID;
                    DTP_SERVICE_DATE.Value = lists[0].Date;
                    DTP_TIME_S.Value = lists[0].Time;
                    DTP_TIME_E.Value = DTP_TIME_S.Value.AddHours(Convert.ToDouble(Management.getDetail(MasterList.MHour, Management.getPackage(lists[0].Package)[0].Hour)));
                    getServiceList(lists[0].Customertype,lists[0].Package);//Set Package for select
                    break;
            }
        }
       
        private void getServiceList(string cusType,string cusSelectedPackage)
        {
            List<CPackage> list = null;
            switch (cusType)
            { 
                case "0001"://General
                    list = DALPackage.getList1(new CPackage
                            {
                                ID = "",
                                DETAIL = "",
                                Hour = "",
                                Price = 1,
                                MoneyUnit = "THB",
                                MassageType = CBO_MASSAGE_TYPE.SelectedValue.ToString(),
                                PerCourse = 1,
                                CustomerType = cusType,
                                StartDate = DateTime.Now,
                                EndDate = DateTime.Now,
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = "8"//
                            }
                        );
                    CBO_MASSAGE_TYPE.Enabled = true;
                    B_SMASSAGE_TYPE.Enabled = true;
                    break;
                case "0002"://Member
                    switch (status)
                    { 
                        case DBStatus.Delete:
                        case DBStatus.Update:
                            list = DALPackage.getList1(new CPackage
                                {
                                    ID = (lists[0].SpecialPackage.Equals("0000")) ? lists[0].FLAG : lists[0].SpecialPackage,
                                    DETAIL = "",
                                    Hour = "1",
                                    Price = 1,
                                    MoneyUnit = "THB",
                                    MassageType = "0", // 0 คือแสดงเฉพาะ package ทั่วไป 1 คือ แสดงเฉพาะ package ที่เป็นของสมาชิก
                                    PerCourse = 1,
                                    CustomerType = "",
                                    StartDate = DateTime.Now,
                                    EndDate = DateTime.Now,
                                    KEYDATE = DateTime.Now,
                                    KEYUSER = Authorize.getUser(),
                                    LASTEDIT = DateTime.Now,
                                    FLAG = "10"//--แสดงข้อมูล package ของสมาชิกแต่ละคน โดยแสดงเฉพาะที่ยังใช้ member ไม่หมด
                                }
                            );
                            spPackage = Management.getPackage(lists[0].SpecialPackage.Equals("0000") ? lists[0].Package : lists[0].FLAG)[0];
                            if (!lists[0].SpecialPackage.Equals("0000"))
                            {
                                LSpecialPackage_Detail.Text = "Package ที่เลือกคือ : " + spPackage.DETAIL;
                            }
                            break;
                        default:
                            list = DALPackage.getList1(new CPackage
                                {
                                    ID = TXT_MEMBERID.Text,
                                    DETAIL = "",
                                    Hour = "1",
                                    Price = 1,
                                    MoneyUnit = "THB",
                                    MassageType = "0", // 0 คือแสดงเฉพาะ package ทั่วไป 1 คือ แสดงเฉพาะ package ที่เป็นของสมาชิก
                                    PerCourse = 1,
                                    CustomerType = "",
                                    StartDate = DateTime.Now,
                                    EndDate = DateTime.Now,
                                    KEYDATE = DateTime.Now,
                                    KEYUSER = Authorize.getUser(),
                                    LASTEDIT = DateTime.Now,
                                    FLAG = "9"//--แสดงข้อมูล package ของสมาชิกแต่ละคน โดยแสดงเฉพาะที่ยังใช้ member ไม่หมด
                                }
                            );
                            break;
                    }
                    CBO_MASSAGE_TYPE.Enabled = false;
                    B_SMASSAGE_TYPE.Enabled = false;
                    break;
            }
            if (list.Count > 0)
            {
                dataGridView2.DataSource = list;
                //กำหนด package ตามที่ลูกค้าเลือก
                if (!cusSelectedPackage.Equals(""))
                {
                    dataGridView2.Focus();
                    for (int i = 0; i < dataGridView2.Rows.Count; i++)
                    {
                        if (dataGridView2.Rows[i].Cells[1].Value.ToString().Equals(cusSelectedPackage))
                        {
                            dataGridView2.CurrentCell = dataGridView2[0, i];
                            dataGridView2.Rows[i].Selected = true;
                            break;
                        }
                    }
                }
                if (cusType.Equals("0002"))
                {
                    CBO_MASSAGE_TYPE.SelectedValue = list[0].MassageType;
                }
            }
        }

        private void CBO_TYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CBO_TYPE.SelectedValue != null)
            {
                switch (CBO_TYPE.SelectedValue.ToString())
                {
                    case "0001":
                        TXT_MEMBERID.Text = DALCustomerTransaction.getLastCostomerID(CBO_TYPE.SelectedValue.ToString());
                        break;
                    case "0002":
                        TXT_MEMBERID.Text = "";
                        break;
                }
            }
        }

        private string getProgrmeType()
        {
            //1 จอง
            //2 ปกติ
            //3 ยกเลิก
            //4 เปลี่ยนแปลงรายการ
            return (RD_01.Checked) ? "1" : (RD_02.Checked) ? "2" : (RD_03.Checked) ? "3" : "4";
        }

        private void dataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 3:
                    e.Value = Management.getDetail(MasterList.MHour, e.Value.ToString());
                    break;
            }
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView2.Rows.Count > 0)
            {
                if (!dataGridView2.Rows[e.RowIndex].Cells[3].Value.ToString().Equals("0008"))//ตรวจสอบว่าไม่ใช่ (-) ถึงจะกำหนด เวลาเริ่มต้นสิ้นสุด Auto
                {
                    DTP_TIME_E.Value = DTP_TIME_S.Value.AddHours(Convert.ToDouble(Management.getDetail(MasterList.MHour, dataGridView2.Rows[e.RowIndex].Cells[3].Value.ToString())));
                }
            }
        }

        private void CBO_SCENT_SelectedIndexChanged(object sender, EventArgs e)
        {
            //แสดงจำนวนคงเหลือกแยกตาม package
            if (CBO_SCENT.SelectedValue != null)
            {
                List<CScent> lists = DALScent.getList1(new CScent
                {
                    ID = CBO_SCENT.SelectedValue.ToString(),
                    DETAIL = "",
                    ScentType = CBO_SCENT.SelectedValue.ToString(),
                    ProgramType = "",
                    Min = 0,
                    Max = 0,
                    KEYDATE = DateTime.Now,
                    KEYUSER = Authorize.getUser(),
                    LASTEDIT = DateTime.Now,
                    FLAG = "5"//ดูจำนวนคงเหลือแยกตาม ประเภท
                });
                if (lists.Count > 0)
                {
                    L_REMAIN.Text = lists[0].Count.ToString();
                    if (!lists[0].Remark.Equals("-"))
                    {
                        Management.ShowMsg(lists[0].Remark);
                    }
                }
            }
        }

        private void CBO_MASSAGE_TYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            getServiceList(CBO_TYPE.SelectedValue.ToString(),"");//Set Package for select
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //ถ้าประเภท การนวดเป็น specail package ให้แสดงหน้าต่างให้เลือก (เฉพาะสมาชิกเท่านั้นที่เลือกได้)
            if (CBO_TYPE.SelectedValue.ToString().Equals("0002"))
            {
                if (CBO_MASSAGE_TYPE.SelectedValue != null)
                {
                    switch (dataGridView2.CurrentRow.Cells[15].Value.ToString())
                    {
                        case "0007"://ประเภท Special package
                            FrmServiceList slist = new FrmServiceList();
                            slist.ShowDialog();
                            if (slist.bSelected)
                            {
                                spPackage = slist.list[slist.index];
                                //กำหนด เวลาเริ่มต้นเวลาสิ้นสุด auto
                                DTP_TIME_E.Value = DTP_TIME_S.Value.AddHours(Convert.ToDouble(Management.getDetail(MasterList.MHour, spPackage.Hour)));
                                //กำหนดชื่อ pacage ที่เลือก
                                LSpecialPackage_Detail.Text = "Package ที่เลือกคือ : " + spPackage.DETAIL;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void CBO_SCENT1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //แสดงจำนวนคงเหลือกแยกตาม package
            if (CBO_SCENT1.SelectedValue != null)
            {
                List<CScent> lists = DALScent.getList1(new CScent
                {
                    ID = CBO_SCENT1.SelectedValue.ToString(),
                    DETAIL = "",
                    ScentType = CBO_SCENT1.SelectedValue.ToString(),
                    ProgramType = "",
                    Min = 0,
                    Max = 0,
                    KEYDATE = DateTime.Now,
                    KEYUSER = Authorize.getUser(),
                    LASTEDIT = DateTime.Now,
                    FLAG = "5"//ดูจำนวนคงเหลือแยกตาม ประเภท
                });
                if (lists.Count > 0)
                {
                    L_REMAIN1.Text = lists[0].Count.ToString();
                    if (!lists[0].Remark.Equals("-"))
                    {
                        Management.ShowMsg(lists[0].Remark);
                    }
                }
            }
        }

    }
}
