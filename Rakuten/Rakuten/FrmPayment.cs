﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Rakuten.Structure;
using Rakuten.DAL;
using Rakuten.Utility;
using Rakuten.Report;
using System.Data.SqlClient;
using System.Drawing;
namespace Rakuten
{
    public partial class FrmPayment : Form
    {
        private FrmMain main;
        private int index = 0;
        private List<CCustomerTransaction> lists = null;
        public FrmPayment()//Normal
        {
            InitializeComponent();
        }
        public FrmPayment(FrmMain _main)//Normal
        {
            InitializeComponent();
            this.main = _main;
        }
        private void FrmPayment_Load(object sender, EventArgs e)
        {
            initial();
        }

        private void CheckIsNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                Management.ShowMsg(Management.NUMBER_ONLY);
            }
        }

        private void CBO_CASH_TYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (CBO_CASH_TYPE.SelectedIndex)
            {
                case 0://Crash
                    CBO_BANK.Enabled = false;
                    B_BANK.Enabled = false;
                    TXT_SLIP.Enabled = false;
                    break;
                case 1://Credit Card
                    CBO_BANK.Enabled = true;
                    B_BANK.Enabled = true;
                    TXT_SLIP.Enabled = true;
                    break;
            }
        }

        private void TXT_RECIEVE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TXT_CHANGE.Text = Convert.ToString((Convert.ToInt16(TXT_RECIEVE.Text) + Convert.ToInt16(TXT_DISCOUNT.Text)) - Convert.ToInt16(TXT_TOTAL.Text));
                if (Convert.ToInt16(TXT_CHANGE.Text) < 0)
                {
                    Management.ShowMsg("จำนวนเงินไม่พอ");
                    TXT_CHANGE.Text = "0";
                    TXT_RECIEVE.SelectAll();
                }
                else
                {
                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void CMD_SAVE_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            bool bSuccess = false;
            switch (CBO_TYPE.SelectedValue.ToString())
            {
                case "0001":
                    bSuccess = true;
                    break;
                case "0002":
                    int result = 0;
                    if (dataGridView2.Rows.Count > 0)
                    {
                        switch (LSpecialPackage_Detail.Text)
                        {
                            case ""://Normal Member
                                result = (dataGridView2.Rows.Count > 0) ? (Convert.ToInt16(dataGridView2.CurrentRow.Cells[6].Value) - (1 + Convert.ToInt16(dataGridView2.CurrentRow.Cells[5].Value))) : 0;
                                if (result < 0)
                                {
                                    Management.ShowMsg("จำนวน Package หมดแล้วไม่สามารถใช้ได้");
                                    bSuccess = false;
                                }
                                else {
                                    bSuccess = true;
                                }
                                break;
                            default://Special Member
                                result = (dataGridView2.Rows.Count > 0) ? (Convert.ToInt16(dataGridView2.CurrentRow.Cells[6].Value) - (Convert.ToInt16(TXT_RECIEVE.Text) + Convert.ToInt16(dataGridView2.CurrentRow.Cells[5].Value))) : 0;
                                if (result < 0)
                                {
                                    if (MessageBox.Show("ราคา Package เกินวงเงินไป " + result + " บาท ต้องการชำระเพิ่มหรือไม่ ? ", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                                    {
                                        TXT_TOTAL.Text = (Convert.ToInt16(TXT_TOTAL.Text) - Math.Abs(result)).ToString();
                                        TXT_RECIEVE.Text = Math.Abs(result).ToString();
                                        TXT_RECIEVE.SelectAll();
                                        bSuccess = false;
                                    }
                                }
                                else
                                {
                                    bSuccess = true;
                                }
                                break;
                        }
                    }
                    else
                    {
                        Management.ShowMsg("จำนวน Package หมดแล้วไม่สามารถใช้ได้");
                        bSuccess = false;
                    }
                    break;
            }
            if (!bSuccess)
            {
            }
            else if ((CBO_TYPE.SelectedValue.Equals("0002") || ((Convert.ToInt16(TXT_RECIEVE.Text) + Convert.ToInt16(TXT_DISCOUNT.Text)) >= Convert.ToInt16(TXT_TOTAL.Text))))
            {
                try
                {
                    CCustomerPayment cuspay = new CCustomerPayment
                    {
                        ID = "",
                        CustomerTranID = main.RecID.ToString(),
                        AccountCode = CBO_ACCOUNT_CODE.SelectedValue.ToString(),
                        Amount = Convert.ToDouble(TXT_RECIEVE.Text) - Convert.ToDouble(TXT_CHANGE.Text),
                        Discount = Convert.ToDouble(TXT_DISCOUNT.Text),
                        paymentType = CBO_CASH_TYPE.SelectedValue.ToString(),
                        Slip = TXT_SLIP.Text,
                        Bank = CBO_BANK.SelectedValue.ToString(),
                        status = "T",//ชำระเงินแล้ว
                        Remark = (lists[0].Customertype.Equals("0001")) ? lists[0].Package : (lists[0].SpecialPackage.Equals("0000")) ? lists[0].FLAG : lists[0].SpecialPackage,
                        KEYUSER = Authorize.getUser(),
                        RUNNING = (CBO_TYPE.SelectedValue.ToString().Equals("0002")) ? Convert.ToInt16(dataGridView2.Rows[index].Cells[1].Value.ToString()) : 0,
                        DETAIL = (lists[0].SpecialPackage.Equals("0000")) ? "F" : "T",
                        FLAG = Management.Insert
                    };
                    //สมาชิกมาใช้บริการจะหักออกจา packate
                    if (DALCustomerPayment.manageMaster(cuspay) > 0)
                    {
                        checkPrint();
                        Management.ShowMsg(Management.INSERT_SUCCESS);
                        main.refreshSchedule();
                        Close();
                    }
                }
                catch (SqlException ex)
                {
                    Management.ShowMsg(ex.Message);
                }
            }
            else
            {
                Management.ShowMsg(Management.NOT_ENOUGH_MONEY);
                TXT_RECIEVE.Focus();
            }
            //}
            Cursor = Cursors.Default;
        }

        private void B_CANCEL_Click(object sender, EventArgs e)
        {
            initial();
        }

        private void initial()
        {
            NUD_YEAR.Value = 1;
            TXT_SLIP.Text = "";
            CB_PRINT_1.Checked = false;
            CB_PRINT_3.Checked = false;
            TXT_TOTAL.Text = "0";
            TXT_RECIEVE.Text = "0";
            TXT_CHANGE.Text = "0";

            //Customer Type
            CBO_TYPE.DataSource = DALMaster.getList(MasterList.MCustomerType,
                                    new CMaster
                                    {
                                        ID = "",
                                        DETAIL = "",
                                        KEYDATE = DateTime.Now,
                                        KEYUSER = Authorize.getUser(),
                                        LASTEDIT = DateTime.Now,
                                        FLAG = Management.SearchAll//Searh Flag
                                    }
                                );

            //รูปแบบการชำเงิน (เงินสด,บัตรเครดิต)
            CBO_CASH_TYPE.DataSource = DALMaster.getList(MasterList.MPaymentType,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            //ธนาคาร
            CBO_BANK.DataSource = DALMaster.getList(MasterList.MBank,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            //ประเภทการชำระ (ค่าสมัครสมาชิก,อื่น ๆ )
            CBO_ACCOUNT_CODE.DataSource = DALMaster.getList(MasterList.MAccountCode,
                            new CMaster
                            {
                                ID = "",//กำหนดให้จ่ายเงินค่าสมาลิก
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            //Initial Cash Type
            CBO_BANK.Enabled = false;
            TXT_SLIP.Enabled = false;
            //แสดงข้อมูล Transaction
            lists = DALCustomerTransaction.getList(
                            new CCustomerTransaction
                            {
                                ID = main.RecID.ToString(),
                                CustomerID = "",
                                TherapistID = "",
                                TherapistRevID = "",
                                MassageType = "",
                                Package = "",
                                Hour = "",
                                Date = main.DTP_SERVICE_DATE.Value,
                                Time = DateTime.Now,
                                Room = "",
                                Customertype = "",
                                Name = "",
                                Surname = "",
                                Sex = "",
                                Age = 0,
                                Nation = "",
                                Mobile = "",
                                Address = "",
                                Status = "",//1 คือ จอง , 2 คือ ใช้งบริการปกติ
                                ScentCount = 0,
                                SpecialPackage = "",
                                ScentType = "",
                                ScentType1 = "",
                                ScentCount1 = 0,
                                KEYUSER = Authorize.getUser(),
                                RUNNING = 0,
                                Remark = "",
                                FLAG = "5"//ดึงข้อมูลมาแสดงตาม record ID
                            }
                    );
            getServiceList(lists[0].Customertype);
            TXT_RECIEVE.SelectAll();
        }

        private void checkPrint()
        {
            string pStatus = (CB_PRINT_1.Checked) ? "1" : (CB_PRINT_2.Checked) ? "2" : "0";
            switch (pStatus)
            { 
                case "1"://Nomal Reciept
                    Rpt02 rpt02 = new Rpt02();
                    rpt02.SetDataSource(DALInvoice.getInvoice(main.RecID));
                    rpt02.SetParameterValue("PInvoice", "ต้นฉบับใบกำกับภาษี/ใบเสร็จรับเงิน");
                    rpt02.PrintToPrinter(1, false, 1, 1);
                    rpt02.SetParameterValue("PInvoice", "สำเนาใบกำกับภาษี/ใบเสร็จรับเงิน");
                    rpt02.PrintToPrinter(1, false, 1, 1);
                    break;
                case "2"://Inc VAT
                    Rpt03 rpt03 = new Rpt03();
                    rpt03.SetDataSource(DALInvoice.getInvoice(main.RecID));
                    rpt03.SetParameterValue("PInvoice", "ต้นฉบับใบกำกับภาษี/ใบเสร็จรับเงิน");
                    rpt03.PrintToPrinter(1, false, 1, 1);
                    rpt03.SetParameterValue("PInvoice", "สำเนาใบกำกับภาษี/ใบเสร็จรับเงิน");
                    rpt03.PrintToPrinter(1, false, 1, 1);
                    break;
            }
        }

        private void B_SPACKAGE_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            switch (b.Name)
            {
                case "B_CASHTYPE":
                    FrmSearch search3 = new FrmSearch(MasterList.MPaymentType, FormStatus.Search);
                    search3.ShowDialog();
                    if (search3.bSelected)
                    {
                        CBO_CASH_TYPE.SelectedValue = search3.masters[search3.index].ID;
                    }
                    break;
                case "B_BANK":
                    FrmSearch search4 = new FrmSearch(MasterList.MBank, FormStatus.Search);
                    search4.ShowDialog();
                    if (search4.bSelected)
                    {
                        CBO_BANK.SelectedValue = search4.masters[search4.index].ID;
                    }
                    break;
                case "B_PAY_TYPE":
                    FrmSearch search5 = new FrmSearch(MasterList.MAccountCode, FormStatus.Search);
                    search5.ShowDialog();
                    if (search5.bSelected)
                    {
                        CBO_ACCOUNT_CODE.SelectedValue = search5.masters[search5.index].ID;
                    }
                    break;
            }
        }

        private void CBO_TYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (CBO_TYPE.SelectedValue.ToString())
            { 
                case "0001":
                    groupBox1.Enabled = true;
                    groupBox2.Enabled = true;
                    GMember.Enabled = false;
                    break;
                case "0002":
                    if (LSpecialPackage_Detail.Equals(""))
                    {
                        //สมาชิก(ปกติ)
                        groupBox1.Enabled = false;
                        groupBox2.Enabled = false;
                        GMember.Enabled = true;
                    }
                    else { 
                        //สมาชิก(specail package)
                        groupBox1.Enabled = true;
                        groupBox2.Enabled = true;
                        GMember.Enabled = true;
                    }
                    break;
            }
        }

        private void getServiceList(string cusType)
        {
            CBO_TYPE.SelectedValue = cusType;
            List<CPackage> list = null;
            switch (cusType)
            {
                case "0001"://General
                            list = DALPackage.getList1(new CPackage
                            {
                                ID = lists[0].Package,
                                DETAIL = "",
                                Hour = "",
                                Price = 1,
                                MoneyUnit = "THB",
                                MassageType = "",
                                PerCourse = 1,
                                CustomerType = cusType,
                                StartDate = DateTime.Now,
                                EndDate = DateTime.Now,
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = "11"//--แสดงข้อมูล package ของสมาชิกแต่ละคน โดยแสดงเฉพาะที่ยังใช้ member ไม่หมด
                            }
                        );
                    TXT_TOTAL.Text = list[0].Price.ToString();
                    break;
                case "0002"://Member
                    list = DALPackage.getList1(new CPackage
                    {
                        ID = (lists[0].SpecialPackage.Equals("0000")) ? lists[0].FLAG : lists[0].SpecialPackage,
                        DETAIL = "",
                        Hour = "1",
                        Price = 1,
                        MoneyUnit = "THB",
                        MassageType = "0", // 0 คือแสดงเฉพาะ package ทั่วไป 1 คือ แสดงเฉพาะ package ที่เป็นของสมาชิก
                        PerCourse = 1,
                        CustomerType = cusType,
                        StartDate = DateTime.Now,
                        EndDate = DateTime.Now,
                        KEYDATE = DateTime.Now,
                        KEYUSER = Authorize.getUser(),
                        LASTEDIT = DateTime.Now,
                        FLAG = "10"//--แสดงข้อมูล package ของสมาชิกแต่ละคน โดยแสดงเฉพาะที่ยังใช้ member ไม่หมด
                    }
                    );

                    List<CPackage> lPackage = Management.getPackage(lists[0].SpecialPackage.Equals("0000") ? lists[0].Package : lists[0].FLAG);
                    if (!lists[0].SpecialPackage.Equals("0000"))
                    {
                        LSpecialPackage_Detail.Text = "Package ที่ใช้คือ : " + lPackage[0].DETAIL;
                    }
                    TXT_TOTAL.Text = lPackage[0].Price.ToString();
                    TXT_RECIEVE.Text = lPackage[0].Price.ToString();
                    break;
            }
            if (list.Count > 0)
            {
                dataGridView2.DataSource = list;
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView2.Rows.Count > 0)
            {
                index = e.RowIndex;
            }
        }

        private void CMD_CANCEL_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            { 
                case 3:
                    e.Value = Management.getDetail(MasterList.MHour, e.Value.ToString());
                    break;
            }
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }

        private void TXT_CHANGE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void TXT_DISCOUNT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void TXT_TOTAL_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

    }
}
