﻿namespace Rakuten
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.TSM_FILE = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_F01 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_F02 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_MASTER = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_1 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_2 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_3 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_4 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_5 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_6 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.TSM_7 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_8 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_9 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_10 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_12 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_13 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_14 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_15 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_16 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_17 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OPER = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP1 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP5 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP2 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP3 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP4 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP6 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP8 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_R1 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OPTION = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP01 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP02 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP03 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_OP04 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_H1 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_MANUAL = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.TSM_OP05 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 479);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(807, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_FILE,
            this.TSM_MASTER,
            this.TSM_OPER,
            this.TSM_R1,
            this.TSM_OPTION,
            this.TSM_H1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(807, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // TSM_FILE
            // 
            this.TSM_FILE.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_F01,
            this.TSM_F02});
            this.TSM_FILE.Name = "TSM_FILE";
            this.TSM_FILE.Size = new System.Drawing.Size(40, 20);
            this.TSM_FILE.Text = "แฟ้ม";
            // 
            // TSM_F01
            // 
            this.TSM_F01.Name = "TSM_F01";
            this.TSM_F01.Size = new System.Drawing.Size(141, 22);
            this.TSM_F01.Text = "เปลี่ยนผู้ใช้งาน";
            this.TSM_F01.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_F02
            // 
            this.TSM_F02.Name = "TSM_F02";
            this.TSM_F02.Size = new System.Drawing.Size(141, 22);
            this.TSM_F02.Text = "ออกจากระบบ";
            this.TSM_F02.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_MASTER
            // 
            this.TSM_MASTER.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_1,
            this.TSM_2,
            this.TSM_3,
            this.TSM_4,
            this.TSM_5,
            this.TSM_6,
            this.TSM_11,
            this.toolStripSeparator1,
            this.TSM_7,
            this.TSM_8,
            this.TSM_9,
            this.TSM_10,
            this.TSM_12,
            this.TSM_13,
            this.TSM_14,
            this.TSM_15,
            this.TSM_16,
            this.TSM_17});
            this.TSM_MASTER.Name = "TSM_MASTER";
            this.TSM_MASTER.Size = new System.Drawing.Size(92, 20);
            this.TSM_MASTER.Text = "ตั้งค่าข้อมูลหลัก";
            // 
            // TSM_1
            // 
            this.TSM_1.Name = "TSM_1";
            this.TSM_1.Size = new System.Drawing.Size(197, 22);
            this.TSM_1.Text = "ตั้งค่าประเภทลูกค้า";
            this.TSM_1.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_2
            // 
            this.TSM_2.Name = "TSM_2";
            this.TSM_2.Size = new System.Drawing.Size(197, 22);
            this.TSM_2.Text = "ตั้งค่าประเภทพนักงาน";
            this.TSM_2.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_3
            // 
            this.TSM_3.Name = "TSM_3";
            this.TSM_3.Size = new System.Drawing.Size(197, 22);
            this.TSM_3.Text = "ตั้งค่าประเภทรายการxxxxx";
            this.TSM_3.Visible = false;
            this.TSM_3.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_4
            // 
            this.TSM_4.Name = "TSM_4";
            this.TSM_4.Size = new System.Drawing.Size(197, 22);
            this.TSM_4.Text = "ตั้งค่าประเภทบริการ";
            this.TSM_4.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_5
            // 
            this.TSM_5.Name = "TSM_5";
            this.TSM_5.Size = new System.Drawing.Size(197, 22);
            this.TSM_5.Text = "ตั้งค่ารูปแบบการชำระเงิน";
            this.TSM_5.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_6
            // 
            this.TSM_6.Name = "TSM_6";
            this.TSM_6.Size = new System.Drawing.Size(197, 22);
            this.TSM_6.Text = "ตั้งค่าธนาคาร";
            this.TSM_6.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_11
            // 
            this.TSM_11.Name = "TSM_11";
            this.TSM_11.Size = new System.Drawing.Size(197, 22);
            this.TSM_11.Text = "ตั้งค่ากลิ่นน้ำหอม";
            this.TSM_11.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(194, 6);
            // 
            // TSM_7
            // 
            this.TSM_7.Name = "TSM_7";
            this.TSM_7.Size = new System.Drawing.Size(197, 22);
            this.TSM_7.Text = "ตั้งค่าเชื้อชาติ";
            this.TSM_7.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_8
            // 
            this.TSM_8.Name = "TSM_8";
            this.TSM_8.Size = new System.Drawing.Size(197, 22);
            this.TSM_8.Text = "ตั้งค่าเพศ";
            this.TSM_8.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_9
            // 
            this.TSM_9.Name = "TSM_9";
            this.TSM_9.Size = new System.Drawing.Size(197, 22);
            this.TSM_9.Text = "ตั้งค่าห้อง";
            this.TSM_9.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_10
            // 
            this.TSM_10.Name = "TSM_10";
            this.TSM_10.Size = new System.Drawing.Size(197, 22);
            this.TSM_10.Text = "ตั้งค่าเพจเกต";
            this.TSM_10.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_12
            // 
            this.TSM_12.Name = "TSM_12";
            this.TSM_12.Size = new System.Drawing.Size(197, 22);
            this.TSM_12.Text = "ตั้งค่าสถานะการชำระเงิน";
            this.TSM_12.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_13
            // 
            this.TSM_13.Name = "TSM_13";
            this.TSM_13.Size = new System.Drawing.Size(197, 22);
            this.TSM_13.Text = "ตั้งค่านำหน้าชื่อxxxx";
            this.TSM_13.Visible = false;
            this.TSM_13.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_14
            // 
            this.TSM_14.Name = "TSM_14";
            this.TSM_14.Size = new System.Drawing.Size(197, 22);
            this.TSM_14.Text = "ตั้งค่าชั่วโมง";
            this.TSM_14.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_15
            // 
            this.TSM_15.Name = "TSM_15";
            this.TSM_15.Size = new System.Drawing.Size(197, 22);
            this.TSM_15.Text = "ตั้งค่ากะงาน";
            this.TSM_15.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_16
            // 
            this.TSM_16.Name = "TSM_16";
            this.TSM_16.Size = new System.Drawing.Size(197, 22);
            this.TSM_16.Text = "ตั้งค่ารหัสค่าใช้จ่าย";
            this.TSM_16.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_17
            // 
            this.TSM_17.Name = "TSM_17";
            this.TSM_17.Size = new System.Drawing.Size(197, 22);
            this.TSM_17.Text = "ตั้งค่าประเภทการลา/ขาด";
            this.TSM_17.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OPER
            // 
            this.TSM_OPER.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_OP1,
            this.TSM_OP5,
            this.TSM_OP2,
            this.TSM_OP3,
            this.TSM_OP4,
            this.TSM_OP6,
            this.TSM_OP8});
            this.TSM_OPER.Name = "TSM_OPER";
            this.TSM_OPER.Size = new System.Drawing.Size(66, 20);
            this.TSM_OPER.Text = "ดำเนินการ";
            // 
            // TSM_OP1
            // 
            this.TSM_OP1.Name = "TSM_OP1";
            this.TSM_OP1.Size = new System.Drawing.Size(168, 22);
            this.TSM_OP1.Text = "เก็บข้อมูลพนักงาน";
            this.TSM_OP1.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OP5
            // 
            this.TSM_OP5.Name = "TSM_OP5";
            this.TSM_OP5.Size = new System.Drawing.Size(168, 22);
            this.TSM_OP5.Text = "สรุปค่ามือพนักงาน";
            this.TSM_OP5.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OP2
            // 
            this.TSM_OP2.Name = "TSM_OP2";
            this.TSM_OP2.Size = new System.Drawing.Size(168, 22);
            this.TSM_OP2.Text = "สมัครสมาชิก";
            this.TSM_OP2.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OP3
            // 
            this.TSM_OP3.Name = "TSM_OP3";
            this.TSM_OP3.Size = new System.Drawing.Size(168, 22);
            this.TSM_OP3.Text = "ตั้งค่าแพคเก็ต";
            this.TSM_OP3.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OP4
            // 
            this.TSM_OP4.Name = "TSM_OP4";
            this.TSM_OP4.Size = new System.Drawing.Size(168, 22);
            this.TSM_OP4.Text = "ใช้บริการ";
            this.TSM_OP4.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OP6
            // 
            this.TSM_OP6.Name = "TSM_OP6";
            this.TSM_OP6.Size = new System.Drawing.Size(168, 22);
            this.TSM_OP6.Text = "ลาหยุด";
            this.TSM_OP6.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OP8
            // 
            this.TSM_OP8.Name = "TSM_OP8";
            this.TSM_OP8.Size = new System.Drawing.Size(168, 22);
            this.TSM_OP8.Text = "จัดเก็บวัสดุ (น้ำหอม)";
            this.TSM_OP8.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_R1
            // 
            this.TSM_R1.Name = "TSM_R1";
            this.TSM_R1.Size = new System.Drawing.Size(52, 20);
            this.TSM_R1.Text = "รายงาน";
            this.TSM_R1.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OPTION
            // 
            this.TSM_OPTION.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_OP01,
            this.TSM_OP02,
            this.TSM_OP03,
            this.TSM_OP04,
            this.TSM_OP05});
            this.TSM_OPTION.Name = "TSM_OPTION";
            this.TSM_OPTION.Size = new System.Drawing.Size(56, 20);
            this.TSM_OPTION.Text = "ตัวเลือก";
            // 
            // TSM_OP01
            // 
            this.TSM_OP01.Name = "TSM_OP01";
            this.TSM_OP01.Size = new System.Drawing.Size(201, 22);
            this.TSM_OP01.Text = "ตั้งค่าสิทธิ์การใช้งาน";
            this.TSM_OP01.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OP02
            // 
            this.TSM_OP02.Name = "TSM_OP02";
            this.TSM_OP02.Size = new System.Drawing.Size(201, 22);
            this.TSM_OP02.Text = "ตั้งค่าอื่นๆ";
            this.TSM_OP02.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OP03
            // 
            this.TSM_OP03.Name = "TSM_OP03";
            this.TSM_OP03.Size = new System.Drawing.Size(201, 22);
            this.TSM_OP03.Text = "ลบข้อมูล";
            this.TSM_OP03.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OP04
            // 
            this.TSM_OP04.Name = "TSM_OP04";
            this.TSM_OP04.Size = new System.Drawing.Size(201, 22);
            this.TSM_OP04.Text = "โหลดข้อมูลสมาชิกเข้าระบบ";
            this.TSM_OP04.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_H1
            // 
            this.TSM_H1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_MANUAL});
            this.TSM_H1.Name = "TSM_H1";
            this.TSM_H1.Size = new System.Drawing.Size(62, 20);
            this.TSM_H1.Text = "ช่วยเหลือ";
            // 
            // TSM_MANUAL
            // 
            this.TSM_MANUAL.Name = "TSM_MANUAL";
            this.TSM_MANUAL.Size = new System.Drawing.Size(141, 22);
            this.TSM_MANUAL.Text = "คู่มือการใช้งาน";
            this.TSM_MANUAL.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton6});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(807, 52);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::Rakuten.Properties.Resources.selection_recycle;
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(78, 49);
            this.toolStripButton1.Text = "เปลี่ยนผู้ใช้งาน";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::Rakuten.Properties.Resources.document_chart;
            this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(94, 49);
            this.toolStripButton2.Text = "สรุปค่ามือพนักงาน";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton2.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::Rakuten.Properties.Resources.photo_portrait;
            this.toolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(77, 49);
            this.toolStripButton3.Text = "ข้อมูลพนักงาน";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = global::Rakuten.Properties.Resources.flash;
            this.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(65, 49);
            this.toolStripButton4.Text = "ข้อมูลลูกค้า";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = global::Rakuten.Properties.Resources.import1;
            this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(67, 49);
            this.toolStripButton5.Text = "ข้อมูลสต๊อก";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = global::Rakuten.Properties.Resources.package_add;
            this.toolStripButton7.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(50, 49);
            this.toolStripButton7.Text = "แพ็คเก็ต";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.Image = global::Rakuten.Properties.Resources.folder_document;
            this.toolStripButton8.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(98, 49);
            this.toolStripButton8.Text = "ประวัติการใช้บริการ";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Image = global::Rakuten.Properties.Resources.error;
            this.toolStripButton6.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(74, 49);
            this.toolStripButton6.Text = "ออกจากระบบ";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton6.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // TSM_OP05
            // 
            this.TSM_OP05.Name = "TSM_OP05";
            this.TSM_OP05.Size = new System.Drawing.Size(201, 22);
            this.TSM_OP05.Text = "ส่งรายงานไปยัง EMAIL";
            this.TSM_OP05.Click += new System.EventHandler(this.TSM_1_Click);
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 501);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบบสปา (Rakuten)";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMenu_FormClosed);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem TSM_FILE;
        private System.Windows.Forms.ToolStripMenuItem TSM_MASTER;
        private System.Windows.Forms.ToolStripMenuItem TSM_1;
        private System.Windows.Forms.ToolStripMenuItem TSM_2;
        private System.Windows.Forms.ToolStripMenuItem TSM_3;
        private System.Windows.Forms.ToolStripMenuItem TSM_4;
        private System.Windows.Forms.ToolStripMenuItem TSM_5;
        private System.Windows.Forms.ToolStripMenuItem TSM_6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem TSM_7;
        private System.Windows.Forms.ToolStripMenuItem TSM_8;
        private System.Windows.Forms.ToolStripMenuItem TSM_9;
        private System.Windows.Forms.ToolStripMenuItem TSM_10;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripMenuItem TSM_OPER;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP1;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP2;
        private System.Windows.Forms.ToolStripMenuItem TSM_R1;
        private System.Windows.Forms.ToolStripMenuItem TSM_H1;
        private System.Windows.Forms.ToolStripMenuItem TSM_11;
        private System.Windows.Forms.ToolStripMenuItem TSM_12;
        private System.Windows.Forms.ToolStripMenuItem TSM_13;
        private System.Windows.Forms.ToolStripMenuItem TSM_14;
        private System.Windows.Forms.ToolStripMenuItem TSM_15;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP3;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP4;
        private System.Windows.Forms.ToolStripMenuItem TSM_16;
        private System.Windows.Forms.ToolStripMenuItem TSM_17;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP5;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP6;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP8;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripMenuItem TSM_F01;
        private System.Windows.Forms.ToolStripMenuItem TSM_F02;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripMenuItem TSM_MANUAL;
        private System.Windows.Forms.ToolStripMenuItem TSM_OPTION;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP01;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP02;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP03;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP04;
        private System.Windows.Forms.ToolStripMenuItem TSM_OP05;
    }
}