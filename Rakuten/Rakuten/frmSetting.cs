﻿using System;
using System.Data;
using System.Windows.Forms;
using Rakuten.Utility;
using Rakuten.DAL;
using System.Data.SqlClient;
namespace Rakuten
{
    public partial class frmSetting : Form
    {
        public frmSetting()
        {
            InitializeComponent();
        }

        private void CMD_CLOSE_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CMD_CREATEDB_Click(object sender, EventArgs e)
        {
            CMD_OK.Visible = false;
            CMD_CREATEDB.Visible = true;
            panel1.Visible = false;
            panel2.Visible = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            LTIME.Text = DateTime.Now.ToString();
        }

        private void frmSetting_Load(object sender, EventArgs e)
        {
            pictureBox2.Image = null;
            groupBox2.Enabled = false;
            CMD_OK.Enabled = false;
            CMD_CREATEDB.Visible = false;
            CMD_OK.Visible = true;
            panel1.Visible = true;
            panel2.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ManageLOG.Formula(textBox1.Text + textBox2.Text + textBox3.Text + textBox4.Text))
            {
                pictureBox2.Image = Properties.Resources.CT;
                groupBox2.Enabled = true;
                textBox1.Enabled = false;
                textBox2.Enabled = false;
                textBox3.Enabled = false;
                textBox4.Enabled = false;
                SIP.Focus();
            }
            else
            {
                pictureBox2.Image = Properties.Resources.CF;
                groupBox2.Enabled = false;
                textBox1.Enabled = true;
                textBox2.Enabled = true;
                textBox3.Enabled = true;
                textBox4.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                if (con.State == ConnectionState.Open) Close();
                con.ConnectionString = Connection.TestConnection(SIP.Text, DBN.Text);
                con.Open();
                MessageBox.Show("Connected Success!");
                CMD_OK.Enabled = true;
                groupBox2.Enabled = false;
                CMD_OK.Focus();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Conect Fail!");
                SIP.Focus();
            }
        }

        private void CMD_CREATEDB_Click_1(object sender, EventArgs e)
        {
            DialogResult dr1 = MessageBox.Show("เริ่มสร้างฐานข้อมูล", "เริ่มสร้างฐานข้อมูล", MessageBoxButtons.OKCancel);
            if (dr1 == DialogResult.OK)
            {
                Cursor = Cursors.WaitCursor;
                if (Management.createScript("RakutenScript.sql"))
                {
                    if (ManageLOG.Formula(textBox1.Text + textBox2.Text + textBox3.Text + textBox4.Text))
                    {
                        pictureBox2.Image = Properties.Resources.CT;
                        ManageLOG.writeRegistry("Rakuten", "SK", ManageLOG.enCode((textBox1.Text + textBox2.Text + textBox3.Text + textBox4.Text)));
                        ManageLOG.writeRegistry("Rakuten", "HDR", ManageLOG.enCode(ManageLOG.GetHDDSerialNumber("C")));
                        ManageLOG.writeRegistry("Rakuten", "CON", ManageLOG.enCode(Connection.TestConnection(SIP.Text, DBN.Text, UN.Text, PASS.Text)));
                        Management.ShowMsg("สร้างฐานข้อมูลเรียบร้อยแล้ว\nการตั้งค่าระบบจะมีผลกับการเปิดใช้งานโปรแกรมในครั้งต่อไป");
                        Application.Exit();
                    }
                    else
                    {
                        pictureBox2.Image = Properties.Resources.CF;
                        MessageBox.Show("Serial Key ไม่ถูกต้อง");
                        pictureBox2.Image = null;
                    }
                }
                else
                {
                    Cursor = Cursors.Default;
                    Management.ShowMsg("ไม่สามารถสร้างไฟล์ฐานข้อมูลได้\nกรุณาติดต่อผู้พัฒนาระบบ");
                }
                Cursor = Cursors.Default;
            }
        }


    }
}
