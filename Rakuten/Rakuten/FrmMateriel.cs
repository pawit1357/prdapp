﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using Rakuten.Structure;
using Rakuten.DAL;
using Rakuten.Utility;
using System.Data.SqlClient;

namespace Rakuten
{
    public partial class FrmMateriel : Form
    {
        private List<CScent> lists = null;
        public FrmMateriel()
        {
            InitializeComponent();
        }

        private void FrmMateriel_Load(object sender, EventArgs e)
        {
            initial();//Initial Data
        }

        private void FrmMateriel_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                //Enable Tab
                tabControl1.TabPages.Add(tabPage2);
                tabPage2.Controls.Add(panel1);
                tabControl1.SelectTab(1);
                //Initial Data
                TXT_ID.Text = lists[e.RowIndex].ID;
                TXT_DETAIL.Text = lists[e.RowIndex].DETAIL;
                TXT_STOCKREMAIN.Text = lists[e.RowIndex].Count.ToString();
                TXT_COUNT.Focus();
            }
        }

        private void B_SAVE_Click(object sender, EventArgs e)
        {
            CScent scent = new CScent
              {
                  ID = TXT_ID.Text,
                  ScentType = TXT_ID.Text,
                  ProgramType = "1",//เพิ่ม
                  Count = Convert.ToInt16(TXT_COUNT.Text),
                  KEYUSER = Authorize.getUser(),
                  LASTEDIT = DateTime.Now,
                  FLAG = Management.Insert
              };
            if (DALScent.manageMaster1(scent) > 0)
            {
                Management.ShowMsg(Management.INSERT_SUCCESS);
                initial();
            }
        }

        private void B_CANCEL_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void initial()
        {
            //Clear Text Box
            TXT_ID.Text = "";
            TXT_DETAIL.Text = "";
            TXT_STOCKREMAIN.Text = "0";
            TXT_COUNT.Text = "0";
            tabControl1.TabPages.RemoveAt(1);//Hide Tab
            lists = DALScent.getList1(new CScent
            {
                ID = "",
                DETAIL = "",
                ScentType = "",
                ProgramType = "",
                Min = 0,
                Max = 0,
                KEYDATE = DateTime.Now,
                KEYUSER = Authorize.getUser(),
                LASTEDIT = DateTime.Now,
                FLAG = "4"//ดูจำนวนคงเหลือแยกตาม ประเภท
            });
            dataGridView1.DataSource = lists; //Initial Data Grid View
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
            //switch (e.ColumnIndex)
            //{ 
            //    case 14:
            //        e.Value = ((Convert.ToInt16(dataGridView1.CurrentRow.Cells[4].Value) <= Convert.ToInt16(dataGridView1.CurrentRow.Cells[5].Value)) ? "น้ำหอมใกล้หมด" : "-");
            //        break;
            //}
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                    initial();
                    break;
            }
        }
    }
}
