﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using VehicleViewLIB;

namespace VehicleView
{
    public partial class StaticHistoryViewFilterSetting : Form
    {
        public StaticHistoryViewFilterSetting(DataFilters defaultFilter)
        {
            InitializeComponent();
            InitCombo_StationID(defaultFilter);
            InitOthers(defaultFilter);
            this.Deactivate += new EventHandler(StaticHistoryViewFilterSetting_Deactivate);
        }

        void StaticHistoryViewFilterSetting_Deactivate(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        public DataFilters OutputDataFilter = new DataFilters();
       
        List<StationItem> list = new List<StationItem>();
        Color selectionBackColor = Color.DarkGray;

        class StationItem
        {
            public int StationID { get; set; }
            public string StationName { get; set; }
        }

        void comboBox_StationID_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0)
                return;

            ComboBox combo = sender as ComboBox;
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                e.Graphics.FillRectangle(new SolidBrush(selectionBackColor),
                                         e.Bounds);
            else
                e.Graphics.FillRectangle(new SolidBrush(combo.BackColor),
                                         e.Bounds);

            e.Graphics.DrawString(list[e.Index].StationName, e.Font,
                                  new SolidBrush(combo.ForeColor),
                                  new Point(e.Bounds.X, e.Bounds.Y));

            e.DrawFocusRectangle();
        }

        private void InitCombo_StationID(DataFilters filter)
        {
            this.comboBox_StationID.DrawMode = DrawMode.OwnerDrawFixed;
            this.comboBox_StationID.DrawItem += new DrawItemEventHandler(comboBox_StationID_DrawItem);
        
            string connectionString = AppParameters.ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "SELECT * FROM Station WHERE StationID > 0";
                DataTable table = Utils.GetDataTable(sql);

                if (table.Rows.Count > 0)
                {
                    StationItem st0 = new StationItem();
                    st0.StationID = 0;
                    st0.StationName = "ALL Stations";

                    list.Add(st0);

                    foreach (DataRow r in table.Rows)
                    {
                        StationItem st = new StationItem();
                        st.StationID = (int)r["StationID"];
                        st.StationName = (string)r["StationName"];
                        list.Add(st);
                    }

                    BindingSource bs = new BindingSource();
                    bs.DataSource = list;

                    this.comboBox_StationID.Items.Clear();
                    this.comboBox_StationID.DataSource = list;
                    this.comboBox_StationID.DisplayMember = "StationName";
                    this.comboBox_StationID.ValueMember = "StationID";

                    int stid = 0;
                    Int32.TryParse(filter.stationID, out stid);
                    this.comboBox_StationID.SelectedIndex = stid;
                }
            
            }
        }

        private void InitOthers(DataFilters filter)
        { 
            DateTime dt1 = DateTime.Today;
            DateTime dt2 = DateTime.Now;

            if (filter.startTimeStamp != string.Empty)
            {
                DateTime.TryParse(filter.startTimeStamp, out dt1);
            }
            if (filter.endTimeStamp != string.Empty)
            {
                DateTime.TryParse(filter.endTimeStamp, out dt2);
            }

            this.dateTimePicker_StartDate.Value = dt1;
            this.dateTimePicker_StartTime.Value = dt1;
            this.dateTimePicker_EndDate.Value = dt2;
            this.dateTimePicker_EndTime.Value = dt2;

            int vc = 1;
            Int32.TryParse(filter.vehClass, out vc);
            if (vc == 0) vc = 1;
            this.numericUpDown_VehClass_ID.Value = vc;
            this.checkBox_VehClass_ALL.Checked = (filter.vehClass == string.Empty) ? true : false;

            int lane = 0;
            Int32.TryParse(filter.lane, out lane);
            this.numericUpDown_LaneID.Value = lane + 1;
            this.checkBox_Lane_All.Checked = (filter.lane == string.Empty) ? true : false;
     
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            SetOutputDataFilter();
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void SetOutputDataFilter()
        {
            StationItem item = (StationItem)this.comboBox_StationID.SelectedItem;
            this.OutputDataFilter.stationID = item.StationID.ToString();
            this.OutputDataFilter.stationName = item.StationName;

            this.OutputDataFilter.startTimeStamp =
                GetDateTimeString(this.dateTimePicker_StartDate.Value, this.dateTimePicker_StartTime.Value);
            this.OutputDataFilter.endTimeStamp =
                GetDateTimeString(this.dateTimePicker_EndDate.Value, this.dateTimePicker_EndTime.Value);

            int v = (int)this.numericUpDown_VehClass_ID.Value;
            this.OutputDataFilter.vehClass = (this.checkBox_VehClass_ALL.Checked)?string.Empty:v.ToString();

            int n = (int)this.numericUpDown_LaneID.Value;
            this.OutputDataFilter.lane = (this.checkBox_Lane_All.Checked) ? string.Empty : (n-1).ToString();
        }

        string GetDateTimeString(DateTime dt, DateTime tm)
        {
            int y = dt.Year;
            int M = dt.Month;
            int d = dt.Day;
            int h = tm.Hour;
            int m = tm.Minute;
            return new DateTime(y, M, d, h, m, 0).ToString("s");
        }

        private void checkBox_VehClass_ALL_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            this.numericUpDown_VehClass_ID.Visible = (cb.Checked) ? false : true;
        }

        private void checkBox_Lane_All_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            this.numericUpDown_LaneID.Visible = (cb.Checked) ? false : true;
        }
    }
}
