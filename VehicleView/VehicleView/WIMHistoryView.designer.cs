﻿namespace VehicleView
{
    partial class WIMHistoryView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.vehicleDataView1 = new VehicleViewLIB.VehicleDataView();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label_COL3_Title = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label_COL1_Title = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.clock1 = new VehicleViewLIB.Clock();
            this.label_StationName = new System.Windows.Forms.Label();
            this.label_MAIN_Title = new System.Windows.Forms.Label();
            this.button_Goto = new System.Windows.Forms.Button();
            this.button_Filters = new System.Windows.Forms.Button();
            this.button_Exit = new System.Windows.Forms.Button();
            this.button_Refresh = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel14);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1584, 812);
            this.panel1.TabIndex = 1;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.DimGray;
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(1574, 40);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(10, 709);
            this.panel12.TabIndex = 23;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.DimGray;
            this.panel8.Controls.Add(this.vehicleDataView1);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(990, 40);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(594, 709);
            this.panel8.TabIndex = 20;
            // 
            // vehicleDataView1
            // 
            this.vehicleDataView1.BackColor = System.Drawing.Color.DimGray;
            this.vehicleDataView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vehicleDataView1.Location = new System.Drawing.Point(0, 45);
            this.vehicleDataView1.Name = "vehicleDataView1";
            this.vehicleDataView1.Size = new System.Drawing.Size(594, 664);
            this.vehicleDataView1.TabIndex = 2;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.DimGray;
            this.panel9.Controls.Add(this.label_COL3_Title);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(594, 45);
            this.panel9.TabIndex = 0;
            // 
            // label_COL3_Title
            // 
            this.label_COL3_Title.AutoSize = true;
            this.label_COL3_Title.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_COL3_Title.ForeColor = System.Drawing.Color.LightGray;
            this.label_COL3_Title.Location = new System.Drawing.Point(17, 15);
            this.label_COL3_Title.Name = "label_COL3_Title";
            this.label_COL3_Title.Size = new System.Drawing.Size(45, 19);
            this.label_COL3_Title.TabIndex = 4;
            this.label_COL3_Title.Text = "DATA";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.DimGray;
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(980, 40);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(10, 709);
            this.panel11.TabIndex = 22;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.DimGray;
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(970, 40);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(10, 709);
            this.panel10.TabIndex = 21;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dataGridView1);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(10, 40);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(960, 709);
            this.panel4.TabIndex = 18;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.DimGray;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 26;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.Color.DimGray;
            this.dataGridView1.Location = new System.Drawing.Point(0, 45);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowCellErrors = false;
            this.dataGridView1.ShowCellToolTips = false;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.ShowRowErrors = false;
            this.dataGridView1.Size = new System.Drawing.Size(960, 664);
            this.dataGridView1.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DimGray;
            this.panel5.Controls.Add(this.label_COL1_Title);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(960, 45);
            this.panel5.TabIndex = 0;
            // 
            // label_COL1_Title
            // 
            this.label_COL1_Title.AutoSize = true;
            this.label_COL1_Title.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_COL1_Title.ForeColor = System.Drawing.Color.LightGray;
            this.label_COL1_Title.Location = new System.Drawing.Point(6, 15);
            this.label_COL1_Title.Name = "label_COL1_Title";
            this.label_COL1_Title.Size = new System.Drawing.Size(94, 19);
            this.label_COL1_Title.TabIndex = 4;
            this.label_COL1_Title.Tag = "VEHICLE LIST";
            this.label_COL1_Title.Text = "VEHICLE LIST";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DimGray;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(10, 749);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1574, 63);
            this.panel3.TabIndex = 17;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.DimGray;
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 40);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(10, 772);
            this.panel14.TabIndex = 16;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel2.Controls.Add(this.clock1);
            this.panel2.Controls.Add(this.label_StationName);
            this.panel2.Controls.Add(this.label_MAIN_Title);
            this.panel2.Controls.Add(this.button_Goto);
            this.panel2.Controls.Add(this.button_Filters);
            this.panel2.Controls.Add(this.button_Exit);
            this.panel2.Controls.Add(this.button_Refresh);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1584, 40);
            this.panel2.TabIndex = 15;
            // 
            // clock1
            // 
            this.clock1.Location = new System.Drawing.Point(340, 5);
            this.clock1.Name = "clock1";
            this.clock1.Size = new System.Drawing.Size(172, 30);
            this.clock1.TabIndex = 9;
            // 
            // label_StationName
            // 
            this.label_StationName.AutoSize = true;
            this.label_StationName.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_StationName.ForeColor = System.Drawing.Color.Black;
            this.label_StationName.Location = new System.Drawing.Point(185, 8);
            this.label_StationName.Name = "label_StationName";
            this.label_StationName.Size = new System.Drawing.Size(100, 23);
            this.label_StationName.TabIndex = 6;
            this.label_StationName.Text = "All Stations";
            // 
            // label_MAIN_Title
            // 
            this.label_MAIN_Title.AutoSize = true;
            this.label_MAIN_Title.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MAIN_Title.ForeColor = System.Drawing.Color.Black;
            this.label_MAIN_Title.Location = new System.Drawing.Point(6, 8);
            this.label_MAIN_Title.Name = "label_MAIN_Title";
            this.label_MAIN_Title.Size = new System.Drawing.Size(167, 23);
            this.label_MAIN_Title.TabIndex = 2;
            this.label_MAIN_Title.Text = "WIM HISTORY VIEW";
            // 
            // button_Goto
            // 
            this.button_Goto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Goto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Goto.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Goto.ForeColor = System.Drawing.Color.Black;
            this.button_Goto.Location = new System.Drawing.Point(1392, 7);
            this.button_Goto.Name = "button_Goto";
            this.button_Goto.Size = new System.Drawing.Size(80, 26);
            this.button_Goto.TabIndex = 5;
            this.button_Goto.TabStop = false;
            this.button_Goto.Text = "Go to";
            this.button_Goto.UseVisualStyleBackColor = true;
            this.button_Goto.Click += new System.EventHandler(this.button_Goto_Click);
            // 
            // button_Filters
            // 
            this.button_Filters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Filters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Filters.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Filters.ForeColor = System.Drawing.Color.Black;
            this.button_Filters.Location = new System.Drawing.Point(1202, 7);
            this.button_Filters.Name = "button_Filters";
            this.button_Filters.Size = new System.Drawing.Size(80, 26);
            this.button_Filters.TabIndex = 4;
            this.button_Filters.TabStop = false;
            this.button_Filters.Text = "Filters";
            this.button_Filters.UseVisualStyleBackColor = true;
            this.button_Filters.Click += new System.EventHandler(this.button_Filters_Click);
            // 
            // button_Exit
            // 
            this.button_Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Exit.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Exit.ForeColor = System.Drawing.Color.Black;
            this.button_Exit.Location = new System.Drawing.Point(1478, 7);
            this.button_Exit.Name = "button_Exit";
            this.button_Exit.Size = new System.Drawing.Size(80, 26);
            this.button_Exit.TabIndex = 3;
            this.button_Exit.TabStop = false;
            this.button_Exit.Text = "Exit";
            this.button_Exit.UseVisualStyleBackColor = true;
            this.button_Exit.Click += new System.EventHandler(this.button_Exit_Click);
            // 
            // button_Refresh
            // 
            this.button_Refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Refresh.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Refresh.ForeColor = System.Drawing.Color.Black;
            this.button_Refresh.Location = new System.Drawing.Point(1288, 7);
            this.button_Refresh.Name = "button_Refresh";
            this.button_Refresh.Size = new System.Drawing.Size(80, 26);
            this.button_Refresh.TabIndex = 1;
            this.button_Refresh.TabStop = false;
            this.button_Refresh.Text = "Refresh";
            this.button_Refresh.UseVisualStyleBackColor = true;
            this.button_Refresh.Click += new System.EventHandler(this.button_Refresh_Click);
            // 
            // WIMHistoryView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1584, 812);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WIMHistoryView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "WIM History View";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel8;
        private VehicleViewLIB.VehicleDataView vehicleDataView1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label_COL3_Title;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label_COL1_Title;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label_StationName;
        private System.Windows.Forms.Label label_MAIN_Title;
        private System.Windows.Forms.Button button_Goto;
        private System.Windows.Forms.Button button_Filters;
        private System.Windows.Forms.Button button_Exit;
        private System.Windows.Forms.Button button_Refresh;
        private System.Windows.Forms.DataGridView dataGridView1;
        private VehicleViewLIB.Clock clock1;




    }
}

