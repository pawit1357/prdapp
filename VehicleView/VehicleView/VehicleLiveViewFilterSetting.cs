﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using VehicleViewLIB;

namespace VehicleView
{
    public partial class VehicleLiveViewFilterSetting : Form
    {      
        public int StationID { get; set; }
        public string StationName { get; set; }
             
        public VehicleLiveViewFilterSetting()
        {
            StationID = Properties.Settings.Default.StationID;
            InitializeComponent();
            InitDataGridView();
            this.Deactivate += new EventHandler(VehicleLiveViewFilterSetting_Deactivate);
        }

        void VehicleLiveViewFilterSetting_Deactivate(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
       
        private void InitDataGridView()
        {
            this.dataGridView1.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dataGridView1.DataSource = null;
            this.dataGridView1.Rows.Clear();

            string sql = "SELECT * FROM Station WHERE StationID > 0";
            DataTable table = Utils.GetDataTable(sql);

            if (table.Rows.Count > 0)
            {
                int r0 = this.dataGridView1.Rows.Add();
                this.dataGridView1.Rows[r0].Cells[0].Value = 0;
                this.dataGridView1.Rows[r0].Cells[1].Value = "All Stations";

                foreach (DataRow row in table.Rows)
                {
                    int r = this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[r].Cells[0].Value = row["StationID"];
                    this.dataGridView1.Rows[r].Cells[1].Value = row["StationName"];
                }
                if (this.dataGridView1.Rows.Count > StationID)
                {
                    dataGridView1.CurrentCell = dataGridView1.Rows[StationID].Cells[1]; 
                }
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;

            if (this.dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                StationID = (int)row.Cells[0].Value;
                StationName = (string)row.Cells[1].Value;
                Properties.Settings.Default["StationID"] = StationID;
                Properties.Settings.Default.Save();
            }

            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

       
    }
}
