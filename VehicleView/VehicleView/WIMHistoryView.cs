﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using VehicleViewLIB;

namespace VehicleView
{
    public partial class WIMHistoryView : Form
    {
        DataFilters defaultfilter = new DataFilters();
       
        public WIMHistoryView()
        {
            InitializeComponent();
            InitDataGridView();
            SetDefaultFilter();
            RefreshView();
        }

        void SetDefaultFilter()
        {
            defaultfilter.startTimeStamp = DateTime.Now.AddDays(-1).ToString("s");
            defaultfilter.endTimeStamp = DateTime.Now.ToString("s");
        }

        private void InitDataGridView()
        {
            string[] colName = new string[]
            {
                "WIMID",
                "StationName",  "TimeStamp",    "VehicleNumber",   
                "Lane",         "VehicleClass", "GVW",          
                "MaxGVW",       "SortDecision", "StatusCode"       
            };

            string[] colTitle = new string[]
            {
                "WIMID",
                "Station",  "Date-Time",    "Veh Number",   
                "Lane",         "Class", "GVW",          
                "Max GVW",       "Sort Decision", "Status"       
            };

            int[] colWidth = new int[]
            {
                0,
                120,    160,    100,   
                50,     50,     80,          
                80,     100,    200      
            };

            for (int i = 0; i < colName.Length; i++)
            {
                DataGridViewTextBoxColumn col = new DataGridViewTextBoxColumn();
                col.Name = colName[i];
                col.DataPropertyName = colName[i];
                col.HeaderText = colTitle[i];
                col.Width = colWidth[i];

                if (colName[i] == "WIMID")
                {
                    col.Visible = false;
                }
                if (colName[i] == "TimeStamp")
                {
                    col.DefaultCellStyle.Format = "dd-MM-yyyy   HH:mm:ss";
                }
                else if (colName[i] == "Lane" || colName[i] == "VehicleClass")
                {
                    col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                else if (colName[i] == "GVW")
                {
                    col.DefaultCellStyle.Format = "#,###";
                    col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else if (colName[i] == "MaxGVW")
                {
                    col.DefaultCellStyle.Format = "#,###";
                    col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else if (colName[i] == "StatusCode")
                {
                    col.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                }
                else if (colName[i] == "SortDecision")
                {
                    col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                this.dataGridView1.Columns.Add(col);
            }

            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.CellFormatting += new DataGridViewCellFormattingEventHandler(dataGridView1_CellFormatting);
            this.dataGridView1.SelectionChanged += new EventHandler(dataGridView1_SelectionChanged);
            this.dataGridView1.CellPainting += new DataGridViewCellPaintingEventHandler(dataGridView1_CellPainting);
        }

        void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                int size = 2;
                int x = e.CellBounds.Left;
                int y = e.CellBounds.Top;
                int w = e.CellBounds.Width;
                int h = e.ClipBounds.Height;

                Rectangle bounds = new Rectangle(x,y+h-size,w,size);                
                e.Paint(e.ClipBounds, (DataGridViewPaintParts.All));
                e.Graphics.FillRectangle(Brushes.CornflowerBlue, bounds);
                e.Handled = true;
            }
        }

        void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (dgv.SelectedRows.Count > 0)
            {
                long wimID = (long)dgv.SelectedRows[0].Cells["WIMID"].Value;                        
                this.vehicleDataView1.RefreshView(ViewType.WIM, wimID.ToString());
                this.vehicleDataView1.Visible = true;
            }           
        }

        void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {            
            if (e.Value == null)
                return;

            if (this.dataGridView1.Columns[e.ColumnIndex].Name == "Lane")
            {
                int value = (int)(byte)e.Value;
                e.Value = (value+1).ToString();
                e.FormattingApplied = true;
            }
            if (this.dataGridView1.Columns[e.ColumnIndex].Name == "SortDecision")
            {
                VRSortDecisionCode value = (VRSortDecisionCode)(byte)e.Value;
                e.Value = value.ToString();
                e.FormattingApplied = true;
            }
            else if (this.dataGridView1.Columns[e.ColumnIndex].Name == "StatusCode")
            {
                VRStatusCode value = (VRStatusCode)(int)e.Value;
                e.Value = Utils.GetStatusCodeText(value).Replace("\r\n","  ");
                e.FormattingApplied = true;
            }         
        }

        private void RefreshView()
        {
            try
            {
                this.vehicleDataView1.Visible = false;
                this.vehicleDataView1.Clear();
                this.dataGridView1.DataSource = null;

                label_StationName.Text = string.Empty;
                label_StationName.Text = defaultfilter.stationName;
                
                string sql = SQLText.GetSQLText_WIM(defaultfilter);
                DataTable table = Utils.GetDataTable(sql);
                this.dataGridView1.DataSource = table;

                int n = table.Rows.Count;
                label_COL1_Title.Text = (string)label_COL1_Title.Tag + "    (" + n.ToString() + ")";

            }
            catch (Exception ex)
            {
                MessageBox.Show("EXCEPTION: RefreshView" + "\r\n" + ex.Message); 
            }
        }

        private void button_Filters_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<WIMHistoryViewFilterSetting>().Any())
            {
                return;
            }

            WIMHistoryViewFilterSetting settingArea = new WIMHistoryViewFilterSetting(defaultfilter);

            settingArea.Top = this.panel2.Top + this.panel2.Height;
            settingArea.Left = this.button_Filters.Left - settingArea.Width + this.button_Filters.Width;

            settingArea.FormClosed += new FormClosedEventHandler(WIMHistoryViewFilterSetting_FormClosed);
            settingArea.Show();           
        }

        void WIMHistoryViewFilterSetting_FormClosed(object sender, FormClosedEventArgs e)
        {
            WIMHistoryViewFilterSetting settingArea = (WIMHistoryViewFilterSetting)sender;
            if (settingArea.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                this.defaultfilter = settingArea.OutputDataFilter;
                RefreshView();
            }
            else
            {
                Application.DoEvents();
            }
        }

        private void button_Refresh_Click(object sender, EventArgs e)
        {
            RefreshView();
        }

        private void button_Goto_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<GotoSetting>().Any())
            {
                return;
            }

            GotoSetting settingArea = new GotoSetting(1); //Call from wim = 1
            settingArea.Top = this.panel2.Top + this.panel2.Height;
            settingArea.Left = this.button_Goto.Left - settingArea.Width + this.button_Goto.Width;

            settingArea.FormClosed += new FormClosedEventHandler(GotoSetting_FormClosed);
            settingArea.Show();
        }

        void GotoSetting_FormClosed(object sender, FormClosedEventArgs e)
        {
            GotoSetting settingArea = (GotoSetting)sender;
            if (settingArea.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                Timer timer1 = new Timer();
                timer1.Tick += new EventHandler(timer1_Tick);
                timer1.Tag = settingArea.Selection;
                timer1.Interval = 200;
                timer1.Start();             
            }
            else
            {
                Application.DoEvents();
            }
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            Timer timer1 = (Timer)sender;
            timer1.Stop();
            int selection = (int)timer1.Tag;
            timer1.Dispose();

            Control owner = this.Owner;
            this.Close();

            if (selection == 1)
            {              
            }
            else if (selection == 2)
            {
                owner.Cursor = Cursors.WaitCursor;
                StaticHistoryView f = new StaticHistoryView();
                f.Show(owner);
                owner.Cursor = Cursors.Default;
            }
            else if (selection == 3)
            {
                owner.Cursor = Cursors.WaitCursor;
                WIMStationControl f = new WIMStationControl();
                f.Show(owner);
                owner.Cursor = Cursors.Default;
            }
        }

        private void button_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
    }
}

