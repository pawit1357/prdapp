﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VehicleView
{
    public partial class LayoutSetting : Form
    {
        public int LayoutID { get; set; }

        public LayoutSetting(List<string[]> layoutList)
        {
            LayoutID = Properties.Settings.Default.LayoutID;
            InitializeComponent();
            InitDatagridView(layoutList);
            this.Deactivate += new EventHandler(LayoutSetting_Deactivate);
        }

        void LayoutSetting_Deactivate(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

     
        private void InitDatagridView(List<string[]> layoutList)
        {
            this.dataGridView1.DataSource = null;
            this.dataGridView1.Rows.Clear();

            foreach (string[] items in layoutList)
            {
                int r = this.dataGridView1.Rows.Add();
                this.dataGridView1.Rows[r].Cells[0].Value = items[0].Trim();
                this.dataGridView1.Rows[r].Cells[1].Value = items[1].Trim();
                this.dataGridView1.Rows[r].Cells[2].Value = items[2].Trim();
                this.dataGridView1.Rows[r].Cells[3].Value = items[3].Trim();
            }

            if (this.dataGridView1.Rows.Count > LayoutID)
            {
                dataGridView1.CurrentCell = dataGridView1.Rows[LayoutID].Cells[0];              
            }
        }
       
        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            if (this.dataGridView1.SelectedRows.Count > 0)
            {
                LayoutID = this.dataGridView1.SelectedRows[0].Index;
                Properties.Settings.Default["LayoutID"] = LayoutID;
                Properties.Settings.Default.Save();
            }
            
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
   
    }
}
