﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VehicleView
{
    class AppSettings
    {
        public static void Initialize()
        {
            VehicleViewLIB.AppParameters.DataLocation = Properties.Settings.Default.DataLocation;
            VehicleViewLIB.AppParameters.LogLocation = Properties.Settings.Default.LogLocation;
            VehicleViewLIB.AppParameters.ConnectionString = Properties.Settings.Default.ConnectionString;
            VehicleViewLIB.AppParameters.ImageDelay = Properties.Settings.Default.ImageDelay;
            VehicleViewLIB.AppParameters.ImageURL = Properties.Settings.Default.ImageURL;
            VehicleViewLIB.AppParameters.APPFolderName = "VehicleView";
        }
    }
}
