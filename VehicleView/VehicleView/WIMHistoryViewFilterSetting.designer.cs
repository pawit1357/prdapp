﻿namespace VehicleView
{
    partial class WIMHistoryViewFilterSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_SettingArea = new System.Windows.Forms.Panel();
            this.checkBox_Lane_All = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown_LaneID = new System.Windows.Forms.NumericUpDown();
            this.dateTimePicker_EndTime = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_EndDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_StartTime = new System.Windows.Forms.DateTimePicker();
            this.checkBox_VehClass_ALL = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_VehClass_ID = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker_StartDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.comboBox_StationID = new System.Windows.Forms.ComboBox();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.pageSetupDialog2 = new System.Windows.Forms.PageSetupDialog();
            this.panel_SettingArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LaneID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_VehClass_ID)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_SettingArea
            // 
            this.panel_SettingArea.BackColor = System.Drawing.Color.LightGray;
            this.panel_SettingArea.Controls.Add(this.checkBox_Lane_All);
            this.panel_SettingArea.Controls.Add(this.label3);
            this.panel_SettingArea.Controls.Add(this.numericUpDown_LaneID);
            this.panel_SettingArea.Controls.Add(this.dateTimePicker_EndTime);
            this.panel_SettingArea.Controls.Add(this.dateTimePicker_EndDate);
            this.panel_SettingArea.Controls.Add(this.dateTimePicker_StartTime);
            this.panel_SettingArea.Controls.Add(this.checkBox_VehClass_ALL);
            this.panel_SettingArea.Controls.Add(this.label2);
            this.panel_SettingArea.Controls.Add(this.numericUpDown_VehClass_ID);
            this.panel_SettingArea.Controls.Add(this.label1);
            this.panel_SettingArea.Controls.Add(this.dateTimePicker_StartDate);
            this.panel_SettingArea.Controls.Add(this.label6);
            this.panel_SettingArea.Controls.Add(this.label5);
            this.panel_SettingArea.Controls.Add(this.buttonCancel);
            this.panel_SettingArea.Controls.Add(this.buttonOK);
            this.panel_SettingArea.Controls.Add(this.comboBox_StationID);
            this.panel_SettingArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_SettingArea.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel_SettingArea.Location = new System.Drawing.Point(0, 0);
            this.panel_SettingArea.Name = "panel_SettingArea";
            this.panel_SettingArea.Size = new System.Drawing.Size(330, 430);
            this.panel_SettingArea.TabIndex = 5;
            // 
            // checkBox_Lane_All
            // 
            this.checkBox_Lane_All.AutoSize = true;
            this.checkBox_Lane_All.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.checkBox_Lane_All.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_Lane_All.Location = new System.Drawing.Point(166, 247);
            this.checkBox_Lane_All.Name = "checkBox_Lane_All";
            this.checkBox_Lane_All.Size = new System.Drawing.Size(42, 23);
            this.checkBox_Lane_All.TabIndex = 21;
            this.checkBox_Lane_All.TabStop = false;
            this.checkBox_Lane_All.Text = "All";
            this.checkBox_Lane_All.UseVisualStyleBackColor = true;
            this.checkBox_Lane_All.CheckedChanged += new System.EventHandler(this.checkBox_Lane_All_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(38, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 19);
            this.label3.TabIndex = 20;
            this.label3.Text = "Lane";
            // 
            // numericUpDown_LaneID
            // 
            this.numericUpDown_LaneID.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numericUpDown_LaneID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown_LaneID.Location = new System.Drawing.Point(239, 247);
            this.numericUpDown_LaneID.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDown_LaneID.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_LaneID.Name = "numericUpDown_LaneID";
            this.numericUpDown_LaneID.Size = new System.Drawing.Size(53, 27);
            this.numericUpDown_LaneID.TabIndex = 19;
            this.numericUpDown_LaneID.TabStop = false;
            this.numericUpDown_LaneID.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dateTimePicker_EndTime
            // 
            this.dateTimePicker_EndTime.CalendarForeColor = System.Drawing.Color.Black;
            this.dateTimePicker_EndTime.CalendarMonthBackground = System.Drawing.Color.DarkGray;
            this.dateTimePicker_EndTime.CustomFormat = "HH : mm";
            this.dateTimePicker_EndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_EndTime.Location = new System.Drawing.Point(214, 159);
            this.dateTimePicker_EndTime.Name = "dateTimePicker_EndTime";
            this.dateTimePicker_EndTime.ShowUpDown = true;
            this.dateTimePicker_EndTime.Size = new System.Drawing.Size(78, 27);
            this.dateTimePicker_EndTime.TabIndex = 18;
            this.dateTimePicker_EndTime.TabStop = false;
            // 
            // dateTimePicker_EndDate
            // 
            this.dateTimePicker_EndDate.CalendarForeColor = System.Drawing.Color.Black;
            this.dateTimePicker_EndDate.CalendarMonthBackground = System.Drawing.Color.DarkGray;
            this.dateTimePicker_EndDate.CustomFormat = "dd MMM yyyy ";
            this.dateTimePicker_EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_EndDate.Location = new System.Drawing.Point(42, 159);
            this.dateTimePicker_EndDate.Name = "dateTimePicker_EndDate";
            this.dateTimePicker_EndDate.Size = new System.Drawing.Size(166, 27);
            this.dateTimePicker_EndDate.TabIndex = 17;
            this.dateTimePicker_EndDate.TabStop = false;
            // 
            // dateTimePicker_StartTime
            // 
            this.dateTimePicker_StartTime.CalendarForeColor = System.Drawing.Color.Black;
            this.dateTimePicker_StartTime.CalendarMonthBackground = System.Drawing.Color.DarkGray;
            this.dateTimePicker_StartTime.CustomFormat = "HH : mm";
            this.dateTimePicker_StartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_StartTime.Location = new System.Drawing.Point(214, 100);
            this.dateTimePicker_StartTime.Name = "dateTimePicker_StartTime";
            this.dateTimePicker_StartTime.ShowUpDown = true;
            this.dateTimePicker_StartTime.Size = new System.Drawing.Size(78, 27);
            this.dateTimePicker_StartTime.TabIndex = 16;
            this.dateTimePicker_StartTime.TabStop = false;
            // 
            // checkBox_VehClass_ALL
            // 
            this.checkBox_VehClass_ALL.AutoSize = true;
            this.checkBox_VehClass_ALL.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.checkBox_VehClass_ALL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_VehClass_ALL.Location = new System.Drawing.Point(166, 213);
            this.checkBox_VehClass_ALL.Name = "checkBox_VehClass_ALL";
            this.checkBox_VehClass_ALL.Size = new System.Drawing.Size(42, 23);
            this.checkBox_VehClass_ALL.TabIndex = 15;
            this.checkBox_VehClass_ALL.TabStop = false;
            this.checkBox_VehClass_ALL.Text = "All";
            this.checkBox_VehClass_ALL.UseVisualStyleBackColor = true;
            this.checkBox_VehClass_ALL.CheckedChanged += new System.EventHandler(this.checkBox_VehClass_ALL_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 19);
            this.label2.TabIndex = 14;
            this.label2.Text = "Vehicle Class";
            // 
            // numericUpDown_VehClass_ID
            // 
            this.numericUpDown_VehClass_ID.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numericUpDown_VehClass_ID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown_VehClass_ID.Location = new System.Drawing.Point(239, 213);
            this.numericUpDown_VehClass_ID.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDown_VehClass_ID.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_VehClass_ID.Name = "numericUpDown_VehClass_ID";
            this.numericUpDown_VehClass_ID.Size = new System.Drawing.Size(53, 27);
            this.numericUpDown_VehClass_ID.TabIndex = 13;
            this.numericUpDown_VehClass_ID.TabStop = false;
            this.numericUpDown_VehClass_ID.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 19);
            this.label1.TabIndex = 12;
            this.label1.Text = "To";
            // 
            // dateTimePicker_StartDate
            // 
            this.dateTimePicker_StartDate.CalendarForeColor = System.Drawing.Color.Black;
            this.dateTimePicker_StartDate.CalendarMonthBackground = System.Drawing.Color.DarkGray;
            this.dateTimePicker_StartDate.CustomFormat = "dd MMM yyyy ";
            this.dateTimePicker_StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_StartDate.Location = new System.Drawing.Point(42, 100);
            this.dateTimePicker_StartDate.Name = "dateTimePicker_StartDate";
            this.dateTimePicker_StartDate.Size = new System.Drawing.Size(166, 27);
            this.dateTimePicker_StartDate.TabIndex = 10;
            this.dateTimePicker_StartDate.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(38, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 19);
            this.label6.TabIndex = 9;
            this.label6.Text = "From";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(38, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 19);
            this.label5.TabIndex = 8;
            this.label5.Text = "Station";
            // 
            // buttonCancel
            // 
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Location = new System.Drawing.Point(170, 374);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(100, 30);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.TabStop = false;
            this.buttonCancel.Text = "CANCEL";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOK.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOK.ForeColor = System.Drawing.Color.Black;
            this.buttonOK.Location = new System.Drawing.Point(60, 374);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(100, 30);
            this.buttonOK.TabIndex = 6;
            this.buttonOK.TabStop = false;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // comboBox_StationID
            // 
            this.comboBox_StationID.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboBox_StationID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_StationID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_StationID.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_StationID.ForeColor = System.Drawing.Color.Black;
            this.comboBox_StationID.FormattingEnabled = true;
            this.comboBox_StationID.Location = new System.Drawing.Point(42, 42);
            this.comboBox_StationID.Name = "comboBox_StationID";
            this.comboBox_StationID.Size = new System.Drawing.Size(250, 26);
            this.comboBox_StationID.TabIndex = 0;
            this.comboBox_StationID.TabStop = false;
            // 
            // WIMHistoryViewFilterSetting
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(330, 430);
            this.Controls.Add(this.panel_SettingArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WIMHistoryViewFilterSetting";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "WIMHistoryViewSettingArea";
            this.panel_SettingArea.ResumeLayout(false);
            this.panel_SettingArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LaneID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_VehClass_ID)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_SettingArea;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.ComboBox comboBox_StationID;
        private System.Windows.Forms.DateTimePicker dateTimePicker_StartDate;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog2;
        private System.Windows.Forms.CheckBox checkBox_VehClass_ALL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown_VehClass_ID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker_StartTime;
        private System.Windows.Forms.DateTimePicker dateTimePicker_EndTime;
        private System.Windows.Forms.DateTimePicker dateTimePicker_EndDate;
        private System.Windows.Forms.CheckBox checkBox_Lane_All;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDown_LaneID;
    }
}