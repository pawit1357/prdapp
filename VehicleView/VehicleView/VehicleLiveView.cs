﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using VehicleViewLIB;

namespace VehicleView
{
    public partial class VehicleLiveView : Form
    {
        readonly int MAXROW = 30;

        int StationID = 0;
        string StationName = string.Empty;
        int LayoutID = 0;
        
        readonly string a_wim_1 = "WIM-1";
        readonly string a_wim_2 = "WIM-2";
        readonly string a_report = "REPORT";
        readonly string a_repbyp = "REPORT-BYPASS";
        readonly string a_bypass = "BYPASS";
        readonly string a_static = "STATIC";
        readonly string a_data = "DATA";

        public List<string[]> LayoutList = new List<string[]>();
       
        public VehicleLiveView()
        {
            InitializeComponent();
            GetDefaultParam();
            InitLayoutList();
            this.SizeChanged += new EventHandler(VehicleLiveView_SizeChanged);
            InitializeLiveView();

            this.button1.ForeColor = Color.Gray;
            this.button2.ForeColor = Color.Black;
            this.button1.Enabled = false;
            this.button2.Enabled = true;
        }

        void VehicleLiveView_SizeChanged(object sender, EventArgs e)
        {
            this.vehicleListView3.RefreshListView();
        }

        void GetDefaultParam()
        {
            try
            {
                StationID = Properties.Settings.Default.StationID;
                LayoutID = Properties.Settings.Default.LayoutID;
                StationName = (StationID == 0) ? "All Stations" : Utils.GetStationName(StationID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception_GetDefaultParam:" + "\r\n\r\n" + ex.Message);
            }
        }

        void InitLayoutList()
        {
            int i = 1;

            LayoutList = new List<string[]>()
            {
                new string[] {(i++).ToString(), a_wim_1, a_wim_2, a_data},
                //new string[] {(i++).ToString(), a_wim_1, a_report, a_static,},            
                //new string[] {(i++).ToString(), a_wim_1, a_report, a_bypass},
                //new string[] {(i++).ToString(), a_wim_1, a_report, a_data},
                //new string[] {(i++).ToString(), a_wim_1, a_repbyp, a_static},
                //new string[] {(i++).ToString(), a_wim_1, a_repbyp, a_data},
                //new string[] {(i++).ToString(), a_wim_1, a_static, a_data},
                //new string[] {(i++).ToString(), a_report, a_static, a_data},
            };
        }

        void InitializeLiveView()
        {
            try
            {
                SqlDependency.Start(AppParameters.ConnectionString);

                this.vehicleListView1.OnSelectionChanged += new VehicleListViewSelectionChanged(vehicleListView1_OnSelectionChanged);
                this.vehicleListView2.OnSelectionChanged += new VehicleListViewSelectionChanged(vehicleListView2_OnSelectionChanged);
                this.vehicleListView3.OnSelectionChanged += new VehicleListViewSelectionChanged(vehicleListView3_OnSelectionChanged);

                ApplyLayout();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception_InitializeLiveView:" + "\r\n\r\n" + ex.Message);
            }
        }

        string GetLaneString(string laneName)
        {
            string lane = string.Empty;
            lane = (laneName == a_wim_1) ? "0" : lane;
            lane = (laneName == a_wim_2) ? "1" : lane;   
            //lane = (laneName == a_report) ? "1" : lane;
            //lane = (laneName == a_bypass) ? "2" : lane;
            //lane = (laneName == a_repbyp) ? "1,2" : lane;
            return lane;
        }

        void ApplyLayout()
        {
            try
            {
                if (this.LayoutList.Count <= 0)
                {
                    return;
                }
                Application.UseWaitCursor = true;
                Cursor.Current = Cursors.WaitCursor;

                ResetDBDetector();

                // Set Station Name
                label_StationName.Text = string.Empty;
                label_StationName.Text = StationName;

                // Set Column Header Text
                int i = this.LayoutID;
                string col1 = LayoutList[i][1];
                string col2 = LayoutList[i][2];
                string col3 = LayoutList[i][3];

                this.label_COL1_Title.Text = "LANE: " + col1;
                this.label_COL2_Title.Text = "LANE: " + col2;
                this.label_COL3_Title.Text = (col3.Contains(a_data)) ? a_data : "LANE: " + col3;

                // Create Data Filter
                DataFilters df = new DataFilters();
                df.stationID = StationID.ToString();
                df.top = MAXROW.ToString();

                // Clear DataView on column 3
                this.vehicleDataView1.Visible = false;
                this.vehicleDataView1.Clear();

                string col = string.Empty;

                #region COL3

                col = col3;
                if (col == a_wim_1 || col == a_wim_2 || col == a_bypass || col == a_report || col == a_repbyp)
                {
                    df.lane = GetLaneString(col);
                    string sql_view = SQLText.GetSQLText_WIM(df);

                    this.vehicleListView3.Show2ndImage = false;

                    string sql_listener = SQLText.GetSQLText_WIM_DBListener(df);
                    this.vehicleListView3.Initialize(3, sql_view, sql_listener, ViewType.WIM);

                    this.vehicleListView3.Visible = true;
                    this.vehicleDataView1.Visible = false;
                }
                else if (col == a_static)
                {
                    string sql_view = SQLText.GetSQLText_STATIC(df);
                    string sql_listener = SQLText.GetSQLText_STATIC_DBListener(df);

                    this.vehicleListView3.Show2ndImage = true;
                 
                    this.vehicleListView3.Initialize(3, sql_view, sql_listener, ViewType.STATIC);

                    this.vehicleListView3.Visible = true;
                    this.vehicleDataView1.Visible = false;
                 

                }
                else if (col == a_data)
                {
                    this.vehicleListView3.Visible = false;
                    this.vehicleDataView1.Visible = true;
                }

                #endregion COL3

                #region COL2

                col = col2;
                if (col == a_wim_1 || col == a_wim_2 || col == a_bypass || col == a_report || col == a_repbyp)
                {
                    df.lane = GetLaneString(col);
                    string sql_view = SQLText.GetSQLText_WIM(df);
                    string sql_listener = SQLText.GetSQLText_WIM_DBListener(df);
                    this.vehicleListView2.Initialize(2, sql_view, sql_listener, ViewType.WIM);
                }
                else if (col == a_static)
                {
                    string sql_view = SQLText.GetSQLText_STATIC(df);
                    string sql_listener = SQLText.GetSQLText_STATIC_DBListener(df);
                    this.vehicleListView2.Initialize(2, sql_view, sql_listener, ViewType.STATIC);
                }

                #endregion COL2

                #region COL1

                col = col1;
                if (col == a_wim_1 || col == a_wim_2 || col == a_bypass || col == a_report || col == a_repbyp)
                {
                    df.lane = GetLaneString(col);
                    string sql_view = SQLText.GetSQLText_WIM(df);
                    string sql_listener = SQLText.GetSQLText_WIM_DBListener(df);
                    this.vehicleListView1.Initialize(1, sql_view, sql_listener, ViewType.WIM);
                }
                else if (col == a_static)
                {
                    string sql_view = SQLText.GetSQLText_STATIC(df);
                    string sql_listener = SQLText.GetSQLText_STATIC_DBListener(df);
                    this.vehicleListView1.Initialize(1, sql_view, sql_listener, ViewType.STATIC);
                }

                #endregion COL1
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception_ApplyLayout:" + "\r\n\r\n" + ex.Message);
            }
            finally
            {
                Application.UseWaitCursor = false;
                Cursor.Current = Cursors.Default;
            }

            
        }

        void RefreshView()
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                this.vehicleDataView1.Clear();
                this.vehicleListView3.RefreshListView();
                this.vehicleListView2.RefreshListView();
                this.vehicleListView1.RefreshListView();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception_RefreshView:" + "\r\n\r\n" + ex.Message);
            }

            Cursor.Current = Cursors.Default;
        }

        void vehicleListView1_OnSelectionChanged(ViewType viewtype, string refID)
        {
            this.vehicleListView2.ClearRowSelection();
            this.vehicleListView3.ClearRowSelection();
            this.vehicleDataView1.RefreshView(viewtype, refID);
        }

        void vehicleListView2_OnSelectionChanged(ViewType viewtype, string refID)
        {
            this.vehicleListView1.ClearRowSelection();
            this.vehicleListView3.ClearRowSelection();
            this.vehicleDataView1.RefreshView(viewtype, refID);
        }

        void vehicleListView3_OnSelectionChanged(ViewType viewtype, string refID)
        {
            this.vehicleListView1.ClearRowSelection();
            this.vehicleListView2.ClearRowSelection();
        }

        void button_Layout_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<LayoutSetting>().Any())
            {
                return;
            }
           
            LayoutSetting settingForm = new LayoutSetting(this.LayoutList);
            settingForm.Top = this.panel2.Top + this.panel2.Height;
            settingForm.Left = ((Button)sender).Left - settingForm.Width + ((Button)sender).Width;

            settingForm.FormClosed += new FormClosedEventHandler(layoutSetting_FormClosed);
            settingForm.Show();          
        }

        void layoutSetting_FormClosed(object sender, FormClosedEventArgs e)
        {
            LayoutSetting settingForm = (LayoutSetting)sender;

            if (settingForm.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                LayoutID = settingForm.LayoutID;

                Cursor.Current = Cursors.WaitCursor;
                ApplyLayout();
                Cursor.Current = Cursors.Default;
            }
            else
            {
                Application.DoEvents();
            }
        }

        void button_Filters_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<VehicleLiveViewFilterSetting>().Any())
            {
                return;
            }

            VehicleLiveViewFilterSetting settingForm = new VehicleLiveViewFilterSetting();
            settingForm.Top = this.panel2.Top + this.panel2.Height;
            settingForm.Left = this.button_Filters.Left - settingForm.Width + this.button_Filters.Width;

            settingForm.FormClosed += new FormClosedEventHandler(VehicleLiveViewFilterSetting_FormClosed);
            settingForm.Show();          
        }

        void VehicleLiveViewFilterSetting_FormClosed(object sender, FormClosedEventArgs e)
        {
            VehicleLiveViewFilterSetting settingArea = (VehicleLiveViewFilterSetting)sender;
            if (settingArea.DialogResult == System.Windows.Forms.DialogResult.OK)
            {            
                StationID = settingArea.StationID;
                StationName = settingArea.StationName;

                Cursor.Current = Cursors.WaitCursor;
                ApplyLayout();
                Cursor.Current = Cursors.Default;
            }
            else
            {
                Application.DoEvents();
            }
        }

        void button_Refresh_Click(object sender, EventArgs e)
        {
            RefreshView();
        }

        void button_Goto_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<GotoSetting>().Any())
            {
                return;
            }

            GotoSetting settingArea = new GotoSetting(0); //Call from liveview = 0
            settingArea.Top = this.panel2.Top + this.panel2.Height;
            settingArea.Left = this.button_Goto.Left - settingArea.Width + this.button_Goto.Width;

            settingArea.FormClosed += new FormClosedEventHandler(GotoSetting_FormClosed);
            settingArea.Show();           
        }

        void GotoSetting_FormClosed(object sender, FormClosedEventArgs e)
        {
            GotoSetting settingArea = (GotoSetting)sender;
            if (settingArea.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                Timer timer1 = new Timer();
                timer1.Tick += new EventHandler(timer1_Tick);
                timer1.Tag = settingArea.Selection;
                timer1.Interval = 200;
                timer1.Start();              
            }
            else
            {
                Application.DoEvents();
            }
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            Timer timer1 = (Timer)sender;
            timer1.Stop();
            int selection = (int)timer1.Tag;
            timer1.Dispose();

            if (selection == 1)
            {
                this.Cursor = Cursors.WaitCursor;
                WIMHistoryView f = new WIMHistoryView();
                f.Show(this);
                this.Cursor = Cursors.Default;
            }
            else if (selection == 2)
            {
                this.Cursor = Cursors.WaitCursor;
                StaticHistoryView f = new StaticHistoryView();
                f.Show(this);
                this.Cursor = Cursors.Default;
            }
            else if (selection == 3)
            {
                this.Cursor = Cursors.WaitCursor;
                WIMStationControl f = new WIMStationControl();
                f.Show(this);
                this.Cursor = Cursors.Default;
            }            
        }

        void button_Exit_Click(object sender, EventArgs e)
        {
            try
            {
                this.vehicleListView1.Close();
                this.vehicleListView2.Close();
                this.vehicleListView3.Close();
                SqlDependency.Stop(AppParameters.ConnectionString);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception_Exit:" + "\r\n\r\n" + ex.Message);
            }

            Application.Exit();
        }

        void ResetDBDetector()
        {
            try
            {
                this.vehicleListView1.Close();
                this.vehicleListView2.Close();
                this.vehicleListView3.Close();
                SqlDependency.Stop(AppParameters.ConnectionString);
                SqlDependency.Start(AppParameters.ConnectionString);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception_ResetDBDetector:" + "\r\n\r\n" + ex.Message);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            LiveViewON(true);
            this.button1.ForeColor = Color.Gray;
            this.button2.ForeColor = Color.Black;
            this.button1.Enabled = false;
            this.button2.Enabled = true;
            RefreshView();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LiveViewON(false);
            this.button1.ForeColor = Color.Black;
            this.button2.ForeColor = Color.Gainsboro;
            this.button1.Enabled = true;
            this.button2.Enabled = false;
        }

        void LiveViewON(bool trueOrFalse)
        {
            this.vehicleListView1.LiveView = trueOrFalse;
            this.vehicleListView2.LiveView = trueOrFalse;
            this.vehicleListView3.LiveView = trueOrFalse;
        }
       
    }
}

