﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace doh.vehicleview.service.Models
{
    public class StationModel
    {
      public int StationID {get;set;}
      public string StationName {get;set;}
      //public string StationDescription {get;set;}
      //public string LocationDescription {get;set;}
      //public int ProvinceID {get;set;}
      //public double Latitude {get;set;}
      //public double Longtitude { get; set; }
      public int StationGroupID{get;set;}
    }
}