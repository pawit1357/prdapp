﻿using System;

namespace doh.vehicleview.service.Models
{
    public class UserGroupModel
    {
        public int UserGroupID { get; set; }
        public String UserGroupName { get; set; }
        public String UserGroupDescription { get; set; }
    }
}