﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace doh.vehicleview.service.Models
{
    public class AxleModel
    {
        public String axleName { get; set; }
        public String col_1 { get; set; }
        public String col_2 { get; set; }
        public String col_3 { get; set; }
        public String col_4 { get; set; }
        public String col_5 { get; set; }
        public String col_6 { get; set; }
        public String col_7 { get; set; }
        public String col_8 { get; set; }
        public String col_9 { get; set; }

    }
}