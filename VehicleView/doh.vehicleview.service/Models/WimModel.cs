﻿using doh.vehicleview.service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace doh.vehicleview.service
{
    public class WimModel
    {
        public String wimid { get; set; }
        public String imgpath { get; set; }
        public String imgPath2 { get; set; }
        public String RefID { get; set; }
        public String StationName { get; set; }
        public String StationId { get; set; }
        public String TimeStamp { get; set; }
        public String VehicleNumber { get; set; }
        public String Lane { get; set; }
        public String LaneId { get; set; }
        public String VehicleClass { get; set; }
        public String SortDecision { get; set; }
        public String MaxGvw { get; set; }
        public String Gvw { get; set; }

        public String LicensePlateNumber { get; set; }
        public String ProvinceName { get; set; }

        public String statusCode { get; set; }
        public String errorCode { get; set; }
        public String StatusColor { get; set; }

        public String Speed { get; set; }
        public String ESAL { get; set; }
        public String AxleCount { get; set; }
        public String Length { get; set; }
        public String FrontOverHang { get; set; }
        public String RearOverHang { get; set; }

        public String SeperationCol_1 { get; set; }
        public String SeperationCol_2 { get; set; }
        public String SeperationCol_3 { get; set; }
        public String SeperationCol_4 { get; set; }
        public String SeperationCol_5 { get; set; }
        public String SeperationCol_6 { get; set; }
        public String SeperationCol_7 { get; set; }
        public String SeperationCol_8 { get; set; }
        public String SeperationCol_9 { get; set; }

        public String GroupCol_1 { get; set; }
        public String GroupCol_2 { get; set; }
        public String GroupCol_3 { get; set; }
        public String GroupCol_4 { get; set; }
        public String GroupCol_5 { get; set; }
        public String GroupCol_6 { get; set; }
        public String GroupCol_7 { get; set; }
        public String GroupCol_8 { get; set; }
        public String GroupCol_9 { get; set; }


        public String WeightCol_1 { get; set; }
        public String WeightCol_2 { get; set; }
        public String WeightCol_3 { get; set; }
        public String WeightCol_4 { get; set; }
        public String WeightCol_5 { get; set; }
        public String WeightCol_6 { get; set; }
        public String WeightCol_7 { get; set; }
        public String WeightCol_8 { get; set; }
        public String WeightCol_9 { get; set; }

        public int StationGroupID { get; set; }
        //public List<AxleModel> axles { get; set; }

        
    }
}




