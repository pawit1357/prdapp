﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace doh.vehicleview.service
{
    public class AppParameters
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AppParameters));

        public static string StationID = "0";
        //public static string DataLocation = @"D:\WEIGHING_DATA";
        //public static string LogLocation = @"D:\WEIGHING_DATA";
        ////public static string ImageURL = @"http://192.168.100.12/weighing_data_img";
        //public static string ImageURL = @"http://7.59.252.211/weighing_data_img";
        //public static string ConnectionString = @"Data Source=7.59.252.211;Initial Catalog=WEIGHING_00;Integrated Security=True";
        public static string APPFolderName = "VehicleView";
        public static string MaxLogFileSizeKB = "500";
        public static int ImageDelay = 1500;

        public static string StationNamePrefix = "สถานีตรวจสอบน้ำหนักรถบรรทุก";

        public static string WarningSignalPort = "COM1";
        public static int WarningSignalDuration = 1000;
        public static bool WarningSignalType1Enable = true;
        public static bool WarningSignalType2Enable = true;
        public static bool WarningSignalType3Enable = true;
        public static bool WarningSignalType4Enable = true;
        public static string SoundFilePath = string.Empty;

        public static string WIM_IRD_ServerAddress = string.Empty;
        public static string WIM_IRD_ServerPort = string.Empty;




        public static String ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["VehicleView.Properties.Settings.ConnectionString"].ConnectionString;
            }
        }

        public static String ImageURL
        {
            get
            {
                return ConfigurationManager.AppSettings["ImageURL"].ToString();
            }
        }
        public static String ImageURL_2
        {
            get
            {
                return ConfigurationManager.AppSettings["ImageURL_2"].ToString();
            }
        }
        public static String ImageURLVPN
        {
            get
            {
                return ConfigurationManager.AppSettings["ImageURLVPN"].ToString();
            }
        }
        public static String DataLocation
        {
            get
            {
                return ConfigurationManager.AppSettings["DataLocation"].ToString();
            }
        }
        public static String DataLocation_2
        {
            get
            {
                return ConfigurationManager.AppSettings["DataLocation_2"].ToString();
            }
        }

    }
}
