﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.ComponentModel;
using VehicleViewLIB;
using System.Net;
using System.IO;
using System.Globalization;


namespace doh.vehicleview.service
{
    public class Utils
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Utils));

        private const String NUMBER_FORMAT_1 = "{0:#,##}";

        public static String customNumberFormat(String _value)
        {

            if (_value == null) return "";
            if (_value == "null") return "";
            if (_value.Equals(String.Empty)) return "";
            return String.Format(CultureInfo.InvariantCulture,
                        NUMBER_FORMAT_1, Convert.ToDouble(_value));
        }

        public static DataTable GetDataTableNormal(string SQLText)
        {
            DataTable table = new DataTable();
            try
            {

                using (SqlConnection connection = new SqlConnection(AppParameters.ConnectionString))
                {
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(SQLText, connection))
                    {
                        adapter.Fill(table);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Debug(String.Format(("GetDataTable Exception_GetDataTableBG: " +
                    "\r\n\r\n" + ex.Message +
                    //  "\r\n\r\n" + AppParameters.ConnectionString +
                    "\r\n\r\n" + SQLText)));
            }

            return table;
        }

        public static bool ExecuteSQLCommand(string SQLText, out object output)
        {
            bool success = true;
            try
            {
                using (SqlConnection connection = new SqlConnection(AppParameters.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(SQLText, connection))
                    {
                        output = command.ExecuteScalar();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
                output = ex;
            }
            GC.Collect();
            return success;
        }

        public static string GetImageFolder(string imageName)
        {
            string imageFolder = string.Empty;
            string appname = string.Empty;

            if (imageName.StartsWith("rsd"))
            {
                appname = "RSD";
            }
            else if (imageName.StartsWith("lpr"))
            {
                appname = "STATIC";
            }
            else if (imageName.StartsWith("wim"))
            {
                appname = "WIM";
            }

            if (imageName.EndsWith("jpg"))
            {
                if (imageName.Length > 15)
                {
                    string siteID = imageName.Substring(4, 2);
                    string y = imageName.Substring(7, 4);
                    string m = imageName.Substring(11, 2);
                    string d = imageName.Substring(13, 2);

                    string subFolder = siteID + @"\" + y + @"\" + m + @"\" + d;
                    imageFolder = AppParameters.DataLocation + @"\IMAGES\" + subFolder + @"\" + appname;
                }
            }

            return imageFolder;
        }
        public static string GetImageFolder_2(string imageName)
        {
            string imageFolder = string.Empty;
            string appname = string.Empty;

            if (imageName.StartsWith("rsd"))
            {
                appname = "RSD";
            }
            else if (imageName.StartsWith("lpr"))
            {
                appname = "STATIC";
            }
            else if (imageName.StartsWith("wim"))
            {
                appname = "WIM";
            }

            if (imageName.EndsWith("jpg"))
            {
                if (imageName.Length > 15)
                {
                    string siteID = imageName.Substring(4, 2);
                    string y = imageName.Substring(7, 4);
                    string m = imageName.Substring(11, 2);
                    string d = imageName.Substring(13, 2);

                    string subFolder = siteID + @"\" + y + @"\" + m + @"\" + d;
                    imageFolder = AppParameters.DataLocation_2 + @"\IMAGES\" + subFolder + @"\" + appname;
                }
            }

            return imageFolder;
        }

        public static string GetImageURL(string imageName)
        {
            string imageUrl = string.Empty;
            string appname = string.Empty;

            if (imageName.StartsWith("rsd"))
            {
                appname = "RSD";
            }
            else if (imageName.StartsWith("lpr"))
            {
                appname = "LPR";
            }
            else if (imageName.StartsWith("wim"))
            {
                appname = "WIM";
            }

            if (imageName.EndsWith("jpg"))
            {
                if (imageName.Length > 15)
                {
                    string siteID = imageName.Substring(4, 2);
                    string y = imageName.Substring(7, 4);
                    string m = imageName.Substring(11, 2);
                    string d = imageName.Substring(13, 2);

                    string subFolder = siteID + @"/" + y + @"/" + m + @"/" + d;
                    imageUrl = AppParameters.ImageURL + @"/" + subFolder + @"/" + appname + @"/" + imageName;
                }

                if (imageName.StartsWith("class"))
                {
                    imageUrl = AppParameters.ImageURL + @"/vclass/" + imageName;
                }
            }
            /*
            using (WebClient Client = new WebClient())
            {

                String imageFolder = GetImageFolder(imageName);
                if (!Directory.Exists(imageFolder))
                {
                    Directory.CreateDirectory(imageFolder);
                }

                string siteID = imageName.Substring(4, 2);
                string y = imageName.Substring(7, 4);
                string m = imageName.Substring(11, 2);
                string d = imageName.Substring(13, 2);

                string subFolder = siteID + @"/" + y + @"/" + m + @"/" + d;

                String imageUrlVpn = AppParameters.ImageURLVPN + @"/" + subFolder + @"/" + appname + @"/" + imageName;
                try
                {
                    Client.DownloadFile(imageUrlVpn, imageFolder + @"/" + imageName);
                }
                catch (Exception ex) { }
                
            }
            */
            return imageUrl;
        }
        public static string GetImageURL_2(string imageName)
        {
            string imageUrl = string.Empty;
            string appname = string.Empty;

            if (imageName.StartsWith("rsd"))
            {
                appname = "RSD";
            }
            else if (imageName.StartsWith("lpr"))
            {
                appname = "LPR";
            }
            else if (imageName.StartsWith("wim"))
            {
                appname = "WIM";
            }

            if (imageName.EndsWith("jpg"))
            {
                if (imageName.Length > 15)
                {
                    string siteID = imageName.Substring(4, 2);
                    string y = imageName.Substring(7, 4);
                    string m = imageName.Substring(11, 2);
                    string d = imageName.Substring(13, 2);

                    string subFolder = siteID + @"/" + y + @"/" + m + @"/" + d;
                    imageUrl = AppParameters.ImageURL_2 + @"/" + subFolder + @"/" + appname + @"/" + imageName;
                }

                if (imageName.StartsWith("class"))
                {
                    imageUrl = AppParameters.ImageURL + @"/vclass/" + imageName;
                }
            }
            /*
            using (WebClient Client = new WebClient())
            {

                String imageFolder = GetImageFolder(imageName);
                if (!Directory.Exists(imageFolder))
                {
                    Directory.CreateDirectory(imageFolder);
                }

                string siteID = imageName.Substring(4, 2);
                string y = imageName.Substring(7, 4);
                string m = imageName.Substring(11, 2);
                string d = imageName.Substring(13, 2);

                string subFolder = siteID + @"/" + y + @"/" + m + @"/" + d;

                String imageUrlVpn = AppParameters.ImageURLVPN + @"/" + subFolder + @"/" + appname + @"/" + imageName;
                try
                {
                    Client.DownloadFile(imageUrlVpn, imageFolder + @"/" + imageName);
                }
                catch (Exception ex) { }
                
            }
            */
            return imageUrl;
        }

        public static string GetStatusCodeText(VRStatusCode status)
        {
            string text = string.Empty;
            foreach (VRStatusCode val in Enum.GetValues(typeof(VRStatusCode)))
            {
                if (val != VRStatusCode.StatusClear)
                {
                    if ((val & status) == val)
                    {
                        text += (text.Length > 0) ? "\r\n" : "";
                        text += val.ToString();
                    }
                }
            }
            return text;
        }

        public static string GetAxleGroupText(byte groupType)
        {
            if (groupType == 1)
            {
                return "single";
            }
            else if (groupType == 2)
            {
                return "tandem";
            }
            else if (groupType == 3)
            {
                return "tridem";
            }
            else if (groupType == 4)
            {
                return "quadrem";
            }
            else
            {
                return "";
            }
        }

        public static string GetLaneName(byte lane)
        {
            if (lane == 0)
            {
                return "WIM";
            }
            else if (lane == 1)
            {
                return "Report";
            }
            else if (lane == 2)
            {
                return "Bypass";
            }
            else
            {
                return "";
            }
        }

        public static Color GetSortDecisionColorCode(VRSortDecisionCode sortDecision)
        {
            Color green = Color.LawnGreen;
            Color red = Color.Crimson;
            Color gray = Color.Silver;
            Color orange = Color.Orange;

            if (sortDecision == VRSortDecisionCode.Bypass)
            {
                return green;
            }
            else if (sortDecision == VRSortDecisionCode.CredentialReport)
            {
                return red;
            }
            //else if (sortDecision == VRSortDecisionCode.None)
            //{
            //    return gray;
            //}
            else if (sortDecision == VRSortDecisionCode.Off)
            {
                return gray;
            }
            else if (sortDecision == VRSortDecisionCode.Report)
            {
                return red;
            }
            else if (sortDecision == VRSortDecisionCode.Sorting)
            {
                return orange;
            }
            else
            {
                return gray;
            }
        }

        public static Color GetIsOverWeightColorCode(string status)
        {
            Color green = Color.LawnGreen;
            Color red = Color.Crimson;
            Color gray = Color.Silver;

            if (status.ToUpper() == "Y")
            {
                return red;
            }
            else if (status.ToUpper() == "N")
            {
                return green;
            }
            else
            {
                return gray;
            }

        }

        public static Color GetWarningTypeColorCode(int Type)
        {
            Color avoiding = Color.Yellow;
            Color runningscale = Color.Orange;
            Color overweight = Color.Crimson;
            Color bgopened = Color.BlueViolet;

            if (Type == 1)
            {
                return avoiding;
            }
            else if (Type == 2)
            {
                return runningscale;
            }
            else if (Type == 3)
            {
                return overweight;
            }
            else if (Type == 4)
            {
                return bgopened;
            }
            else
            {
                return Color.Gray;
            }

        }

        public static string GetStationName(int stationID)
        {
            string name = string.Empty;
            DataTable table = GetDataTableNormal("SELECT * FROM Station");
            if (table.Rows.Count > 0)
            {
                foreach (DataRow r in table.Rows)
                {
                    if (stationID == (int)r["StationID"])
                    {
                        name = (string)r["StationName"];
                        break;
                    }
                }
            }
            return name;
        }

        public static string GetDateTimeString(DateTime date, DateTime time)
        {
            int y = date.Year;
            int M = date.Month;
            int d = date.Day;
            int h = time.Hour;
            int m = time.Minute;
            return new DateTime(y, M, d, h, m, 0).ToString("s");
        }

        public static bool isFileExist(String filePath)
        {
            return File.Exists(filePath);

        }

        public byte[] FileToByteArray(string fileName)
        {
            byte[] buff = null;
            FileStream fs = new FileStream(fileName,
                                           FileMode.Open,
                                           FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            long numBytes = new FileInfo(fileName).Length;
            buff = br.ReadBytes((int)numBytes);
            return buff;
        }

        public static  bool URLExists(string url)
        {
            bool result = false;

            WebRequest webRequest = WebRequest.Create(url);
            webRequest.Timeout = 1200; // miliseconds
            webRequest.Method = "HEAD";

            HttpWebResponse response = null;

            try
            {
                response = (HttpWebResponse)webRequest.GetResponse();
                result = true;
            }
            catch (WebException webException)
            {
                Console.WriteLine(webException);
                //Debug.Log(url + " doesn't exist: " + webException.Message);
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            return result;
        }



    }
}
