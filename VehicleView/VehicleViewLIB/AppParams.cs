﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace VehicleViewLIB
{
    public class AppParameters
    {
        public static string StationID = "0";
        public static string DataLocation = @"C:\WEIGHING_DATA";
        public static string LogLocation = @"C:\WEIGHING_DATA";
        public static string ImageURL = @"http://192.168.100.12/weighing_data_img";
        public static string ConnectionString = @"Data Source=7.59.252.211;Initial Catalog=WIMBASE_00;User ID=sa;Password=Station0";
        public static string APPFolderName = "VehicleView";
        public static string MaxLogFileSizeKB = "500";
        public static int ImageDelay = 1500;

        public static string StationNamePrefix = "สถานีตรวจสอบน้ำหนักรถบรรทุก";

        public static string WarningSignalPort = "COM1";
        public static int WarningSignalDuration = 1000;
        public static bool WarningSignalType1Enable = true;
        public static bool WarningSignalType2Enable = true;
        public static bool WarningSignalType3Enable = true;
        public static bool WarningSignalType4Enable = true;
        public static string SoundFilePath = string.Empty;

        public static string WIM_IRD_ServerAddress = string.Empty;
        public static string WIM_IRD_ServerPort = string.Empty;



        /*
        public static String ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["VehicleView.Properties.Settings.ConnectionString"].ConnectionString;
            }
        }

        public static String ImageURL
        {
            get
            {
                return ConfigurationManager.AppSettings["ImageURL"].ToString();
            }
        }
        */
    }
}
