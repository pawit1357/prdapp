﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VehicleViewLIB
{
    public class DataFilters
    {
        public string top { get; set; }
        public string stationID { get; set; }
        public string stationName { get; set; }
        public string lane { get; set; }
        public string startTimeStamp { get; set; }
        public string endTimeStamp { get; set; }
        public string vehClass { get; set; }
        public string warningType { get; set; }

        public DataFilters()
        {
            top = string.Empty;
            stationID = string.Empty;
            stationName = string.Empty;
            lane = string.Empty;
            startTimeStamp = string.Empty; ;
            endTimeStamp = string.Empty;
            vehClass = string.Empty;
            warningType = string.Empty;
        }
    }

    public class SQLText
    {
        public static string GetSQLText_WIM(DataFilters filter)
        {
            string sql = string.Empty;

            DataFilters f = new DataFilters();

            if (filter.top != string.Empty)
            {
                f.top = "TOP " + filter.top;
            }

            if (filter.stationID == "0" || filter.stationID == string.Empty)
            {
                f.stationID = "%";
            }
            else
            {
                f.stationID = filter.stationID;
            }

            if (filter.vehClass == "0" || filter.vehClass == string.Empty)
            {
                f.vehClass = "%";
            }
            else
            {
                f.vehClass = filter.vehClass;
            }

            if (filter.startTimeStamp != string.Empty)
            {
                f.startTimeStamp = "AND dbo.WIM.TimeStamp >= '" + filter.startTimeStamp + "' ";
            }

            if (filter.endTimeStamp != string.Empty)
            {
                f.endTimeStamp = "AND dbo.WIM.TimeStamp <= '" + filter.endTimeStamp + "' ";
            }

            if (filter.lane.Contains(","))
            {
                string[] lanes = filter.lane.Split(new char[] { ',' });
                f.lane += "(";
                for (int i = 0; i < lanes.Length - 1; i++)
                {
                    f.lane += "dbo.WIM.Lane = " + lanes[i] + " OR ";
                }
                f.lane += "dbo.WIM.Lane = " + lanes[lanes.Length - 1] + ") ";
            }
            else if (filter.lane != string.Empty)
            {
                f.lane = "dbo.WIM.Lane = " + filter.lane + " ";
            }
            else if (filter.lane == string.Empty)
            {
                f.lane = "dbo.WIM.Lane LIKE '%' ";
            }

            sql += "SELECT " + f.top + " dbo.Station.StationName, dbo.Province.ProvinceName, dbo.WIM.* ";
            sql += "FROM dbo.WIM INNER JOIN ";
            sql += "dbo.Station ON dbo.WIM.StationID = dbo.Station.StationID ";
            sql += "INNER JOIN ";
            sql += "dbo.Province ON dbo.WIM.LicensePlateProvinceID = dbo.Province.ProvinceID ";
            sql += "WHERE " ;
            sql += "dbo.WIM.StationID LIKE '" + f.stationID + "' ";
            sql += "AND " + f.lane + " ";
            sql += "AND dbo.WIM.VehicleClass LIKE '" + f.vehClass + "' ";
            sql += f.startTimeStamp;
            sql += f.endTimeStamp;
            sql += "ORDER BY dbo.WIM.TimeStamp DESC";

            return sql;
        }

        public static string GetSQLText_WIM_DBListener(DataFilters filter)
        {
            string sql = string.Empty;
            DataFilters f = new DataFilters();

            if (filter.stationID == "0")
            {
                f.stationID = "%";
            }
            else
            {
                f.stationID = filter.stationID;
            }

            if (filter.lane.Contains(","))
            {
                string[] lanes = filter.lane.Split(new char[] { ',' });
                f.lane += "(";
                for (int i = 0; i < lanes.Length - 1; i++)
                {
                    f.lane += "dbo.WIM.Lane = " + lanes[i] + " OR ";
                }
                f.lane += "dbo.WIM.Lane = " + lanes[lanes.Length - 1] + ") ";
            }
            else if (filter.lane != string.Empty)
            {
                f.lane = "dbo.WIM.Lane = " + filter.lane + " ";
            }

            sql += "SELECT dbo.WIM.WIMID, dbo.WIM.StationID, dbo.WIM.TimeStamp, dbo.WIM.Lane, dbo.WIM.VehicleClass ";
            sql += "FROM dbo.WIM ";
            sql += "WHERE dbo.WIM.StationID LIKE '" + f.stationID + "' ";
            sql += "AND " + f.lane + " ";
           
            return sql;
        }

        public static string GetSQLText_WIM(string wimID)
        {
            string sql = string.Empty;

            sql += "SELECT dbo.WIM.*, dbo.Province.ProvinceName, dbo.Station.StationName ";
            sql += "FROM dbo.WIM INNER JOIN ";
            sql += "dbo.Station ON dbo.WIM.StationID = dbo.Station.StationID ";
            sql += "INNER JOIN ";
            sql += "dbo.Province ON dbo.WIM.LicensePlateProvinceID = dbo.Province.ProvinceID ";            
            sql += "WHERE dbo.WIM.WIMID = " + wimID + " ";
            sql += "ORDER BY dbo.WIM.TimeStamp DESC";

            return sql;
        }

        public static string GetSQLText_STATIC(DataFilters filter)
        {
            string sql = string.Empty;

            DataFilters f = new DataFilters();

            if (filter.top != string.Empty)
            {
                f.top = "TOP " + filter.top;
            }

            if (filter.stationID == "0" || filter.stationID == string.Empty)
            {
                f.stationID = "%";
            }
            else
            {
                f.stationID = filter.stationID;
            }

            if (filter.vehClass == "0" || filter.vehClass == string.Empty)
            {
                f.vehClass = "%";
            }
            else
            {
                f.vehClass = filter.vehClass;
            }

            if (filter.startTimeStamp != string.Empty)
            {
                f.startTimeStamp = "AND dbo.Weighing.TimeStamp >= '" + filter.startTimeStamp + "' ";
            }

            if (filter.endTimeStamp != string.Empty)
            {
                f.endTimeStamp = "AND dbo.Weighing.TimeStamp <= '" + filter.endTimeStamp + "' ";
            }


            string tmp = " dbo.Weighing.[ENFID]" +
                          ",dbo.Weighing.[StationID]" +
                          ",dbo.Weighing.[TimeStamp]" +
                          ",dbo.Weighing.[SeqNumber]" +
                          ",dbo.Weighing.[WIMID]" +
                          ",dbo.Weighing.[VehicleClassID]" +
                          ",dbo.Weighing.[LPR_Number]" +
                          ",dbo.Weighing.[LPR_ProvinceID]" +
                          ",dbo.Weighing.[LPR_ImageName]" +
                          ",dbo.Weighing.[VehicleImageName]" +
                          ",dbo.Weighing.[GVW_Weight_Max]" +
                          ",dbo.Weighing.[GVW_Weight_Measured]" +
                          ",dbo.Weighing.[GVW_Weight_Over]" +
                          ",dbo.Weighing.[GVW_Weight_WIM]" +
                          ",dbo.Weighing.[IsOverWeight]" +
                          ",dbo.Weighing.[IsBGOpened]" +
                          ",dbo.Weighing.[UserID] " + 
                          ", ";
            
            sql += "SELECT " + f.top + tmp + " dbo.Station.StationName, dbo.Users.FirstName, ";
            sql += "dbo.Users.LastName, dbo.Users.LoginID, dbo.Provinces.ProvinceName  ";
            sql += "FROM dbo.Weighing INNER JOIN ";
            sql += "dbo.Station ON dbo.Weighing.StationID = dbo.Station.StationID INNER JOIN ";
            sql += "dbo.Users ON dbo.Weighing.UserID = dbo.Users.UserID INNER JOIN ";
            sql += "dbo.Provinces ON dbo.Weighing.LPR_ProvinceID = dbo.Provinces.ProvinceID ";
            sql += "WHERE dbo.Weighing.StationID LIKE '" + f.stationID + "' ";
            sql += "AND dbo.Weighing.VehicleClassID LIKE '" + f.vehClass + "' ";
            sql += f.startTimeStamp;
            sql += f.endTimeStamp;
            sql += "ORDER BY dbo.Weighing.TimeStamp DESC";

            return sql;
        }

        public static string GetSQLText_STATIC_DBListener(DataFilters filter)
        {
            string sql = string.Empty;

            DataFilters f = new DataFilters();

            if (filter.stationID == "0" || filter.stationID == string.Empty)
            {
                f.stationID = "%";
            }
            else
            {
                f.stationID = filter.stationID;
            }

            sql += "SELECT dbo.Weighing.ENFID, dbo.Weighing.StationID, dbo.Weighing.TimeStamp ";       
            sql += "FROM dbo.Weighing ";            
            sql += "WHERE dbo.Weighing.StationID LIKE '" + f.stationID + "' ";
            
            return sql;
        }

        public static string GetSQLText_STATIC(string enfID)
        {
            string sql = string.Empty;

            sql += "SELECT dbo.Weighing.*, dbo.Station.StationName, dbo.Users.FirstName, ";
            sql += "dbo.Users.LastName, dbo.Users.LoginID, dbo.Provinces.ProvinceName ";
            sql += "FROM dbo.Weighing INNER JOIN ";
            sql += "dbo.Station ON dbo.Weighing.StationID = dbo.Station.StationID INNER JOIN ";
            sql += "dbo.Users ON dbo.Weighing.UserID = dbo.Users.UserID INNER JOIN ";
            sql += "dbo.Provinces ON dbo.Weighing.LPR_ProvinceID = dbo.Provinces.ProvinceID ";
            sql += "WHERE dbo.Weighing.ENFID = " + enfID + " ";
            sql += "ORDER BY dbo.Weighing.TimeStamp DESC";

            return sql;
        }

        public static string GetSQLText_WARNING(DataFilters filter)
        {
            string sql = string.Empty;

            DataFilters f = new DataFilters();

            if (filter.top != string.Empty)
            {
                f.top = "TOP " + filter.top;
            }

            if (filter.stationID == "0" || filter.stationID == string.Empty)
            {
                f.stationID = "%";
            }
            else
            {
                f.stationID = filter.stationID;
            }

            if (filter.warningType.Contains(","))
            {
                string[] wtypes = filter.warningType.Split(new char[] { ',' });
                f.warningType += "(";
                for (int i = 0; i < wtypes.Length - 1; i++)
                {
                    f.warningType += "dbo.WarningVehicles.Type = " + wtypes[i] + " OR ";
                }
                f.warningType += "dbo.WarningVehicles.Type = " + wtypes[wtypes.Length - 1] + ") ";
            }
            else if (filter.warningType == "0" || filter.warningType == string.Empty)
            {
                f.warningType = "dbo.WarningVehicles.Type LIKE '%' ";
            }
            else if (filter.warningType != string.Empty)
            {
                f.warningType = "dbo.WarningVehicles.Type = " + filter.warningType + " ";
            }

            if (filter.startTimeStamp != string.Empty)
            {
                f.startTimeStamp = "AND dbo.WarningVehicles.TimeStamp >= '" + filter.startTimeStamp + "' ";
            }

            if (filter.endTimeStamp != string.Empty)
            {
                f.endTimeStamp = "AND dbo.WarningVehicles.TimeStamp <= '" + filter.endTimeStamp + "' ";
            }

            sql += "SELECT " + f.top + " dbo.WarningVehicles.*, dbo.Station.StationName ";
            sql += "FROM dbo.WarningVehicles INNER JOIN ";
            sql += "dbo.Station ON dbo.WarningVehicles.StationID = dbo.Station.StationID ";
            sql += "WHERE dbo.WarningVehicles.StationID LIKE '" + f.stationID + "' ";
            sql += "AND " + f.warningType + " ";
            sql += f.startTimeStamp;
            sql += f.endTimeStamp;
            sql += "ORDER BY dbo.WarningVehicles.TimeStamp DESC";
                
            return sql;
        }

        public static string GetSQLText_WARNING_DBListener(DataFilters filter)
        {
            string sql = string.Empty;

            DataFilters f = new DataFilters();

            f.warningType = filter.warningType;

            if (filter.stationID == "0" || filter.stationID == string.Empty)
            {
                f.stationID = "%";
            }
            else
            {
                f.stationID = filter.stationID;
            }

            if (f.warningType == "1")
            {
                sql += "SELECT dbo.RadarDetection.RSID ";
                sql += "FROM dbo.RadarDetection ";
                sql += "WHERE dbo.RadarDetection.StationID LIKE '" + f.stationID + "' ";
            }
            else if (f.warningType == "2")
            {
                sql += "SELECT dbo.WIM.WIMID ";
                sql += "FROM dbo.WIM ";
                sql += "WHERE dbo.WIM.StationID LIKE '" + f.stationID + "' ";
                sql += "AND ((dbo.WIM.StatusCode & 512) = 512) ";
            }
            else if (f.warningType == "3")
            {
                sql += "SELECT dbo.Weighing.ENFID ";
                sql += "FROM dbo.Weighing ";
                sql += "WHERE dbo.Weighing.StationID LIKE '" + f.stationID + "' ";
                sql += "AND dbo.Weighing.IsOverweight = 'Y'";
            }
            else if (f.warningType == "4")
            {
                sql += "SELECT dbo.Weighing.ENFID ";
                sql += "FROM dbo.Weighing ";
                sql += "WHERE dbo.Weighing.StationID LIKE '" + f.stationID + "' ";
                sql += "AND dbo.Weighing.IsBGOpened = 'Y'";
            }
        

            return sql;
        }



      

        public static string GetSQLText_RADAR(string rsID)
        {
            string sql = string.Empty;

            sql += "SELECT dbo.RadarDetection.*, dbo.Station.StationName ";
            sql += "FROM dbo.RadarDetection INNER JOIN ";
            sql += "dbo.Station ON dbo.RadarDetection.StationID = dbo.Station.StationID ";
            sql += "WHERE dbo.RadarDetection.RSID = " + rsID + " ";
         
            return sql;
        }

    }

   
}
