﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Security.Cryptography;
using System.Linq;

namespace VehicleViewLIB
{
    public delegate void ConnectionConnected(ClientSocket client);
    public delegate void ConnectionConnectError(ClientSocket client, string ErrorMessage);
    public delegate void ConnectionReceived(ClientSocket client, byte[] data);
    public delegate void ConnectionDisconnected(ClientSocket client);

    public class ClientSocket
    {
        public int ID { get; set; }
        public bool ShowMessage { get; set; }
        public IPEndPoint RemoteEndPoint { get; set; }

        public event ConnectionConnected OnConnected;
        public event ConnectionReceived OnReceived;
        public event ConnectionDisconnected OnDisconnected;
        public event ConnectionConnectError OnConnectError;

        private Socket workSocket;
        private const int BUFFSIZE = 1024;
        private byte[] buffer = new byte[BUFFSIZE];
        private const int SPLITSIZE = BUFFSIZE / 2;

        public ClientSocket()
        {
            ShowMessage = false;
        }

        public void StartWithSocket(Socket socket)
        {
            workSocket = socket;
            Receive();
        }

        public bool IsConnected()
        {
            if (workSocket == null)
            {
                return false;
            }
            else
            {
                return workSocket.Connected;
            }
        }

        public void Connect() { Connect(this.RemoteEndPoint); }
        public void Connect(IPEndPoint RemoteEP)
        {
            try
            {
                this.RemoteEndPoint = RemoteEP;
                Socket cs = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
                cs.BeginConnect(this.RemoteEndPoint, new AsyncCallback(ConnectCallback), cs);
            }
            catch (Exception ex)
            {
                msgout("ERROR\tConnect\t" + ex.Message);
                if (OnConnectError != null)
                {
                    OnConnectError(this, ex.Message);
                }
            }
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket cs = (Socket)ar.AsyncState;
                cs.EndConnect(ar);
                //msgout("Connected to " + cs.RemoteEndPoint.ToString());

                workSocket = cs;
                this.RemoteEndPoint = (IPEndPoint)workSocket.RemoteEndPoint;

                if (OnConnected != null)
                {
                    OnConnected(this);
                }

                Receive();

            }
            catch (Exception ex)
            {
                msgout("ERROR\tConnectCallback\t" + ex.Message);
                if (OnConnectError != null)
                {
                    OnConnectError(this, ex.Message);
                }
            }
        }

        private void Receive()
        {
            try
            {
                workSocket.BeginReceive(buffer, 0, BUFFSIZE, 0, new AsyncCallback(ReceiveCallback), null);
            }
            catch (Exception ex)
            {
                msgout("ERROR\tReceive\t" + ex.Message);
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                int readbytes = workSocket.EndReceive(ar);

                if (readbytes == 0)
                {
                    msgout("Connection Disconnected");
                    if (OnDisconnected != null)
                    {
                        OnDisconnected(this);
                    }
                }
                else
                {
                    if (OnReceived != null)
                    {
                        byte[] receivedData = new byte[readbytes];
                        Array.Copy(buffer, receivedData, readbytes);
                        OnReceived(this, receivedData);
                    }
                }
                //Wait for next round
                Receive();
            }
            catch (SocketException ex)
            {
                msgout("ERROR Connection Disconnected\n" + ex.Message);
                if (OnDisconnected != null)
                {
                    OnDisconnected(this);
                }
            }
            catch (ObjectDisposedException ex)
            {
                msgout("ERROR Connection Disconnected\n" + ex.Message);
                if (OnDisconnected != null)
                {
                    OnDisconnected(this);
                }
            }
        }

        public void SendTextASCII(String asciiText)
        {
            byte[] data = Encoding.ASCII.GetBytes(asciiText);
            SendBytes(data);
        }

        public void SendTextUTF8(String utf8Text)
        {
            byte[] data = Encoding.UTF8.GetBytes(utf8Text);
            SendBytes(data);
        }

        object SendBytes_Lock = new object();
        public void SendBytes(byte[] data)
        {
            lock (SendBytes_Lock)
            {
                try
                {
                    if (data.Length == 0)
                        return;

                    if (data.Length > SPLITSIZE)
                    {
                        int n = data.Length / SPLITSIZE;
                        for (int i = 0; i < n + 1; i++)
                        {
                            int pos = i * SPLITSIZE;
                            int len = (i == n) ? (data.Length % SPLITSIZE) : SPLITSIZE;
                            byte[] splitdata = new byte[len];
                            Array.Copy(data, pos, splitdata, 0, len);
                            Send(splitdata);
                        }
                    }
                    else
                    {
                        Send(data);
                    }
                }
                catch (Exception ex)
                {
                    msgout("ERROR\tSendBytes\t" + ex.Message);
                }
            }
        }

        private void Send(byte[] data)
        {
            try
            {
                if (!workSocket.Connected)
                {
                    return;
                }

                workSocket.BeginSend(data, 0, data.Length, 0, new AsyncCallback(SendCallback), workSocket);
            }
            catch (Exception ex)
            {
                msgout("ERROR\tSend\t" + ex.Message);
            }
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket cs = (Socket)ar.AsyncState;
                cs.EndSend(ar);
                //int bytesSent = cs.EndSend(ar);
                //msgout("Sent " + bytesSent + " bytes");
            }
            catch (Exception ex)
            {
                msgout("ERROR\tSendCallback\t" + ex.Message);
            }
        }

        public void Close()
        {
            try
            {
                this.workSocket.Shutdown(SocketShutdown.Both);
            }
            catch
            { }
        }

        private void msgout(string text)
        {
            if (ShowMessage)
            {
                text = "[Client " + this.ID + "] > " + text;
                Console.WriteLine(text);
            }

        }

    }

    //===================================================================================
    //===================================================================================

    public delegate void ClientConnected(ClientSocket client);
    public delegate void ClientReceived(ClientSocket client, byte[] data);
    public delegate void ClientDisconnected(ClientSocket client);

    public class ServerSocket
    {
        public event ClientConnected OnClientConnected;
        public event ClientReceived OnClientReceived;
        public event ClientDisconnected OnClientDisconnected;

        public int MaxClients { get; set; }
        public bool ShowMessage { get; set; }

        Hashtable hClients = new Hashtable();
        private IEnumerable Clients { get { return hClients.Values; } }


        public ServerSocket()
        {
            MaxClients = 10;
            ShowMessage = false;
        }

        public void StartListening(IPEndPoint LocalEndPoint)
        {
            try
            {
                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                listener.Bind(LocalEndPoint);
                listener.Listen(MaxClients);
                listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

            }
            catch (Exception ex)
            {
                msgout("ERROR\tListening\t" + ex.Message);
                throw;
            }
        }

        private object SyncRoot { get { return this; } }
        private int CID = 0;

        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {

                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);

                listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                ClientSocket client = new ClientSocket();

                client.ID = CID;
                CID++;
                if (CID > MaxClients) CID = 0;

                client.RemoteEndPoint = (IPEndPoint)handler.RemoteEndPoint;

                lock (SyncRoot)
                {
                    hClients[client.ID] = client;
                }

                //Console.WriteLine("Connected to {0} ({1})",handler.RemoteEndPoint.ToString(),
                //                 client.ID.ToString());	

                // Register Events which might be occurred from Client class instance
                client.OnReceived += HandleClientOnReceived;
                client.OnDisconnected += HandleClientOnDisconnected;

                // Run - It will run BeginReceive and the function 
                // HandleClientOnReceived will be fired.
                client.StartWithSocket(handler);

                // Raise this event to this class instance
                if (OnClientConnected != null)
                    OnClientConnected(client);

            }
            catch (ObjectDisposedException)
            {

            }
            catch (SocketException)
            {

            }
            catch (Exception ex)
            {
                msgout("ERROR\tAcceptCallback\t" + ex.Message);
                throw;
            }
        }

        private void HandleClientOnDisconnected(ClientSocket client)
        {
            lock (SyncRoot) hClients.Remove(client.ID);

            if (OnClientDisconnected != null)
                OnClientDisconnected(client);
        }

        private void HandleClientOnReceived(ClientSocket client, byte[] data)
        {
            if (OnClientReceived != null)
                OnClientReceived(client, data);
        }

        public void BroadCastData(byte[] data)
        {
            foreach (ClientSocket c in Clients)
            {
                lock (SyncRoot) c.SendBytes(data);
            }
        }

        public void BroadCastTextASCII(string text)
        {
            byte[] data = Encoding.ASCII.GetBytes(text);

            foreach (ClientSocket c in Clients)
            {
                lock (SyncRoot) c.SendBytes(data);
            }
        }

        public void SendData(ClientSocket client, byte[] data)
        {
            client.SendBytes(data);
        }

        public void SendTextASCII(ClientSocket client, string text)
        {
            client.SendTextASCII(text);
        }

        public void SendTextUTF8(ClientSocket client, string text)
        {
            client.SendTextUTF8(text);
        }

        private void msgout(string text)
        {
            if (ShowMessage)
            {
                text = "[Server] > " + text;
                Console.WriteLine(text);
            }

        }
    }

}