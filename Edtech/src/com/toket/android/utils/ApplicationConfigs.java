package com.toket.android.utils;

import android.os.Environment;


public interface ApplicationConfigs {

	//#######################
	// ImageLoader //
	//#######################
	public static final String IMAGELOADER_NAME = "MUIC";
	public static final String DATA_STORAGE = Environment.getExternalStorageDirectory().toString() + "/Android/data/";
	public static final String MUIC_IMAGE = Environment.getExternalStorageDirectory().toString() + "/MUIC";

	//#######################
	// Assets Path //
	//#######################
	public static final String PATH_START	= "start.xml";
	public static final String PATH_HOME	= "views/home.xml";
	public static final String PATH_INDEX	= "views/index.xml";
	public static final String PATH_MEDIA	= "views/media.xml";
	public static final String PATH_AR		= "views/ar.xml";
	public static final String PATH_CONTACT	= "views/contact.xml";
	
}
