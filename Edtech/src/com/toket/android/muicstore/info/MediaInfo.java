package com.toket.android.muicstore.info;

import java.util.ArrayList;

public class MediaInfo {
	
	private ArrayList<MediaClipInfo> clip = new ArrayList<MediaClipInfo>();

	public ArrayList<MediaClipInfo> getClip() {
		return clip;
	}

	public void addClip(MediaClipInfo clip) {
		this.clip.add(clip);
	}
	
}
