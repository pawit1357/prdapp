package com.toket.android.muicstore.info;

import java.util.ArrayList;

public class IndexChapterInfo {
	
	private String id;
	private String name;
	private String src;
	private ArrayList<IndexTitleInfo> title = new ArrayList<IndexTitleInfo>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public ArrayList<IndexTitleInfo> getTitle() {
		return title;
	}
	public void addTitle(IndexTitleInfo title) {
		this.title.add(title);
	}
	
}
