package com.toket.android.muicstore.info;

import java.util.ArrayList;

public class HomeInfo {
	
	private ArrayList<HomeCoverInfo> cover = new ArrayList<HomeCoverInfo>();

	public ArrayList<HomeCoverInfo> getCover() {
		return cover;
	}

	public void addCover(HomeCoverInfo cover) {
		this.cover.add(cover);
	}
	
}
