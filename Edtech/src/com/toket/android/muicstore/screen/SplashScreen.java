package com.toket.android.muicstore.screen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.toket.android.muicstore.R;
import com.toket.android.muicstore.activity.MUICStoreActivity;

public class SplashScreen extends MUICStoreActivity{
	
	private static Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		
		mContext = this;
		
		initActivity();
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					Thread.sleep(1500);
				} catch (Exception e) {
					// TODO: handle exception
				}finally{
					Intent goToScreen = new Intent(mContext, MainScreen.class);
					startActivity(goToScreen);
					finish();
				}
			}
		}).start();
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void cleanUp() {
		// TODO Auto-generated method stub
		
	}

}
