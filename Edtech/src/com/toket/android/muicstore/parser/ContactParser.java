package com.toket.android.muicstore.parser;

import android.content.Context;

import com.toket.android.muicstore.info.ContactInfo;
import com.toket.android.muicstore.info.ContactTeacherInfo;

public class ContactParser extends AbstractXMLParser{
	
	public ContactParser(Context context, String url) {
		super(context, url);
		// TODO Auto-generated constructor stub
	}
	
	public boolean connectInputStreamAsset(String assetsPath) {
		return super.connectInputStreamAsset(assetsPath);
	}
	
	public ContactInfo parserInfo(){
		ContactInfo info_ = new ContactInfo();
		while(content.indexOf("<teacher") > -1){
			attribute		= content;
			String id		= getAttributeValue("id");
			String picture		= getXMLValue("picture");
			String lecturer		= getXMLValue("Lecturer");
			String subject		= getXMLValue("subject");
			String email		= getXMLValue("email");
			String phone		= getXMLValue("phone");
			String description		= getXMLValue("description");

			ContactTeacherInfo item = new ContactTeacherInfo();
			item.setId(id);
			item.setPicture(picture);
			item.setLecturer(lecturer);
			item.setSubject(subject);
			item.setEmail(email);
			item.setPhone(phone);
			item.setDescription(description);
			
			info_.addTeacher(item);
		}
		return info_;
	}
	
}
