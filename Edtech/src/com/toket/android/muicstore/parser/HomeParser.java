package com.toket.android.muicstore.parser;

import android.content.Context;

import com.toket.android.muicstore.info.HomeCoverInfo;
import com.toket.android.muicstore.info.HomeInfo;

public class HomeParser extends AbstractXMLParser{
	
	public HomeParser(Context context, String url) {
		super(context, url);
		// TODO Auto-generated constructor stub
	}
	
	public boolean connectInputStreamAsset(String assetsPath) {
		return super.connectInputStreamAsset(assetsPath);
	}
	
	public HomeInfo parserInfo(){
		HomeInfo info_ = new HomeInfo();
		while(content.indexOf("<cover") > -1){
			attribute		= content;
			String id		= getAttributeValue("id");
			String name		= getXMLValue("name");
			String src		= getXMLValue("src");

			HomeCoverInfo item = new HomeCoverInfo();
			item.setId(id);
			item.setName(name);
			item.setSrc(src);
			
			info_.addCover(item);
		}
		return info_;
	}
	
}
