package com.toket.android.muicstore.parser;

import android.content.Context;

import com.toket.android.muicstore.info.IndexChapterInfo;
import com.toket.android.muicstore.info.IndexInfo;
import com.toket.android.muicstore.info.IndexItemInfo;
import com.toket.android.muicstore.info.IndexTitleInfo;

public class IndexParser extends AbstractXMLParser{
	
	public IndexParser(Context context, String url) {
		super(context, url);
		// TODO Auto-generated constructor stub
	}
	
	public boolean connectInputStreamAsset(String assetsPath) {
		return super.connectInputStreamAsset(assetsPath);
	}
	
	public IndexInfo parserInfo(){
		IndexInfo info_ = new IndexInfo();
		while(content.indexOf("<chapter") > -1){
			attribute		= content;
			node_content	= getXMLValue("chapter");
			String id		= getAttributeValue("id");
			String name		= getAttributeValue("name");
			String src		= getAttributeValue("src");
			
			IndexChapterInfo item = new IndexChapterInfo();
			item.setId(id);
			item.setName(name);
			item.setSrc(src);
			
//			System.out.println("## id = " + id);
//			System.out.println("## name = " + name);
//			System.out.println("## src = " + src);
//			System.out.println("## node_content = " + node_content);
//			System.out.println("## content = " + content);
			
			attribute = node_content;
			while(node_content.indexOf("<title") > -1){
				node_content2		= getXMLNodeValue("title");
				if(node_content2.trim().length() == 0) continue;
				String title_id		= getAttributeValue("id");
				String title_name	= getAttributeValue("name");
				String title_src	= getAttributeValue("src");

//				System.out.println("## title_id = " + title_id);
//				System.out.println("## title_name = " + title_name);
//				System.out.println("## title_src = " + title_src);
//				System.out.println("## node_content = " + node_content);
				
				IndexTitleInfo item2 = new IndexTitleInfo();
				item2.setId(title_id);
				item2.setName(title_name);
				item2.setSrc(title_src);
				
//				attribute = node_content2;
				while(node_content2.indexOf("<item") > -1){
					String item_id		= getAttributeValue("id");
					String item_src		= getAttributeValue("src");
					String item_item	= getXMLNodeValue2("item");

//					System.out.println("## item_id = " + item_id);
//					System.out.println("## item_src = " + item_src);
//					System.out.println("## item_item = " + item_item);
					
					IndexItemInfo item3 = new IndexItemInfo();
					item3.setId(item_id);
					item3.setSrc(item_src);
					item3.setItem(item_item);
					
					item2.addItem(item3);
				}
				
				item.addTitle(item2);
			}
			
			
			info_.addChapter(item);
		}
		return info_;
	}
	
}
