package com.toket.android.muicstore.view;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.toket.android.muicstore.R;
import com.toket.android.muicstore.info.StartInfo;
import com.toket.android.muicstore.screen.MainScreen;

public class BottomView extends AbstractView implements OnClickListener{
	
	public static final int MENU_HOME		= R.id.btn_home;
	public static final int MENU_INDEX		= R.id.btn_index;
	public static final int MENU_MEDIA		= R.id.btn_media;
	public static final int MENU_AR			= R.id.btn_ar;
	public static final int MENU_CONTACT	= R.id.btn_contact;
	
	private StartInfo info;
	private RelativeLayout Layout_bottomview;
	
	private ImageView btn_home, btn_index, btn_media, btn_ar, btn_contact;
	
	public BottomView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setStartInfo(StartInfo info){
		this.info = info;
	}
	
	public StartInfo getStartInfo(){
		return info;
	}
	
	public void setVisibleView(boolean value){
		if(value){
			Layout_bottomview.setVisibility(View.VISIBLE);
		}else{
			Layout_bottomview.setVisibility(View.GONE);
		}
	}
	
	public boolean isViewVisible(){
		return Layout_bottomview.getVisibility() == View.VISIBLE;
	}
	
	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		Layout_bottomview = (RelativeLayout) screen.findViewById(R.id.Layout_bottomview);
		
		btn_home	= (ImageView) screen.findViewById(R.id.btn_home);
		btn_index	= (ImageView) screen.findViewById(R.id.btn_index);
		btn_media	= (ImageView) screen.findViewById(R.id.btn_media);
		btn_ar		= (ImageView) screen.findViewById(R.id.btn_ar);
		btn_contact	= (ImageView) screen.findViewById(R.id.btn_contact);
		
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		btn_home.setOnClickListener(this);
		btn_index.setOnClickListener(this);
		btn_media.setOnClickListener(this);
		btn_ar.setOnClickListener(this);
		btn_contact.setOnClickListener(this);
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub
		
	}
	
	public void onClickMenu(int menu_index){
		if(menu_index != R.id.btn_ar){
			removeAllMenuView();
			resetAllButton();
		}
		String name = "";
		switch (menu_index) {
		case R.id.btn_home:
			btn_home.setImageResource(R.drawable.home_r);
			name = info.getMenu().get(0).getName();
			((MainScreen)screen).getTopView().setTextTitle(name);
			((MainScreen)screen).getHomeView().setVisibleView(true);
			break;
		case R.id.btn_index:
			btn_index.setImageResource(R.drawable.index_r);
			name = info.getMenu().get(1).getName();
			((MainScreen)screen).getTopView().setTextTitle(name);
			((MainScreen)screen).getIndexExpandListView().setVisibleView(true);
			break;
		case R.id.btn_media:
			btn_media.setImageResource(R.drawable.media_r);
			name = info.getMenu().get(2).getName();
			((MainScreen)screen).getTopView().setTextTitle(name);
			((MainScreen)screen).getMediaView().setVisibleView(true);
			break;
		case R.id.btn_ar:
//			btn_ar.setImageResource(R.drawable.ar_r);
//			name = info.getMenu().get(3).getName();
//			((MainScreen)screen).getTopView().setTextTitle(name);
			((MainScreen)screen).getARView().setVisibleView(true);
			break;
		case R.id.btn_contact:
			btn_contact.setImageResource(R.drawable.contact_r);
			name = info.getMenu().get(4).getName();
			((MainScreen)screen).getTopView().setTextTitle(name);
			((MainScreen)screen).getContactView().setVisibleView(true);
			break;
		}
	}
	
	private void resetAllButton(){
		btn_home.setImageResource(R.drawable.button_home);
		btn_index.setImageResource(R.drawable.button_index);
		btn_media.setImageResource(R.drawable.button_media);
		btn_ar.setImageResource(R.drawable.button_ar);
		btn_contact.setImageResource(R.drawable.button_contact);
	}
	
	private void removeAllMenuView(){
		((MainScreen)screen).getHomeView().setVisibleView(false);
		((MainScreen)screen).getIndexExpandListView().setVisibleView(false);
		((MainScreen)screen).getIndexDetailView().setVisibleView(false, "", "");
		((MainScreen)screen).getMediaView().setVisibleView(false);
		((MainScreen)screen).getARView().setVisibleView(false);
		((MainScreen)screen).getContactView().setVisibleView(false);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		onClickMenu(v.getId());
	}

}
