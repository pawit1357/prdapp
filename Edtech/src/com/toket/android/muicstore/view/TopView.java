package com.toket.android.muicstore.view;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.toket.android.muicstore.R;
import com.toket.android.muicstore.screen.MainScreen;

public class TopView extends AbstractView{
	
	private RelativeLayout Layout_topview;
	
	private TextView text_title_top_view;
	private ImageView btn_back;
	
	public TopView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setVisibleView(boolean value){
		if(value){
			Layout_topview.setVisibility(View.VISIBLE);
		}else{
			Layout_topview.setVisibility(View.GONE);
		}
	}
	
	public void setVisibleBackButton(boolean value){
		if(value){
			btn_back.setVisibility(View.VISIBLE);
		}else{
			btn_back.setVisibility(View.GONE);
		}
	}

	public boolean isViewVisible(){
		return Layout_topview.getVisibility() == View.VISIBLE;
	}
	
	public void setTextTitle(String title_msg){
		text_title_top_view.setText(title_msg);
	}
	
	public String getTextTitle(){
		return text_title_top_view.getText().toString();
	}
	
	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		Layout_topview = (RelativeLayout) screen.findViewById(R.id.Layout_topview);

		btn_back			= (ImageView) screen.findViewById(R.id.btn_back);
		text_title_top_view = (TextView) screen.findViewById(R.id.text_title_top_view);
		
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		text_title_top_view.setSelected(true);
		btn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((MainScreen)screen).onBackPressed();
			}
		});
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub
		
	}

}
