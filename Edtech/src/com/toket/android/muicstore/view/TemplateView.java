package com.toket.android.muicstore.view;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

import com.toket.android.muicstore.R;

public class TemplateView extends AbstractView{
	
	private RelativeLayout view;
	
	public TemplateView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setVisibleView(boolean value){
		if(value){
			view.setVisibility(View.VISIBLE);
		}else{
			view.setVisibility(View.GONE);
		}
	}
	
	public boolean isViewVisible(){
		return view.getVisibility() == View.VISIBLE;
	}
	
	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		view = (RelativeLayout) screen.findViewById(R.id.text_title_top_view);
		
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub
		
	}

}
