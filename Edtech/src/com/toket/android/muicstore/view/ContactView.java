package com.toket.android.muicstore.view;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.toket.android.muicstore.R;
import com.toket.android.muicstore.info.ContactInfo;
import com.toket.android.muicstore.info.ContactTeacherInfo;
import com.toket.android.muicstore.parser.ContactParser;
import com.toket.android.utils.ScalableImageView;

public class ContactView extends AbstractView{
	
	private LinearLayout Layout_contact_view;
	
	private ScalableImageView image_contact_view;
	private TextView text_contact_lecturer;
	private TextView text_contact_subject;
	private TextView text_contact_email;
	private TextView text_contact_phone;
	
	private LinearLayout view_contact_description;
	
	public ContactView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setVisibleView(boolean value){
		if(value){
			Layout_contact_view.setVisibility(View.VISIBLE);
		}else{
			Layout_contact_view.setVisibility(View.GONE);
		}
	}
	
	public boolean isViewVisible(){
		return Layout_contact_view.getVisibility() == View.VISIBLE;
	}

	public void loadContent(String path){
		MyJsonParser parser = new MyJsonParser();
		parser.execute(path);
	}
	
	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		Layout_contact_view = (LinearLayout) screen.findViewById(R.id.Layout_contact_view);
		
		image_contact_view		= (ScalableImageView) screen.findViewById(R.id.image_contact_view);
		text_contact_lecturer	= (TextView) screen.findViewById(R.id.text_contact_lecturer);
		text_contact_subject	= (TextView) screen.findViewById(R.id.text_contact_subject);
		text_contact_email		= (TextView) screen.findViewById(R.id.text_contact_email);
		text_contact_phone		= (TextView) screen.findViewById(R.id.text_contact_phone);
		
		view_contact_description = (LinearLayout) screen.findViewById(R.id.view_contact_description);
		
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		text_contact_lecturer.setSelected(true);
	}

	private class MyJsonParser extends AsyncTask<String, Void, ContactInfo>{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected ContactInfo doInBackground(String... url) {
			// TODO Auto-generated method stub
			ContactParser parser = new ContactParser(context, url[0]);
			if(parser.connectInputStreamAsset(url[0])){
				return parser.parserInfo();
			}
			return null;
		}

		@Override
		protected void onPostExecute(ContactInfo result) {
			// TODO Auto-generated method stub
			if(result != null){
				setUpContent(result.getTeacher().get(0));
			}
		}
		
	}

	private void setUpContent(ContactTeacherInfo info) {
		// TODO Auto-generated method stub
		try {
			// get input stream
			InputStream ims = screen.getAssets().open(info.getPicture());
			// load image as Drawable
			Drawable d = Drawable.createFromStream(ims, null);
			image_contact_view.setImageDrawable(d);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		text_contact_lecturer.setText(info.getLecturer());
		text_contact_subject.setText(info.getSubject());
		text_contact_email.setText(info.getEmail());
		text_contact_phone.setText(info.getPhone());
		
		TextView text_contact_description = new TextView(context);
		text_contact_description.setText(info.getDescription());
		text_contact_description.setTextSize(15);
		
		view_contact_description.addView(text_contact_description);
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub
		
	}

}
