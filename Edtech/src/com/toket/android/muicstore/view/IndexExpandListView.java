package com.toket.android.muicstore.view;

import pl.polidea.treeview.TreeBuilder;
import pl.polidea.treeview.TreeNodeInfo;
import pl.polidea.treeview.TreeViewList;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;

import com.toket.android.muicstore.R;
import com.toket.android.muicstore.adapter.IndexAdapter;
import com.toket.android.muicstore.adapter.IndexAdapter.OnHandleItemClickListener;
import com.toket.android.muicstore.info.IndexChapterInfo;
import com.toket.android.muicstore.info.IndexInfo;
import com.toket.android.muicstore.info.IndexItemInfo;
import com.toket.android.muicstore.info.IndexTitleInfo;
import com.toket.android.muicstore.parser.IndexParser;
import com.toket.android.muicstore.screen.MainScreen;

public class IndexExpandListView extends AbstractView{
	
	private LinearLayout Layout_index_expandlist_view;
	
	private static final int LEVEL_NUMBER = 3;
    private IndexAdapter mIndexAdapter;
    private TreeViewList treeView;
    
	public IndexExpandListView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setVisibleView(boolean value){
		if(value){
			Layout_index_expandlist_view.setVisibility(View.VISIBLE);
		}else{
			Layout_index_expandlist_view.setVisibility(View.GONE);
		}
	}
	
	public boolean isViewVisible(){
		return Layout_index_expandlist_view.getVisibility() == View.VISIBLE;
	}

	public void loadContent(String path){
		MyJsonParser parser = new MyJsonParser();
		parser.execute(path);
	}
	
	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		
		Layout_index_expandlist_view = (LinearLayout) screen.findViewById(R.id.Layout_index_expandlist_view);

		treeView = (TreeViewList) screen.findViewById(R.id.mainTreeView);
		mIndexAdapter = new IndexAdapter(screen, ((MainScreen)screen).getTreeStateManager(), LEVEL_NUMBER);
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub
		
	}

	private void addItemExpand(final IndexInfo info){
		final TreeBuilder<String> treeBuilder = new TreeBuilder<String>(((MainScreen)screen).getTreeStateManager());
		for(IndexChapterInfo chapter : info.getChapter()){
			treeBuilder.sequentiallyAddNextNode(chapter.getName(), 0);
			for(IndexTitleInfo title : chapter.getTitle()){
				treeBuilder.sequentiallyAddNextNode(title.getName(), 1);
				for(IndexItemInfo item : title.getItem()){
					treeBuilder.sequentiallyAddNextNode(item.getId() + " " + item.getItem(), 2);
				}
			}
		}
		mIndexAdapter.setOnHandleItemClickListener(new OnHandleItemClickListener() {
			
			@Override
			public void onItemClick(View view, TreeNodeInfo<String> node_info, String value) {
				// TODO Auto-generated method stub
				if(!node_info.isWithChildren()){
	        		
					for(IndexChapterInfo chapter : info.getChapter()){
						if(value.equalsIgnoreCase(chapter.getName())){
							if(chapter.getSrc().length() != 0){
								((MainScreen)screen).getIndexDetailView().setVisibleView(true, chapter.getSrc(), chapter.getName());
							}
							break;
						}
						for(IndexTitleInfo title : chapter.getTitle()){
							if(value.equalsIgnoreCase(title.getName())){
								if(title.getSrc().length() != 0){
									((MainScreen)screen).getIndexDetailView().setVisibleView(true, title.getSrc(), title.getName());
								}
								break;
							}
							for(IndexItemInfo item : title.getItem()){
								if(value.equalsIgnoreCase(item.getId() + " " + item.getItem())){
									if(item.getSrc().length() != 0){
										((MainScreen)screen).getIndexDetailView().setVisibleView(true, item.getSrc(), title.getName());
									}
									break;
								}
							}
						}
					}
				}
			}
		});
		treeView.setAdapter(mIndexAdapter);
//		treeView.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
//					long arg3) {
//				// TODO Auto-generated method stub
//				if(item_src.get(position).length() == 0) return;
//				((MainScreen)screen).getIndexDetailView().setVisibleView(true, item_src.get(position));
//				Toast.makeText(context, item_src.get(position), Toast.LENGTH_SHORT).show();
//			}
//		});
	}
	
	private class MyJsonParser extends AsyncTask<String, Void, IndexInfo>{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
//			showProgressDialog("", "Loading...", true);
		}

		@Override
		protected IndexInfo doInBackground(String... url) {
			// TODO Auto-generated method stub
			IndexParser parser = new IndexParser(context, url[0]);
			if(parser.connectInputStreamAsset(url[0])){
				return parser.parserInfo();
			}
			return null;
		}

		@Override
		protected void onPostExecute(IndexInfo result) {
			// TODO Auto-generated method stub
			if(result != null){
//				Toast.makeText(context, "Loading Success.", Toast.LENGTH_SHORT).show();
				addItemExpand(result);
			}
			else{
//				Toast.makeText(context, "Loading fail.", Toast.LENGTH_SHORT).show();
			}
//			dismissProgressDialog();
		}
		
	}

}
