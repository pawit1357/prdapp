package com.toket.android.muicstore;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.toket.android.muicstore.screen.SplashScreen;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent goToScreen = new Intent(this, SplashScreen.class);
		startActivity(goToScreen);
		finish();
		
	}
	
}
