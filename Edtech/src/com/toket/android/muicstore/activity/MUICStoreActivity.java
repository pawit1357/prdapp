package com.toket.android.muicstore.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.util.DisplayMetrics;
import android.widget.Toast;

public abstract class MUICStoreActivity extends Activity{
	protected ProgressDialog progressBar;
	public boolean isCancleProgressBar;
	protected int screenWidth, screenHeight;
	
	abstract protected void initLayout();
	abstract protected void initValue();
	abstract protected void cleanUp();

	/**
	 * default initial activity
	 * add this method in "onCreate()"
	 */
	protected void initActivity(){
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
		dm = null;
		
		initLayout();
		initValue();
	}

	public void onDestroy(){
		cleanUp();
		super.onDestroy();
	}

	public void showToast(final String text, final int duration){
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), text, duration).show();
			}
		});
	}
	
	public ProgressDialog showProgressDialog(final String header, final String body,
			final boolean cancelable) {
		isCancleProgressBar = false;
		dismissProgressDialog();
		
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub

				try {		
					progressBar = new ProgressDialog(MUICStoreActivity.this);
					progressBar.setMax(2);
					progressBar.setTitle(header);
					progressBar.setMessage(body);
					progressBar.setCancelable(cancelable);
					progressBar.setIndeterminate(true);
					progressBar.show();
					
					progressBar.setOnCancelListener(new OnCancelListener() {
						
						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							isCancleProgressBar = true;
						}
					});
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		return progressBar;
	}

	
	public void dismissProgressDialog() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					if (progressBar != null && progressBar.isShowing()){
						progressBar.dismiss();
						progressBar = null;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
	}
	
	public boolean isShowProgressDialog(){
		if(progressBar != null){
			return progressBar.isShowing();
		}else{
			return false;
		}
	}
	
}