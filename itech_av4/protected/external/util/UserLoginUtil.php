<?php
class UserLoginUtil {
	private static $userPermissions = array ();
	public static function hasPermission($permissionCodes) {
		$userLoginId = self::getUserLoginId ();
		if ($userLoginId == null) {
			return false;
		}
		
		$currentTime = time ();
		// Cache timeout in 5 minites
		$cacheTimeout = (5 * 60 * 1000);
		
		if (self::$userPermissions ['latestUpdateTime'] < ($currentTime - $cacheTimeout)) {
			$userLogin = UserLogin::model ()->with ( 'role' )->findByPk ( $userLoginId );
			$permissions = array ();
			if (isset ( $userLogin->role )) {
				$criteria = new CDbCriteria ();
				$criteria->condition = "role_id = '" . $userLogin->role->id . "'";
				$rolePermissions = RolePermission::model ()->with ( 'permission' )->findAll ( $criteria );
				foreach ( $rolePermissions as $rolePermission ) {
					if (! in_array ( $rolePermission->permission_code, $permissions )) {
						$permissions [count ( $permissions )] = $rolePermission->permission_code;
					}
				}
			}
			self::$userPermissions ['permissions'] = $permissions;
			self::$userPermissions ['latestUpdateTime'] = $currentTime;
		}
		
		$permissions = self::$userPermissions ['permissions'];
		foreach ( $permissionCodes as $permissionCode ) {
			if (in_array ( $permissionCode, $permissions )) {
				return true;
				break;
			}
		}
		
		return false;
	}
	public static function isLogin() {
		return isset ( $_SESSION ['USER_LOGIN_ID'] );
	}
	public static function logout() {
		unset ( $_SESSION ['USER_LOGIN_ID'] );
	}
	public static function authen($username, $password) {
		
		// test
		$criteria = new CDbCriteria ();
		$criteria->condition = "username = '" . $username . "' and password='" . md5 ( $password ) . "'";
		$userLogin = UserLogin::model ()->findAll ( $criteria );
		if (isset ( $userLogin [0] )) {
			$userLogin [0]->latest_login = date ( "Y-m-d H:i:s" );
			$userLogin [0]->update ();
			$_SESSION ['USER_LOGIN_ID'] = $userLogin [0]->id;
			return true;
		} else {
			$_SESSION ['FAIL_MESSAGE'] = 'Incorrect Username or Password!';
			return false;
		}
		
		// Skip LDAP if admin
		
		// if( $username == "admin" ){
		
		// $criteria = new CDbCriteria;
		// $criteria->condition="username = '".$username."' and password='".md5($password)."'";
		// $userLogin = UserLogin::model()->findAll($criteria);
		// if(isset($userLogin[0])) {
		
		// $userLogin[0]->latest_login = date("Y-m-d H:i:s");
		// $userLogin[0]->update();
		// $_SESSION['USER_LOGIN_ID'] = $userLogin[0]->id;
		// return true;
		// }else{
		// $_SESSION['FAIL_MESSAGE'] = 'Incorrect Username or Password!';
		// return false;
		// }
		
		// }else{
		
		// $ldap = new LDAP("10.27.101.243:389" ,"sky.local","MUIC", $username,$password);
		// $user = new User( $username,$password);
		// $ldap->authenticate($user);
		
		// $ldaprdn = $username.'@sky.local'; // ldap rdn or dn
		// $ldappass = $password; // associated password
		// // connect to ldap server
		// $ldapconn = ldap_connect("10.27.101.243:389")
		// or die("Could not connect to LDAP server.");
		
		// if ($ldapconn) {
		// // binding to ldap server
		// $ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);
		// // verify binding
		// if ($ldapbind) {
		// //echo "LDAP bind successful...";
		// $user_role = ( is_numeric(substr($username,1,1)) )? 3:4;
		// $criteria = new CDbCriteria ();
		// $criteria->condition = "username = '" . $username . "'";
		// $userLogin = UserLogin::model ()->findAll ( $criteria );
		// if (isset ( $userLogin [0] )) {
		
		// $userLogin [0]->latest_login = date ( "Y-m-d H:i:s" );
		// $userLogin [0]->update ();
		// $_SESSION ['USER_LOGIN_ID'] = $userLogin [0]->id;
		
		// return true;
		// } else {
		
		// // if not exist insert new to db
		// $model = new UserLogin ();
		// $model->parent = 1;
		// $model->role_id = $user_role;
		// $model->username = $username;
		// $model->password = md5 ( "" );
		// $model->status = "ACTIVE";
		// $model->create_by = "1";
		// $model->email = $username . "@mahidol.ac.th";
		// $model->latest_login = date ( "Y-m-d H:i:s" );
		
		// if ($model->save ()) {
		
		// list ( $name, $surname ) = split ( '.', $username );
		// $uInfo = new UserInformation ();
		// $uInfo->id = $model->id;
		// $uInfo->personal_card_id = $model->id;
		// $uInfo->first_name = $name;
		// $uInfo->last_name = $surname;
		// if ($uInfo->save ()) {
		// $_SESSION ['USER_LOGIN_ID'] = $model->id;
		// return true;
		// } else {
		// $_SESSION ['FAIL_MESSAGE'] = 'Incorrect Username or Password!';
		// return false;
		// }
		// } else {
		// $_SESSION ['FAIL_MESSAGE'] = 'Incorrect Username or Password!';
		// return false;
		// }
		// }
		// } else {
		// // echo "LDAP bind failed...";
		// $_SESSION ['FAIL_MESSAGE'] = 'Incorrect Username or Password!';
		// return false;
		// }
		// }else{
		// $_SESSION ['FAIL_MESSAGE'] = 'Incorrect Username or Password!';
		// return false;
		// }
		
		// $ldap = new LDAP("10.27.101.243:389" ,"sky.local","MUIC", $username,$password);
		// $user = new User( $username,$password);
		// $ldap->authenticate($user);
		
		// if( $user->auth_status == AuthStatus::OK ){
		
		// $user_role = ( is_numeric(substr($username,1,1)) )? 3:4;
		
		// $criteria = new CDbCriteria;
		// $criteria->condition="username = '".$username."'";
		// $userLogin = UserLogin::model()->findAll($criteria);
		// if(isset($userLogin[0])) {
		
		// $userLogin[0]->latest_login = date("Y-m-d H:i:s");
		// $userLogin[0]->update();
		// $_SESSION['USER_LOGIN_ID'] = $userLogin[0]->id;
		
		// return true;
		// }else{
		
		// //if not exist insert new to db
		// $model = new UserLogin();
		// $model->parent = 1;
		// $model->role_id = $user_role;
		// $model->username = $username;
		// $model->password = md5("");
		// $model->status = "ACTIVE";
		// $model->create_by = "1";
		// $model->email = $user->username."@mahidol.ac.th";
		// $model->latest_login = date("Y-m-d H:i:s");
		
		// if($model->save()){
		
		// list($name, $surname) = split(' ', $user->name);
		// $uInfo = new UserInformation();
		// $uInfo->id=$model->id;
		// $uInfo->personal_card_id=$model->id;
		// $uInfo->first_name = $name;
		// $uInfo->last_name = $surname;
		// if($uInfo->save()){
		// $_SESSION['USER_LOGIN_ID'] = $model->id;
		// return true;
		// }else{
		// $_SESSION['FAIL_MESSAGE'] = 'Incorrect Username or Password!';
		// return false;
		// }
		
		// }else{
		// $_SESSION['FAIL_MESSAGE'] = 'Incorrect Username or Password!';
		// return false;
		// }
		// }
		// }else{
		// $_SESSION['FAIL_MESSAGE'] = 'Incorrect Username or Password!';
		// return false;
		// }
		
		// }
	}
	public static function getUserLoginId() {
		if (isset ( $_SESSION ['USER_LOGIN_ID'] )) {
			return $_SESSION ['USER_LOGIN_ID'];
		} else {
			return null;
		}
	}
	public static function getUserLogin() {
		if (self::isLogin ()) {
			$userLogin = UserLogin::model ()->findByPk ( self::getUserLoginId () );
			return $userLogin;
		} else {
			return null;
		}
	}
	public static function getUserRole() {
		if (self::isLogin ()) {
			$userLogin = UserLogin::model ()->findByPk ( self::getUserLoginId () );
			return $userLogin->role->id;
		} else {
			return null;
		}
	}
	public static function getUserRoleName() {
		if (self::isLogin ()) {
			$userLogin = UserLogin::model ()->findByPk ( self::getUserLoginId () );
			return $userLogin->role->name;
		} else {
			return null;
		}
	}
	public static function areUserRole($roles) {
		if (self::isLogin ()) {
			$userLogin = UserLogin::model ()->findByPk ( self::getUserLoginId () );
			return in_array ( $userLogin->role->name, $roles );
		} else {
			return null;
		}
	}
	public static function areUserRoleById($roles, $userId) {
		if (self::isLogin ()) {
			$userLogin = UserLogin::model ()->findByPk ( $userId );
			return in_array ( $userLogin->role->name, $roles );
		} else {
			return null;
		}
	}
	public static function getUserById($userId) {
		$userLogin = UserLogin::model ()->findByPk ( $userId );
		return $userLogin;
	}
	public static function isBlackList($userId) {
		$times = 0;
		$reqs = RequestBorrow::model ()->findAll ( array (
				'condition' => "user_login_id = '" . $userId . "'" 
		) );
		
		if (isset ( $reqs ) && count ( $reqs ) > 0) {
			
			foreach ( $reqs as $req ) {
				
				$RequestBorrowEquipmentTypes = RequestBorrowEquipmentType::model ()->findAll ( array (
						'condition' => "request_borrow_id = '" . $req->id . "'" 
				) );
				$isLate = false;
				
				if (isset ( $RequestBorrowEquipmentTypes ) && count ( $RequestBorrowEquipmentTypes ) > 0) {
					
					foreach ( $RequestBorrowEquipmentTypes as $RequestBorrowEquipmentType ) {
						$RequestBorrowEquipmentTypeItems = RequestBorrowEquipmentTypeItem::model ()->findAll ( array (
								'condition' => "request_borrow_equipment_type_id = '" . $RequestBorrowEquipmentType->id . "'" 
						) );
						
						if (isset ( $RequestBorrowEquipmentTypeItems ) && count ( $RequestBorrowEquipmentTypeItems ) > 0) {
							foreach ( $RequestBorrowEquipmentTypeItems as $RequestBorrowEquipmentTypeItem ) {
								if ($RequestBorrowEquipmentTypeItem->return_price > 0) {
									$isLate = true;
								}
							}
						}
					}
				}
				
				if ($isLate) {
					$times = $times + 1;
				}
			}
		}
		
		return ($times >= 2) ? true : false;
	}
	
	public static function isInUseEquipment($userId) {
		$inuse = false;
		$reqs = RequestBorrow::model ()->findAll ( array (
				'condition' => " user_login_id='".$userId."' And status_code not in ('R_B_NEW_RETURNED','R_B_NEW_CANCELLED','R_B_NEW_DISAPPROVE')"
		) );
		if (isset ( $reqs ) && count ( $reqs ) > 0) {
			
			$inuse = ture;
		}
	
		return $inuse;
	}
}
interface UserRoles {
	const ADMIN = "Admin";
	const STAFF = "Staff";
	const STUDENT = "Student";
	const LECTURER = "Lecturer";
	const STAFF_AV = "StaffAV";
	const STUDENT_FAA = "StudentFAA";
}
?>