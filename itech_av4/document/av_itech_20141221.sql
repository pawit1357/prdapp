-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 21, 2014 at 02:50 PM
-- Server version: 5.5.29-cll
-- PHP Version: 5.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `av_itech`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
`id` int(11) NOT NULL,
  `department_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `department_code`, `name`, `description`) VALUES
(1, '10', 'การเงิน และ การบัญชี', ''),
(2, '001', 'Ed.Tech', 'Education Technology');

-- --------------------------------------------------------

--
-- Table structure for table `enumeration`
--

CREATE TABLE IF NOT EXISTS `enumeration` (
`id` int(11) NOT NULL,
  `enumeration_type_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `enumeration`
--

INSERT INTO `enumeration` (`id`, `enumeration_type_code`, `name`, `description`) VALUES
(1, 'DAY_IN_WEEK', 'Sunday', ''),
(2, 'DAY_IN_WEEK', 'Monday', ''),
(3, 'DAY_IN_WEEK', 'Tuesday', ''),
(4, 'DAY_IN_WEEK', 'Wednesday', ''),
(5, 'DAY_IN_WEEK', 'Thursday', ''),
(6, 'DAY_IN_WEEK', 'Friday', ''),
(7, 'DAY_IN_WEEK', 'Saturday', '');

-- --------------------------------------------------------

--
-- Table structure for table `enumeration_type`
--

CREATE TABLE IF NOT EXISTS `enumeration_type` (
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enumeration_type`
--

INSERT INTO `enumeration_type` (`code`, `name`, `description`) VALUES
('DAY_IN_WEEK', 'Day in week', '');

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE IF NOT EXISTS `equipment` (
`id` int(11) NOT NULL,
  `equipment_type_id` int(11) NOT NULL,
  `equipment_type_list_id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `client_user` varchar(20) NOT NULL,
  `client_pass` varchar(20) NOT NULL,
  `mac_address` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  `barcode` varchar(200) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1234 ;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`id`, `equipment_type_id`, `equipment_type_list_id`, `room_id`, `name`, `description`, `ip_address`, `client_user`, `client_pass`, `mac_address`, `status`, `barcode`) VALUES
(1, 4, 0, 1302, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(2, 5, 0, 1302, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(3, 6, 0, 1302, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(4, 8, 0, 1302, 'COM', 'COM', '', '', '', '', 'A', ''),
(5, 9, 0, 1302, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(6, 4, 0, 1303, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(7, 5, 0, 1303, 'DVD', 'DVD', '', '', '', '', 'A', '332'),
(8, 6, 0, 1303, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(9, 8, 0, 1303, 'COM', 'COM', '', '', '', '', 'A', ''),
(10, 9, 0, 1303, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(11, 4, 0, 1304, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(12, 5, 0, 1304, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(13, 6, 0, 1304, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(14, 8, 0, 1304, 'COM', 'COM', '', '', '', '', 'A', ''),
(15, 9, 0, 1304, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(16, 4, 0, 1305, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(17, 5, 0, 1305, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(18, 6, 0, 1305, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(19, 8, 0, 1305, 'COM', 'COM', '', '', '', '', 'A', ''),
(20, 9, 0, 1305, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(21, 4, 0, 1306, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(22, 5, 0, 1306, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(23, 6, 0, 1306, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(24, 8, 0, 1306, 'COM', 'COM', '', '', '', '', 'A', ''),
(25, 9, 0, 1306, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(26, 4, 0, 1307, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(27, 5, 0, 1307, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(28, 6, 0, 1307, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(29, 8, 0, 1307, 'COM', 'COM', '', '', '', '', 'A', ''),
(30, 9, 0, 1307, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(31, 4, 0, 1308, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(32, 5, 0, 1308, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(33, 6, 0, 1308, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(34, 8, 0, 1308, 'COM', 'COM', '', '', '', '', 'A', ''),
(35, 9, 0, 1308, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(36, 4, 0, 1314, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(37, 5, 0, 1314, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(38, 6, 0, 1314, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(39, 8, 0, 1314, 'COM', 'COM', '', '', '', '', 'A', ''),
(40, 9, 0, 1314, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(41, 4, 0, 1315, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(42, 5, 0, 1315, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(43, 6, 0, 1315, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(44, 8, 0, 1315, 'COM', 'COM', '', '', '', '', 'A', ''),
(45, 9, 0, 1315, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(46, 4, 0, 1402, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(47, 5, 0, 1402, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(48, 6, 0, 1402, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(49, 8, 0, 1402, 'COM', 'COM', '', '', '', '', 'A', ''),
(50, 9, 0, 1402, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(51, 4, 0, 1403, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(52, 5, 0, 1403, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(53, 6, 0, 1403, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(54, 8, 0, 1403, 'COM', 'COM', '', '', '', '', 'A', ''),
(55, 9, 0, 1403, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(56, 4, 0, 1404, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(57, 5, 0, 1404, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(58, 6, 0, 1404, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(59, 8, 0, 1404, 'COM', 'COM', '', '', '', '', 'A', ''),
(60, 9, 0, 1404, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(61, 4, 0, 1405, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(62, 5, 0, 1405, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(63, 6, 0, 1405, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(64, 8, 0, 1405, 'COM', 'COM', '', '', '', '', 'A', ''),
(65, 9, 0, 1405, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(66, 4, 0, 1406, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(67, 5, 0, 1406, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(68, 6, 0, 1406, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(69, 8, 0, 1406, 'COM', 'COM', '', '', '', '', 'A', ''),
(70, 9, 0, 1406, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(71, 4, 0, 1407, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(72, 5, 0, 1407, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(73, 6, 0, 1407, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(74, 8, 0, 1407, 'COM', 'COM', '', '', '', '', 'A', ''),
(75, 9, 0, 1407, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(76, 4, 0, 1408, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(77, 5, 0, 1408, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(78, 6, 0, 1408, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(79, 8, 0, 1408, 'COM', 'COM', '', '', '', '', 'A', ''),
(80, 9, 0, 1408, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(81, 4, 0, 1417, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(82, 5, 0, 1417, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(83, 6, 0, 1417, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(84, 8, 0, 1417, 'COM', 'COM', '', '', '', '', 'A', ''),
(85, 9, 0, 1417, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(86, 4, 0, 1418, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(87, 5, 0, 1418, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(88, 6, 0, 1418, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(89, 8, 0, 1418, 'COM', 'COM', '', '', '', '', 'A', ''),
(90, 9, 0, 1418, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(91, 4, 0, 1419, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(92, 5, 0, 1419, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(93, 6, 0, 1419, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(94, 8, 0, 1419, 'COM', 'COM', '', '', '', '', 'A', ''),
(95, 9, 0, 1419, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(96, 4, 0, 1502, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(97, 5, 0, 1502, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(98, 6, 0, 1502, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(99, 8, 0, 1502, 'COM', 'COM', '', '', '', '', 'A', ''),
(100, 9, 0, 1502, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(101, 4, 0, 1503, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(102, 5, 0, 1503, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(103, 6, 0, 1503, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(104, 8, 0, 1503, 'COM', 'COM', '', '', '', '', 'A', ''),
(105, 9, 0, 1503, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(106, 4, 0, 1504, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(107, 5, 0, 1504, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(108, 6, 0, 1504, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(109, 8, 0, 1504, 'COM', 'COM', '', '', '', '', 'A', ''),
(110, 9, 0, 1504, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(111, 4, 0, 2302, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(112, 5, 0, 2302, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(113, 6, 0, 2302, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(114, 8, 0, 2302, 'COM', 'COM', '', '', '', '', 'A', ''),
(115, 9, 0, 2302, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(116, 4, 0, 2303, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(117, 5, 0, 2303, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(118, 6, 0, 2303, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(119, 8, 0, 2303, 'COM', 'COM', '', '', '', '', 'A', ''),
(120, 9, 0, 2303, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(121, 4, 0, 2306, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(122, 5, 0, 2306, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(123, 6, 0, 2306, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(124, 8, 0, 2306, 'COM', 'COM', '', '', '', '', 'A', ''),
(125, 9, 0, 2306, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(126, 4, 0, 2307, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(127, 5, 0, 2307, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(128, 6, 0, 2307, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(129, 8, 0, 2307, 'COM', 'COM', '', '', '', '', 'A', ''),
(130, 9, 0, 2307, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(131, 4, 0, 2308, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(132, 5, 0, 2308, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(133, 6, 0, 2308, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(134, 8, 0, 2308, 'COM', 'COM', '', '', '', '', 'A', ''),
(135, 9, 0, 2308, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(136, 4, 0, 3302, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(137, 5, 0, 3302, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(138, 6, 0, 3302, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(139, 8, 0, 3302, 'COM', 'COM', '', '', '', '', 'A', ''),
(140, 9, 0, 3302, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(141, 4, 0, 3303, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(142, 5, 0, 3303, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(143, 6, 0, 3303, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(144, 8, 0, 3303, 'COM', 'COM', '', '', '', '', 'A', ''),
(145, 9, 0, 3303, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(146, 4, 0, 3304, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(147, 5, 0, 3304, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(148, 6, 0, 3304, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(149, 8, 0, 3304, 'COM', 'COM', '', '', '', '', 'A', ''),
(150, 9, 0, 3304, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(151, 4, 0, 3305, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(152, 5, 0, 3305, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(153, 6, 0, 3305, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(154, 8, 0, 3305, 'COM', 'COM', '', '', '', '', 'A', ''),
(155, 9, 0, 3305, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(156, 4, 0, 3306, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(157, 5, 0, 3306, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(158, 6, 0, 3306, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(159, 8, 0, 3306, 'COM', 'COM', '', '', '', '', 'A', ''),
(160, 9, 0, 3306, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(161, 4, 0, 3315, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(162, 5, 0, 3315, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(163, 6, 0, 3315, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(164, 8, 0, 3315, 'COM', 'COM', '', '', '', '', 'A', ''),
(165, 9, 0, 3315, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(166, 4, 0, 3316, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(167, 5, 0, 3316, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(168, 6, 0, 3316, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(169, 8, 0, 3316, 'COM', 'COM', '', '', '', '', 'A', ''),
(170, 9, 0, 3316, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(171, 4, 0, 3317, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(172, 5, 0, 3317, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(173, 6, 0, 3317, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(174, 8, 0, 3317, 'COM', 'COM', '', '', '', '', 'A', ''),
(175, 9, 0, 3317, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(176, 4, 0, 3407, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(177, 5, 0, 3407, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(178, 6, 0, 3407, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(179, 8, 0, 3407, 'COM', 'COM', '10.27.3.200', 'AV_MUIC', '1234', '5C:F9:DD:DD:29:1C', 'A', ''),
(180, 9, 0, 3407, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(181, 4, 0, 3408, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(182, 5, 0, 3408, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(183, 6, 0, 3408, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(184, 8, 0, 3408, 'COM', 'COM', '10.27.3.146', 'AV_MUIC', '1234', '5C:F9:DD:DD:31:25', 'A', ''),
(185, 9, 0, 3408, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(186, 4, 0, 3409, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(187, 5, 0, 3409, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(188, 6, 0, 3409, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(189, 8, 0, 3409, 'COM', 'COM', '10.27.7.82', 'AV_MUIC', '1234', '5C:F9:DD:DD:1C:94', 'A', ''),
(190, 9, 0, 3409, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(191, 4, 0, 3410, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(192, 5, 0, 3410, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(193, 6, 0, 3410, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(194, 8, 0, 3410, 'COM', 'COM', '', '', '', '', 'A', ''),
(195, 9, 0, 3410, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(196, 4, 0, 3411, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(197, 5, 0, 3411, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(198, 6, 0, 3411, 'Visualizer', 'Visualizer', '10.27.3.212', '', '', '', 'A', ''),
(199, 8, 0, 3411, 'COM', 'COM', '10.27.3.149', '', '', '', 'A', ''),
(200, 9, 0, 3411, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(201, 4, 0, 3412, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(202, 5, 0, 3412, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(203, 6, 0, 3412, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(204, 8, 0, 3412, 'COM', 'COM', '10.27.1.143', 'AV_MUIC', 'av_muic', '3C-D9-2B-62-35-AD', 'A', ''),
(205, 9, 0, 3412, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(206, 4, 0, 3420, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(207, 5, 0, 3420, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(208, 6, 0, 3420, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(209, 8, 0, 3420, 'COM', 'COM', '', '', '', '', 'A', ''),
(210, 9, 0, 3420, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(211, 4, 0, 3421, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(212, 5, 0, 3421, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(213, 6, 0, 3421, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(214, 8, 0, 3421, 'COM', 'COM', '', '', '', '', 'A', ''),
(215, 9, 0, 3421, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(216, 4, 0, 3422, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(217, 5, 0, 3422, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(218, 6, 0, 3422, 'Visualizer', 'Visualizer', '10.27.3.210', '', '', '', 'A', ''),
(219, 8, 0, 3422, 'COM', 'COM', '10.27.7.90', 'AV_MUIC', '1234', '5C:F9:DD:DD:32:D4', 'A', ''),
(220, 9, 0, 3422, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(221, 4, 0, 5207, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(222, 5, 0, 5207, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(223, 6, 0, 5207, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(224, 8, 0, 5207, 'COM', 'COM', '', '', '', '', 'A', ''),
(225, 9, 0, 5207, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(226, 4, 0, 5208, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(227, 5, 0, 5208, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(228, 6, 0, 5208, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(229, 8, 0, 5208, 'COM', 'COM', '', '', '', '', 'A', ''),
(230, 9, 0, 5208, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(231, 4, 0, 5209, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(232, 5, 0, 5209, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(233, 6, 0, 5209, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(234, 8, 0, 5209, 'COM', 'COM', '', '', '', '', 'A', ''),
(235, 9, 0, 5209, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(236, 4, 0, 5210, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(237, 5, 0, 5210, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(238, 6, 0, 5210, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(239, 8, 0, 5210, 'COM', 'COM', '', '', '', '', 'A', ''),
(240, 9, 0, 5210, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(241, 4, 0, 5211, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(242, 5, 0, 5211, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(243, 6, 0, 5211, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(244, 8, 0, 5211, 'COM', 'COM', '', '', '', '', 'A', ''),
(245, 9, 0, 5211, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(246, 4, 0, 5212, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(247, 5, 0, 5212, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(248, 6, 0, 5212, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(249, 8, 0, 5212, 'COM', 'COM', '', '', '', '', 'A', ''),
(250, 9, 0, 5212, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(251, 4, 0, 5301, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(252, 5, 0, 5301, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(253, 6, 0, 5301, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(254, 8, 0, 5301, 'COM', 'COM', '', '', '', '', 'A', ''),
(255, 9, 0, 5301, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(256, 4, 0, 5307, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(257, 5, 0, 5307, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(258, 6, 0, 5307, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(259, 8, 0, 5307, 'COM', 'COM', '', '', '', '', 'A', ''),
(260, 9, 0, 5307, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(261, 4, 0, 5308, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(262, 5, 0, 5308, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(263, 6, 0, 5308, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(264, 8, 0, 5308, 'COM', 'COM', '', '', '', '', 'A', ''),
(265, 9, 0, 5308, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(266, 4, 0, 5309, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(267, 5, 0, 5309, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(268, 6, 0, 5309, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(269, 8, 0, 5309, 'COM', 'COM', '', '', '', '', 'A', ''),
(270, 9, 0, 5309, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(271, 4, 0, 5310, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(272, 5, 0, 5310, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(273, 6, 0, 5310, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(274, 8, 0, 5310, 'COM', 'COM', '', '', '', '', 'A', ''),
(275, 9, 0, 5310, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(276, 4, 0, 5311, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(277, 5, 0, 5311, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(278, 6, 0, 5311, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(279, 8, 0, 5311, 'COM', 'COM', '', '', '', '', 'A', ''),
(280, 9, 0, 5311, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(281, 4, 0, 5312, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(282, 5, 0, 5312, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(283, 6, 0, 5312, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(284, 8, 0, 5312, 'COM', 'COM', '', '', '', '', 'A', ''),
(285, 9, 0, 5312, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(286, 4, 0, 5313, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(287, 5, 0, 5313, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(288, 6, 0, 5313, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(289, 8, 0, 5313, 'COM', 'COM', '', '', '', '', 'A', ''),
(290, 9, 0, 5313, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(291, 8, 0, 1309, 'COM', 'COM', '', '', '', '', 'A', ''),
(292, 9, 0, 1309, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(293, 9, 0, 1312, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(294, 9, 0, 1506, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(295, 8, 0, 3307, 'COM', 'COM', '', '', '', '', 'A', ''),
(296, 9, 0, 3307, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(297, 6, 0, 3414, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(298, 8, 0, 3414, 'COM', 'COM', '', '', '', '', 'A', ''),
(299, 9, 0, 3414, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(300, 4, 0, 3415, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(301, 5, 0, 3415, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(302, 6, 0, 3415, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(303, 8, 0, 3415, 'COM', 'COM', '', '', '', '', 'A', ''),
(304, 9, 0, 3415, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(305, 9, 0, 3508, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(306, 8, 0, 1108, 'COM', 'COM', '', '', '', '', 'A', ''),
(307, 9, 0, 1108, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(308, 9, 0, 1210, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(309, 5, 0, 1210, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(310, 4, 0, 1210, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(311, 4, 0, 1210, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(312, 4, 0, 1210, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(313, 6, 0, 1214, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(314, 8, 0, 1214, 'COM', 'COM', '', '', '', '', 'A', ''),
(315, 9, 0, 1214, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(316, 9, 0, 1318, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(317, 9, 0, 1318, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(318, 9, 0, 1318, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(319, 5, 0, 1318, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(320, 6, 0, 1318, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(321, 4, 0, 1318, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(322, 4, 0, 1318, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(323, 4, 0, 1318, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(324, 4, 0, 1318, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(325, 4, 0, 1318, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(326, 4, 0, 1318, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(327, 3, 0, 1318, 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A', ''),
(328, 3, 0, 1318, 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A', ''),
(329, 3, 0, 1318, 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A', ''),
(330, 8, 0, 2207, 'COM', 'COM', '', '', '', '', 'A', ''),
(331, 9, 0, 2207, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(332, 6, 0, 2207, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(333, 9, 0, 7, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(334, 9, 0, 7, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(335, 9, 0, 7, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(336, 9, 0, 7, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(337, 9, 0, 7, 'LCD', 'LCD', '', '', '', '', 'A', ''),
(338, 8, 0, 7, 'COM', 'COM', '', '', '', '', 'A', ''),
(339, 8, 0, 7, 'COM', 'COM', '', '', '', '', 'A', ''),
(340, 8, 0, 7, 'COM', 'COM', '', '', '', '', 'A', ''),
(341, 7, 0, 7, 'COM(NOTEBOOK)', 'COM(NOTEBOOK)', '', '', '', '', 'A', ''),
(342, 7, 0, 7, 'COM(NOTEBOOK)', 'COM(NOTEBOOK)', '', '', '', '', 'A', ''),
(343, 7, 0, 7, 'COM(NOTEBOOK)', 'COM(NOTEBOOK)', '', '', '', '', 'A', ''),
(344, 7, 0, 7, 'COM(NOTEBOOK)', 'COM(NOTEBOOK)', '', '', '', '', 'A', ''),
(345, 7, 0, 7, 'COM(NOTEBOOK)', 'COM(NOTEBOOK)', '', '', '', '', 'A', ''),
(346, 6, 0, 7, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(347, 6, 0, 7, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(348, 6, 0, 7, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(349, 6, 0, 7, 'Visualizer', 'Visualizer', '', '', '', '', 'A', ''),
(350, 5, 0, 7, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(351, 5, 0, 7, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(352, 5, 0, 7, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(353, 5, 0, 7, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(354, 5, 0, 7, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(355, 5, 0, 7, 'DVD', 'DVD', '', '', '', '', 'A', '333'),
(356, 5, 0, 7, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(357, 5, 0, 7, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(358, 5, 0, 7, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(359, 5, 0, 7, 'DVD', 'DVD', '', '', '', '', 'A', ''),
(360, 3, 0, 7, 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A', ''),
(361, 3, 0, 7, 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A', '12345'),
(362, 3, 0, 7, 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A', ''),
(363, 3, 0, 7, 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A', ''),
(364, 1, 0, 7, 'Portable Sound System', 'Portable Sound System', '', '', '', '', 'A', '1357'),
(365, 1, 0, 7, 'Portable Sound System', 'Portable Sound System', '', '', '', '', 'A', ''),
(366, 10, 0, 7, 'LED TV', 'LED TV', '', '', '', '', 'A', ''),
(367, 10, 0, 7, 'LED TV', 'LED TV', '', '', '', '', 'A', ''),
(368, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(369, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(370, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(371, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(372, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(373, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(374, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(375, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(376, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(377, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(378, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(379, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(380, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(381, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(382, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(383, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(384, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(385, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(386, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(387, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(388, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(389, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(390, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(391, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(392, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(393, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(394, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(395, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(396, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(397, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(398, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(399, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(400, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(401, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(402, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(403, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(404, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(405, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(406, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(407, 4, 0, 7, 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A', ''),
(408, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', '2245'),
(409, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(410, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(411, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(412, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(413, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(414, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(415, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(416, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(417, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(418, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(419, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(420, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(421, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(422, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(423, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(424, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(425, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(426, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(427, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(428, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(429, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(430, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(431, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(432, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(433, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(434, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(435, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(436, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(437, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(438, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(439, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(440, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(441, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(442, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(443, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(444, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(445, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(446, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', '444'),
(447, 2, 0, 7, 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A', ''),
(861, 11, 522, 2, 'Canon EOS 5D Mark III + 24-105', 'EF24-105F/4L IS USM+Charger+LP-E6', '', '', '', '', 'A', '404000008820'),
(862, 11, 522, 2, 'Canon EOS 5D Mark III + 24-105', 'EF24-105F/4L IS USM+Charger+LP-E7', '', '', '', '', 'A', '404000007619'),
(863, 11, 522, 2, 'Canon EOS 5D Mark III + 24-105', 'EF24-105F/4L IS USM+Charger+LP-E8', '', '', '', '', 'A', '404000007620'),
(864, 11, 523, NULL, 'Canon  5D Mark III(BODY) High Definition', '', '', '', '', '', 'A', '404000008529'),
(865, 11, 523, NULL, 'Canon  5D Mark III(BODY) High Definition', '', '', '', '', '', 'A', '404000008530'),
(866, 11, 524, NULL, 'Canon  5D Mark III(BODY)', '', '', '', '', '', 'A', '404000015086'),
(867, 11, 524, NULL, 'Canon  5D Mark III(BODY)', '', '', '', '', '', 'A', '404000015087'),
(868, 11, 524, NULL, 'Canon  5D Mark III(BODY)', '', '', '', '', '', 'A', '404000015088'),
(869, 11, 524, 2, 'Canon  5D Mark III(BODY)', '', '', '', '', '', 'A', '404000015089'),
(870, 11, 524, 2, 'Canon  5D Mark III(BODY)', '', '', '', '', '', 'A', '404000015090'),
(871, 11, 525, 2, 'Canon EOS 550D', '', '', '', '', '', 'A', '404000004699'),
(872, 11, 525, 2, 'Canon EOS 550D', '', '', '', '', '', 'A', '404000004700'),
(873, 12, 526, 2, 'Sony VCL-HGA07 0.7 Converter Lens-30/37mm.', '', '', '', '', '', 'A', '404000004163'),
(874, 12, 526, 2, 'Sony VCL-HGA07 0.7 Converter Lens-30/37mm.', '', '', '', '', '', 'A', '404000004164'),
(875, 12, 526, 2, 'Sony VCL-HGA07 0.7 Converter Lens-30/37mm.', '', '', '', '', '', 'A', '404000004165'),
(876, 12, 526, 2, 'Sony VCL-HGA07 0.7 Converter Lens-30/37mm.', '', '', '', '', '', 'A', '404000004166'),
(877, 12, 527, 2, 'Sony VCL-HG1737C 1.7xTele Conversion Lens', '', '', '', '', '', 'A', '404000004167'),
(878, 12, 527, 2, 'Sony VCL-HG1737C 1.7xTele Conversion Lens', '', '', '', '', '', 'A', '404000004168'),
(879, 12, 528, 2, 'Lens Canon EF 70-200mm.F2.8L IS2USM', '', '', '', '', '', 'A', '404000004705'),
(880, 12, 528, 2, 'Lens Canon EF 70-200mm.F2.8L IS2USM', '', '', '', '', '', 'A', '404000004706'),
(881, 12, 528, 2, 'Lens Canon EF 70-200mm.F2.8L IS2USM', '', '', '', '', '', 'A', '404000004708'),
(882, 12, 528, 2, 'Lens Canon EF 70-200mm.F2.8L IS2USM', '', '', '', '', '', 'A', '404000009125'),
(883, 12, 529, 2, 'Lens Canon EF 24-70mm.F2.8L IS2USM', '', '', '', '', '', 'A', '404000004709'),
(884, 12, 529, 2, 'Lens Canon EF 24-70mm.F2.8L IS2USM', '', '', '', '', '', 'A', '404000004710'),
(885, 12, 529, 2, 'Lens Canon EF 24-70mm.F2.8L IS2USM', '', '', '', '', '', 'A', '404000004712'),
(886, 12, 529, 2, 'Lens Canon EF 24-70mm.F2.8L IS2USM', '', '', '', '', '', 'A', '404000009124'),
(887, 12, 530, 2, 'Lens TOKINA 11-16mm F2.8IF For CanonMount', '', '', '', '', '', 'A', '404000009117'),
(888, 12, 530, 2, 'Lens TOKINA 11-16mm F2.8IF For CanonMount', '', '', '', '', '', 'A', '404000004713'),
(889, 12, 530, 2, 'Lens TOKINA 11-16mm F2.8IF For CanonMount', '', '', '', '', '', 'A', '404000004714'),
(890, 12, 531, 2, 'Lens Canon EF 50mm.F1.8', '', '', '', '', '', 'A', '904000003741'),
(891, 12, 531, 2, 'Lens Canon EF 50mm.F1.8', '', '', '', '', '', 'A', '904000003742'),
(892, 12, 532, 2, 'LUMIX G VARIO 45-200 mm.F/4.0-F5.6', '', '', '', '', '', 'A', '404000009114'),
(893, 12, 533, 2, 'LUMIX G VARIO 14-45 mm.F/3.5-F5.6 ASPH.', '', '', '', '', '', 'A', '404000009115'),
(894, 12, 534, 2, 'Lens 35 mm.F1.4 DG HSM', '', '', '', '', '', 'A', '404000009116'),
(895, 12, 535, 2, 'Lens EF 50 mm. F1.4 USM', '', '', '', '', '', 'A', '404000009118'),
(896, 12, 535, 2, 'Lens EF 50 mm. F1.4 USM', '', '', '', '', '', 'A', '404000009119'),
(897, 12, 536, 2, 'Lens EF 85 mm. F1.2LII USM', '', '', '', '', '', 'A', '404000009120'),
(898, 12, 536, 2, 'Lens EF 85 mm. F1.2LII USM', '', '', '', '', '', 'A', '404000009121'),
(899, 12, 537, 2, 'Lens EF 100 mm. F2.8L Macro IS USM', '', '', '', '', '', 'A', '404000009122'),
(900, 12, 537, 2, 'Lens EF 100 mm. F2.8L Macro IS USM', '', '', '', '', '', 'A', '404000009123'),
(901, 13, 538, 2, 'Sony PD-170P(3CCD Camera DVCAM) + Remote', '', '', '', '', '', 'A', '404000004041'),
(902, 13, 539, 2, 'Sony HVR-Z7P HDV + optional item', '', '', '', '', '', 'A', '404000004047'),
(903, 13, 539, 2, 'Sony HVR-Z7P HDV + optional item', '', '', '', '', '', 'A', '404000004048'),
(904, 13, 539, 2, 'Sony HVR-Z7P HDV + optional item', '', '', '', '', '', 'A', '404000004049'),
(905, 13, 539, 2, 'Sony HVR-Z7P HDV + optional item', '', '', '', '', '', 'A', '404000004050'),
(906, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004051'),
(907, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004052'),
(908, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004053'),
(909, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004054'),
(910, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004055'),
(911, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004056'),
(912, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004057'),
(913, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004058'),
(914, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004059'),
(915, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004060'),
(916, 13, 540, 2, 'Sony HVR-HD1000P Camcorder', '', '', '', '', '', 'A', '404000004061'),
(917, 13, 541, 2, 'กล้องบันทึกภาพเคลื่อนไหวพร้อมอุปกรณ์', '', '', '', '', '', 'A', '404000009113'),
(918, 14, 542, 2, 'Shoulder Tripod', '', '', '', '', '', 'A', '404000007246'),
(919, 14, 542, 2, 'Shoulder Tripod', '', '', '', '', '', 'A', '404000007247'),
(920, 14, 543, 2, 'Car mount tripod', '', '', '', '', '', 'A', '404000007248'),
(921, 14, 544, 2, 'Peter Shoulder Brace (BCS-050)', '', '', '', '', '', 'A', '404000004042'),
(922, 14, 544, 2, 'Peter Shoulder Brace (BCS-050)', '', '', '', '', '', 'A', '404000004043'),
(923, 14, 545, 2, 'Peter Lisand Studio Pedestal+100mm.Head', '', '', '', '', '', 'A', '404000004044'),
(924, 14, 545, 2, 'Peter Lisand Studio Pedestal+100mm.Head', '', '', '', '', '', 'A', '404000004045'),
(925, 14, 545, 2, 'Peter Lisand Studio Pedestal+100mm.Head', '', '', '', '', '', 'A', '404000004046'),
(926, 14, 546, 2, 'Manfrotto MVK502AM', '', '', '', '', '', 'A', '404000009092'),
(927, 14, 546, 2, 'Manfrotto MVK502AM', '', '', '', '', '', 'A', '404000009093'),
(928, 14, 546, 2, 'Manfrotto MVK502AM', '', '', '', '', '', 'A', '404000009094'),
(929, 14, 546, 2, 'Manfrotto MVK502AM', '', '', '', '', '', 'A', '404000009095'),
(930, 14, 547, 2, 'Sachtler FSB 4 TRIPOD', '', '', '', '', '', 'A', '404000005112'),
(931, 14, 547, 2, 'Sachtler FSB 4 TRIPOD', '', '', '', '', '', 'A', '404000005113'),
(932, 14, 547, 2, 'Sachtler FSB 4 TRIPOD', '', '', '', '', '', 'A', '404000005114'),
(933, 14, 547, 2, 'Sachtler FSB 4 TRIPOD', '', '', '', '', '', 'A', '404000005115'),
(934, 14, 547, 2, 'Sachtler FSB 4 TRIPOD', '', '', '', '', '', 'A', '404000005116'),
(935, 14, 547, 2, 'Sachtler FSB 4 TRIPOD', '', '', '', '', '', 'A', '404000005117'),
(936, 14, 547, 2, 'Sachtler FSB 4 TRIPOD', '', '', '', '', '', 'A', '404000005118'),
(937, 14, 547, 2, 'Sachtler FSB 4 TRIPOD', '', '', '', '', '', 'A', '404000005119'),
(938, 14, 547, 2, 'Sachtler FSB 4 TRIPOD', '', '', '', '', '', 'A', '404000005120'),
(939, 14, 547, 2, 'Sachtler FSB 4 TRIPOD', '', '', '', '', '', 'A', '404000005121'),
(940, 14, 548, 2, 'Jib Crane 14 feet', '', '', '', '', '', 'A', '404000007242'),
(941, 14, 548, 2, 'Jib Crane 14 feet', '', '', '', '', '', 'A', '404000007243'),
(942, 14, 549, 2, 'Jib Crane 9 feet', '', '', '', '', '', 'A', '404000007244'),
(943, 14, 549, 2, 'Jib Crane 9 feet', '', '', '', '', '', 'A', '404000007245'),
(944, 15, 550, 2, 'Avanger Manfrotto A2033L (c-stand)', '', '', '', '', '', 'A', '404000004547'),
(945, 15, 550, 2, 'Avanger Manfrotto A2033L (c-stand)', '', '', '', '', '', 'A', '404000004548'),
(946, 15, 550, 2, 'Avanger Manfrotto A2033L (c-stand)', '', '', '', '', '', 'A', '404000004549'),
(947, 15, 550, 2, 'Avanger Manfrotto A2033L (c-stand)', '', '', '', '', '', 'A', '404000004550'),
(948, 15, 550, 2, 'Avanger Manfrotto A2033L (c-stand)', '', '', '', '', '', 'A', '404000004551'),
(949, 15, 550, 2, 'Avanger Manfrotto A2033L (c-stand)', '', '', '', '', '', 'A', '404000004552'),
(950, 15, 550, 2, 'Avanger Manfrotto A2033L (c-stand)', '', '', '', '', '', 'A', '404000004553'),
(951, 15, 550, 2, 'Avanger Manfrotto A2033L (c-stand)', '', '', '', '', '', 'A', '404000004554'),
(952, 15, 550, 2, 'Avanger Manfrotto A2033L (c-stand)', '', '', '', '', '', 'A', '404000004555'),
(953, 15, 550, 2, 'Avanger Manfrotto A2033L (c-stand)', '', '', '', '', '', 'A', '404000004556'),
(954, 15, 551, 2, 'Avanger Manfrotto D520  (arm)', '', '', '', '', '', 'A', '904000003532'),
(955, 15, 551, 2, 'Avanger Manfrotto D520  (arm)', '', '', '', '', '', 'A', '904000003533'),
(956, 15, 551, 2, 'Avanger Manfrotto D520  (arm)', '', '', '', '', '', 'A', '904000003534'),
(957, 15, 551, 2, 'Avanger Manfrotto D520  (arm)', '', '', '', '', '', 'A', '904000003535'),
(958, 15, 551, 2, 'Avanger Manfrotto D520  (arm)', '', '', '', '', '', 'A', '904000003536'),
(959, 15, 551, 2, 'Avanger Manfrotto D520  (arm)', '', '', '', '', '', 'A', '904000003537'),
(960, 15, 551, 2, 'Avanger Manfrotto D520  (arm)', '', '', '', '', '', 'A', '904000003538'),
(961, 15, 551, 2, 'Avanger Manfrotto D520  (arm)', '', '', '', '', '', 'A', '904000003539'),
(962, 15, 551, 2, 'Avanger Manfrotto D520  (arm)', '', '', '', '', '', 'A', '904000003540'),
(963, 15, 551, 2, 'Avanger Manfrotto D520  (arm)', '', '', '', '', '', 'A', '904000003541'),
(964, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008419'),
(965, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008420'),
(966, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008421'),
(967, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008422'),
(968, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008423'),
(969, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008424'),
(970, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008425'),
(971, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008426'),
(972, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008427'),
(973, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008428'),
(974, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008429'),
(975, 15, 552, 2, 'CN-900HS LED LIGHT + Light Stand', '', '', '', '', '', 'A', '404000008430'),
(976, 15, 553, 2, 'Dedo light 150 W + Light stand', '', '', '', '', '', 'A', '404000008431'),
(977, 15, 553, 2, 'Dedo light 150 W + Light stand', '', '', '', '', '', 'A', '404000008432'),
(978, 15, 553, 2, 'Dedo light 150 W + Light stand', '', '', '', '', '', 'A', '404000008433'),
(979, 15, 554, 2, 'ARRi 300W PLUS PRESNEL + Light stand', '', '', '', '', '', 'A', '404000008434'),
(980, 15, 554, 2, 'ARRi 300W PLUS PRESNEL + Light stand', '', '', '', '', '', 'A', '404000008435'),
(981, 15, 554, 2, 'ARRi 300W PLUS PRESNEL + Light stand', '', '', '', '', '', 'A', '404000008436'),
(982, 15, 554, 2, 'ARRi 300W PLUS PRESNEL + Light stand', '', '', '', '', '', 'A', '404000008437'),
(983, 15, 554, 2, 'ARRi 300W PLUS PRESNEL + Light stand', '', '', '', '', '', 'A', '404000008438'),
(984, 15, 554, 2, 'ARRi 300W PLUS PRESNEL + Light stand', '', '', '', '', '', 'A', '404000008439'),
(985, 15, 554, 2, 'ARRi 300W PLUS PRESNEL + Light stand', '', '', '', '', '', 'A', '404000008440'),
(986, 15, 554, 2, 'ARRi 300W PLUS PRESNEL + Light stand', '', '', '', '', '', 'A', '404000008441'),
(987, 15, 554, 2, 'ARRi 300W PLUS PRESNEL + Light stand', '', '', '', '', '', 'A', '404000008442'),
(988, 16, 555, 2, 'KINO FLO GRAFFERKIT', '', '', '', '', '', 'A', '404000004726'),
(989, 16, 555, 2, 'KINO FLO GRAFFERKIT', '', '', '', '', '', 'A', '404000004727'),
(990, 16, 556, 2, 'KINO FLO Interview KIT', '', '', '', '', '', 'A', '404000004728'),
(991, 16, 557, 2, '800W ARRI set', '', '', '', '', '', 'A', '404000005129'),
(992, 16, 557, 2, '800W ARRI set', '', '', '', '', '', 'A', '404000005130'),
(993, 16, 557, 2, '800W ARRI set', '', '', '', '', '', 'A', '404000005131'),
(994, 16, 557, 2, '800W ARRI set', '', '', '', '', '', 'A', '404000005132'),
(995, 16, 557, 2, '800W ARRI set', '', '', '', '', '', 'A', '404000005133'),
(996, 16, 557, 2, '800W ARRI set', '', '', '', '', '', 'A', '404000005134'),
(997, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004092'),
(998, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004093'),
(999, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004094'),
(1000, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004095'),
(1001, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004096'),
(1002, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004097'),
(1003, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004098'),
(1004, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004099'),
(1005, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004100'),
(1006, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004101'),
(1007, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004102'),
(1008, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004103'),
(1009, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004104'),
(1010, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004105'),
(1011, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004106'),
(1012, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004107'),
(1013, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004108'),
(1014, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004109'),
(1015, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004110'),
(1016, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004111'),
(1017, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004112'),
(1018, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004113'),
(1019, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004114'),
(1020, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004115'),
(1021, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004116'),
(1022, 16, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004117'),
(1023, 17, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004118'),
(1024, 18, 558, 2, 'Nikon FM 10 Film Camera ', '', '', '', '', '', 'A', '404000004119'),
(1025, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004557'),
(1026, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004558'),
(1027, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004559'),
(1028, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004560'),
(1029, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004561'),
(1030, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004562'),
(1031, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004563'),
(1032, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004564'),
(1033, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004565'),
(1034, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004566'),
(1035, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004567'),
(1036, 18, 559, 2, 'Flag 30x36 inch', '', '', '', '', '', 'A', '404000004568'),
(1037, 18, 560, 2, 'Meat Axe Flag 24x48 inch', '', '', '', '', '', 'A', '404000004569'),
(1038, 18, 560, 2, 'Meat Axe Flag 24x48 inch', '', '', '', '', '', 'A', '404000004570'),
(1039, 18, 560, 2, 'Meat Axe Flag 24x48 inch', '', '', '', '', '', 'A', '404000004571'),
(1040, 18, 560, 2, 'Meat Axe Flag 24x48 inch', '', '', '', '', '', 'A', '404000004572'),
(1041, 18, 561, 2, 'High Temperature Metal Flag 24x36 inch', '', '', '', '', '', 'A', '404000004573'),
(1042, 18, 561, 2, 'High Temperature Metal Flag 24x36 inch', '', '', '', '', '', 'A', '404000004574'),
(1043, 18, 561, 2, 'High Temperature Metal Flag 24x36 inch', '', '', '', '', '', 'A', '404000004575'),
(1044, 18, 561, 2, 'High Temperature Metal Flag 24x36 inch', '', '', '', '', '', 'A', '404000004576'),
(1045, 18, 561, 2, 'High Temperature Metal Flag 24x36 inch', '', '', '', '', '', 'A', '404000004577'),
(1046, 18, 561, 2, 'High Temperature Metal Flag 24x36 inch', '', '', '', '', '', 'A', '404000004578'),
(1047, 18, 561, 2, 'High Temperature Metal Flag 24x36 inch', '', '', '', '', '', 'A', '404000004579'),
(1048, 18, 561, 2, 'High Temperature Metal Flag 24x36 inch', '', '', '', '', '', 'A', '404000004580'),
(1049, 18, 562, 2, 'Flag 24x30 inch', '', '', '', '', '', 'A', '904000003488'),
(1050, 18, 562, 2, 'Flag 24x30 inch', '', '', '', '', '', 'A', '904000003489'),
(1051, 18, 562, 2, 'Flag 24x30 inch', '', '', '', '', '', 'A', '904000003490'),
(1052, 18, 562, 2, 'Flag 24x30 inch', '', '', '', '', '', 'A', '904000003491'),
(1053, 18, 562, 2, 'Flag 24x30 inch', '', '', '', '', '', 'A', '904000003492'),
(1054, 18, 562, 2, 'Flag 24x30 inch', '', '', '', '', '', 'A', '904000003493'),
(1055, 18, 562, 2, 'Flag 24x30 inch', '', '', '', '', '', 'A', '904000003494'),
(1056, 18, 562, 2, 'Flag 24x30 inch', '', '', '', '', '', 'A', '904000003495'),
(1057, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003496'),
(1058, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003497'),
(1059, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003498'),
(1060, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003499'),
(1061, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003500'),
(1062, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003501'),
(1063, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003502'),
(1064, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003503'),
(1065, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003504'),
(1066, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003505'),
(1067, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003506'),
(1068, 18, 563, 2, 'Flag 24x36 inch', '', '', '', '', '', 'A', '904000003507'),
(1069, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003508'),
(1070, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003509'),
(1071, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003510'),
(1072, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003511'),
(1073, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003512'),
(1074, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003513'),
(1075, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003514'),
(1076, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003515'),
(1077, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003516'),
(1078, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003517'),
(1079, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003518'),
(1080, 18, 564, 2, 'Flag 18x24 inch', '', '', '', '', '', 'A', '904000003519');
INSERT INTO `equipment` (`id`, `equipment_type_id`, `equipment_type_list_id`, `room_id`, `name`, `description`, `ip_address`, `client_user`, `client_pass`, `mac_address`, `status`, `barcode`) VALUES
(1081, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003520'),
(1082, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003521'),
(1083, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003522'),
(1084, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003523'),
(1085, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003524'),
(1086, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003525'),
(1087, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003526'),
(1088, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003527'),
(1089, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003528'),
(1090, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003529'),
(1091, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003530'),
(1092, 18, 565, 2, 'Flag 12x18 inch', '', '', '', '', '', 'A', '904000003531'),
(1093, 18, 566, 2, 'Flag Roadrags 18x24 inch', '', '', '', '', '', 'A', '904000003542'),
(1094, 18, 566, 2, 'Flag Roadrags 18x24 inch', '', '', '', '', '', 'A', '904000003543'),
(1095, 18, 566, 2, 'Flag Roadrags 18x24 inch', '', '', '', '', '', 'A', '904000003544'),
(1096, 18, 566, 2, 'Flag Roadrags 18x24 inch', '', '', '', '', '', 'A', '904000003545'),
(1097, 19, 567, 2, '12 Channels Audio Mixing Console', 'Yamaha MG12CX-USB', '', '', '', '', 'A', '403000003650'),
(1098, 19, 568, 2, '8x8 Analog in/out USB/Firewire Audio interface', ' MOTU Audio Express', '', '', '', '', 'A', '403000003651'),
(1099, 19, 569, 2, '4 Channels Headphone Amplifier', ' Samson  C-Que 8', '', '', '', '', 'A', '403000003652'),
(1100, 19, 570, 2, 'Monitor Speaker', 'ยี่ห้อ ESI รุ่น nEar05', '', '', '', '', 'A', '404000007029'),
(1101, 19, 571, 2, 'Foley Equpment', '', '', '', '', '', 'A', '403000003653'),
(1102, 19, 572, 2, 'Guitar Godin  Redline3', '', '', '', '', '', 'A', '414000000882'),
(1103, 19, 573, 2, 'Amplifier Fender Frontman 212R', '', '', '', '', '', 'A', '414000000883'),
(1104, 19, 574, 2, 'E-Drun Roland', 'TD-4K2', '', '', '', '', 'A', '414000000884'),
(1105, 19, 575, 2, 'Amplifier for Drum Roland', 'PM-10', '', '', '', '', 'A', '414000000885'),
(1106, 19, 576, 2, 'Amplifier Roland  KC-150', '', '', '', '', '', 'A', '414000000906'),
(1107, 19, 577, 2, 'Piano M-Audio', '', '', '', '', '', 'A', '414000000907'),
(1108, 19, 578, 2, 'Studio Vocal Condenser Mic incl Mic Stand', 'Rode  NT1A', '', '', '', '', 'A', '404000007030'),
(1109, 19, 579, 2, 'Studio Closed Dynamic Headphone', 'Audio  AT-M30', '', '', '', '', 'A', '904000004191'),
(1110, 19, 579, 2, 'Studio Closed Dynamic Headphone', 'Audio  AT-M30', '', '', '', '', 'A', '904000004192'),
(1111, 19, 579, 2, 'Studio Closed Dynamic Headphone', 'Audio  AT-M30', '', '', '', '', 'A', '904000004193'),
(1112, 19, 579, 2, 'Studio Closed Dynamic Headphone', 'Audio  AT-M30', '', '', '', '', 'A', '904000004194'),
(1113, 19, 580, 2, 'ZOOM H4N Digital Recorder', '', '', '', '', '', 'A', '403000002463'),
(1114, 19, 580, 2, 'ZOOM H4N Digital Recorder', '', '', '', '', '', 'A', '403000002480'),
(1115, 19, 581, 2, 'Headphone Extreme Isolation EX29 ', '', '', '', '', '', 'A', '404000004523'),
(1116, 19, 582, 2, 'Condenser Microphone  Avant Electronics', '', '', '', '', '', 'A', '404000004529'),
(1117, 19, 583, 2, 'Sennheiser Long-gun', '', '', '', '', '', 'A', '404000004583'),
(1118, 19, 584, 2, 'Shure Microphone', '', '', '', '', '', 'A', '404000007412'),
(1119, 19, 584, 2, 'Shure Microphone', '', '', '', '', '', 'A', '404000007438'),
(1120, 19, 585, 2, 'Boom Microphone with case', '', '', '', '', '', 'A', '404000009090'),
(1121, 19, 585, 2, 'Boom Microphone with case', '', '', '', '', '', 'A', '404000005111'),
(1122, 19, 586, 2, 'Audio Interface MOTU Ultralite Hybrid', '', '', '', '', '', 'A', '403000002365'),
(1123, 19, 587, 2, 'YAMAHA RX-V463', '', '', '', '', '', 'A', '403000002399'),
(1124, 19, 588, 2, 'Subwoofer YAMAHA  YST-SW315', '', '', '', '', '', 'A', '404000004602'),
(1125, 19, 589, 2, 'Monitor Speaker QUEST  MS801', '', '', '', '', '', 'A', '404000004603'),
(1126, 19, 589, 2, 'Monitor Speaker QUEST  MS801', '', '', '', '', '', 'A', '404000004604'),
(1127, 19, 589, 2, 'Monitor Speaker QUEST  MS801', '', '', '', '', '', 'A', '404000004605'),
(1128, 19, 589, 2, 'Monitor Speaker QUEST  MS801', '', '', '', '', '', 'A', '404000004606'),
(1129, 19, 589, 2, 'Monitor Speaker QUEST  MS801', '', '', '', '', '', 'A', '404000004607'),
(1130, 19, 590, 2, 'Monitor Speaker Active MixCubes', '', '', '', '', '', 'A', '404000004625'),
(1131, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003631'),
(1132, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003632'),
(1133, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003633'),
(1134, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003634'),
(1135, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003635'),
(1136, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003636'),
(1137, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003637'),
(1138, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003638'),
(1139, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003639'),
(1140, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003640'),
(1141, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003641'),
(1142, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003642'),
(1143, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003643'),
(1144, 19, 591, 2, 'Speaker NPE รุ่น VST-802B', '', '', '', '', '', 'A', '904000003644'),
(1145, 19, 592, 2, 'Amplifeier NPE TR-250', '', '', '', '', '', 'A', '903000002968'),
(1146, 19, 592, NULL, 'Amplifeier NPE TR-250', '', '', '', '', '', 'A', '903000002969'),
(1147, 19, 592, NULL, 'Amplifeier NPE TR-250', '', '', '', '', '', 'A', '903000002970'),
(1148, 19, 592, NULL, 'Amplifeier NPE TR-250', '', '', '', '', '', 'A', '903000002971'),
(1149, 19, 592, 2, 'Amplifeier NPE TR-250', '', '', '', '', '', 'A', '903000002972'),
(1150, 19, 592, 2, 'Amplifeier NPE TR-250', '', '', '', '', '', 'A', '903000002973'),
(1151, 20, 593, 2, '32 inch LCD TV Monitor', '', '', '', '', '', 'A', '404000007031'),
(1152, 20, 594, 2, 'Monitor LCD LG 32CS460', '', '', '', '', '', 'A', '404000008565'),
(1153, 20, 595, 2, 'Monitor LCD LG 32CS461', '', '', '', '', '', 'A', '404000008566'),
(1154, 20, 596, 2, 'Monitor LCD LG 32CS462', '', '', '', '', '', 'A', '404000008567'),
(1155, 20, 597, 2, 'Monitor LCD LG 32CS463', '', '', '', '', '', 'A', '404000008568'),
(1156, 20, 598, 2, 'Monitor LCD LG 32CS464', '', '', '', '', '', 'A', '404000008569'),
(1157, 20, 599, 2, 'Monitor LCD LG 32CS465', '', '', '', '', '', 'A', '404000008570'),
(1158, 20, 600, 2, 'Monitor LCD LG 32CS466', '', '', '', '', '', 'A', '404000008571'),
(1159, 20, 601, 2, 'Monitor LCD LG 32CS467', '', '', '', '', '', 'A', '404000008572'),
(1160, 20, 602, 2, 'Monitor LCD LG 32CS468', '', '', '', '', '', 'A', '404000008573'),
(1161, 20, 603, 2, 'Monitor LCD LG 32CS469', '', '', '', '', '', 'A', '404000008574'),
(1162, 20, 604, 2, 'CCTV recorder system + camera', '', '', '', '', '', 'A', '404000004142'),
(1163, 20, 605, 2, 'Plasma 3D TV Panasonic 50 inch', '', '', '', '', '', 'A', '404000004424'),
(1164, 20, 606, 2, 'Sceen 120 inch  GYGAR', '', '', '', '', '', 'A', '404000004488'),
(1165, 20, 606, 2, 'Sceen 120 inch  GYGAR', '', '', '', '', '', 'A', '404000004489'),
(1166, 20, 606, 2, 'Sceen 120 inch  GYGAR', '', '', '', '', '', 'A', '404000004490'),
(1167, 20, 606, 2, 'Sceen 120 inch  GYGAR', '', '', '', '', '', 'A', '404000004491'),
(1168, 20, 606, 2, 'Sceen 120 inch  GYGAR', '', '', '', '', '', 'A', '404000004492'),
(1169, 20, 606, 2, 'Sceen 120 inch  GYGAR', '', '', '', '', '', 'A', '404000004493'),
(1170, 20, 607, 2, 'Screen RAZR  WMW 150 inch', '', '', '', '', '', 'A', '404000004494'),
(1171, 20, 608, 2, 'Sony FWD-S42H1', '', '', '', '', '', 'A', '404000004581'),
(1172, 20, 609, 2, 'Sony LMD-2451W', '', '', '', '', '', 'A', '404000004582'),
(1173, 20, 610, 2, 'Marshall  V-LCD70MD-3G', '', '', '', '', '', 'A', '404000008709'),
(1174, 20, 610, 2, 'Marshall  V-LCD70MD-3G', '', '', '', '', '', 'A', '404000008710'),
(1175, 20, 610, 2, 'Marshall  V-LCD70MD-3G', '', '', '', '', '', 'A', '404000008711'),
(1176, 20, 611, 2, '8 inch LCD HDMI/HD-SD Monitor', '', '', '', '', '', 'A', '404000004716'),
(1177, 20, 612, 2, 'Computer Workstation', '', '', '', '', '', 'A', '410000020739'),
(1178, 21, 613, 2, 'Doorway Dolly', '', '', '', '', '', 'A', '404000004533'),
(1179, 21, 614, 2, 'Hot button for tracks 4PSC/SET', '', '', '', '', '', 'A', '404000004534'),
(1180, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004535'),
(1181, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004536'),
(1182, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004537'),
(1183, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004538'),
(1184, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004539'),
(1185, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004540'),
(1186, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004541'),
(1187, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004542'),
(1188, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004543'),
(1189, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004544'),
(1190, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004545'),
(1191, 21, 615, 2, 'Track Straight 4 ft', '', '', '', '', '', 'A', '404000004546'),
(1192, 21, 616, 2, 'Track Curve for 20 FT Diameter', '', '', '', '', '', 'A', '404000004587'),
(1193, 21, 616, 2, 'Track Curve for 20 FT Diameter', '', '', '', '', '', 'A', '404000004588'),
(1194, 21, 616, 2, 'Track Curve for 20 FT Diameter', '', '', '', '', '', 'A', '404000004589'),
(1195, 21, 616, 2, 'Track Curve for 20 FT Diameter', '', '', '', '', '', 'A', '404000004590'),
(1196, 21, 616, 2, 'Track Curve for 20 FT Diameter', '', '', '', '', '', 'A', '404000004591'),
(1197, 21, 616, 2, 'Track Curve for 20 FT Diameter', '', '', '', '', '', 'A', '404000004592'),
(1198, 21, 616, 2, 'Track Curve for 20 FT Diameter', '', '', '', '', '', 'A', '404000004593'),
(1199, 21, 616, 2, 'Track Curve for 20 FT Diameter', '', '', '', '', '', 'A', '404000004594'),
(1200, 22, 617, 2, 'Recorder Sony GV-HD700E', '', '', '', '', '', 'A', '404000004454'),
(1201, 22, 618, 2, 'Bluray Player DMP-BDT300', '', '', '', '', '', 'A', '404000004455'),
(1202, 22, 619, 2, 'Bluray Player SONY EDP-S300', '', '', '', '', '', 'A', '404000004456'),
(1203, 22, 620, 2, 'DVD Player Pioneer DV-310S', '', '', '', '', '', 'A', '904000003426'),
(1204, 22, 620, 2, 'DVD Player Pioneer DV-310S', '', '', '', '', '', 'A', '904000003427'),
(1205, 22, 620, 2, 'DVD Player Pioneer DV-310S', '', '', '', '', '', 'A', '904000003428'),
(1206, 22, 620, 2, 'DVD Player Pioneer DV-310S', '', '', '', '', '', 'A', '904000003429'),
(1207, 22, 620, 2, 'DVD Player Pioneer DV-310S', '', '', '', '', '', 'A', '904000003430'),
(1208, 22, 620, 2, 'DVD Player Pioneer DV-310S', '', '', '', '', '', 'A', '904000003431'),
(1209, 23, 621, 2, 'Data Video Tally (RMC-140)SE-800', '', '', '', '', '', 'A', '403000002364'),
(1210, 23, 622, 2, 'HP work station Model XW8600(Avid)', '', '', '', '', '', 'A', '410000015488'),
(1211, 24, 623, 2, 'Sony HVR-MRC1K CF Memmory Recording Unit', '', '', '', '', '', 'A', '404000004159'),
(1212, 24, 623, 2, 'Sony HVR-MRC1K CF Memmory Recording Unit', '', '', '', '', '', 'A', '404000004160'),
(1213, 24, 624, 2, 'Sony HVR-DR60 Hard Disk Units', '', '', '', '', '', 'A', '404000004161'),
(1214, 24, 624, 2, 'Sony HVR-DR60 Hard Disk Units', '', '', '', '', '', 'A', '404000004162'),
(1215, 25, 625, 2, '3D Scanner', '', '', '', '', '', 'A', '410000019946'),
(1216, 25, 626, 2, 'Laeser cutter MC90', '', '', '', '', '', 'A', '410000020067'),
(1217, 25, 627, 2, '3D Printer V-Flash desktop modelers', '', '', '', '', '', 'A', '410000020523'),
(1218, 25, 628, 2, 'Professionnal Etching Place', '', '', '', '', '', 'A', '411000001629'),
(1219, 26, 629, 2, 'Amplifier Rack 52.5x40x20 cm', '', '', '', '', '', 'A', '401000021367'),
(1220, 26, 629, 2, 'Amplifier Rack 52.5x40x20 cm', '', '', '', '', '', 'A', '401000021368'),
(1221, 26, 629, 2, 'Amplifier Rack 52.5x40x20 cm', '', '', '', '', '', 'A', '401000021369'),
(1222, 26, 629, 2, 'Amplifier Rack 52.5x40x20 cm', '', '', '', '', '', 'A', '401000021370'),
(1223, 26, 629, 2, 'Amplifier Rack 52.5x40x20 cm', '', '', '', '', '', 'A', '401000021371'),
(1224, 26, 629, 2, 'Amplifier Rack 52.5x40x20 cm', '', '', '', '', '', 'A', '401000021372'),
(1225, 26, 629, 2, 'Amplifier Rack 52.5x40x20 cm', '', '', '', '', '', 'A', '401000021373'),
(1226, 26, 630, 2, 'Amplifier in Classroom', '', '', '', '', '', 'A', '401000021664'),
(1227, 27, 631, 2, 'Generator 220 V Jiangdong', 'รุ่น JD6500-EC', '', '', '', '', 'A', '403000003726'),
(1228, 27, 632, 2, 'Visualizer LUMENS PS', '', '', '', '', '', 'A', '404000004344'),
(1229, 27, 633, 2, 'Screen Motor', '', '', '', '', '', 'A', '404000004510'),
(1230, 27, 634, 2, 'Mark VB Director Viewfinder', '', '', '', '', '', 'A', '404000004595'),
(1231, 27, 635, 2, 'Animation Work Station Wacom table Storage', '', '', '', '', '', 'A', '410000015429'),
(1232, 27, 636, 2, 'Bag for Canon 5D.7D', '', '', '', '', '', 'A', '904000004662'),
(1233, 27, 637, 2, 'Studio flash Set', '', '', '', '', '', 'A', '404000005135');

-- --------------------------------------------------------

--
-- Table structure for table `equipment_cracked_log`
--

CREATE TABLE IF NOT EXISTS `equipment_cracked_log` (
`id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `cracked_date` date NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_type`
--

CREATE TABLE IF NOT EXISTS `equipment_type` (
`id` int(11) NOT NULL,
  `equipment_type_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `equipment_type`
--

INSERT INTO `equipment_type` (`id`, `equipment_type_code`, `name`, `description`) VALUES
(1, '1001', 'Portable Sound System', 'Portable Sound System'),
(2, '1002', 'Remote Presenter', 'Remote Presenter'),
(3, '1003', 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)'),
(4, '1004', 'Sound Mic (Cable)', 'Sound Mic (Cable)'),
(5, '1005', 'DVD', 'DVD'),
(6, '1006', 'Visualizer', 'Visualizer'),
(7, '1007', 'COM(NOTEBOOK)', 'COM(NOTEBOOK)'),
(8, '1008', 'COM', 'COM'),
(9, '1009', 'LCD', 'LCD'),
(10, '1010', 'LED TV', 'LED TV'),
(11, '1011', 'DSLR', 'DSLR'),
(12, '1012', 'Lens', 'Lens'),
(13, '1013', 'VIDEO CAM', 'VIDEO CAM'),
(14, '1014', 'Tripod', 'Tripod'),
(15, '1015', 'C-Stand', 'C-Stand'),
(16, '1016', 'Lighting', 'Lighting'),
(17, '1017', 'Film camera', 'Film camera'),
(18, '1018', 'Flag', 'Flag'),
(19, '1019', 'Audio', 'Audio'),
(20, '1020', 'Monitor', 'Monitor'),
(21, '1021', 'Dolly', 'Dolly'),
(22, '1022', 'Player', 'Player'),
(23, '1023', 'Switcher', 'Switcher'),
(24, '1024', 'Recorder Media', 'Recorder Media'),
(25, '1025', 'Special', 'Special'),
(26, '1026', 'Av Rack', 'Av Rack'),
(27, '1027', 'Etc', 'Etc');

-- --------------------------------------------------------

--
-- Table structure for table `equipment_type_list`
--

CREATE TABLE IF NOT EXISTS `equipment_type_list` (
`id` int(11) NOT NULL,
  `equipment_type_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=638 ;

--
-- Dumping data for table `equipment_type_list`
--

INSERT INTO `equipment_type_list` (`id`, `equipment_type_id`, `name`) VALUES
(636, 27, 'Bag for Canon 5D.7D'),
(637, 27, 'Studio flash Set'),
(633, 27, 'Screen Motor'),
(634, 27, 'Mark VB Director Viewfinder'),
(635, 27, 'Animation Work Station Wacom table Storage'),
(631, 27, 'Generator 220 V Jiangdong'),
(632, 27, 'Visualizer LUMENS PS'),
(630, 26, 'Amplifier in Classroom'),
(629, 26, 'Amplifier Rack 52.5x40x20 cm'),
(627, 25, '3D Printer V-Flash desktop modelers'),
(628, 25, 'Professionnal Etching Place'),
(625, 25, '3D Scanner'),
(626, 25, 'Laeser cutter MC90'),
(624, 24, 'Sony HVR-DR60 Hard Disk Units'),
(623, 24, 'Sony HVR-MRC1K CF Memmory Recording Unit'),
(622, 23, 'HP work station Model XW8600(Avid)'),
(621, 23, 'Data Video Tally (RMC-140)SE-800'),
(619, 22, 'Bluray Player SONY EDP-S300'),
(620, 22, 'DVD Player Pioneer DV-310S'),
(618, 22, 'Bluray Player DMP-BDT300'),
(617, 22, 'Recorder Sony GV-HD700E'),
(616, 21, 'Track Curve for 20 FT Diameter'),
(615, 21, 'Track Straight 4 ft'),
(614, 21, 'Hot button for tracks 4PSC/SET'),
(613, 21, 'Doorway Dolly'),
(612, 20, 'Computer Workstation'),
(611, 20, '8 inch LCD HDMI/HD-SD Monitor'),
(610, 20, 'Marshall  V-LCD70MD-3G'),
(609, 20, 'Sony LMD-2451W'),
(608, 20, 'Sony FWD-S42H1'),
(607, 20, 'Screen RAZR  WMW 150 inch'),
(606, 20, 'Sceen 120 inch  GYGAR'),
(604, 20, 'CCTV recorder system + camera'),
(605, 20, 'Plasma 3D TV Panasonic 50 inch'),
(602, 20, 'Monitor LCD LG 32CS468'),
(603, 20, 'Monitor LCD LG 32CS469'),
(601, 20, 'Monitor LCD LG 32CS467'),
(599, 20, 'Monitor LCD LG 32CS465'),
(600, 20, 'Monitor LCD LG 32CS466'),
(598, 20, 'Monitor LCD LG 32CS464'),
(597, 20, 'Monitor LCD LG 32CS463'),
(596, 20, 'Monitor LCD LG 32CS462'),
(595, 20, 'Monitor LCD LG 32CS461'),
(593, 20, '32 inch LCD TV Monitor'),
(594, 20, 'Monitor LCD LG 32CS460'),
(592, 19, 'Amplifeier NPE TR-250'),
(591, 19, 'Speaker NPE รุ่น VST-802B'),
(590, 19, 'Monitor Speaker Active MixCubes'),
(589, 19, 'Monitor Speaker QUEST  MS801'),
(587, 19, 'YAMAHA RX-V463'),
(588, 19, 'Subwoofer YAMAHA  YST-SW315'),
(586, 19, 'Audio Interface MOTU Ultralite Hybrid'),
(585, 19, 'Boom Microphone with case'),
(583, 19, 'Sennheiser Long-gun'),
(584, 19, 'Shure Microphone'),
(582, 19, 'Condenser Microphone  Avant Electronics'),
(581, 19, 'Headphone Extreme Isolation EX29 '),
(579, 19, 'Studio Closed Dynamic Headphone'),
(580, 19, 'ZOOM H4N Digital Recorder'),
(577, 19, 'Piano M-Audio'),
(578, 19, 'Studio Vocal Condenser Mic incl Mic Stand'),
(575, 19, 'Amplifier for Drum Roland'),
(576, 19, 'Amplifier Roland  KC-150'),
(573, 19, 'Amplifier Fender Frontman 212R'),
(574, 19, 'E-Drun Roland'),
(572, 19, 'Guitar Godin  Redline3'),
(570, 19, 'Monitor Speaker'),
(571, 19, 'Foley Equpment'),
(569, 19, '4 Channels Headphone Amplifier'),
(567, 19, '12 Channels Audio Mixing Console'),
(568, 19, '8x8 Analog in/out USB/Firewire Audio interface'),
(565, 18, 'Flag 12x18 inch'),
(566, 18, 'Flag Roadrags 18x24 inch'),
(564, 18, 'Flag 18x24 inch'),
(562, 18, 'Flag 24x30 inch'),
(563, 18, 'Flag 24x36 inch'),
(560, 18, 'Meat Axe Flag 24x48 inch'),
(561, 18, 'High Temperature Metal Flag 24x36 inch'),
(559, 18, 'Flag 30x36 inch'),
(557, 16, '800W ARRI set'),
(558, 16, 'Nikon FM 10 Film Camera '),
(555, 16, 'KINO FLO GRAFFERKIT'),
(556, 16, 'KINO FLO Interview KIT'),
(554, 15, 'ARRi 300W PLUS PRESNEL + Light stand'),
(553, 15, 'Dedo light 150 W + Light stand'),
(551, 15, 'Avanger Manfrotto D520  (arm)'),
(552, 15, 'CN-900HS LED LIGHT + Light Stand'),
(549, 14, 'Jib Crane 9 feet'),
(550, 15, 'Avanger Manfrotto A2033L (c-stand)'),
(547, 14, 'Sachtler FSB 4 TRIPOD'),
(548, 14, 'Jib Crane 14 feet'),
(546, 14, 'Manfrotto MVK502AM'),
(544, 14, 'Peter Shoulder Brace (BCS-050)'),
(545, 14, 'Peter Lisand Studio Pedestal+100mm.Head'),
(543, 14, 'Car mount tripod'),
(542, 14, 'Shoulder Tripod'),
(541, 13, 'กล้องบันทึกภาพเคลื่อนไหวพร้อมอุปกรณ์'),
(539, 13, 'Sony HVR-Z7P HDV + optional item'),
(540, 13, 'Sony HVR-HD1000P Camcorder'),
(538, 13, 'Sony PD-170P(3CCD Camera DVCAM) + Remote'),
(537, 12, 'Lens EF 100 mm. F2.8L Macro IS USM'),
(535, 12, 'Lens EF 50 mm. F1.4 USM'),
(536, 12, 'Lens EF 85 mm. F1.2LII USM'),
(533, 12, 'LUMIX G VARIO 14-45 mm.F/3.5-F5.6 ASPH.'),
(534, 12, 'Lens 35 mm.F1.4 DG HSM'),
(531, 12, 'Lens Canon EF 50mm.F1.8'),
(532, 12, 'LUMIX G VARIO 45-200 mm.F/4.0-F5.6'),
(529, 12, 'Lens Canon EF 24-70mm.F2.8L IS2USM'),
(530, 12, 'Lens TOKINA 11-16mm F2.8IF For CanonMount'),
(528, 12, 'Lens Canon EF 70-200mm.F2.8L IS2USM'),
(527, 12, 'Sony VCL-HG1737C 1.7xTele Conversion Lens'),
(526, 12, 'Sony VCL-HGA07 0.7 Converter Lens-30/37mm.'),
(524, 11, 'Canon  5D Mark III(BODY)'),
(525, 11, 'Canon EOS 550D'),
(522, 11, 'Canon EOS 5D Mark III + 24-105'),
(523, 11, 'Canon  5D Mark III(BODY) High Definition');

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE IF NOT EXISTS `event_type` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `event_type`
--

INSERT INTO `event_type` (`id`, `name`, `description`) VALUES
(1, 'Meeting', ''),
(2, 'Seminar', ''),
(3, 'Teaching', ''),
(4, 'Visitting', ''),
(5, 'Exhibition', '');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `link`
--

CREATE TABLE IF NOT EXISTS `link` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `link`
--

INSERT INTO `link` (`id`, `name`, `description`, `url`) VALUES
(2, 'Intensive Mathematics', 'Intensive Mathematics', 'upload/files/1386317910/File.pdf'),
(3, 'Demand of MUIC students on the usage of e-Lecture for reviewing lessons', 'Demand of MUIC students on the usage of e-Lecture for reviewing lessons', 'upload/files/1386317902/File.pdf'),
(4, 'i2COM', 'i2COM', 'upload/files/1386317892/File.pdf'),
(5, 'The Acceptance of Technology Supporting Mobile Learning on 3G Wireless Network System of MUIC Instructors', 'THE ACCEPTANCE OF TECHNOLOGY SUPPORTING MOBILE LEARNING ON 3G WIRELESS NETWORK SYSTEM OF MUIC INSTRUCTOR1', 'upload/files/1386317883/File.pdf'),
(6, 'The Development of Web-Based Training on English Course for Language Proficiency Test for Academic Support Staff of Mahidol University International College', 'The Development of Web-Based Training on English Course for Language Proficiency Test for Academic Support Staff of Mahidol University International College', 'upload/files/1386317857/File.pdf'),
(7, 'iOS', 'Test', 'upload/files/1399367461/File.png');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `short_description`, `description`, `pic`, `create_by`, `create_date`) VALUES
(10, 'Training on e-Learning Session 01', 'e-Learning Development of MUIC', 'Training on e-Learning Development of MUIC On Friday, November 23, 2012', 'upload/news/10/Training on e-Learning Session 01.jpg', '1', '2013-12-11 14:43:13'),
(11, 'Training on e-Learning Session 02', 'e-Learning Development of MUIC', 'Training on e-Learning Development of MUIC On Friday, March 22, 2013', 'upload/news/11/Training on e-Learning Session 02.jpg', '1', '2013-12-11 14:43:51'),
(12, 'Training on e-Learning Session 03', 'e-Learning Development of MUIC', 'Training on e-Learning Development of MUIC On Friday, June 28, 2013', 'upload/news/12/Training on e-Learning Session 03.jpg', '1', '2013-12-11 14:44:01'),
(13, 'Practical Training Workshop', 'Creative Multimedia for an Improving Quality of Academic Work.', 'Instructor Asst. Prof. Jintavee Khlaisang, Ed.D. Assistant Professor, Department of Educational Technology and Communications, Chulalongkorn University. Contents (The workshop is conducted in English.)\r\n1. Multimedia and Academic Works: Research Case Study\r\n2. Benefits of utilizing multimedia: From Idea to Innovative Creation\r\n3. Design and Improvement\r\n4. Rights and Copyrights of Utilizing Multimedia\r\n5. Design and Improvement: From Idea to Practice', 'upload/news/13/Practical Training Workshop.jpg', '1', '2013-12-11 14:44:12');

-- --------------------------------------------------------

--
-- Table structure for table `period`
--

CREATE TABLE IF NOT EXISTS `period` (
`id` int(11) NOT NULL,
  `period_group_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `start_hour` int(11) NOT NULL,
  `start_min` int(11) NOT NULL,
  `end_hour` int(11) NOT NULL,
  `end_min` int(11) NOT NULL,
  `status_code` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `period`
--

INSERT INTO `period` (`id`, `period_group_id`, `name`, `description`, `start_hour`, `start_min`, `end_hour`, `end_min`, `status_code`) VALUES
(1, 1, '08.00 - 09.50', '', 8, 0, 9, 50, 'PERIOD_ACTIVE'),
(2, 1, '10.00 - 11.50', '', 10, 0, 11, 50, 'PERIOD_ACTIVE'),
(3, 1, '12.00 - 13.50', '', 12, 0, 13, 50, 'PERIOD_ACTIVE'),
(4, 1, '14.00 - 15.50', '', 14, 0, 15, 50, 'PERIOD_ACTIVE'),
(5, 1, '16.00 - 17.50', '', 16, 0, 17, 50, 'PERIOD_ACTIVE'),
(6, 1, '18.00 - 19.50', '', 18, 0, 19, 50, 'PERIOD_ACTIVE'),
(101, 2, '08.00 - 08.30', '', 8, 0, 8, 30, 'PERIOD_ACTIVE'),
(102, 2, '08.30 - 09.00', '', 8, 30, 9, 0, 'PERIOD_ACTIVE'),
(103, 2, '09.00 - 09.30', '', 9, 0, 9, 30, 'PERIOD_ACTIVE'),
(104, 2, '09.30 - 10.00', '', 9, 30, 10, 0, 'PERIOD_ACTIVE'),
(105, 2, '10.00 - 10.30', '', 10, 0, 10, 30, 'PERIOD_ACTIVE'),
(106, 2, '10.30 - 11.00', '', 10, 30, 11, 0, 'PERIOD_ACTIVE'),
(107, 2, '11.00 - 11.30', '', 11, 0, 11, 30, 'PERIOD_ACTIVE'),
(108, 2, '11.30 - 12.00', '', 11, 30, 12, 0, 'PERIOD_ACTIVE'),
(109, 2, '12.00 - 12.30', '', 12, 0, 12, 30, 'PERIOD_ACTIVE'),
(110, 2, '12.30 - 13.00', '', 12, 30, 13, 0, 'PERIOD_ACTIVE'),
(111, 2, '13.00 - 13.30', '', 13, 0, 13, 30, 'PERIOD_ACTIVE'),
(112, 2, '13.30 - 14.00', '', 13, 30, 14, 0, 'PERIOD_ACTIVE'),
(113, 2, '14.00 - 14.30', '', 14, 0, 14, 30, 'PERIOD_ACTIVE'),
(114, 2, '14.30 - 15.00', '', 14, 30, 15, 0, 'PERIOD_ACTIVE'),
(115, 2, '15.00 - 15.30', '', 15, 0, 15, 30, 'PERIOD_ACTIVE'),
(116, 2, '15.30 - 16.00', '', 15, 30, 16, 0, 'PERIOD_ACTIVE'),
(117, 2, '16.00 - 16.30', '', 16, 0, 16, 30, 'PERIOD_ACTIVE'),
(118, 2, '16.30 - 17.00', '', 16, 30, 17, 0, 'PERIOD_ACTIVE'),
(119, 2, '17.00 - 17.30', '', 17, 0, 17, 30, 'PERIOD_ACTIVE'),
(120, 2, '17.30 - 18.00', '', 17, 30, 18, 0, 'PERIOD_ACTIVE'),
(121, 2, '18.00 - 18.30', '', 18, 0, 18, 30, 'PERIOD_INACTIVE'),
(122, 2, '18.30 - 19.00', '', 18, 30, 19, 0, 'PERIOD_INACTIVE'),
(123, 2, '19.00 - 19.30', '', 19, 0, 19, 30, 'PERIOD_INACTIVE'),
(124, 2, '19.30 - 20.00', '', 19, 30, 20, 0, 'PERIOD_INACTIVE'),
(125, 2, '20.00 - 20.30', '', 20, 0, 20, 30, 'PERIOD_INACTIVE'),
(126, 2, '20.30 - 21.00', '', 20, 30, 21, 0, 'PERIOD_INACTIVE'),
(127, 2, '21.00 - 21.30', '', 21, 0, 21, 30, 'PERIOD_INACTIVE'),
(128, 2, '21.30 - 22.00', '', 21, 30, 22, 0, 'PERIOD_INACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `period_group`
--

CREATE TABLE IF NOT EXISTS `period_group` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status_code` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `period_group`
--

INSERT INTO `period_group` (`id`, `name`, `description`, `status_code`) VALUES
(1, 'Default Period', 'Default period is make each period for 30 minutes. Start from 8.00 - 22.00', 'PERIOD_GROUP_ACTIVE'),
(2, 'Meeting Period', '', 'PERIOD_GROUP_ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `permission_code` varchar(255) NOT NULL,
  `permission_group_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permission_code`, `permission_group_id`, `name`, `description`) VALUES
('CHECK_STATUS_REQUEST_BOOKING', 3, 'Check Status Booking', ''),
('CHECK_STATUS_REQUEST_BORROW', 4, 'Check Status Borrow', ''),
('CONFIRM_USER', 1, 'Confirm User', ''),
('CREATE_DEPARTMENT', 8, 'Create Department', ''),
('CREATE_EQUIPMENT', 7, 'Create Equipment', ''),
('CREATE_EQUIPMENT_TYPE', 6, 'Create Equipment Type', ''),
('CREATE_GALLERY', 16, 'Create Gallery', ''),
('CREATE_LINK', 17, 'Create Link', ''),
('CREATE_NEWS', 15, 'Create News', ''),
('CREATE_POSITION', 10, 'Create Position', ''),
('CREATE_PRESEN_TYPE', 11, 'Create Present Type', ''),
('CREATE_REQUEST_BOOKING', 3, 'Create Request Booking', ''),
('CREATE_REQUEST_BORROW', 4, 'Create Request Borrow', ''),
('CREATE_REQUEST_SERVICE', 5, 'Create Request Service', ''),
('CREATE_ROLE', 2, 'Create Role', ''),
('CREATE_ROOM', 9, 'Create Room', ''),
('CREATE_SEMESTER', 14, 'Create Semester', ''),
('CREATE_SERVICE_TYPE', 12, 'Create Service Type', ''),
('CREATE_SERVICE_TYPE_ITEM', 13, 'Create Service Type Item', ''),
('CREATE_SOCIAL_MEDIA', 18, 'Create Social Media', ''),
('CREATE_USER', 1, 'Create User', 'Can create user login and user information for user.'),
('DELETE_DEPARTMENT', 8, 'Delete Department', ''),
('DELETE_EQUIPMENT', 7, 'Delete Equipment', ''),
('DELETE_EQUIPMENT_TYPE', 6, 'Delete Equipment Type', ''),
('DELETE_GALLERY', 16, 'Delete Gallery', ''),
('DELETE_LINK', 17, 'Delete Link', ''),
('DELETE_NEWS', 15, 'Delete News', ''),
('DELETE_POSITION', 10, 'Delete Position', ''),
('DELETE_PRESENT_TYPE', 11, 'Delete Present Type', ''),
('DELETE_REQUEST_BOOKING', 3, 'Delete Request Booking', ''),
('DELETE_REQUEST_BORROW', 4, 'Delete Request Borrow', ''),
('DELETE_REQUEST_SERVICE', 5, 'Delete Request Service', ''),
('DELETE_ROLE', 2, 'Delete Role', ''),
('DELETE_ROOM', 9, 'Delete Room', ''),
('DELETE_SEMESTER', 14, 'Delete Semester', ''),
('DELETE_SERVICE_TYPE', 12, 'Delete Service Type', ''),
('DELETE_SERVICE_TYPE_ITEM', 13, 'Delete Service Type Item', ''),
('DELETE_SOCIAL_MEDIA', 18, 'Delete Social Media', ''),
('DELETE_USER', 1, 'Delete User', ''),
('EDIT_REQUEST_BOOKING', 3, 'Edit', ''),
('FULL_ADMIN', NULL, 'Full Admin', '"Full Admin" can access all module.'),
('UPDATE_DEPARTMENT', 8, 'Update Department', ''),
('UPDATE_EQUIPMENT', 7, 'Update Equipment', ''),
('UPDATE_EQUIPMENT_TYPE', 6, 'Update Equipment Type', ''),
('UPDATE_GALLERY', 16, 'Update Gallery', ''),
('UPDATE_LINK', 17, 'Update Link', ''),
('UPDATE_NEWS', 15, 'Update News', ''),
('UPDATE_POSITION', 10, 'Update Position', ''),
('UPDATE_PRESENT_TYPE', 11, 'Update Present Type', ''),
('UPDATE_REQUEST_BORROW', 4, 'Update Request Borrow', ''),
('UPDATE_REQUEST_SERVICE', 5, 'Update Request Service', ''),
('UPDATE_ROLE', 2, 'Update Role', ''),
('UPDATE_ROOM', 9, 'Update Room', ''),
('UPDATE_SEMESTER', 14, 'Update Semester', ''),
('UPDATE_SERVICE_TYPE', 12, 'Update Service Type', ''),
('UPDATE_SERVICE_TYPE)ITEM', 13, 'Update Service Type Item', ''),
('UPDATE_SOCIAL_MEDIA', 18, 'Update Social Media', ''),
('UPDATE_USER', 1, 'Update User', ''),
('VIEW_ALL_REQUEST_BOOKING', 3, 'View All Request Booking', ''),
('VIEW_ALL_REQUEST_BORROW', 4, 'View All Request Borrow', ''),
('VIEW_ALL_REQUEST_SERVICE', 5, 'View All Request Service', ''),
('VIEW_DEPARTMENT', 8, 'View Department', ''),
('VIEW_EQUIPMENT', 7, 'View Equipment', ''),
('VIEW_EQUIPMENT_TYPE', 6, 'View Equipment Type', ''),
('VIEW_GALLERY', 16, 'View Gallery', ''),
('VIEW_LINK', 17, 'View Link', ''),
('VIEW_NEWS', 15, 'View News', ''),
('VIEW_POSITION', 10, 'View Position', ''),
('VIEW_PRESENT_TYPE', 11, 'View Present Type', ''),
('VIEW_REQUEST_BOOKING', 3, 'View Request Booking', ''),
('VIEW_REQUEST_BORROW', 4, 'View Request Borrow', ''),
('VIEW_REQUEST_SERVICE', 5, 'View Request Service', ''),
('VIEW_ROLE', 2, 'View Role', ''),
('VIEW_ROOM', 9, 'View Room', ''),
('VIEW_SEMESTER', 14, 'View Semester', ''),
('VIEW_SERVICE_TYPE', 12, 'View Service Type', ''),
('VIEW_SERVICE_TYPE_ITEM', 13, 'View Service Type Item', ''),
('VIEW_SOCIAL_MEDIA', 18, 'View Social Media', ''),
('VIEW_USER', 1, 'View User', 'View user information');

-- --------------------------------------------------------

--
-- Table structure for table `permission_group`
--

CREATE TABLE IF NOT EXISTS `permission_group` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `permission_group`
--

INSERT INTO `permission_group` (`id`, `name`, `description`) VALUES
(1, 'User', ''),
(2, 'Role', ''),
(3, 'Request Booking', ''),
(4, 'Request Borrow', ''),
(5, 'Request Service', ''),
(6, 'Equipment Type', ''),
(7, 'Equipment', ''),
(8, 'Department', ''),
(9, 'Room', ''),
(10, 'Position', ''),
(11, 'Present Type', ''),
(12, 'Service Type', ''),
(13, 'Service Type Item', ''),
(14, 'Semester', ''),
(15, 'ED Tech News', ''),
(16, 'Gallery', ''),
(17, 'Link', ''),
(18, 'Social Media', '');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
`id` int(11) NOT NULL,
  `position_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `present_type`
--

CREATE TABLE IF NOT EXISTS `present_type` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `present_type`
--

INSERT INTO `present_type` (`id`, `name`, `description`) VALUES
(1, 'Powerpoint presentation', ''),
(2, 'VDO presentation', ''),
(3, 'MUIC presentation', ''),
(4, 'Paper or Written on paper', '');

-- --------------------------------------------------------

--
-- Table structure for table `request_booking`
--

CREATE TABLE IF NOT EXISTS `request_booking` (
`id` int(11) NOT NULL,
  `user_login_id` int(11) NOT NULL,
  `request_booking_type_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `request_date` date DEFAULT NULL,
  `request_day_in_week` int(11) DEFAULT NULL,
  `period_start` int(11) NOT NULL,
  `period_end` int(11) NOT NULL,
  `description` text NOT NULL,
  `status_code` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `request_sky_id` int(11) NOT NULL,
  `request_sky_noti_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1025 ;

--
-- Dumping data for table `request_booking`
--

INSERT INTO `request_booking` (`id`, `user_login_id`, `request_booking_type_id`, `room_id`, `semester_id`, `request_date`, `request_day_in_week`, `period_start`, `period_end`, `description`, `status_code`, `create_date`, `course_name`, `request_sky_id`, `request_sky_noti_id`) VALUES
(84, 198, 1, 1302, NULL, '2014-09-08', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-09-07 14:12:59', 'TEST01', 0, 0),
(85, 198, 2, 1407, 7, NULL, 5, 1, 1, '', 'REQUEST_APPROVE', '2014-09-07 14:15:32', 'TEST02_SEM', 0, 0),
(86, 185, 3, 2, NULL, '2014-09-23', NULL, 105, 115, '', 'REQUEST_APPROVE', '2014-09-07 14:46:11', '', 0, 0),
(87, 185, 3, 1, NULL, '2014-09-08', NULL, 106, 114, '', 'REQUEST_APPROVE', '2014-09-07 15:21:07', '', 0, 0),
(88, 1, 2, 1308, 7, NULL, 2, 1, 1, '', 'REQUEST_APPROVE', '2014-09-20 09:37:18', '001', 0, 0),
(89, 1, 1, 1314, NULL, '2014-09-23', NULL, 1, 4, '', 'REQUEST_APPROVE', '2014-09-20 09:40:09', '001', 0, 0),
(90, 185, 1, 1314, NULL, '2014-09-24', NULL, 5, 5, '', 'REQUEST_APPROVE', '2014-09-24 13:47:34', 'ce111', 0, 0),
(91, 185, 1, 1406, NULL, '2014-09-24', NULL, 5, 5, '', 'REQUEST_APPROVE', '2014-09-24 13:48:29', 'ce333', 0, 0),
(92, 158, 1, 1303, NULL, '2014-11-27', NULL, 5, 5, '', 'REQUEST_APPROVE', '2014-09-24 13:49:32', 'ce33', 0, 0),
(93, 158, 2, 1405, 7, NULL, 5, 1, 3, '', 'REQUEST_APPROVE', '2014-09-24 13:50:21', 'c5', 0, 0),
(94, 1, 2, 1306, 7, NULL, 2, 1, 2, '', 'REQUEST_APPROVE', '2014-09-28 11:42:26', '001', 0, 0),
(95, 1, 3, 2207, NULL, '2014-11-05', NULL, 103, 107, '', 'REQUEST_APPROVE', '2014-11-03 08:02:44', '', 0, 0),
(96, 1, 2, 1306, 7, NULL, 4, 3, 4, '', 'REQUEST_APPROVE', '2014-11-03 08:13:52', '1123', 0, 0),
(97, 1, 3, 1214, NULL, '2014-11-19', NULL, 105, 106, '', 'REQUEST_APPROVE', '2014-11-03 08:21:05', '', 0, 0),
(98, 1, 1, 2307, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:00:26', '1745, BBA', 233, 9),
(99, 1, 1, 2307, NULL, '2014-07-19', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:08:25', '1745, BBA', 237, 9),
(100, 1, 1, 2307, NULL, '2014-07-14', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:10:45', '1745, BBA', 238, 9),
(101, 1, 1, 3306, NULL, '2014-07-17', NULL, 1, 2, 'Tel: 0899181232', 'REQUEST_APPROVE', '2014-07-16 10:31:21', 'Tel: 0899181232', 647, 9),
(102, 1, 1, 1417, NULL, '2014-07-28', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:04:43', '1745, BBA', 235, 9),
(103, 1, 1, 1417, NULL, '2014-07-29', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:06:00', '1745, BBA', 236, 9),
(104, 1, 1, 2307, NULL, '2014-07-23', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-25 06:55:35', '1745, BBA', 255, 9),
(105, 1, 1, 3302, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-27 01:58:22', '1745, BBA', 286, 9),
(106, 1, 1, 1404, NULL, '2014-07-07', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-06 06:49:17', '', 435, 9),
(107, 1, 1, 3414, NULL, '2014-07-30', NULL, 1, 1, 'งานพัสดุ 1306', 'REQUEST_APPROVE', '2014-07-28 08:44:15', 'งานพัสดุ 1306', 745, 9),
(108, 1, 1, 1318, NULL, '2014-07-09', NULL, 1, 3, 'I must use this room for our important final presentation. ', 'REQUEST_APPROVE', '2014-06-02 12:07:04', 'I must use this room for our important final presentation. ', 19, 9),
(109, 1, 1, 3410, NULL, '2014-07-18', NULL, 1, 1, 'Division: BBA\nTel: (66) 2-441-5090 ext. 1419\nE-mail: alessandro.sta@mahidol.ac.th\nRoom Requirements: Any room with projector and microphone', 'REQUEST_APPROVE', '2014-06-20 09:42:41', 'Division: BBA\nTel: (66) 2-441-5090 ext. 1419\nE-mail: alessandro.sta@mahidol.ac.th\nRoom Requirements: Any room with projector and microphone', 202, 9),
(110, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-20 03:28:40', '', 195, 9),
(111, 1, 1, 1307, NULL, '2014-07-09', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-06-23 11:49:25', '', 219, 9),
(112, 1, 1, 2308, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:02:27', '1745, BBA', 234, 9),
(113, 1, 1, -1, NULL, '2014-07-29', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-12 08:49:22', '', 99, 9),
(114, 1, 1, 3414, NULL, '2014-07-28', NULL, 1, 1, '1305 \nงานพัสดุ \nห้อง 1207', 'REQUEST_APPROVE', '2014-07-16 09:39:27', '1305 \nงานพัสดุ \nห้อง 1207', 645, 9),
(115, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:03:06', '', 589, 9),
(116, 1, 1, -1, NULL, '2014-07-28', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-12 08:46:19', '', 98, 9),
(117, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 2, 'IT ', 'REQUEST_APPROVE', '2014-07-18 01:13:42', 'IT ', 681, 9),
(118, 1, 1, 3421, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 16:57:22', '', 648, 9),
(119, 1, 1, 3412, NULL, '2014-07-08', NULL, 1, 1, '1511/ THM/ 3412', 'REQUEST_APPROVE', '2014-06-26 03:31:21', '1511/ THM/ 3412', 274, 9),
(120, 1, 1, 3414, NULL, '2014-07-22', NULL, 1, 1, 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 'REQUEST_APPROVE', '2014-07-16 02:56:37', 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 551, 9),
(121, 1, 1, 3316, NULL, '2014-07-17', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-07-02 12:20:17', '', 359, 9),
(122, 1, 1, 3304, NULL, '2014-07-17', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 09:01:33', 'prepare for exam', 478, 9),
(123, 1, 1, 3415, NULL, '2014-07-23', NULL, 1, 2, 'นักศึกษา ป.โท defense 23 ก.ค.57 เวลา 9.00 -18.00 น. ขอห้องให้ (3515)', 'REQUEST_APPROVE', '2014-07-18 01:08:28', 'นักศึกษา ป.โท defense 23 ก.ค.57 เวลา 9.00 -18.00 น. ขอห้องให้ (3515)', 680, 9),
(124, 1, 1, 3410, NULL, '2014-07-07', NULL, 1, 3, 'Tel: 0818252922\nDivision: Computer Science \nRoom Requirements: Any lecture room ', 'REQUEST_APPROVE', '2014-07-03 06:41:44', 'Tel: 0818252922\nDivision: Computer Science \nRoom Requirements: Any lecture room ', 395, 9),
(125, 1, 1, -1, NULL, '2014-07-07', NULL, 1, 2, 'Tel: 0827866709\nDivision:  \nRoom Requirements: Any room', 'REQUEST_APPROVE', '2014-07-03 12:02:19', 'Tel: 0827866709\nDivision:  \nRoom Requirements: Any room', 408, 9),
(126, 1, 1, 3420, NULL, '2014-07-24', NULL, 1, 1, 'Accord Visit (พลังงาน)  วันที่ 24 ก.ค.57 เวลา 14.00-16.00 น. (ขอใช้ห้องประชุม 2207)', 'REQUEST_APPROVE', '2014-07-18 01:13:50', 'Accord Visit (พลังงาน)  วันที่ 24 ก.ค.57 เวลา 14.00-16.00 น. (ขอใช้ห้องประชุม 2207)', 682, 9),
(127, 1, 1, 2207, NULL, '2014-07-30', NULL, 1, 1, 'ขอใช้ห้อง 2207ค่ะ', 'REQUEST_APPROVE', '2014-07-22 03:25:14', 'ขอใช้ห้อง 2207ค่ะ', 705, 9),
(128, 1, 1, 2207, NULL, '2014-07-30', NULL, 1, 1, 'Tel : 1761\nDivision : THM \nReserved by : Teerawan Nuntakij\nRoom Requirements : meeting room', 'REQUEST_APPROVE', '2014-07-25 03:25:08', 'Tel : 1761\nDivision : THM \nReserved by : Teerawan Nuntakij\nRoom Requirements : meeting room', 724, 9),
(129, 1, 1, 3414, NULL, '2014-07-23', NULL, 1, 1, 'BBA, 1745, Meeting room', 'REQUEST_APPROVE', '2014-07-17 06:09:22', 'BBA, 1745, Meeting room', 654, 9),
(130, 1, 1, 1418, NULL, '2014-07-07', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-04 10:10:11', '', 425, 9),
(131, 1, 1, 2302, NULL, '2014-07-07', NULL, 1, 1, 'room with mirrror', 'REQUEST_APPROVE', '2014-07-04 10:05:39', 'room with mirrror', 423, 9),
(132, 1, 1, 2302, NULL, '2014-07-07', NULL, 1, 3, 'any room with mirror', 'REQUEST_APPROVE', '2014-07-04 10:07:58', 'any room with mirror', 424, 9),
(133, 1, 1, 1314, NULL, '2014-07-07', NULL, 1, 2, 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Computer', 'REQUEST_APPROVE', '2014-07-04 13:32:35', 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Computer', 428, 9),
(134, 1, 1, 1302, NULL, '2014-07-07', NULL, 1, 1, 'Tel: 090-994-4285', 'REQUEST_APPROVE', '2014-07-05 09:33:46', 'Tel: 090-994-4285', 433, 9),
(135, 1, 1, 1406, NULL, '2014-07-07', NULL, 1, 3, 'tel; 0805918753\ndivisin;thm\nrequirements''ayrooms with omputer', 'REQUEST_APPROVE', '2014-07-07 07:36:32', 'tel; 0805918753\ndivisin;thm\nrequirements''ayrooms with omputer', 441, 9),
(136, 1, 1, 1302, NULL, '2014-07-07', NULL, 1, 1, 'Tel: 090-994-4285', 'REQUEST_APPROVE', '2014-07-05 09:29:14', 'Tel: 090-994-4285', 432, 9),
(137, 1, 1, 3415, NULL, '2014-07-07', NULL, 1, 1, 'Room requirements: using computer ', 'REQUEST_APPROVE', '2014-07-05 05:16:29', 'Room requirements: using computer ', 430, 9),
(138, 1, 1, 3408, NULL, '2014-07-07', NULL, 1, 3, 'Tel:0817200446\nRoom Requirement : Any rooms at IC building', 'REQUEST_APPROVE', '2014-07-07 05:33:29', 'Tel:0817200446\nRoom Requirement : Any rooms at IC building', 439, 9),
(139, 1, 1, 3306, NULL, '2014-07-07', NULL, 1, 2, '1118: BBA', 'REQUEST_APPROVE', '2014-07-07 03:07:20', '1118: BBA', 437, 9),
(140, 1, 1, 3411, NULL, '2014-07-07', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-07 05:19:51', '', 438, 9),
(141, 1, 1, 3412, NULL, '2014-07-07', NULL, 1, 2, '1745, BBA', 'REQUEST_APPROVE', '2014-07-07 07:12:28', '1745, BBA', 440, 9),
(142, 1, 1, 3411, NULL, '2014-07-09', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-08 10:40:25', '', 450, 9),
(143, 1, 1, 1304, NULL, '2014-07-09', NULL, 2, 3, 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Compute lap', 'REQUEST_APPROVE', '2014-07-07 10:23:13', 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Compute lap', 443, 9),
(144, 1, 1, 2302, NULL, '2014-07-09', NULL, 1, 2, 'Contact งานคุมสอบ', 'REQUEST_APPROVE', '2014-07-08 01:04:07', 'Contact งานคุมสอบ', 445, 9),
(145, 1, 1, 1312, NULL, '2014-07-27', NULL, 1, 1, 'Admissions\nTel: 1245\nUse for MUIC Entrance Examination ', 'REQUEST_APPROVE', '2014-07-08 07:58:03', 'Admissions\nTel: 1245\nUse for MUIC Entrance Examination ', 448, 9),
(146, 1, 1, 3412, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:54:54', 'Tel: 0909944285', 452, 9),
(147, 1, 1, 3315, NULL, '2014-07-09', NULL, 1, 3, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:56:18', 'Tel: 0909944285', 455, 9),
(148, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:55:20', 'Tel: 0909944285', 453, 9),
(149, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:55:48', 'Tel: 0909944285', 454, 9),
(150, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-08 10:38:46', '', 449, 9),
(151, 1, 1, 1418, NULL, '2014-07-26', NULL, 1, 2, 'Admissions\nTel: 1245\nUse for the MUIC Entrance Examination (ขอห้อง 1418-1419 ค่ะ)', 'REQUEST_APPROVE', '2014-07-08 07:54:47', 'Admissions\nTel: 1245\nUse for the MUIC Entrance Examination (ขอห้อง 1418-1419 ค่ะ)', 447, 9),
(152, 1, 1, 3410, NULL, '2014-07-15', NULL, 1, 1, 'BBA:1118', 'REQUEST_APPROVE', '2014-07-09 01:10:42', 'BBA:1118', 457, 9),
(153, 1, 1, -1, NULL, '2014-07-17', NULL, 1, 1, 'Faculty support services unit\nMrs. Sopa ext. 1125', 'REQUEST_APPROVE', '2014-07-09 03:09:18', 'Faculty support services unit\nMrs. Sopa ext. 1125', 459, 9),
(154, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 1, 'Faculty Support Services Unit\nMrs. Sopa Ext. 1125', 'REQUEST_APPROVE', '2014-07-09 03:24:03', 'Faculty Support Services Unit\nMrs. Sopa Ext. 1125', 460, 9),
(155, 1, 1, 3407, NULL, '2014-07-09', NULL, 2, 3, 'tel; 0805918753\ndivision; thm\nroom requirements; an cmpter lab', 'REQUEST_APPROVE', '2014-07-09 04:37:23', 'tel; 0805918753\ndivision; thm\nroom requirements; an cmpter lab', 461, 9),
(156, 1, 1, -1, NULL, '2014-07-16', NULL, 2, 3, 'Faculty Support Services Units\nMrs. Sopa ext 1125', 'REQUEST_APPROVE', '2014-07-10 06:30:45', 'Faculty Support Services Units\nMrs. Sopa ext 1125', 462, 9),
(157, 1, 1, -1, NULL, '2014-07-17', NULL, 2, 3, 'Faculty Support Services Units\nMrs. Sopa ext 1125', 'REQUEST_APPROVE', '2014-07-10 06:32:16', 'Faculty Support Services Units\nMrs. Sopa ext 1125', 463, 9),
(158, 1, 1, 3412, NULL, '2014-07-15', NULL, 1, 2, 'prepare exam', 'REQUEST_APPROVE', '2014-07-14 08:14:50', 'prepare exam', 475, 9),
(159, 1, 1, 1307, NULL, '2014-07-16', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 08:16:30', 'prepare for exam', 476, 9),
(160, 1, 1, 3410, NULL, '2014-07-17', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 08:18:04', 'prepare for exam', 477, 9),
(161, 1, 1, 3317, NULL, '2014-07-12', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-11 07:06:49', '', 467, 9),
(162, 1, 1, 3412, NULL, '2014-07-12', NULL, 1, 1, 'Room Requirements: Projector', 'REQUEST_APPROVE', '2014-07-11 11:18:52', 'Room Requirements: Projector', 468, 9),
(163, 1, 1, 3407, NULL, '2014-07-12', NULL, 2, 2, 'tel;0805918753\ndivision; thm\nroom  requirements; any room with  computer', 'REQUEST_APPROVE', '2014-07-12 06:16:16', 'tel;0805918753\ndivision; thm\nroom  requirements; any room with  computer', 470, 9),
(164, 1, 1, -1, NULL, '2014-07-16', NULL, 1, 1, 'Mrs. Sopa\nFaculty Support Units\next. 1125', 'REQUEST_APPROVE', '2014-07-12 06:31:53', 'Mrs. Sopa\nFaculty Support Units\next. 1125', 471, 9),
(165, 1, 1, 3410, NULL, '2014-07-14', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-07-13 05:48:20', '', 472, 9),
(166, 1, 1, 3407, NULL, '2014-07-14', NULL, 1, 2, 'tel;0805918753\ndivisioon;scieence\nroom  quirement; anny computer lab', 'REQUEST_APPROVE', '2014-07-14 06:16:10', 'tel;0805918753\ndivisioon;scieence\nroom  quirement; anny computer lab', 474, 9),
(167, 1, 1, 3306, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-14 09:39:27', '', 479, 9),
(168, 1, 1, 3410, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-14 09:40:14', '', 480, 9),
(169, 1, 1, 3317, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 02:59:35', '', 484, 9),
(170, 1, 1, 3302, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 03:00:12', '', 485, 9),
(171, 1, 1, 3410, NULL, '2014-07-16', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-15 03:01:25', '', 487, 9),
(172, 1, 1, 3302, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 03:00:47', '', 486, 9),
(173, 1, 1, 3306, NULL, '2014-07-15', NULL, 1, 1, 'BBA, 1745', 'REQUEST_APPROVE', '2014-07-15 03:01:49', 'BBA, 1745', 488, 9),
(174, 1, 1, 3305, NULL, '2014-07-16', NULL, 6, 1, 'tel:08144444311', 'REQUEST_APPROVE', '2014-07-15 18:26:47', 'tel:08144444311', 503, 9),
(175, 1, 1, 2207, NULL, '2014-07-16', NULL, 1, 2, 'MATV system meeting', 'REQUEST_APPROVE', '2014-07-15 06:45:49', 'MATV system meeting', 491, 9),
(176, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '1118:BBA', 'REQUEST_APPROVE', '2014-07-15 06:49:35', '1118:BBA', 492, 9),
(177, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'Tel: 0814444311', 'REQUEST_APPROVE', '2014-07-15 18:30:58', 'Tel: 0814444311', 505, 9),
(178, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 06:53:44', 'Tel:0814444311', 493, 9),
(179, 1, 1, 3415, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 06:56:53', '', 494, 9),
(180, 1, 1, 2207, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 07:41:55', '', 496, 9),
(181, 1, 1, 3407, NULL, '2014-07-15', NULL, 1, 2, 'te;0805918753\ndivisioon;science\nroom requireents; any opter lab', 'REQUEST_APPROVE', '2014-07-15 07:34:34', 'te;0805918753\ndivisioon;science\nroom requireents; any opter lab', 495, 9),
(182, 1, 1, 2207, NULL, '2014-07-28', NULL, 1, 1, 'Science Division\nRoom Requirement: any meeting room', 'REQUEST_APPROVE', '2014-07-15 09:53:41', 'Science Division\nRoom Requirement: any meeting room', 498, 9),
(183, 1, 1, 3414, NULL, '2014-07-16', NULL, 1, 1, '1306 งานพัสดุ', 'REQUEST_APPROVE', '2014-07-15 08:35:01', '1306 งานพัสดุ', 497, 9),
(184, 1, 1, 3414, NULL, '2014-07-24', NULL, 1, 1, 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 'REQUEST_APPROVE', '2014-07-17 01:45:56', 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 649, 9),
(185, 1, 1, 3411, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 15:35:20', '', 500, 9),
(186, 1, 1, 3305, NULL, '2014-07-15', NULL, 6, 1, 'tel: 0814444311', 'REQUEST_APPROVE', '2014-07-15 18:21:30', 'tel: 0814444311', 501, 9),
(187, 1, 1, 3305, NULL, '2014-07-15', NULL, 1, 1, 'tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 18:23:35', 'tel:0814444311', 502, 9),
(188, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 1, 'Tel. 1201\nOffice of the Dean\nRoom : 1214 Dean meeting room', 'REQUEST_APPROVE', '2014-07-15 10:15:36', 'Tel. 1201\nOffice of the Dean\nRoom : 1214 Dean meeting room', 499, 9),
(189, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 18:28:19', 'tel:0814444311', 504, 9),
(190, 1, 1, 3414, NULL, '2014-07-19', NULL, 1, 1, 'Science Division\nRoom Requirement: Any meeting room', 'REQUEST_APPROVE', '2014-07-17 05:24:32', 'Science Division\nRoom Requirement: Any meeting room', 653, 9),
(191, 1, 1, 1214, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 01:35:55', '', 512, 9),
(192, 1, 1, 1214, NULL, '2014-07-18', NULL, 1, 1, 'มีการจัดเลี้ยงอาหารด้วยค่ะ ', 'REQUEST_APPROVE', '2014-07-18 02:20:52', 'มีการจัดเลี้ยงอาหารด้วยค่ะ ', 683, 9),
(193, 1, 1, 3303, NULL, '2014-07-18', NULL, 1, 2, 'Any room on the 3rd floor', 'REQUEST_APPROVE', '2014-07-17 01:55:09', 'Any room on the 3rd floor', 650, 9),
(194, 1, 1, 1210, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-18 02:49:35', '', 684, 9),
(195, 1, 1, 1214, NULL, '2014-07-23', NULL, 1, 1, '02-800-3574\nDivision : HLD\nRoom : 1214', 'REQUEST_APPROVE', '2014-07-22 03:31:39', '02-800-3574\nDivision : HLD\nRoom : 1214', 706, 9),
(196, 1, 1, 3315, NULL, '2014-07-25', NULL, 1, 1, 'การอบรมการใช้งานชุดทดสอบประสิทธิภาพการเรียนการสอน (Clicker)\nโดยวิทยากร คือ คุณสรวีร์ วรกุลพิทักษ์ ตัวแทนจาก บริษัท อินเตอร์เอ็ดดูเคชั่น ซัพพลายส์ จำกัด \nในวันศุกร์ที่ 25 กรกฎาคม พ.ศ. 2557 เวลา 10:00-11:00 น.  ห้อง 3415', 'REQUEST_APPROVE', '2014-07-22 10:35:22', 'การอบรมการใช้งานชุดทดสอบประสิทธิภาพการเรียนการสอน (Clicker)\nโดยวิทยากร คือ คุณสรวีร์ วรกุลพิทักษ์ ตัวแทนจาก บริษัท อินเตอร์เอ็ดดูเคชั่น ซัพพลายส์ จำกัด \nในวันศุกร์ที่ 25 กรกฎาคม พ.ศ. 2557 เวลา 10:00-11:00 น.  ห้อง 3415', 711, 9),
(197, 1, 1, 3306, NULL, '2014-07-18', NULL, 1, 1, 'Tel: 0899181232', 'REQUEST_APPROVE', '2014-07-17 07:49:35', 'Tel: 0899181232', 655, 9),
(198, 1, 1, 2207, NULL, '2014-07-29', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:08:17', '', 528, 9),
(199, 1, 1, 3407, NULL, '2014-07-18', NULL, 1, 2, 'tel;0805918753\ndivision;thm\nroom requirement;any computer lab', 'REQUEST_APPROVE', '2014-07-18 06:38:12', 'tel;0805918753\ndivision;thm\nroom requirement;any computer lab', 685, 9),
(200, 1, 1, 3414, NULL, '2014-07-22', NULL, 1, 1, 'Tel.3555\nIT\nRoom 3414', 'REQUEST_APPROVE', '2014-07-22 07:02:07', 'Tel.3555\nIT\nRoom 3414', 709, 9),
(201, 1, 1, 1210, NULL, '2014-07-25', NULL, 1, 2, 'งานบริหารกลาง\nขอความอนุเคราะห์จัดให้สำหรับให้มีการเสนอชื่อ เบื้องต้นได้พูดคุยกับคุณสิทธาแล้ว ขอบคุณมากค่ะ\n', 'REQUEST_APPROVE', '2014-07-22 05:45:47', 'งานบริหารกลาง\nขอความอนุเคราะห์จัดให้สำหรับให้มีการเสนอชื่อ เบื้องต้นได้พูดคุยกับคุณสิทธาแล้ว ขอบคุณมากค่ะ\n', 708, 9),
(202, 1, 1, 1314, NULL, '2014-07-25', NULL, 6, 1, '', 'REQUEST_APPROVE', '2014-07-25 13:28:22', '', 728, 9),
(203, 1, 1, 1214, NULL, '2014-07-28', NULL, 1, 1, 'Ext. 1708-9\nDivision: IA\nRoom Requirements: Meeting room: 1214', 'REQUEST_APPROVE', '2014-07-18 06:39:25', 'Ext. 1708-9\nDivision: IA\nRoom Requirements: Meeting room: 1214', 686, 9),
(204, 1, 1, 3414, NULL, '2014-07-28', NULL, 1, 1, 'ประชุมงานเทคโนโลยีการศึกษา \nในวันจันทร์ที่ 28 กรกฏาคม พ.ศ. 2557\nห้องประชุม 3414 เวลา 09:00 - 10:00 น. \n\n', 'REQUEST_APPROVE', '2014-07-22 07:59:24', 'ประชุมงานเทคโนโลยีการศึกษา \nในวันจันทร์ที่ 28 กรกฏาคม พ.ศ. 2557\nห้องประชุม 3414 เวลา 09:00 - 10:00 น. \n\n', 710, 9),
(205, 1, 1, 2207, NULL, '2014-07-29', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:06:51', '', 527, 9),
(206, 1, 1, 2302, NULL, '2014-07-28', NULL, 1, 1, 'Tel: 085-8288832\nRoom Requirement : Mirror\n                                    ขอห้องกระจกค่าาาา', 'REQUEST_APPROVE', '2014-07-27 15:30:15', 'Tel: 085-8288832\nRoom Requirement : Mirror\n                                    ขอห้องกระจกค่าาาา', 730, 9),
(207, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:22:40', '', 535, 9),
(208, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:24:34', '', 536, 9),
(209, 1, 1, 2207, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:27:04', '', 537, 9),
(210, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:49:49', '', 549, 9),
(211, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:51:44', '', 550, 9),
(212, 1, 1, 1307, NULL, '2014-07-22', NULL, 1, 1, '085-8288832\nRoom Request: Mirror (ต้องการห้องกระจกค่ะ)', 'REQUEST_APPROVE', '2014-07-21 16:35:34', '085-8288832\nRoom Request: Mirror (ต้องการห้องกระจกค่ะ)', 703, 9),
(213, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:57:01', '', 583, 9),
(214, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:53:05', '', 582, 9),
(215, 1, 1, 3414, NULL, '2014-07-25', NULL, 2, 2, 'Science Division', 'REQUEST_APPROVE', '2014-07-24 04:02:25', 'Science Division', 715, 9),
(216, 1, 1, 2303, NULL, '2014-07-24', NULL, 1, 1, '1118:BBA', 'REQUEST_APPROVE', '2014-07-24 04:04:15', '1118:BBA', 716, 9),
(217, 1, 1, 3305, NULL, '2014-07-18', NULL, 1, 1, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-16 03:35:23', 'Tel:0814444311', 570, 9),
(218, 1, 1, 1210, NULL, '2014-07-20', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:49:19', '', 580, 9),
(219, 1, 1, 5212, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-20 07:46:50', '', 693, 9),
(220, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 04:01:47', '', 586, 9),
(221, 1, 1, 1210, NULL, '2014-07-20', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:51:09', '', 581, 9),
(222, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:05:21', '', 590, 9),
(223, 1, 1, 2207, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:06:56', '', 591, 9),
(224, 1, 1, 2207, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:09:07', '', 592, 9),
(225, 1, 1, 2207, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:13:29', '', 593, 9),
(226, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:15:16', '', 594, 9),
(227, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:16:58', '', 595, 9),
(228, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:18:56', '', 596, 9),
(229, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:20:20', '', 597, 9),
(230, 1, 1, 1214, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:22:28', '', 598, 9),
(231, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:30:43', '', 603, 9),
(232, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:32:16', '', 604, 9),
(233, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:33:58', '', 605, 9),
(234, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:35:13', '', 606, 9),
(235, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:36:47', '', 607, 9),
(236, 1, 1, 1210, NULL, '2014-07-27', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:38:10', '', 608, 9),
(237, 1, 1, 1210, NULL, '2014-07-27', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:39:25', '', 609, 9),
(238, 1, 1, 1214, NULL, '2014-07-16', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:47:13', '', 610, 9),
(239, 1, 1, 1214, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:48:47', '', 611, 9),
(240, 1, 1, 3305, NULL, '2014-07-16', NULL, 2, 3, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-16 05:20:58', 'Tel:0814444311', 587, 9),
(241, 1, 1, 1408, NULL, '2014-07-17', NULL, 1, 1, 'Tel: 0832502220', 'REQUEST_APPROVE', '2014-07-16 05:40:56', 'Tel: 0832502220', 588, 9),
(242, 1, 1, 3410, NULL, '2014-07-17', NULL, 1, 1, 'Computer', 'REQUEST_APPROVE', '2014-07-16 08:13:19', 'Computer', 635, 9),
(243, 1, 1, 3412, NULL, '2014-07-17', NULL, 1, 1, 'Computer', 'REQUEST_APPROVE', '2014-07-16 08:14:52', 'Computer', 636, 9),
(244, 1, 1, 3317, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:18:46', '', 625, 9),
(245, 1, 1, 3407, NULL, '2014-07-16', NULL, 1, 2, 'Tel.0805918753\nDivision.science \nRoom requirements.any computer lab ', 'REQUEST_APPROVE', '2014-07-16 08:01:59', 'Tel.0805918753\nDivision.science \nRoom requirements.any computer lab ', 634, 9),
(246, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:50:26', '', 612, 9),
(247, 1, 1, 2207, NULL, '2014-07-21', NULL, 1, 1, 'Room 2207\n02-800 3560 \nHLD', 'REQUEST_APPROVE', '2014-07-21 02:26:42', 'Room 2207\n02-800 3560 \nHLD', 695, 9),
(248, 1, 1, 3414, NULL, '2014-07-25', NULL, 1, 1, 'Ext.1210/SA', 'REQUEST_APPROVE', '2014-07-24 05:53:48', 'Ext.1210/SA', 717, 9),
(249, 1, 1, 3411, NULL, '2014-07-17', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-16 07:05:54', '', 623, 9),
(250, 1, 1, 3411, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:06:13', '', 624, 9),
(251, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:51:57', '', 613, 9),
(252, 1, 1, 1210, NULL, '2014-07-23', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 07:58:12', '', 633, 9),
(253, 1, 1, 2307, NULL, '2014-07-25', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-07-17 09:56:24', '1745, BBA', 678, 9),
(254, 1, 1, 3409, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:04:47', '', 620, 9),
(255, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 07:05:33', '', 622, 9),
(256, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:01:50', '', 618, 9),
(257, 1, 1, 1318, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:48:15', '', 630, 9),
(258, 1, 1, 1318, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:50:33', '', 631, 9),
(259, 1, 1, 1210, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:53:09', '', 632, 9),
(260, 1, 1, 1318, NULL, '2014-07-22', NULL, 1, 3, 'Tel : 085-828-8832\nRequirements : 3 Microphones and Projector screen\n** 3days Reservation - 18,19,22 August 2014\n', 'REQUEST_APPROVE', '2014-07-18 08:21:14', 'Tel : 085-828-8832\nRequirements : 3 Microphones and Projector screen\n** 3days Reservation - 18,19,22 August 2014\n', 689, 9),
(261, 1, 1, 3420, NULL, '2014-07-17', NULL, 1, 2, 'Final Revision for Asian Philosophy\n(Computer+internet)', 'REQUEST_APPROVE', '2014-07-16 02:16:41', 'Final Revision for Asian Philosophy\n(Computer+internet)', 533, 9),
(262, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:00:02', '', 617, 9),
(263, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:04:27', '', 619, 9),
(264, 1, 1, 3414, NULL, '2014-07-29', NULL, 1, 1, '1305\nงานพัสดุ\n3414', 'REQUEST_APPROVE', '2014-07-24 09:28:26', '1305\nงานพัสดุ\n3414', 721, 9),
(265, 1, 1, 3421, NULL, '2014-07-30', NULL, 1, 1, 'Computer, Projector, Internet', 'REQUEST_APPROVE', '2014-07-29 14:29:21', 'Computer, Projector, Internet', 760, 9),
(266, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:32:28', '', 626, 9),
(267, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:05:14', '', 621, 9),
(268, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:32:51', '', 637, 9),
(269, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 2, 'OE, ext. 1106', 'REQUEST_APPROVE', '2014-07-24 09:39:22', 'OE, ext. 1106', 722, 9),
(270, 1, 1, 1214, NULL, '2014-07-29', NULL, 1, 1, 'Data Hub Meeting room 2207 ', 'REQUEST_APPROVE', '2014-07-16 08:51:13', 'Data Hub Meeting room 2207 ', 643, 9),
(271, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:34:42', '', 638, 9),
(272, 1, 1, 3414, NULL, '2014-07-18', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 08:38:12', '', 640, 9),
(273, 1, 1, 3414, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:36:35', '', 639, 9),
(274, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:40:15', '', 641, 9),
(275, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:43:04', '', 642, 9),
(276, 1, 1, 1210, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:53:16', '', 644, 9),
(277, 1, 1, 3409, NULL, '2014-07-18', NULL, 1, 1, 'ประชุมด่วน วาระพิเศษ (SLL) ', 'REQUEST_APPROVE', '2014-07-17 10:28:24', 'ประชุมด่วน วาระพิเศษ (SLL) ', 679, 9),
(278, 1, 1, 3414, NULL, '2014-07-24', NULL, 1, 1, '1305\nงานพัสดุ \nห้อง 3414', 'REQUEST_APPROVE', '2014-07-21 09:02:00', '1305\nงานพัสดุ \nห้อง 3414', 700, 9),
(279, 1, 1, 2307, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:00:26', '1745, BBA', 233, 8),
(280, 1, 1, 2307, NULL, '2014-07-19', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:08:25', '1745, BBA', 237, 8),
(281, 1, 1, 2307, NULL, '2014-07-14', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:10:45', '1745, BBA', 238, 8),
(282, 1, 1, 3306, NULL, '2014-07-17', NULL, 1, 2, 'Tel: 0899181232', 'REQUEST_APPROVE', '2014-07-16 10:31:21', 'Tel: 0899181232', 647, 8),
(283, 1, 1, 1417, NULL, '2014-07-28', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:04:43', '1745, BBA', 235, 8),
(284, 1, 1, 1417, NULL, '2014-07-29', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:06:00', '1745, BBA', 236, 8),
(285, 1, 1, 2307, NULL, '2014-07-23', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-25 06:55:35', '1745, BBA', 255, 8),
(286, 1, 1, 3302, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-27 01:58:22', '1745, BBA', 286, 8),
(287, 1, 1, 1404, NULL, '2014-07-07', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-06 06:49:17', '', 435, 8),
(288, 1, 1, 3414, NULL, '2014-07-30', NULL, 1, 1, 'งานพัสดุ 1306', 'REQUEST_APPROVE', '2014-07-28 08:44:15', 'งานพัสดุ 1306', 745, 8),
(289, 1, 1, 1318, NULL, '2014-07-09', NULL, 1, 3, 'I must use this room for our important final presentation. ', 'REQUEST_APPROVE', '2014-06-02 12:07:04', 'I must use this room for our important final presentation. ', 19, 8),
(290, 1, 1, 3410, NULL, '2014-07-18', NULL, 1, 1, 'Division: BBA\nTel: (66) 2-441-5090 ext. 1419\nE-mail: alessandro.sta@mahidol.ac.th\nRoom Requirements: Any room with projector and microphone', 'REQUEST_APPROVE', '2014-06-20 09:42:41', 'Division: BBA\nTel: (66) 2-441-5090 ext. 1419\nE-mail: alessandro.sta@mahidol.ac.th\nRoom Requirements: Any room with projector and microphone', 202, 8),
(291, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-20 03:28:40', '', 195, 8),
(292, 1, 1, 1307, NULL, '2014-07-09', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-06-23 11:49:25', '', 219, 8),
(293, 1, 1, 2308, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:02:27', '1745, BBA', 234, 8),
(294, 1, 1, -1, NULL, '2014-07-29', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-12 08:49:22', '', 99, 8),
(295, 1, 1, 3414, NULL, '2014-07-28', NULL, 1, 1, '1305 \nงานพัสดุ \nห้อง 1207', 'REQUEST_APPROVE', '2014-07-16 09:39:27', '1305 \nงานพัสดุ \nห้อง 1207', 645, 8),
(296, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:03:06', '', 589, 8),
(297, 1, 1, -1, NULL, '2014-07-28', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-12 08:46:19', '', 98, 8),
(298, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 2, 'IT ', 'REQUEST_APPROVE', '2014-07-18 01:13:42', 'IT ', 681, 8),
(299, 1, 1, 3421, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 16:57:22', '', 648, 8),
(300, 1, 1, 3412, NULL, '2014-07-08', NULL, 1, 1, '1511/ THM/ 3412', 'REQUEST_APPROVE', '2014-06-26 03:31:21', '1511/ THM/ 3412', 274, 8),
(301, 1, 1, 3414, NULL, '2014-07-22', NULL, 1, 1, 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 'REQUEST_APPROVE', '2014-07-16 02:56:37', 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 551, 8),
(302, 1, 1, 3316, NULL, '2014-07-17', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-07-02 12:20:17', '', 359, 8),
(303, 1, 1, 3304, NULL, '2014-07-17', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 09:01:33', 'prepare for exam', 478, 8),
(304, 1, 1, 3415, NULL, '2014-07-23', NULL, 1, 2, 'นักศึกษา ป.โท defense 23 ก.ค.57 เวลา 9.00 -18.00 น. ขอห้องให้ (3515)', 'REQUEST_APPROVE', '2014-07-18 01:08:28', 'นักศึกษา ป.โท defense 23 ก.ค.57 เวลา 9.00 -18.00 น. ขอห้องให้ (3515)', 680, 8),
(305, 1, 1, 3410, NULL, '2014-07-07', NULL, 1, 3, 'Tel: 0818252922\nDivision: Computer Science \nRoom Requirements: Any lecture room ', 'REQUEST_APPROVE', '2014-07-03 06:41:44', 'Tel: 0818252922\nDivision: Computer Science \nRoom Requirements: Any lecture room ', 395, 8),
(306, 1, 1, -1, NULL, '2014-07-07', NULL, 1, 2, 'Tel: 0827866709\nDivision:  \nRoom Requirements: Any room', 'REQUEST_APPROVE', '2014-07-03 12:02:19', 'Tel: 0827866709\nDivision:  \nRoom Requirements: Any room', 408, 8),
(307, 1, 1, 3420, NULL, '2014-07-24', NULL, 1, 1, 'Accord Visit (พลังงาน)  วันที่ 24 ก.ค.57 เวลา 14.00-16.00 น. (ขอใช้ห้องประชุม 2207)', 'REQUEST_APPROVE', '2014-07-18 01:13:50', 'Accord Visit (พลังงาน)  วันที่ 24 ก.ค.57 เวลา 14.00-16.00 น. (ขอใช้ห้องประชุม 2207)', 682, 8),
(308, 1, 1, 2207, NULL, '2014-07-30', NULL, 1, 1, 'ขอใช้ห้อง 2207ค่ะ', 'REQUEST_APPROVE', '2014-07-22 03:25:14', 'ขอใช้ห้อง 2207ค่ะ', 705, 8),
(309, 1, 1, 2207, NULL, '2014-07-30', NULL, 1, 1, 'Tel : 1761\nDivision : THM \nReserved by : Teerawan Nuntakij\nRoom Requirements : meeting room', 'REQUEST_APPROVE', '2014-07-25 03:25:08', 'Tel : 1761\nDivision : THM \nReserved by : Teerawan Nuntakij\nRoom Requirements : meeting room', 724, 8),
(310, 1, 1, 3414, NULL, '2014-07-23', NULL, 1, 1, 'BBA, 1745, Meeting room', 'REQUEST_APPROVE', '2014-07-17 06:09:22', 'BBA, 1745, Meeting room', 654, 8),
(311, 1, 1, 1418, NULL, '2014-07-07', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-04 10:10:11', '', 425, 8),
(312, 1, 1, 2302, NULL, '2014-07-07', NULL, 1, 1, 'room with mirrror', 'REQUEST_APPROVE', '2014-07-04 10:05:39', 'room with mirrror', 423, 8),
(313, 1, 1, 2302, NULL, '2014-07-07', NULL, 1, 3, 'any room with mirror', 'REQUEST_APPROVE', '2014-07-04 10:07:58', 'any room with mirror', 424, 8),
(314, 1, 1, 1314, NULL, '2014-07-07', NULL, 1, 2, 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Computer', 'REQUEST_APPROVE', '2014-07-04 13:32:35', 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Computer', 428, 8),
(315, 1, 1, 1302, NULL, '2014-07-07', NULL, 1, 1, 'Tel: 090-994-4285', 'REQUEST_APPROVE', '2014-07-05 09:33:46', 'Tel: 090-994-4285', 433, 8),
(316, 1, 1, 1406, NULL, '2014-07-07', NULL, 1, 3, 'tel; 0805918753\ndivisin;thm\nrequirements''ayrooms with omputer', 'REQUEST_APPROVE', '2014-07-07 07:36:32', 'tel; 0805918753\ndivisin;thm\nrequirements''ayrooms with omputer', 441, 8),
(317, 1, 1, 1302, NULL, '2014-07-07', NULL, 1, 1, 'Tel: 090-994-4285', 'REQUEST_APPROVE', '2014-07-05 09:29:14', 'Tel: 090-994-4285', 432, 8),
(318, 1, 1, 3415, NULL, '2014-07-07', NULL, 1, 1, 'Room requirements: using computer ', 'REQUEST_APPROVE', '2014-07-05 05:16:29', 'Room requirements: using computer ', 430, 8),
(319, 1, 1, 3408, NULL, '2014-07-07', NULL, 1, 3, 'Tel:0817200446\nRoom Requirement : Any rooms at IC building', 'REQUEST_APPROVE', '2014-07-07 05:33:29', 'Tel:0817200446\nRoom Requirement : Any rooms at IC building', 439, 8),
(320, 1, 1, 3306, NULL, '2014-07-07', NULL, 1, 2, '1118: BBA', 'REQUEST_APPROVE', '2014-07-07 03:07:20', '1118: BBA', 437, 8),
(321, 1, 1, 3411, NULL, '2014-07-07', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-07 05:19:51', '', 438, 8),
(322, 1, 1, 3412, NULL, '2014-07-07', NULL, 1, 2, '1745, BBA', 'REQUEST_APPROVE', '2014-07-07 07:12:28', '1745, BBA', 440, 8),
(323, 1, 1, 3411, NULL, '2014-07-09', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-08 10:40:25', '', 450, 8),
(324, 1, 1, 1304, NULL, '2014-07-09', NULL, 2, 3, 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Compute lap', 'REQUEST_APPROVE', '2014-07-07 10:23:13', 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Compute lap', 443, 8),
(325, 1, 1, 2302, NULL, '2014-07-09', NULL, 1, 2, 'Contact งานคุมสอบ', 'REQUEST_APPROVE', '2014-07-08 01:04:07', 'Contact งานคุมสอบ', 445, 8),
(326, 1, 1, 1312, NULL, '2014-07-27', NULL, 1, 1, 'Admissions\nTel: 1245\nUse for MUIC Entrance Examination ', 'REQUEST_APPROVE', '2014-07-08 07:58:03', 'Admissions\nTel: 1245\nUse for MUIC Entrance Examination ', 448, 8),
(327, 1, 1, 3412, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:54:54', 'Tel: 0909944285', 452, 8),
(328, 1, 1, 3315, NULL, '2014-07-09', NULL, 1, 3, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:56:18', 'Tel: 0909944285', 455, 8),
(329, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:55:20', 'Tel: 0909944285', 453, 8),
(330, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:55:48', 'Tel: 0909944285', 454, 8),
(331, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-08 10:38:46', '', 449, 8),
(332, 1, 1, 1418, NULL, '2014-07-26', NULL, 1, 2, 'Admissions\nTel: 1245\nUse for the MUIC Entrance Examination (ขอห้อง 1418-1419 ค่ะ)', 'REQUEST_APPROVE', '2014-07-08 07:54:47', 'Admissions\nTel: 1245\nUse for the MUIC Entrance Examination (ขอห้อง 1418-1419 ค่ะ)', 447, 8),
(333, 1, 1, 3410, NULL, '2014-07-15', NULL, 1, 1, 'BBA:1118', 'REQUEST_APPROVE', '2014-07-09 01:10:42', 'BBA:1118', 457, 8),
(334, 1, 1, -1, NULL, '2014-07-17', NULL, 1, 1, 'Faculty support services unit\nMrs. Sopa ext. 1125', 'REQUEST_APPROVE', '2014-07-09 03:09:18', 'Faculty support services unit\nMrs. Sopa ext. 1125', 459, 8),
(335, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 1, 'Faculty Support Services Unit\nMrs. Sopa Ext. 1125', 'REQUEST_APPROVE', '2014-07-09 03:24:03', 'Faculty Support Services Unit\nMrs. Sopa Ext. 1125', 460, 8),
(336, 1, 1, 3407, NULL, '2014-07-09', NULL, 2, 3, 'tel; 0805918753\ndivision; thm\nroom requirements; an cmpter lab', 'REQUEST_APPROVE', '2014-07-09 04:37:23', 'tel; 0805918753\ndivision; thm\nroom requirements; an cmpter lab', 461, 8),
(337, 1, 1, -1, NULL, '2014-07-16', NULL, 2, 3, 'Faculty Support Services Units\nMrs. Sopa ext 1125', 'REQUEST_APPROVE', '2014-07-10 06:30:45', 'Faculty Support Services Units\nMrs. Sopa ext 1125', 462, 8),
(338, 1, 1, -1, NULL, '2014-07-17', NULL, 2, 3, 'Faculty Support Services Units\nMrs. Sopa ext 1125', 'REQUEST_APPROVE', '2014-07-10 06:32:16', 'Faculty Support Services Units\nMrs. Sopa ext 1125', 463, 8),
(339, 1, 1, 3412, NULL, '2014-07-15', NULL, 1, 2, 'prepare exam', 'REQUEST_APPROVE', '2014-07-14 08:14:50', 'prepare exam', 475, 8),
(340, 1, 1, 1307, NULL, '2014-07-16', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 08:16:30', 'prepare for exam', 476, 8),
(341, 1, 1, 3410, NULL, '2014-07-17', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 08:18:04', 'prepare for exam', 477, 8),
(342, 1, 1, 3317, NULL, '2014-07-12', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-11 07:06:49', '', 467, 8),
(343, 1, 1, 3412, NULL, '2014-07-12', NULL, 1, 1, 'Room Requirements: Projector', 'REQUEST_APPROVE', '2014-07-11 11:18:52', 'Room Requirements: Projector', 468, 8),
(344, 1, 1, 3407, NULL, '2014-07-12', NULL, 2, 2, 'tel;0805918753\ndivision; thm\nroom  requirements; any room with  computer', 'REQUEST_APPROVE', '2014-07-12 06:16:16', 'tel;0805918753\ndivision; thm\nroom  requirements; any room with  computer', 470, 8),
(345, 1, 1, -1, NULL, '2014-07-16', NULL, 1, 1, 'Mrs. Sopa\nFaculty Support Units\next. 1125', 'REQUEST_APPROVE', '2014-07-12 06:31:53', 'Mrs. Sopa\nFaculty Support Units\next. 1125', 471, 8),
(346, 1, 1, 3410, NULL, '2014-07-14', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-07-13 05:48:20', '', 472, 8),
(347, 1, 1, 3407, NULL, '2014-07-14', NULL, 1, 2, 'tel;0805918753\ndivisioon;scieence\nroom  quirement; anny computer lab', 'REQUEST_APPROVE', '2014-07-14 06:16:10', 'tel;0805918753\ndivisioon;scieence\nroom  quirement; anny computer lab', 474, 8),
(348, 1, 1, 3306, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-14 09:39:27', '', 479, 8),
(349, 1, 1, 3410, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-14 09:40:14', '', 480, 8),
(350, 1, 1, 3317, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 02:59:35', '', 484, 8),
(351, 1, 1, 3302, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 03:00:12', '', 485, 8),
(352, 1, 1, 3410, NULL, '2014-07-16', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-15 03:01:25', '', 487, 8),
(353, 1, 1, 3302, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 03:00:47', '', 486, 8),
(354, 1, 1, 3306, NULL, '2014-07-15', NULL, 1, 1, 'BBA, 1745', 'REQUEST_APPROVE', '2014-07-15 03:01:49', 'BBA, 1745', 488, 8),
(355, 1, 1, 3305, NULL, '2014-07-16', NULL, 6, 1, 'tel:08144444311', 'REQUEST_APPROVE', '2014-07-15 18:26:47', 'tel:08144444311', 503, 8),
(356, 1, 1, 2207, NULL, '2014-07-16', NULL, 1, 2, 'MATV system meeting', 'REQUEST_APPROVE', '2014-07-15 06:45:49', 'MATV system meeting', 491, 8),
(357, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '1118:BBA', 'REQUEST_APPROVE', '2014-07-15 06:49:35', '1118:BBA', 492, 8),
(358, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'Tel: 0814444311', 'REQUEST_APPROVE', '2014-07-15 18:30:58', 'Tel: 0814444311', 505, 8),
(359, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 06:53:44', 'Tel:0814444311', 493, 8),
(360, 1, 1, 3415, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 06:56:53', '', 494, 8),
(361, 1, 1, 2207, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 07:41:55', '', 496, 8),
(362, 1, 1, 3407, NULL, '2014-07-15', NULL, 1, 2, 'te;0805918753\ndivisioon;science\nroom requireents; any opter lab', 'REQUEST_APPROVE', '2014-07-15 07:34:34', 'te;0805918753\ndivisioon;science\nroom requireents; any opter lab', 495, 8),
(363, 1, 1, 2207, NULL, '2014-07-28', NULL, 1, 1, 'Science Division\nRoom Requirement: any meeting room', 'REQUEST_APPROVE', '2014-07-15 09:53:41', 'Science Division\nRoom Requirement: any meeting room', 498, 8),
(364, 1, 1, 3414, NULL, '2014-07-16', NULL, 1, 1, '1306 งานพัสดุ', 'REQUEST_APPROVE', '2014-07-15 08:35:01', '1306 งานพัสดุ', 497, 8),
(365, 1, 1, 3414, NULL, '2014-07-24', NULL, 1, 1, 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 'REQUEST_APPROVE', '2014-07-17 01:45:56', 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 649, 8),
(366, 1, 1, 3411, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 15:35:20', '', 500, 8),
(367, 1, 1, 3305, NULL, '2014-07-15', NULL, 6, 1, 'tel: 0814444311', 'REQUEST_APPROVE', '2014-07-15 18:21:30', 'tel: 0814444311', 501, 8),
(368, 1, 1, 3305, NULL, '2014-07-15', NULL, 1, 1, 'tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 18:23:35', 'tel:0814444311', 502, 8),
(369, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 1, 'Tel. 1201\nOffice of the Dean\nRoom : 1214 Dean meeting room', 'REQUEST_APPROVE', '2014-07-15 10:15:36', 'Tel. 1201\nOffice of the Dean\nRoom : 1214 Dean meeting room', 499, 8),
(370, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 18:28:19', 'tel:0814444311', 504, 8),
(371, 1, 1, 3414, NULL, '2014-07-19', NULL, 1, 1, 'Science Division\nRoom Requirement: Any meeting room', 'REQUEST_APPROVE', '2014-07-17 05:24:32', 'Science Division\nRoom Requirement: Any meeting room', 653, 8),
(372, 1, 1, 1214, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 01:35:55', '', 512, 8),
(373, 1, 1, 1214, NULL, '2014-07-18', NULL, 1, 1, 'มีการจัดเลี้ยงอาหารด้วยค่ะ ', 'REQUEST_APPROVE', '2014-07-18 02:20:52', 'มีการจัดเลี้ยงอาหารด้วยค่ะ ', 683, 8),
(374, 1, 1, 3303, NULL, '2014-07-18', NULL, 1, 2, 'Any room on the 3rd floor', 'REQUEST_APPROVE', '2014-07-17 01:55:09', 'Any room on the 3rd floor', 650, 8),
(375, 1, 1, 1210, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-18 02:49:35', '', 684, 8),
(376, 1, 1, 1214, NULL, '2014-07-23', NULL, 1, 1, '02-800-3574\nDivision : HLD\nRoom : 1214', 'REQUEST_APPROVE', '2014-07-22 03:31:39', '02-800-3574\nDivision : HLD\nRoom : 1214', 706, 8),
(377, 1, 1, 3315, NULL, '2014-07-25', NULL, 1, 1, 'การอบรมการใช้งานชุดทดสอบประสิทธิภาพการเรียนการสอน (Clicker)\nโดยวิทยากร คือ คุณสรวีร์ วรกุลพิทักษ์ ตัวแทนจาก บริษัท อินเตอร์เอ็ดดูเคชั่น ซัพพลายส์ จำกัด \nในวันศุกร์ที่ 25 กรกฎาคม พ.ศ. 2557 เวลา 10:00-11:00 น.  ห้อง 3415', 'REQUEST_APPROVE', '2014-07-22 10:35:22', 'การอบรมการใช้งานชุดทดสอบประสิทธิภาพการเรียนการสอน (Clicker)\nโดยวิทยากร คือ คุณสรวีร์ วรกุลพิทักษ์ ตัวแทนจาก บริษัท อินเตอร์เอ็ดดูเคชั่น ซัพพลายส์ จำกัด \nในวันศุกร์ที่ 25 กรกฎาคม พ.ศ. 2557 เวลา 10:00-11:00 น.  ห้อง 3415', 711, 8),
(378, 1, 1, 3306, NULL, '2014-07-18', NULL, 1, 1, 'Tel: 0899181232', 'REQUEST_APPROVE', '2014-07-17 07:49:35', 'Tel: 0899181232', 655, 8),
(379, 1, 1, 2207, NULL, '2014-07-29', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:08:17', '', 528, 8),
(380, 1, 1, 3407, NULL, '2014-07-18', NULL, 1, 2, 'tel;0805918753\ndivision;thm\nroom requirement;any computer lab', 'REQUEST_APPROVE', '2014-07-18 06:38:12', 'tel;0805918753\ndivision;thm\nroom requirement;any computer lab', 685, 8),
(381, 1, 1, 3414, NULL, '2014-07-22', NULL, 1, 1, 'Tel.3555\nIT\nRoom 3414', 'REQUEST_APPROVE', '2014-07-22 07:02:07', 'Tel.3555\nIT\nRoom 3414', 709, 8),
(382, 1, 1, 1210, NULL, '2014-07-25', NULL, 1, 2, 'งานบริหารกลาง\nขอความอนุเคราะห์จัดให้สำหรับให้มีการเสนอชื่อ เบื้องต้นได้พูดคุยกับคุณสิทธาแล้ว ขอบคุณมากค่ะ\n', 'REQUEST_APPROVE', '2014-07-22 05:45:47', 'งานบริหารกลาง\nขอความอนุเคราะห์จัดให้สำหรับให้มีการเสนอชื่อ เบื้องต้นได้พูดคุยกับคุณสิทธาแล้ว ขอบคุณมากค่ะ\n', 708, 8),
(383, 1, 1, 1314, NULL, '2014-07-25', NULL, 6, 1, '', 'REQUEST_APPROVE', '2014-07-25 13:28:22', '', 728, 8),
(384, 1, 1, 1214, NULL, '2014-07-28', NULL, 1, 1, 'Ext. 1708-9\nDivision: IA\nRoom Requirements: Meeting room: 1214', 'REQUEST_APPROVE', '2014-07-18 06:39:25', 'Ext. 1708-9\nDivision: IA\nRoom Requirements: Meeting room: 1214', 686, 8);
INSERT INTO `request_booking` (`id`, `user_login_id`, `request_booking_type_id`, `room_id`, `semester_id`, `request_date`, `request_day_in_week`, `period_start`, `period_end`, `description`, `status_code`, `create_date`, `course_name`, `request_sky_id`, `request_sky_noti_id`) VALUES
(385, 1, 1, 3414, NULL, '2014-07-28', NULL, 1, 1, 'ประชุมงานเทคโนโลยีการศึกษา \nในวันจันทร์ที่ 28 กรกฏาคม พ.ศ. 2557\nห้องประชุม 3414 เวลา 09:00 - 10:00 น. \n\n', 'REQUEST_APPROVE', '2014-07-22 07:59:24', 'ประชุมงานเทคโนโลยีการศึกษา \nในวันจันทร์ที่ 28 กรกฏาคม พ.ศ. 2557\nห้องประชุม 3414 เวลา 09:00 - 10:00 น. \n\n', 710, 8),
(386, 1, 1, 2207, NULL, '2014-07-29', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:06:51', '', 527, 8),
(387, 1, 1, 2302, NULL, '2014-07-28', NULL, 1, 1, 'Tel: 085-8288832\nRoom Requirement : Mirror\n                                    ขอห้องกระจกค่าาาา', 'REQUEST_APPROVE', '2014-07-27 15:30:15', 'Tel: 085-8288832\nRoom Requirement : Mirror\n                                    ขอห้องกระจกค่าาาา', 730, 8),
(388, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:22:40', '', 535, 8),
(389, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:24:34', '', 536, 8),
(390, 1, 1, 2207, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:27:04', '', 537, 8),
(391, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:49:49', '', 549, 8),
(392, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:51:44', '', 550, 8),
(393, 1, 1, 1307, NULL, '2014-07-22', NULL, 1, 1, '085-8288832\nRoom Request: Mirror (ต้องการห้องกระจกค่ะ)', 'REQUEST_APPROVE', '2014-07-21 16:35:34', '085-8288832\nRoom Request: Mirror (ต้องการห้องกระจกค่ะ)', 703, 8),
(394, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:57:01', '', 583, 8),
(395, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:53:05', '', 582, 8),
(396, 1, 1, 3414, NULL, '2014-07-25', NULL, 2, 2, 'Science Division', 'REQUEST_APPROVE', '2014-07-24 04:02:25', 'Science Division', 715, 8),
(397, 1, 1, 2303, NULL, '2014-07-24', NULL, 1, 1, '1118:BBA', 'REQUEST_APPROVE', '2014-07-24 04:04:15', '1118:BBA', 716, 8),
(398, 1, 1, 3305, NULL, '2014-07-18', NULL, 1, 1, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-16 03:35:23', 'Tel:0814444311', 570, 8),
(399, 1, 1, 1210, NULL, '2014-07-20', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:49:19', '', 580, 8),
(400, 1, 1, 5212, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-20 07:46:50', '', 693, 8),
(401, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 04:01:47', '', 586, 8),
(402, 1, 1, 1210, NULL, '2014-07-20', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:51:09', '', 581, 8),
(403, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:05:21', '', 590, 8),
(404, 1, 1, 2207, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:06:56', '', 591, 8),
(405, 1, 1, 2207, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:09:07', '', 592, 8),
(406, 1, 1, 2207, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:13:29', '', 593, 8),
(407, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:15:16', '', 594, 8),
(408, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:16:58', '', 595, 8),
(409, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:18:56', '', 596, 8),
(410, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:20:20', '', 597, 8),
(411, 1, 1, 1214, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:22:28', '', 598, 8),
(412, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:30:43', '', 603, 8),
(413, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:32:16', '', 604, 8),
(414, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:33:58', '', 605, 8),
(415, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:35:13', '', 606, 8),
(416, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:36:47', '', 607, 8),
(417, 1, 1, 1210, NULL, '2014-07-27', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:38:10', '', 608, 8),
(418, 1, 1, 1210, NULL, '2014-07-27', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:39:25', '', 609, 8),
(419, 1, 1, 1214, NULL, '2014-07-16', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:47:13', '', 610, 8),
(420, 1, 1, 1214, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:48:47', '', 611, 8),
(421, 1, 1, 3305, NULL, '2014-07-16', NULL, 2, 3, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-16 05:20:58', 'Tel:0814444311', 587, 8),
(422, 1, 1, 1408, NULL, '2014-07-17', NULL, 1, 1, 'Tel: 0832502220', 'REQUEST_APPROVE', '2014-07-16 05:40:56', 'Tel: 0832502220', 588, 8),
(423, 1, 1, 3410, NULL, '2014-07-17', NULL, 1, 1, 'Computer', 'REQUEST_APPROVE', '2014-07-16 08:13:19', 'Computer', 635, 8),
(424, 1, 1, 3412, NULL, '2014-07-17', NULL, 1, 1, 'Computer', 'REQUEST_APPROVE', '2014-07-16 08:14:52', 'Computer', 636, 8),
(425, 1, 1, 3317, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:18:46', '', 625, 8),
(426, 1, 1, 3407, NULL, '2014-07-16', NULL, 1, 2, 'Tel.0805918753\nDivision.science \nRoom requirements.any computer lab ', 'REQUEST_APPROVE', '2014-07-16 08:01:59', 'Tel.0805918753\nDivision.science \nRoom requirements.any computer lab ', 634, 8),
(427, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:50:26', '', 612, 8),
(428, 1, 1, 2207, NULL, '2014-07-21', NULL, 1, 1, 'Room 2207\n02-800 3560 \nHLD', 'REQUEST_APPROVE', '2014-07-21 02:26:42', 'Room 2207\n02-800 3560 \nHLD', 695, 8),
(429, 1, 1, 3414, NULL, '2014-07-25', NULL, 1, 1, 'Ext.1210/SA', 'REQUEST_APPROVE', '2014-07-24 05:53:48', 'Ext.1210/SA', 717, 8),
(430, 1, 1, 3411, NULL, '2014-07-17', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-16 07:05:54', '', 623, 8),
(431, 1, 1, 3411, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:06:13', '', 624, 8),
(432, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:51:57', '', 613, 8),
(433, 1, 1, 1210, NULL, '2014-07-23', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 07:58:12', '', 633, 8),
(434, 1, 1, 2307, NULL, '2014-07-25', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-07-17 09:56:24', '1745, BBA', 678, 8),
(435, 1, 1, 3409, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:04:47', '', 620, 8),
(436, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 07:05:33', '', 622, 8),
(437, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:01:50', '', 618, 8),
(438, 1, 1, 1318, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:48:15', '', 630, 8),
(439, 1, 1, 1318, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:50:33', '', 631, 8),
(440, 1, 1, 1210, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:53:09', '', 632, 8),
(441, 1, 1, 1318, NULL, '2014-07-22', NULL, 1, 3, 'Tel : 085-828-8832\nRequirements : 3 Microphones and Projector screen\n** 3days Reservation - 18,19,22 August 2014\n', 'REQUEST_APPROVE', '2014-07-18 08:21:14', 'Tel : 085-828-8832\nRequirements : 3 Microphones and Projector screen\n** 3days Reservation - 18,19,22 August 2014\n', 689, 8),
(442, 1, 1, 3420, NULL, '2014-07-17', NULL, 1, 2, 'Final Revision for Asian Philosophy\n(Computer+internet)', 'REQUEST_APPROVE', '2014-07-16 02:16:41', 'Final Revision for Asian Philosophy\n(Computer+internet)', 533, 8),
(443, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:00:02', '', 617, 8),
(444, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:04:27', '', 619, 8),
(445, 1, 1, 3414, NULL, '2014-07-29', NULL, 1, 1, '1305\nงานพัสดุ\n3414', 'REQUEST_APPROVE', '2014-07-24 09:28:26', '1305\nงานพัสดุ\n3414', 721, 8),
(446, 1, 1, 3421, NULL, '2014-07-30', NULL, 1, 1, 'Computer, Projector, Internet', 'REQUEST_APPROVE', '2014-07-29 14:29:21', 'Computer, Projector, Internet', 760, 8),
(447, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:32:28', '', 626, 8),
(448, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:05:14', '', 621, 8),
(449, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:32:51', '', 637, 8),
(450, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 2, 'OE, ext. 1106', 'REQUEST_APPROVE', '2014-07-24 09:39:22', 'OE, ext. 1106', 722, 8),
(451, 1, 1, 1214, NULL, '2014-07-29', NULL, 1, 1, 'Data Hub Meeting room 2207 ', 'REQUEST_APPROVE', '2014-07-16 08:51:13', 'Data Hub Meeting room 2207 ', 643, 8),
(452, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:34:42', '', 638, 8),
(453, 1, 1, 3414, NULL, '2014-07-18', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 08:38:12', '', 640, 8),
(454, 1, 1, 3414, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:36:35', '', 639, 8),
(455, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:40:15', '', 641, 8),
(456, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:43:04', '', 642, 8),
(457, 1, 1, 1210, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:53:16', '', 644, 8),
(458, 1, 1, 3409, NULL, '2014-07-18', NULL, 1, 1, 'ประชุมด่วน วาระพิเศษ (SLL) ', 'REQUEST_APPROVE', '2014-07-17 10:28:24', 'ประชุมด่วน วาระพิเศษ (SLL) ', 679, 8),
(459, 1, 1, 3414, NULL, '2014-07-24', NULL, 1, 1, '1305\nงานพัสดุ \nห้อง 3414', 'REQUEST_APPROVE', '2014-07-21 09:02:00', '1305\nงานพัสดุ \nห้อง 3414', 700, 8),
(460, 1, 1, 2307, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:00:26', '1745, BBA', 233, 7),
(461, 1, 1, 2307, NULL, '2014-07-19', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:08:25', '1745, BBA', 237, 7),
(462, 1, 1, 2307, NULL, '2014-07-14', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:10:45', '1745, BBA', 238, 7),
(463, 1, 1, 3306, NULL, '2014-07-17', NULL, 1, 2, 'Tel: 0899181232', 'REQUEST_APPROVE', '2014-07-16 10:31:21', 'Tel: 0899181232', 647, 7),
(464, 1, 1, 1417, NULL, '2014-07-28', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:04:43', '1745, BBA', 235, 7),
(465, 1, 1, 1417, NULL, '2014-07-29', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:06:00', '1745, BBA', 236, 7),
(466, 1, 1, 2307, NULL, '2014-07-23', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-25 06:55:35', '1745, BBA', 255, 7),
(467, 1, 1, 3302, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-27 01:58:22', '1745, BBA', 286, 7),
(468, 1, 1, 1404, NULL, '2014-07-07', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-06 06:49:17', '', 435, 7),
(469, 1, 1, 3414, NULL, '2014-07-30', NULL, 1, 1, 'งานพัสดุ 1306', 'REQUEST_APPROVE', '2014-07-28 08:44:15', 'งานพัสดุ 1306', 745, 7),
(470, 1, 1, 1318, NULL, '2014-07-09', NULL, 1, 3, 'I must use this room for our important final presentation. ', 'REQUEST_APPROVE', '2014-06-02 12:07:04', 'I must use this room for our important final presentation. ', 19, 7),
(471, 1, 1, 3410, NULL, '2014-07-18', NULL, 1, 1, 'Division: BBA\nTel: (66) 2-441-5090 ext. 1419\nE-mail: alessandro.sta@mahidol.ac.th\nRoom Requirements: Any room with projector and microphone', 'REQUEST_APPROVE', '2014-06-20 09:42:41', 'Division: BBA\nTel: (66) 2-441-5090 ext. 1419\nE-mail: alessandro.sta@mahidol.ac.th\nRoom Requirements: Any room with projector and microphone', 202, 7),
(472, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-20 03:28:40', '', 195, 7),
(473, 1, 1, 1307, NULL, '2014-07-09', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-06-23 11:49:25', '', 219, 7),
(474, 1, 1, 2308, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:02:27', '1745, BBA', 234, 7),
(475, 1, 1, -1, NULL, '2014-07-29', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-12 08:49:22', '', 99, 7),
(476, 1, 1, 3414, NULL, '2014-07-28', NULL, 1, 1, '1305 \nงานพัสดุ \nห้อง 1207', 'REQUEST_APPROVE', '2014-07-16 09:39:27', '1305 \nงานพัสดุ \nห้อง 1207', 645, 7),
(477, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:03:06', '', 589, 7),
(478, 1, 1, -1, NULL, '2014-07-28', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-12 08:46:19', '', 98, 7),
(479, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 2, 'IT ', 'REQUEST_APPROVE', '2014-07-18 01:13:42', 'IT ', 681, 7),
(480, 1, 1, 3421, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 16:57:22', '', 648, 7),
(481, 1, 1, 3412, NULL, '2014-07-08', NULL, 1, 1, '1511/ THM/ 3412', 'REQUEST_APPROVE', '2014-06-26 03:31:21', '1511/ THM/ 3412', 274, 7),
(482, 1, 1, 3414, NULL, '2014-07-22', NULL, 1, 1, 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 'REQUEST_APPROVE', '2014-07-16 02:56:37', 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 551, 7),
(483, 1, 1, 3316, NULL, '2014-07-17', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-07-02 12:20:17', '', 359, 7),
(484, 1, 1, 3304, NULL, '2014-07-17', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 09:01:33', 'prepare for exam', 478, 7),
(485, 1, 1, 3415, NULL, '2014-07-23', NULL, 1, 2, 'นักศึกษา ป.โท defense 23 ก.ค.57 เวลา 9.00 -18.00 น. ขอห้องให้ (3515)', 'REQUEST_APPROVE', '2014-07-18 01:08:28', 'นักศึกษา ป.โท defense 23 ก.ค.57 เวลา 9.00 -18.00 น. ขอห้องให้ (3515)', 680, 7),
(486, 1, 1, 3410, NULL, '2014-07-07', NULL, 1, 3, 'Tel: 0818252922\nDivision: Computer Science \nRoom Requirements: Any lecture room ', 'REQUEST_APPROVE', '2014-07-03 06:41:44', 'Tel: 0818252922\nDivision: Computer Science \nRoom Requirements: Any lecture room ', 395, 7),
(487, 1, 1, -1, NULL, '2014-07-07', NULL, 1, 2, 'Tel: 0827866709\nDivision:  \nRoom Requirements: Any room', 'REQUEST_APPROVE', '2014-07-03 12:02:19', 'Tel: 0827866709\nDivision:  \nRoom Requirements: Any room', 408, 7),
(488, 1, 1, 3420, NULL, '2014-07-24', NULL, 1, 1, 'Accord Visit (พลังงาน)  วันที่ 24 ก.ค.57 เวลา 14.00-16.00 น. (ขอใช้ห้องประชุม 2207)', 'REQUEST_APPROVE', '2014-07-18 01:13:50', 'Accord Visit (พลังงาน)  วันที่ 24 ก.ค.57 เวลา 14.00-16.00 น. (ขอใช้ห้องประชุม 2207)', 682, 7),
(489, 1, 1, 2207, NULL, '2014-07-30', NULL, 1, 1, 'ขอใช้ห้อง 2207ค่ะ', 'REQUEST_APPROVE', '2014-07-22 03:25:14', 'ขอใช้ห้อง 2207ค่ะ', 705, 7),
(490, 1, 1, 2207, NULL, '2014-07-30', NULL, 1, 1, 'Tel : 1761\nDivision : THM \nReserved by : Teerawan Nuntakij\nRoom Requirements : meeting room', 'REQUEST_APPROVE', '2014-07-25 03:25:08', 'Tel : 1761\nDivision : THM \nReserved by : Teerawan Nuntakij\nRoom Requirements : meeting room', 724, 7),
(491, 1, 1, 3414, NULL, '2014-07-23', NULL, 1, 1, 'BBA, 1745, Meeting room', 'REQUEST_APPROVE', '2014-07-17 06:09:22', 'BBA, 1745, Meeting room', 654, 7),
(492, 1, 1, 1418, NULL, '2014-07-07', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-04 10:10:11', '', 425, 7),
(493, 1, 1, 2302, NULL, '2014-07-07', NULL, 1, 1, 'room with mirrror', 'REQUEST_APPROVE', '2014-07-04 10:05:39', 'room with mirrror', 423, 7),
(494, 1, 1, 2302, NULL, '2014-07-07', NULL, 1, 3, 'any room with mirror', 'REQUEST_APPROVE', '2014-07-04 10:07:58', 'any room with mirror', 424, 7),
(495, 1, 1, 1314, NULL, '2014-07-07', NULL, 1, 2, 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Computer', 'REQUEST_APPROVE', '2014-07-04 13:32:35', 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Computer', 428, 7),
(496, 1, 1, 1302, NULL, '2014-07-07', NULL, 1, 1, 'Tel: 090-994-4285', 'REQUEST_APPROVE', '2014-07-05 09:33:46', 'Tel: 090-994-4285', 433, 7),
(497, 1, 1, 1406, NULL, '2014-07-07', NULL, 1, 3, 'tel; 0805918753\ndivisin;thm\nrequirements''ayrooms with omputer', 'REQUEST_APPROVE', '2014-07-07 07:36:32', 'tel; 0805918753\ndivisin;thm\nrequirements''ayrooms with omputer', 441, 7),
(498, 1, 1, 1302, NULL, '2014-07-07', NULL, 1, 1, 'Tel: 090-994-4285', 'REQUEST_APPROVE', '2014-07-05 09:29:14', 'Tel: 090-994-4285', 432, 7),
(499, 1, 1, 3415, NULL, '2014-07-07', NULL, 1, 1, 'Room requirements: using computer ', 'REQUEST_APPROVE', '2014-07-05 05:16:29', 'Room requirements: using computer ', 430, 7),
(500, 1, 1, 3408, NULL, '2014-07-07', NULL, 1, 3, 'Tel:0817200446\nRoom Requirement : Any rooms at IC building', 'REQUEST_APPROVE', '2014-07-07 05:33:29', 'Tel:0817200446\nRoom Requirement : Any rooms at IC building', 439, 7),
(501, 1, 1, 3306, NULL, '2014-07-07', NULL, 1, 2, '1118: BBA', 'REQUEST_APPROVE', '2014-07-07 03:07:20', '1118: BBA', 437, 7),
(502, 1, 1, 3411, NULL, '2014-07-07', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-07 05:19:51', '', 438, 7),
(503, 1, 1, 3412, NULL, '2014-07-07', NULL, 1, 2, '1745, BBA', 'REQUEST_APPROVE', '2014-07-07 07:12:28', '1745, BBA', 440, 7),
(504, 1, 1, 3411, NULL, '2014-07-09', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-08 10:40:25', '', 450, 7),
(505, 1, 1, 1304, NULL, '2014-07-09', NULL, 2, 3, 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Compute lap', 'REQUEST_APPROVE', '2014-07-07 10:23:13', 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Compute lap', 443, 7),
(506, 1, 1, 2302, NULL, '2014-07-09', NULL, 1, 2, 'Contact งานคุมสอบ', 'REQUEST_APPROVE', '2014-07-08 01:04:07', 'Contact งานคุมสอบ', 445, 7),
(507, 1, 1, 1312, NULL, '2014-07-27', NULL, 1, 1, 'Admissions\nTel: 1245\nUse for MUIC Entrance Examination ', 'REQUEST_APPROVE', '2014-07-08 07:58:03', 'Admissions\nTel: 1245\nUse for MUIC Entrance Examination ', 448, 7),
(508, 1, 1, 3412, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:54:54', 'Tel: 0909944285', 452, 7),
(509, 1, 1, 3315, NULL, '2014-07-09', NULL, 1, 3, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:56:18', 'Tel: 0909944285', 455, 7),
(510, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:55:20', 'Tel: 0909944285', 453, 7),
(511, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:55:48', 'Tel: 0909944285', 454, 7),
(512, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-08 10:38:46', '', 449, 7),
(513, 1, 1, 1418, NULL, '2014-07-26', NULL, 1, 2, 'Admissions\nTel: 1245\nUse for the MUIC Entrance Examination (ขอห้อง 1418-1419 ค่ะ)', 'REQUEST_APPROVE', '2014-07-08 07:54:47', 'Admissions\nTel: 1245\nUse for the MUIC Entrance Examination (ขอห้อง 1418-1419 ค่ะ)', 447, 7),
(514, 1, 1, 3410, NULL, '2014-07-15', NULL, 1, 1, 'BBA:1118', 'REQUEST_APPROVE', '2014-07-09 01:10:42', 'BBA:1118', 457, 7),
(515, 1, 1, -1, NULL, '2014-07-17', NULL, 1, 1, 'Faculty support services unit\nMrs. Sopa ext. 1125', 'REQUEST_APPROVE', '2014-07-09 03:09:18', 'Faculty support services unit\nMrs. Sopa ext. 1125', 459, 7),
(516, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 1, 'Faculty Support Services Unit\nMrs. Sopa Ext. 1125', 'REQUEST_APPROVE', '2014-07-09 03:24:03', 'Faculty Support Services Unit\nMrs. Sopa Ext. 1125', 460, 7),
(517, 1, 1, 3407, NULL, '2014-07-09', NULL, 2, 3, 'tel; 0805918753\ndivision; thm\nroom requirements; an cmpter lab', 'REQUEST_APPROVE', '2014-07-09 04:37:23', 'tel; 0805918753\ndivision; thm\nroom requirements; an cmpter lab', 461, 7),
(518, 1, 1, -1, NULL, '2014-07-16', NULL, 2, 3, 'Faculty Support Services Units\nMrs. Sopa ext 1125', 'REQUEST_APPROVE', '2014-07-10 06:30:45', 'Faculty Support Services Units\nMrs. Sopa ext 1125', 462, 7),
(519, 1, 1, -1, NULL, '2014-07-17', NULL, 2, 3, 'Faculty Support Services Units\nMrs. Sopa ext 1125', 'REQUEST_APPROVE', '2014-07-10 06:32:16', 'Faculty Support Services Units\nMrs. Sopa ext 1125', 463, 7),
(520, 1, 1, 3412, NULL, '2014-07-15', NULL, 1, 2, 'prepare exam', 'REQUEST_APPROVE', '2014-07-14 08:14:50', 'prepare exam', 475, 7),
(521, 1, 1, 1307, NULL, '2014-07-16', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 08:16:30', 'prepare for exam', 476, 7),
(522, 1, 1, 3410, NULL, '2014-07-17', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 08:18:04', 'prepare for exam', 477, 7),
(523, 1, 1, 3317, NULL, '2014-07-12', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-11 07:06:49', '', 467, 7),
(524, 1, 1, 3412, NULL, '2014-07-12', NULL, 1, 1, 'Room Requirements: Projector', 'REQUEST_APPROVE', '2014-07-11 11:18:52', 'Room Requirements: Projector', 468, 7),
(525, 1, 1, 3407, NULL, '2014-07-12', NULL, 2, 2, 'tel;0805918753\ndivision; thm\nroom  requirements; any room with  computer', 'REQUEST_APPROVE', '2014-07-12 06:16:16', 'tel;0805918753\ndivision; thm\nroom  requirements; any room with  computer', 470, 7),
(526, 1, 1, -1, NULL, '2014-07-16', NULL, 1, 1, 'Mrs. Sopa\nFaculty Support Units\next. 1125', 'REQUEST_APPROVE', '2014-07-12 06:31:53', 'Mrs. Sopa\nFaculty Support Units\next. 1125', 471, 7),
(527, 1, 1, 3410, NULL, '2014-07-14', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-07-13 05:48:20', '', 472, 7),
(528, 1, 1, 3407, NULL, '2014-07-14', NULL, 1, 2, 'tel;0805918753\ndivisioon;scieence\nroom  quirement; anny computer lab', 'REQUEST_APPROVE', '2014-07-14 06:16:10', 'tel;0805918753\ndivisioon;scieence\nroom  quirement; anny computer lab', 474, 7),
(529, 1, 1, 3306, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-14 09:39:27', '', 479, 7),
(530, 1, 1, 3410, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-14 09:40:14', '', 480, 7),
(531, 1, 1, 3317, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 02:59:35', '', 484, 7),
(532, 1, 1, 3302, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 03:00:12', '', 485, 7),
(533, 1, 1, 3410, NULL, '2014-07-16', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-15 03:01:25', '', 487, 7),
(534, 1, 1, 3302, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 03:00:47', '', 486, 7),
(535, 1, 1, 3306, NULL, '2014-07-15', NULL, 1, 1, 'BBA, 1745', 'REQUEST_APPROVE', '2014-07-15 03:01:49', 'BBA, 1745', 488, 7),
(536, 1, 1, 3305, NULL, '2014-07-16', NULL, 6, 1, 'tel:08144444311', 'REQUEST_APPROVE', '2014-07-15 18:26:47', 'tel:08144444311', 503, 7),
(537, 1, 1, 2207, NULL, '2014-07-16', NULL, 1, 2, 'MATV system meeting', 'REQUEST_APPROVE', '2014-07-15 06:45:49', 'MATV system meeting', 491, 7),
(538, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '1118:BBA', 'REQUEST_APPROVE', '2014-07-15 06:49:35', '1118:BBA', 492, 7),
(539, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'Tel: 0814444311', 'REQUEST_APPROVE', '2014-07-15 18:30:58', 'Tel: 0814444311', 505, 7),
(540, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 06:53:44', 'Tel:0814444311', 493, 7),
(541, 1, 1, 3415, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 06:56:53', '', 494, 7),
(542, 1, 1, 2207, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 07:41:55', '', 496, 7),
(543, 1, 1, 3407, NULL, '2014-07-15', NULL, 1, 2, 'te;0805918753\ndivisioon;science\nroom requireents; any opter lab', 'REQUEST_APPROVE', '2014-07-15 07:34:34', 'te;0805918753\ndivisioon;science\nroom requireents; any opter lab', 495, 7),
(544, 1, 1, 2207, NULL, '2014-07-28', NULL, 1, 1, 'Science Division\nRoom Requirement: any meeting room', 'REQUEST_APPROVE', '2014-07-15 09:53:41', 'Science Division\nRoom Requirement: any meeting room', 498, 7),
(545, 1, 1, 3414, NULL, '2014-07-16', NULL, 1, 1, '1306 งานพัสดุ', 'REQUEST_APPROVE', '2014-07-15 08:35:01', '1306 งานพัสดุ', 497, 7),
(546, 1, 1, 3414, NULL, '2014-07-24', NULL, 1, 1, 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 'REQUEST_APPROVE', '2014-07-17 01:45:56', 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 649, 7),
(547, 1, 1, 3411, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 15:35:20', '', 500, 7),
(548, 1, 1, 3305, NULL, '2014-07-15', NULL, 6, 1, 'tel: 0814444311', 'REQUEST_APPROVE', '2014-07-15 18:21:30', 'tel: 0814444311', 501, 7),
(549, 1, 1, 3305, NULL, '2014-07-15', NULL, 1, 1, 'tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 18:23:35', 'tel:0814444311', 502, 7),
(550, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 1, 'Tel. 1201\nOffice of the Dean\nRoom : 1214 Dean meeting room', 'REQUEST_APPROVE', '2014-07-15 10:15:36', 'Tel. 1201\nOffice of the Dean\nRoom : 1214 Dean meeting room', 499, 7),
(551, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 18:28:19', 'tel:0814444311', 504, 7),
(552, 1, 1, 3414, NULL, '2014-07-19', NULL, 1, 1, 'Science Division\nRoom Requirement: Any meeting room', 'REQUEST_APPROVE', '2014-07-17 05:24:32', 'Science Division\nRoom Requirement: Any meeting room', 653, 7),
(553, 1, 1, 1214, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 01:35:55', '', 512, 7),
(554, 1, 1, 1214, NULL, '2014-07-18', NULL, 1, 1, 'มีการจัดเลี้ยงอาหารด้วยค่ะ ', 'REQUEST_APPROVE', '2014-07-18 02:20:52', 'มีการจัดเลี้ยงอาหารด้วยค่ะ ', 683, 7),
(555, 1, 1, 3303, NULL, '2014-07-18', NULL, 1, 2, 'Any room on the 3rd floor', 'REQUEST_APPROVE', '2014-07-17 01:55:09', 'Any room on the 3rd floor', 650, 7),
(556, 1, 1, 1210, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-18 02:49:35', '', 684, 7),
(557, 1, 1, 1214, NULL, '2014-07-23', NULL, 1, 1, '02-800-3574\nDivision : HLD\nRoom : 1214', 'REQUEST_APPROVE', '2014-07-22 03:31:39', '02-800-3574\nDivision : HLD\nRoom : 1214', 706, 7),
(558, 1, 1, 3315, NULL, '2014-07-25', NULL, 1, 1, 'การอบรมการใช้งานชุดทดสอบประสิทธิภาพการเรียนการสอน (Clicker)\nโดยวิทยากร คือ คุณสรวีร์ วรกุลพิทักษ์ ตัวแทนจาก บริษัท อินเตอร์เอ็ดดูเคชั่น ซัพพลายส์ จำกัด \nในวันศุกร์ที่ 25 กรกฎาคม พ.ศ. 2557 เวลา 10:00-11:00 น.  ห้อง 3415', 'REQUEST_APPROVE', '2014-07-22 10:35:22', 'การอบรมการใช้งานชุดทดสอบประสิทธิภาพการเรียนการสอน (Clicker)\nโดยวิทยากร คือ คุณสรวีร์ วรกุลพิทักษ์ ตัวแทนจาก บริษัท อินเตอร์เอ็ดดูเคชั่น ซัพพลายส์ จำกัด \nในวันศุกร์ที่ 25 กรกฎาคม พ.ศ. 2557 เวลา 10:00-11:00 น.  ห้อง 3415', 711, 7),
(559, 1, 1, 3306, NULL, '2014-07-18', NULL, 1, 1, 'Tel: 0899181232', 'REQUEST_APPROVE', '2014-07-17 07:49:35', 'Tel: 0899181232', 655, 7),
(560, 1, 1, 2207, NULL, '2014-07-29', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:08:17', '', 528, 7),
(561, 1, 1, 3407, NULL, '2014-07-18', NULL, 1, 2, 'tel;0805918753\ndivision;thm\nroom requirement;any computer lab', 'REQUEST_APPROVE', '2014-07-18 06:38:12', 'tel;0805918753\ndivision;thm\nroom requirement;any computer lab', 685, 7),
(562, 1, 1, 3414, NULL, '2014-07-22', NULL, 1, 1, 'Tel.3555\nIT\nRoom 3414', 'REQUEST_APPROVE', '2014-07-22 07:02:07', 'Tel.3555\nIT\nRoom 3414', 709, 7),
(563, 1, 1, 1210, NULL, '2014-07-25', NULL, 1, 2, 'งานบริหารกลาง\nขอความอนุเคราะห์จัดให้สำหรับให้มีการเสนอชื่อ เบื้องต้นได้พูดคุยกับคุณสิทธาแล้ว ขอบคุณมากค่ะ\n', 'REQUEST_APPROVE', '2014-07-22 05:45:47', 'งานบริหารกลาง\nขอความอนุเคราะห์จัดให้สำหรับให้มีการเสนอชื่อ เบื้องต้นได้พูดคุยกับคุณสิทธาแล้ว ขอบคุณมากค่ะ\n', 708, 7),
(564, 1, 1, 1314, NULL, '2014-07-25', NULL, 6, 1, '', 'REQUEST_APPROVE', '2014-07-25 13:28:22', '', 728, 7),
(565, 1, 1, 1214, NULL, '2014-07-28', NULL, 1, 1, 'Ext. 1708-9\nDivision: IA\nRoom Requirements: Meeting room: 1214', 'REQUEST_APPROVE', '2014-07-18 06:39:25', 'Ext. 1708-9\nDivision: IA\nRoom Requirements: Meeting room: 1214', 686, 7),
(566, 1, 1, 3414, NULL, '2014-07-28', NULL, 1, 1, 'ประชุมงานเทคโนโลยีการศึกษา \nในวันจันทร์ที่ 28 กรกฏาคม พ.ศ. 2557\nห้องประชุม 3414 เวลา 09:00 - 10:00 น. \n\n', 'REQUEST_APPROVE', '2014-07-22 07:59:24', 'ประชุมงานเทคโนโลยีการศึกษา \nในวันจันทร์ที่ 28 กรกฏาคม พ.ศ. 2557\nห้องประชุม 3414 เวลา 09:00 - 10:00 น. \n\n', 710, 7),
(567, 1, 1, 2207, NULL, '2014-07-29', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:06:51', '', 527, 7),
(568, 1, 1, 2302, NULL, '2014-07-28', NULL, 1, 1, 'Tel: 085-8288832\nRoom Requirement : Mirror\n                                    ขอห้องกระจกค่าาาา', 'REQUEST_APPROVE', '2014-07-27 15:30:15', 'Tel: 085-8288832\nRoom Requirement : Mirror\n                                    ขอห้องกระจกค่าาาา', 730, 7),
(569, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:22:40', '', 535, 7),
(570, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:24:34', '', 536, 7),
(571, 1, 1, 2207, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:27:04', '', 537, 7),
(572, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:49:49', '', 549, 7),
(573, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:51:44', '', 550, 7),
(574, 1, 1, 1307, NULL, '2014-07-22', NULL, 1, 1, '085-8288832\nRoom Request: Mirror (ต้องการห้องกระจกค่ะ)', 'REQUEST_APPROVE', '2014-07-21 16:35:34', '085-8288832\nRoom Request: Mirror (ต้องการห้องกระจกค่ะ)', 703, 7),
(575, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:57:01', '', 583, 7),
(576, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:53:05', '', 582, 7),
(577, 1, 1, 3414, NULL, '2014-07-25', NULL, 2, 2, 'Science Division', 'REQUEST_APPROVE', '2014-07-24 04:02:25', 'Science Division', 715, 7),
(578, 1, 1, 2303, NULL, '2014-07-24', NULL, 1, 1, '1118:BBA', 'REQUEST_APPROVE', '2014-07-24 04:04:15', '1118:BBA', 716, 7),
(579, 1, 1, 3305, NULL, '2014-07-18', NULL, 1, 1, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-16 03:35:23', 'Tel:0814444311', 570, 7),
(580, 1, 1, 1210, NULL, '2014-07-20', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:49:19', '', 580, 7),
(581, 1, 1, 5212, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-20 07:46:50', '', 693, 7),
(582, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 04:01:47', '', 586, 7),
(583, 1, 1, 1210, NULL, '2014-07-20', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:51:09', '', 581, 7),
(584, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:05:21', '', 590, 7),
(585, 1, 1, 2207, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:06:56', '', 591, 7),
(586, 1, 1, 2207, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:09:07', '', 592, 7),
(587, 1, 1, 2207, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:13:29', '', 593, 7),
(588, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:15:16', '', 594, 7),
(589, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:16:58', '', 595, 7),
(590, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:18:56', '', 596, 7),
(591, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:20:20', '', 597, 7),
(592, 1, 1, 1214, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:22:28', '', 598, 7),
(593, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:30:43', '', 603, 7),
(594, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:32:16', '', 604, 7),
(595, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:33:58', '', 605, 7),
(596, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:35:13', '', 606, 7),
(597, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:36:47', '', 607, 7),
(598, 1, 1, 1210, NULL, '2014-07-27', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:38:10', '', 608, 7),
(599, 1, 1, 1210, NULL, '2014-07-27', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:39:25', '', 609, 7),
(600, 1, 1, 1214, NULL, '2014-07-16', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:47:13', '', 610, 7),
(601, 1, 1, 1214, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:48:47', '', 611, 7),
(602, 1, 1, 3305, NULL, '2014-07-16', NULL, 2, 3, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-16 05:20:58', 'Tel:0814444311', 587, 7),
(603, 1, 1, 1408, NULL, '2014-07-17', NULL, 1, 1, 'Tel: 0832502220', 'REQUEST_APPROVE', '2014-07-16 05:40:56', 'Tel: 0832502220', 588, 7),
(604, 1, 1, 3410, NULL, '2014-07-17', NULL, 1, 1, 'Computer', 'REQUEST_APPROVE', '2014-07-16 08:13:19', 'Computer', 635, 7),
(605, 1, 1, 3412, NULL, '2014-07-17', NULL, 1, 1, 'Computer', 'REQUEST_APPROVE', '2014-07-16 08:14:52', 'Computer', 636, 7),
(606, 1, 1, 3317, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:18:46', '', 625, 7),
(607, 1, 1, 3407, NULL, '2014-07-16', NULL, 1, 2, 'Tel.0805918753\nDivision.science \nRoom requirements.any computer lab ', 'REQUEST_APPROVE', '2014-07-16 08:01:59', 'Tel.0805918753\nDivision.science \nRoom requirements.any computer lab ', 634, 7),
(608, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:50:26', '', 612, 7),
(609, 1, 1, 2207, NULL, '2014-07-21', NULL, 1, 1, 'Room 2207\n02-800 3560 \nHLD', 'REQUEST_APPROVE', '2014-07-21 02:26:42', 'Room 2207\n02-800 3560 \nHLD', 695, 7),
(610, 1, 1, 3414, NULL, '2014-07-25', NULL, 1, 1, 'Ext.1210/SA', 'REQUEST_APPROVE', '2014-07-24 05:53:48', 'Ext.1210/SA', 717, 7),
(611, 1, 1, 3411, NULL, '2014-07-17', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-16 07:05:54', '', 623, 7),
(612, 1, 1, 3411, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:06:13', '', 624, 7),
(613, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:51:57', '', 613, 7),
(614, 1, 1, 1210, NULL, '2014-07-23', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 07:58:12', '', 633, 7),
(615, 1, 1, 2307, NULL, '2014-07-25', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-07-17 09:56:24', '1745, BBA', 678, 7),
(616, 1, 1, 3409, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:04:47', '', 620, 7),
(617, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 07:05:33', '', 622, 7),
(618, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:01:50', '', 618, 7),
(619, 1, 1, 1318, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:48:15', '', 630, 7),
(620, 1, 1, 1318, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:50:33', '', 631, 7),
(621, 1, 1, 1210, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:53:09', '', 632, 7),
(622, 1, 1, 1318, NULL, '2014-07-22', NULL, 1, 3, 'Tel : 085-828-8832\nRequirements : 3 Microphones and Projector screen\n** 3days Reservation - 18,19,22 August 2014\n', 'REQUEST_APPROVE', '2014-07-18 08:21:14', 'Tel : 085-828-8832\nRequirements : 3 Microphones and Projector screen\n** 3days Reservation - 18,19,22 August 2014\n', 689, 7),
(623, 1, 1, 3420, NULL, '2014-07-17', NULL, 1, 2, 'Final Revision for Asian Philosophy\n(Computer+internet)', 'REQUEST_APPROVE', '2014-07-16 02:16:41', 'Final Revision for Asian Philosophy\n(Computer+internet)', 533, 7),
(624, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:00:02', '', 617, 7),
(625, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:04:27', '', 619, 7),
(626, 1, 1, 3414, NULL, '2014-07-29', NULL, 1, 1, '1305\nงานพัสดุ\n3414', 'REQUEST_APPROVE', '2014-07-24 09:28:26', '1305\nงานพัสดุ\n3414', 721, 7),
(627, 1, 1, 3421, NULL, '2014-07-30', NULL, 1, 1, 'Computer, Projector, Internet', 'REQUEST_APPROVE', '2014-07-29 14:29:21', 'Computer, Projector, Internet', 760, 7),
(628, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:32:28', '', 626, 7),
(629, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:05:14', '', 621, 7),
(630, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:32:51', '', 637, 7),
(631, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 2, 'OE, ext. 1106', 'REQUEST_APPROVE', '2014-07-24 09:39:22', 'OE, ext. 1106', 722, 7),
(632, 1, 1, 1214, NULL, '2014-07-29', NULL, 1, 1, 'Data Hub Meeting room 2207 ', 'REQUEST_APPROVE', '2014-07-16 08:51:13', 'Data Hub Meeting room 2207 ', 643, 7),
(633, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:34:42', '', 638, 7),
(634, 1, 1, 3414, NULL, '2014-07-18', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 08:38:12', '', 640, 7),
(635, 1, 1, 3414, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:36:35', '', 639, 7),
(636, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:40:15', '', 641, 7),
(637, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:43:04', '', 642, 7),
(638, 1, 1, 1210, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:53:16', '', 644, 7),
(639, 1, 1, 3409, NULL, '2014-07-18', NULL, 1, 1, 'ประชุมด่วน วาระพิเศษ (SLL) ', 'REQUEST_APPROVE', '2014-07-17 10:28:24', 'ประชุมด่วน วาระพิเศษ (SLL) ', 679, 7),
(640, 1, 1, 3414, NULL, '2014-07-24', NULL, 1, 1, '1305\nงานพัสดุ \nห้อง 3414', 'REQUEST_APPROVE', '2014-07-21 09:02:00', '1305\nงานพัสดุ \nห้อง 3414', 700, 7),
(641, 1, 1, 2307, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:00:26', '1745, BBA', 233, 6),
(642, 1, 1, 2307, NULL, '2014-07-19', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:08:25', '1745, BBA', 237, 6),
(643, 1, 1, 2307, NULL, '2014-07-14', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:10:45', '1745, BBA', 238, 6),
(644, 1, 1, 3306, NULL, '2014-07-17', NULL, 1, 2, 'Tel: 0899181232', 'REQUEST_APPROVE', '2014-07-16 10:31:21', 'Tel: 0899181232', 647, 6),
(645, 1, 1, 1417, NULL, '2014-07-28', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:04:43', '1745, BBA', 235, 6),
(646, 1, 1, 1417, NULL, '2014-07-29', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:06:00', '1745, BBA', 236, 6),
(647, 1, 1, 2307, NULL, '2014-07-23', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-25 06:55:35', '1745, BBA', 255, 6),
(648, 1, 1, 3302, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-27 01:58:22', '1745, BBA', 286, 6),
(649, 1, 1, 1404, NULL, '2014-07-07', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-06 06:49:17', '', 435, 6),
(650, 1, 1, 3414, NULL, '2014-07-30', NULL, 1, 1, 'งานพัสดุ 1306', 'REQUEST_APPROVE', '2014-07-28 08:44:15', 'งานพัสดุ 1306', 745, 6),
(651, 1, 1, 1318, NULL, '2014-07-09', NULL, 1, 3, 'I must use this room for our important final presentation. ', 'REQUEST_APPROVE', '2014-06-02 12:07:04', 'I must use this room for our important final presentation. ', 19, 6),
(652, 1, 1, 3410, NULL, '2014-07-18', NULL, 1, 1, 'Division: BBA\nTel: (66) 2-441-5090 ext. 1419\nE-mail: alessandro.sta@mahidol.ac.th\nRoom Requirements: Any room with projector and microphone', 'REQUEST_APPROVE', '2014-06-20 09:42:41', 'Division: BBA\nTel: (66) 2-441-5090 ext. 1419\nE-mail: alessandro.sta@mahidol.ac.th\nRoom Requirements: Any room with projector and microphone', 202, 6),
(653, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-20 03:28:40', '', 195, 6),
(654, 1, 1, 1307, NULL, '2014-07-09', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-06-23 11:49:25', '', 219, 6),
(655, 1, 1, 2308, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:02:27', '1745, BBA', 234, 6),
(656, 1, 1, -1, NULL, '2014-07-29', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-12 08:49:22', '', 99, 6),
(657, 1, 1, 3414, NULL, '2014-07-28', NULL, 1, 1, '1305 \nงานพัสดุ \nห้อง 1207', 'REQUEST_APPROVE', '2014-07-16 09:39:27', '1305 \nงานพัสดุ \nห้อง 1207', 645, 6),
(658, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:03:06', '', 589, 6),
(659, 1, 1, -1, NULL, '2014-07-28', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-12 08:46:19', '', 98, 6),
(660, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 2, 'IT ', 'REQUEST_APPROVE', '2014-07-18 01:13:42', 'IT ', 681, 6),
(661, 1, 1, 3421, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 16:57:22', '', 648, 6),
(662, 1, 1, 3412, NULL, '2014-07-08', NULL, 1, 1, '1511/ THM/ 3412', 'REQUEST_APPROVE', '2014-06-26 03:31:21', '1511/ THM/ 3412', 274, 6),
(663, 1, 1, 3414, NULL, '2014-07-22', NULL, 1, 1, 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 'REQUEST_APPROVE', '2014-07-16 02:56:37', 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 551, 6),
(664, 1, 1, 3316, NULL, '2014-07-17', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-07-02 12:20:17', '', 359, 6),
(665, 1, 1, 3304, NULL, '2014-07-17', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 09:01:33', 'prepare for exam', 478, 6),
(666, 1, 1, 3415, NULL, '2014-07-23', NULL, 1, 2, 'นักศึกษา ป.โท defense 23 ก.ค.57 เวลา 9.00 -18.00 น. ขอห้องให้ (3515)', 'REQUEST_APPROVE', '2014-07-18 01:08:28', 'นักศึกษา ป.โท defense 23 ก.ค.57 เวลา 9.00 -18.00 น. ขอห้องให้ (3515)', 680, 6),
(667, 1, 1, 3410, NULL, '2014-07-07', NULL, 1, 3, 'Tel: 0818252922\nDivision: Computer Science \nRoom Requirements: Any lecture room ', 'REQUEST_APPROVE', '2014-07-03 06:41:44', 'Tel: 0818252922\nDivision: Computer Science \nRoom Requirements: Any lecture room ', 395, 6),
(668, 1, 1, -1, NULL, '2014-07-07', NULL, 1, 2, 'Tel: 0827866709\nDivision:  \nRoom Requirements: Any room', 'REQUEST_APPROVE', '2014-07-03 12:02:19', 'Tel: 0827866709\nDivision:  \nRoom Requirements: Any room', 408, 6),
(669, 1, 1, 3420, NULL, '2014-07-24', NULL, 1, 1, 'Accord Visit (พลังงาน)  วันที่ 24 ก.ค.57 เวลา 14.00-16.00 น. (ขอใช้ห้องประชุม 2207)', 'REQUEST_APPROVE', '2014-07-18 01:13:50', 'Accord Visit (พลังงาน)  วันที่ 24 ก.ค.57 เวลา 14.00-16.00 น. (ขอใช้ห้องประชุม 2207)', 682, 6),
(670, 1, 1, 2207, NULL, '2014-07-30', NULL, 1, 1, 'ขอใช้ห้อง 2207ค่ะ', 'REQUEST_APPROVE', '2014-07-22 03:25:14', 'ขอใช้ห้อง 2207ค่ะ', 705, 6),
(671, 1, 1, 2207, NULL, '2014-07-30', NULL, 1, 1, 'Tel : 1761\nDivision : THM \nReserved by : Teerawan Nuntakij\nRoom Requirements : meeting room', 'REQUEST_APPROVE', '2014-07-25 03:25:08', 'Tel : 1761\nDivision : THM \nReserved by : Teerawan Nuntakij\nRoom Requirements : meeting room', 724, 6),
(672, 1, 1, 3414, NULL, '2014-07-23', NULL, 1, 1, 'BBA, 1745, Meeting room', 'REQUEST_APPROVE', '2014-07-17 06:09:22', 'BBA, 1745, Meeting room', 654, 6),
(673, 1, 1, 1418, NULL, '2014-07-07', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-04 10:10:11', '', 425, 6),
(674, 1, 1, 2302, NULL, '2014-07-07', NULL, 1, 1, 'room with mirrror', 'REQUEST_APPROVE', '2014-07-04 10:05:39', 'room with mirrror', 423, 6),
(675, 1, 1, 2302, NULL, '2014-07-07', NULL, 1, 3, 'any room with mirror', 'REQUEST_APPROVE', '2014-07-04 10:07:58', 'any room with mirror', 424, 6),
(676, 1, 1, 1314, NULL, '2014-07-07', NULL, 1, 2, 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Computer', 'REQUEST_APPROVE', '2014-07-04 13:32:35', 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Computer', 428, 6),
(677, 1, 1, 1302, NULL, '2014-07-07', NULL, 1, 1, 'Tel: 090-994-4285', 'REQUEST_APPROVE', '2014-07-05 09:33:46', 'Tel: 090-994-4285', 433, 6),
(678, 1, 1, 1406, NULL, '2014-07-07', NULL, 1, 3, 'tel; 0805918753\ndivisin;thm\nrequirements''ayrooms with omputer', 'REQUEST_APPROVE', '2014-07-07 07:36:32', 'tel; 0805918753\ndivisin;thm\nrequirements''ayrooms with omputer', 441, 6),
(679, 1, 1, 1302, NULL, '2014-07-07', NULL, 1, 1, 'Tel: 090-994-4285', 'REQUEST_APPROVE', '2014-07-05 09:29:14', 'Tel: 090-994-4285', 432, 6),
(680, 1, 1, 3415, NULL, '2014-07-07', NULL, 1, 1, 'Room requirements: using computer ', 'REQUEST_APPROVE', '2014-07-05 05:16:29', 'Room requirements: using computer ', 430, 6),
(681, 1, 1, 3408, NULL, '2014-07-07', NULL, 1, 3, 'Tel:0817200446\nRoom Requirement : Any rooms at IC building', 'REQUEST_APPROVE', '2014-07-07 05:33:29', 'Tel:0817200446\nRoom Requirement : Any rooms at IC building', 439, 6),
(682, 1, 1, 3306, NULL, '2014-07-07', NULL, 1, 2, '1118: BBA', 'REQUEST_APPROVE', '2014-07-07 03:07:20', '1118: BBA', 437, 6),
(683, 1, 1, 3411, NULL, '2014-07-07', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-07 05:19:51', '', 438, 6),
(684, 1, 1, 3412, NULL, '2014-07-07', NULL, 1, 2, '1745, BBA', 'REQUEST_APPROVE', '2014-07-07 07:12:28', '1745, BBA', 440, 6),
(685, 1, 1, 3411, NULL, '2014-07-09', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-08 10:40:25', '', 450, 6),
(686, 1, 1, 1304, NULL, '2014-07-09', NULL, 2, 3, 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Compute lap', 'REQUEST_APPROVE', '2014-07-07 10:23:13', 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Compute lap', 443, 6),
(687, 1, 1, 2302, NULL, '2014-07-09', NULL, 1, 2, 'Contact งานคุมสอบ', 'REQUEST_APPROVE', '2014-07-08 01:04:07', 'Contact งานคุมสอบ', 445, 6),
(688, 1, 1, 1312, NULL, '2014-07-27', NULL, 1, 1, 'Admissions\nTel: 1245\nUse for MUIC Entrance Examination ', 'REQUEST_APPROVE', '2014-07-08 07:58:03', 'Admissions\nTel: 1245\nUse for MUIC Entrance Examination ', 448, 6),
(689, 1, 1, 3412, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:54:54', 'Tel: 0909944285', 452, 6),
(690, 1, 1, 3315, NULL, '2014-07-09', NULL, 1, 3, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:56:18', 'Tel: 0909944285', 455, 6),
(691, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:55:20', 'Tel: 0909944285', 453, 6),
(692, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:55:48', 'Tel: 0909944285', 454, 6),
(693, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-08 10:38:46', '', 449, 6),
(694, 1, 1, 1418, NULL, '2014-07-26', NULL, 1, 2, 'Admissions\nTel: 1245\nUse for the MUIC Entrance Examination (ขอห้อง 1418-1419 ค่ะ)', 'REQUEST_APPROVE', '2014-07-08 07:54:47', 'Admissions\nTel: 1245\nUse for the MUIC Entrance Examination (ขอห้อง 1418-1419 ค่ะ)', 447, 6),
(695, 1, 1, 3410, NULL, '2014-07-15', NULL, 1, 1, 'BBA:1118', 'REQUEST_APPROVE', '2014-07-09 01:10:42', 'BBA:1118', 457, 6),
(696, 1, 1, -1, NULL, '2014-07-17', NULL, 1, 1, 'Faculty support services unit\nMrs. Sopa ext. 1125', 'REQUEST_APPROVE', '2014-07-09 03:09:18', 'Faculty support services unit\nMrs. Sopa ext. 1125', 459, 6),
(697, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 1, 'Faculty Support Services Unit\nMrs. Sopa Ext. 1125', 'REQUEST_APPROVE', '2014-07-09 03:24:03', 'Faculty Support Services Unit\nMrs. Sopa Ext. 1125', 460, 6),
(698, 1, 1, 3407, NULL, '2014-07-09', NULL, 2, 3, 'tel; 0805918753\ndivision; thm\nroom requirements; an cmpter lab', 'REQUEST_APPROVE', '2014-07-09 04:37:23', 'tel; 0805918753\ndivision; thm\nroom requirements; an cmpter lab', 461, 6),
(699, 1, 1, -1, NULL, '2014-07-16', NULL, 2, 3, 'Faculty Support Services Units\nMrs. Sopa ext 1125', 'REQUEST_APPROVE', '2014-07-10 06:30:45', 'Faculty Support Services Units\nMrs. Sopa ext 1125', 462, 6),
(700, 1, 1, -1, NULL, '2014-07-17', NULL, 2, 3, 'Faculty Support Services Units\nMrs. Sopa ext 1125', 'REQUEST_APPROVE', '2014-07-10 06:32:16', 'Faculty Support Services Units\nMrs. Sopa ext 1125', 463, 6),
(701, 1, 1, 3412, NULL, '2014-07-15', NULL, 1, 2, 'prepare exam', 'REQUEST_APPROVE', '2014-07-14 08:14:50', 'prepare exam', 475, 6),
(702, 1, 1, 1307, NULL, '2014-07-16', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 08:16:30', 'prepare for exam', 476, 6);
INSERT INTO `request_booking` (`id`, `user_login_id`, `request_booking_type_id`, `room_id`, `semester_id`, `request_date`, `request_day_in_week`, `period_start`, `period_end`, `description`, `status_code`, `create_date`, `course_name`, `request_sky_id`, `request_sky_noti_id`) VALUES
(703, 1, 1, 3410, NULL, '2014-07-17', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 08:18:04', 'prepare for exam', 477, 6),
(704, 1, 1, 3317, NULL, '2014-07-12', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-11 07:06:49', '', 467, 6),
(705, 1, 1, 3412, NULL, '2014-07-12', NULL, 1, 1, 'Room Requirements: Projector', 'REQUEST_APPROVE', '2014-07-11 11:18:52', 'Room Requirements: Projector', 468, 6),
(706, 1, 1, 3407, NULL, '2014-07-12', NULL, 2, 2, 'tel;0805918753\ndivision; thm\nroom  requirements; any room with  computer', 'REQUEST_APPROVE', '2014-07-12 06:16:16', 'tel;0805918753\ndivision; thm\nroom  requirements; any room with  computer', 470, 6),
(707, 1, 1, -1, NULL, '2014-07-16', NULL, 1, 1, 'Mrs. Sopa\nFaculty Support Units\next. 1125', 'REQUEST_APPROVE', '2014-07-12 06:31:53', 'Mrs. Sopa\nFaculty Support Units\next. 1125', 471, 6),
(708, 1, 1, 3410, NULL, '2014-07-14', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-07-13 05:48:20', '', 472, 6),
(709, 1, 1, 3407, NULL, '2014-07-14', NULL, 1, 2, 'tel;0805918753\ndivisioon;scieence\nroom  quirement; anny computer lab', 'REQUEST_APPROVE', '2014-07-14 06:16:10', 'tel;0805918753\ndivisioon;scieence\nroom  quirement; anny computer lab', 474, 6),
(710, 1, 1, 3306, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-14 09:39:27', '', 479, 6),
(711, 1, 1, 3410, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-14 09:40:14', '', 480, 6),
(712, 1, 1, 3317, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 02:59:35', '', 484, 6),
(713, 1, 1, 3302, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 03:00:12', '', 485, 6),
(714, 1, 1, 3410, NULL, '2014-07-16', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-15 03:01:25', '', 487, 6),
(715, 1, 1, 3302, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 03:00:47', '', 486, 6),
(716, 1, 1, 3306, NULL, '2014-07-15', NULL, 1, 1, 'BBA, 1745', 'REQUEST_APPROVE', '2014-07-15 03:01:49', 'BBA, 1745', 488, 6),
(717, 1, 1, 3305, NULL, '2014-07-16', NULL, 6, 1, 'tel:08144444311', 'REQUEST_APPROVE', '2014-07-15 18:26:47', 'tel:08144444311', 503, 6),
(718, 1, 1, 2207, NULL, '2014-07-16', NULL, 1, 2, 'MATV system meeting', 'REQUEST_APPROVE', '2014-07-15 06:45:49', 'MATV system meeting', 491, 6),
(719, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '1118:BBA', 'REQUEST_APPROVE', '2014-07-15 06:49:35', '1118:BBA', 492, 6),
(720, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'Tel: 0814444311', 'REQUEST_APPROVE', '2014-07-15 18:30:58', 'Tel: 0814444311', 505, 6),
(721, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 06:53:44', 'Tel:0814444311', 493, 6),
(722, 1, 1, 3415, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 06:56:53', '', 494, 6),
(723, 1, 1, 2207, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 07:41:55', '', 496, 6),
(724, 1, 1, 3407, NULL, '2014-07-15', NULL, 1, 2, 'te;0805918753\ndivisioon;science\nroom requireents; any opter lab', 'REQUEST_APPROVE', '2014-07-15 07:34:34', 'te;0805918753\ndivisioon;science\nroom requireents; any opter lab', 495, 6),
(725, 1, 1, 2207, NULL, '2014-07-28', NULL, 1, 1, 'Science Division\nRoom Requirement: any meeting room', 'REQUEST_APPROVE', '2014-07-15 09:53:41', 'Science Division\nRoom Requirement: any meeting room', 498, 6),
(726, 1, 1, 3414, NULL, '2014-07-16', NULL, 1, 1, '1306 งานพัสดุ', 'REQUEST_APPROVE', '2014-07-15 08:35:01', '1306 งานพัสดุ', 497, 6),
(727, 1, 1, 3414, NULL, '2014-07-24', NULL, 1, 1, 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 'REQUEST_APPROVE', '2014-07-17 01:45:56', 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 649, 6),
(728, 1, 1, 3411, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 15:35:20', '', 500, 6),
(729, 1, 1, 3305, NULL, '2014-07-15', NULL, 6, 1, 'tel: 0814444311', 'REQUEST_APPROVE', '2014-07-15 18:21:30', 'tel: 0814444311', 501, 6),
(730, 1, 1, 3305, NULL, '2014-07-15', NULL, 1, 1, 'tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 18:23:35', 'tel:0814444311', 502, 6),
(731, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 1, 'Tel. 1201\nOffice of the Dean\nRoom : 1214 Dean meeting room', 'REQUEST_APPROVE', '2014-07-15 10:15:36', 'Tel. 1201\nOffice of the Dean\nRoom : 1214 Dean meeting room', 499, 6),
(732, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 18:28:19', 'tel:0814444311', 504, 6),
(733, 1, 1, 3414, NULL, '2014-07-19', NULL, 1, 1, 'Science Division\nRoom Requirement: Any meeting room', 'REQUEST_APPROVE', '2014-07-17 05:24:32', 'Science Division\nRoom Requirement: Any meeting room', 653, 6),
(734, 1, 1, 1214, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 01:35:55', '', 512, 6),
(735, 1, 1, 1214, NULL, '2014-07-18', NULL, 1, 1, 'มีการจัดเลี้ยงอาหารด้วยค่ะ ', 'REQUEST_APPROVE', '2014-07-18 02:20:52', 'มีการจัดเลี้ยงอาหารด้วยค่ะ ', 683, 6),
(736, 1, 1, 3303, NULL, '2014-07-18', NULL, 1, 2, 'Any room on the 3rd floor', 'REQUEST_APPROVE', '2014-07-17 01:55:09', 'Any room on the 3rd floor', 650, 6),
(737, 1, 1, 1210, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-18 02:49:35', '', 684, 6),
(738, 1, 1, 1214, NULL, '2014-07-23', NULL, 1, 1, '02-800-3574\nDivision : HLD\nRoom : 1214', 'REQUEST_APPROVE', '2014-07-22 03:31:39', '02-800-3574\nDivision : HLD\nRoom : 1214', 706, 6),
(739, 1, 1, 3315, NULL, '2014-07-25', NULL, 1, 1, 'การอบรมการใช้งานชุดทดสอบประสิทธิภาพการเรียนการสอน (Clicker)\nโดยวิทยากร คือ คุณสรวีร์ วรกุลพิทักษ์ ตัวแทนจาก บริษัท อินเตอร์เอ็ดดูเคชั่น ซัพพลายส์ จำกัด \nในวันศุกร์ที่ 25 กรกฎาคม พ.ศ. 2557 เวลา 10:00-11:00 น.  ห้อง 3415', 'REQUEST_APPROVE', '2014-07-22 10:35:22', 'การอบรมการใช้งานชุดทดสอบประสิทธิภาพการเรียนการสอน (Clicker)\nโดยวิทยากร คือ คุณสรวีร์ วรกุลพิทักษ์ ตัวแทนจาก บริษัท อินเตอร์เอ็ดดูเคชั่น ซัพพลายส์ จำกัด \nในวันศุกร์ที่ 25 กรกฎาคม พ.ศ. 2557 เวลา 10:00-11:00 น.  ห้อง 3415', 711, 6),
(740, 1, 1, 3306, NULL, '2014-07-18', NULL, 1, 1, 'Tel: 0899181232', 'REQUEST_APPROVE', '2014-07-17 07:49:35', 'Tel: 0899181232', 655, 6),
(741, 1, 1, 2207, NULL, '2014-07-29', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:08:17', '', 528, 6),
(742, 1, 1, 3407, NULL, '2014-07-18', NULL, 1, 2, 'tel;0805918753\ndivision;thm\nroom requirement;any computer lab', 'REQUEST_APPROVE', '2014-07-18 06:38:12', 'tel;0805918753\ndivision;thm\nroom requirement;any computer lab', 685, 6),
(743, 1, 1, 3414, NULL, '2014-07-22', NULL, 1, 1, 'Tel.3555\nIT\nRoom 3414', 'REQUEST_APPROVE', '2014-07-22 07:02:07', 'Tel.3555\nIT\nRoom 3414', 709, 6),
(744, 1, 1, 1210, NULL, '2014-07-25', NULL, 1, 2, 'งานบริหารกลาง\nขอความอนุเคราะห์จัดให้สำหรับให้มีการเสนอชื่อ เบื้องต้นได้พูดคุยกับคุณสิทธาแล้ว ขอบคุณมากค่ะ\n', 'REQUEST_APPROVE', '2014-07-22 05:45:47', 'งานบริหารกลาง\nขอความอนุเคราะห์จัดให้สำหรับให้มีการเสนอชื่อ เบื้องต้นได้พูดคุยกับคุณสิทธาแล้ว ขอบคุณมากค่ะ\n', 708, 6),
(745, 1, 1, 1314, NULL, '2014-07-25', NULL, 6, 1, '', 'REQUEST_APPROVE', '2014-07-25 13:28:22', '', 728, 6),
(746, 1, 1, 1214, NULL, '2014-07-28', NULL, 1, 1, 'Ext. 1708-9\nDivision: IA\nRoom Requirements: Meeting room: 1214', 'REQUEST_APPROVE', '2014-07-18 06:39:25', 'Ext. 1708-9\nDivision: IA\nRoom Requirements: Meeting room: 1214', 686, 6),
(747, 1, 1, 3414, NULL, '2014-07-28', NULL, 1, 1, 'ประชุมงานเทคโนโลยีการศึกษา \nในวันจันทร์ที่ 28 กรกฏาคม พ.ศ. 2557\nห้องประชุม 3414 เวลา 09:00 - 10:00 น. \n\n', 'REQUEST_APPROVE', '2014-07-22 07:59:24', 'ประชุมงานเทคโนโลยีการศึกษา \nในวันจันทร์ที่ 28 กรกฏาคม พ.ศ. 2557\nห้องประชุม 3414 เวลา 09:00 - 10:00 น. \n\n', 710, 6),
(748, 1, 1, 2207, NULL, '2014-07-29', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:06:51', '', 527, 6),
(749, 1, 1, 2302, NULL, '2014-07-28', NULL, 1, 1, 'Tel: 085-8288832\nRoom Requirement : Mirror\n                                    ขอห้องกระจกค่าาาา', 'REQUEST_APPROVE', '2014-07-27 15:30:15', 'Tel: 085-8288832\nRoom Requirement : Mirror\n                                    ขอห้องกระจกค่าาาา', 730, 6),
(750, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:22:40', '', 535, 6),
(751, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:24:34', '', 536, 6),
(752, 1, 1, 2207, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:27:04', '', 537, 6),
(753, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:49:49', '', 549, 6),
(754, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:51:44', '', 550, 6),
(755, 1, 1, 1307, NULL, '2014-07-22', NULL, 1, 1, '085-8288832\nRoom Request: Mirror (ต้องการห้องกระจกค่ะ)', 'REQUEST_APPROVE', '2014-07-21 16:35:34', '085-8288832\nRoom Request: Mirror (ต้องการห้องกระจกค่ะ)', 703, 6),
(756, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:57:01', '', 583, 6),
(757, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:53:05', '', 582, 6),
(758, 1, 1, 3414, NULL, '2014-07-25', NULL, 2, 2, 'Science Division', 'REQUEST_APPROVE', '2014-07-24 04:02:25', 'Science Division', 715, 6),
(759, 1, 1, 2303, NULL, '2014-07-24', NULL, 1, 1, '1118:BBA', 'REQUEST_APPROVE', '2014-07-24 04:04:15', '1118:BBA', 716, 6),
(760, 1, 1, 3305, NULL, '2014-07-18', NULL, 1, 1, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-16 03:35:23', 'Tel:0814444311', 570, 6),
(761, 1, 1, 1210, NULL, '2014-07-20', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:49:19', '', 580, 6),
(762, 1, 1, 5212, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-20 07:46:50', '', 693, 6),
(763, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 04:01:47', '', 586, 6),
(764, 1, 1, 1210, NULL, '2014-07-20', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:51:09', '', 581, 6),
(765, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:05:21', '', 590, 6),
(766, 1, 1, 2207, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:06:56', '', 591, 6),
(767, 1, 1, 2207, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:09:07', '', 592, 6),
(768, 1, 1, 2207, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:13:29', '', 593, 6),
(769, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:15:16', '', 594, 6),
(770, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:16:58', '', 595, 6),
(771, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:18:56', '', 596, 6),
(772, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:20:20', '', 597, 6),
(773, 1, 1, 1214, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:22:28', '', 598, 6),
(774, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:30:43', '', 603, 6),
(775, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:32:16', '', 604, 6),
(776, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:33:58', '', 605, 6),
(777, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:35:13', '', 606, 6),
(778, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:36:47', '', 607, 6),
(779, 1, 1, 1210, NULL, '2014-07-27', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:38:10', '', 608, 6),
(780, 1, 1, 1210, NULL, '2014-07-27', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:39:25', '', 609, 6),
(781, 1, 1, 1214, NULL, '2014-07-16', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:47:13', '', 610, 6),
(782, 1, 1, 1214, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:48:47', '', 611, 6),
(783, 1, 1, 3305, NULL, '2014-07-16', NULL, 2, 3, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-16 05:20:58', 'Tel:0814444311', 587, 6),
(784, 1, 1, 1408, NULL, '2014-07-17', NULL, 1, 1, 'Tel: 0832502220', 'REQUEST_APPROVE', '2014-07-16 05:40:56', 'Tel: 0832502220', 588, 6),
(785, 1, 1, 3410, NULL, '2014-07-17', NULL, 1, 1, 'Computer', 'REQUEST_APPROVE', '2014-07-16 08:13:19', 'Computer', 635, 6),
(786, 1, 1, 3412, NULL, '2014-07-17', NULL, 1, 1, 'Computer', 'REQUEST_APPROVE', '2014-07-16 08:14:52', 'Computer', 636, 6),
(787, 1, 1, 3317, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:18:46', '', 625, 6),
(788, 1, 1, 3407, NULL, '2014-07-16', NULL, 1, 2, 'Tel.0805918753\nDivision.science \nRoom requirements.any computer lab ', 'REQUEST_APPROVE', '2014-07-16 08:01:59', 'Tel.0805918753\nDivision.science \nRoom requirements.any computer lab ', 634, 6),
(789, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:50:26', '', 612, 6),
(790, 1, 1, 2207, NULL, '2014-07-21', NULL, 1, 1, 'Room 2207\n02-800 3560 \nHLD', 'REQUEST_APPROVE', '2014-07-21 02:26:42', 'Room 2207\n02-800 3560 \nHLD', 695, 6),
(791, 1, 1, 3414, NULL, '2014-07-25', NULL, 1, 1, 'Ext.1210/SA', 'REQUEST_APPROVE', '2014-07-24 05:53:48', 'Ext.1210/SA', 717, 6),
(792, 1, 1, 3411, NULL, '2014-07-17', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-16 07:05:54', '', 623, 6),
(793, 1, 1, 3411, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:06:13', '', 624, 6),
(794, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:51:57', '', 613, 6),
(795, 1, 1, 1210, NULL, '2014-07-23', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 07:58:12', '', 633, 6),
(796, 1, 1, 2307, NULL, '2014-07-25', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-07-17 09:56:24', '1745, BBA', 678, 6),
(797, 1, 1, 3409, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:04:47', '', 620, 6),
(798, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 07:05:33', '', 622, 6),
(799, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:01:50', '', 618, 6),
(800, 1, 1, 1318, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:48:15', '', 630, 6),
(801, 1, 1, 1318, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:50:33', '', 631, 6),
(802, 1, 1, 1210, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:53:09', '', 632, 6),
(803, 1, 1, 1318, NULL, '2014-07-22', NULL, 1, 3, 'Tel : 085-828-8832\nRequirements : 3 Microphones and Projector screen\n** 3days Reservation - 18,19,22 August 2014\n', 'REQUEST_APPROVE', '2014-07-18 08:21:14', 'Tel : 085-828-8832\nRequirements : 3 Microphones and Projector screen\n** 3days Reservation - 18,19,22 August 2014\n', 689, 6),
(804, 1, 1, 3420, NULL, '2014-07-17', NULL, 1, 2, 'Final Revision for Asian Philosophy\n(Computer+internet)', 'REQUEST_APPROVE', '2014-07-16 02:16:41', 'Final Revision for Asian Philosophy\n(Computer+internet)', 533, 6),
(805, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:00:02', '', 617, 6),
(806, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:04:27', '', 619, 6),
(807, 1, 1, 3414, NULL, '2014-07-29', NULL, 1, 1, '1305\nงานพัสดุ\n3414', 'REQUEST_APPROVE', '2014-07-24 09:28:26', '1305\nงานพัสดุ\n3414', 721, 6),
(808, 1, 1, 3421, NULL, '2014-07-30', NULL, 1, 1, 'Computer, Projector, Internet', 'REQUEST_APPROVE', '2014-07-29 14:29:21', 'Computer, Projector, Internet', 760, 6),
(809, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:32:28', '', 626, 6),
(810, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:05:14', '', 621, 6),
(811, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:32:51', '', 637, 6),
(812, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 2, 'OE, ext. 1106', 'REQUEST_APPROVE', '2014-07-24 09:39:22', 'OE, ext. 1106', 722, 6),
(813, 1, 1, 1214, NULL, '2014-07-29', NULL, 1, 1, 'Data Hub Meeting room 2207 ', 'REQUEST_APPROVE', '2014-07-16 08:51:13', 'Data Hub Meeting room 2207 ', 643, 6),
(814, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:34:42', '', 638, 6),
(815, 1, 1, 3414, NULL, '2014-07-18', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 08:38:12', '', 640, 6),
(816, 1, 1, 3414, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:36:35', '', 639, 6),
(817, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:40:15', '', 641, 6),
(818, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:43:04', '', 642, 6),
(819, 1, 1, 1210, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 08:53:16', '', 644, 6),
(820, 1, 1, 3409, NULL, '2014-07-18', NULL, 1, 1, 'ประชุมด่วน วาระพิเศษ (SLL) ', 'REQUEST_APPROVE', '2014-07-17 10:28:24', 'ประชุมด่วน วาระพิเศษ (SLL) ', 679, 6),
(821, 1, 1, 3414, NULL, '2014-07-24', NULL, 1, 1, '1305\nงานพัสดุ \nห้อง 3414', 'REQUEST_APPROVE', '2014-07-21 09:02:00', '1305\nงานพัสดุ \nห้อง 3414', 700, 6),
(822, 1, 1, 2307, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:00:26', '1745, BBA', 233, 5),
(823, 1, 1, 2307, NULL, '2014-07-19', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:08:25', '1745, BBA', 237, 5),
(824, 1, 1, 2307, NULL, '2014-07-14', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:10:45', '1745, BBA', 238, 5),
(825, 1, 1, 3306, NULL, '2014-07-17', NULL, 1, 2, 'Tel: 0899181232', 'REQUEST_APPROVE', '2014-07-16 10:31:21', 'Tel: 0899181232', 647, 5),
(826, 1, 1, 1417, NULL, '2014-07-28', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:04:43', '1745, BBA', 235, 5),
(827, 1, 1, 1417, NULL, '2014-07-29', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:06:00', '1745, BBA', 236, 5),
(828, 1, 1, 2307, NULL, '2014-07-23', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-25 06:55:35', '1745, BBA', 255, 5),
(829, 1, 1, 3302, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-27 01:58:22', '1745, BBA', 286, 5),
(830, 1, 1, 1404, NULL, '2014-07-07', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-06 06:49:17', '', 435, 5),
(831, 1, 1, 3414, NULL, '2014-07-30', NULL, 1, 1, 'งานพัสดุ 1306', 'REQUEST_APPROVE', '2014-07-28 08:44:15', 'งานพัสดุ 1306', 745, 5),
(832, 1, 1, 1318, NULL, '2014-07-09', NULL, 1, 3, 'I must use this room for our important final presentation. ', 'REQUEST_APPROVE', '2014-06-02 12:07:04', 'I must use this room for our important final presentation. ', 19, 5),
(833, 1, 1, 3410, NULL, '2014-07-18', NULL, 1, 1, 'Division: BBA\nTel: (66) 2-441-5090 ext. 1419\nE-mail: alessandro.sta@mahidol.ac.th\nRoom Requirements: Any room with projector and microphone', 'REQUEST_APPROVE', '2014-06-20 09:42:41', 'Division: BBA\nTel: (66) 2-441-5090 ext. 1419\nE-mail: alessandro.sta@mahidol.ac.th\nRoom Requirements: Any room with projector and microphone', 202, 5),
(834, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-20 03:28:40', '', 195, 5),
(835, 1, 1, 1307, NULL, '2014-07-09', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-06-23 11:49:25', '', 219, 5),
(836, 1, 1, 2308, NULL, '2014-07-15', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-06-24 10:02:27', '1745, BBA', 234, 5),
(837, 1, 1, -1, NULL, '2014-07-29', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-12 08:49:22', '', 99, 5),
(838, 1, 1, 3414, NULL, '2014-07-28', NULL, 1, 1, '1305 \nงานพัสดุ \nห้อง 1207', 'REQUEST_APPROVE', '2014-07-16 09:39:27', '1305 \nงานพัสดุ \nห้อง 1207', 645, 5),
(839, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:03:06', '', 589, 5),
(840, 1, 1, -1, NULL, '2014-07-28', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-06-12 08:46:19', '', 98, 5),
(841, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 2, 'IT ', 'REQUEST_APPROVE', '2014-07-18 01:13:42', 'IT ', 681, 5),
(842, 1, 1, 3421, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 16:57:22', '', 648, 5),
(843, 1, 1, 3412, NULL, '2014-07-08', NULL, 1, 1, '1511/ THM/ 3412', 'REQUEST_APPROVE', '2014-06-26 03:31:21', '1511/ THM/ 3412', 274, 5),
(844, 1, 1, 3414, NULL, '2014-07-22', NULL, 1, 1, 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 'REQUEST_APPROVE', '2014-07-16 02:56:37', 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 551, 5),
(845, 1, 1, 3316, NULL, '2014-07-17', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-07-02 12:20:17', '', 359, 5),
(846, 1, 1, 3304, NULL, '2014-07-17', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 09:01:33', 'prepare for exam', 478, 5),
(847, 1, 1, 3415, NULL, '2014-07-23', NULL, 1, 2, 'นักศึกษา ป.โท defense 23 ก.ค.57 เวลา 9.00 -18.00 น. ขอห้องให้ (3515)', 'REQUEST_APPROVE', '2014-07-18 01:08:28', 'นักศึกษา ป.โท defense 23 ก.ค.57 เวลา 9.00 -18.00 น. ขอห้องให้ (3515)', 680, 5),
(848, 1, 1, 3410, NULL, '2014-07-07', NULL, 1, 3, 'Tel: 0818252922\nDivision: Computer Science \nRoom Requirements: Any lecture room ', 'REQUEST_APPROVE', '2014-07-03 06:41:44', 'Tel: 0818252922\nDivision: Computer Science \nRoom Requirements: Any lecture room ', 395, 5),
(849, 1, 1, -1, NULL, '2014-07-07', NULL, 1, 2, 'Tel: 0827866709\nDivision:  \nRoom Requirements: Any room', 'REQUEST_APPROVE', '2014-07-03 12:02:19', 'Tel: 0827866709\nDivision:  \nRoom Requirements: Any room', 408, 5),
(850, 1, 1, 3420, NULL, '2014-07-24', NULL, 1, 1, 'Accord Visit (พลังงาน)  วันที่ 24 ก.ค.57 เวลา 14.00-16.00 น. (ขอใช้ห้องประชุม 2207)', 'REQUEST_APPROVE', '2014-07-18 01:13:50', 'Accord Visit (พลังงาน)  วันที่ 24 ก.ค.57 เวลา 14.00-16.00 น. (ขอใช้ห้องประชุม 2207)', 682, 5),
(851, 1, 1, 2207, NULL, '2014-07-30', NULL, 1, 1, 'ขอใช้ห้อง 2207ค่ะ', 'REQUEST_APPROVE', '2014-07-22 03:25:14', 'ขอใช้ห้อง 2207ค่ะ', 705, 5),
(852, 1, 1, 2207, NULL, '2014-07-30', NULL, 1, 1, 'Tel : 1761\nDivision : THM \nReserved by : Teerawan Nuntakij\nRoom Requirements : meeting room', 'REQUEST_APPROVE', '2014-07-25 03:25:08', 'Tel : 1761\nDivision : THM \nReserved by : Teerawan Nuntakij\nRoom Requirements : meeting room', 724, 5),
(853, 1, 1, 3414, NULL, '2014-07-23', NULL, 1, 1, 'BBA, 1745, Meeting room', 'REQUEST_APPROVE', '2014-07-17 06:09:22', 'BBA, 1745, Meeting room', 654, 5),
(854, 1, 1, 1418, NULL, '2014-07-07', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-04 10:10:11', '', 425, 5),
(855, 1, 1, 2302, NULL, '2014-07-07', NULL, 1, 1, 'room with mirrror', 'REQUEST_APPROVE', '2014-07-04 10:05:39', 'room with mirrror', 423, 5),
(856, 1, 1, 2302, NULL, '2014-07-07', NULL, 1, 3, 'any room with mirror', 'REQUEST_APPROVE', '2014-07-04 10:07:58', 'any room with mirror', 424, 5),
(857, 1, 1, 1314, NULL, '2014-07-07', NULL, 1, 2, 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Computer', 'REQUEST_APPROVE', '2014-07-04 13:32:35', 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Computer', 428, 5),
(858, 1, 1, 1302, NULL, '2014-07-07', NULL, 1, 1, 'Tel: 090-994-4285', 'REQUEST_APPROVE', '2014-07-05 09:33:46', 'Tel: 090-994-4285', 433, 5),
(859, 1, 1, 1406, NULL, '2014-07-07', NULL, 1, 3, 'tel; 0805918753\ndivisin;thm\nrequirements''ayrooms with omputer', 'REQUEST_APPROVE', '2014-07-07 07:36:32', 'tel; 0805918753\ndivisin;thm\nrequirements''ayrooms with omputer', 441, 5),
(860, 1, 1, 1302, NULL, '2014-07-07', NULL, 1, 1, 'Tel: 090-994-4285', 'REQUEST_APPROVE', '2014-07-05 09:29:14', 'Tel: 090-994-4285', 432, 5),
(861, 1, 1, 3415, NULL, '2014-07-07', NULL, 1, 1, 'Room requirements: using computer ', 'REQUEST_APPROVE', '2014-07-05 05:16:29', 'Room requirements: using computer ', 430, 5),
(862, 1, 1, 3408, NULL, '2014-07-07', NULL, 1, 3, 'Tel:0817200446\nRoom Requirement : Any rooms at IC building', 'REQUEST_APPROVE', '2014-07-07 05:33:29', 'Tel:0817200446\nRoom Requirement : Any rooms at IC building', 439, 5),
(863, 1, 1, 3306, NULL, '2014-07-07', NULL, 1, 2, '1118: BBA', 'REQUEST_APPROVE', '2014-07-07 03:07:20', '1118: BBA', 437, 5),
(864, 1, 1, 3411, NULL, '2014-07-07', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-07 05:19:51', '', 438, 5),
(865, 1, 1, 3412, NULL, '2014-07-07', NULL, 1, 2, '1745, BBA', 'REQUEST_APPROVE', '2014-07-07 07:12:28', '1745, BBA', 440, 5),
(866, 1, 1, 3411, NULL, '2014-07-09', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-08 10:40:25', '', 450, 5),
(867, 1, 1, 1304, NULL, '2014-07-09', NULL, 2, 3, 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Compute lap', 'REQUEST_APPROVE', '2014-07-07 10:23:13', 'Tel: 0867565252\nDivision: THM\nRoom Requirements: Compute lap', 443, 5),
(868, 1, 1, 2302, NULL, '2014-07-09', NULL, 1, 2, 'Contact งานคุมสอบ', 'REQUEST_APPROVE', '2014-07-08 01:04:07', 'Contact งานคุมสอบ', 445, 5),
(869, 1, 1, 1312, NULL, '2014-07-27', NULL, 1, 1, 'Admissions\nTel: 1245\nUse for MUIC Entrance Examination ', 'REQUEST_APPROVE', '2014-07-08 07:58:03', 'Admissions\nTel: 1245\nUse for MUIC Entrance Examination ', 448, 5),
(870, 1, 1, 3412, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:54:54', 'Tel: 0909944285', 452, 5),
(871, 1, 1, 3315, NULL, '2014-07-09', NULL, 1, 3, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:56:18', 'Tel: 0909944285', 455, 5),
(872, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:55:20', 'Tel: 0909944285', 453, 5),
(873, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, 'Tel: 0909944285', 'REQUEST_APPROVE', '2014-07-08 12:55:48', 'Tel: 0909944285', 454, 5),
(874, 1, 1, 3304, NULL, '2014-07-09', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-08 10:38:46', '', 449, 5),
(875, 1, 1, 1418, NULL, '2014-07-26', NULL, 1, 2, 'Admissions\nTel: 1245\nUse for the MUIC Entrance Examination (ขอห้อง 1418-1419 ค่ะ)', 'REQUEST_APPROVE', '2014-07-08 07:54:47', 'Admissions\nTel: 1245\nUse for the MUIC Entrance Examination (ขอห้อง 1418-1419 ค่ะ)', 447, 5),
(876, 1, 1, 3410, NULL, '2014-07-15', NULL, 1, 1, 'BBA:1118', 'REQUEST_APPROVE', '2014-07-09 01:10:42', 'BBA:1118', 457, 5),
(877, 1, 1, -1, NULL, '2014-07-17', NULL, 1, 1, 'Faculty support services unit\nMrs. Sopa ext. 1125', 'REQUEST_APPROVE', '2014-07-09 03:09:18', 'Faculty support services unit\nMrs. Sopa ext. 1125', 459, 5),
(878, 1, 1, -1, NULL, '2014-07-18', NULL, 1, 1, 'Faculty Support Services Unit\nMrs. Sopa Ext. 1125', 'REQUEST_APPROVE', '2014-07-09 03:24:03', 'Faculty Support Services Unit\nMrs. Sopa Ext. 1125', 460, 5),
(879, 1, 1, 3407, NULL, '2014-07-09', NULL, 2, 3, 'tel; 0805918753\ndivision; thm\nroom requirements; an cmpter lab', 'REQUEST_APPROVE', '2014-07-09 04:37:23', 'tel; 0805918753\ndivision; thm\nroom requirements; an cmpter lab', 461, 5),
(880, 1, 1, -1, NULL, '2014-07-16', NULL, 2, 3, 'Faculty Support Services Units\nMrs. Sopa ext 1125', 'REQUEST_APPROVE', '2014-07-10 06:30:45', 'Faculty Support Services Units\nMrs. Sopa ext 1125', 462, 5),
(881, 1, 1, -1, NULL, '2014-07-17', NULL, 2, 3, 'Faculty Support Services Units\nMrs. Sopa ext 1125', 'REQUEST_APPROVE', '2014-07-10 06:32:16', 'Faculty Support Services Units\nMrs. Sopa ext 1125', 463, 5),
(882, 1, 1, 3412, NULL, '2014-07-15', NULL, 1, 2, 'prepare exam', 'REQUEST_APPROVE', '2014-07-14 08:14:50', 'prepare exam', 475, 5),
(883, 1, 1, 1307, NULL, '2014-07-16', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 08:16:30', 'prepare for exam', 476, 5),
(884, 1, 1, 3410, NULL, '2014-07-17', NULL, 1, 2, 'prepare for exam', 'REQUEST_APPROVE', '2014-07-14 08:18:04', 'prepare for exam', 477, 5),
(885, 1, 1, 3317, NULL, '2014-07-12', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-11 07:06:49', '', 467, 5),
(886, 1, 1, 3412, NULL, '2014-07-12', NULL, 1, 1, 'Room Requirements: Projector', 'REQUEST_APPROVE', '2014-07-11 11:18:52', 'Room Requirements: Projector', 468, 5),
(887, 1, 1, 3407, NULL, '2014-07-12', NULL, 2, 2, 'tel;0805918753\ndivision; thm\nroom  requirements; any room with  computer', 'REQUEST_APPROVE', '2014-07-12 06:16:16', 'tel;0805918753\ndivision; thm\nroom  requirements; any room with  computer', 470, 5),
(888, 1, 1, -1, NULL, '2014-07-16', NULL, 1, 1, 'Mrs. Sopa\nFaculty Support Units\next. 1125', 'REQUEST_APPROVE', '2014-07-12 06:31:53', 'Mrs. Sopa\nFaculty Support Units\next. 1125', 471, 5),
(889, 1, 1, 3410, NULL, '2014-07-14', NULL, 1, 3, '', 'REQUEST_APPROVE', '2014-07-13 05:48:20', '', 472, 5),
(890, 1, 1, 3407, NULL, '2014-07-14', NULL, 1, 2, 'tel;0805918753\ndivisioon;scieence\nroom  quirement; anny computer lab', 'REQUEST_APPROVE', '2014-07-14 06:16:10', 'tel;0805918753\ndivisioon;scieence\nroom  quirement; anny computer lab', 474, 5),
(891, 1, 1, 3306, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-14 09:39:27', '', 479, 5),
(892, 1, 1, 3410, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-14 09:40:14', '', 480, 5),
(893, 1, 1, 3317, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 02:59:35', '', 484, 5),
(894, 1, 1, 3302, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 03:00:12', '', 485, 5),
(895, 1, 1, 3410, NULL, '2014-07-16', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-15 03:01:25', '', 487, 5),
(896, 1, 1, 3302, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 03:00:47', '', 486, 5),
(897, 1, 1, 3306, NULL, '2014-07-15', NULL, 1, 1, 'BBA, 1745', 'REQUEST_APPROVE', '2014-07-15 03:01:49', 'BBA, 1745', 488, 5),
(898, 1, 1, 3305, NULL, '2014-07-16', NULL, 6, 1, 'tel:08144444311', 'REQUEST_APPROVE', '2014-07-15 18:26:47', 'tel:08144444311', 503, 5),
(899, 1, 1, 2207, NULL, '2014-07-16', NULL, 1, 2, 'MATV system meeting', 'REQUEST_APPROVE', '2014-07-15 06:45:49', 'MATV system meeting', 491, 5),
(900, 1, 1, 2207, NULL, '2014-07-23', NULL, 1, 1, '1118:BBA', 'REQUEST_APPROVE', '2014-07-15 06:49:35', '1118:BBA', 492, 5),
(901, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'Tel: 0814444311', 'REQUEST_APPROVE', '2014-07-15 18:30:58', 'Tel: 0814444311', 505, 5),
(902, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 06:53:44', 'Tel:0814444311', 493, 5),
(903, 1, 1, 3415, NULL, '2014-07-15', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 06:56:53', '', 494, 5),
(904, 1, 1, 2207, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 07:41:55', '', 496, 5),
(905, 1, 1, 3407, NULL, '2014-07-15', NULL, 1, 2, 'te;0805918753\ndivisioon;science\nroom requireents; any opter lab', 'REQUEST_APPROVE', '2014-07-15 07:34:34', 'te;0805918753\ndivisioon;science\nroom requireents; any opter lab', 495, 5),
(906, 1, 1, 2207, NULL, '2014-07-28', NULL, 1, 1, 'Science Division\nRoom Requirement: any meeting room', 'REQUEST_APPROVE', '2014-07-15 09:53:41', 'Science Division\nRoom Requirement: any meeting room', 498, 5),
(907, 1, 1, 3414, NULL, '2014-07-16', NULL, 1, 1, '1306 งานพัสดุ', 'REQUEST_APPROVE', '2014-07-15 08:35:01', '1306 งานพัสดุ', 497, 5),
(908, 1, 1, 3414, NULL, '2014-07-24', NULL, 1, 1, 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 'REQUEST_APPROVE', '2014-07-17 01:45:56', 'Tel: Ext.1116\nDivision: PR\nRoom Requirement: -', 649, 5),
(909, 1, 1, 3411, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-15 15:35:20', '', 500, 5),
(910, 1, 1, 3305, NULL, '2014-07-15', NULL, 6, 1, 'tel: 0814444311', 'REQUEST_APPROVE', '2014-07-15 18:21:30', 'tel: 0814444311', 501, 5),
(911, 1, 1, 3305, NULL, '2014-07-15', NULL, 1, 1, 'tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 18:23:35', 'tel:0814444311', 502, 5),
(912, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 1, 'Tel. 1201\nOffice of the Dean\nRoom : 1214 Dean meeting room', 'REQUEST_APPROVE', '2014-07-15 10:15:36', 'Tel. 1201\nOffice of the Dean\nRoom : 1214 Dean meeting room', 499, 5),
(913, 1, 1, 3305, NULL, '2014-07-16', NULL, 1, 1, 'tel:0814444311', 'REQUEST_APPROVE', '2014-07-15 18:28:19', 'tel:0814444311', 504, 5),
(914, 1, 1, 3414, NULL, '2014-07-19', NULL, 1, 1, 'Science Division\nRoom Requirement: Any meeting room', 'REQUEST_APPROVE', '2014-07-17 05:24:32', 'Science Division\nRoom Requirement: Any meeting room', 653, 5),
(915, 1, 1, 1214, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 01:35:55', '', 512, 5),
(916, 1, 1, 1214, NULL, '2014-07-18', NULL, 1, 1, 'มีการจัดเลี้ยงอาหารด้วยค่ะ ', 'REQUEST_APPROVE', '2014-07-18 02:20:52', 'มีการจัดเลี้ยงอาหารด้วยค่ะ ', 683, 5),
(917, 1, 1, 3303, NULL, '2014-07-18', NULL, 1, 2, 'Any room on the 3rd floor', 'REQUEST_APPROVE', '2014-07-17 01:55:09', 'Any room on the 3rd floor', 650, 5),
(918, 1, 1, 1210, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-18 02:49:35', '', 684, 5),
(919, 1, 1, 1214, NULL, '2014-07-23', NULL, 1, 1, '02-800-3574\nDivision : HLD\nRoom : 1214', 'REQUEST_APPROVE', '2014-07-22 03:31:39', '02-800-3574\nDivision : HLD\nRoom : 1214', 706, 5),
(920, 1, 1, 3315, NULL, '2014-07-25', NULL, 1, 1, 'การอบรมการใช้งานชุดทดสอบประสิทธิภาพการเรียนการสอน (Clicker)\nโดยวิทยากร คือ คุณสรวีร์ วรกุลพิทักษ์ ตัวแทนจาก บริษัท อินเตอร์เอ็ดดูเคชั่น ซัพพลายส์ จำกัด \nในวันศุกร์ที่ 25 กรกฎาคม พ.ศ. 2557 เวลา 10:00-11:00 น.  ห้อง 3415', 'REQUEST_APPROVE', '2014-07-22 10:35:22', 'การอบรมการใช้งานชุดทดสอบประสิทธิภาพการเรียนการสอน (Clicker)\nโดยวิทยากร คือ คุณสรวีร์ วรกุลพิทักษ์ ตัวแทนจาก บริษัท อินเตอร์เอ็ดดูเคชั่น ซัพพลายส์ จำกัด \nในวันศุกร์ที่ 25 กรกฎาคม พ.ศ. 2557 เวลา 10:00-11:00 น.  ห้อง 3415', 711, 5),
(921, 1, 1, 3306, NULL, '2014-07-18', NULL, 1, 1, 'Tel: 0899181232', 'REQUEST_APPROVE', '2014-07-17 07:49:35', 'Tel: 0899181232', 655, 5),
(922, 1, 1, 2207, NULL, '2014-07-29', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:08:17', '', 528, 5),
(923, 1, 1, 3407, NULL, '2014-07-18', NULL, 1, 2, 'tel;0805918753\ndivision;thm\nroom requirement;any computer lab', 'REQUEST_APPROVE', '2014-07-18 06:38:12', 'tel;0805918753\ndivision;thm\nroom requirement;any computer lab', 685, 5),
(924, 1, 1, 3414, NULL, '2014-07-22', NULL, 1, 1, 'Tel.3555\nIT\nRoom 3414', 'REQUEST_APPROVE', '2014-07-22 07:02:07', 'Tel.3555\nIT\nRoom 3414', 709, 5),
(925, 1, 1, 1210, NULL, '2014-07-25', NULL, 1, 2, 'งานบริหารกลาง\nขอความอนุเคราะห์จัดให้สำหรับให้มีการเสนอชื่อ เบื้องต้นได้พูดคุยกับคุณสิทธาแล้ว ขอบคุณมากค่ะ\n', 'REQUEST_APPROVE', '2014-07-22 05:45:47', 'งานบริหารกลาง\nขอความอนุเคราะห์จัดให้สำหรับให้มีการเสนอชื่อ เบื้องต้นได้พูดคุยกับคุณสิทธาแล้ว ขอบคุณมากค่ะ\n', 708, 5),
(926, 1, 1, 1314, NULL, '2014-07-25', NULL, 6, 1, '', 'REQUEST_APPROVE', '2014-07-25 13:28:22', '', 728, 5),
(927, 1, 1, 1214, NULL, '2014-07-28', NULL, 1, 1, 'Ext. 1708-9\nDivision: IA\nRoom Requirements: Meeting room: 1214', 'REQUEST_APPROVE', '2014-07-18 06:39:25', 'Ext. 1708-9\nDivision: IA\nRoom Requirements: Meeting room: 1214', 686, 5),
(928, 1, 1, 3414, NULL, '2014-07-28', NULL, 1, 1, 'ประชุมงานเทคโนโลยีการศึกษา \nในวันจันทร์ที่ 28 กรกฏาคม พ.ศ. 2557\nห้องประชุม 3414 เวลา 09:00 - 10:00 น. \n\n', 'REQUEST_APPROVE', '2014-07-22 07:59:24', 'ประชุมงานเทคโนโลยีการศึกษา \nในวันจันทร์ที่ 28 กรกฏาคม พ.ศ. 2557\nห้องประชุม 3414 เวลา 09:00 - 10:00 น. \n\n', 710, 5),
(929, 1, 1, 2207, NULL, '2014-07-29', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:06:51', '', 527, 5),
(930, 1, 1, 2302, NULL, '2014-07-28', NULL, 1, 1, 'Tel: 085-8288832\nRoom Requirement : Mirror\n                                    ขอห้องกระจกค่าาาา', 'REQUEST_APPROVE', '2014-07-27 15:30:15', 'Tel: 085-8288832\nRoom Requirement : Mirror\n                                    ขอห้องกระจกค่าาาา', 730, 5),
(931, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:22:40', '', 535, 5),
(932, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:24:34', '', 536, 5),
(933, 1, 1, 2207, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:27:04', '', 537, 5),
(934, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:49:49', '', 549, 5),
(935, 1, 1, 2207, NULL, '2014-07-25', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 02:51:44', '', 550, 5),
(936, 1, 1, 1307, NULL, '2014-07-22', NULL, 1, 1, '085-8288832\nRoom Request: Mirror (ต้องการห้องกระจกค่ะ)', 'REQUEST_APPROVE', '2014-07-21 16:35:34', '085-8288832\nRoom Request: Mirror (ต้องการห้องกระจกค่ะ)', 703, 5),
(937, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:57:01', '', 583, 5),
(938, 1, 1, 1214, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:53:05', '', 582, 5),
(939, 1, 1, 3414, NULL, '2014-07-25', NULL, 2, 2, 'Science Division', 'REQUEST_APPROVE', '2014-07-24 04:02:25', 'Science Division', 715, 5),
(940, 1, 1, 2303, NULL, '2014-07-24', NULL, 1, 1, '1118:BBA', 'REQUEST_APPROVE', '2014-07-24 04:04:15', '1118:BBA', 716, 5),
(941, 1, 1, 3305, NULL, '2014-07-18', NULL, 1, 1, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-16 03:35:23', 'Tel:0814444311', 570, 5),
(942, 1, 1, 1210, NULL, '2014-07-20', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:49:19', '', 580, 5),
(943, 1, 1, 5212, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-20 07:46:50', '', 693, 5),
(944, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 04:01:47', '', 586, 5),
(945, 1, 1, 1210, NULL, '2014-07-20', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 03:51:09', '', 581, 5),
(946, 1, 1, 2207, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:05:21', '', 590, 5),
(947, 1, 1, 2207, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:06:56', '', 591, 5),
(948, 1, 1, 2207, NULL, '2014-07-24', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:09:07', '', 592, 5),
(949, 1, 1, 2207, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:13:29', '', 593, 5),
(950, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:15:16', '', 594, 5),
(951, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:16:58', '', 595, 5),
(952, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:18:56', '', 596, 5),
(953, 1, 1, 1214, NULL, '2014-07-21', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:20:20', '', 597, 5),
(954, 1, 1, 1214, NULL, '2014-07-22', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:22:28', '', 598, 5),
(955, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:30:43', '', 603, 5),
(956, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:32:16', '', 604, 5),
(957, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:33:58', '', 605, 5),
(958, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:35:13', '', 606, 5),
(959, 1, 1, 1210, NULL, '2014-07-26', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:36:47', '', 607, 5),
(960, 1, 1, 1210, NULL, '2014-07-27', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:38:10', '', 608, 5),
(961, 1, 1, 1210, NULL, '2014-07-27', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:39:25', '', 609, 5),
(962, 1, 1, 1214, NULL, '2014-07-16', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 06:47:13', '', 610, 5),
(963, 1, 1, 1214, NULL, '2014-07-16', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:48:47', '', 611, 5),
(964, 1, 1, 3305, NULL, '2014-07-16', NULL, 2, 3, 'Tel:0814444311', 'REQUEST_APPROVE', '2014-07-16 05:20:58', 'Tel:0814444311', 587, 5),
(965, 1, 1, 1408, NULL, '2014-07-17', NULL, 1, 1, 'Tel: 0832502220', 'REQUEST_APPROVE', '2014-07-16 05:40:56', 'Tel: 0832502220', 588, 5),
(966, 1, 1, 3410, NULL, '2014-07-17', NULL, 1, 1, 'Computer', 'REQUEST_APPROVE', '2014-07-16 08:13:19', 'Computer', 635, 5),
(967, 1, 1, 3412, NULL, '2014-07-17', NULL, 1, 1, 'Computer', 'REQUEST_APPROVE', '2014-07-16 08:14:52', 'Computer', 636, 5),
(968, 1, 1, 3317, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:18:46', '', 625, 5),
(969, 1, 1, 3407, NULL, '2014-07-16', NULL, 1, 2, 'Tel.0805918753\nDivision.science \nRoom requirements.any computer lab ', 'REQUEST_APPROVE', '2014-07-16 08:01:59', 'Tel.0805918753\nDivision.science \nRoom requirements.any computer lab ', 634, 5),
(970, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:50:26', '', 612, 5),
(971, 1, 1, 2207, NULL, '2014-07-21', NULL, 1, 1, 'Room 2207\n02-800 3560 \nHLD', 'REQUEST_APPROVE', '2014-07-21 02:26:42', 'Room 2207\n02-800 3560 \nHLD', 695, 5),
(972, 1, 1, 3414, NULL, '2014-07-25', NULL, 1, 1, 'Ext.1210/SA', 'REQUEST_APPROVE', '2014-07-24 05:53:48', 'Ext.1210/SA', 717, 5),
(973, 1, 1, 3411, NULL, '2014-07-17', NULL, 2, 3, '', 'REQUEST_APPROVE', '2014-07-16 07:05:54', '', 623, 5),
(974, 1, 1, 3411, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:06:13', '', 624, 5),
(975, 1, 1, 2207, NULL, '2014-07-18', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 06:51:57', '', 613, 5),
(976, 1, 1, 1210, NULL, '2014-07-23', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 07:58:12', '', 633, 5),
(977, 1, 1, 2307, NULL, '2014-07-25', NULL, 1, 1, '1745, BBA', 'REQUEST_APPROVE', '2014-07-17 09:56:24', '1745, BBA', 678, 5),
(978, 1, 1, 3409, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:04:47', '', 620, 5),
(979, 1, 1, 3411, NULL, '2014-07-17', NULL, 1, 2, '', 'REQUEST_APPROVE', '2014-07-16 07:05:33', '', 622, 5),
(980, 1, 1, 3414, NULL, '2014-07-17', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:01:50', '', 618, 5),
(981, 1, 1, 1318, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:48:15', '', 630, 5),
(982, 1, 1, 1318, NULL, '2014-07-21', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:50:33', '', 631, 5),
(983, 1, 1, 1210, NULL, '2014-07-23', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-07-16 07:53:09', '', 632, 5);

-- --------------------------------------------------------

--
-- Table structure for table `request_booking_activity_detail`
--

CREATE TABLE IF NOT EXISTS `request_booking_activity_detail` (
`id` int(11) NOT NULL,
  `event_type_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1024 ;

--
-- Dumping data for table `request_booking_activity_detail`
--

INSERT INTO `request_booking_activity_detail` (`id`, `event_type_id`, `room_type_id`, `description`, `file_path`) VALUES
(86, 1, 1, 'test', 'upload/files/1410075878/File.docx'),
(87, 2, 1, 'dd', ''),
(95, 1, 1, '123', ''),
(97, 1, 2, '1456', ''),
(1003, 2, 1, '', ''),
(1023, 3, 2, 'xx', '');

-- --------------------------------------------------------

--
-- Table structure for table `request_booking_activity_present_type`
--

CREATE TABLE IF NOT EXISTS `request_booking_activity_present_type` (
`id` int(11) NOT NULL,
  `request_booking_activity_id` int(11) NOT NULL,
  `present_type_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `request_booking_activity_present_type`
--

INSERT INTO `request_booking_activity_present_type` (`id`, `request_booking_activity_id`, `present_type_id`) VALUES
(40, 86, 1),
(41, 86, 2),
(42, 87, 1),
(43, 95, 1),
(44, 97, 1),
(45, 1003, 3),
(46, 1003, 4),
(47, 1023, 1),
(48, 1023, 2);

-- --------------------------------------------------------

--
-- Table structure for table `request_booking_alert_log`
--

CREATE TABLE IF NOT EXISTS `request_booking_alert_log` (
`id` int(11) NOT NULL,
  `request_booking_id` int(11) NOT NULL,
  `alert_date` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `request_booking_alert_log`
--

INSERT INTO `request_booking_alert_log` (`id`, `request_booking_id`, `alert_date`) VALUES
(6, 87, '2014-09-08'),
(7, 86, '2014-09-23');

-- --------------------------------------------------------

--
-- Table structure for table `request_booking_equipment_type`
--

CREATE TABLE IF NOT EXISTS `request_booking_equipment_type` (
`id` int(11) NOT NULL,
  `request_booking_id` int(11) NOT NULL,
  `equipment_type_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=290 ;

--
-- Dumping data for table `request_booking_equipment_type`
--

INSERT INTO `request_booking_equipment_type` (`id`, `request_booking_id`, `equipment_type_id`, `quantity`) VALUES
(217, 84, 4, NULL),
(218, 84, 5, NULL),
(219, 85, 4, NULL),
(220, 85, 5, NULL),
(221, 86, 4, 1),
(222, 86, 7, 1),
(223, 87, 8, 1),
(224, 88, 4, NULL),
(225, 88, 8, NULL),
(226, 88, 9, NULL),
(227, 89, 4, NULL),
(228, 89, 8, NULL),
(229, 89, 9, NULL),
(230, 90, 4, NULL),
(231, 90, 5, NULL),
(232, 91, 4, NULL),
(233, 91, 6, NULL),
(234, 92, 5, NULL),
(235, 92, 6, NULL),
(236, 93, 5, NULL),
(237, 93, 6, NULL),
(238, 94, 4, NULL),
(239, 94, 8, NULL),
(240, 94, 9, NULL),
(241, 95, 5, 1),
(242, 95, 6, 1),
(243, 95, 7, 1),
(244, 96, 6, NULL),
(245, 96, 8, NULL),
(246, 96, 9, NULL),
(247, 97, 7, 1),
(248, 97, 8, 1),
(249, 1003, 24, 1),
(250, 1004, 8, NULL),
(251, 1004, 9, NULL),
(252, 1005, 4, NULL),
(253, 1005, 5, NULL),
(254, 1006, 4, NULL),
(255, 1006, 5, NULL),
(256, 1007, 4, NULL),
(257, 1007, 5, NULL),
(258, 1008, 4, NULL),
(259, 1008, 5, NULL),
(260, 1009, 4, NULL),
(261, 1009, 5, NULL),
(262, 1010, 4, NULL),
(263, 1010, 5, NULL),
(264, 1011, 4, NULL),
(265, 1011, 5, NULL),
(266, 1012, 4, NULL),
(267, 1012, 5, NULL),
(268, 1013, 4, NULL),
(269, 1013, 5, NULL),
(270, 1014, 4, NULL),
(271, 1014, 5, NULL),
(272, 1015, 4, NULL),
(273, 1015, 5, NULL),
(274, 1016, 4, NULL),
(275, 1016, 5, NULL),
(276, 1017, 4, NULL),
(277, 1017, 5, NULL),
(278, 1018, 4, NULL),
(279, 1018, 5, NULL),
(280, 1019, 4, NULL),
(281, 1019, 5, NULL),
(282, 1020, 4, NULL),
(283, 1020, 5, NULL),
(284, 1021, 4, NULL),
(285, 1021, 5, NULL),
(286, 1022, 4, NULL),
(287, 1022, 5, NULL),
(288, 1023, 3, 1),
(289, 1024, 9, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_booking_type`
--

CREATE TABLE IF NOT EXISTS `request_booking_type` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `request_booking_type`
--

INSERT INTO `request_booking_type` (`id`, `name`, `description`) VALUES
(1, 'Daily', ''),
(2, 'Semester', ''),
(3, 'Activity / Meeting', '');

-- --------------------------------------------------------

--
-- Table structure for table `request_borrow`
--

CREATE TABLE IF NOT EXISTS `request_borrow` (
`id` int(11) NOT NULL,
  `DocumentNo` varchar(20) NOT NULL,
  `user_login_id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `approve_by` varchar(255) NOT NULL,
  `chef_email` varchar(255) NOT NULL,
  `event_type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `from_date` date NOT NULL,
  `thru_date` date NOT NULL,
  `status_code` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `request_borrow`
--

INSERT INTO `request_borrow` (`id`, `DocumentNo`, `user_login_id`, `location`, `approve_by`, `chef_email`, `event_type_id`, `description`, `from_date`, `thru_date`, `status_code`, `create_date`) VALUES
(87, '1400000001', 1, 'WHITHIN_MUIC', '-1', '', 1, 'test', '2014-12-24', '2014-12-26', 'R_B_NEW_RETURNED', '2014-12-21 12:40:09'),
(88, '1400000002', 244, 'WHITHIN_MUIC', '', '', 1, 'immuic', '2014-12-23', '2014-12-25', 'R_B_NEW_PREPARE', '2014-12-21 14:37:18'),
(89, '1400000003', 241, 'WHITHIN_MUIC', '-1', '', 1, 'ยืมใช้งานในวันเดียว', '2014-12-23', '2014-12-23', 'R_B_NEW_PREPARE', '2014-12-21 14:41:40'),
(90, '1400000004', 241, 'WHITHIN_MUIC', '', '', 3, 'ยืมใช้งานมากกว่า 1 วัน', '2014-12-23', '2014-12-24', 'R_B_NEW_PREPARE', '2014-12-21 14:42:13'),
(91, '1400000005', 241, 'WITHOUT_MUIC', '', '', 2, 'ยืมใช้งานในวันเดียว แต่ใช้ภายนอก', '2014-12-24', '2014-12-24', 'R_B_NEW_DISAPPROVE', '2014-12-21 14:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `request_borrow_alert_log`
--

CREATE TABLE IF NOT EXISTS `request_borrow_alert_log` (
`id` int(11) NOT NULL,
  `request_borrow_id` int(11) NOT NULL,
  `alert_date` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `request_borrow_approve_link`
--

CREATE TABLE IF NOT EXISTS `request_borrow_approve_link` (
`id` int(11) NOT NULL,
  `request_key` varchar(255) NOT NULL,
  `request_borrow_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `approve_type` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=86 ;

-- --------------------------------------------------------

--
-- Table structure for table `request_borrow_equipment_type`
--

CREATE TABLE IF NOT EXISTS `request_borrow_equipment_type` (
`id` int(11) NOT NULL,
  `request_borrow_id` int(11) NOT NULL,
  `equipment_type_list_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `request_borrow_equipment_type`
--

INSERT INTO `request_borrow_equipment_type` (`id`, `request_borrow_id`, `equipment_type_list_id`, `quantity`) VALUES
(17, 87, 564, 1),
(18, 88, 522, 1),
(19, 88, 566, 1),
(20, 89, 628, 1),
(21, 90, 530, 1),
(22, 91, 619, 1);

-- --------------------------------------------------------

--
-- Table structure for table `request_borrow_equipment_type_item`
--

CREATE TABLE IF NOT EXISTS `request_borrow_equipment_type_item` (
`id` int(11) NOT NULL,
  `request_borrow_equipment_type_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `return_date` varchar(255) DEFAULT NULL,
  `return_price` double NOT NULL,
  `broken_price` double NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=95 ;

--
-- Dumping data for table `request_borrow_equipment_type_item`
--

INSERT INTO `request_borrow_equipment_type_item` (`id`, `request_borrow_equipment_type_id`, `equipment_id`, `return_date`, `return_price`, `broken_price`) VALUES
(94, 17, 1072, '1419140595', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `request_service`
--

CREATE TABLE IF NOT EXISTS `request_service` (
`id` int(11) NOT NULL,
  `user_login_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `time_period` int(11) NOT NULL,
  `status_code` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `request_service`
--

INSERT INTO `request_service` (`id`, `user_login_id`, `description`, `file_path`, `quantity`, `due_date`, `time_period`, `status_code`, `create_date`) VALUES
(19, 1, 'Print', 'upload/files/1411180688/File.jpg', 1, '2014-09-23', 2, 'REQUEST_ISERVICE_PROCESSING', '2014-09-20 09:38:42'),
(20, 185, 'test', 'upload/files/1411542035/File.docx', 1, '2014-09-27', 107, 'REQUEST_ISERVICE_PROCESSING', '2014-09-24 14:01:05'),
(21, 1, 'test', 'upload/files/1415603238/File.mp3', 1, '2014-11-13', 4, 'REQUEST_ISERVICE_APPROVE', '2014-11-10 14:07:51'),
(23, 1, 'ติดกระจก', '', 1, '2014-11-14', 101, 'REQUEST_ISERVICE_APPROVE', '2014-11-13 11:03:37'),
(24, 1, 'ะะ', '', 1, '2014-11-14', 104, 'REQUEST_ISERVICE_APPROVE', '2014-11-13 11:05:23'),
(25, 1, '123', 'upload/files/1417223488/File.jpg', 1, '2014-12-01', 3, 'REQUEST_ISERVICE_PROCESSING', '2014-11-29 08:12:22'),
(26, 1, 'oo', 'upload/files/1419141987/File.pdf', 1, '2014-12-21', 111, 'REQUEST_ISERVICE_COMPLETED', '2014-12-21 13:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `request_service_detail`
--

CREATE TABLE IF NOT EXISTS `request_service_detail` (
`id` int(11) NOT NULL,
  `request_service_id` int(11) NOT NULL,
  `request_service_type_detail_id` int(11) DEFAULT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `request_service_detail`
--

INSERT INTO `request_service_detail` (`id`, `request_service_id`, `request_service_type_detail_id`, `description`) VALUES
(17, 19, 1, ''),
(18, 20, 1, ''),
(19, 21, 17, ''),
(21, 23, 7, ''),
(22, 24, 2, ''),
(23, 25, 2, ''),
(24, 26, 4, '');

-- --------------------------------------------------------

--
-- Table structure for table `request_service_type`
--

CREATE TABLE IF NOT EXISTS `request_service_type` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `request_service_type`
--

INSERT INTO `request_service_type` (`id`, `name`, `description`) VALUES
(1, 'Printing', ''),
(2, 'Graphic', ''),
(3, 'Sticker', ''),
(4, 'VDO Records', ''),
(5, 'Sound Records', ''),
(6, 'Copy', ''),
(7, 'VDO Record', ''),
(8, 'Sound Record', '');

-- --------------------------------------------------------

--
-- Table structure for table `request_service_type_detail`
--

CREATE TABLE IF NOT EXISTS `request_service_type_detail` (
`id` int(11) NOT NULL,
  `request_service_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `request_service_type_detail`
--

INSERT INTO `request_service_type_detail` (`id`, `request_service_type_id`, `name`, `description`) VALUES
(1, 1, 'A4', ''),
(2, 1, 'A3', ''),
(3, 1, 'A2', ''),
(4, 2, 'Poster / Paper', ''),
(5, 2, 'File formats', ''),
(6, 2, 'Books', ''),
(7, 3, 'Lable', ''),
(8, 3, 'Backdrop', ''),
(9, 4, 'Sound', ''),
(10, 4, 'VDO', ''),
(11, 5, 'E-book', ''),
(12, 5, 'Presentation', ''),
(13, 6, 'CD', ''),
(14, 6, 'DVD', ''),
(15, 7, 'Class Room', ''),
(16, 7, 'Activity', ''),
(17, 8, 'Activity', ''),
(18, 8, 'Media Presentation', '');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `description`, `status`) VALUES
(1, 'Admin', 'Admin can access all data', 'HIDDEN'),
(2, 'Staff', 'Staff is person who check and manage about user and booking.', ''),
(3, 'Student', 'User can request book for equipment. ', ''),
(4, 'Lecturer', 'Lecturer', 'A'),
(5, 'StaffAV', 'Staff AV', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE IF NOT EXISTS `role_permission` (
`id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_code` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=200 ;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`id`, `role_id`, `permission_code`) VALUES
(1, 1, 'FULL_ADMIN'),
(100, 2, 'CHECK_STATUS_REQUEST_BOOKING'),
(103, 2, 'CHECK_STATUS_REQUEST_BORROW'),
(45, 2, 'CREATE_DEPARTMENT'),
(46, 2, 'CREATE_EQUIPMENT'),
(47, 2, 'CREATE_EQUIPMENT_TYPE'),
(104, 2, 'CREATE_NEWS'),
(48, 2, 'CREATE_POSITION'),
(49, 2, 'CREATE_PRESEN_TYPE'),
(98, 2, 'CREATE_REQUEST_BOOKING'),
(50, 2, 'CREATE_REQUEST_BORROW'),
(51, 2, 'CREATE_REQUEST_SERVICE'),
(52, 2, 'CREATE_ROOM'),
(53, 2, 'CREATE_SEMESTER'),
(54, 2, 'CREATE_SERVICE_TYPE'),
(55, 2, 'CREATE_SERVICE_TYPE_ITEM'),
(34, 2, 'CREATE_USER'),
(56, 2, 'DELETE_DEPARTMENT'),
(57, 2, 'DELETE_EQUIPMENT'),
(58, 2, 'DELETE_EQUIPMENT_TYPE'),
(59, 2, 'DELETE_POSITION'),
(60, 2, 'DELETE_PRESENT_TYPE'),
(61, 2, 'DELETE_ROOM'),
(62, 2, 'DELETE_SEMESTER'),
(63, 2, 'DELETE_SERVICE_TYPE'),
(64, 2, 'DELETE_SERVICE_TYPE_ITEM'),
(65, 2, 'EDIT_REQUEST_BOOKING'),
(66, 2, 'UPDATE_DEPARTMENT'),
(67, 2, 'UPDATE_EQUIPMENT'),
(68, 2, 'UPDATE_EQUIPMENT_TYPE'),
(69, 2, 'UPDATE_POSITION'),
(70, 2, 'UPDATE_PRESENT_TYPE'),
(71, 2, 'UPDATE_REQUEST_BORROW'),
(72, 2, 'UPDATE_REQUEST_SERVICE'),
(73, 2, 'UPDATE_ROOM'),
(74, 2, 'UPDATE_SEMESTER'),
(75, 2, 'UPDATE_SERVICE_TYPE'),
(76, 2, 'UPDATE_SERVICE_TYPE)ITEM'),
(32, 2, 'UPDATE_USER'),
(77, 2, 'VIEW_ALL_REQUEST_BOOKING'),
(78, 2, 'VIEW_ALL_REQUEST_BORROW'),
(79, 2, 'VIEW_ALL_REQUEST_SERVICE'),
(80, 2, 'VIEW_DEPARTMENT'),
(81, 2, 'VIEW_EQUIPMENT'),
(82, 2, 'VIEW_EQUIPMENT_TYPE'),
(83, 2, 'VIEW_POSITION'),
(84, 2, 'VIEW_PRESENT_TYPE'),
(85, 2, 'VIEW_REQUEST_BOOKING'),
(86, 2, 'VIEW_REQUEST_BORROW'),
(87, 2, 'VIEW_REQUEST_SERVICE'),
(88, 2, 'VIEW_ROOM'),
(89, 2, 'VIEW_SEMESTER'),
(90, 2, 'VIEW_SERVICE_TYPE'),
(91, 2, 'VIEW_SERVICE_TYPE_ITEM'),
(35, 2, 'VIEW_USER'),
(101, 3, 'CHECK_STATUS_REQUEST_BOOKING'),
(102, 3, 'CHECK_STATUS_REQUEST_BORROW'),
(99, 3, 'CREATE_REQUEST_BOOKING'),
(93, 3, 'CREATE_REQUEST_BORROW'),
(94, 3, 'CREATE_REQUEST_SERVICE'),
(105, 3, 'VIEW_ALL_REQUEST_BORROW'),
(95, 3, 'VIEW_REQUEST_BOOKING'),
(96, 3, 'VIEW_REQUEST_BORROW'),
(97, 3, 'VIEW_REQUEST_SERVICE'),
(106, 4, 'CHECK_STATUS_REQUEST_BOOKING'),
(107, 4, 'CHECK_STATUS_REQUEST_BORROW'),
(108, 4, 'CREATE_REQUEST_BOOKING'),
(109, 4, 'CREATE_REQUEST_BORROW'),
(110, 4, 'CREATE_REQUEST_SERVICE'),
(111, 4, 'DELETE_REQUEST_BOOKING'),
(112, 4, 'DELETE_REQUEST_SERVICE'),
(113, 4, 'EDIT_REQUEST_BOOKING'),
(121, 4, 'UPDATE_REQUEST_BORROW'),
(114, 4, 'UPDATE_REQUEST_SERVICE'),
(115, 4, 'VIEW_ALL_REQUEST_BOOKING'),
(116, 4, 'VIEW_ALL_REQUEST_BORROW'),
(117, 4, 'VIEW_ALL_REQUEST_SERVICE'),
(118, 4, 'VIEW_REQUEST_BOOKING'),
(119, 4, 'VIEW_REQUEST_BORROW'),
(120, 4, 'VIEW_REQUEST_SERVICE'),
(122, 5, 'CHECK_STATUS_REQUEST_BOOKING'),
(123, 5, 'CHECK_STATUS_REQUEST_BORROW'),
(124, 5, 'CONFIRM_USER'),
(125, 5, 'CREATE_DEPARTMENT'),
(126, 5, 'CREATE_EQUIPMENT'),
(127, 5, 'CREATE_EQUIPMENT_TYPE'),
(128, 5, 'CREATE_GALLERY'),
(129, 5, 'CREATE_LINK'),
(130, 5, 'CREATE_NEWS'),
(131, 5, 'CREATE_POSITION'),
(132, 5, 'CREATE_PRESEN_TYPE'),
(133, 5, 'CREATE_REQUEST_BOOKING'),
(134, 5, 'CREATE_REQUEST_BORROW'),
(135, 5, 'CREATE_REQUEST_SERVICE'),
(136, 5, 'CREATE_ROLE'),
(137, 5, 'CREATE_ROOM'),
(138, 5, 'CREATE_SEMESTER'),
(139, 5, 'CREATE_SERVICE_TYPE'),
(140, 5, 'CREATE_SERVICE_TYPE_ITEM'),
(141, 5, 'CREATE_SOCIAL_MEDIA'),
(142, 5, 'CREATE_USER'),
(143, 5, 'DELETE_DEPARTMENT'),
(144, 5, 'DELETE_EQUIPMENT'),
(145, 5, 'DELETE_EQUIPMENT_TYPE'),
(146, 5, 'DELETE_GALLERY'),
(147, 5, 'DELETE_LINK'),
(148, 5, 'DELETE_NEWS'),
(149, 5, 'DELETE_POSITION'),
(150, 5, 'DELETE_PRESENT_TYPE'),
(151, 5, 'DELETE_REQUEST_BOOKING'),
(152, 5, 'DELETE_REQUEST_BORROW'),
(153, 5, 'DELETE_REQUEST_SERVICE'),
(154, 5, 'DELETE_ROLE'),
(155, 5, 'DELETE_ROOM'),
(156, 5, 'DELETE_SEMESTER'),
(157, 5, 'DELETE_SERVICE_TYPE'),
(158, 5, 'DELETE_SERVICE_TYPE_ITEM'),
(159, 5, 'DELETE_SOCIAL_MEDIA'),
(160, 5, 'DELETE_USER'),
(161, 5, 'EDIT_REQUEST_BOOKING'),
(162, 5, 'UPDATE_DEPARTMENT'),
(163, 5, 'UPDATE_EQUIPMENT'),
(164, 5, 'UPDATE_EQUIPMENT_TYPE'),
(165, 5, 'UPDATE_GALLERY'),
(166, 5, 'UPDATE_LINK'),
(167, 5, 'UPDATE_NEWS'),
(168, 5, 'UPDATE_POSITION'),
(169, 5, 'UPDATE_PRESENT_TYPE'),
(170, 5, 'UPDATE_REQUEST_BORROW'),
(171, 5, 'UPDATE_REQUEST_SERVICE'),
(172, 5, 'UPDATE_ROLE'),
(173, 5, 'UPDATE_ROOM'),
(174, 5, 'UPDATE_SEMESTER'),
(175, 5, 'UPDATE_SERVICE_TYPE'),
(176, 5, 'UPDATE_SERVICE_TYPE)ITEM'),
(177, 5, 'UPDATE_SOCIAL_MEDIA'),
(178, 5, 'UPDATE_USER'),
(199, 5, 'VIEW_ALL_REQUEST_BOOKING'),
(179, 5, 'VIEW_ALL_REQUEST_BORROW'),
(180, 5, 'VIEW_ALL_REQUEST_SERVICE'),
(181, 5, 'VIEW_DEPARTMENT'),
(182, 5, 'VIEW_EQUIPMENT'),
(183, 5, 'VIEW_EQUIPMENT_TYPE'),
(184, 5, 'VIEW_GALLERY'),
(185, 5, 'VIEW_LINK'),
(186, 5, 'VIEW_NEWS'),
(187, 5, 'VIEW_POSITION'),
(188, 5, 'VIEW_PRESENT_TYPE'),
(189, 5, 'VIEW_REQUEST_BOOKING'),
(190, 5, 'VIEW_REQUEST_BORROW'),
(191, 5, 'VIEW_REQUEST_SERVICE'),
(192, 5, 'VIEW_ROLE'),
(193, 5, 'VIEW_ROOM'),
(194, 5, 'VIEW_SEMESTER'),
(195, 5, 'VIEW_SERVICE_TYPE'),
(196, 5, 'VIEW_SERVICE_TYPE_ITEM'),
(197, 5, 'VIEW_SOCIAL_MEDIA'),
(198, 5, 'VIEW_USER');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE IF NOT EXISTS `room` (
`id` int(11) NOT NULL,
  `room_group_id` int(11) NOT NULL,
  `room_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5314 ;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `room_group_id`, `room_code`, `name`, `description`) VALUES
(-1, 1, '-1', '-', '-'),
(1, 2, '1', 'Ground Floor', 'Ground Floor'),
(2, 2, '2', 'SPF(Taweewattana I)', 'SPF(Taweewattana I)'),
(3, 2, '3', 'SPF(Taweewattana II)', 'SPF(Taweewattana II)'),
(4, 2, '4', 'SPF(Narapirom)', 'SPF(Narapirom)'),
(5, 2, '5', 'SPF(Mahaswasdee I)', 'SPF(Mahaswasdee I)'),
(6, 2, '6', 'SPF(Mahaswasdee II)', 'SPF(Mahaswasdee II)'),
(7, 4, '7', 'Ed Teach. Office', 'Ed Teach. Office'),
(1108, 1, '1108', '1108 (Lab Food.)', '1108 (Lab Food.)'),
(1210, 2, '1210', '1210 (Seminar)', '1210 (Seminar)'),
(1214, 2, '1214', '1214(Dean)', '1214(Dean)'),
(1302, 1, '1302', '1302', '1302'),
(1303, 1, '1303', '1303', '1303'),
(1304, 1, '1304', '1304', '1304'),
(1305, 1, '1305', '1305', '1305'),
(1306, 1, '1306', '1306', '1306'),
(1307, 1, '1307', '1307', '1307'),
(1308, 1, '1308', '1308', '1308'),
(1309, 1, '1309', '1309 (Math Clinic)', '1309 (Math Clinic)'),
(1312, 1, '1312', '1312 (FFA)', '1312 (FFA)'),
(1314, 1, '1314', '1314', '1314'),
(1315, 1, '1315', '1315', '1315'),
(1318, 2, '1318', '1318 (Audi)', '1318 (Audi)'),
(1402, 1, '1402', '1402', '1402'),
(1403, 1, '1403', '1403', '1403'),
(1404, 1, '1404', '1404', '1404'),
(1405, 1, '1405', '1405', '1405'),
(1406, 1, '1406', '1406', '1406'),
(1407, 1, '1407', '1407', '1407'),
(1408, 1, '1408', '1408', '1408'),
(1417, 1, '1417', '1417', '1417'),
(1418, 1, '1418', '1418', '1418'),
(1419, 1, '1419', '1419', '1419'),
(1502, 1, '1502', '1502', '1502'),
(1503, 1, '1503', '1503', '1503'),
(1504, 1, '1504', '1504', '1504'),
(1506, 1, '1506', '1506 (Lab Sci.)', '1506 (Lab Sci.)'),
(2207, 2, '2207', '2207 (BBA)', '2207 (BBA)'),
(2302, 1, '2302', '2302', '2302'),
(2303, 1, '2303', '2303', '2303'),
(2306, 1, '2306', '2306', '2306'),
(2307, 1, '2307', '2307', '2307'),
(2308, 1, '2308', '2308', '2308'),
(3302, 1, '3302', '3302', '3302'),
(3303, 1, '3303', '3303', '3303'),
(3304, 1, '3304', '3304', '3304'),
(3305, 1, '3305', '3305', '3305'),
(3306, 1, '3306', '3306', '3306'),
(3307, 1, '3307', '3307 (FFA)', '3307 (FFA)'),
(3315, 1, '3315', '3315', '3315'),
(3316, 1, '3316', '3316', '3316'),
(3317, 1, '3317', '3317', '3317'),
(3407, 1, '3407', '3407', '3407'),
(3408, 1, '3408', '3408', '3408'),
(3409, 1, '3409', '3409', '3409'),
(3410, 1, '3410', '3410', '3410'),
(3411, 1, '3411', '3411', '3411'),
(3412, 1, '3412', '3412', '3412'),
(3414, 2, '3414', '3414 (Meeting)', '3414 (Meeting)'),
(3415, 2, '3415', '3415 (Meeting)', '3415 (Meeting)'),
(3420, 1, '3420', '3420', '3420'),
(3421, 1, '3421', '3421', '3421'),
(3422, 1, '3422', '3422', '3422'),
(3508, 1, '3508', '3508 (Lab Sci.)', '3508 (Lab Sci.)'),
(5207, 1, '5207', '5207', '5207'),
(5208, 1, '5208', '5208', '5208'),
(5209, 1, '5209', '5209', '5209'),
(5210, 1, '5210', '5210', '5210'),
(5211, 1, '5211', '5211', '5211'),
(5212, 1, '5212', '5212', '5212'),
(5301, 1, '5301', '5301', '5301'),
(5307, 1, '5307', '5307', '5307'),
(5308, 1, '5308', '5308', '5308'),
(5309, 1, '5309', '5309', '5309'),
(5310, 1, '5310', '5310', '5310'),
(5311, 1, '5311', '5311', '5311'),
(5312, 1, '5312', '5312', '5312'),
(5313, 1, '5313', '5313', '5313');

-- --------------------------------------------------------

--
-- Table structure for table `room_equipment`
--

CREATE TABLE IF NOT EXISTS `room_equipment` (
`id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `room_group`
--

CREATE TABLE IF NOT EXISTS `room_group` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `room_group`
--

INSERT INTO `room_group` (`id`, `name`, `description`) VALUES
(1, 'Class Room', ''),
(2, 'Meeting Room', ''),
(3, 'Activity Floor', 'Activity Floor'),
(4, 'Ed Tech. Office', 'Ed Tech. Office');

-- --------------------------------------------------------

--
-- Table structure for table `room_staff`
--

CREATE TABLE IF NOT EXISTS `room_staff` (
`id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `room_staff`
--

INSERT INTO `room_staff` (`id`, `room_id`, `staff_id`) VALUES
(28, 1, 2),
(12, 1, 13),
(13, 3, 13),
(14, 4, 13),
(15, 6, 13),
(16, 1108, 13),
(32, 1108, 158),
(17, 1214, 13),
(29, 1302, 2),
(18, 1304, 13),
(19, 1307, 13),
(20, 1309, 13),
(21, 1312, 13),
(22, 1315, 13),
(23, 1318, 13),
(24, 1404, 13);

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE IF NOT EXISTS `room_type` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`id`, `name`, `description`) VALUES
(1, 'Classroom', ''),
(2, 'Conference', ''),
(3, 'Without MUIC', ''),
(4, 'Other', '');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE IF NOT EXISTS `semester` (
`id` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `semester_number` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id`, `academic_year`, `semester_number`, `name`, `description`, `start_date`, `end_date`, `status`) VALUES
(1, 2555, 2, '2 / 2555', '', '2012-11-05', '2013-03-15', 'ACTIVE'),
(2, 2555, 3, '3 / 2555', '', '2013-03-18', '2013-05-17', 'ACTIVE'),
(3, 2556, 1, '1 / 2556', '', '2013-05-20', '2013-09-27', 'ACTIVE'),
(4, 2013, 1, 'Trimester 1', 'Academic Year: 2013-2014', '2013-09-16', '2013-12-07', ''),
(5, 2013, 2, 'Trimester 2', 'Trimester 2', '2014-01-20', '2014-04-12', ''),
(6, 2013, 3, 'Trimester 3', 'Trimester 3', '2014-04-28', '2014-07-12', ''),
(7, 2014, 1, 'First Trimester_14-15', '', '2014-09-15', '2014-12-31', '');

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE IF NOT EXISTS `social_media` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `name`, `description`, `url`) VALUES
(1, 'facebook', 'dfdfadsf', 'http://www.facebook.com');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `status_code` varchar(255) NOT NULL,
  `status_group_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `active` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_code`, `status_group_id`, `name`, `description`, `active`) VALUES
('ACTIVE', 'USER_LOGIN_STATUS', 'Active', '', 'Y'),
('INACTIVE', 'USER_LOGIN_STATUS', 'Inactive', '', 'Y'),
('PERIOD_ACTIVE', 'PERIOD_STATUS', 'Active', '', 'Y'),
('PERIOD_GROUP_ACTIVE', 'PERIOD_GROUP_STATUS', 'Active', '', 'Y'),
('PERIOD_GROUP_INACTIVE', 'PERIOD_GROUP_STATUS', 'Inactive', '', 'Y'),
('PERIOD_INACTIVE', 'PERIOD_STATUS', 'Inactive', '', 'Y'),
('REQUEST_APPROVE', 'REQUEST_STATUS', 'Approve', '', 'Y'),
('REQUEST_BORROW_APPROVED', 'REQUEST_BORROW_STATUS', 'Approved', '', 'Y'),
('REQUEST_BORROW_APPROVE_1', 'REQUEST_BORROW_STATUS', 'Approve1', '', 'N'),
('REQUEST_BORROW_CANCELLED', 'REQUEST_BORROW_STATUS', 'Cancelled', '', 'Y'),
('REQUEST_BORROW_COMPLETED', 'REQUEST_BORROW_STATUS', 'Returned', '', 'Y'),
('REQUEST_BORROW_WAIT_FOR_APPROVE', 'REQUEST_BORROW_STATUS', 'Wait For Approve', '', 'Y'),
('REQUEST_CANCEL', 'REQUEST_STATUS', 'Cancel', '', 'Y'),
('REQUEST_COMPLETED', 'REQUEST_STATUS', 'Completed', '', 'Y'),
('REQUEST_ISERVICE_APPROVE', 'REQUEST_ISERVICE_STATUS', 'Approved', ' ', 'Y'),
('REQUEST_ISERVICE_CANCEL', 'REQUEST_ISERVICE_STATUS', 'Cancel', ' ', 'Y'),
('REQUEST_ISERVICE_COMPLETED', 'REQUEST_ISERVICE_STATUS', 'Completed', ' ', 'Y'),
('REQUEST_ISERVICE_PROCESSING', 'REQUEST_ISERVICE_STATUS', 'Processing', '', 'Y'),
('REQUEST_ISERVICE_WAIT_APPROVE', 'REQUEST_ISERVICE_STATUS', 'Wait for approve', ' ', 'Y'),
('REQUEST_SKYDATA_WAIT_APPROVE', 'SKY_INTERFACE_STATUS', '-', '-', 'Y'),
('REQUEST_WAIT_APPROVE', 'REQUEST_STATUS', 'Wait for approve', '', 'Y'),
('R_B_NEW_CANCELLED', 'REQUEST_BORROW_STATUS_NEW', 'Cancel', '', 'A'),
('R_B_NEW_DISAPPROVE', 'REQUEST_BORROW_STATUS_NEW', 'Disaprove', '', 'Y'),
('R_B_NEW_PREPARE', 'REQUEST_BORROW_STATUS_NEW', 'Preparing', '', 'Y'),
('R_B_NEW_READY', 'REQUEST_BORROW_STATUS_NEW', 'Ready', '', 'Y'),
('R_B_NEW_READY_MISSING', 'REQUEST_BORROW_STATUS_NEW', 'Ready but missing', '', 'Y'),
('R_B_NEW_RETURNED', 'REQUEST_BORROW_STATUS_NEW', 'Returned', '', ''),
('R_B_NEW_RETURNED_MISSING', 'REQUEST_BORROW_STATUS_NEW', 'Return but missing', '', 'Y'),
('R_B_NEW_WAIT_APPROVE_1', 'REQUEST_BORROW_STATUS_NEW', 'Waiting Approve 1', '', 'Y'),
('R_B_NEW_WAIT_APPROVE_2', 'REQUEST_BORROW_STATUS_NEW', 'Waiting Approve 2', '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `status_group`
--

CREATE TABLE IF NOT EXISTS `status_group` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `active` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status_group`
--

INSERT INTO `status_group` (`id`, `name`, `description`, `active`) VALUES
('PERIOD_GROUP_STATUS', 'Period Group Status', '', 'Y'),
('PERIOD_STATUS', 'Period Status', '', 'Y'),
('REQUEST_BORROW_STATUS', 'Request Borrow Status', '', 'Y'),
('REQUEST_BORROW_STATUS_NEW', 'Request Borrow Status', '', 'Y'),
('REQUEST_ISERVICE_STATUS', 'Request IService Status', '', 'Y'),
('REQUEST_STATUS', 'Request Status', '', 'Y'),
('SKY_INTERFACE_STATUS', 'Interface with sky status', '', 'Y'),
('USER_LOGIN_STATUS', 'User Login Status', '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
`id` int(11) NOT NULL,
  `subj_code` varchar(255) DEFAULT NULL,
  `sbj_name` varchar(255) DEFAULT NULL,
  `teacher_user_id` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `subj_code`, `sbj_name`, `teacher_user_id`) VALUES
(5, 'FA-099', 'วิชา 1', 184),
(6, 'FA-089', 'วิชา 2', 185);

-- --------------------------------------------------------

--
-- Table structure for table `tb_email_forapprove`
--

CREATE TABLE IF NOT EXISTS `tb_email_forapprove` (
  `id` int(11) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `status_code` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_email_forapprove`
--

INSERT INTO `tb_email_forapprove` (`id`, `email`, `status_code`) VALUES
(1, 'chayanonp@gmail.com', 'REQUEST_BORROW_APPROVE_1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_setting`
--

CREATE TABLE IF NOT EXISTS `tb_setting` (
  `id` int(11) NOT NULL,
  `print_format` varchar(45) DEFAULT '0' COMMENT '0=NORMAL,1=THEMAL',
  `returnLatePricePerDay` int(11) DEFAULT '500',
  `borrow_doc_prefix` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_setting`
--

INSERT INTO `tb_setting` (`id`, `print_format`, `returnLatePricePerDay`, `borrow_doc_prefix`) VALUES
(1, '0', 500, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_sky_notification`
--

CREATE TABLE IF NOT EXISTS `tb_sky_notification` (
`id` int(11) NOT NULL,
  `request_type` varchar(45) DEFAULT NULL,
  `request_key` varchar(45) DEFAULT NULL,
  `data` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tb_sky_notification`
--

INSERT INTO `tb_sky_notification` (`id`, `request_type`, `request_key`, `data`, `create_date`, `status`) VALUES
(5, 'REQUEST_KEY_PUSH', '121212', '', '2014-07-18 08:39:55', 1),
(6, 'REQUEST_KEY_PUSH', '437d2b611411d9942123a848d1ee11ecb6a70e', 'true', '2014-07-18 08:41:44', 1),
(7, 'REQUEST_KEY_PUSH', '437d2b611411d9942123a848d1ee11ecb6a70e', 'true', '2014-08-11 10:29:06', 1),
(8, 'REQUEST_KEY_PUSH', '437d2b611411d9942123a848d1ee11ecb6a70e', 'true', '2014-08-11 10:38:14', 1),
(9, 'REQUEST_KEY_PUSH', '121212', '', '2014-11-17 10:43:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_forget_password_request`
--

CREATE TABLE IF NOT EXISTS `user_forget_password_request` (
`id` int(11) NOT NULL,
  `user_login_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `request_date` datetime NOT NULL,
  `status` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_information`
--

CREATE TABLE IF NOT EXISTS `user_information` (
`id` int(11) NOT NULL COMMENT 'รหัส',
  `personal_card_id` varchar(13) NOT NULL COMMENT 'เลขประจำตัวประชาชน',
  `personal_title` varchar(255) NOT NULL COMMENT 'คำนำหน้า',
  `first_name` varchar(255) NOT NULL COMMENT 'ชื่อ',
  `last_name` varchar(255) NOT NULL COMMENT 'นามสกุล',
  `gender` varchar(5) NOT NULL COMMENT 'เพศ',
  `birth_date` date NOT NULL COMMENT 'วันเกิด',
  `address1` varchar(255) NOT NULL COMMENT 'ที่อยู่ 1',
  `address2` varchar(255) NOT NULL COMMENT 'ที่อยู่ 2',
  `sub_district` varchar(255) NOT NULL COMMENT 'ตำบล',
  `district` varchar(255) NOT NULL COMMENT 'อำเภอ',
  `province` varchar(255) NOT NULL COMMENT 'จังหวัด',
  `postal_code` varchar(5) NOT NULL COMMENT 'รหัสไปรษณีย์',
  `phone` varchar(50) NOT NULL COMMENT 'โทรศัพท์',
  `mobile` varchar(50) NOT NULL COMMENT 'โทรศัพท์มือถือ'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `user_information`
--

INSERT INTO `user_information` (`id`, `personal_card_id`, `personal_title`, `first_name`, `last_name`, `gender`, `birth_date`, `address1`, `address2`, `sub_district`, `district`, `province`, `postal_code`, `phone`, `mobile`) VALUES
(1, '1', '', 'admin', '', '', '0000-00-00', '', '', '', '', '', '', '', ''),
(158, '003', '', 'edtech3416', '', 'F', '0000-00-00', '', '', '', '', '', '', '', ''),
(183, '001', '', 'edtech3417', '', '', '0000-00-00', '', '', '', '', '', '', '', ''),
(185, '004', '', 'edtech3414', '', 'M', '0000-00-00', '', '', '', '', '', '', '', ''),
(198, '0015', '', 'kroonaka', '', 'M', '0000-00-00', 'MUIC', '', '', '', '', '', '', ''),
(224, '224', '', 'Chayanon', 'Poonthong', '', '0000-00-00', '', '', '', '', '', '', '', ''),
(240, '240', '', 'thammachart', 'kan', '', '0000-00-00', '', '', '', '', '', '', '', ''),
(241, '241', '', 'aaron', '', '', '0000-00-00', '', '', '', '', '', '', '', ''),
(242, '242', '', 'agnieszka', '', '', '0000-00-00', '', '', '', '', '', '', '', ''),
(244, '244', '', '5680162', '', '', '0000-00-00', '', '', '', '', '', '', '', ''),
(245, '245', '', '5680163', '', '', '0000-00-00', '', '', '', '', '', '', '', ''),
(246, '246', '', '5680164', '', '', '0000-00-00', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
`id` int(11) NOT NULL COMMENT 'รหัส',
  `role_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL COMMENT 'ชื่อผู้ใช้สำหรับ login',
  `password` varchar(100) NOT NULL COMMENT 'รหัสผ่าน',
  `status` varchar(255) NOT NULL COMMENT 'สถานะ',
  `create_by` int(11) DEFAULT NULL COMMENT 'สร้างโดย',
  `latest_login` datetime NOT NULL COMMENT 'เข้าสู่ระบบครั้งล่าสุด',
  `department_id` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `token_id` text NOT NULL,
  `phone_type` varchar(1) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `isApprover_1` varchar(1) NOT NULL DEFAULT '0',
  `isApprover_2` varchar(1) DEFAULT '0',
  `ApproverType` varchar(10) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`id`, `role_id`, `username`, `password`, `status`, `create_by`, `latest_login`, `department_id`, `email`, `token_id`, `phone_type`, `parent`, `isApprover_1`, `isApprover_2`, `ApproverType`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', NULL, '2014-12-21 14:46:42', NULL, 'icedtechAV4@gmail.com', 'e07ba8d96728c0d7ac013f8292c48ca6f0495d25b800ec8a21155defda52a7df', '1', 0, '0', '0', NULL),
(158, 4, 'edtech3416', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', 1, '2014-09-24 13:34:59', 2, 'edtech3416@gmail.com', '', '', 183, '0', '0', NULL),
(183, 4, 'edtech3417', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', NULL, '2014-12-21 14:38:38', NULL, 'edtech3417@gmail.com', '', '', NULL, '0', '1', '1'),
(185, 4, 'edtech3414', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', NULL, '2014-12-21 14:37:49', NULL, 'edtech3414@gmail.com', '', '', 158, '1', '0', '1'),
(198, 2, 'kroonaka', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', NULL, '2014-12-21 14:39:54', NULL, 'kroonaka@gmail.com', '', '', NULL, '0', '0', NULL),
(224, 5, 'chayanon.poo', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', 1, '2014-12-21 14:40:21', NULL, 'chayanon.poo@mahidol.ac.th', '', '', 1, '0', '0', NULL),
(240, 5, 'thammachart.kan', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', 1, '2014-12-21 14:42:35', NULL, 'thammachart.kan@mahidol.ac.th', '', '', 1, '1', '0', '2'),
(241, 4, 'aaron.sch', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', 1, '2014-12-21 14:41:14', NULL, 'pawit1357@hotmail.com', '', '', 1, '0', '0', NULL),
(242, 4, 'agnieszka.att', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', 1, '2014-12-10 11:14:25', NULL, 'pawitvaap@gmail.com', '', '', 1, '0', '0', NULL),
(244, 3, '5680162', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', 1, '2014-12-21 14:36:26', NULL, '5680162@mahidol.ac.th', '', '', 241, '0', '0', NULL),
(245, 3, '5680163', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', 1, '2014-12-10 11:11:04', NULL, '5680163@mahidol.ac.th', '', '', 241, '0', '0', NULL),
(246, 3, '5680164', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', 1, '2014-12-10 11:11:11', NULL, '5680164@mahidol.ac.th', '', '', 242, '0', '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `using_log`
--

CREATE TABLE IF NOT EXISTS `using_log` (
`id` int(11) NOT NULL,
  `user_login_id` int(11) DEFAULT NULL,
  `controller_id` varchar(255) NOT NULL,
  `action_id` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=746 ;

--
-- Dumping data for table `using_log`
--

INSERT INTO `using_log` (`id`, `user_login_id`, `controller_id`, `action_id`, `datetime`) VALUES
(5, 1, 'Solution', '', '2013-06-10 01:57:35'),
(6, 1, 'Report', '', '2013-06-10 01:57:39'),
(7, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-10 01:57:53'),
(8, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-10 01:57:55'),
(9, 1, 'RequestService', '', '2013-06-10 02:00:38'),
(10, 1, 'RequestService', 'Request', '2013-06-10 02:00:40'),
(11, 1, 'RequestService', '', '2013-06-10 02:00:57'),
(12, 1, 'requestService', 'update', '2013-06-10 02:01:00'),
(13, 1, 'RequestBorrow', '', '2013-06-10 02:02:09'),
(14, 1, 'RequestBorrow', 'Request', '2013-06-10 02:02:12'),
(15, 1, 'RequestBorrow', '', '2013-06-10 02:02:17'),
(16, 1, 'RequestBorrow', 'Index', '2013-06-10 02:02:19'),
(17, 1, 'RequestBorrow', '', '2013-06-10 02:02:24'),
(18, 1, 'RequestBorrow', 'Index', '2013-06-10 02:02:25'),
(19, 1, 'RequestBorrow', 'Index', '2013-06-10 02:02:27'),
(20, 1, 'RequestBorrow', 'Index', '2013-06-10 02:02:29'),
(21, 1, 'RequestBorrow', 'Request', '2013-06-10 02:02:41'),
(22, 1, 'RequestBorrow', 'View', '2013-06-10 02:02:56'),
(23, 1, 'RequestBorrow', 'Update', '2013-06-10 02:02:59'),
(24, 1, 'Report', '', '2013-06-10 02:05:27'),
(25, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:05:29'),
(26, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:05:42'),
(27, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:06:14'),
(28, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:06:19'),
(29, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:06:30'),
(30, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:06:35'),
(31, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-10 02:06:39'),
(32, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-10 02:07:13'),
(33, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-10 02:07:18'),
(34, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-10 02:07:20'),
(35, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-10 02:07:22'),
(36, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-10 02:07:24'),
(37, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-10 02:07:27'),
(38, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-10 02:07:28'),
(39, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-10 02:07:33'),
(40, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-10 02:07:37'),
(41, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-10 02:08:42'),
(42, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-10 02:08:44'),
(43, 1, 'RequestBooking', '', '2013-06-10 02:16:45'),
(44, 1, 'RequestBooking', '', '2013-06-10 02:16:45'),
(45, 1, 'RequestBooking', 'Request', '2013-06-10 02:16:47'),
(46, 1, 'RequestBooking', 'View', '2013-06-10 02:16:57'),
(47, 1, 'RequestBooking', 'update', '2013-06-10 02:23:02'),
(48, 1, 'RequestBorrow', '', '2013-06-10 02:23:03'),
(49, 1, 'RequestBorrow', 'CheckStatus', '2013-06-10 02:23:05'),
(50, 1, 'RequestBorrow', '', '2013-06-10 02:23:07'),
(51, 1, 'RequestBorrow', 'Index', '2013-06-10 02:23:09'),
(52, 1, 'RequestBorrow', 'Request', '2013-06-10 02:23:11'),
(53, 1, 'RequestBorrow', '', '2013-06-10 02:23:28'),
(54, 1, 'RequestBorrow', 'Index', '2013-06-10 02:23:31'),
(55, 1, 'RequestBorrow', 'Index', '2013-06-10 02:23:33'),
(56, 1, 'RequestBorrow', '', '2013-06-10 02:23:34'),
(57, 1, 'RequestBorrow', 'Index', '2013-06-10 02:23:36'),
(58, 1, 'RequestBorrow', '', '2013-06-10 02:24:54'),
(59, 1, 'RequestBorrow', 'Index', '2013-06-10 02:24:56'),
(60, 1, 'requestBorrow', 'update', '2013-06-10 02:25:16'),
(61, 1, 'requestBorrow', 'update', '2013-06-10 02:25:26'),
(62, 1, 'requestBorrow', 'update', '2013-06-10 02:25:31'),
(63, 1, 'requestBorrow', 'update', '2013-06-10 02:25:40'),
(64, 1, 'RequestBorrow', '', '2013-06-10 02:25:50'),
(65, 1, 'RequestBorrow', 'Index', '2013-06-10 02:25:52'),
(66, 1, 'requestBorrow', 'update', '2013-06-10 02:25:54'),
(67, 1, 'RequestBorrow', 'Request', '2013-06-10 02:25:57'),
(68, NULL, 'management', 'login', '2013-06-10 14:40:58'),
(69, NULL, '', '', '2013-06-10 14:40:59'),
(70, 1, 'RequestBooking', '', '2013-06-10 14:41:01'),
(71, 1, 'Report', '', '2013-06-10 14:41:04'),
(72, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:41:06'),
(73, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:41:20'),
(74, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:43:21'),
(75, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:43:37'),
(76, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:43:48'),
(77, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:44:11'),
(78, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:44:20'),
(79, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:44:41'),
(80, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:44:54'),
(81, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:00:40'),
(82, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:01:02'),
(83, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:01:24'),
(84, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:01:59'),
(85, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:02:12'),
(86, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:04:25'),
(87, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:13:35'),
(88, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:19:06'),
(89, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:20:56'),
(90, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:27:07'),
(91, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:27:24'),
(92, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:27:33'),
(93, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:29:15'),
(94, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:29:22'),
(95, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:29:37'),
(96, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:30:17'),
(97, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:31:32'),
(98, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:32:09'),
(99, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:32:36'),
(100, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:32:40'),
(101, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:32:44'),
(102, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:33:33'),
(103, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:35:01'),
(104, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 16:35:03'),
(105, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:35:05'),
(106, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:35:28'),
(107, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:35:32'),
(108, 1, 'Solution', '', '2013-06-10 16:35:39'),
(109, 1, 'RequestService', '', '2013-06-10 16:35:41'),
(110, 1, 'RequestBorrow', '', '2013-06-10 16:35:43'),
(111, 1, 'RequestBooking', '', '2013-06-10 16:35:43'),
(112, 1, 'Report', '', '2013-06-10 16:35:45'),
(113, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:35:46'),
(114, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:53:16'),
(115, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:53:22'),
(116, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:59:27'),
(117, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:59:31'),
(118, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:59:52'),
(119, NULL, 'management', 'login', '2013-06-10 17:34:46'),
(120, 1, 'RequestBooking', '', '2013-06-10 17:34:48'),
(121, 1, 'Report', '', '2013-06-10 17:34:50'),
(122, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:34:52'),
(123, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:39:20'),
(124, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:39:25'),
(125, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:39:43'),
(126, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:40:30'),
(127, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:40:40'),
(128, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:41:00'),
(129, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:41:09'),
(130, NULL, 'management', 'login', '2013-06-10 17:41:58'),
(131, 1, 'RequestBooking', '', '2013-06-10 17:42:00'),
(132, 1, 'Report', '', '2013-06-10 17:42:04'),
(133, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:42:05'),
(134, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:42:09'),
(135, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:42:56'),
(136, NULL, 'management', 'login', '2013-06-10 23:49:36'),
(137, 1, 'RequestBooking', '', '2013-06-10 23:49:37'),
(138, 1, 'RequestBooking', 'Request', '2013-06-10 23:49:39'),
(139, 1, 'RequestBooking', 'Request', '2013-06-11 00:25:03'),
(140, 1, 'RequestBooking', 'Request', '2013-06-11 00:25:54'),
(141, 1, 'RequestBooking', 'Request', '2013-06-11 00:26:54'),
(142, 1, 'RequestBooking', 'Request', '2013-06-11 00:28:07'),
(143, 1, 'RequestBooking', 'Request', '2013-06-11 00:29:41'),
(144, 1, 'RequestBooking', 'Request', '2013-06-11 00:30:52'),
(145, 1, 'RequestBooking', 'Request', '2013-06-11 00:32:03'),
(146, 1, 'RequestBooking', 'Request', '2013-06-11 00:36:28'),
(147, 1, 'RequestBooking', 'Request', '2013-06-11 00:39:27'),
(148, 1, 'RequestBooking', 'Request', '2013-06-11 00:41:03'),
(149, 1, 'RequestBooking', 'Request', '2013-06-11 00:41:13'),
(150, 1, 'RequestBooking', 'Request', '2013-06-11 00:52:51'),
(151, 1, 'RequestBooking', 'Request', '2013-06-11 00:53:03'),
(152, 1, 'RequestBooking', 'Request', '2013-06-11 00:54:56'),
(153, 1, 'RequestBooking', 'Request', '2013-06-11 00:55:22'),
(154, 1, 'RequestBooking', 'Request', '2013-06-11 00:56:23'),
(155, 1, 'RequestBooking', 'Request', '2013-06-11 00:56:37'),
(156, 1, 'RequestBooking', 'Request', '2013-06-11 00:56:59'),
(157, 1, 'RequestBooking', 'Request', '2013-06-11 01:00:02'),
(158, 1, 'RequestBooking', 'Request', '2013-06-11 01:00:16'),
(159, 1, 'RequestBooking', 'Request', '2013-06-11 01:00:26'),
(160, 1, 'RequestBooking', 'Request', '2013-06-11 01:01:26'),
(161, 1, 'RequestBooking', 'Request', '2013-06-11 01:01:50'),
(162, 1, 'RequestBooking', 'Request', '2013-06-11 01:04:04'),
(163, 1, 'RequestBooking', 'Request', '2013-06-11 01:04:52'),
(164, 1, 'RequestBooking', 'Request', '2013-06-11 01:06:24'),
(165, 1, 'RequestBooking', 'Request', '2013-06-11 01:06:46'),
(166, 1, 'RequestBooking', 'Request', '2013-06-11 01:09:23'),
(167, 1, 'RequestBooking', 'Request', '2013-06-11 01:10:41'),
(168, 1, 'RequestBooking', 'Request', '2013-06-11 01:13:12'),
(169, 1, 'RequestBooking', 'Request', '2013-06-11 01:13:33'),
(170, 1, 'RequestBooking', 'Request', '2013-06-11 01:15:17'),
(171, 1, 'RequestBooking', 'Request', '2013-06-11 01:16:35'),
(172, 1, 'RequestBooking', 'Request', '2013-06-11 01:16:53'),
(173, 1, 'RequestBooking', 'Request', '2013-06-11 01:20:12'),
(174, 1, 'RequestBooking', 'Request', '2013-06-11 01:20:36'),
(175, 1, 'RequestBooking', 'Request', '2013-06-11 01:25:04'),
(176, 1, 'RequestBooking', 'Request', '2013-06-11 01:25:40'),
(177, 1, 'RequestBooking', 'View', '2013-06-11 01:26:13'),
(178, 1, 'RequestBooking', 'View', '2013-06-11 01:27:20'),
(179, 1, 'RequestBooking', 'Request', '2013-06-11 01:27:21'),
(180, 1, 'RequestBooking', 'Request', '2013-06-11 01:27:22'),
(181, 1, 'RequestBooking', 'Request', '2013-06-11 01:27:52'),
(182, 1, 'RequestBooking', 'View', '2013-06-11 01:28:16'),
(183, 1, 'RequestBooking', 'Request', '2013-06-11 01:29:04'),
(184, 1, 'RequestBooking', 'Request', '2013-06-11 01:29:05'),
(185, 1, 'RequestBooking', 'Request', '2013-06-11 01:29:17'),
(186, 1, 'RequestBooking', 'View', '2013-06-11 01:29:39'),
(187, 1, 'RequestBooking', 'Request', '2013-06-11 01:29:47'),
(188, 1, 'RequestBooking', 'View', '2013-06-11 01:30:04'),
(189, 1, 'RequestService', '', '2013-06-11 01:30:30'),
(190, 1, 'RequestService', 'Request', '2013-06-11 01:30:31'),
(191, 1, 'Report', '', '2013-06-11 01:32:33'),
(192, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:32:34'),
(193, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:32:45'),
(194, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-11 01:33:30'),
(195, 1, 'Report', 'RequestServiceStatisticReport', '2013-06-11 01:33:34'),
(196, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:33:36'),
(197, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:33:38'),
(198, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-11 01:33:39'),
(199, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:33:41'),
(200, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:33:46'),
(201, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:33:54'),
(202, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:33:56'),
(203, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:34:05'),
(204, 1, 'RequestService', '', '2013-06-11 01:36:26'),
(205, 1, 'RequestService', 'Request', '2013-06-11 01:36:27'),
(206, 1, 'RequestService', 'Request', '2013-06-11 01:37:33'),
(207, 1, 'RequestService', '', '2013-06-11 01:37:46'),
(208, 1, 'requestService', 'view', '2013-06-11 01:37:53'),
(209, 1, 'requestService', 'view', '2013-06-11 01:37:59'),
(210, 1, 'RequestService', 'Request', '2013-06-11 01:38:01'),
(211, 1, 'RequestService', '', '2013-06-11 01:38:12'),
(212, 1, 'requestService', 'view', '2013-06-11 01:38:15'),
(213, 1, 'Report', '', '2013-06-11 01:38:20'),
(214, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:38:22'),
(215, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:38:43'),
(216, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:39:12'),
(217, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:39:21'),
(218, 1, 'Report', 'RequestServiceStatisticReport', '2013-06-11 01:39:31'),
(219, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:39:43'),
(220, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-11 01:39:53'),
(221, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:39:55'),
(222, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:39:57'),
(223, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:39:58'),
(224, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:40:21'),
(225, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:44:30'),
(226, 1, 'RequestBooking', '', '2013-06-11 01:44:50'),
(227, 1, 'RequestBooking', 'Request', '2013-06-11 01:44:53'),
(228, 1, 'RequestBooking', 'View', '2013-06-11 01:45:03'),
(229, 1, 'Report', '', '2013-06-11 01:45:04'),
(230, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:45:06'),
(231, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:45:22'),
(232, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:46:19'),
(233, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:46:56'),
(234, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:47:01'),
(235, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:48:00'),
(236, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:48:17'),
(237, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-11 01:48:20'),
(238, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-11 01:49:11'),
(239, 1, 'Report', 'RequestServiceStatisticReport', '2013-06-11 01:49:15'),
(240, 1, 'Report', 'RequestServiceStatisticReport', '2013-06-11 01:50:00'),
(241, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:50:10'),
(242, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:50:53'),
(243, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-11 01:51:01'),
(244, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:51:04'),
(245, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-11 01:51:05'),
(246, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-11 01:51:50'),
(247, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:51:58'),
(248, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:52:00'),
(249, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:52:03'),
(250, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-11 01:52:05'),
(251, 1, 'Report', 'RequestServiceStatisticReport', '2013-06-11 01:52:10'),
(252, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:52:12'),
(253, 1, '', '', '2013-06-11 01:52:17'),
(254, NULL, 'management', 'login', '2013-06-11 10:55:12'),
(255, 1, 'RequestBooking', '', '2013-06-11 10:59:07'),
(256, 1, 'RequestBooking', '', '2013-06-11 10:59:43'),
(257, 1, 'RequestBooking', 'Request', '2013-06-11 10:59:44'),
(258, 1, 'RequestBooking', 'View', '2013-06-11 11:00:04'),
(259, 1, 'RequestBooking', 'Request', '2013-06-11 11:00:10'),
(260, 1, 'RequestBooking', 'Request', '2013-06-11 11:00:34'),
(261, 1, 'RequestBooking', 'Request', '2013-06-11 11:05:27'),
(262, 1, 'RequestBooking', 'View', '2013-06-11 11:05:52'),
(263, 1, 'RequestBooking', 'Request', '2013-06-11 11:05:53'),
(264, 1, 'RequestBooking', 'Request', '2013-06-11 11:18:13'),
(265, 1, 'RequestBooking', 'Request', '2013-06-11 11:18:53'),
(266, 1, 'RequestBooking', 'Request', '2013-06-11 11:20:23'),
(267, 1, 'RequestBooking', 'Request', '2013-06-11 11:20:36'),
(268, 1, 'RequestBooking', 'Request', '2013-06-11 11:20:47'),
(269, 1, 'RequestBooking', 'Request', '2013-06-11 11:29:45'),
(270, 1, 'RequestBooking', 'Request', '2013-06-11 11:29:49'),
(271, 1, 'RequestBooking', 'Request', '2013-06-11 11:30:10'),
(272, 1, 'RequestBooking', 'Request', '2013-06-11 11:30:46'),
(273, 1, 'RequestBooking', 'Request', '2013-06-11 11:31:09'),
(274, 1, 'RequestBooking', 'Request', '2013-06-11 11:39:06'),
(275, 1, 'RequestBooking', 'Request', '2013-06-11 11:39:32'),
(276, 1, 'RequestBooking', 'Request', '2013-06-11 11:40:19'),
(277, 1, 'RequestBooking', 'Request', '2013-06-11 11:41:40'),
(278, 1, 'RequestBooking', 'Request', '2013-06-11 11:43:18'),
(279, 1, 'RequestBooking', 'Request', '2013-06-11 11:43:24'),
(280, 1, 'RequestBooking', 'Request', '2013-06-11 11:53:32'),
(281, 1, 'RequestBooking', 'Request', '2013-06-11 11:55:15'),
(282, 1, 'RequestBooking', 'Request', '2013-06-11 11:57:30'),
(283, 1, 'RequestBooking', 'Request', '2013-06-11 11:58:02'),
(284, 1, 'RequestBooking', 'Request', '2013-06-11 12:00:51'),
(285, 1, 'RequestBorrow', '', '2013-06-11 12:42:57'),
(286, 1, 'RequestBorrow', 'Request', '2013-06-11 12:42:59'),
(287, 1, 'RequestBorrow', 'Request', '2013-06-11 12:50:47'),
(288, 1, 'RequestBorrow', 'Request', '2013-06-11 12:50:51'),
(289, 1, 'RequestBorrow', 'Request', '2013-06-11 12:50:52'),
(290, 1, 'RequestBorrow', 'Request', '2013-06-11 12:50:56'),
(291, 1, 'RequestBorrow', 'Request', '2013-06-11 12:51:01'),
(292, 1, 'RequestBorrow', 'Request', '2013-06-11 12:52:08'),
(293, 1, 'RequestBorrow', 'Request', '2013-06-11 12:52:09'),
(294, 1, 'RequestBorrow', 'Request', '2013-06-11 12:52:53'),
(295, 1, 'RequestBorrow', 'Request', '2013-06-11 12:53:48'),
(296, 1, 'RequestBorrow', 'View', '2013-06-11 12:54:32'),
(297, 1, 'RequestService', '', '2013-06-11 12:55:19'),
(298, 1, 'RequestService', 'Request', '2013-06-11 12:55:21'),
(299, NULL, 'management', 'login', '2013-06-13 10:48:09'),
(300, 1, 'RequestBooking', '', '2013-06-13 10:48:26'),
(301, 1, 'Solution', '', '2013-06-13 10:48:30'),
(302, 1, 'RequestService', '', '2013-06-13 10:48:31'),
(303, 1, 'RequestService', 'Request', '2013-06-13 10:48:33'),
(304, 1, 'Solution', '', '2013-06-13 10:48:37'),
(305, 1, 'RequestService', '', '2013-06-13 10:48:42'),
(306, 1, 'RequestBorrow', '', '2013-06-13 10:48:45'),
(307, 1, 'RequestBorrow', 'Request', '2013-06-13 10:48:46'),
(308, 1, 'RequestService', '', '2013-06-13 10:48:51'),
(309, 1, 'RequestService', 'Request', '2013-06-13 10:48:53'),
(310, 1, 'RequestService', '', '2013-06-13 10:50:03'),
(311, 1, 'requestService', 'view', '2013-06-13 10:50:05'),
(312, 1, 'RequestService', '', '2013-06-13 10:50:11'),
(313, 1, 'RequestService', 'Request', '2013-06-13 10:50:12'),
(314, 1, 'RequestService', '', '2013-06-13 11:03:18'),
(315, 1, 'RequestService', 'Request', '2013-06-13 11:04:04'),
(316, 1, 'RequestService', 'Request', '2013-06-13 11:04:05'),
(317, 1, 'RequestService', 'View', '2013-06-13 11:04:17'),
(318, 1, 'RequestService', '', '2013-06-13 11:04:23'),
(319, 1, 'RequestService', 'Request', '2013-06-13 11:09:05'),
(320, 1, 'RequestService', 'Request', '2013-06-13 11:14:02'),
(321, 1, 'RequestService', 'View', '2013-06-13 11:14:46'),
(322, NULL, 'management', 'login', '2013-06-13 20:35:52'),
(323, 1, 'RequestBooking', '', '2013-06-13 20:35:55'),
(324, 1, 'RequestBorrow', '', '2013-06-13 20:36:06'),
(325, 1, 'RequestBorrow', 'Request', '2013-06-13 20:36:07'),
(326, 1, 'RequestBorrow', 'View', '2013-06-13 20:37:12'),
(327, NULL, 'management', 'login', '2013-06-14 08:32:55'),
(328, NULL, 'management', 'login', '2013-06-15 11:12:00'),
(329, 1, 'RequestBooking', '', '2013-06-15 11:12:03'),
(330, 1, 'RequestBooking', 'Request', '2013-06-15 11:12:05'),
(331, 1, 'RequestBooking', 'Request', '2013-06-15 11:12:33'),
(332, 1, 'RequestBooking', 'Request', '2013-06-15 11:13:08'),
(333, 1, 'RequestBooking', 'Request', '2013-06-15 11:13:30'),
(334, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:15:49'),
(335, 1, 'RequestBorrow', '', '2013-06-15 11:16:02'),
(336, 1, 'requestBorrow', 'view', '2013-06-15 11:16:09'),
(337, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:18:37'),
(338, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:19:31'),
(339, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:26:07'),
(340, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:26:16'),
(341, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:27:07'),
(342, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:27:12'),
(343, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:27:13'),
(344, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:27:25'),
(345, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:27:33'),
(346, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:28:16'),
(347, NULL, 'management', 'login', '2013-06-15 11:28:26'),
(348, NULL, 'RequestBorrow', 'Approve', '2013-06-15 11:28:27'),
(349, 1, 'RequestBooking', '', '2013-06-15 11:28:32'),
(350, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:28:33'),
(351, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 11:34:05'),
(352, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 11:34:22'),
(353, 1, 'RequestBorrow', '', '2013-06-15 11:34:31'),
(354, 1, 'RequestBorrow', '', '2013-06-15 11:34:33'),
(355, 1, 'RequestBorrow', 'Request', '2013-06-15 12:12:04'),
(356, 1, 'RequestBorrow', 'Request', '2013-06-15 12:13:43'),
(357, 1, 'RequestBorrow', 'Request', '2013-06-15 12:14:08'),
(358, 1, 'RequestBorrow', 'View', '2013-06-15 12:14:32'),
(359, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 12:15:17'),
(360, 1, 'RequestBorrow', 'Approve', '2013-06-15 12:15:30'),
(361, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 12:15:50'),
(362, 1, 'RequestBorrow', 'Request', '2013-06-15 12:16:22'),
(363, 1, 'RequestBorrow', 'Request', '2013-06-15 12:22:06'),
(364, 1, 'RequestBorrow', 'Request', '2013-06-15 12:22:24'),
(365, 1, 'RequestBorrow', 'Request', '2013-06-15 12:23:51'),
(366, 1, 'RequestBorrow', 'Request', '2013-06-15 12:25:48'),
(367, 1, 'RequestBorrow', 'Request', '2013-06-15 12:26:47'),
(368, 1, 'RequestBorrow', 'View', '2013-06-15 12:27:19'),
(369, 1, 'RequestBorrow', 'Approve', '2013-06-15 12:27:41'),
(370, 1, 'RequestBorrow', 'View', '2013-06-15 12:28:43'),
(371, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 12:29:00'),
(372, 1, 'RequestBorrow', 'View', '2013-06-15 12:29:02'),
(373, 1, 'RequestBorrow', 'View', '2013-06-15 12:29:03'),
(374, 1, 'RequestBorrow', 'Request', '2013-06-15 12:31:02'),
(375, 1, 'RequestBorrow', 'View', '2013-06-15 12:31:32'),
(376, 1, 'RequestBorrow', 'Approve', '2013-06-15 12:31:52'),
(377, 1, 'RequestBorrow', 'View', '2013-06-15 12:32:00'),
(378, 1, 'RequestBorrow', 'View', '2013-06-15 12:32:19'),
(379, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 12:35:24'),
(380, 1, 'RequestBorrow', 'View', '2013-06-15 12:35:27'),
(381, 1, 'RequestBorrow', 'Request', '2013-06-15 12:36:26'),
(382, 1, 'RequestBorrow', 'View', '2013-06-15 12:36:49'),
(383, 1, 'RequestBorrow', 'Approve', '2013-06-15 12:37:03'),
(384, 1, 'RequestBorrow', 'View', '2013-06-15 12:37:05'),
(385, 1, 'RequestBorrow', 'Disapprove', '2013-06-15 12:37:22'),
(386, 1, 'RequestBorrow', 'View', '2013-06-15 12:37:27'),
(387, 1, 'RequestBooking', '', '2013-06-15 12:41:21'),
(388, 1, 'RequestBooking', '', '2013-06-15 12:50:03'),
(389, 1, 'RequestBooking', 'Request', '2013-06-15 12:50:05'),
(390, 1, 'RequestBooking', 'View', '2013-06-15 12:50:23'),
(391, 1, 'RequestBooking', '', '2013-06-15 12:50:31'),
(392, 1, 'RequestBooking', 'Request', '2013-06-15 12:51:17'),
(393, NULL, 'management', 'login', '2013-06-15 12:51:34'),
(394, 1, 'RequestBooking', '', '2013-06-15 12:51:35'),
(395, 1, 'RequestBooking', 'Request', '2013-06-15 12:51:38'),
(396, 1, 'RequestBooking', 'View', '2013-06-15 12:51:47'),
(397, 1, 'RequestBooking', 'Request', '2013-06-15 12:52:25'),
(398, 1, 'RequestBooking', 'Request', '2013-06-15 12:52:51'),
(399, 1, 'RequestBooking', 'View', '2013-06-15 12:53:12'),
(400, 1, 'RequestBooking', '', '2013-06-15 12:53:26'),
(401, 1, 'RequestBooking', '', '2013-06-15 12:53:29'),
(402, 1, 'RequestBooking', '', '2013-06-15 12:54:34'),
(403, 1, 'RequestBooking', 'Index', '2013-06-15 12:54:37'),
(404, 1, 'RequestBooking', 'Index', '2013-06-15 12:54:39'),
(405, 1, 'RequestBooking', 'Index', '2013-06-15 12:54:42'),
(406, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:26'),
(407, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:33'),
(408, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:37'),
(409, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:44'),
(410, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:45'),
(411, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:47'),
(412, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:48'),
(413, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:49'),
(414, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:54'),
(415, 1, 'RequestBooking', '', '2013-06-15 12:56:19'),
(416, 1, 'RequestBooking', 'CheckStatus', '2013-06-15 12:56:21'),
(417, 1, 'RequestBooking', '', '2013-06-15 12:56:22'),
(418, 1, 'RequestBooking', 'Request', '2013-06-15 12:56:26'),
(419, 1, 'RequestBooking', '', '2013-06-15 12:56:27'),
(420, 1, 'RequestBooking', 'Index', '2013-06-15 12:56:30'),
(421, 1, 'requestBooking', 'view', '2013-06-15 12:56:32'),
(422, 1, 'RequestBooking', '', '2013-06-15 12:56:34'),
(423, 1, 'RequestBooking', 'Index', '2013-06-15 12:56:36'),
(424, 1, 'RequestBooking', 'Index', '2013-06-15 12:56:38'),
(425, 1, 'requestBooking', 'view', '2013-06-15 12:56:47'),
(426, 1, 'RequestBooking', 'update', '2013-06-15 12:56:49'),
(427, 1, 'RequestBooking', 'update', '2013-06-15 12:56:56'),
(428, 1, 'RequestBooking', 'update', '2013-06-15 12:57:04'),
(429, 1, 'RequestBooking', '', '2013-06-15 12:57:52'),
(430, 1, 'RequestBooking', 'Index', '2013-06-15 12:57:55'),
(431, 1, 'RequestBooking', 'Index', '2013-06-15 12:57:57'),
(432, 1, 'RequestBooking', 'Index', '2013-06-15 12:58:01'),
(433, 1, 'RequestBooking', 'Index', '2013-06-15 12:58:03'),
(434, 1, 'RequestBooking', 'Index', '2013-06-15 12:58:05'),
(435, 1, 'RequestBooking', '', '2013-06-15 12:58:53'),
(436, 1, 'RequestBooking', '', '2013-06-15 12:59:15'),
(437, 1, 'RequestBooking', '', '2013-06-15 12:59:29'),
(438, 1, 'RequestBooking', '', '2013-06-15 12:59:42'),
(439, 1, 'RequestBooking', '', '2013-06-15 13:00:00'),
(440, 1, 'RequestBooking', 'Index', '2013-06-15 13:00:04'),
(441, 1, 'RequestBooking', '', '2013-06-15 13:00:13'),
(442, 1, 'RequestBooking', '', '2013-06-15 13:01:07'),
(443, 1, 'RequestBooking', 'Index', '2013-06-15 13:01:11'),
(444, 1, 'RequestBooking', 'Index', '2013-06-15 13:01:13'),
(445, 1, 'RequestBooking', 'Index', '2013-06-15 13:01:16'),
(446, 1, 'RequestBooking', '', '2013-06-15 13:01:29'),
(447, 1, 'RequestBooking', '', '2013-06-15 13:01:31'),
(448, 1, 'RequestBooking', 'Index', '2013-06-15 13:01:33'),
(449, 1, 'RequestBooking', 'Index', '2013-06-15 13:01:45'),
(450, 1, 'RequestBooking', '', '2013-06-15 13:02:04'),
(451, 1, 'RequestBooking', '', '2013-06-15 13:02:06'),
(452, 1, 'RequestBooking', '', '2013-06-15 13:02:08'),
(453, 1, 'RequestBooking', '', '2013-06-15 13:03:12'),
(454, 1, 'RequestBooking', '', '2013-06-15 13:03:22'),
(455, 1, 'RequestBooking', '', '2013-06-15 13:03:58'),
(456, 1, 'RequestBooking', '', '2013-06-15 13:04:09'),
(457, 1, 'RequestBooking', '', '2013-06-15 13:04:10'),
(458, 1, 'RequestBooking', '', '2013-06-15 13:04:18'),
(459, 1, 'RequestBooking', 'Index', '2013-06-15 13:04:27'),
(460, 1, 'RequestBooking', 'Index', '2013-06-15 13:04:31'),
(461, 1, 'RequestBooking', 'Index', '2013-06-15 13:04:33'),
(462, 1, 'RequestBooking', '', '2013-06-15 13:06:20'),
(463, 1, 'RequestBooking', '', '2013-06-15 13:06:22'),
(464, 1, 'RequestBooking', '', '2013-06-15 13:06:23'),
(465, 1, 'RequestBooking', '', '2013-06-15 13:06:23'),
(466, 1, 'RequestBooking', '', '2013-06-15 13:06:23'),
(467, 1, 'RequestBooking', '', '2013-06-15 13:06:36'),
(468, 1, 'RequestBooking', '', '2013-06-15 13:17:17'),
(469, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:17:20'),
(470, 1, 'RequestBooking', '', '2013-06-15 13:17:39'),
(471, 1, 'RequestBooking', '', '2013-06-15 13:17:41'),
(472, 1, 'RequestBooking', '', '2013-06-15 13:17:49'),
(473, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:17:57'),
(474, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:17:58'),
(475, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:17:59'),
(476, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:18:00'),
(477, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:18:00'),
(478, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:18:00'),
(479, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:18:01'),
(480, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:18'),
(481, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:25'),
(482, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:58'),
(483, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:59'),
(484, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:51:58'),
(485, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:56:17'),
(486, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:56:30'),
(487, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:56:31'),
(488, 1, 'management', 'CheckAlertToTurnItemBack', '2013-06-15 14:14:29'),
(489, 1, 'management', 'CheckAlertToTurnItemBack', '2013-06-15 14:15:29'),
(490, 1, 'management', 'CheckAlertToTurnItemBack', '2013-06-15 14:16:33'),
(491, 1, 'management', 'CheckAlertToTurnItemBack', '2013-06-15 14:16:37'),
(492, 1, 'management', 'CheckAlertToTurnItemBack', '2013-06-15 14:16:38'),
(493, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 14:19:16'),
(494, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 14:19:59'),
(495, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 14:19:59'),
(496, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 14:20:01'),
(497, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:27'),
(498, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:36'),
(499, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:46'),
(500, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:51:01'),
(501, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:53:18'),
(502, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:54:29'),
(503, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:55:29'),
(504, NULL, '', '', '2013-06-16 10:42:23'),
(505, NULL, 'management', 'login', '2013-06-16 10:42:50'),
(506, NULL, 'management', 'login', '2013-06-16 10:42:55'),
(507, 1, 'RequestBooking', '', '2013-06-16 10:43:03'),
(508, 1, 'Admin', '', '2013-06-16 10:43:06'),
(509, 1, 'Report', '', '2013-06-16 10:43:07'),
(510, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-16 10:43:14'),
(511, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-16 10:43:26'),
(512, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-16 10:43:29'),
(513, 1, 'Admin', '', '2013-06-16 10:43:53'),
(514, 1, 'Room', '', '2013-06-16 10:43:57'),
(515, 1, 'EquipmentType', '', '2013-06-16 10:44:00'),
(516, 1, 'equipmentType', 'update', '2013-06-16 10:44:06'),
(517, 1, 'EquipmentType', '', '2013-06-16 10:44:18'),
(518, 1, 'equipmentType', 'update', '2013-06-16 10:44:20'),
(519, 1, 'EquipmentType', '', '2013-06-16 10:44:30'),
(520, 1, 'equipmentType', 'update', '2013-06-16 10:44:32'),
(521, 1, 'EquipmentType', '', '2013-06-16 10:44:45'),
(522, 1, 'equipmentType', 'update', '2013-06-16 10:44:47'),
(523, 1, 'EquipmentType', '', '2013-06-16 10:44:59'),
(524, 1, 'equipmentType', 'update', '2013-06-16 10:45:08'),
(525, 1, 'EquipmentType', '', '2013-06-16 10:45:21'),
(526, 1, 'equipmentType', 'update', '2013-06-16 10:45:23'),
(527, 1, 'EquipmentType', '', '2013-06-16 10:45:38'),
(528, 1, 'equipmentType', 'update', '2013-06-16 10:45:41'),
(529, 1, 'EquipmentType', '', '2013-06-16 10:45:46'),
(530, 1, 'EquipmentType', '', '2013-06-16 10:45:47'),
(531, 1, 'equipmentType', 'update', '2013-06-16 10:45:49'),
(532, 1, 'EquipmentType', '', '2013-06-16 10:45:57'),
(533, 1, 'equipmentType', 'update', '2013-06-16 10:46:02'),
(534, 1, 'EquipmentType', '', '2013-06-16 10:46:14'),
(535, 1, 'EquipmentType', 'create', '2013-06-16 10:46:16'),
(536, 1, 'EquipmentType', '', '2013-06-16 10:46:20'),
(537, 1, 'EquipmentType', 'create', '2013-06-16 10:46:28'),
(538, 1, 'EquipmentType', '', '2013-06-16 10:46:53'),
(539, 1, 'Equipment', '', '2013-06-16 10:47:02'),
(540, 1, 'Equipment', '', '2013-06-16 11:40:44'),
(541, 1, 'Room', '', '2013-06-16 11:40:46'),
(542, 1, 'Equipment', '', '2013-06-16 11:41:14'),
(543, NULL, '', '', '2013-06-16 12:50:19'),
(544, 1, 'RequestBooking', '', '2013-06-16 12:50:23'),
(545, 1, 'Admin', '', '2013-06-16 12:50:30'),
(546, 1, 'Equipment', '', '2013-06-16 12:50:33'),
(547, 1, 'Solution', '', '2013-06-16 12:50:39'),
(548, 1, 'Solution', 'Index', '2013-06-16 12:50:43'),
(549, 1, 'Solution', 'Index', '2013-06-16 12:50:54'),
(550, 1, 'RequestBooking', '', '2013-06-16 12:51:07'),
(551, 1, 'RequestBooking', 'Request', '2013-06-16 12:51:08'),
(552, 1, 'RequestBooking', 'Request', '2013-06-16 12:51:14'),
(553, 1, 'RequestBooking', '', '2013-06-16 12:51:20'),
(554, 1, 'RequestBooking', 'Request', '2013-06-16 12:51:23'),
(555, NULL, '', '', '2013-06-16 22:22:35'),
(556, 2, 'RequestBooking', '', '2013-06-16 22:22:40'),
(557, 2, 'RequestBooking', 'CheckStatus', '2013-06-16 22:22:47'),
(558, 2, 'RequestBooking', '', '2013-06-16 22:22:49'),
(559, 2, 'RequestBooking', 'Index', '2013-06-16 22:22:50'),
(560, 2, 'RequestBooking', 'Request', '2013-06-16 22:22:55'),
(561, 2, 'RequestBooking', 'View', '2013-06-16 22:23:57'),
(562, 2, 'RequestBooking', '', '2013-06-16 22:23:59'),
(563, 2, 'RequestBooking', 'Request', '2013-06-16 22:24:04'),
(564, 2, 'RequestBooking', 'View', '2013-06-16 22:24:42'),
(565, 2, 'RequestBooking', '', '2013-06-16 22:27:07'),
(566, 2, 'RequestBooking', 'Request', '2013-06-16 22:27:09'),
(567, 2, 'RequestBorrow', '', '2013-06-16 22:27:28'),
(568, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:27:30'),
(569, 2, 'RequestBooking', '', '2013-06-16 22:27:38'),
(570, 2, 'RequestBooking', 'Request', '2013-06-16 22:27:39'),
(571, 2, 'RequestBorrow', '', '2013-06-16 22:28:24'),
(572, 2, 'RequestBorrow', 'Request', '2013-06-16 22:28:26'),
(573, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:28:40'),
(574, 2, 'RequestBorrow', 'Request', '2013-06-16 22:29:56'),
(575, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:30:01'),
(576, 2, 'RequestBorrow', 'Request', '2013-06-16 22:30:05'),
(577, 2, 'RequestService', '', '2013-06-16 22:30:07'),
(578, 2, 'RequestService', 'Request', '2013-06-16 22:30:08'),
(579, 2, 'RequestBorrow', '', '2013-06-16 22:30:10'),
(580, 2, 'RequestBorrow', 'Request', '2013-06-16 22:30:11'),
(581, 2, 'RequestBorrow', 'Request', '2013-06-16 22:30:40'),
(582, 2, 'RequestBorrow', 'Request', '2013-06-16 22:31:06'),
(583, 2, 'RequestBorrow', '', '2013-06-16 22:31:45'),
(584, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:31:47'),
(585, 2, 'RequestBorrow', 'Request', '2013-06-16 22:31:49'),
(586, 2, 'RequestBooking', '', '2013-06-16 22:33:51'),
(587, 2, 'RequestBooking', 'Request', '2013-06-16 22:33:53'),
(588, 2, 'RequestBooking', 'Request', '2013-06-16 22:34:37'),
(589, 2, 'RequestBooking', 'Request', '2013-06-16 22:35:02'),
(590, 2, 'RequestBooking', 'View', '2013-06-16 22:35:30'),
(591, 2, 'RequestBooking', '', '2013-06-16 22:35:32'),
(592, 2, 'RequestBooking', 'Index', '2013-06-16 22:35:34'),
(593, 2, 'RequestBooking', 'Index', '2013-06-16 22:35:38'),
(594, 2, 'RequestBooking', 'Index', '2013-06-16 22:35:42'),
(595, 2, 'RequestService', '', '2013-06-16 22:35:46'),
(596, 2, 'RequestService', 'Request', '2013-06-16 22:35:48'),
(597, 2, 'RequestService', 'View', '2013-06-16 22:36:01'),
(598, 2, 'RequestService', '', '2013-06-16 22:36:03'),
(599, 2, 'RequestService', 'Request', '2013-06-16 22:36:12'),
(600, 2, 'Admin', '', '2013-06-16 22:36:15'),
(601, 2, 'ServiceTypeItem', '', '2013-06-16 22:36:17'),
(602, 2, 'EquipmentType', '', '2013-06-16 22:36:18'),
(603, 2, 'Equipment', '', '2013-06-16 22:36:19'),
(604, 2, 'Department', '', '2013-06-16 22:36:20'),
(605, 2, 'Room', '', '2013-06-16 22:36:22'),
(606, 2, 'Semester', '', '2013-06-16 22:36:23'),
(607, 2, 'User', '', '2013-06-16 22:36:24'),
(608, 2, 'user', 'update', '2013-06-16 22:37:43'),
(609, 2, 'User', '', '2013-06-16 22:37:50'),
(610, 2, 'RequestBorrow', '', '2013-06-16 22:38:29'),
(611, 2, 'RequestBooking', '', '2013-06-16 22:38:31'),
(612, 2, 'RequestBooking', 'Request', '2013-06-16 22:38:32'),
(613, 2, 'RequestBorrow', '', '2013-06-16 22:45:50'),
(614, 2, 'RequestBooking', '', '2013-06-16 22:45:52'),
(615, 2, 'RequestBooking', 'CheckStatus', '2013-06-16 22:45:54'),
(616, 2, 'RequestBooking', 'Request', '2013-06-16 22:45:56'),
(617, 2, 'RequestBorrow', '', '2013-06-16 22:46:08'),
(618, 2, 'RequestBorrow', 'Request', '2013-06-16 22:46:10'),
(619, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:46:11'),
(620, 2, 'RequestBorrow', 'request', '2013-06-16 22:46:48'),
(621, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:46:58'),
(622, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:02'),
(623, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:08'),
(624, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:12'),
(625, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:14'),
(626, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:16'),
(627, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:18'),
(628, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:20'),
(629, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:22'),
(630, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:27'),
(631, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:29'),
(632, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:31'),
(633, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:33'),
(634, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:41'),
(635, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:43'),
(636, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:46'),
(637, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:48'),
(638, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:52'),
(639, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:54'),
(640, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:59'),
(641, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:48:01'),
(642, 2, 'RequestBorrow', 'request', '2013-06-16 22:48:02'),
(643, 2, 'RequestBorrow', 'Request', '2013-06-16 22:48:05'),
(644, 2, 'RequestService', '', '2013-06-16 22:48:11'),
(645, 2, 'Admin', '', '2013-06-16 22:48:13'),
(646, 2, 'User', '', '2013-06-16 22:59:42'),
(647, 2, 'Room', '', '2013-06-16 23:04:22'),
(648, 2, 'User', '', '2013-06-16 23:04:26'),
(649, 2, 'user', 'update', '2013-06-16 23:04:28'),
(650, 2, 'User', '', '2013-06-16 23:04:30'),
(651, 2, 'user', 'view', '2013-06-16 23:04:34'),
(652, 2, 'user', '', '2013-06-16 23:04:36'),
(653, 2, 'Room', '', '2013-06-16 23:04:39'),
(654, 2, 'Semester', '', '2013-06-16 23:04:47'),
(655, 2, 'Department', '', '2013-06-16 23:04:50'),
(656, 2, 'department', 'view', '2013-06-16 23:04:54'),
(657, 2, 'Department', '', '2013-06-16 23:04:55'),
(658, NULL, 'management', 'login', '2013-06-16 23:05:08'),
(659, 1, 'RequestBooking', '', '2013-06-16 23:05:16'),
(660, 1, 'Admin', '', '2013-06-16 23:05:18'),
(661, 1, 'User', '', '2013-06-16 23:05:21'),
(662, 1, 'user', 'delete', '2013-06-16 23:05:24'),
(663, 1, 'User', '', '2013-06-16 23:05:24'),
(664, 1, 'user', 'delete', '2013-06-16 23:05:27'),
(665, 1, 'User', '', '2013-06-16 23:05:27'),
(666, 1, 'User', '', '2013-06-16 23:06:24'),
(667, 1, 'user', 'delete', '2013-06-16 23:06:28'),
(668, 1, 'User', '', '2013-06-16 23:06:28'),
(669, 1, 'user', 'delete', '2013-06-16 23:06:31'),
(670, 1, 'User', '', '2013-06-16 23:06:31'),
(671, 1, 'user', 'delete', '2013-06-16 23:06:33'),
(672, 1, 'User', '', '2013-06-16 23:06:33'),
(673, 1, 'user', 'delete', '2013-06-16 23:06:36'),
(674, 1, 'User', '', '2013-06-16 23:06:36'),
(675, 1, 'user', 'delete', '2013-06-16 23:06:38'),
(676, 1, 'User', '', '2013-06-16 23:06:38'),
(677, 1, 'user', 'delete', '2013-06-16 23:06:40'),
(678, 1, 'User', '', '2013-06-16 23:06:41'),
(679, 1, 'user', 'delete', '2013-06-16 23:06:42'),
(680, 1, 'User', '', '2013-06-16 23:06:43'),
(681, 1, 'ConfirmUser', '', '2013-06-16 23:06:45'),
(682, 1, 'admin', '', '2013-06-16 23:06:46'),
(683, 1, 'ConfirmUser', '', '2013-06-16 23:06:48'),
(684, 1, 'User', '', '2013-06-16 23:06:49'),
(685, 1, 'User', 'Staff', '2013-06-16 23:06:51'),
(686, 1, 'User', '', '2013-06-16 23:06:55'),
(687, 1, 'Role', '', '2013-06-16 23:06:59'),
(688, 1, 'User', 'Staff', '2013-06-16 23:07:01'),
(689, 1, 'User', '', '2013-06-16 23:07:11'),
(690, 1, 'user', 'create', '2013-06-16 23:07:17'),
(691, 1, 'user', 'create', '2013-06-16 23:07:38'),
(692, 1, 'user', 'create', '2013-06-16 23:08:00'),
(693, 1, 'RequestBooking', '', '2013-06-16 23:08:11'),
(694, 1, 'Admin', '', '2013-06-16 23:08:13'),
(695, 1, 'User', '', '2013-06-16 23:08:14'),
(696, 1, 'user', 'delete', '2013-06-16 23:08:26'),
(697, 1, 'User', '', '2013-06-16 23:08:26'),
(698, 1, 'user', 'update', '2013-06-16 23:08:28'),
(699, 1, 'user', 'view', '2013-06-16 23:08:44'),
(700, 1, 'user', '', '2013-06-16 23:08:46'),
(701, 1, 'user', 'update', '2013-06-16 23:08:55'),
(702, 1, 'user', 'view', '2013-06-16 23:09:08'),
(703, 1, 'user', '', '2013-06-16 23:09:09'),
(704, NULL, 'management', 'login', '2013-06-16 23:09:14'),
(705, NULL, 'Management', 'Register', '2013-06-16 23:09:15'),
(706, NULL, 'Management', 'Register', '2013-06-16 23:09:41'),
(707, 1, 'RequestBooking', '', '2013-06-16 23:09:52'),
(708, 1, 'Admin', '', '2013-06-16 23:09:54'),
(709, 1, 'User', '', '2013-06-16 23:09:56'),
(710, 1, 'RequestBorrow', '', '2013-06-16 23:09:57'),
(711, 1, 'RequestBorrow', 'Request', '2013-06-16 23:09:58'),
(712, 1, 'RequestBorrow', 'Request', '2013-06-16 23:10:33'),
(713, 1, 'RequestBorrow', 'Request', '2013-06-16 23:11:00'),
(714, 1, 'RequestService', '', '2013-06-16 23:11:11'),
(715, 1, 'RequestBorrow', '', '2013-06-16 23:11:15'),
(716, 1, 'Solution', '', '2013-06-16 23:12:11'),
(717, 1, 'Solution', 'Index', '2013-06-16 23:12:14'),
(718, 1, 'Solution', 'Index', '2013-06-16 23:12:15'),
(719, 1, 'Solution', 'Index', '2013-06-16 23:12:22'),
(720, 1, 'Solution', 'Index', '2013-06-16 23:12:23'),
(721, 1, 'RequestService', '', '2013-06-16 23:12:25'),
(722, 1, 'RequestBooking', '', '2013-06-16 23:12:26'),
(723, 1, 'RequestBooking', 'Request', '2013-06-16 23:12:29'),
(724, 1, 'RequestBooking', 'CheckStatus', '2013-06-16 23:12:31'),
(725, 1, 'RequestBooking', '', '2013-06-16 23:12:32'),
(726, 1, 'RequestBooking', 'Index', '2013-06-16 23:12:34'),
(727, 1, 'RequestBooking', 'Request', '2013-06-16 23:12:38'),
(728, 1, 'RequestBooking', 'Request', '2013-06-16 23:12:57'),
(729, 1, 'RequestBooking', 'View', '2013-06-16 23:13:26'),
(730, 1, 'RequestBooking', '', '2013-06-16 23:13:28'),
(731, 1, 'RequestBorrow', '', '2013-06-16 23:13:29'),
(732, 1, 'RequestBorrow', 'Request', '2013-06-16 23:13:30'),
(733, 1, 'RequestBorrow', 'Request', '2013-06-16 23:13:49'),
(734, 1, 'RequestBorrow', 'Request', '2013-06-16 23:21:41'),
(735, 1, 'RequestBorrow', 'Request', '2013-06-16 23:22:01'),
(736, 1, 'RequestBorrow', 'View', '2013-06-16 23:25:56'),
(737, 1, 'RequestBorrow', '', '2013-06-16 23:26:04'),
(738, 1, 'RequestBorrow', 'Request', '2013-06-16 23:26:06'),
(739, 1, 'RequestBorrow', 'View', '2013-06-16 23:26:36'),
(740, 1, 'RequestBorrow', '', '2013-06-16 23:26:38'),
(741, 1, 'RequestBorrow', '', '2013-06-16 23:26:52'),
(742, 1, 'RequestBorrow', 'Index', '2013-06-16 23:26:56'),
(743, 1, 'RequestBorrow', 'CheckStatus', '2013-06-16 23:27:02'),
(744, NULL, 'management', 'login', '2013-06-16 23:46:43'),
(745, 3, 'RequestBooking', '', '2013-06-16 23:46:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `department_code` (`department_code`,`name`);

--
-- Indexes for table `enumeration`
--
ALTER TABLE `enumeration`
 ADD PRIMARY KEY (`id`), ADD KEY `enumeration_type_id` (`enumeration_type_code`);

--
-- Indexes for table `enumeration_type`
--
ALTER TABLE `enumeration_type`
 ADD PRIMARY KEY (`code`);

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
 ADD PRIMARY KEY (`id`), ADD KEY `equipment_type_id` (`equipment_type_id`), ADD KEY `room_id` (`room_id`);

--
-- Indexes for table `equipment_cracked_log`
--
ALTER TABLE `equipment_cracked_log`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipment_type`
--
ALTER TABLE `equipment_type`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `equipment_type_code` (`equipment_type_code`,`name`);

--
-- Indexes for table `equipment_type_list`
--
ALTER TABLE `equipment_type_list`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_group_idx` (`equipment_type_id`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `link`
--
ALTER TABLE `link`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `period`
--
ALTER TABLE `period`
 ADD PRIMARY KEY (`id`), ADD KEY `status_code` (`status_code`), ADD KEY `preriod_group_id` (`period_group_id`);

--
-- Indexes for table `period_group`
--
ALTER TABLE `period_group`
 ADD PRIMARY KEY (`id`), ADD KEY `status_code` (`status_code`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
 ADD PRIMARY KEY (`permission_code`), ADD UNIQUE KEY `name` (`name`), ADD KEY `permission_group_id` (`permission_group_id`);

--
-- Indexes for table `permission_group`
--
ALTER TABLE `permission_group`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `position_code` (`position_code`);

--
-- Indexes for table `present_type`
--
ALTER TABLE `present_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_booking`
--
ALTER TABLE `request_booking`
 ADD PRIMARY KEY (`id`), ADD KEY `user_login_id` (`user_login_id`), ADD KEY `room_id` (`room_id`), ADD KEY `period_start` (`period_start`), ADD KEY `period_end` (`period_end`), ADD KEY `status_code` (`status_code`), ADD KEY `semester_id` (`semester_id`), ADD KEY `request_booking_type_id` (`request_booking_type_id`);

--
-- Indexes for table `request_booking_activity_detail`
--
ALTER TABLE `request_booking_activity_detail`
 ADD PRIMARY KEY (`id`), ADD KEY `event_type_id` (`event_type_id`), ADD KEY `room_type_id` (`room_type_id`);

--
-- Indexes for table `request_booking_activity_present_type`
--
ALTER TABLE `request_booking_activity_present_type`
 ADD PRIMARY KEY (`id`), ADD KEY `request_booking_activity_id` (`request_booking_activity_id`), ADD KEY `present_type_id` (`present_type_id`);

--
-- Indexes for table `request_booking_alert_log`
--
ALTER TABLE `request_booking_alert_log`
 ADD PRIMARY KEY (`id`), ADD KEY `request_booking_id` (`request_booking_id`);

--
-- Indexes for table `request_booking_equipment_type`
--
ALTER TABLE `request_booking_equipment_type`
 ADD PRIMARY KEY (`id`), ADD KEY `request_booking_id` (`request_booking_id`), ADD KEY `equipment_type_id` (`equipment_type_id`);

--
-- Indexes for table `request_booking_type`
--
ALTER TABLE `request_booking_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_borrow`
--
ALTER TABLE `request_borrow`
 ADD PRIMARY KEY (`id`), ADD KEY `event_type_id` (`event_type_id`), ADD KEY `user_login_id` (`user_login_id`), ADD KEY `status_code` (`status_code`);

--
-- Indexes for table `request_borrow_alert_log`
--
ALTER TABLE `request_borrow_alert_log`
 ADD PRIMARY KEY (`id`), ADD KEY `request_borrow_id` (`request_borrow_id`);

--
-- Indexes for table `request_borrow_approve_link`
--
ALTER TABLE `request_borrow_approve_link`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_borrow_equipment_type`
--
ALTER TABLE `request_borrow_equipment_type`
 ADD PRIMARY KEY (`id`), ADD KEY `request_borrow_id` (`request_borrow_id`), ADD KEY `request_borrow_equipment_type_ibfk_4_idx` (`equipment_type_list_id`);

--
-- Indexes for table `request_borrow_equipment_type_item`
--
ALTER TABLE `request_borrow_equipment_type_item`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_service`
--
ALTER TABLE `request_service`
 ADD PRIMARY KEY (`id`), ADD KEY `user_login_id` (`user_login_id`), ADD KEY `time_period` (`time_period`), ADD KEY `status_code` (`status_code`);

--
-- Indexes for table `request_service_detail`
--
ALTER TABLE `request_service_detail`
 ADD PRIMARY KEY (`id`), ADD KEY `request_service_id` (`request_service_id`), ADD KEY `request_service_type_detail_id` (`request_service_type_detail_id`);

--
-- Indexes for table `request_service_type`
--
ALTER TABLE `request_service_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_service_type_detail`
--
ALTER TABLE `request_service_type_detail`
 ADD PRIMARY KEY (`id`), ADD KEY `request_service_type_id` (`request_service_type_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `role_id` (`role_id`,`permission_code`), ADD KEY `permission_id` (`permission_code`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `room_code` (`room_code`), ADD KEY `room_type_id` (`room_group_id`);

--
-- Indexes for table `room_equipment`
--
ALTER TABLE `room_equipment`
 ADD PRIMARY KEY (`id`), ADD KEY `room_id` (`room_id`), ADD KEY `equipment_id` (`equipment_id`);

--
-- Indexes for table `room_group`
--
ALTER TABLE `room_group`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_staff`
--
ALTER TABLE `room_staff`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `room_id` (`room_id`,`staff_id`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
 ADD PRIMARY KEY (`status_code`), ADD KEY `status_group_id` (`status_group_id`);

--
-- Indexes for table `status_group`
--
ALTER TABLE `status_group`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_email_forapprove`
--
ALTER TABLE `tb_email_forapprove`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_setting`
--
ALTER TABLE `tb_setting`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_sky_notification`
--
ALTER TABLE `tb_sky_notification`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_forget_password_request`
--
ALTER TABLE `user_forget_password_request`
 ADD PRIMARY KEY (`id`), ADD KEY `user_login_id` (`user_login_id`);

--
-- Indexes for table `user_information`
--
ALTER TABLE `user_information`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `personal_card_id` (`personal_card_id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `email` (`email`), ADD KEY `role_id` (`role_id`), ADD KEY `department_id` (`department_id`), ADD KEY `status` (`status`);

--
-- Indexes for table `using_log`
--
ALTER TABLE `using_log`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `enumeration`
--
ALTER TABLE `enumeration`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `equipment`
--
ALTER TABLE `equipment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1234;
--
-- AUTO_INCREMENT for table `equipment_cracked_log`
--
ALTER TABLE `equipment_cracked_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `equipment_type`
--
ALTER TABLE `equipment_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `equipment_type_list`
--
ALTER TABLE `equipment_type_list`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=638;
--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `link`
--
ALTER TABLE `link`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `period`
--
ALTER TABLE `period`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `period_group`
--
ALTER TABLE `period_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `permission_group`
--
ALTER TABLE `permission_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `present_type`
--
ALTER TABLE `present_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `request_booking`
--
ALTER TABLE `request_booking`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1025;
--
-- AUTO_INCREMENT for table `request_booking_activity_detail`
--
ALTER TABLE `request_booking_activity_detail`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1024;
--
-- AUTO_INCREMENT for table `request_booking_activity_present_type`
--
ALTER TABLE `request_booking_activity_present_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `request_booking_alert_log`
--
ALTER TABLE `request_booking_alert_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `request_booking_equipment_type`
--
ALTER TABLE `request_booking_equipment_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=290;
--
-- AUTO_INCREMENT for table `request_booking_type`
--
ALTER TABLE `request_booking_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `request_borrow`
--
ALTER TABLE `request_borrow`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `request_borrow_alert_log`
--
ALTER TABLE `request_borrow_alert_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `request_borrow_approve_link`
--
ALTER TABLE `request_borrow_approve_link`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `request_borrow_equipment_type`
--
ALTER TABLE `request_borrow_equipment_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `request_borrow_equipment_type_item`
--
ALTER TABLE `request_borrow_equipment_type_item`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `request_service`
--
ALTER TABLE `request_service`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `request_service_detail`
--
ALTER TABLE `request_service_detail`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `request_service_type`
--
ALTER TABLE `request_service_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `request_service_type_detail`
--
ALTER TABLE `request_service_type_detail`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `role_permission`
--
ALTER TABLE `role_permission`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=200;
--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5314;
--
-- AUTO_INCREMENT for table `room_equipment`
--
ALTER TABLE `room_equipment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `room_group`
--
ALTER TABLE `room_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `room_staff`
--
ALTER TABLE `room_staff`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_sky_notification`
--
ALTER TABLE `tb_sky_notification`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user_forget_password_request`
--
ALTER TABLE `user_forget_password_request`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_information`
--
ALTER TABLE `user_information`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัส',AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัส',AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT for table `using_log`
--
ALTER TABLE `using_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=746;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `enumeration`
--
ALTER TABLE `enumeration`
ADD CONSTRAINT `enumeration_ibfk_1` FOREIGN KEY (`enumeration_type_code`) REFERENCES `enumeration_type` (`code`);

--
-- Constraints for table `equipment`
--
ALTER TABLE `equipment`
ADD CONSTRAINT `equipment_ibfk_1` FOREIGN KEY (`equipment_type_id`) REFERENCES `equipment_type` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `equipment_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`);

--
-- Constraints for table `period`
--
ALTER TABLE `period`
ADD CONSTRAINT `period_ibfk_2` FOREIGN KEY (`period_group_id`) REFERENCES `period_group` (`id`),
ADD CONSTRAINT `period_ibfk_3` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

--
-- Constraints for table `period_group`
--
ALTER TABLE `period_group`
ADD CONSTRAINT `period_group_ibfk_1` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

--
-- Constraints for table `permission`
--
ALTER TABLE `permission`
ADD CONSTRAINT `permission_ibfk_1` FOREIGN KEY (`permission_group_id`) REFERENCES `permission_group` (`id`);

--
-- Constraints for table `request_booking`
--
ALTER TABLE `request_booking`
ADD CONSTRAINT `request_booking_ibfk_30` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`),
ADD CONSTRAINT `request_booking_ibfk_31` FOREIGN KEY (`request_booking_type_id`) REFERENCES `request_booking_type` (`id`),
ADD CONSTRAINT `request_booking_ibfk_32` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
ADD CONSTRAINT `request_booking_ibfk_33` FOREIGN KEY (`semester_id`) REFERENCES `semester` (`id`),
ADD CONSTRAINT `request_booking_ibfk_34` FOREIGN KEY (`period_start`) REFERENCES `period` (`id`),
ADD CONSTRAINT `request_booking_ibfk_35` FOREIGN KEY (`period_end`) REFERENCES `period` (`id`),
ADD CONSTRAINT `request_booking_ibfk_36` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

--
-- Constraints for table `request_booking_activity_present_type`
--
ALTER TABLE `request_booking_activity_present_type`
ADD CONSTRAINT `request_booking_activity_present_type_ibfk_3` FOREIGN KEY (`request_booking_activity_id`) REFERENCES `request_booking_activity_detail` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `request_booking_activity_present_type_ibfk_4` FOREIGN KEY (`present_type_id`) REFERENCES `present_type` (`id`);

--
-- Constraints for table `request_booking_alert_log`
--
ALTER TABLE `request_booking_alert_log`
ADD CONSTRAINT `request_booking_alert_log_ibfk_1` FOREIGN KEY (`request_booking_id`) REFERENCES `request_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `request_borrow`
--
ALTER TABLE `request_borrow`
ADD CONSTRAINT `request_borrow_ibfk_11` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`),
ADD CONSTRAINT `request_borrow_ibfk_12` FOREIGN KEY (`event_type_id`) REFERENCES `event_type` (`id`),
ADD CONSTRAINT `request_borrow_ibfk_13` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

--
-- Constraints for table `request_borrow_alert_log`
--
ALTER TABLE `request_borrow_alert_log`
ADD CONSTRAINT `request_borrow_alert_log_ibfk_1` FOREIGN KEY (`request_borrow_id`) REFERENCES `request_borrow` (`id`);

--
-- Constraints for table `request_borrow_equipment_type`
--
ALTER TABLE `request_borrow_equipment_type`
ADD CONSTRAINT `request_borrow_equipment_type_ibfk_3` FOREIGN KEY (`request_borrow_id`) REFERENCES `request_borrow` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `request_service`
--
ALTER TABLE `request_service`
ADD CONSTRAINT `request_service_ibfk_6` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`),
ADD CONSTRAINT `request_service_ibfk_7` FOREIGN KEY (`time_period`) REFERENCES `period` (`id`),
ADD CONSTRAINT `request_service_ibfk_8` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

--
-- Constraints for table `request_service_detail`
--
ALTER TABLE `request_service_detail`
ADD CONSTRAINT `request_service_detail_ibfk_3` FOREIGN KEY (`request_service_id`) REFERENCES `request_service` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `request_service_detail_ibfk_4` FOREIGN KEY (`request_service_type_detail_id`) REFERENCES `request_service_type_detail` (`id`);

--
-- Constraints for table `request_service_type_detail`
--
ALTER TABLE `request_service_type_detail`
ADD CONSTRAINT `request_service_type_detail_ibfk_1` FOREIGN KEY (`request_service_type_id`) REFERENCES `request_service_type` (`id`);

--
-- Constraints for table `role_permission`
--
ALTER TABLE `role_permission`
ADD CONSTRAINT `role_permission_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `role_permission_ibfk_2` FOREIGN KEY (`permission_code`) REFERENCES `permission` (`permission_code`) ON DELETE CASCADE;

--
-- Constraints for table `room`
--
ALTER TABLE `room`
ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`room_group_id`) REFERENCES `room_group` (`id`);

--
-- Constraints for table `room_equipment`
--
ALTER TABLE `room_equipment`
ADD CONSTRAINT `room_equipment_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
ADD CONSTRAINT `room_equipment_ibfk_2` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`);

--
-- Constraints for table `status`
--
ALTER TABLE `status`
ADD CONSTRAINT `status_ibfk_1` FOREIGN KEY (`status_group_id`) REFERENCES `status_group` (`id`);

--
-- Constraints for table `user_forget_password_request`
--
ALTER TABLE `user_forget_password_request`
ADD CONSTRAINT `user_forget_password_request_ibfk_1` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`);

--
-- Constraints for table `user_information`
--
ALTER TABLE `user_information`
ADD CONSTRAINT `user_information_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user_login` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_login`
--
ALTER TABLE `user_login`
ADD CONSTRAINT `user_login_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
ADD CONSTRAINT `user_login_ibfk_4` FOREIGN KEY (`status`) REFERENCES `status` (`status_code`),
ADD CONSTRAINT `user_login_ibfk_5` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
