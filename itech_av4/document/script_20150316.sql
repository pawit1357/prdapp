USE [DMT]
GO
/****** Object:  Table [dbo].[Weighing]    Script Date: 03/16/2015 10:26:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Weighing](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TimeStamp] [datetime] NULL,
	[RunningNumber] [int] NULL,
	[StationID] [tinyint] NULL,
	[LaneID] [tinyint] NULL,
	[NumberAxles] [tinyint] NULL,
	[Weight_Axle01] [int] NULL,
	[Weight_Axle02] [int] NULL,
	[Weight_Axle03] [int] NULL,
	[Weight_Axle04] [int] NULL,
	[Weight_Axle05] [int] NULL,
	[Weight_Axle06] [int] NULL,
	[Weight_Axle07] [int] NULL,
	[Weight_Axle08] [int] NULL,
	[Weight_Axle09] [int] NULL,
	[Weight_Axle10] [int] NULL,
	[Weight_GVW] [int] NULL,
	[Weight_MaxGVW] [int] NULL,
	[Weight_MaxAXW] [int] NULL,
	[IsOverWeight] [bit] NULL,
	[RecordStatus] [nchar](10) NULL,
	[Image01] [varchar](100) NULL,
	[Image02] [varchar](100) NULL,
 CONSTRAINT [PK_Weighing] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
