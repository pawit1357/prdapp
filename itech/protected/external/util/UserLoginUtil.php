<?php
class UserLoginUtil {
	private static $userPermissions = array();

	public static function hasPermission($permissionCodes) {
		$userLoginId = self::getUserLoginId();
		if($userLoginId == null) {
			return false;
		}

		$currentTime = time();
		// Cache timeout in 5 minites
		$cacheTimeout = (5 * 60 * 1000);


			
		if(self::$userPermissions['latestUpdateTime'] < ($currentTime - $cacheTimeout)){
			$userLogin = UserLogin::model()->with('role')->findByPk($userLoginId);
			$permissions = array();
			if(isset($userLogin->role)) {
				$criteria = new CDbCriteria;
				$criteria->condition="role_id = '".$userLogin->role->id."'";
				$rolePermissions = RolePermission::model()->with('permission')->findAll($criteria);
				foreach ($rolePermissions as $rolePermission){
					if(!in_array($rolePermission->permission_code, $permissions)){
						$permissions[count($permissions)] = $rolePermission->permission_code;
					}
				}
			}
			self::$userPermissions['permissions'] = $permissions;
			self::$userPermissions['latestUpdateTime'] = $currentTime;
		}

		$permissions = self::$userPermissions['permissions'];
		foreach ($permissionCodes as $permissionCode){
			if(in_array($permissionCode, $permissions)) {
				return true;
				break;
			}
		}


		return false;
	}
	public static function isLogin() {
		return isset($_SESSION['USER_LOGIN_ID']);
	}

	public static function logout() {
		unset($_SESSION['USER_LOGIN_ID']);
	}

	public static function authen($username, $password) {
		$criteria = new CDbCriteria;
		$criteria->condition="username = '".$username."' and password='".md5($password)."' and status='ACTIVE'";
		$userLogin = UserLogin::model()->findAll($criteria);
		if(isset($userLogin[0])) {
			$userLogin[0]->latest_login = date("Y-m-d H:i:s");
			$userLogin[0]->update();
			//echo"XXXX---------XX:"."username = '".$username."' and password='".md5($password)."' and status='ACTIVE'". $userLogin[0]->id;
			$_SESSION['USER_LOGIN_ID'] = $userLogin[0]->id;
			return true;
		} else {
			$_SESSION['FAIL_MESSAGE'] = 'Incorrect Username or Password!';
			return false;
		}
	}

	public static function getUserLoginId(){
		if(isset($_SESSION['USER_LOGIN_ID'])){
			return $_SESSION['USER_LOGIN_ID'];
		} else {
			return null;
		}
	}

	public static function getUserLogin(){
		if(self::isLogin()){
			$userLogin = UserLogin::model()->findByPk(self::getUserLoginId());
			return $userLogin;
		} else {
			return null;
		}
	}

	public static function getUserRole(){
		if(self::isLogin()){
			$userLogin = UserLogin::model()->findByPk(self::getUserLoginId());
			return $userLogin->role->id;
		} else {
			return null;
		}
	}
	

	public static function areUserRole($roles){
		if(self::isLogin()){
			$userLogin = UserLogin::model()->findByPk(self::getUserLoginId());
			return in_array($userLogin->role->name, $roles);
		} else {
			return null;
		}
	}
	
	public static function areUserRoleById($roles,$userId){
		if(self::isLogin()){
			$userLogin = UserLogin::model()->findByPk($userId);
			return in_array($userLogin->role->name, $roles);
		} else {
			return null;
		}
	}
}

interface UserRoles
{
	const ADMIN = "Admin";
	const STAFF = "Staff";
	const STUDENT = "Student";
	const LECTURER = "Lecturer";
	const STAFF_AV = "StaffAV";
}
?>