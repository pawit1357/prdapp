<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class UserController extends CController
{
	public $layout='management';
	private $_model;

	/**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex()
	{
		// Permission
		if(!UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_USER", "CREATE_USER", "UPDATE_USER", "DELETE_USER"))){
			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
		}
		$model = new UserLogin();
		// Make not find self account
		$model->exceptFindId = UserLoginUtil::getUserLoginId();

		// Set Search Text
		if(isset($_GET['search_text'])){
			$model->search_text = addslashes($_GET['search_text']);
			$model->username_search = $usernameSearch;
		}

		$this->render('main', array(
				'data' => $model,
		));
	}
	public function actionStaff()
	{
		// Permission
		if(!UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_USER", "CREATE_USER", "UPDATE_USER", "DELETE_USER"))){
			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
		}
		$model = new UserLogin();
		// Make not find self account
		$model->exceptFindId = UserLoginUtil::getUserLoginId();
		$model->onlyStaff = true;

		// Set Search Text
		if(isset($_GET['search_text'])){
			$usernameSearch = addslashes($_GET['search_text']);
			$personalTitleSearch = addslashes($_GET['search_text']);
			$firstNameSearch = addslashes($_GET['search_text']);
			$lastNameSearch = addslashes($_GET['search_text']);
			$emailSearch = addslashes($_GET['search_text']);

			$model->username_search = $usernameSearch;
			$model->user_information_personal_title_search = $personalTitleSearch;
			$model->user_information_first_name_search = $firstNameSearch;
			$model->user_information_last_name_search = $lastNameSearch;
			$model->user_information_email_search = $emailSearch;
		}

		$this->render('staff', array(
				'data' => $model,
		));
	}
	public function actionAssignRoom()
	{
		// Permission
		if(!UserLoginUtil::hasPermission(array("FULL_ADMIN"))){
			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
		}
		$model = $this->loadModel();

		if(isset($_POST['submit'])) {
			RoomStaff::model()->deleteAll("staff_id='".$model->id."'");
			if(isset($_POST['rooms'])) {
				$rooms = $_POST['rooms'];
				foreach($rooms as $room){
					$rs = new RoomStaff();
					$rs->room_id = $room;
					$rs->staff_id = $model->id;
					$rs->save();
				}
			}
			$this->redirect(Yii::app()->createUrl('User/Staff/'));
		}



		$this->render('assign_room', array(
				'data' => $model,
		));
	}

	public function actionCreate()
	{
		// Permission
		if(!UserLoginUtil::hasPermission(array("FULL_ADMIN", "CREATE_USER"))){
			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
		}

		if(isset($_POST['add_user'])){
			$userLogin = new UserLogin();
			$userLogin->attributes = $_POST['UserLogin'];
			$userLogin->password = md5($userLogin->password);

			if($userLogin->save()){
				$userInfo = new UserInformation();
				$userInfo->id = $userLogin->getPrimaryKey();
				$userInfo->attributes = $_POST['UserInformation'];
				if($userInfo->save()){
					$this->redirect(Yii::app()->createUrl('user/view/id/'.$userInfo->id));
				} else {
					$this->render('create');
				}
			}
		} else {
			$this->render('create');
		}

	}
	public function actionDelete()
	{
		// Permission
		if(!UserLoginUtil::hasPermission(array("FULL_ADMIN", "DELETE_USER"))){
			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
		}

		$model = $this->loadModel();
		$model->delete();
		$this->redirect(Yii::app()->createUrl('User/'));
	}
	public function actionView()
	{
		// Permission
		if(!UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_USER"))){
			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
		}

		$model = $this->loadModel();
		$this->render('view', array(
				'model' => $model,
		));
	}
	public function actionUpdate()
	{
		// Permission
		if(!UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_USER"))){
			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
		}

		$userLogin = $this->loadModel();
		if ($userLogin === null) {
			Yii::log("User not found!", CLogger::LEVEL_INFO, __METHOD__);
			throw new CHttpException(404, Yii::t("MusicModule.general", 'The requested page does not exist.'));
		}

		if(isset($_POST['UserLogin'])) {
			if($_POST['UserLogin']['password'] == '') {
				$_POST['UserLogin']['password'] = md5($userLogin->password);
			} else {
				$_POST['UserLogin']['password'] = md5($_POST['UserLogin']['password']);
			}
			$userLogin->attributes = $_POST['UserLogin'];
			var_dump($userLogin->attributes);
			if($userLogin->update()) {
				$userInfo = UserInformation::model()->findByPk($userLogin->id);
				if(isset($userInfo)){
					$userInfo->attributes = $_POST['UserInformation'];
					if($userInfo->update()){
						$this->redirect(Yii::app()->createUrl('user/view/id/'.$userInfo->id));
					}
				}
			}
		} else {
			$this->render('update', array(
					'model' => $userLogin,
			));
		}


	}

	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=UserLogin::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}


}