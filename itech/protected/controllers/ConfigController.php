<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class ConfigController extends CController
{
	public $layout='management';
	private $_model;

	/**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex()
	{
		// Permission
		if(!UserLoginUtil::hasPermission(array("FULL_ADMIN"))){
			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
		}
		if(isset($_POST['returnlate'])) {
			ConfigUtil::saveConfig("BORROW_RETURN_LATE_PRICE_PER_DAY", $_POST['returnlate']);
			$this->redirect(Yii::app()->createUrl('Config/'));
		}
		$this->render('main');
	}
}