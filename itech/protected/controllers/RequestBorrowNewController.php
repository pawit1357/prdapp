<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class RequestBorrowNewController extends CController {
	public $layout = 'management';
	private $_model;
	
	/**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"VIEW_REQUEST_BORROW",
				"VIEW_ALL_REQUEST_BORROW",
				"CREATE_REQUEST_BORROW",
				"UPDATE_REQUEST_BORROW",
				"DELETE_REQUEST_BORROW" 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		$model = new RequestBorrow ();
		if (UserLoginUtil::areUserRole ( array (
				UserRoles::ADMIN,
				UserRoles::STAFF_AV 
		) )) {
			$model->view_all_request = true;
		} else {
			$model->view_all_request = false;
		}
		
		$model->status_not_finish = true;
		// Render
		
		$this->render ( 'main', array (
				'data' => $model 
		) );
	}
	public function actionBorrow() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"CREATE_REQUEST_BORROW" 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		
		if (isset ( $_POST ['RequestBorrow'] )) {
			$transaction = Yii::app ()->db->beginTransaction ();
			$eqs = $_POST ['eqs'];
			// Add Request
			$requestBorrow = new RequestBorrow ();
			
			$requestBorrow->approve_by = UserLoginUtil::getUserLogin ()->parent;
			
			list ( $day, $month, $year ) = explode ( '-', $_POST ['RequestBorrow'] ['from_date'] );
			$_POST ['RequestBorrow'] ['from_date'] = $year . '-' . $month . '-' . $day;
			list ( $day, $month, $year ) = explode ( '-', $_POST ['RequestBorrow'] ['thru_date'] );
			$_POST ['RequestBorrow'] ['thru_date'] = $year . '-' . $month . '-' . $day;
			$requestBorrow->attributes = $_POST ['RequestBorrow'];
			if (isset ( $_POST ['RequestBorrow'] ['teacher_id'] ) && $_POST ['RequestBorrow'] ['teacher_id'] != '') {
				$requestBorrow->approve_by = $_POST ['RequestBorrow'] ['teacher_id'];
			}
			$requestBorrow->user_login_id = UserLoginUtil::getUserLoginId ();
			
			if (UserLoginUtil::areUserRole ( array (
					UserRoles::ADMIN 
			) )) {
				$requestBorrow->status_code = "R_B_NEW_PREPARE";
			} else if (UserLoginUtil::areUserRole ( array (
					UserRoles::STAFF,
					UserRoles::LECTURER,
					UserRoles::STAFF_AV 
			) )) {
				$requestBorrow->status_code = "R_B_NEW_WAIT_APPROVE_3";
			} else {
				$requestBorrow->status_code = "R_B_NEW_WAIT_APPROVE_1";
			}
			
			// Check for Lecturer borrow in 1 day
			$sendMailToApprover = true;
			if (UserLoginUtil::areUserRole ( array (
					UserRoles::LECTURER 
			) ) && $_POST ['RequestBorrow'] ['from_date'] == $_POST ['RequestBorrow'] ['thru_date']) {
				
				if ($requestBorrow->location == 'WHITHIN_MUIC') {
					$requestBorrow->status_code = "R_B_NEW_PREPARE";
					$sendMailToApprover = false;
				} else {
					$requestBorrow->status_code = "R_B_NEW_WAIT_APPROVE_3";
				}
			}
			
			$requestBorrow->create_date = date ( "Y-m-d H:i:s" );
			$addSuccess = true;
			$validate = true;
			
			// Validate
			// now and request date
			
			// request must less than 5 days
			
			if ($validate) {
				if ($requestBorrow->save ()) {
					$model = $requestBorrow;
					if (isset ( $eqs )) {
						foreach ( $eqs as $equipment => $qty ) {
							$equipmentTypeId = addslashes ( $equipment );
							$requestBorrowEquipmentType = new RequestBorrowEquipmentType ();
							$requestBorrowEquipmentType->request_borrow_id = $requestBorrow->getPrimaryKey ();
							$requestBorrowEquipmentType->equipment_type_id = $equipmentTypeId;
							$requestBorrowEquipmentType->quantity = addslashes ( $qty );
							if (! $requestBorrowEquipmentType->save ()) {
								$addSuccess = false;
								break;
							}
						}
					}
					
					$content = MailUtil::getBorrowDetailMailContent ( $model );
					
					if (isset ( $model->user_login->email )) {
						MailUtil::sendMail ( $model->user_login->email, 'Support AV-Online, Request Borrow Result', $content );
					}
					
					// Send Mail to approver
					if ($sendMailToApprover) {
						$key = md5 ( rand ( 0, 2000 ) ) . md5 ( rand ( 0, 2000 ) ) . md5 ( rand ( 0, 2000 ) ) . md5 ( rand ( 0, 2000 ) ) . md5 ( rand ( 0, 2000 ) ) . md5 ( rand ( 0, 2000 ) );
						$requestBorrowId = $requestBorrow->getPrimaryKey ();
						$createDate = date ( 'Y-m-d H:i:s' );
						$status = "ACTIVE";
						$approveType = "APPROVE_1";
						
						$requestBorrowApproveLink = new RequestBorrowApproveLink ();
						$requestBorrowApproveLink->request_key = $key;
						$requestBorrowApproveLink->request_borrow_id = $requestBorrowId;
						$requestBorrowApproveLink->create_date = $createDate;
						$requestBorrowApproveLink->status = $status;
						$requestBorrowApproveLink->approve_type = $approveType;
						
						if ($requestBorrowApproveLink->save ()) {
							if (isset ( $requestBorrow->approve_by ) && $requestBorrow->approve_by != '') {
								$approveId = $requestBorrow->approve_by;
							} else {
								$approveId = UserLoginUtil::getUserLogin ()->parent;
							}
							
							$approveUser = UserLogin::model ()->findByPk ( $approveId );
							$content = MailUtil::getApproveMailContent ( $key, $requestBorrow );
							if (isset ( $approveUser->email )) {
								MailUtil::sendMail ( $approveUser->email, 'Support AV-Online, Approve Request Booking', $content );
							}
						}
					}
				} else {
					$addSuccess = false;
				}
			}
			
			if ($addSuccess) {
				
				$transaction->commit ();
				$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/View/id/' . $requestBorrow->getPrimaryKey () ) );
				$this->render ( 'borrow', array (
						'data' => $model 
				) );
			} else {
				$transaction->rollback ();
				$model = new RequestBorrow ();
				$this->render ( 'borrow', array (
						'data' => $model 
				) );
			}
		} else {
			// Render
			$this->render ( 'borrow' );
		}
	}
	public function actionView() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"VIEW_REQUEST_BORROW",
				"VIEW_ALL_REQUEST_BORROW" 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		// Render
		$model = $this->loadModel ();
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"VIEW_ALL_REQUEST_BORROW" 
		) ) && $model->user_login_id != UserLoginUtil::getUserLoginId ()) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		$this->render ( 'view', array (
				'data' => $model 
		) );
	}
	public function actionPrepare() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		
		if (! UserLoginUtil::areUserRole ( array (
				UserRoles::ADMIN,
				UserRoles::STAFF_AV 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		
		if (isset ( $_POST ['eq_item'] )) {
			$id = addslashes ( $_GET ['id'] );
			$items = $_POST ['eq_item'];
			$transaction = Yii::app ()->db->beginTransaction ();
			$addSuccess = true;
			foreach ( $items as $eId => $eTypeId ) {
				$criteria = new CDbCriteria ();
				$criteria->condition = "request_borrow_id = '" . $id . "' and equipment_type_id='" . $eTypeId . "'";
				$requestBorrowEquipmentTypes = RequestBorrowEquipmentType::model ()->findAll ( $criteria );
				if (isset ( $requestBorrowEquipmentTypes ) && count ( $requestBorrowEquipmentTypes ) > 0) {
					$requestBorrowEquipmentType = $requestBorrowEquipmentTypes [0];
				} else {
					$requestBorrowEquipmentType = new RequestBorrowEquipmentType ();
					$requestBorrowEquipmentType->request_borrow_id = $id;
					$requestBorrowEquipmentType->equipment_type_id = $eTypeId;
					if ($requestBorrowEquipmentType->save ()) {
						$requestBorrowEquipmentType->id = $requestBorrowEquipmentType->getPrimaryKey ();
					} else {
						$addSuccess = false;
					}
				}
				$criteria = new CDbCriteria ();
				$criteria->condition = "equipment_id = '" . $eId . "' and request_borrow_equipment_type_id='" . $requestBorrowEquipmentType->id . "'";
				$requestItems = RequestBorrowEquipmentTypeItem::model ()->findAll ( $criteria );
				if (count ( $requestItems ) <= 0) {
					$model = new RequestBorrowEquipmentTypeItem ();
					$model->request_borrow_equipment_type_id = $requestBorrowEquipmentType->id;
					$model->equipment_id = $eId;
					if (! $model->save ()) {
						$addSuccess = false;
					}
				}
			}
			
			$countFinish = true;
			$requestEquipmentTypes = RequestBorrowEquipmentType::model ()->findAll ( array (
					'condition' => "request_borrow_id='" . $id . "'" 
			) );
			foreach ( $requestEquipmentTypes as $requestEquipmentType ) {
				$requestQty = $requestEquipmentType->quantity;
				$requestEquipmentTypeItems = RequestBorrowEquipmentTypeItem::model ()->findAll ( array (
						'condition' => "request_borrow_equipment_type_id='" . $requestEquipmentType->id . "'" 
				) );
				if (count ( $requestEquipmentTypeItems ) < $requestQty) {
					$countFinish = false;
					break;
				}
			}
			if (! $countFinish) {
				$status = 'R_B_NEW_READY_MISSING';
			} else {
				$status = 'R_B_NEW_READY';
			}
			$requestBorrow = RequestBorrow::model ()->findbyPk ( $id );
			if (isset ( $requestBorrow )) {
				$requestBorrow->status_code = $status;
				$requestBorrow->save ();
			}
			
			if ($addSuccess) {
				$transaction->commit ();
				$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/View/id/' . $id ) );
			} else {
				$transaction->rollback ();
				$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/Prepare/id/' . $id ) );
			}
		} else {
			$data = RequestBorrow::model ()->findByPk ( addslashes ( $_GET ['id'] ) );
			$this->render ( 'prepare', array (
					'data' => $data 
			) );
		}
	}
	public function actionReturn() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		
		if (! UserLoginUtil::areUserRole ( array (
				UserRoles::ADMIN,
				UserRoles::STAFF_AV 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		
		if (isset ( $_POST ['save_request'] ) && $_POST ['save_request'] == 'Save') {
			$id = addslashes ( $_GET ['id'] );
			$items = $_POST ['eq_items'];
			$transaction = Yii::app ()->db->beginTransaction ();
			$addSuccess = true;
			
			// clear all return
			$criteria = new CDbCriteria ();
			$criteria->condition = "request_borrow_id = '" . $id . "'";
			$requestBorrowEquipmentTypes = RequestBorrowEquipmentType::model ()->findAll ( $criteria );
			if (isset ( $requestBorrowEquipmentTypes ) && count ( $requestBorrowEquipmentTypes > 0 )) {
				foreach ( $requestBorrowEquipmentTypes as $requestBorrowEquipmentType ) {
					$criteria = new CDbCriteria ();
					$criteria->condition = "request_borrow_equipment_type_id = '" . $requestBorrowEquipmentType->id . "'";
					$requestBorrowEquipmentTypeItems = RequestBorrowEquipmentTypeItem::model ()->findAll ( $criteria );
					foreach ( $requestBorrowEquipmentTypeItems as $requestBorrowEquipmentTypeItem ) {
						$requestBorrowEquipmentTypeItem->return_date = '';
						$requestBorrowEquipmentTypeItem->return_price = '0';
						$requestBorrowEquipmentTypeItem->broken_price = '0';
						$requestBorrowEquipmentTypeItem->save ();
					}
				}
			}
			
			// set Return
			if (isset ( $items )) {
				foreach ( $items as $eTypeId => $eId ) {
					$criteria = new CDbCriteria ();
					$criteria->condition = "t.equipment_id = '" . $eId . "' and request_borrow_equipment_type.request_borrow_id = '" . $id . "'";
					$requestItems = RequestBorrowEquipmentTypeItem::model ()->with ( array (
							'request_borrow_equipment_type' 
					) )->findAll ( $criteria );
					if (isset ( $requestItems ) && count ( $requestItems ) > 0) {
						foreach ( $requestItems as $requestItem ) {
							$model = $this->loadModel ();
							$borrow = strtotime ( $model->thru_date );
							$current = strtotime ( date ( 'Y-m-d' ) );
							$day_diff = floor ( ($current - $borrow) / 60 / 60 / 24 );
							if ($day_diff > 0) {
								$requestItem->return_price = $day_diff * ConfigUtil::getReturnLatePricePerDay ();
							}
							
							if (isset ( $_POST ['brokenPrice'] [$requestItem->equipment_id] )) {
								$requestItem->broken_price = $_POST ['brokenPrice'] [$requestItem->equipment_id];
							}
							$requestItem->return_date = time ();
							if (! $requestItem->save ()) {
								$addSuccess = false;
							}
						}
					}
				}
			}
			
			$criteria = new CDbCriteria ();
			$criteria->condition = "request_borrow_equipment_type.request_borrow_id ='" . $id . "'";
			$requestBorrowEquipmentTypeItems = RequestBorrowEquipmentTypeItem::model ()->with ( array (
					'request_borrow_equipment_type' 
			) )->findAll ( $criteria );
			$isReturnAll = true;
			if (isset ( $requestBorrowEquipmentTypeItems )) {
				foreach ( $requestBorrowEquipmentTypeItems as $requestBorrowEquipmentTypeItem ) {
					if ($requestBorrowEquipmentTypeItem->return_date == '') {
						$isReturnAll = false;
						break;
					}
				}
			}
			$status = '';
			if ($isReturnAll) {
				$status = 'R_B_NEW_RETURNED';
			} else {
				$status = 'R_B_NEW_RETURNED_MISSING';
			}
			$requestBorrow = RequestBorrow::model ()->findbyPk ( $id );
			if (isset ( $requestBorrow )) {
				$requestBorrow->status_code = $status;
				$requestBorrow->save ();
			}
			
			if ($addSuccess) {
				$transaction->commit ();
				$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/View/id/' . $id ) );
			} else {
				$transaction->rollback ();
				$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/Return/id/' . $id ) );
			}
		} else {
			$data = RequestBorrow::model ()->findByPk ( addslashes ( $_GET ['id'] ) );
			$availableReturnStatus = array (
					'R_B_NEW_READY',
					'R_B_NEW_READY_MISSING',
					'R_B_NEW_RETURNED_MISSING' 
			);
			if (! in_array ( $data->status_code, $availableReturnStatus )) {
				throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
						'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
				) ) );
			}
			
			$this->render ( 'return', array (
					'data' => $data 
			) );
		}
	}
	public function actionListApprove() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"UPDATE_REQUEST_BORROW" 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		// Render
		$model = new RequestBorrow ();
		if (UserLoginUtil::areUserRole ( array (
				UserRoles::ADMIN 
		) )) {
			$model->view_all_request = true;
		} else {
			$model->approve_filter = true;
		}
		$model->status_for_approve = true;
		$model->clearDateFilter ();
		$this->render ( 'approve', array (
				'data' => $model 
		) );
	}
	public function actionApproveRequest() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"UPDATE_REQUEST_BORROW" 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		
		$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/ListApprove' ) );
	}
	public function actionDisapproveRequest() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"UPDATE_REQUEST_BORROW" 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		
		$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/ListApprove' ) );
	}
	public function actionApprove() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"VIEW_REQUEST_BORROW",
				"VIEW_ALL_REQUEST_BORROW" 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		
		// Render
		$model = $this->loadModel ();
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"VIEW_ALL_REQUEST_BORROW" 
		) ) && $model->user_login_id != UserLoginUtil::getUserLoginId ()) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		
		if (! (($model->approve_by == UserLoginUtil::getUserLoginId ()) && ! UserLoginUtil::areUserRole ( array (
				'Admin' 
		) ))) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		/*
		 * coding by x
		 * if (! (($model->user_login->parent == UserLoginUtil::getUserLoginId () && $model->approve_by == '') || ($model->approver->parent == UserLoginUtil::getUserLoginId () && $model->approve_by != '')) && ! UserLoginUtil::areUserRole ( array (
		 * 'Admin'
		 * ) )) {
		 * throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
		 * '{action}' => $actionID == '' ? $this->defaultAction : $actionID
		 * ) ) );
		 * }
		 */
		if ($model->status_code != 'R_B_NEW_WAIT_APPROVE_1' && $model->status_code != 'R_B_NEW_WAIT_APPROVE_2' && $model->status_code != 'R_B_NEW_WAIT_APPROVE_3') {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		
		if (isset ( $_POST ['submit'] )) {
			$submit = $_POST ['submit'];
			$id = $_GET ['id'];
			$status = '';
			$nextApproveId = "";
			$userLogin = UserLoginUtil::getUserLogin ();
			$hasNextApprove = true;
			if ($model->status_code == 'R_B_NEW_WAIT_APPROVE_1') {
				if ($submit == 'Approve') {
					$status = 'R_B_NEW_WAIT_APPROVE_2';
					$nextApproveId = $userLogin->parent;
				} else {
					$status = 'R_B_NEW_DISAPPROVE_1';
					$hasNextApprove = false;
				}
			} else if ($model->status_code == 'R_B_NEW_WAIT_APPROVE_2') {
				if ($submit == 'Approve') {
					$status = 'R_B_NEW_WAIT_APPROVE_3';
					$nextApproveId = $userLogin->parent;
				} else {
					$status = 'R_B_NEW_DISAPPROVE_2';
					$hasNextApprove = false;
				}
			} else if ($model->status_code == 'R_B_NEW_WAIT_APPROVE_3') {
				if ($submit == 'Approve') {
					$status = 'R_B_NEW_PREPARE';
					$hasNextApprove = false;
				} else {
					$status = 'R_B_NEW_DISAPPROVE_3';
					$hasNextApprove = false;
				}
			}
			
			$model->status_code = $status;
			$model->approve_by = $nextApproveId; // UserLoginUtil::getUserLoginId ();
			if ($model->save ()) {
				if (CommonUtil::isEmpty ( $nextApproveId )) {
					$nextApproveId = '1';
				}
				RequestUtil::deleteAllRequestLinkKey ( $model->id );
				$stausObj = Status::model ()->findByPk ( $status );
				
				if ($submit == 'Approve') {
					$content = MailUtil::getBorrowStatusChangeMailContent ( $model );
					MailUtil::sendMail ( $model->user_login->email, 'Support AV-Online, Request Booking Status Approve', $content );
					if ($hasNextApprove) {
						RequestUtil::sendApproveLink ( $model->id, $nextApproveId );
					}
				} else {
					$content = MailUtil::getBorrowStatusChangeMailContent ( $model );
					MailUtil::sendMail ( $model->user_login->email, 'Support AV-Online, Request Booking Status Disapprove', $content );
				}
				
				$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/View/id/' . $id ) );
			} else {
				$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/Approve/id/' . $id ) );
			}
		} else {
			$this->render ( 'approve_item', array (
					'data' => $model 
			) );
		}
	}
	public function actionPrint() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"VIEW_REQUEST_BORROW",
				"VIEW_ALL_REQUEST_BORROW" 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		// Render
		$model = $this->loadModel ();
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"VIEW_ALL_REQUEST_BORROW" 
		) ) && $model->user_login_id != UserLoginUtil::getUserLoginId ()) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		$this->layout = 'ajax2';
		$this->render ( 'print', array (
				'data' => $model 
		) );
	}
	public function actionPrintSmall() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"VIEW_REQUEST_BORROW",
				"VIEW_ALL_REQUEST_BORROW"
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID
			) ) );
		}
		// Render
		$model = $this->loadModel ();
		if (! UserLoginUtil::hasPermission ( array (
				"FULL_ADMIN",
				"VIEW_ALL_REQUEST_BORROW"
		) ) && $model->user_login_id != UserLoginUtil::getUserLoginId ()) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID
			) ) );
		}
		$this->layout = 'ajax2';
		$this->render ( 'print_small', array (
				'data' => $model
		) );
	}
	public function actionDelete() {
		// Authen Login
		if (! UserLoginUtil::isLogin ()) {
			$this->redirect ( Yii::app ()->createUrl ( 'management/login' ) );
		}
		
		$model = $this->loadModel ();
		if (! UserLoginUtil::areUserRole ( array (
				UserRoles::ADMIN,
				UserRoles::STAFF_AV 
		) ) && $model->user_login_id != UserLoginUtil::getUserLoginId ()) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		if (in_array ( $model->status_code, array (
				'R_B_NEW_CANCELLED',
				'R_B_NEW_RETURNED_MISSING',
				'R_B_NEW_RETURNED',
				'R_B_NEW_READY',
				'R_B_NEW_READY_MISSING' 
		) )) {
			throw new CHttpException ( 404, Yii::t ( 'yii', 'The system is unable to find the requested', array (
					'{action}' => $actionID == '' ? $this->defaultAction : $actionID 
			) ) );
		}
		$model->status_code = 'R_B_NEW_CANCELLED';
		$model->save ();
		$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/View/id/' . $model->id ) );
	}
	public function actionApproveExternal() {
		// $this->layout='main';
		$key = $_GET ['key'];
		$requestBorrowApproveLinks = RequestBorrowApproveLink::model ()->findAll ( array (
				'condition' => "request_key='" . $key . "'" 
		) );
		if (isset ( $requestBorrowApproveLinks ) && count ( $requestBorrowApproveLinks ) > 0) {
			$requestBorrowApproveLink = $requestBorrowApproveLinks [0];
			$requestBorrow = RequestBorrow::model ()->findByPk ( $requestBorrowApproveLink->request_borrow_id );
			if (isset ( $requestBorrow )) {
				$hasNextApprove = true;
				$nextApproveId = '';
				if ($requestBorrow->status_code == 'R_B_NEW_WAIT_APPROVE_1') {
					$status = 'R_B_NEW_WAIT_APPROVE_2';
				} else if ($requestBorrow->status_code == 'R_B_NEW_WAIT_APPROVE_2') {
					$status = 'R_B_NEW_WAIT_APPROVE_3';
				} else if ($requestBorrow->status_code == 'R_B_NEW_WAIT_APPROVE_3') {
					$status = 'R_B_NEW_PREPARE';
					$hasNextApprove = false;
				}
				if ($requestBorrow->approve_by == '') {
					if (CommonUtil::isEmpty ( $requestBorrow->user_login->parent )) {
						$requestBorrow->approve_by = '1';
					} else {
						$requestBorrow->approve_by = $requestBorrow->user_login->parent;
					}
				}
				$userApp = UserLogin::model ()->findByPk ( $requestBorrow->approve_by );
				if (CommonUtil::isEmpty ( $userApp->parent )) {
					$nextApproveId = '1';
				} else {
					$nextApproveId = $userApp->parent;
				}
				
				$requestBorrow->status_code = $status;
				$requestBorrow->update ();
				RequestUtil::deleteAllRequestLinkKey ( $requestBorrow->id );
				$stausObj = Status::model ()->findByPk ( $status );
				
				$content = MailUtil::getBorrowStatusChangeMailContent ( $requestBorrow );
				MailUtil::sendMail ( $requestBorrow->user_login->email, 'Support AV-Online, Request Booking Status Approve', $content );
				$_SESSION ['r-message'] = 'The request has been approved.';
				if ($hasNextApprove) {
					RequestUtil::sendApproveLink ( $requestBorrow->id, $nextApproveId );
				}
			} else {
				$_SESSION ['r-message'] = 'The request not found.';
			}
		} else {
			$_SESSION ['r-message'] = 'Key not found.';
		}
		$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/ApproveResult' ) );
	}
	public function actionDisapproveExternal() {
		$key = $_GET ['key'];
		$requestBorrowApproveLinks = RequestBorrowApproveLink::model ()->findAll ( array (
				'condition' => "request_key='" . $key . "'" 
		) );
		if (isset ( $requestBorrowApproveLinks ) && count ( $requestBorrowApproveLinks ) > 0) {
			$requestBorrowApproveLink = $requestBorrowApproveLinks [0];
			$requestBorrow = RequestBorrow::model ()->findByPk ( $requestBorrowApproveLink->request_borrow_id );
			if (isset ( $requestBorrow )) {
				if ($requestBorrow->status_code == 'R_B_NEW_WAIT_APPROVE_1') {
					$status = 'R_B_NEW_DISAPPROVE_1';
				} else if ($requestBorrow->status_code == 'R_B_NEW_WAIT_APPROVE_2') {
					$status = 'R_B_NEW_DISAPPROVE_2';
				} else if ($requestBorrow->status_code == 'R_B_NEW_WAIT_APPROVE_3') {
					$status = 'R_B_NEW_DISAPPROVE_3';
				}
				
				$requestBorrow->status_code = $status;
				$requestBorrow->update ();
				RequestUtil::deleteAllRequestLinkKey ( $requestBorrow->id );
				$stausObj = Status::model ()->findByPk ( $status );
				
				$content = MailUtil::getBorrowStatusChangeMailContent ( $requestBorrow );
				MailUtil::sendMail ( $requestBorrow->user_login->email, 'Support AV-Online, Request Booking Status Disapprove', $content );
				$_SESSION ['r-message'] = 'The request has been disapproved.';
			} else {
				$_SESSION ['r-message'] = 'The request not found.';
			}
		} else {
			$_SESSION ['r-message'] = 'Key not found.';
		}
		$this->redirect ( Yii::app ()->createUrl ( 'RequestBorrowNew/ApproveResult' ) );
	}
	public function actionApproveResult() {
		$this->render ( 'approve_result' );
	}
	public function loadModel() {
		if ($this->_model === null) {
			if (isset ( $_GET ['id'] ))
				$this->_model = RequestBorrow::model ()->findbyPk ( $_GET ['id'] );
			if ($this->_model === null)
				throw new CHttpException ( 404, 'The requested page does not exist.' );
		}
		return $this->_model;
	}
}