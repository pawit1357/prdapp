<?php
class RequestBooking extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return CActiveRecord the static model class
	 */
	public $view_all_request = false;
	public $view_all_admin = false;
	public $year_filter;
	public $month_filter;
	public $day_filter;
	public $status_filter;
	public $room_filter;
	public $user_filter;
	public $exceptRoomId;
	public $hiddenPass = false;
	public function __construct($scenario = 'insert') {
		parent::__construct ( $scenario );
		$this->year_filter = date ( "Y" ) * 1;
		$this->month_filter = date ( "m" ) * 1;
		$this->day_filter = (date ( "d" ) * 1);
	}
	public static function model($className = __CLASS__) {
		return parent::model ( $className );
	}
	public function clearDateFilter() {
		$this->year_filter = null;
		$this->month_filter = null;
		$this->day_filter = null;
	}
	
	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'request_booking';
	}
	
	/**
	 *
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array (
				'user_login' => array (
						self::BELONGS_TO,
						'UserLogin',
						'user_login_id' 
				),
				'request_booking_type' => array (
						self::BELONGS_TO,
						'RequestBookingType',
						'request_booking_type_id' 
				),
				'period_s' => array (
						self::BELONGS_TO,
						'Period',
						'period_start' 
				),
				'period_e' => array (
						self::BELONGS_TO,
						'Period',
						'period_end' 
				),
				'room' => array (
						self::BELONGS_TO,
						'Room',
						'room_id' 
				),
				'status' => array (
						self::BELONGS_TO,
						'Status',
						'status_code' 
				),
				'day_in_week' => array (
						self::BELONGS_TO,
						'Enumeration',
						'request_day_in_week' 
				),
				'semester' => array (
						self::BELONGS_TO,
						'Semester',
						'semester_id' 
				) 
		);
	}
	
	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array (
				array (
						'room_id, request_booking_type_id, request_date, period_start, period_end, description, course_name, semester_id, request_day_in_week,request_sky_id,request_sky_noti_id',
						'safe' 
				) 
		);
		// array('room_id, request_booking_type_id, period_start, period_end', 'required'),
		
	}
	
	/**
	 *
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array (
				'request_date' => 'Request Date',
				'description' => 'Description' 
		);
	}
	
	/**
	 *
	 * @param
	 *        	Post the post that this comment belongs to. If null, the method
	 *        	will query for the post.
	 * @return string the permalink URL for this comment
	 */
	public function getUrl($post = null) {
		if ($post === null)
			$post = $this->post;
		return $post->url . '#c' . $this->id;
	}
	
	/**
	 * This is invoked before the record is saved.
	 *
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave() {
		/*
		 * if(parent::beforeSave())
		 * {
		 * if($this->isNewRecord)
		 * $this->create_time=time();
		 * return true;
		 * }
		 * else
		 * return false;
		 */
		return true;
	}
	public function searchSkypData() {
		$criteria = new CDbCriteria ();
		
		if (isset ( $this->user_filter ) && $this->user_filter != '') {
			$criteria->addCondition ( "t.user_login_id='" . $this->user_filter . "'" );
		}
		
		return new CActiveDataProvider ( get_class ( $this ), array (
				'criteria' => $criteria,
				'sort' => array (
						'defaultOrder' => 't.request_sky_id' 
				),
				'pagination' => array (
						'pageSize' => 20 
				) 
		) );
	}
	public function search() {
		$criteria = new CDbCriteria ();
		$criteria->with = array (
				'semester',
				'period_e',
				'period_s',
				'user_login' 
		);
		
		if (isset ( $this->year_filter ) && $this->year_filter != '') {
			$criteria->addCondition ( "((YEAR(t.request_date) = " . $this->year_filter . " AND request_booking_type_id != 2) OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR (" . $this->year_filter . " between YEAR(semester.start_date) AND YEAR(semester.end_date)  AND request_booking_type_id = 2))" );
			
			if (isset ( $this->month_filter ) && $this->month_filter != '') {
				$criteria->addCondition ( "((MONTH(t.request_date) = '" . $this->month_filter . "' AND request_booking_type_id != 2 AND YEAR(t.request_date) = '" . $this->year_filter . "') OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR (" . $this->year_filter . " between YEAR(semester.start_date) AND YEAR(semester.end_date)  AND request_booking_type_id = 2))" );
				
				if (isset ( $this->day_filter ) && $this->day_filter != '' && ! $this->hiddenPass) {
					$criteria->addCondition ( "((DAY(t.request_date) = '" . $this->day_filter . "' AND request_booking_type_id != 2 AND YEAR(t.request_date) = '" . $this->year_filter . "') OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR (" . $this->year_filter . " between YEAR(semester.start_date) AND YEAR(semester.end_date)  AND request_booking_type_id = 2))" );
				}
			}
		}
		
		if ($this->hiddenPass) {
			$criteria->addCondition ( "(((t.request_date > '" . Date ( "Y-m-d" ) . "')  AND t.request_booking_type_id != 2) OR ((t.request_date = '" . Date ( "Y-m-d" ) . "' AND period_e.end_hour >= '" . date ( "H" ) . "')  AND t.request_booking_type_id != 2)   OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR ('" . date ( "Y-m-d" ) . "' BETWEEN semester.start_date AND semester.end_date AND t.request_booking_type_id = 2))" );
		}
		if (isset ( $this->status_filter ) && $this->status_filter != '') {
			
			$criteria->addCondition ( "t.status_code='" . $this->status_filter . "'" );
		}
		if (isset ( $this->room_filter ) && $this->room_filter != '') {
			$criteria->addCondition ( "t.room_id='" . $this->room_filter . "'" );
		}
		
		if (isset ( $this->user_filter ) && $this->user_filter != '') {
			$criteria->addCondition ( "t.user_login_id='" . $this->user_filter . "'" );
		}
		
		return new CActiveDataProvider ( get_class ( $this ), array (
				'criteria' => $criteria,
				'sort' => array (
						'defaultOrder' => 't.request_date, t.room_id' 
				),
				'pagination' => array (
						'pageSize' => 10 
				) 
		) );
	}
	
	// All Booking
	public function searchAllBooking() {
		$criteria = new CDbCriteria ();
		$criteria->with = array (
				'semester',
				'period_e',
				'period_s',
				'user_login' 
		);
		
		if (isset ( $this->year_filter ) && $this->year_filter != '') {
			$criteria->addCondition ( "((YEAR(t.request_date) = " . $this->year_filter . " AND request_booking_type_id != 2) OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR (" . $this->year_filter . " between YEAR(semester.start_date) AND YEAR(semester.end_date)  AND request_booking_type_id = 2))" );
			
			if (isset ( $this->month_filter ) && $this->month_filter != '') {
				$criteria->addCondition ( "((MONTH(t.request_date) = '" . $this->month_filter . "' AND request_booking_type_id != 2 AND YEAR(t.request_date) = '" . $this->year_filter . "') OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR (" . $this->month_filter . " between MONTH(semester.start_date) AND MONTH(semester.end_date)  AND request_booking_type_id = 2))" );
				
				if (isset ( $this->day_filter ) && $this->day_filter != '') {
					$criteria->addCondition ( "((DAY(t.request_date) = '" . $this->day_filter . "' AND request_booking_type_id != 2 AND YEAR(t.request_date) = '" . $this->year_filter . "') OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR (" . (date ( "w", strtotime ( $this->year_filter . "-" . $this->month_filter . "-" . $this->day_filter ) ) + 1) . " = t.request_day_in_week  AND request_booking_type_id = 2))" );
				}
			}
		}
		
		if ($this->hiddenPass) {
			$criteria->addCondition ( "(((t.request_date > '" . Date ( "Y-m-d" ) . "')  AND t.request_booking_type_id != 2) OR ((t.request_date = '" . Date ( "Y-m-d" ) . "' AND period_e.end_hour >= '" . date ( "H" ) . "')  AND t.request_booking_type_id != 2)   OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR ('" . date ( "Y-m-d" ) . "' BETWEEN semester.start_date AND semester.end_date AND t.request_booking_type_id = 2))" );
		}
		if (isset ( $this->status_filter ) && $this->status_filter != '') {
			
			$criteria->addCondition ( "t.status_code='" . $this->status_filter . "'" );
		}
		if (isset ( $this->room_filter ) && $this->room_filter != '') {
			$criteria->addCondition ( "t.room_id='" . $this->room_filter . "'" );
		}
		
		if (isset ( $this->user_filter ) && $this->user_filter != '') {
			$criteria->addCondition ( "t.user_login_id='" . $this->user_filter . "'" );
		}
		
		if (UserLoginUtil::getUserRole () == 3 || UserLoginUtil::getUserRole () == 4) {
			$criteria->addCondition ( "t.user_login_id = '" . UserLoginUtil::getUserLoginId () . "'" );
		}
		
		return new CActiveDataProvider ( get_class ( $this ), array (
				'criteria' => $criteria,
				'sort' => array (
						'defaultOrder' => 't.request_date, t.room_id' 
				),
				'pagination' => array (
						'pageSize' => 10 
				) 
		) );
	}
	public function searchMyBooking() {
		$criteria = new CDbCriteria ();
		$criteria->with = array (
				'semester',
				'period_e',
				'period_s',
				'user_login' 
		);
		
		if (isset ( $this->year_filter ) && $this->year_filter != '') {
			$criteria->addCondition ( "((YEAR(t.request_date) = " . $this->year_filter . " AND request_booking_type_id != 2) OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR (" . $this->year_filter . " between YEAR(semester.start_date) AND YEAR(semester.end_date)  AND request_booking_type_id = 2))" );
			
			if (isset ( $this->month_filter ) && $this->month_filter != '') {
				$criteria->addCondition ( "((MONTH(t.request_date) = '" . $this->month_filter . "' AND request_booking_type_id != 2 AND YEAR(t.request_date) = '" . $this->year_filter . "') OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR (" . $this->month_filter . " between MONTH(semester.start_date) AND MONTH(semester.end_date)  AND request_booking_type_id = 2))" );
				
				if (isset ( $this->day_filter ) && $this->day_filter != '') {
					$criteria->addCondition ( "((DAY(t.request_date) = '" . $this->day_filter . "' AND request_booking_type_id != 2 AND YEAR(t.request_date) = '" . $this->year_filter . "') OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR (" . (date ( "w", strtotime ( $this->year_filter . "-" . $this->month_filter . "-" . $this->day_filter ) ) + 1) . " = t.request_day_in_week  AND request_booking_type_id = 2))" );
				}
			}
		}
		if ($this->hiddenPass) {
			/* INITIAL FIRST LOAD THIS PAGE */
			$criteria->addCondition ( "(((t.request_date > '" . Date ( "Y-m-d" ) . "')  AND t.request_booking_type_id != 2) OR ((t.request_date = '" . Date ( "Y-m-d" ) . "' AND period_e.end_hour >= '" . date ( "H" ) . "')  AND t.request_booking_type_id != 2)   OR  (t.status_code = 'REQUEST_WAIT_APPROVE' ) OR ('" . date ( "Y-m-d" ) . "' BETWEEN semester.start_date AND semester.end_date AND t.request_booking_type_id = 2))" );
		}
		if (isset ( $this->status_filter ) && $this->status_filter != '') {
			
			$criteria->addCondition ( "t.status_code='" . $this->status_filter . "'" );
		}
		if (isset ( $this->room_filter ) && $this->room_filter != '') {
			$criteria->addCondition ( "t.room_id='" . $this->room_filter . "'" );
		}
		
		if (isset ( $this->user_filter ) && $this->user_filter != '') {
			$criteria->addCondition ( "t.user_login_id='" . $this->user_filter . "'" );
		}
		
		$criteria->addCondition ( "t.user_login_id = '" . UserLoginUtil::getUserLoginId () . "'" );
		
		return new CActiveDataProvider ( get_class ( $this ), array (
				'criteria' => $criteria,
				'sort' => array (
						'defaultOrder' => 't.request_date, t.room_id' 
				),
				'pagination' => array (
						'pageSize' => 10 
				) 
		) );
	}
}