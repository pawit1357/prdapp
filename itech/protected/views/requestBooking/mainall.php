﻿
<span class="module-head">All Booking</span>

<script type="text/javascript">
$(function(){
	var yyy = '<?php echo isset($_GET['year_filter']) ? $_GET['year_filter'] : '-1' ?>';	
	if(yyy == '-1') {
		$('#year_filter').val('<?php echo date('Y') *1?>')
		
		var mmm = '<?php echo isset($_GET['month_filter']) ? $_GET['month_filter'] : '-1' ?>';
		if(mmm == '-1') {
			$('#month_filter').val('<?php echo date('m') *1?>');
		}
		var ddd = '<?php echo isset($_GET['day_filter']) ? $_GET['day_filter'] : '-1' ?>';
		if(ddd == -1) {
			$('#day_filter').val('<?php echo date('d') *1?>');
		}
	} else {
		if(yyy == '') {
			$('#month_filter').html('<option value="">- All Month -</option>');
			$('#day_filter').html('<option value="">- All Day -</option>');
		}
	}
	//filter();	
});

function filter(){
	var data = '';
	if($('#year_filter').val() != '') {
		data = 'year_filter='+$('#year_filter').val();
		if($('#month_filter').html() == '<option value="">- All Month -</option>') {
			$('#month_filter').html('');
			$('#month_filter').append('<option value="">- All Month -</option>');
			$('#month_filter').append('<option value="1">January</option>');
			$('#month_filter').append('<option value="2">February</option>');
			$('#month_filter').append('<option value="3">March</option>');
			$('#month_filter').append('<option value="4">April</option>');
			$('#month_filter').append('<option value="5">May</option>');
			$('#month_filter').append('<option value="6">June</option>');
			$('#month_filter').append('<option value="7">July</option>');
			$('#month_filter').append('<option value="8">August</option>');
			$('#month_filter').append('<option value="9">September</option>');
			$('#month_filter').append('<option value="10">October</option>');
			$('#month_filter').append('<option value="11">November</option>');
			$('#month_filter').append('<option value="12">December</option>');
		}
	} else {
		$('#month_filter').html('<option value="">- All Month -</option>');
		$('#day_filter').html('<option value="">- All Day -</option>');
		data = 'year_filter=';
	}

	if($('#month_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'month_filter='+$('#month_filter').val();
		if($('#day_filter').html() == '<option value="">- All Day -</option>') {
			var endDayOfMonth = 31;
			if($('#month_filter').val() == '4' || $('#month_filter').val() == '6' || $('#month_filter').val() == '9' || $('#month_filter').val() == '11') {
				endDayOfMonth = 30;
			}
			if($('#month_filter').val() == '2') {
				var year = parseInt($('#year_filter').val());
				if(year % 4 == 0) {
					endDayOfMonth = 29;
				} else {
					endDayOfMonth = 28;
				}
			}
			for(var i = 1; i <= endDayOfMonth; i++) {
				$('#day_filter').append('<option value="' + i + '">' + i + '</option>');
			}	
		}
	} else {
		$('#day_filter').html('<option value="">- All Day -</option>');
		$('#day_filter').val('');
		if(data != ''){
			data = data + '&';
		}
		data = data + 'month_filter=';
	}
	
	var dayValue = '';
	if($('#day_filter').val() != '' && $('#month_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'day_filter='+$('#day_filter').val();
		dayValue = $('#day_filter').val();
	} else {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'day_filter=';
	}	
	if($('#status_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'status_filter='+$('#status_filter').val();			

	}
	if($('#room_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'room_filter='+$('#room_filter').val();			
	}
	$('#frm1').submit();	
	//$('#my-model-grid').yiiGridView('update', {url : '<?php echo Yii::app()->createUrl('RequestBooking/allBooking')?>/ajax/my-model-grid', data: data});
}
</script>
<?php 
$requestTypes = RequestBookingType::model()->findAll();
$requestStatuses = Status::model()->findAll(array('condition'=>"t.status_group_id='REQUEST_STATUS'"));
?>
<div>
	<div class="filter">
	<form id="frm1" action="" method="get">
		<b>Filter</b> <select name="year_filter" id="year_filter"
			onchange="filter()">
			<option value="">All Year</option>
			<?php 
			for($i = date("Y"); $i < (date("Y") + 5); $i++) {
			?>
			<option value="<?php echo $i?>"
			<?php echo $i == $_GET['year_filter'] ? 'selected="selected"' : ''?>>
				<?php echo $i?>
			</option>
			<?php }?>
		</select> <select name="month_filter" id="month_filter"
			onchange="filter()"><option value="">- All Month -</option>
			<option value="1"
			<?php echo 1 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>January</option>
			<option value="2"
			<?php echo 2 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>February</option>
			<option value="3"
			<?php echo 3 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>March</option>
			<option value="4"
			<?php echo 4 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>April</option>
			<option value="5"
			<?php echo 5 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>May</option>
			<option value="6"
			<?php echo 6 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>June</option>
			<option value="7"
			<?php echo 7 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>July</option>
			<option value="8"
			<?php echo 8 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>August</option>
			<option value="9"
			<?php echo 9 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>September</option>
			<option value="10"
			<?php echo 10 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>October</option>
			<option value="11"
			<?php echo 11 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>November</option>
			<option value="12"
			<?php echo 12 == $_GET['month_filter'] ? 'selected="selected"' : ''?>>December</option>
		</select> <select name="day_filter" id="day_filter"
			onchange="filter()"><option value="">- All Day -</option>
			<?php 
			for($i = 1; $i <= 31; $i++) {
				?>
			<option value="<?php echo $i?>" <?php echo $i == $_GET['day_filter'] ? 'selected="selected"' : ''?>>
				<?php echo $i?>
			</option>
			<?php }?>
		</select> <select name="status_filter" id="status_filter"
			onchange="filter()"><option value="">- All Status -</option>
			<?php 
			foreach($requestStatuses as $requestStatus) {
				?>
			<option value="<?php echo $requestStatus->status_code?>" <?php echo $requestStatus->status_code == $_GET['status_filter'] ? 'selected="selected"' : ''?>>
				<?php echo $requestStatus->name?>
			</option>
			<?php }?>
		</select> <select name="room_filter" id="room_filter"
			onchange="filter()">
			<?php 
			$rooms = RequestBooking::model()->findAll(array(
			'select'=>'t.room_id',
    		'group'=>'t.room_id',
    		'distinct'=>true,));
		?>
			<option value="">- All Room -</option>
			<?php foreach($rooms as $room) {?>
			<option value="<?php echo $room->room->id?>" <?php echo $room->room->id == $_GET['room_filter'] ? 'selected="selected"' : ''?>>
				<?php echo $room->room->name?>
			</option>
			<?php }?>
		</select>
		
		</form>
	</div>

	<div class="clear"></div>
</div>

<div class="simple-grid">
	<table class="items">
		<thead>
			<tr>
				<th width="5%">#</th>
				<th width="15%">User</th>
				<th width="14%">Booking Type</th>
				<th width="13%">Equipments</th>
				<th width="10%">Use Date</th>
				<th width="10%">Use Time</th>
				<th width="10%">Room</th>
				<th width="10%">Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php 
			$counter = 1;
			$dataProvider = $data->searchAllBooking();


			foreach ($dataProvider->data as $request) {
		?>
			<tr class="line-<?php echo $counter%2 == 0 ? '1' : '2'?>">
			<td class="center"><?php echo GridUtil::getDataIndex($dataProvider, $counter++)?></td>
				<td class="center"><?php echo $request->user_login->user_information->first_name?>
				<td class="center"><?php echo $request->request_booking_type->name?>
				<td class="center"><?php echo CommonUtil::getEquipmentList($request->id);?>
				<td class="center"><?php echo $request->request_date == null ? $request->day_in_week->name : DateTimeUtil::getDateFormat($request->request_date, "dd MM yyyy");?>
				<td class="center"><?php echo DateTimeUtil::getTimeFormat($request->period_s->start_hour, $request->period_s->start_min)." - ".DateTimeUtil::getTimeFormat($request->period_e->end_hour, $request->period_e->end_min); ?>
				<td class="center"><?php echo $request->room->room_code?></td>
				<td class="center"><?php echo $request->status->name?></td>

				<td class="center">
				<?php if(UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_REQUEST_BOOKING", "VIEW_ALL_REQUEST_BOOKING"))){?>
					<a title="View" class="ico-s-view" href="<?php echo Yii::app()->CreateUrl('RequestBooking/view/id/'.$request->id)?>"></a>
					<?php }?>
					<?php if(UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_REQUEST_BOOKING"))){?>
					<a title="edit" class="ico-s-edit" href="<?php echo Yii::app()->CreateUrl('RequestBooking/update/id/'.$request->id)?>"></a>
					<?php }?>
					<?php 
					if(UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_REQUEST_BOOKING")) && RequestUtil::hasRequestBookingActivityFile("$request->id")){
					?>
					<a title="edit" class="ico-s-download" href="<?php echo Yii::app()->request->baseUrl."/".RequestUtil::getActivityFilePath($request->id)?>"></a>
					<?php }?>
				</td>
				
			</tr>
				
		<?php }?>
		</tbody>
	</table>
				<div class="paging">
				<?php GridUtil::RenderPageButton($this, $dataProvider); ?>
			</div>
</div>

<br>


