﻿<?php 
/*
$eqs = $_POST['eqs'];
if(count($eqs) > 0) {
	foreach($eqs as $key => $val) {
		$eqType = EquipmentType::model()->findByPk($key);
		echo $eqType->name.'==='.$val . '--<br>';
	}
}
*/
?>


<div class="module-head">
	<span>Borrow</span>
</div>
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.1.custom.js"></script>
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl;?>/js/borrow/request.js"></script>

<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl;?>/js/util.js"></script>
<script type="text/javascript">
$(function(){	
	$( "#EquipmentTypeQty").spinner({min: 0, max: 20, stop: function(event, ui) {$(this).change();}    });

	$( "#from_date" ).datepicker({
        minDate: 0,
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        numberOfMonths: 1,
        changeYear: true,
        onClose: function( selectedDate, inst ) {
        	selectedDate = getDateFormat(selectedDate, 'yy-mm-dd');
            var minDate = new Date(Date.parse(selectedDate));
            var maxDate = new Date(Date.parse(selectedDate));
            minDate.setDate(minDate.getDate());
            maxDate.setDate(maxDate.getDate() + 5);
            $( "#thru_date" ).datepicker( "option", "minDate", minDate);
            $( "#thru_date" ).datepicker( "option", "maxDate", maxDate);
         }
    });

    $( "#thru_date" ).datepicker({
        minDate: "+1D",
       	dateFormat: "dd-mm-yy",
        changeMonth: true,
        numberOfMonths: 1,
        changeYear: true,
        onClose: function( selectedDate, inst ) {
        	selectedDate = getDateFormat(selectedDate, 'yy-mm-dd');
             var maxDate = new Date(Date.parse(selectedDate));
             maxDate.setDate(maxDate.getDate() - 1);            
            //$( "#from_date" ).datepicker( "option", "maxDate", maxDate);
        }
    });
	
});
</script>

<?php 
$subjects = Subject::model()->findAll();
$eventTypes = EventType::model()->findAll();
$equipmentTypes = EquipmentType::model()->findAll();
?>
<form method="post" action="">
	<table class="simple-form">
		<tr>
			<td class="column-left" width="25%">From Date</td>
			<td class="column-right"><input type="text"
				name="RequestBorrow[from_date]" id="from_date">
			</td>
		</tr>
		<tr>
			<td class="column-left">To Date</td>
			<td class="column-right"><input type="text"
				name="RequestBorrow[thru_date]" id="thru_date">
			</td>
		</tr>
		<tr>
			<td class="column-left" valign="top">Place of use</td>
			<td class="column-right"><Input type='Radio' checked="checked"
				Name='RequestBorrow[location]' value='WHITHIN_MUIC'>In MUIC <Input
				type='Radio' Name='RequestBorrow[location]' value='WITHOUT_MUIC'>Outside
				MUIC</td>
		</tr>
		<tr>
			<td class="column-left">Type of Event</td>
			<td class="column-right"><select id="type_of_event"
				name="RequestBorrow[event_type_id]">
					<option value="">--Select--</option>
					<?php foreach($eventTypes as $eventType) {?>
					<option value="<?php echo $eventType->id?>">
						<?php echo $eventType->name?>
					</option>
					<?php }?>
			</select>
			</td>
		</tr>
		<tr>
			<td class="column-left">Purpose of borrowing</td>
			<td class="column-right"><textarea name="RequestBorrow[description]"></textarea>
			</td>
		</tr>
		<?php if (UserLoginUtil::areUserRole ( array (
					UserRoles::STUDENT,
			) )) {?>
		<tr>
			<td class="column-left">Subject</td>
			<td class="column-right"><select id="teacher_id"
				name="RequestBorrow[teacher_id]">
					<option value="">--Select--</option>
					<?php foreach($subjects as $subject) {?>
					<option value="<?php echo $subject->teacher_user_id?>">
						<?php echo $subject->sbj_name?>
					</option>
					<?php }?>
			</select>
			</td>
		</tr>
		<?php }?>

	</table>
	<br>
	<table class="simple-form">
		<tr>
			<td align="right"></td>
			<td align="left">Equipment : <select name="equipment_type"
				id="equipment_type">
					<option value="">-Select-</option>
					<?php foreach ($equipmentTypes as $equipmentType){ ?>
					<option value="<?php echo $equipmentType->id?>">
						<?php echo $equipmentType->name?>
					</option>
					<?php }?>
			</select>
			</td>
			<td>Quantity : <input type="text" readonly="readonly"
				id="EquipmentTypeQty" />
			</td>
			<td><input type="button" name="add_equipment" id="add_equipment"
				value="Add Equipment" />
			</td>
		</tr>
	</table>

	<fieldset>
		<legend>Equipment List</legend>
		<div id="equipmentList"></div>
	</fieldset>
	<table class="simple-form">
		<tr>
			<td align="center"><input type="submit" name="add_request"
				value="Save" /></td>
		</tr>
	</table>

</form>

