<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.9.1.js"></script>
<script type="text/javascript">
$(function() {
	window.print();	
	window.close();	
});
</script>

<h1>Request Borrow Details</h1>

<table class="simple-form">
	<tr>
		<td class="column-left" width="120"><b>User</b></td>
		<td class="column-right"><?php echo '('.$data->user_login->username.') '.$data->user_login->user_information->personal_title.$data->user_login->user_information->first_name.' '.$data->user_login->user_information->last_name ?>
		</td>
	</tr>
	<tr>
		<td class="column-left"><b>Borrow Date</b></td>
		<td class="column-right"><?php echo DateTimeUtil::getDateFormat($data->from_date, "dd MM yyyy")?>
			- <?php echo DateTimeUtil::getDateFormat($data->thru_date, "dd MM yyyy")?>
		</td>
	</tr>
	<tr>
		<td class="column-left" valign="top"><b>Location Type</b></td>
		<td class="column-right"><?php echo $data->location == 'WHITHIN_MUIC' ? 'Within MUIC' : 'Without MUIC <br>approve by '.$data->approve_by?>
		</td>
	</tr>
	<tr>
		<td class="column-left"><b>Type of event</b></td>
		<td class="column-right"><?php echo $data->event_type->name?>
		</td>
	</tr>
	<tr>
		<td class="column-left"><b>Important Notes</b></td>
		<td class="column-right"><?php echo $data->description?>
		</td>
	</tr>
	<tr>
		<td class="column-left"><b>Status</b></td>
		<td class="column-right"><?php echo $data->status->name?></td>
	</tr>
	<tr>
		<td colspan="2">
			<fieldset>
				<legend><b>Equipment List</b></legend>
				<div id="equipmentList">
					<?php 
					$returnPriceList = array();
					$brokenList = array();
					$requestBorrowEquipmentTypes = RequestBorrowEquipmentType::model()->findAll(array('condition'=>"request_borrow_id = '".$data->id."'"));
					if(count($requestBorrowEquipmentTypes) > 0) {
					foreach($requestBorrowEquipmentTypes as $requestBorrowEquipmentType){
						?>
					<div class="eq-detail">
						<div class="item-detail-left">
							<?php echo $requestBorrowEquipmentType->equipment_type->name?> (Requested <?php echo $requestBorrowEquipmentType->quantity?>)
						</div>
						<div class="clear"></div>
						<div
							id="eq-detail-<?php echo $requestBorrowEquipmentType->equipment_type->id?>">
							<?php 
							$criteria = new CDbCriteria();
							$criteria->condition = "request_borrow_equipment_type_id = '".$requestBorrowEquipmentType->id."'";
							$requestBorrowEquipmentTypeItems = RequestBorrowEquipmentTypeItem::model()->findAll($criteria);
							if(isset($requestBorrowEquipmentTypeItems) && count($requestBorrowEquipmentTypeItems) > 0) {
									foreach($requestBorrowEquipmentTypeItems as $requestBorrowEquipmentTypeItem) {
									if($requestBorrowEquipmentTypeItem->return_price > 0) {
										$returnPriceList[count($returnPriceList)] = $requestBorrowEquipmentTypeItem;
									}
									if($requestBorrowEquipmentTypeItem->broken_price > 0) {
										$brokenList[count($brokenList)] = $requestBorrowEquipmentTypeItem;
									}
									?>
							<div class="eq-item"
								id="eq-item-<?php echo $requestBorrowEquipmentTypeItem->equipment_id?>">
								<div class="left">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-<?php echo $requestBorrowEquipmentTypeItem->equipment->barcode?>
									<?php echo $requestBorrowEquipmentTypeItem->return_date != '' ? ' ( Returned ) ' : '' ?>
								</div>
								<input type="hidden"
									id="eq_item_req_<?php echo $requestBorrowEquipmentTypeItem->equipment_id?>"
									name="eq_item[<?php echo $requestBorrowEquipmentType->equipment_type->id?>]"
									value="<?php echo $requestBorrowEquipmentTypeItem->equipment_id?>">
								<div class="clear"></div>
							</div>
							<?php }
}?>
						</div>
					</div>
					<?php 
					}
				} else {
					echo '<i>- no item found -</i>';
				}

				?>
				</div>
			</fieldset>
		</td>
	</tr>
	<?php if(count($returnPriceList) > 0) {?>
	<tr>
		<td colspan="2">
			<fieldset>
				<legend><b>Return Late List</b></legend>
				<div class="simple-grid">
					<table class="items">
						<tr>
							<th width="500">Equipment</th>
							<th width="200">Price</th>
						</tr>
						<?php 
						$counter = 1;
						$sum = 0;
						foreach ($returnPriceList as $returnPrice) {
				$sum = $sum + $returnPrice->return_price;
				?>
						<tr bgcolor="<?php echo $counter%2 == 0 ? '#F4F4F4' : '#E8E8E8'?>">
							<td color="black" align="left"><?php echo $returnPrice->equipment->barcode?>
							</td>
							<td align="right"><?php echo number_format($returnPrice->return_price)?>
							</td>
						</tr>
						<?php }?>
						<tr>
							<td align="right">Total</td>
							<td align="right"><?php echo number_format($sum)?>
							</td>
						</tr>
					</table>
				</div>
			</fieldset>
		</td>
	</tr>
	<?php }?>
	<?php if(count($brokenList) > 0) {?>
	<tr>
		<td colspan="2">
			<fieldset>
				<legend><b>Broken List</b></legend>
				<div class="simple-grid">
					<table width="100%">
						<tr>
							<th width="500">Equipment</th>
							<th width="200">Price</th>
						</tr>
						<?php 
						$counter = 1;
						$sum = 0;
						foreach ($brokenList as $broken) {
				$sum = $sum + $broken->broken_price;
				?>
						<tr bgcolor="<?php echo $counter%2 == 0 ? '#F4F4F4' : '#E8E8E8'?>">
							<td align="left"><?php echo $broken->equipment->barcode?>
							</td>
							<td align="right"><?php echo number_format($broken->broken_price)?>
							</td>
						</tr>
						<?php }?>
						<tr>
							<td align="right">Total</td>
							<td align="right"><?php echo number_format($sum)?>
							</td>
						</tr>
					</table>
				</div>
			</fieldset>
		</td>
	</tr>
	<?php }?>
</table>
