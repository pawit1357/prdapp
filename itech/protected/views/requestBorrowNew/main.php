﻿<div>
	<div class="search-box">
		<?php
		$form = $this->beginWidget ( 'CActiveForm', array (
				'id' => 'room-form',
				'method' => 'get',
				'action' => '',
				'enableAjaxValidation' => false
		) );
		?>
		<input type="text" name="search_text"
			value="<?php echo $_GET['search_text']?>">
		<?php $this->endWidget(); ?>
		<br>
	</div>
</div>
<br>
<div>
	<table>
		<tr>
			<td><?php echo CHtml::image('/itech/images/a1-icon-yellow.png', 'Waiting apporve.'); ?>
			</td>
			<td>Waiting apporve.</td>
			<td></td>

			<td><?php echo CHtml::image('/itech/images/a1-icon-green.png', 'Waiting apporve.'); ?>
			</td>
			<td>Apporve</td>
			<td></td>

			<td><?php echo CHtml::image('/itech/images/a1-icon-red.png', 'Waiting apporve.'); ?>
			</td>
			<td>Disapporve</td>
			<td></td>
		</tr>
	</table>
</div>
<br>
<div></div>
<div class="simple-grid">
	<table class="items">
		<thead>
			<tr>
				<th width="5%">#</th>
				<th width="7%">ID</th>
				<th>User</th>
				<th width="14%">Request Date</th>
				<th width="13%">Return Date</th>
				<th width="7%">Remain</th>
				<th width="10%">Approve 1</th>
				<?php if(!UserLoginUtil::areUserRole(array(UserRoles::STAFF, UserRoles::LECTURER))) {?>
				<th width="10%">Approve 2</th>
				<th width="10%">Approve 3</th>
				<?php }?>
				<th width="13%">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$counter = 1;
			$dataProvider = $data->search ();
			foreach ( $dataProvider->data as $request ) {
				?>
			<tr class="line-<?php echo $counter%2 == 0 ? '1' : '2'?>">
				<td class="center"><?php echo GridUtil::getDataIndex($dataProvider, $counter++)?>
				</td>
				<td class="center"><?php echo $request->id?>
				</td>
				<td class="center"><?php echo $request->user_login->username ?>
				</td>
				<td class="center"><?php echo DateTimeUtil::getDateFormat($request->from_date, "dd MM yyyy")?>
				</td>
				<td class="center"><?php echo DateTimeUtil::getDateFormat($request->thru_date, "dd MM yyyy")?>
				</td>
				<td class="center"><?php echo getRemainContent(date("Y-m-d"), $request->thru_date)?>
				</td>
				<?php if(!UserLoginUtil::areUserRole(array(UserRoles::STAFF, UserRoles::LECTURER))) {?>
				<td class="center"><?php
				switch ($request->status_code) {
					case 'R_B_NEW_WAIT_APPROVE_1' :
						echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a1-icon-yellow.png", "", array (
						'width' => 16,
						'height' => 15
						) );
						break;
					case 'R_B_NEW_WAIT_APPROVE_2' :
						echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a1-icon-green.png", "", array (
						'width' => 16,
						'height' => 15
						) );
						break;
					case 'R_B_NEW_WAIT_APPROVE_3' :
						if(UserLoginUtil::areUserRoleById(array(UserRoles::ADMIN, UserRoles::STAFF,UserRoles::LECTURER,UserRoles::STAFF_AV),$request->user_login_id)){
							echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a1-icon-yellow.png", "", array (
									'width' => 16,
									'height' => 15
							) );
						}else{
							echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a1-icon-green.png", "", array (
							'width' => 16,
							'height' => 15
							) );
						}
						break;
					case 'R_B_NEW_DISAPPROVE_1' :
							echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a1-icon-red.png", "", array (
							'width' => 16,
							'height' => 15
							) );
							break;
					case 'R_B_NEW_DISAPPROVE_2' :
						echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a1-icon-red.png", "", array (
						'width' => 16,
						'height' => 15
						) );
						break;
					case 'R_B_NEW_DISAPPROVE_3' :
						echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a1-icon-red.png", "", array (
						'width' => 16,
						'height' => 15
						) );
						break;
				}

				if ($request->status_code == 'R_B_NEW_PREPARE' || $request->status_code == 'R_B_NEW_READY' || $request->status_code == 'R_B_NEW_RETURNED' || $request->status_code == 'R_B_NEW_READY_MISSING' || $request->status_code == 'R_B_NEW_RETURNED_MISSING') {
					echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a1-icon-green.png", "", array (
							'width' => 16,
							'height' => 15
					) );
				}
?>
				</td>
				<td class="center"><?php
				
				switch ($request->status_code) {
					case 'R_B_NEW_WAIT_APPROVE_2' :
						echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a2-icon-yellow.png", "", array (
						'width' => 16,
						'height' => 15
						) );
						break;
					case 'R_B_NEW_DISAPPROVE_2' :
						echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a2-icon-red.png", "", array (
						'width' => 16,
						'height' => 15
						) );
						break;
					case 'R_B_NEW_WAIT_APPROVE_3' :
						echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a2-icon-green.png", "", array (
						'width' => 16,
						'height' => 15
						) );
						break;
					case 'R_B_NEW_DISAPPROVE_3' :
						echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a2-icon-green.png", "", array (
						'width' => 16,
						'height' => 15
						) );
						break;
				}
				if ($request->status_code == 'R_B_NEW_PREPARE' || $request->status_code == 'R_B_NEW_READY' || $request->status_code == 'R_B_NEW_RETURNED' || $request->status_code == 'R_B_NEW_READY_MISSING' || $request->status_code == 'R_B_NEW_RETURNED_MISSING') {
					echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a2-icon-green.png", "", array (
							'width' => 16,
							'height' => 15
					) );
				}
				?>
				</td>
				<?php }?>
				<td class="center"><?php
				switch ($request->status_code) {
					case 'R_B_NEW_WAIT_APPROVE_3' :
						echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a3-icon-yellow.png", "", array (
						'width' => 16,
						'height' => 15
						) );
						break;
					case 'R_B_NEW_DISAPPROVE_3' :
						echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a3-icon-red.png", "", array (
						'width' => 16,
						'height' => 15
						) );
						break;
				}
				if ($request->status_code == 'R_B_NEW_PREPARE' || $request->status_code == 'R_B_NEW_READY' || $request->status_code == 'R_B_NEW_RETURNED' || $request->status_code == 'R_B_NEW_READY_MISSING' || $request->status_code == 'R_B_NEW_RETURNED_MISSING') {
					echo CHtml::image ( Yii::app ()->request->baseUrl . "/images/a3-icon-green.png", "", array (
							'width' => 16,
							'height' => 15
					) );
				}
				?>
				</td>

				<td class="center"><a title="View" class="ico-s-view"
					href="<?php echo Yii::app()->CreateUrl('RequestBorrowNew/view/id/'.$request->id)?>"></a>
					
					<?php if(UserLoginUtil::areUserRole(array(UserRoles::ADMIN, UserRoles::STAFF_AV)) && in_array($request->status_code, array('R_B_NEW_PREPARE', 'R_B_NEW_READY_MISSING'))) {?>
					<a title="Prepare" class="ico-s"
					style="background: url(../images/data_edit.png)"
					href="<?php echo Yii::app()->CreateUrl('RequestBorrowNew/prepare/id/'.$request->id)?>"></a>
					<?php }?> 
					
					<?php if(UserLoginUtil::areUserRole(array(UserRoles::ADMIN, UserRoles::STAFF_AV)) && in_array($request->status_code, array('R_B_NEW_READY', 'R_B_NEW_READY_MISSING', 'R_B_NEW_RETURNED_MISSING'))) {?>
					<a title="Return" class="ico-s"
					style="background: url(../images/data_out.png)"
					href="<?php echo Yii::app()->CreateUrl('RequestBorrowNew/return/id/'.$request->id)?>"></a>
					<?php }?>
					
					<?php if(UserLoginUtil::getUserLoginId() == $request->user_login_id || UserLoginUtil::areUserRole(array(UserRoles::ADMIN))) {?>
					 <?php if(!in_array($request->status_code, array('R_B_NEW_READY', 'R_B_NEW_READY_MISSING', 'R_B_NEW_RETURNED_MISSING'))) {?>
					<a title="Delete" class="ico-s-delete"
					onclick="return confirm('Are you sure to delete?')"
					href="<?php echo Yii::app()->CreateUrl('RequestBorrowNew/delete/id/'.$request->id)?>"></a>
					<?php }}?>
					
				</td>
			</tr>
			<?php
			}
			if ($counter == 1) {
				?>
			<tr>
				<td colspan="9"><i>-No item found-</i></td>
			</tr>
			<?php }?>
		</tbody>
	</table>
	<div class="paging">
		<?php GridUtil::RenderPageButton($this, $dataProvider); ?>
	</div>
</div>

<?php
function getRemainContent($startDate, $endDate) {
	$class = "";
	$remain = DateTimeUtil::getDayRemain ( $startDate, $endDate );
	if ($remain == 0) {
		$class = "text-orange";
	} else if ($remain < 0) {
		$class = "text-red";
	} else if ($remain == 1) {
		$class = "text-yellow";
	}
	return '<span class="' . $class . '"><b>' . $remain . "</b></span>";
}
?>
