﻿<link rel="stylesheet" type="text/css"
	href="<?php echo Yii::app()->request->baseUrl; ?>/css/prepare.css"
	media="screen, projection" />
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl;?>/js/borrow/prepare.js"></script>
<span class="module-head">Request Borrow Prepare Item</span>
<input type="hidden"
	id="base_url" value="<?php echo Yii::app()->getBaseUrl()?>">
<table class="simple-form">
	<tr>
		<td class="column-left" width="150">From Date</td>
		<td class="column-right"><?php echo DateTimeUtil::getDateFormat($data->from_date, "dd MM yyyy")?>
		</td>
	</tr>
	<tr>
		<td class="column-left">To Date</td>
		<td class="column-right"><?php echo DateTimeUtil::getDateFormat($data->thru_date, "dd MM yyyy")?>
		</td>
	</tr>
	<tr>
		<td class="column-left" valign="top">Location Type</td>
		<td class="column-right"><?php echo $data->location == 'WHITHIN_MUIC' ? 'Within MUIC' : 'Without MUIC <br>approve by '.$data->approve_by?>
		</td>
	</tr>
	<tr>
		<td class="column-left">Type of event</td>
		<td class="column-right"><?php echo $data->event_type->name?>
		</td>
	</tr>
	<tr>
		<td class="column-left">Important Notes</td>
		<td class="column-right"><?php echo $data->description?>
		</td>
	</tr>
	<tr>
		<td class="column-left">Status</td>
		<td class="column-right"><?php echo $data->status->name?></td>
	</tr>
	<tr>
		<td colspan="2"><br>
			<div class="barcode-area">
				<div id="barcode-status-area" class="barcode-status ready">
					<b>Barcode Status</b>
					<div class="text" id="barcode-status-text">Ready</div>
				</div>
				<div class="barcode-panel">
					<div class="barcode-input">
						<input id="barcode" type="text">
					</div>
					<div id="last-scan-result" class=""></div>
				</div>
				<div class="clear"></div>
			</div> <br>
			<form action="" method="post">
				<fieldset>
					<legend>Equipment List</legend>
					<div id="equipmentList">
						<?php 
						$requestBorrowEquipmentTypes = RequestBorrowEquipmentType::model()->findAll(array('condition'=>"request_borrow_id = '".$data->id."'"));
						if(count($requestBorrowEquipmentTypes) > 0) {
					foreach($requestBorrowEquipmentTypes as $requestBorrowEquipmentType){
						?>
						<input type="hidden"
							name="eqids[<?php echo $requestBorrowEquipmentType->equipment_type->id?>]"
							value="<?php echo $requestBorrowEquipmentType->equipment_type->id?>">
						<input type="hidden"
							id="eqs-<?php echo $requestBorrowEquipmentType->equipment_type->id?>"
							value="<?php echo $requestBorrowEquipmentType->quantity?>">
							
							<?php $criteria = new CDbCriteria();
								$criteria->condition = "request_borrow_equipment_type_id = '".$requestBorrowEquipmentType->id."'";
								$requestBorrowEquipmentTypeItems = RequestBorrowEquipmentTypeItem::model()->findAll($criteria);
								
								?>
						<div class="eq-detail-p <?php echo count($requestBorrowEquipmentTypeItems) == $requestBorrowEquipmentType->quantity ? 'complete' : 'incomplete'?>"
							id="eq-detail-head-<?php echo $requestBorrowEquipmentType->equipment_type->id?>">
							<div class="item-detail-left">
								<?php echo $requestBorrowEquipmentType->equipment_type->name?>
							</div>
							<div id="qt-'+ eq+ '" class="item-detail-right">
								<?php echo $requestBorrowEquipmentType->quantity?>
							</div>
							<div class="clear"></div>
							<div
								id="eq-detail-<?php echo $requestBorrowEquipmentType->equipment_type->id?>">
								<?php 
								
								if(isset($requestBorrowEquipmentTypeItems) && count($requestBorrowEquipmentTypeItems) > 0) {
									foreach($requestBorrowEquipmentTypeItems as $requestBorrowEquipmentTypeItem) {
								?>
								<div class="eq-item"
									id="eq-item-<?php echo $requestBorrowEquipmentTypeItem->equipment_id?>">
									<div class="left"><?php echo $requestBorrowEquipmentTypeItem->equipment->barcode?></div>
									<input type="hidden"
										id="eq_item_req_<?php echo $requestBorrowEquipmentTypeItem->equipment_id?>"
										name="eq_item[<?php echo $requestBorrowEquipmentTypeItem->equipment_id?>]"
										value="<?php echo $requestBorrowEquipmentType->equipment_type->id?>">
									<div class="clear"></div>
								</div>
								<?php }
}?>
							</div>
						</div>
						<?php 
					}
				} else {
					echo '<i>- no item found -</i>';
				}

				?>
					</div>
				</fieldset>
				<br>
				<div align="center">
					<input type="submit" name="save_request" value="Save" />
				</div>
			</form>
		</td>
	</tr>
</table>
