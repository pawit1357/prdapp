﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MTAOBatch.DAO;

namespace MTAOBatchAutoApprove
{
    class Program : IDisposable
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            int total = 0;
            int success = 0;

            Console.WriteLine("## Start Run MTAOBatchAutoApprove ##");
            logger.Debug("## Start Run MTAOBatchAutoApprove ##");
            try
            {
                using (MungthaiEntities context = new MungthaiEntities())
                {
                    
                    //ForNew
                    List<tbDCusCard> checkAT = context.tbDCusCardS.Where(" (it.[FinanceUpDateBy]=='' or it.[FinanceUpDateBy] is NULL ) and it.[FinanceUpDateDate] is NULL    and it.[InsurNo] IS NOT NULL ").ToList();

                    if (checkAT.Count > 0)
                    {
                        total = checkAT.Count;

                        foreach (tbDCusCard data in checkAT)
                        {
                            List<tbDCusCard> tbCC = context.tbDCusCardS.Where(" it.[DocuMentNo]='" + data.DocuMentNo + "'  and it.[WorkType]=='999' and (it.[FinanceUpDateBy]=='' or it.[FinanceUpDateBy] is NULL ) and it.[InsurNo] IS NOT NULL  ").ToList();
                            if (tbCC.Count > 0)
                            {
                                foreach (tbDCusCard tbdCC in tbCC)
                                {
                                    tbdCC.Status = "1";
                                    tbdCC.FinanceUpDateBy = "AUTO APPROVE";
                                    tbdCC.FinanceUpDateDate = DateTime.Now;
                                    Console.WriteLine("## Update doc:{0}", data.DocuMentNo);
                                }
                            }
                            else {
                                logger.Debug(String.Format("tbDCusCard size equal 0."));
                            }
                        }
                        success = context.SaveChanges();
                        Console.WriteLine("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success));
                        logger.Debug(String.Format("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success)));
                    }
                    else {
                        logger.Debug(String.Format("tbDCusCard size equal 0."));
                    }
                    //clear old data
                    total = 0;
                    success = 0;

                    //ForCountService
                    List<tbDCusCard> checkATC = context.tbDCusCardS.Where(" (it.[FinanceUpDateBy]=='' or it.[FinanceUpDateBy] is NULL ) and it.[PayInslipCode] is not null   and  it.[PayStatus]='T'  ").ToList();                                      
                    if (checkATC.Count > 0)
                    {
                        total = checkAT.Count;
                        foreach (tbDCusCard dataC in checkATC)
                        {
                            List<tbDCusCard> tbCCC = context.tbDCusCardS.Where(" it.[InsurNo]='" + dataC.InsurNo + "' and it.[WorkType]!='999'    and it.[PayStatus]='T'   and it.[StatusTransfer]='T'  and (it.[FinanceUpDateBy]=='' or it.[FinanceUpDateBy] is NULL ) ").ToList();
                            if (tbCCC.Count > 0)
                            {
                                foreach (tbDCusCard tbdCCC in tbCCC)
                                {
                                    tbdCCC.Status = "1";
                                    tbdCCC.FinanceUpDateBy = "AUTO APPROVE";
                                    tbdCCC.FinanceUpDateDate = DateTime.Now;
                                    Console.WriteLine("## Update doc:{0}", dataC.DocuMentNo);
                                }
                            }
                            else {
                                logger.Debug(String.Format("tbDCusCard size equal 0."));
                            }
   
                        }
                        success = context.SaveChanges();
                        Console.WriteLine("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success));
                        logger.Debug(String.Format("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success)));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                logger.Error(ex.StackTrace);
            }
            Console.WriteLine("## End Run MTAOBatchAutoApprove ##");
            logger.Debug("## End Run MTAOBatchAutoApprove ##");
            //Console.ReadLine();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
