﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using MTAOBatch.Biz;

namespace MTAOBatch.DAO
{
    public class DefaultDAO
    {

        public static List<tbDCusCard> getCheckAutoApprove()
        {
            List<tbDCusCard> lists = new List<tbDCusCard>();
            String sql = " select tbDCusCardS.CardSNo as CardSNo,tbDCusCardS.DocuMentNo as DocuMentNo,tbDCusCardS.InsurNo as InsurNo,tbDCusCardS.CardFID as CardFID, " +
                         " LEFT(TempID,6)AS SURCPTNO1,RIGHT(TempID,6)AS SURCPTNO2 " +
                         " from tbDCusCardS join tbMCardS on tbDCusCardS.CardSNo=tbMCardS.CardSNo  " +
                         " where tbDCusCardS.FinanceUpDateBy is null and tbDCusCardS.FinanceUpDateDate is null " +
                         " and tbMCardS.WorkStatus='สมบูรณ์' and tbDCusCardS.InsurNo is not null AND tbDCusCardS.WorkType !='999' and DATALENGTH((LTRIM(RTRIM(TempID))))=12 ";
            using (SqlConnection con = new SqlConnection(Configurations.BaseMungthai))
            {

                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int index = 1;
                while (dr.Read())
                {
                    tbDCusCard data = new tbDCusCard()
                    {
                        CardFID = Convert.ToInt32(dr["CardFID"]),
                        CardSNo = Convert.ToString(dr["CardSNo"]),
                        DocuMentNo = Convert.ToString(dr["DocuMentNo"]),
                        InsurNo = Convert.ToString(dr["InsurNo"]),
                        TempID = Convert.ToString(dr["SURCPTNO1"]),
                        TX_ID = Convert.ToString(dr["SURCPTNO2"])

                    };
                    lists.Add(data);
                    index++;
                }
            }
            return lists;
        }

        public static String GetPolicyNumberByApplicationNumber2(String DocuMentNo)
        {
            String messageReturn = "";
            try{
            ServiceAdminForAo.WS_Admin_ForAOPortTypeClient proxy = new ServiceAdminForAo.WS_Admin_ForAOPortTypeClient();
            String fld_admin_username = "APLUSER";
            String fld_admin_password = "RTYHGF";
            String fld_application_number = DocuMentNo;
            String fld_sessionID = "";
            String fld_result = "";
            String fld_policy_number = "";
            String fld_plan_name = "";
            String fld_status = "";
            String fld_client_name = "";
            String fld_fpo = "";
            String fld_client_surname = "";
            String fld_client_title = "";
            String fld_approve_date = "";
            String respone = proxy.GetPolicyNumberByApplicationNumber(fld_admin_username, fld_admin_password, fld_application_number,
                                                                       out fld_sessionID, out fld_result, out fld_policy_number, out fld_plan_name,
                                                                       out fld_status, out fld_client_name, out fld_fpo, out fld_client_surname,
                                                                       out fld_client_title, out fld_approve_date);
            if (respone != null)
            {
                if (respone.Equals("ไม่พบเลข Application"))
                {

                    messageReturn = "404,ไม่พบเลข Application";
                }
                else
                {
                    messageReturn = fld_policy_number;
                }
            }
            else {
                messageReturn = "404,Check DocuMentNo from GetPolicyNumberByApplicationNumber return value is null.";
            }
            }catch(Exception ex)
            {
                return "404,"+ex.Message;

            }
            return messageReturn;
        }
    
    }
}
