﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using MTAOBatch.DAO;
using System.Data;
using MTAOBatch.Biz;

namespace MTAOBatchAutoApproveRenew
{
    class Program : IDisposable
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            int total = 0;
            int success = 0;

            Console.WriteLine("## Start Run MTAOBatchAutoApproveRenew ##");
            logger.Debug("## Start Run MTAOBatchAutoApproveRenew ##");
            try
            {
                using (OdbcConnection con2 = new OdbcConnection(Configurations.As400ConStr))
                {
                    con2.Open();

                    using (MungthaiEntities context = new MungthaiEntities())
                    {
                        //ForNew

                        List<tbDCusCard> checkAT = DefaultDAO.getCheckAutoApprove().ToList();

                        if (checkAT.Count > 0)
                        {
                            total = checkAT.Count;

                            foreach (tbDCusCard data in checkAT)
                            {
                                // For Renew  paid not via Counter Servide by Check with AS400
                                String statusPaid = "";
                                string sql = "select SU_STATUS   from CP3FPRD.ORDUSU  where  SU_REF_NO='" + data.InsurNo + "' AND SU_LOG_TYPE  ='R' AND SU_TEMP_RCPT_NO_1 ='" + data.TempID + "' AND SU_TEMP_RCPT_NO_2= '" + data.TX_ID + "' ";
                                OdbcDataAdapter da = new OdbcDataAdapter(sql, con2);
                                DataSet ds = new DataSet();
                                da.Fill(ds, "tableTo");
                                DataTable dtDCusCardS = ds.Tables["tableTo"];

                                if (ds.Tables["tableTo"].Rows.Count > 0)
                                {
                                    statusPaid = (string)ds.Tables["tableTo"].Rows[0][0].ToString();
                                    // Status "A" maen paid 
                                    if (statusPaid.Equals("A"))
                                    {
                                        Console.WriteLine("<br>   statusPaid =" + statusPaid + "<br> CardFID =" + data.CardFID + "<br> ");
                                        logger.Debug("<br>   statusPaid =" + statusPaid + "<br> CardFID =" + data.CardFID + "<br> ");
                                        List<tbDCusCard> tbCC = context.tbDCusCardS.Where(" it.[CardFID]=" + data.CardFID + "  and (it.[FinanceUpDateBy] is null or it.[FinanceUpDateBy] ='')  ").ToList();
                                        if (tbCC.Count > 0)
                                        {
                                            foreach (tbDCusCard tbdCC in tbCC)
                                            {
                                                tbdCC.FinanceUpDateBy = "AUTO APPROVE";
                                                tbdCC.Status = "1";
                                                tbdCC.FinanceUpDateDate = DateTime.Now;

                                                Console.WriteLine("## Update doc:{0}", data.DocuMentNo);
                                            }
                                        }
                                        else
                                        {
                                            logger.Debug(String.Format("tbCC size equal 0."));
                                        }
                                    }
                                    else
                                    {
                                        logger.Debug(String.Format("Paid status not equal 'A'."));
                                    }
                                }
                                else
                                {
                                    logger.Debug(String.Format("dtDCusCardS size equal 0."));
                                }
                            }
                            success = context.SaveChanges();
                            Console.WriteLine("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success));
                            logger.Debug(String.Format("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success)));
                        }
                        else
                        {
                            logger.Debug(String.Format("tbDCusCard size equal 0."));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                logger.Error(ex.StackTrace);
            }
            Console.WriteLine("## End Run MTAOBatchAutoApproveRenew ##");
            logger.Debug("## End Run MTAOBatchAutoApproveRenew ##");
            //Console.ReadLine();
        }

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
