<div class="module-head">Semester</div>
<div>
	<?php 
	if(UserLoginUtil::hasPermission(array("FULL_ADMIN", "CREATE_USER"))){
		echo CHtml::link('Create New',array('semester/create'),array('class'=>'add'));
	}
	?>

	<div class="search-box">
		<?php 
		$form = $this->beginWidget('CActiveForm', array(
				'id' => 'room-form',
				'method'=>'get',
				'action'=>'',
				'enableAjaxValidation' => false,
		));
		?>
		<input type="text" name="search_text" value="<?php echo $_GET['search_text']?>">
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid',
		'dataProvider' => $data->search(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'name'=>'semester_number',
				),
				array(
						'name'=>'academic_year',
				),
				array(
						'name'=>'name',
				),
				array(
						'name'=>'start_date',
						'value'=>'DateTimeUtil::getDateFormat($data->start_date, "dd MM yyyy");',
				),
				array(
						'name'=>'end_date',
						'value'=>'DateTimeUtil::getDateFormat($data->end_date, "dd MM yyyy");',
				),
				array(
						'header'=>'Active',
						'type'=>'html',
						'value'=>'(date("Y-m-d H:i:s") >= $data->start_date && date("Y-m-d H:i:s") <= $data->end_date) ? "<img src=\"'.Yii::app()->getBaseUrl().'/images/active.png\">" : "<img src=\"'.Yii::app()->getBaseUrl().'/images/inactive.png\">"',
						'htmlOptions'=>array('align'=>'center'),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'template'=>'{view} {update} {delete}',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'buttons'=>array
						(
								'view' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_USER"))',
								),
								'update' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_USER"))',
								),
								'delete' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "DELETE_USER"))',
								),
						),
				),
		),
));
?>
