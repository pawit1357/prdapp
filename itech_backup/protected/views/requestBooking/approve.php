<span class="module-head">Approve</span>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid',
		'dataProvider' => $data->search1(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'header'=>'User',
						'value'=>'$data->user_login->user_information->first_name',
				),
				array(
						'header'=>'Booking Type',
						'value'=>'$data->request_booking_type->name',
						'htmlOptions'=>array('width'=>'14%', 'align'=>'center'),
				),
				array(
						'header'=>'Requested Date',
						'value'=>'DateTimeUtil::getDateFormat($data->create_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'15%', 'align'=>'center'),
				),
				array(
						'header'=>'Use Date',
						'value'=>'$data->request_date == null ? $data->day_in_week->name : DateTimeUtil::getDateFormat($data->request_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'12%', 'align'=>'center'),
				),
				array(
						'header'=>'Use Time',
						'value'=>'DateTimeUtil::getTimeFormat($data->period_s->start_hour, $data->period_s->start_min)." - "'.
						'.DateTimeUtil::getTimeFormat($data->period_e->end_hour, $data->period_e->end_min)',
						'htmlOptions'=>array('width'=>'12%', 'align'=>'center'),
				),
				array(
						'header'=>'Room',
						'value'=>'$data->room->room_code',
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'header'=>'Status',
						'value'=>'$data->status->name',
						'htmlOptions'=>array('width'=>'15%', 'align'=>'center'),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'header'=>'Approve',
						'template'=>'{Approve}',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'buttons'=>array
						(
								'Approve' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "EDIT_REQUEST_BOOKING"))',
										'url' => 'Yii::app()->createUrl("/RequestBooking/ApproveRequest/")."/id/".$data->id',
								),
						),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'header'=>'Disapprove',
						'template'=>'{Disapprove}',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'buttons'=>array
						(
								'Disapprove' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "EDIT_REQUEST_BOOKING"))',
										'url' => 'Yii::app()->createUrl("/RequestBooking/DisapproveRequest/")."/id/".$data->id',
								),
						),
				),
		),
));
?>
<br>

