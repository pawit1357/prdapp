﻿
<?php if(UserLoginUtil::getUserRole() == '1' || UserLoginUtil::getUserRole() == '2') {?>

<span class="module-head">All Booking</span>

<script type="text/javascript">
$(function(){
	if($('#year_filter').val() != '') {
		$('#month_filter').val('<?php echo date('m') *1?>');
		$('#day_filter').val('<?php echo date('d') *1?>');
	} else {
		$('#month_filter').html('<option value="">- All Month -</option>');
		$('#day_filter').html('<option value="">- All Day -</option>');
	}
	//filter();	
});
function filter(){
	var data = '';
	if($('#year_filter').val() != '') {
		data = 'year_filter='+$('#year_filter').val();
		if($('#month_filter').html() == '<option value="">- All Month -</option>') {
			$('#month_filter').html('');
			$('#month_filter').append('<option value="">- All Month -</option>');
			$('#month_filter').append('<option value="1">January</option>');
			$('#month_filter').append('<option value="2">February</option>');
			$('#month_filter').append('<option value="3">March</option>');
			$('#month_filter').append('<option value="4">April</option>');
			$('#month_filter').append('<option value="5">May</option>');
			$('#month_filter').append('<option value="6">June</option>');
			$('#month_filter').append('<option value="7">July</option>');
			$('#month_filter').append('<option value="8">August</option>');
			$('#month_filter').append('<option value="9">September</option>');
			$('#month_filter').append('<option value="10">October</option>');
			$('#month_filter').append('<option value="11">November</option>');
			$('#month_filter').append('<option value="12">December</option>');
		}
	} else {
		$('#month_filter').html('<option value="">- All Month -</option>');
		$('#day_filter').html('<option value="">- All Day -</option>');
		data = 'year_filter=';
	}

	if($('#month_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'month_filter='+$('#month_filter').val();
		if($('#day_filter').html() == '<option value="">- All Day -</option>') {
			var endDayOfMonth = 31;
			if($('#month_filter').val() == '4' || $('#month_filter').val() == '6' || $('#month_filter').val() == '9' || $('#month_filter').val() == '11') {
				endDayOfMonth = 30;
			}
			if($('#month_filter').val() == '2') {
				var year = parseInt($('#year_filter').val());
				if(year % 4 == 0) {
					endDayOfMonth = 29;
				} else {
					endDayOfMonth = 28;
				}
			}
			for(var i = 1; i <= endDayOfMonth; i++) {
				$('#day_filter').append('<option value="' + i + '">' + i + '</option>');
			}	
		}
	} else {
		$('#day_filter').html('<option value="">- All Day -</option>');
		$('#day_filter').val('');
		if(data != ''){
			data = data + '&';
		}
		data = data + 'month_filter=';
	}
	
	var dayValue = '';
	if($('#day_filter').val() != '' && $('#month_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'day_filter='+$('#day_filter').val();
		dayValue = $('#day_filter').val();
	} else {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'day_filter=';
	}
	if($('#status_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'status_filter='+$('#status_filter').val();			

	}
	if($('#room_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'room_filter='+$('#room_filter').val();			
	}
		//alert(data);
	$('#my-model-grid').yiiGridView('update', {url : '<?php echo Yii::app()->createUrl('RequestBooking/AllBooking')?>/ajax/my-model-grid', data: data});
}
</script>
<?php 
$requestTypes = RequestBookingType::model()->findAll();
$requestStatuses = Status::model()->findAll(array('condition'=>"t.status_group_id='REQUEST_STATUS'"));
?>
<div>
	<div class="filter">
		<b>Filter</b> <select name="year_filter" id="year_filter"
			onchange="filter()">
			<option value="">All Year</option>
			<?php 
			for($i = date("Y"); $i < (date("Y") + 5); $i++) {
			?>
			<option value="<?php echo $i?>"
			<?php echo $i == date("Y") ? 'selected="selected"' : ''?>>
				<?php echo $i?>
			</option>
			<?php }?>
		</select> <select name="month_filter" id="month_filter"
			onchange="filter()"><option value="">- All Month -</option>
			<option value="1" <?php echo 1 == date("n") ? 'selected="selected"' : ''?>>January</option>
			<option value="2" <?php echo 2 == date("n") ? 'selected="selected"' : ''?>>February</option>
			<option value="3" <?php echo 3 == date("n") ? 'selected="selected"' : ''?>>March</option>
			<option value="4" <?php echo 4 == date("n") ? 'selected="selected"' : ''?>>April</option>
			<option value="5" <?php echo 5 == date("n") ? 'selected="selected"' : ''?>>May</option>
			<option value="6" <?php echo 6 == date("n") ? 'selected="selected"' : ''?>>June</option>
			<option value="7" <?php echo 7 == date("n") ? 'selected="selected"' : ''?>>July</option>
			<option value="8" <?php echo 8 == date("n") ? 'selected="selected"' : ''?>>August</option>
			<option value="9" <?php echo 9 == date("n") ? 'selected="selected"' : ''?>>September</option>
			<option value="10" <?php echo 10 == date("n") ? 'selected="selected"' : ''?>>October</option>
			<option value="11" <?php echo 11 == date("n") ? 'selected="selected"' : ''?>>November</option>
			<option value="12" <?php echo 12 == date("n") ? 'selected="selected"' : ''?>>December</option>
		</select>
		<select name="day_filter" id="day_filter"
			onchange="filter()"><option value="">- All Day -</option>
			<?php 
			for($i = 1; $i <= 31; $i++) {
				?>
			<option value="<?php echo $i?>">
				<?php echo $i?>
			</option>
			<?php }?>
		</select>
		<select name="status_filter" id="status_filter"
			onchange="filter()"><option value="">- All Status -</option>
			<?php 
			foreach($requestStatuses as $requestStatus) {
				?>
			<option value="<?php echo $requestStatus->status_code?>">
				<?php echo $requestStatus->name?>
			</option>
			<?php }?>
		</select> 
		<select name="room_filter" id="room_filter" onchange="filter()">
			<?php 
			$rooms = RequestBooking::model()->findAll(array(
			'select'=>'t.room_id',
    		'group'=>'t.room_id',
    		'distinct'=>true,));
		?>
			<option value="">- All Room -</option>
			<?php foreach($rooms as $room) {?>
			<option value="<?php echo $room->room->id?>">
				<?php echo $room->room->name?>
			</option>
			<?php }?>
		</select>
	</div>

	<div class="clear"></div>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid',
		'dataProvider' => $data->search(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'header'=>'User',
						'value'=>'$data->user_login->user_information->first_name',
				),
				array(
						'header'=>'Booking Type',
						'value'=>'$data->request_booking_type->name',
						'htmlOptions'=>array('width'=>'14%', 'align'=>'center'),
				),
array(
		'header'=>'Equipments',
		'value'=>'CommonUtil::getEquipmentList($data->id);',
		'htmlOptions'=>array('width'=>'13%', 'align'=>'center'),
),
// 				array(
// 						'header'=>'Created Date',
// 						'value'=>'DateTimeUtil::getDateFormat($data->create_date, "dd MM yyyy");',
// 						'htmlOptions'=>array('width'=>'13%', 'align'=>'center'),
// 				),
				array(
						'header'=>'Use Date',
						'value'=>'$data->request_date == null ? $data->day_in_week->name : DateTimeUtil::getDateFormat($data->request_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'12%', 'align'=>'center'),
				),
				array(
						'header'=>'Use Time',
						'value'=>'DateTimeUtil::getTimeFormat($data->period_s->start_hour, $data->period_s->start_min)." - "'.
						'.DateTimeUtil::getTimeFormat($data->period_e->end_hour, $data->period_e->end_min)',
						'htmlOptions'=>array('width'=>'12%', 'align'=>'center'),
				),
				array(
						'header'=>'Room',
						'value'=>'$data->room->room_code',
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'header'=>'Status',
						'value'=>'$data->status->name',
						'htmlOptions'=>array('width'=>'15%', 'align'=>'center'),
				),
				// 				array(
						// 						'header'=>'อุปกรณ์',
						// 						'value'=>'$data->equipment->name',
						// 				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'template'=>'{view} {update} {file}',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'buttons'=>array
						(
								'view' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_REQUEST_BOOKING", "VIEW_ALL_REQUEST_BOOKING"))',
								),
								'update' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_REQUEST_BOOKING"))',
								),
								'file' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_REQUEST_BOOKING")) && RequestUtil::hasRequestBookingActivityFile("$data->id")',
										'imageUrl' => Yii::app()->request->baseUrl.'/images/attach.jpg',
										'url' => 'Yii::app()->request->baseUrl."/".RequestUtil::getActivityFilePath($data->id)',
								),
								// 								'delete' => array
				// 								(
// 										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "DELETE_USER"))',
// 								),
						),
				),
		),
));
}
?>
<br>


