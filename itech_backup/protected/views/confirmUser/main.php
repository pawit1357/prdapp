<div class="module-head">User</div>
<div>
	&nbsp;

	<div class="search-box">
		<?php 
		$form = $this->beginWidget('CActiveForm', array(
				'id' => 'users-form',
				'method'=>'get',
				'action'=>'',
				'enableAjaxValidation' => false,
		));
		?>
		<input type="text" name="search_text" value="<?php echo $_GET['search_text']?>">
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid',
		'dataProvider' => $data->search(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'name'=>'username',
						'htmlOptions'=>array('width'=>'15%', 'align'=>'center'),
				),
				array(
						'header' => 'Title',
						'name'=>'user_information_personal_title_search',
						'value'=>'$data->user_information->personal_title',
						'htmlOptions'=>array('width'=>'10%'),
				),
				array(
						'header' => 'First Name',
						'name'=>'user_information_first_name_search',
						'value'=>'$data->user_information->first_name',
						'htmlOptions'=>array('width'=>'15%'),
				),
				array(
						'header' => 'Last Name',
						'name'=>'user_information_last_name_search',
						'value'=>'$data->user_information->last_name',
						'htmlOptions'=>array('width'=>'15%'),
				),
				array(
						'header' => 'Email',
						'name'=>'user_information_email_search',
						'value'=>'$data->email',
				),
				array(
						'header' => 'Role',
						'name'=>'role_name_search',
						'value'=>'$data->role->name',
						'htmlOptions'=>array('width'=>'8%', 'align'=>'center'),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'template'=>'{view} {delete}',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'buttons'=>array
						(
								'view' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_USER"))',
								),
								'delete' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "DELETE_USER"))',
								),
						),
				),
		),
));
?>
