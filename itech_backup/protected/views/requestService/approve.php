<span class="module-head">Approve</span>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid',
		'dataProvider' => $data->search(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'header'=>'Service Type',
						'value'=>'RequestUtil::getAllRequestServiceTypeName($data->id)',
				),
				array(
						'header'=>'Requested Date',
						'value'=>'DateTimeUtil::getDateFormat($data->create_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'15%', 'align'=>'center'),
				),
				array(
						'header'=>'Due Date',
						'value'=>'DateTimeUtil::getDateFormat($data->due_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'25%', 'align'=>'center'),
				),
				array(
						'header'=>'Status',
						'value'=>'$data->status->name;',
						'htmlOptions'=>array('width'=>'25%', 'align'=>'center'),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'header'=>'Approve',
						'template'=>'{Approve}',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'buttons'=>array
						(
								'Approve' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_REQUEST_SERVICE"))',
										'url' => 'Yii::app()->createUrl("/RequestService/ApproveRequest/")."/id/".$data->id',
								),
						),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'header'=>'Disapprove',
						'template'=>'{Disapprove}',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'buttons'=>array
						(
								'Disapprove' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_REQUEST_SERVICE"))',
										'url' => 'Yii::app()->createUrl("/RequestService/DisapproveRequest/")."/id/".$data->id',
								),
						),
				),
		),
));
?>
<br>

