﻿<span class="module-head">Status</span>
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.js"></script>

<style>
.ui-dialog-titlebar-close {
	display: none;
}
</style>
<script type="text/javascript">

/*
var url = 'http://api.twitter.com/1/users/show.json?screen_name=nightoutinc&callback=??';
$.getJSON(url,  function(twitterAPI) {
        console.log(twitterAPI)
        var twitter = twitterAPI.followers_count;
        $('#followCounter').html(twitter);

        alert(twitter);
});
*/

$(function() {
	$( "#dialogON,#dialogOff,#dialogPing" ).dialog({
	      height: 140,
	      autoOpen: false,
	      modal: true
	    });
  });
  
function turnOnEquipment(ip) {
	$( "#dialogON" ).dialog("open");
			$.ajax({
				url : "http://" + ip + "/cgi-bin/sender.exe?KEY=3B",
				type : "GET",
				data : '',
				success : function(data) {		
					$( "#dialogON" ).dialog("close");
				},
				error : function() {
					$( "#dialogON" ).dialog("close");
				}
			});
}

function turnOffEquipment(ip) {
	$( "#dialogOff" ).dialog("open");
	$('#ip').val(ip);
	$.getJSON("http://" + ip + "/cgi-bin/sender.exe?KEY=3B",
			function(data) {

			});
}

function confirmTurnOffEquipment() {
	var ip = $("#ip").val();
	$.getJSON("http://" + ip + "/cgi-bin/sender.exe?KEY=3B",
			function(data) {

			});
	$( "#dialogOff" ).dialog("close");
}

function turnOnComputer(ip, mac) {

	$( "#dialogPing" ).dialog("open");
	$('#pResult').empty();
	$('#pResult').append("<font color='green'><strong>Wake-On-Lan command has been sent.</strong></font>");	
	$.getJSON("http://localhost:88/itech/wol.php?wake_machine="+mac+"&wake_ip="+ip,
			function(data) {		
				alert(data.result+"xxxx");		
			});	
}

function turnOffComputer(ip, mac,client_user,client_pass) {

	$( "#dialogPing" ).dialog("open");
	
	$('#pResult').empty();
	
	$.getJSON("<?php echo Yii::app()->createUrl('Management/Shutdown/ip')?>/"+ip+"/user/"+client_user+"/pass/"+client_pass,
			function(data) {

				if( data != null){					
					if( data == "1"){
						$('#pResult').append("<font color='green'><strong>Shutdown Success!</strong></font>");
					}else
					{
						$('#pResult').append("<font color='red'><strong>Shutdown Fail!</strong></font>");
					}					
				}else
				{
					$('#pResult').append("<font color='red'><strong>Shutdown Fail!</strong></font>");
				}		
			});
	
	//$( "#dialogOff" ).dialog("open");
	//$('#ip').val(ip);
	//$.getJSON("http://localhost:88/itech/shutdown.php?ip_address="+ip+"&client_user="+client_user+"&client_pass="+client_pass,
	//		function(data) {
	//		});	
}

function pingEquipment(ip) {
	$( "#dialogPing" ).dialog("open");
	
	$('#pResult').empty();
	$.getJSON("<?php echo Yii::app()->createUrl('Management/Ping/ip')?>/"+ip,
			function(data) {
		if( data != null){	
				if( data == '1'){
					$('#pResult').append("<font color='green'><strong>Online!</strong></font>");					
				}else
				{
					$('#pResult').append("<font color='red'><strong>Offline!</strong></font>");
				}		
		}else
		{
			$('#pResult').append("<font color='red'><strong>Offline!</strong></font>");
			}
			});
}

</script>

<script type="text/javascript">
function filter(){
	var data = '';
	if($('#room_filter').val() != '') {
		data = 'room_filter='+$('#room_filter').val();
	}
	if($('#equipment_type_filter').val() != '') {
		if(data != '') {
			data = data + '&';
		}
		data = data + 'equipment_type_filter='+$('#equipment_type_filter').val();
	}
	$('#my-model-grid').yiiGridView('update', {url : '<?php echo Yii::app()->createUrl('Solution/Index')?>/ajax/my-model-grid', data: data});
}
</script>
<input type="hidden"
	name="ip" id="ip" value="">


<div id="dialogON" title="Alert">
	<div id="test1"></div>
	<p>Turning Equipment ON / OFF</p>
	<p></p>
</div>

<div id="dialogOff" title="Alert">
	<p>Power Off ?</p>
	<p style="text-align: center">
		<input type="submit" value="Yes" onclick="confirmTurnOffEquipment()" />
	</p>
</div>

<div id="dialogPing" title="Alert">
	<p>Begin Sent command.</p>
	<div id="pResult"></div>

</div>

<div>
	Equipment <select name="equipment_type_filter"
		id="equipment_type_filter" onchange="filter()">
		<option value="">All Equipment</option>
		<?php 
		$equipmentTypes = EquipmentType::model()->findAll(array('condition'=>"id='8' OR id='9'"));
		foreach($equipmentTypes as $equipmentType) {
			?>
		<option value="<?php echo $equipmentType->id?>">
			<?php echo $equipmentType->name?>
		</option>
		<?php }?>
	</select> Room <select name="room_filter" id="room_filter"
		onchange="filter()">
		<option value="">All Room</option>
		<?php 
		$rooms = Room::model()->findAll();
		foreach($rooms as $room) {
			?>
		<option value="<?php echo $room->id?>">
			<?php echo $room->name?>
		</option>
		<?php }?>
	</select> &nbsp;

	<div class="search-box">
		<?php 
		$form = $this->beginWidget('CActiveForm', array(
				'id' => 'room-form',
				'method'=>'get',
				'action'=>'',
				'enableAjaxValidation' => false,
		));
		?>
		<input type="text" name="search_text"
			value="<?php echo $_GET['search_text']?>">
		<?php $this->endWidget(); ?>
	</div>
</div>


<div id="followCounter"></div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid',
		'dataProvider' => $data->search(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'header'=>'Name',
						'value'=>'$data->name',
				),
				array(
						'header'=>'Room',
						'value'=>'$data->room->name',
						'htmlOptions'=>array('width'=>'10%'),
				),
				array(
						'header'=>'Using Hour',
						'value'=>'16',
						'htmlOptions'=>array('width'=>'12%'),
				),
				array(
						'header'=>'Temperature',
						'value'=>'25',
						'htmlOptions'=>array('width'=>'10%'),
				),
				array(
						'header'=>'IP Address',
						'value'=>'$data->ip_address',
						'htmlOptions'=>array('width'=>'15%'),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'template'=>'{ON}',
						'htmlOptions'=>array('width'=>'7%', 'align'=>'center'),
						'buttons'=>array
						(
								'ON' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_USER"))',
										'url'=>'$data->equipment_type->equipment_type_code == "1008" ? "javascript:turnOnComputer(\'".$data->ip_address."\', \'".$data->mac_address."\')" : "javascript:turnOnEquipment(\'".$data->ip_address."\')"',
								),
						),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'template'=>'{OFF}',
						'htmlOptions'=>array('width'=>'7%', 'align'=>'center'),
						'buttons'=>array
						(
								'OFF' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_USER"))',
										'url'=>'$data->equipment_type->equipment_type_code == "1008" ? "javascript:turnOffComputer(\'".$data->ip_address."\', \'".$data->mac_address."\', \'".$data->client_user."\',\'".$data->client_pass."\' )" : "javascript:turnOffEquipment(\'".$data->ip_address."\')"',
								),
						),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'header'=>'Status',
						'template'=>'{ONLINE} {OFFLINE}',//{PING}
						'htmlOptions'=>array('width'=>'12%', 'align'=>'center'),
						'buttons'=>array
						(
								'PING' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_USER"))',
										'url'=>'$data->equipment_type->equipment_type_code == "1008" ? "javascript:pingEquipment(\'".$data->ip_address."\')" : "javascript:pingEquipment(\'".$data->ip_address."\')"',
								),
								'ONLINE' => array
								(
										'visible'=>'HardwareUtil::isEquipmentOnline($data->id,$data->ip_address)',
										'imageUrl' => Yii::app()->request->baseUrl.'/images/green-circle.png',
								),
								'OFFLINE' => array
								(
										'visible'=>'!HardwareUtil::isEquipmentOnline($data->id,$data->ip_address)',
										'imageUrl' => Yii::app()->request->baseUrl.'/images/red-circle.png',
								),
						),
				),
		),
));
?>
