<?php

class RequestBorrow extends CActiveRecord
{
	public $view_all_request = false;
	public $year_filter;
	public $month_filter;
	public $day_filter;
	public $status_filter;
	public $show_all_active;
	public $completed_request;
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public function __construct($scenario='insert') {
		parent::__construct($scenario);
		$this->year_filter = date("Y") * 1;
		$this->month_filter = date("m") * 1;
		$this->day_filter = date("d") * 1;
	}
	public static function model($className=__CLASS__)
	{

		return parent::model($className);
	}

	public function clearDateFilter()
	{
		$this->year_filter = '';
		$this->month_filter = '';
		$this->day_filter = '';
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'request_borrow';
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'user_login' => array(self::BELONGS_TO, 'UserLogin', 'user_login_id'),
				'event_type' => array(self::BELONGS_TO, 'EventType', 'event_type_id'),
				'status' => array(self::BELONGS_TO, 'Status', 'status_code'),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('event_type_id, location, approve_by, chef_email, description, from_date, thru_date', 'safe'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
				'description' => 'Description',
		);
	}

	/**
	 * @param Post the post that this comment belongs to. If null, the method
	 * will query for the post.
	 * @return string the permalink URL for this comment
	 */
	public function getUrl($post=null)
	{
		if($post===null)
			$post=$this->post;
		return $post->url.'#c'.$this->id;
	}

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		/*
		 if(parent::beforeSave())
		 {
		if($this->isNewRecord)
			$this->create_time=time();
		return true;
		}
		else
			return false;
		*/
		return true;
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		if(!$this->view_all_request) {
			$criteria->addCondition("t.user_login_id = '".UserLoginUtil::getUserLoginId()."'");
		}
		if(isset($this->show_all_active)){
			$criteria->addCondition("status_code != 'REQUEST_BORROW_COMPLETED' AND status_code != 'REQUEST_BORROW_CANCELLED'");
		} else {			
			if(isset($this->year_filter) && $this->year_filter != ''){
				$criteria->addCondition($this->year_filter." between YEAR(t.from_date) AND YEAR(t.thru_date)");

			}
			if(isset($this->month_filter) && $this->month_filter != ''){
				$criteria->addCondition("(".$this->year_filter."  >= YEAR(t.from_date) AND ".$this->month_filter." >= MONTH(t.from_date)) AND (".$this->year_filter."  <= YEAR(t.thru_date) AND ".$this->month_filter." <= MONTH(t.thru_date))");


			}
// 			if(isset($this->day_filter) && $this->day_filter != ''){
// 				$criteria->addCondition("'".$this->year_filter."-".$this->month_filter."-".$this->day_filter."' between from_date and thru_date");

// 			}
			if(isset($this->status_filter)){
				$criteria->addCondition("status_code='".$this->status_filter."'");
			}

			if($this->completed_request){
				$criteria->addCondition("status_code IN ('REQUEST_BORROW_CANCELLED', 'REQUEST_BORROW_COMPLETED')");
			} else {
				$criteria->addCondition("status_code NOT IN ('REQUEST_BORROW_CANCELLED', 'REQUEST_BORROW_COMPLETED')");
			}
		}


		return new CActiveDataProvider(get_class($this), array(
				'criteria' => $criteria,
				'sort' => array(
						'defaultOrder' => 't.from_date',
				),
				'pagination' => array(
						'pageSize' => ConfigUtil::getDefaultPageSize()
				),
		));
	}
}