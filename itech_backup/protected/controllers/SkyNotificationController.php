<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class SkyNotificationController extends CController
{
	public $layout='management';
	private $_model;

	/**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex()
	{
		// Permission
		if(!UserLoginUtil::hasPermission(array("FULL_ADMIN"))){
			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
		}

		$model = new SkyNotification();

		// Set Search Text
		if(isset($_GET['search_text'])){
			$model->search_text = addslashes($_GET['search_text']);
		}

		$this->render('main', array(
				'data' => $model,
		));
	}


	public function actionLoadData()
	{

		echo 'Begin process....';
	}
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id'])) {
				$id = addslashes($_GET['id']);
				$this->_model=Role::model()->findbyPk($id);
			}
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}


}