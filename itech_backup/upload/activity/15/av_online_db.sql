-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- โฮสต์: localhost
-- เวลาในการสร้าง: 
-- รุ่นของเซิร์ฟเวอร์: 5.0.51
-- รุ่นของ PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- ฐานข้อมูล: `av_online_db`
-- 

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `department`
-- 

CREATE TABLE `department` (
  `id` int(11) NOT NULL auto_increment,
  `department_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `department_code` (`department_code`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- dump ตาราง `department`
-- 

INSERT INTO `department` VALUES (1, '10', 'การเงิน และ การบัญชี', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `enumeration`
-- 

CREATE TABLE `enumeration` (
  `id` int(11) NOT NULL auto_increment,
  `enumeration_type_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `enumeration_type_id` (`enumeration_type_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- 
-- dump ตาราง `enumeration`
-- 

INSERT INTO `enumeration` VALUES (1, 'DAY_IN_WEEK', 'Sunday', '');
INSERT INTO `enumeration` VALUES (2, 'DAY_IN_WEEK', 'Monday', '');
INSERT INTO `enumeration` VALUES (3, 'DAY_IN_WEEK', 'Tuesday', '');
INSERT INTO `enumeration` VALUES (4, 'DAY_IN_WEEK', 'Wednesday', '');
INSERT INTO `enumeration` VALUES (5, 'DAY_IN_WEEK', 'Thursday', '');
INSERT INTO `enumeration` VALUES (6, 'DAY_IN_WEEK', 'Friday', '');
INSERT INTO `enumeration` VALUES (7, 'DAY_IN_WEEK', 'Saturday', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `enumeration_type`
-- 

CREATE TABLE `enumeration_type` (
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- dump ตาราง `enumeration_type`
-- 

INSERT INTO `enumeration_type` VALUES ('DAY_IN_WEEK', 'Day in week', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `equipment`
-- 

CREATE TABLE `equipment` (
  `id` int(11) NOT NULL auto_increment,
  `equipment_type_id` int(11) NOT NULL,
  `room_id` int(11) default NULL,
  `equipment_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `equipment_code` (`equipment_code`),
  KEY `equipment_type_id` (`equipment_type_id`),
  KEY `room_id` (`room_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=207 ;

-- 
-- dump ตาราง `equipment`
-- 

INSERT INTO `equipment` VALUES (1, 3, NULL, '10001', 'Slide AV1', 'Slide AV1', '192.168.1.1', 'A');
INSERT INTO `equipment` VALUES (2, 3, NULL, '10002', 'Slide AV2', 'Slide AV2', '192.168.1.2', 'A');
INSERT INTO `equipment` VALUES (3, 3, NULL, '10003', 'Slide AV3', 'Slide AV3', '192.168.1.3', 'A');
INSERT INTO `equipment` VALUES (4, 3, NULL, '10004', 'Slide AV4', 'Slide AV4', '192.168.1.4', 'A');
INSERT INTO `equipment` VALUES (5, 1, 1502, '10005', 'LCD AV1', 'LCD AV1', '192.168.1.5', 'A');
INSERT INTO `equipment` VALUES (6, 1, 1503, '10006', 'LCD AV2', 'LCD AV2', '192.168.1.6', 'A');
INSERT INTO `equipment` VALUES (7, 1, 1504, '10007', 'LCD AV3', 'LCD AV3', '192.168.1.7', 'A');
INSERT INTO `equipment` VALUES (8, 1, 1505, '10008', 'LCD AV4', 'LCD AV4', '192.168.1.8', 'A');
INSERT INTO `equipment` VALUES (9, 1, 1515, '10009', 'LCD AV5', 'LCD AV5', '192.168.1.9', 'A');
INSERT INTO `equipment` VALUES (10, 1, 1516, '10010', 'LCD AV6', 'LCD AV6', '192.168.1.10', 'A');
INSERT INTO `equipment` VALUES (11, 1, NULL, '10011', 'LCD AV7', 'LCD AV7', '192.168.1.11', 'A');
INSERT INTO `equipment` VALUES (12, 1, NULL, '10012', 'LCD AV8', 'LCD AV8', '192.168.1.12', 'A');
INSERT INTO `equipment` VALUES (13, 1, 1402, '10013', 'LCD AV10', 'LCD AV10', '192.168.1.13', 'A');
INSERT INTO `equipment` VALUES (14, 1, 1210, '10014', 'LCD AV11', 'LCD AV11', '192.168.1.14', 'A');
INSERT INTO `equipment` VALUES (15, 1, 1318, '10015', 'LCD AV12', 'LCD AV12', '192.168.1.15', 'A');
INSERT INTO `equipment` VALUES (16, 1, 1318, '10016', 'LCD AV13', 'LCD AV13', '192.168.1.16', 'A');
INSERT INTO `equipment` VALUES (17, 1, 1318, '10017', 'LCD AV14', 'LCD AV14', '192.168.1.17', 'A');
INSERT INTO `equipment` VALUES (18, 4, NULL, '10018', 'Tape AV1', 'Tape AV1', '192.168.1.18', 'A');
INSERT INTO `equipment` VALUES (19, 4, NULL, '10019', 'Tape  AV2', 'Tape  AV2', '192.168.1.19', 'A');
INSERT INTO `equipment` VALUES (20, 4, NULL, '10020', 'Tape  AV3', 'Tape  AV3', '192.168.1.20', 'A');
INSERT INTO `equipment` VALUES (21, 4, NULL, '10021', 'Tape  AV4', 'Tape  AV4', '192.168.1.21', 'A');
INSERT INTO `equipment` VALUES (22, 4, NULL, '10022', 'Tape  AV5', 'Tape  AV5', '192.168.1.22', 'A');
INSERT INTO `equipment` VALUES (23, 4, NULL, '10023', 'Tape  AV6', 'Tape  AV6', '192.168.1.23', 'A');
INSERT INTO `equipment` VALUES (24, 4, NULL, '10024', 'Tape  AV7', 'Tape  AV7', '192.168.1.24', 'A');
INSERT INTO `equipment` VALUES (25, 4, NULL, '10025', 'Tape  AV8', 'Tape  AV8', '192.168.1.25', 'A');
INSERT INTO `equipment` VALUES (26, 5, NULL, '10026', 'DVD AV1', 'DVD AV1', '192.168.1.26', 'A');
INSERT INTO `equipment` VALUES (27, 5, NULL, '10027', 'DVD AV2', 'DVD AV2', '192.168.1.27', 'A');
INSERT INTO `equipment` VALUES (28, 5, NULL, '10028', 'DVD AV3', 'DVD AV3', '192.168.1.28', 'A');
INSERT INTO `equipment` VALUES (29, 5, 1318, '10029', 'DVD AV4', 'DVD AV4', '192.168.1.29', 'A');
INSERT INTO `equipment` VALUES (30, 6, NULL, '10030', 'TV AV1', 'TV AV1', '192.168.1.30', 'A');
INSERT INTO `equipment` VALUES (31, 6, NULL, '10031', 'TV AV2', 'TV AV2', '192.168.1.31', 'A');
INSERT INTO `equipment` VALUES (32, 6, NULL, '10032', 'TV AV3', 'TV AV3', '192.168.1.32', 'A');
INSERT INTO `equipment` VALUES (33, 6, NULL, '10033', 'TV AV4', 'TV AV4', '192.168.1.33', 'A');
INSERT INTO `equipment` VALUES (34, 6, NULL, '10034', 'TV AV5', 'TV AV5', '192.168.1.34', 'A');
INSERT INTO `equipment` VALUES (35, 6, NULL, '10035', 'TV AV6', 'TV AV6', '192.168.1.35', 'A');
INSERT INTO `equipment` VALUES (36, 6, NULL, '10036', 'TV AV7', 'TV AV7', '192.168.1.36', 'A');
INSERT INTO `equipment` VALUES (37, 6, NULL, '10037', 'TV AV8', 'TV AV8', '192.168.1.37', 'A');
INSERT INTO `equipment` VALUES (38, 7, NULL, '10038', 'VDO AV1', 'VDO AV1', '192.168.1.38', 'A');
INSERT INTO `equipment` VALUES (39, 7, NULL, '10039', 'VDO AV2', 'VDO AV2', '192.168.1.39', 'A');
INSERT INTO `equipment` VALUES (40, 7, NULL, '10040', 'VDO AV3', 'VDO AV3', '192.168.1.40', 'A');
INSERT INTO `equipment` VALUES (41, 7, NULL, '10041', 'VDO AV4', 'VDO AV4', '192.168.1.41', 'A');
INSERT INTO `equipment` VALUES (42, 7, NULL, '10042', 'VDO AV5', 'VDO AV5', '192.168.1.42', 'A');
INSERT INTO `equipment` VALUES (43, 7, NULL, '10043', 'VDO AV6', 'VDO AV6', '192.168.1.43', 'A');
INSERT INTO `equipment` VALUES (44, 7, NULL, '10044', 'VDO AV7', 'VDO AV7', '192.168.1.44', 'A');
INSERT INTO `equipment` VALUES (45, 7, NULL, '10045', 'VDO AV8', 'VDO AV8', '192.168.1.45', 'A');
INSERT INTO `equipment` VALUES (46, 7, 1502, '10046', 'VDO AV9', 'VDO AV9', '192.168.1.46', 'A');
INSERT INTO `equipment` VALUES (47, 7, 1503, '10047', 'VDO AV10', 'VDO AV10', '192.168.1.47', 'A');
INSERT INTO `equipment` VALUES (48, 7, 1504, '10048', 'VDO AV11', 'VDO AV11', '192.168.1.48', 'A');
INSERT INTO `equipment` VALUES (49, 7, 1505, '10049', 'VDO AV12', 'VDO AV12', '192.168.1.49', 'A');
INSERT INTO `equipment` VALUES (50, 7, 1515, '10050', 'VDO AV13', 'VDO AV13', '192.168.1.50', 'A');
INSERT INTO `equipment` VALUES (51, 7, 1516, '10051', 'VDO AV14', 'VDO AV14', '192.168.1.51', 'A');
INSERT INTO `equipment` VALUES (52, 2, 1418, '10052', 'Mic. AV1', 'Mic. AV1', '192.168.1.52', 'A');
INSERT INTO `equipment` VALUES (53, 2, 1302, '10053', 'Mic.  AV3', 'Mic.  AV3', '192.168.1.53', 'A');
INSERT INTO `equipment` VALUES (54, 2, 1303, '10054', 'Mic.  AV4', 'Mic.  AV4', '192.168.1.54', 'A');
INSERT INTO `equipment` VALUES (55, 2, 1304, '10055', 'Mic.  AV5', 'Mic.  AV5', '192.168.1.55', 'A');
INSERT INTO `equipment` VALUES (56, 2, 1305, '10056', 'Mic.  AV6', 'Mic.  AV6', '192.168.1.56', 'A');
INSERT INTO `equipment` VALUES (57, 2, 1306, '10057', 'Mic.  AV7', 'Mic.  AV7', '192.168.1.57', 'A');
INSERT INTO `equipment` VALUES (58, 2, 1314, '10058', 'Mic.  AV8', 'Mic.  AV8', '192.168.1.58', 'A');
INSERT INTO `equipment` VALUES (59, 2, 1315, '10059', 'Mic.  AV9', 'Mic.  AV9', '192.168.1.59', 'A');
INSERT INTO `equipment` VALUES (60, 2, 1417, '10060', 'Mic.  AV10', 'Mic.  AV10', '192.168.1.60', 'A');
INSERT INTO `equipment` VALUES (61, 2, 1402, '10061', 'Mic.  AV11', 'Mic.  AV11', '192.168.1.61', 'A');
INSERT INTO `equipment` VALUES (62, 2, 1403, '10062', 'Mic.  AV12', 'Mic.  AV12', '192.168.1.62', 'A');
INSERT INTO `equipment` VALUES (63, 2, 1404, '10063', 'Mic.  AV13', 'Mic.  AV13', '192.168.1.63', 'A');
INSERT INTO `equipment` VALUES (64, 2, 1405, '10064', 'Mic.  AV14', 'Mic.  AV14', '192.168.1.64', 'A');
INSERT INTO `equipment` VALUES (65, 2, 1406, '10065', 'Mic.  AV15', 'Mic.  AV15', '192.168.1.65', 'A');
INSERT INTO `equipment` VALUES (66, 2, 1407, '10066', 'Mic.  AV16', 'Mic.  AV16', '192.168.1.66', 'A');
INSERT INTO `equipment` VALUES (67, 2, 1408, '10067', 'Mic.  AV17', 'Mic.  AV17', '192.168.1.67', 'A');
INSERT INTO `equipment` VALUES (68, 2, 1502, '10068', 'Mic.  AV18', 'Mic.  AV18', '192.168.1.68', 'A');
INSERT INTO `equipment` VALUES (69, 2, 1503, '10069', 'Mic.  AV19', 'Mic.  AV19', '192.168.1.69', 'A');
INSERT INTO `equipment` VALUES (70, 2, 1504, '10070', 'Mic.  AV20', 'Mic.  AV20', '192.168.1.70', 'A');
INSERT INTO `equipment` VALUES (71, 2, 1505, '10071', 'Mic.  AV21', 'Mic.  AV21', '192.168.1.71', 'A');
INSERT INTO `equipment` VALUES (72, 2, 1515, '10072', 'Mic.  AV22', 'Mic.  AV22', '192.168.1.72', 'A');
INSERT INTO `equipment` VALUES (73, 2, 1516, '10073', 'Mic.  AV23', 'Mic.  AV23', '192.168.1.73', 'A');
INSERT INTO `equipment` VALUES (74, 8, NULL, '10074', 'Visual. AV01', 'Visual. AV01', '192.168.1.74', 'A');
INSERT INTO `equipment` VALUES (75, 8, NULL, '10075', 'Visual.  AV02', 'Visual.  AV02', '192.168.1.75', 'A');
INSERT INTO `equipment` VALUES (76, 8, NULL, '10076', 'Visual.  AV03', 'Visual.  AV03', '192.168.1.76', 'A');
INSERT INTO `equipment` VALUES (77, 9, 1515, '10077', 'Com. AV1', 'Com. AV1', '192.168.1.77', 'A');
INSERT INTO `equipment` VALUES (78, 9, 1516, '10078', 'Com. AV2', 'Com. AV2', '192.168.1.78', 'A');
INSERT INTO `equipment` VALUES (79, 9, 1502, '10079', 'Com. AV3', 'Com. AV3', '192.168.1.79', 'A');
INSERT INTO `equipment` VALUES (80, 9, 1503, '10080', 'Com. AV4', 'Com. AV4', '192.168.1.80', 'A');
INSERT INTO `equipment` VALUES (81, 1, 1403, '10081', 'LCD AV15', 'LCD AV15', '192.168.1.81', 'A');
INSERT INTO `equipment` VALUES (82, 1, 1404, '10082', 'LCD AV16', 'LCD AV16', '192.168.1.82', 'A');
INSERT INTO `equipment` VALUES (83, 1, 1405, '10083', 'LCD AV17', 'LCD AV17', '192.168.1.83', 'A');
INSERT INTO `equipment` VALUES (84, 1, 1406, '10084', 'LCD AV18', 'LCD AV18', '192.168.1.84', 'A');
INSERT INTO `equipment` VALUES (85, 1, 1407, '10085', 'LCD AV19', 'LCD AV19', '192.168.1.85', 'A');
INSERT INTO `equipment` VALUES (86, 1, 1417, '10086', 'LCD AV20', 'LCD AV20', '192.168.1.86', 'A');
INSERT INTO `equipment` VALUES (87, 1, 1418, '10087', 'LCD AV21', 'LCD AV21', '192.168.1.87', 'A');
INSERT INTO `equipment` VALUES (88, 1, 1419, '10088', 'LCD AV22', 'LCD AV22', '192.168.1.88', 'A');
INSERT INTO `equipment` VALUES (89, 5, NULL, '10089', 'DVD AV5', 'DVD AV5', '192.168.1.89', 'A');
INSERT INTO `equipment` VALUES (90, 5, NULL, '10090', 'DVD AV6', 'DVD AV6', '192.168.1.90', 'A');
INSERT INTO `equipment` VALUES (91, 5, NULL, '10091', 'DVD AV7', 'DVD AV7', '192.168.1.91', 'A');
INSERT INTO `equipment` VALUES (92, 9, 1402, '10092', 'Com. AV5', 'Com. AV5', '192.168.1.92', 'A');
INSERT INTO `equipment` VALUES (93, 9, 1403, '10093', 'Com. AV6', 'Com. AV6', '192.168.1.93', 'A');
INSERT INTO `equipment` VALUES (94, 9, 1404, '10094', 'Com. AV7', 'Com. AV7', '192.168.1.94', 'A');
INSERT INTO `equipment` VALUES (95, 9, 1405, '10095', 'Com. AV8', 'Com. AV8', '192.168.1.95', 'A');
INSERT INTO `equipment` VALUES (96, 9, 1406, '10096', 'Com. AV9', 'Com. AV9', '192.168.1.96', 'A');
INSERT INTO `equipment` VALUES (97, 9, 1407, '10097', 'Com. AV10', 'Com. AV10', '192.168.1.97', 'A');
INSERT INTO `equipment` VALUES (98, 9, 1417, '10098', 'Com. AV11', 'Com. AV11', '192.168.1.98', 'A');
INSERT INTO `equipment` VALUES (99, 9, 1418, '10099', 'Com. AV12', 'Com. AV12', '192.168.1.99', 'A');
INSERT INTO `equipment` VALUES (100, 9, 1419, '10100', 'Com. AV13', 'Com. AV13', '192.168.1.100', 'A');
INSERT INTO `equipment` VALUES (101, 9, 1504, '10101', 'Com. AV14', 'Com. AV14', '192.168.1.101', 'A');
INSERT INTO `equipment` VALUES (102, 9, 1505, '10102', 'Com. AV15', 'Com. AV15', '192.168.1.102', 'A');
INSERT INTO `equipment` VALUES (103, 1, 1302, '10103', 'LCD AV23', 'LCD AV23', '192.168.1.103', 'A');
INSERT INTO `equipment` VALUES (104, 1, 1303, '10104', 'LCD AV24', 'LCD AV24', '192.168.1.104', 'A');
INSERT INTO `equipment` VALUES (105, 1, 1304, '10105', 'LCD AV25', 'LCD AV25', '192.168.1.105', 'A');
INSERT INTO `equipment` VALUES (106, 1, 1305, '10106', 'LCD AV26', 'LCD AV26', '192.168.1.106', 'A');
INSERT INTO `equipment` VALUES (107, 1, 1306, '10107', 'LCD AV27', 'LCD AV27', '192.168.1.107', 'A');
INSERT INTO `equipment` VALUES (108, 1, 1314, '10108', 'LCD AV28', 'LCD AV28', '192.168.1.108', 'A');
INSERT INTO `equipment` VALUES (109, 1, 1315, '10109', 'LCD AV29', 'LCD AV29', '192.168.1.109', 'A');
INSERT INTO `equipment` VALUES (110, 9, 1302, '10110', 'Com. AV16', 'Com. AV16', '192.168.1.110', 'A');
INSERT INTO `equipment` VALUES (111, 9, 1303, '10111', 'Com. AV17', 'Com. AV17', '192.168.1.111', 'A');
INSERT INTO `equipment` VALUES (112, 9, 1304, '10112', 'Com. AV18', 'Com. AV18', '192.168.1.112', 'A');
INSERT INTO `equipment` VALUES (113, 9, 1305, '10113', 'Com. AV19', 'Com. AV19', '192.168.1.113', 'A');
INSERT INTO `equipment` VALUES (114, 9, 1306, '10114', 'Com. AV20', 'Com. AV20', '192.168.1.114', 'A');
INSERT INTO `equipment` VALUES (115, 9, 1314, '10115', 'Com. AV21', 'Com. AV21', '192.168.1.115', 'A');
INSERT INTO `equipment` VALUES (116, 9, 1315, '10116', 'Com. AV22', 'Com. AV22', '192.168.1.116', 'A');
INSERT INTO `equipment` VALUES (117, 7, 2302, '10117', 'VDO AV15', 'VDO AV15', '192.168.1.117', 'A');
INSERT INTO `equipment` VALUES (118, 7, 2303, '10118', 'VDO AV16', 'VDO AV16', '192.168.1.118', 'A');
INSERT INTO `equipment` VALUES (119, 7, 2306, '10119', 'VDO AV17', 'VDO AV17', '192.168.1.119', 'A');
INSERT INTO `equipment` VALUES (120, 7, 2307, '10120', 'VDO AV18', 'VDO AV18', '192.168.1.120', 'A');
INSERT INTO `equipment` VALUES (121, 7, 2308, '10121', 'VDO AV19', 'VDO AV19', '192.168.1.121', 'A');
INSERT INTO `equipment` VALUES (122, 6, 2302, '10122', 'TV AV9', 'TV AV9', '192.168.1.122', 'A');
INSERT INTO `equipment` VALUES (123, 6, 2303, '10123', 'TV AV10', 'TV AV10', '192.168.1.123', 'A');
INSERT INTO `equipment` VALUES (124, 6, 2306, '10124', 'TV AV11', 'TV AV11', '192.168.1.124', 'A');
INSERT INTO `equipment` VALUES (125, 6, 2307, '10125', 'TV AV12', 'TV AV12', '192.168.1.125', 'A');
INSERT INTO `equipment` VALUES (126, 6, 2308, '10126', 'TV AV13', 'TV AV13', '192.168.1.126', 'A');
INSERT INTO `equipment` VALUES (127, 1, 2303, '10127', 'LCD AV30', 'LCD AV30', '192.168.1.127', 'A');
INSERT INTO `equipment` VALUES (128, 1, 2306, '10128', 'LCD AV31', 'LCD AV31', '192.168.1.128', 'A');
INSERT INTO `equipment` VALUES (129, 1, 2307, '10129', 'LCD AV32', 'LCD AV32', '192.168.1.129', 'A');
INSERT INTO `equipment` VALUES (130, 1, 2308, '10130', 'LCD AV33', 'LCD AV33', '192.168.1.130', 'A');
INSERT INTO `equipment` VALUES (131, 2, 2302, '10131', 'Mic. AV23', 'Mic. AV23', '192.168.1.131', 'A');
INSERT INTO `equipment` VALUES (132, 2, 2303, '10132', 'Mic. AV24', 'Mic. AV24', '192.168.1.132', 'A');
INSERT INTO `equipment` VALUES (133, 2, 2306, '10133', 'Mic. AV25', 'Mic. AV25', '192.168.1.133', 'A');
INSERT INTO `equipment` VALUES (134, 2, 2307, '10134', 'Mic. AV26', 'Mic. AV26', '192.168.1.134', 'A');
INSERT INTO `equipment` VALUES (135, 2, 2308, '10135', 'Mic. AV27', 'Mic. AV27', '192.168.1.135', 'A');
INSERT INTO `equipment` VALUES (136, 9, 2302, '10136', 'Com. AV23', 'Com. AV23', '192.168.1.136', 'A');
INSERT INTO `equipment` VALUES (137, 9, 2303, '10137', 'Com. AV24', 'Com. AV24', '192.168.1.137', 'A');
INSERT INTO `equipment` VALUES (138, 9, 2306, '10138', 'Com. AV25', 'Com. AV25', '192.168.1.138', 'A');
INSERT INTO `equipment` VALUES (139, 9, 2307, '10139', 'Com. AV26', 'Com. AV26', '192.168.1.139', 'A');
INSERT INTO `equipment` VALUES (140, 9, 2308, '10140', 'Com. AV27', 'Com. AV27', '192.168.1.140', 'A');
INSERT INTO `equipment` VALUES (141, 1, 2302, '10141', 'LCD AV34', 'LCD AV34', '192.168.1.141', 'A');
INSERT INTO `equipment` VALUES (142, 9, NULL, '10142', 'Laptop AV2', 'Laptop AV2', '192.168.1.142', 'A');
INSERT INTO `equipment` VALUES (143, 1, NULL, '10143', 'LCD AV35', 'LCD AV35', '192.168.1.143', 'A');
INSERT INTO `equipment` VALUES (144, 1, NULL, '10144', 'LCD AV36', 'LCD AV36', '192.168.1.144', 'A');
INSERT INTO `equipment` VALUES (145, 1, NULL, '10145', 'LCD AV37', 'LCD AV37', '192.168.1.145', 'A');
INSERT INTO `equipment` VALUES (146, 1, NULL, '10146', 'LCD AV38', 'LCD AV38', '192.168.1.146', 'A');
INSERT INTO `equipment` VALUES (147, 1, NULL, '10147', 'LCD AV39', 'LCD AV39', '192.168.1.147', 'A');
INSERT INTO `equipment` VALUES (148, 1, NULL, '10148', 'LCD AV40', 'LCD AV40', '192.168.1.148', 'A');
INSERT INTO `equipment` VALUES (149, 1, NULL, '10149', 'LCD AV41', 'LCD AV41', '192.168.1.149', 'A');
INSERT INTO `equipment` VALUES (150, 1, NULL, '10150', 'LCD AV42', 'LCD AV42', '192.168.1.150', 'A');
INSERT INTO `equipment` VALUES (151, 9, NULL, '10151', 'Laptop AV3', 'Laptop AV3', '192.168.1.151', 'A');
INSERT INTO `equipment` VALUES (152, 9, NULL, '10152', 'Laptop AV4', 'Laptop AV4', '192.168.1.152', 'A');
INSERT INTO `equipment` VALUES (153, 9, NULL, '10153', 'Laptop AV5', 'Laptop AV5', '192.168.1.153', 'A');
INSERT INTO `equipment` VALUES (154, 9, NULL, '10154', 'Laptop AV6', 'Laptop AV6', '192.168.1.154', 'A');
INSERT INTO `equipment` VALUES (155, 9, NULL, '10155', 'Laptop AV7', 'Laptop AV7', '192.168.1.155', 'A');
INSERT INTO `equipment` VALUES (156, 9, NULL, '10156', 'Laptop AV8', 'Laptop AV8', '192.168.1.156', 'A');
INSERT INTO `equipment` VALUES (157, 9, NULL, '10157', 'Laptop AV9', 'Laptop AV9', '192.168.1.157', 'A');
INSERT INTO `equipment` VALUES (158, 2, 1419, '10158', 'Mic. AV28', 'Mic. AV28', '192.168.1.158', 'A');
INSERT INTO `equipment` VALUES (159, 2, 1210, '10159', 'Mic. AV 77', 'Mic. AV 77', '192.168.1.159', 'A');
INSERT INTO `equipment` VALUES (160, 8, NULL, '10160', 'Visual. AV04', 'Visual. AV04', '192.168.1.160', 'A');
INSERT INTO `equipment` VALUES (161, 8, NULL, '10161', 'Visual. AV05', 'Visual. AV05', '192.168.1.161', 'A');
INSERT INTO `equipment` VALUES (162, 1, 3302, '10162', 'LCD AV302', 'LCD AV302', '192.168.1.162', 'A');
INSERT INTO `equipment` VALUES (163, 1, 3303, '10163', 'LCD AV303', 'LCD AV303', '192.168.1.163', 'A');
INSERT INTO `equipment` VALUES (164, 1, 3304, '10164', 'LCD AV304', 'LCD AV304', '192.168.1.164', 'A');
INSERT INTO `equipment` VALUES (165, 1, 3305, '10165', 'LCD AV305', 'LCD AV305', '192.168.1.165', 'A');
INSERT INTO `equipment` VALUES (166, 1, 3306, '10166', 'LCD AV306', 'LCD AV306', '192.168.1.166', 'A');
INSERT INTO `equipment` VALUES (167, 1, 3315, '10167', 'LCD AV315', 'LCD AV315', '192.168.1.167', 'A');
INSERT INTO `equipment` VALUES (168, 1, 3316, '10168', 'LCD AV316', 'LCD AV316', '192.168.1.168', 'A');
INSERT INTO `equipment` VALUES (169, 1, 3317, '10169', 'LCD AV317', 'LCD AV317', '192.168.1.169', 'A');
INSERT INTO `equipment` VALUES (170, 1, 3409, '10170', 'LCD AV409', 'LCD AV409', '192.168.1.170', 'A');
INSERT INTO `equipment` VALUES (171, 1, 3410, '10171', 'LCD AV410', 'LCD AV410', '192.168.1.171', 'A');
INSERT INTO `equipment` VALUES (172, 1, 3411, '10172', 'LCD AV411', 'LCD AV411', '192.168.1.172', 'A');
INSERT INTO `equipment` VALUES (173, 1, 3412, '10173', 'LCD AV412', 'LCD AV412', '192.168.1.173', 'A');
INSERT INTO `equipment` VALUES (174, 1, 3414, '10174', 'LCD AV414', 'LCD AV414', '192.168.1.174', 'A');
INSERT INTO `equipment` VALUES (175, 1, 3415, '10175', 'LCD AV415', 'LCD AV415', '192.168.1.175', 'A');
INSERT INTO `equipment` VALUES (176, 1, 3420, '10176', 'LCD AV420', 'LCD AV420', '192.168.1.176', 'A');
INSERT INTO `equipment` VALUES (177, 1, 3421, '10177', 'LCD AV421', 'LCD AV421', '192.168.1.177', 'A');
INSERT INTO `equipment` VALUES (178, 1, 3422, '10178', 'LCD AV422', 'LCD AV422', '192.168.1.178', 'A');
INSERT INTO `equipment` VALUES (179, 9, 3302, '10179', 'Com. AV302', 'Com. AV302', '192.168.1.179', 'A');
INSERT INTO `equipment` VALUES (180, 9, 3303, '10180', 'Com. AV303', 'Com. AV303', '192.168.1.180', 'A');
INSERT INTO `equipment` VALUES (181, 9, 3304, '10181', 'Com. AV304', 'Com. AV304', '192.168.1.181', 'A');
INSERT INTO `equipment` VALUES (182, 9, 3305, '10182', 'Com. AV305', 'Com. AV305', '192.168.1.182', 'A');
INSERT INTO `equipment` VALUES (183, 9, 3306, '10183', 'Com. AV306', 'Com. AV306', '192.168.1.183', 'A');
INSERT INTO `equipment` VALUES (184, 9, 3316, '10184', 'Com. AV316', 'Com. AV316', '192.168.1.184', 'A');
INSERT INTO `equipment` VALUES (185, 9, 3317, '10185', 'Com. AV317', 'Com. AV317', '192.168.1.185', 'A');
INSERT INTO `equipment` VALUES (186, 9, 3409, '10186', 'Com. AV409', 'Com. AV409', '192.168.1.186', 'A');
INSERT INTO `equipment` VALUES (187, 9, 3410, '10187', 'Com. AV410', 'Com. AV410', '192.168.1.187', 'A');
INSERT INTO `equipment` VALUES (188, 9, 3411, '10188', 'Com. AV411', 'Com. AV411', '192.168.1.188', 'A');
INSERT INTO `equipment` VALUES (189, 9, 3412, '10189', 'Com. AV412', 'Com. AV412', '192.168.1.189', 'A');
INSERT INTO `equipment` VALUES (190, 9, 3414, '10190', 'Com. AV414', 'Com. AV414', '192.168.1.190', 'A');
INSERT INTO `equipment` VALUES (191, 9, 3415, '10191', 'Com. AV415', 'Com. AV415', '192.168.1.191', 'A');
INSERT INTO `equipment` VALUES (192, 9, 3420, '10192', 'Com. AV420', 'Com. AV420', '192.168.1.192', 'A');
INSERT INTO `equipment` VALUES (193, 9, 3421, '10193', 'Com. AV421', 'Com. AV421', '192.168.1.193', 'A');
INSERT INTO `equipment` VALUES (194, 9, 3422, '10194', 'Com. AV422', 'Com. AV422', '192.168.1.194', 'A');
INSERT INTO `equipment` VALUES (195, 9, 3315, '10195', 'Com. AV504', 'Com. AV504', '192.168.1.195', 'A');
INSERT INTO `equipment` VALUES (196, 2, 3411, '10196', 'Mic.  AV411', 'Mic.  AV411', '192.168.1.196', 'A');
INSERT INTO `equipment` VALUES (197, 2, 3412, '10197', 'Mic.  AV412', 'Mic.  AV412', '192.168.1.197', 'A');
INSERT INTO `equipment` VALUES (198, 2, 3414, '10198', 'Mic.  AV414', 'Mic.  AV414', '192.168.1.198', 'A');
INSERT INTO `equipment` VALUES (199, 2, 3415, '10199', 'Mic.  AV415', 'Mic.  AV415', '192.168.1.199', 'A');
INSERT INTO `equipment` VALUES (200, 2, 3420, '10200', 'Mic.  AV420', 'Mic.  AV420', '192.168.1.200', 'A');
INSERT INTO `equipment` VALUES (201, 2, 3421, '10201', 'Mic.  AV421', 'Mic.  AV421', '192.168.1.201', 'A');
INSERT INTO `equipment` VALUES (202, 2, 3422, '10202', 'Mic.  AV422', 'Mic.  AV422', '192.168.1.202', 'A');
INSERT INTO `equipment` VALUES (203, 1, 1307, '10203', 'LCD AV307', 'LCD AV307', '192.168.1.203', 'A');
INSERT INTO `equipment` VALUES (204, 9, 1307, '10204', 'Com. AV28', 'Com. AV28', '192.168.1.204', 'A');
INSERT INTO `equipment` VALUES (205, 1, 1308, '10205', 'LCD AV308', 'LCD AV308', '192.168.1.205', 'A');
INSERT INTO `equipment` VALUES (206, 2, NULL, '10206', 'Portable Sound PA.', 'Portable Sound PA.', '192.168.1.206', 'A');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `equipment_type`
-- 

CREATE TABLE `equipment_type` (
  `id` int(11) NOT NULL auto_increment,
  `equipment_type_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `equipment_type_code` (`equipment_type_code`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- 
-- dump ตาราง `equipment_type`
-- 

INSERT INTO `equipment_type` VALUES (1, '1001', 'Lcd', 'Lcd');
INSERT INTO `equipment_type` VALUES (2, '1002', 'Microphone', 'Microphone');
INSERT INTO `equipment_type` VALUES (3, '1003', 'Slide', 'Slide');
INSERT INTO `equipment_type` VALUES (4, '1004', 'Tape', 'Tape');
INSERT INTO `equipment_type` VALUES (5, '1005', 'DVD', 'DVD');
INSERT INTO `equipment_type` VALUES (6, '1006', 'TV', 'TV');
INSERT INTO `equipment_type` VALUES (7, '1007', 'VDO', 'VDO');
INSERT INTO `equipment_type` VALUES (8, '1008', 'Visualizer', 'Visualizer');
INSERT INTO `equipment_type` VALUES (9, '1009', 'Computer', 'Computer');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `event_type`
-- 

CREATE TABLE `event_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- 
-- dump ตาราง `event_type`
-- 

INSERT INTO `event_type` VALUES (1, 'Meeting', '');
INSERT INTO `event_type` VALUES (2, 'Seminar', '');
INSERT INTO `event_type` VALUES (3, 'Teaching', '');
INSERT INTO `event_type` VALUES (4, 'Visitting', '');
INSERT INTO `event_type` VALUES (5, 'Exhibition', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `gallery`
-- 

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `gallery`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `news`
-- 

CREATE TABLE `news` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `pic` varchar(255) default NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- 
-- dump ตาราง `news`
-- 

INSERT INTO `news` VALUES (7, '2nd inter cup', 'MUIC played host to the 2nd International Universities Cup, also known as the Inter Cup, from January 28 to February 4, 2013. The College’s football team placed third, after competing with teams from Thamassat University', 'MUIC played host to the 2nd International Universities Cup, also known as the Inter Cup, from January 28 to February 4, 2013. The College’s football team placed third, after competing with teams from Thamassat University ', 'upload/news/7/news2.gif', '1', '2013-04-28 20:08:01');
INSERT INTO `news` VALUES (9, 'Art Exhibition', 'The MUIC Art Club is currently sponsoring an exhibition on the ground floor of Building 1. The exhibition, which runs until February 13, consists of two features: artwork by members of the club and paintings from the general student  test. ', 'The MUIC Art Club is currently sponsoring an exhibition on the ground floor of Building 1. The exhibition, which runs until February 13, consists of two features: artwork by members of the club and paintings from the general student  test. ', 'upload/news/9/news1.gif', '1', '2013-04-28 20:09:41');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `period`
-- 

CREATE TABLE `period` (
  `id` int(11) NOT NULL auto_increment,
  `preriod_group_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `start_hour` int(11) NOT NULL,
  `start_min` int(11) NOT NULL,
  `end_hour` int(11) NOT NULL,
  `end_min` int(11) NOT NULL,
  `status_code` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `status_code` (`status_code`),
  KEY `preriod_group_id` (`preriod_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- 
-- dump ตาราง `period`
-- 

INSERT INTO `period` VALUES (1, 1, '08.00 - 09.50', '', 8, 0, 9, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (2, 1, '10.00 - 11.50', '', 10, 0, 11, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (3, 1, '12.00 - 13.50', '', 12, 0, 13, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (4, 1, '14.00 - 15.50', '', 14, 0, 15, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (5, 1, '16.00 - 17.50', '', 16, 0, 17, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (6, 1, '18.00 - 19.50', '', 18, 0, 19, 50, 'PERIOD_ACTIVE');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `period_group`
-- 

CREATE TABLE `period_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status_code` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `status_code` (`status_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- dump ตาราง `period_group`
-- 

INSERT INTO `period_group` VALUES (1, 'Default Period', 'Default period is make each period for 30 minutes. Start from 8.00 - 22.00', 'PERIOD_GROUP_ACTIVE');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `permission`
-- 

CREATE TABLE `permission` (
  `permission_code` varchar(255) NOT NULL,
  `permission_group_id` int(11) default NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`permission_code`),
  UNIQUE KEY `name` (`name`),
  KEY `permission_group_id` (`permission_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- dump ตาราง `permission`
-- 

INSERT INTO `permission` VALUES ('CHECK_STATUS_REQUEST_BOOKING', 3, 'Check Status Booking', '');
INSERT INTO `permission` VALUES ('CHECK_STATUS_REQUEST_BORROW', 4, 'Check Status Borrow', '');
INSERT INTO `permission` VALUES ('CONFIRM_USER', 1, 'Confirm User', '');
INSERT INTO `permission` VALUES ('CREATE_DEPARTMENT', 8, 'Create Department', '');
INSERT INTO `permission` VALUES ('CREATE_EQUIPMENT', 7, 'Create Equipment', '');
INSERT INTO `permission` VALUES ('CREATE_EQUIPMENT_TYPE', 6, 'Create Equipment Type', '');
INSERT INTO `permission` VALUES ('CREATE_POSITION', 10, 'Create Position', '');
INSERT INTO `permission` VALUES ('CREATE_PRESEN_TYPE', 11, 'Create Present Type', '');
INSERT INTO `permission` VALUES ('CREATE_REQUEST_BOOKING', 3, 'Create Request Booking', '');
INSERT INTO `permission` VALUES ('CREATE_REQUEST_BORROW', 4, 'Create Request Borrow', '');
INSERT INTO `permission` VALUES ('CREATE_REQUEST_SERVICE', 5, 'Create Request Service', '');
INSERT INTO `permission` VALUES ('CREATE_ROLE', 2, 'Create Role', '');
INSERT INTO `permission` VALUES ('CREATE_ROOM', 9, 'Create Room', '');
INSERT INTO `permission` VALUES ('CREATE_SEMESTER', 14, 'Create Semester', '');
INSERT INTO `permission` VALUES ('CREATE_SERVICE_TYPE', 12, 'Create Service Type', '');
INSERT INTO `permission` VALUES ('CREATE_SERVICE_TYPE_ITEM', 13, 'Create Service Type Item', '');
INSERT INTO `permission` VALUES ('CREATE_USER', 1, 'Create User', 'Can create user login and user information for user.');
INSERT INTO `permission` VALUES ('DELETE_DEPARTMENT', 8, 'Delete Department', '');
INSERT INTO `permission` VALUES ('DELETE_EQUIPMENT', 7, 'Delete Equipment', '');
INSERT INTO `permission` VALUES ('DELETE_EQUIPMENT_TYPE', 6, 'Delete Equipment Type', '');
INSERT INTO `permission` VALUES ('DELETE_POSITION', 10, 'Delete Position', '');
INSERT INTO `permission` VALUES ('DELETE_PRESENT_TYPE', 11, 'Delete Present Type', '');
INSERT INTO `permission` VALUES ('DELETE_REQUEST_BOOKING', 3, 'Delete Request Booking', '');
INSERT INTO `permission` VALUES ('DELETE_REQUEST_BORROW', 4, 'Delete Request Borrow', '');
INSERT INTO `permission` VALUES ('DELETE_REQUEST_SERVICE', 5, 'Delete Request Service', '');
INSERT INTO `permission` VALUES ('DELETE_ROLE', 2, 'Delete Role', '');
INSERT INTO `permission` VALUES ('DELETE_ROOM', 9, 'Delete Room', '');
INSERT INTO `permission` VALUES ('DELETE_SEMESTER', 14, 'Delete Semester', '');
INSERT INTO `permission` VALUES ('DELETE_SERVICE_TYPE', 12, 'Delete Service Type', '');
INSERT INTO `permission` VALUES ('DELETE_SERVICE_TYPE_ITEM', 13, 'Delete Service Type Item', '');
INSERT INTO `permission` VALUES ('DELETE_USER', 1, 'Delete User', '');
INSERT INTO `permission` VALUES ('EDIT_REQUEST_BOOKING', 3, 'Edit', '');
INSERT INTO `permission` VALUES ('FULL_ADMIN', NULL, 'Full Admin', '"Full Admin" can access all module.');
INSERT INTO `permission` VALUES ('UPDATE_DEPARTMENT', 8, 'Update Department', '');
INSERT INTO `permission` VALUES ('UPDATE_EQUIPMENT', 7, 'Update Equipment', '');
INSERT INTO `permission` VALUES ('UPDATE_EQUIPMENT_TYPE', 6, 'Update Equipment Type', '');
INSERT INTO `permission` VALUES ('UPDATE_POSITION', 10, 'Update Position', '');
INSERT INTO `permission` VALUES ('UPDATE_PRESENT_TYPE', 11, 'Update Present Type', '');
INSERT INTO `permission` VALUES ('UPDATE_REQUEST_BORROW', 4, 'Update Request Borrow', '');
INSERT INTO `permission` VALUES ('UPDATE_REQUEST_SERVICE', 5, 'Update Request Service', '');
INSERT INTO `permission` VALUES ('UPDATE_ROLE', 2, 'Update Role', '');
INSERT INTO `permission` VALUES ('UPDATE_ROOM', 9, 'Update Room', '');
INSERT INTO `permission` VALUES ('UPDATE_SEMESTER', 14, 'Update Semester', '');
INSERT INTO `permission` VALUES ('UPDATE_SERVICE_TYPE', 12, 'Update Service Type', '');
INSERT INTO `permission` VALUES ('UPDATE_SERVICE_TYPE)ITEM', 13, 'Update Service Type Item', '');
INSERT INTO `permission` VALUES ('UPDATE_USER', 1, 'Update User', '');
INSERT INTO `permission` VALUES ('VIEW_ALL_REQUEST_BOOKING', 3, 'View All Request Booking', '');
INSERT INTO `permission` VALUES ('VIEW_ALL_REQUEST_BORROW', 4, 'View All Request Borrow', '');
INSERT INTO `permission` VALUES ('VIEW_ALL_REQUEST_SERVICE', 5, 'View All Request Service', '');
INSERT INTO `permission` VALUES ('VIEW_DEPARTMENT', 8, 'View Department', '');
INSERT INTO `permission` VALUES ('VIEW_EQUIPMENT', 7, 'View Equipment', '');
INSERT INTO `permission` VALUES ('VIEW_EQUIPMENT_TYPE', 6, 'View Equipment Type', '');
INSERT INTO `permission` VALUES ('VIEW_POSITION', 10, 'View Position', '');
INSERT INTO `permission` VALUES ('VIEW_PRESENT_TYPE', 11, 'View Present Type', '');
INSERT INTO `permission` VALUES ('VIEW_REQUEST_BOOKING', 3, 'View Request Booking', '');
INSERT INTO `permission` VALUES ('VIEW_REQUEST_BORROW', 4, 'View Request Borrow', '');
INSERT INTO `permission` VALUES ('VIEW_REQUEST_SERVICE', 5, 'View Request Service', '');
INSERT INTO `permission` VALUES ('VIEW_ROLE', 2, 'View Role', '');
INSERT INTO `permission` VALUES ('VIEW_ROOM', 9, 'View Room', '');
INSERT INTO `permission` VALUES ('VIEW_SEMESTER', 14, 'View Semester', '');
INSERT INTO `permission` VALUES ('VIEW_SERVICE_TYPE', 12, 'View Service Type', '');
INSERT INTO `permission` VALUES ('VIEW_SERVICE_TYPE_ITEM', 13, 'View Service Type Item', '');
INSERT INTO `permission` VALUES ('VIEW_USER', 1, 'View User', 'View user information');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `permission_group`
-- 

CREATE TABLE `permission_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- 
-- dump ตาราง `permission_group`
-- 

INSERT INTO `permission_group` VALUES (1, 'User', '');
INSERT INTO `permission_group` VALUES (2, 'Role', '');
INSERT INTO `permission_group` VALUES (3, 'Request Booking', '');
INSERT INTO `permission_group` VALUES (4, 'Request Borrow', '');
INSERT INTO `permission_group` VALUES (5, 'Request Service', '');
INSERT INTO `permission_group` VALUES (6, 'Equipment Type', '');
INSERT INTO `permission_group` VALUES (7, 'Equipment', '');
INSERT INTO `permission_group` VALUES (8, 'Department', '');
INSERT INTO `permission_group` VALUES (9, 'Room', '');
INSERT INTO `permission_group` VALUES (10, 'Position', '');
INSERT INTO `permission_group` VALUES (11, 'Present Type', '');
INSERT INTO `permission_group` VALUES (12, 'Service Type', '');
INSERT INTO `permission_group` VALUES (13, 'Service Type Item', '');
INSERT INTO `permission_group` VALUES (14, 'Semester', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `position`
-- 

CREATE TABLE `position` (
  `id` int(11) NOT NULL auto_increment,
  `position_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `position_code` (`position_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `position`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `present_type`
-- 

CREATE TABLE `present_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- dump ตาราง `present_type`
-- 

INSERT INTO `present_type` VALUES (1, 'Powerpoint presentation', '');
INSERT INTO `present_type` VALUES (2, 'VDO presentation', '');
INSERT INTO `present_type` VALUES (3, 'MUIC presentation', '');
INSERT INTO `present_type` VALUES (4, 'Paper or Written on paper', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_booking`
-- 

CREATE TABLE `request_booking` (
  `id` int(11) NOT NULL auto_increment,
  `user_login_id` int(11) NOT NULL,
  `request_booking_type_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `semester_id` int(11) default NULL,
  `request_date` date default NULL,
  `request_day_in_week` int(11) default NULL,
  `period_start` int(11) NOT NULL,
  `period_end` int(11) NOT NULL,
  `description` text NOT NULL,
  `status_code` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `course_name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_login_id` (`user_login_id`),
  KEY `room_id` (`room_id`),
  KEY `period_start` (`period_start`),
  KEY `period_end` (`period_end`),
  KEY `status_code` (`status_code`),
  KEY `semester_id` (`semester_id`),
  KEY `request_booking_type_id` (`request_booking_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- dump ตาราง `request_booking`
-- 

INSERT INTO `request_booking` VALUES (1, 1, 1, 1, NULL, '2013-05-20', NULL, 4, 4, '', 'REQUEST_WAIT_APPROVE', '2013-05-16 20:00:53', 'ดกเหกเ');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_booking_activity_detail`
-- 

CREATE TABLE `request_booking_activity_detail` (
  `id` int(11) NOT NULL auto_increment,
  `event_type_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `event_type_id` (`event_type_id`),
  KEY `room_type_id` (`room_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `request_booking_activity_detail`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_booking_activity_present_type`
-- 

CREATE TABLE `request_booking_activity_present_type` (
  `id` int(11) NOT NULL auto_increment,
  `request_booking_activity_id` int(11) NOT NULL,
  `present_type_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `request_booking_activity_id` (`request_booking_activity_id`),
  KEY `present_type_id` (`present_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `request_booking_activity_present_type`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_booking_equipment_type`
-- 

CREATE TABLE `request_booking_equipment_type` (
  `id` int(11) NOT NULL auto_increment,
  `request_booking_id` int(11) NOT NULL,
  `equipment_type_id` int(11) NOT NULL,
  `quantity` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `request_booking_id` (`request_booking_id`),
  KEY `equipment_type_id` (`equipment_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `request_booking_equipment_type`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_booking_type`
-- 

CREATE TABLE `request_booking_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- dump ตาราง `request_booking_type`
-- 

INSERT INTO `request_booking_type` VALUES (1, 'Daily', '');
INSERT INTO `request_booking_type` VALUES (2, 'Semester', '');
INSERT INTO `request_booking_type` VALUES (3, 'Activity / Meeting', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_borrow`
-- 

CREATE TABLE `request_borrow` (
  `id` int(11) NOT NULL auto_increment,
  `user_login_id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `approve_by` varchar(255) NOT NULL,
  `chef_email` varchar(255) NOT NULL,
  `event_type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `from_date` date NOT NULL,
  `thru_date` date NOT NULL,
  `status_code` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `event_type_id` (`event_type_id`),
  KEY `user_login_id` (`user_login_id`),
  KEY `status_code` (`status_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- 
-- dump ตาราง `request_borrow`
-- 

INSERT INTO `request_borrow` VALUES (2, 1, 'WHITHIN_MUIC', '', 'cccc', 1, 'cccc', '2013-04-02', '2013-04-04', 'REQUEST_BORROW_APPROVED', '2013-04-02 11:44:15');
INSERT INTO `request_borrow` VALUES (3, 4, 'WHITHIN_MUIC', '', 'dsadsf', 1, 'sdfsdf', '2013-04-09', '2013-04-18', 'REQUEST_BORROW_COMPLETED', '2013-04-08 17:11:31');
INSERT INTO `request_borrow` VALUES (5, 1, 'WHITHIN_MUIC', '', 'dsdg', 1, 'sfgsf', '2013-04-28', '2013-04-29', 'REQUEST_BORROW_CONFIRMED', '2013-04-27 08:47:40');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_borrow_equipment_type`
-- 

CREATE TABLE `request_borrow_equipment_type` (
  `id` int(11) NOT NULL auto_increment,
  `request_borrow_id` int(11) NOT NULL,
  `equipment_type_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `request_borrow_id` (`request_borrow_id`),
  KEY `equipment_type_id` (`equipment_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- 
-- dump ตาราง `request_borrow_equipment_type`
-- 

INSERT INTO `request_borrow_equipment_type` VALUES (3, 2, 1, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (4, 3, 1, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (5, 3, 2, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (6, 5, 2, 1);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_service`
-- 

CREATE TABLE `request_service` (
  `id` int(11) NOT NULL auto_increment,
  `user_login_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `time_period` int(11) NOT NULL,
  `status_code` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_login_id` (`user_login_id`),
  KEY `time_period` (`time_period`),
  KEY `status_code` (`status_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `request_service`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_service_detail`
-- 

CREATE TABLE `request_service_detail` (
  `id` int(11) NOT NULL auto_increment,
  `request_service_id` int(11) NOT NULL,
  `request_service_type_detail_id` int(11) default NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `request_service_id` (`request_service_id`),
  KEY `request_service_type_detail_id` (`request_service_type_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `request_service_detail`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_service_type`
-- 

CREATE TABLE `request_service_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- 
-- dump ตาราง `request_service_type`
-- 

INSERT INTO `request_service_type` VALUES (1, 'Printing', '');
INSERT INTO `request_service_type` VALUES (2, 'Graphic', '');
INSERT INTO `request_service_type` VALUES (3, 'Sticker', '');
INSERT INTO `request_service_type` VALUES (4, 'VDO Records', '');
INSERT INTO `request_service_type` VALUES (5, 'Sound Records', '');
INSERT INTO `request_service_type` VALUES (6, 'Copy', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request_service_type_detail`
-- 

CREATE TABLE `request_service_type_detail` (
  `id` int(11) NOT NULL auto_increment,
  `request_service_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `request_service_type_id` (`request_service_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- 
-- dump ตาราง `request_service_type_detail`
-- 

INSERT INTO `request_service_type_detail` VALUES (1, 1, 'A4', '');
INSERT INTO `request_service_type_detail` VALUES (2, 1, 'A3', '');
INSERT INTO `request_service_type_detail` VALUES (3, 1, 'A2', '');
INSERT INTO `request_service_type_detail` VALUES (4, 2, 'Poster / Paper', '');
INSERT INTO `request_service_type_detail` VALUES (5, 2, 'File formats', '');
INSERT INTO `request_service_type_detail` VALUES (6, 2, 'Books', '');
INSERT INTO `request_service_type_detail` VALUES (7, 3, 'Lable', '');
INSERT INTO `request_service_type_detail` VALUES (8, 3, 'Backdrop', '');
INSERT INTO `request_service_type_detail` VALUES (9, 4, 'Sound', '');
INSERT INTO `request_service_type_detail` VALUES (10, 4, 'VDO', '');
INSERT INTO `request_service_type_detail` VALUES (11, 5, 'E-book', '');
INSERT INTO `request_service_type_detail` VALUES (12, 5, 'Presentation', '');
INSERT INTO `request_service_type_detail` VALUES (13, 6, 'CD', '');
INSERT INTO `request_service_type_detail` VALUES (14, 6, 'DVD', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `role`
-- 

CREATE TABLE `role` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- 
-- dump ตาราง `role`
-- 

INSERT INTO `role` VALUES (1, 'Admin', 'Admin can access all data', 'HIDDEN');
INSERT INTO `role` VALUES (2, 'Staff', 'Staff is person who check and manage about user and booking.', '');
INSERT INTO `role` VALUES (3, 'User', 'User can request book for equipment. ', '');
INSERT INTO `role` VALUES (5, 'sdsfadf', 'asdfasee1111', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `role_permission`
-- 

CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL auto_increment,
  `role_id` int(11) NOT NULL,
  `permission_code` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `role_id` (`role_id`,`permission_code`),
  KEY `permission_id` (`permission_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=104 ;

-- 
-- dump ตาราง `role_permission`
-- 

INSERT INTO `role_permission` VALUES (1, 1, 'FULL_ADMIN');
INSERT INTO `role_permission` VALUES (100, 2, 'CHECK_STATUS_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (103, 2, 'CHECK_STATUS_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (45, 2, 'CREATE_DEPARTMENT');
INSERT INTO `role_permission` VALUES (46, 2, 'CREATE_EQUIPMENT');
INSERT INTO `role_permission` VALUES (47, 2, 'CREATE_EQUIPMENT_TYPE');
INSERT INTO `role_permission` VALUES (48, 2, 'CREATE_POSITION');
INSERT INTO `role_permission` VALUES (49, 2, 'CREATE_PRESEN_TYPE');
INSERT INTO `role_permission` VALUES (98, 2, 'CREATE_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (50, 2, 'CREATE_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (51, 2, 'CREATE_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (52, 2, 'CREATE_ROOM');
INSERT INTO `role_permission` VALUES (53, 2, 'CREATE_SEMESTER');
INSERT INTO `role_permission` VALUES (54, 2, 'CREATE_SERVICE_TYPE');
INSERT INTO `role_permission` VALUES (55, 2, 'CREATE_SERVICE_TYPE_ITEM');
INSERT INTO `role_permission` VALUES (34, 2, 'CREATE_USER');
INSERT INTO `role_permission` VALUES (56, 2, 'DELETE_DEPARTMENT');
INSERT INTO `role_permission` VALUES (57, 2, 'DELETE_EQUIPMENT');
INSERT INTO `role_permission` VALUES (58, 2, 'DELETE_EQUIPMENT_TYPE');
INSERT INTO `role_permission` VALUES (59, 2, 'DELETE_POSITION');
INSERT INTO `role_permission` VALUES (60, 2, 'DELETE_PRESENT_TYPE');
INSERT INTO `role_permission` VALUES (61, 2, 'DELETE_ROOM');
INSERT INTO `role_permission` VALUES (62, 2, 'DELETE_SEMESTER');
INSERT INTO `role_permission` VALUES (63, 2, 'DELETE_SERVICE_TYPE');
INSERT INTO `role_permission` VALUES (64, 2, 'DELETE_SERVICE_TYPE_ITEM');
INSERT INTO `role_permission` VALUES (65, 2, 'EDIT_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (66, 2, 'UPDATE_DEPARTMENT');
INSERT INTO `role_permission` VALUES (67, 2, 'UPDATE_EQUIPMENT');
INSERT INTO `role_permission` VALUES (68, 2, 'UPDATE_EQUIPMENT_TYPE');
INSERT INTO `role_permission` VALUES (69, 2, 'UPDATE_POSITION');
INSERT INTO `role_permission` VALUES (70, 2, 'UPDATE_PRESENT_TYPE');
INSERT INTO `role_permission` VALUES (71, 2, 'UPDATE_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (72, 2, 'UPDATE_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (73, 2, 'UPDATE_ROOM');
INSERT INTO `role_permission` VALUES (74, 2, 'UPDATE_SEMESTER');
INSERT INTO `role_permission` VALUES (75, 2, 'UPDATE_SERVICE_TYPE');
INSERT INTO `role_permission` VALUES (76, 2, 'UPDATE_SERVICE_TYPE)ITEM');
INSERT INTO `role_permission` VALUES (32, 2, 'UPDATE_USER');
INSERT INTO `role_permission` VALUES (77, 2, 'VIEW_ALL_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (78, 2, 'VIEW_ALL_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (79, 2, 'VIEW_ALL_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (80, 2, 'VIEW_DEPARTMENT');
INSERT INTO `role_permission` VALUES (81, 2, 'VIEW_EQUIPMENT');
INSERT INTO `role_permission` VALUES (82, 2, 'VIEW_EQUIPMENT_TYPE');
INSERT INTO `role_permission` VALUES (83, 2, 'VIEW_POSITION');
INSERT INTO `role_permission` VALUES (84, 2, 'VIEW_PRESENT_TYPE');
INSERT INTO `role_permission` VALUES (85, 2, 'VIEW_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (86, 2, 'VIEW_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (87, 2, 'VIEW_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (88, 2, 'VIEW_ROOM');
INSERT INTO `role_permission` VALUES (89, 2, 'VIEW_SEMESTER');
INSERT INTO `role_permission` VALUES (90, 2, 'VIEW_SERVICE_TYPE');
INSERT INTO `role_permission` VALUES (91, 2, 'VIEW_SERVICE_TYPE_ITEM');
INSERT INTO `role_permission` VALUES (35, 2, 'VIEW_USER');
INSERT INTO `role_permission` VALUES (101, 3, 'CHECK_STATUS_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (102, 3, 'CHECK_STATUS_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (99, 3, 'CREATE_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (93, 3, 'CREATE_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (94, 3, 'CREATE_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (95, 3, 'VIEW_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (96, 3, 'VIEW_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (97, 3, 'VIEW_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (39, 5, 'CREATE_USER');
INSERT INTO `role_permission` VALUES (42, 5, 'DELETE_ROLE');
INSERT INTO `role_permission` VALUES (40, 5, 'DELETE_USER');
INSERT INTO `role_permission` VALUES (43, 5, 'UPDATE_ROLE');
INSERT INTO `role_permission` VALUES (41, 5, 'UPDATE_USER');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `room`
-- 

CREATE TABLE `room` (
  `id` int(11) NOT NULL auto_increment,
  `room_group_id` int(11) NOT NULL,
  `room_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `room_code` (`room_code`),
  KEY `room_type_id` (`room_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3506 ;

-- 
-- dump ตาราง `room`
-- 

INSERT INTO `room` VALUES (1, 1, '1', 'Ground Floor', 'Ground Floor');
INSERT INTO `room` VALUES (2, 1, '2', 'Heab', 'Heab');
INSERT INTO `room` VALUES (3, 1, '3', 'Mahaswasdee I', 'Mahaswasdee I');
INSERT INTO `room` VALUES (4, 1, '4', 'Mahaswasdee II', 'Mahaswasdee II');
INSERT INTO `room` VALUES (5, 1, '5', 'Narapirom', 'Narapirom');
INSERT INTO `room` VALUES (6, 1, '6', 'Taweewattana  I', 'Taweewattana  I');
INSERT INTO `room` VALUES (7, 1, '7', 'Taweewattana  II', 'Taweewattana  II');
INSERT INTO `room` VALUES (1101, 2, '1101', '1101 (CLW)', '1101 (CLW)');
INSERT INTO `room` VALUES (1108, 2, '1108', '1108 (Food)', '1108 (Food)');
INSERT INTO `room` VALUES (1202, 2, '1202', '1202 (SV)', '1202 (SV)');
INSERT INTO `room` VALUES (1210, 2, '1210', '1210 (Semi)', '1210 (Semi)');
INSERT INTO `room` VALUES (1214, 2, '1214', '1214 (MK)', '1214 (MK)');
INSERT INTO `room` VALUES (1302, 1, '1302', '1302', '1302');
INSERT INTO `room` VALUES (1303, 1, '1303', '1303', '1303');
INSERT INTO `room` VALUES (1304, 1, '1304', '1304', '1304');
INSERT INTO `room` VALUES (1305, 1, '1305', '1305', '1305');
INSERT INTO `room` VALUES (1306, 1, '1306', '1306', '1306');
INSERT INTO `room` VALUES (1307, 2, '1307', '1307 (Next Audi)', '1307 (Next Audi)');
INSERT INTO `room` VALUES (1308, 1, '1308', '1308', '1308');
INSERT INTO `room` VALUES (1309, 2, '1309', '1309 (Math Clinic)', '1309 (Math Clinic)');
INSERT INTO `room` VALUES (1312, 2, '1312', '1312 (FAA)', '1312 (FAA)');
INSERT INTO `room` VALUES (1314, 1, '1314', '1314', '1314');
INSERT INTO `room` VALUES (1315, 1, '1315', '1315', '1315');
INSERT INTO `room` VALUES (1318, 2, '1318', '1318 (Audi)', '1318 (Audi)');
INSERT INTO `room` VALUES (1402, 1, '1402', '1402', '1402');
INSERT INTO `room` VALUES (1403, 1, '1403', '1403', '1403');
INSERT INTO `room` VALUES (1404, 1, '1404', '1404', '1404');
INSERT INTO `room` VALUES (1405, 1, '1405', '1405', '1405');
INSERT INTO `room` VALUES (1406, 1, '1406', '1406', '1406');
INSERT INTO `room` VALUES (1407, 1, '1407', '1407', '1407');
INSERT INTO `room` VALUES (1408, 2, '1408', '1408 (Lab Com)', '1408 (Lab Com)');
INSERT INTO `room` VALUES (1417, 1, '1417', '1417', '1417');
INSERT INTO `room` VALUES (1418, 1, '1418', '1418', '1418');
INSERT INTO `room` VALUES (1419, 1, '1419', '1419', '1419');
INSERT INTO `room` VALUES (1502, 1, '1502', '1502', '1502');
INSERT INTO `room` VALUES (1503, 1, '1503', '1503', '1503');
INSERT INTO `room` VALUES (1504, 1, '1504', '1504', '1504');
INSERT INTO `room` VALUES (1505, 1, '1505', '1505', '1505');
INSERT INTO `room` VALUES (1506, 2, '1506', '1506 (Science LAB)', '1506 (Science LAB)');
INSERT INTO `room` VALUES (1515, 1, '1515', '1515', '1515');
INSERT INTO `room` VALUES (1516, 1, '1516', '1516', '1516');
INSERT INTO `room` VALUES (2207, 2, '2207', '2207 (BBA)', '2207 (BBA)');
INSERT INTO `room` VALUES (2302, 2, '2302', '2302 (Dance)', '2302 (Dance)');
INSERT INTO `room` VALUES (2303, 1, '2303', '2303', '2303');
INSERT INTO `room` VALUES (2306, 1, '2306', '2306', '2306');
INSERT INTO `room` VALUES (2307, 1, '2307', '2307', '2307');
INSERT INTO `room` VALUES (2308, 1, '2308', '2308', '2308');
INSERT INTO `room` VALUES (3302, 1, '3302', '3302', '3302');
INSERT INTO `room` VALUES (3303, 1, '3303', '3303', '3303');
INSERT INTO `room` VALUES (3304, 1, '3304', '3304', '3304');
INSERT INTO `room` VALUES (3305, 1, '3305', '3305', '3305');
INSERT INTO `room` VALUES (3306, 1, '3306', '3306', '3306');
INSERT INTO `room` VALUES (3307, 2, '3307', '3307 (Drawing)', '3307 (Drawing)');
INSERT INTO `room` VALUES (3315, 1, '3315', '3315', '3315');
INSERT INTO `room` VALUES (3316, 1, '3316', '3316', '3316');
INSERT INTO `room` VALUES (3317, 1, '3317', '3317', '3317');
INSERT INTO `room` VALUES (3402, 2, '3402', '3402 (AV)', '3402 (AV)');
INSERT INTO `room` VALUES (3407, 2, '3407', '3407 (Dance AV)', '3407 (Dance AV)');
INSERT INTO `room` VALUES (3408, 1, '3408', '3408', '3408');
INSERT INTO `room` VALUES (3409, 1, '3409', '3409', '3409');
INSERT INTO `room` VALUES (3410, 1, '3410', '3410', '3410');
INSERT INTO `room` VALUES (3411, 1, '3411', '3411', '3411');
INSERT INTO `room` VALUES (3412, 1, '3412', '3412', '3412');
INSERT INTO `room` VALUES (3414, 2, '3414', '3414  (Meeting F.4)', '3414  (Meeting F.4)');
INSERT INTO `room` VALUES (3415, 1, '3415', '3415', '3415');
INSERT INTO `room` VALUES (3420, 1, '3420', '3420', '3420');
INSERT INTO `room` VALUES (3421, 1, '3421', '3421', '3421');
INSERT INTO `room` VALUES (3422, 1, '3422', '3422', '3422');
INSERT INTO `room` VALUES (3504, 2, '3504', '3504 (Lab Science)', '3504 (Lab Science)');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `room_equipment`
-- 

CREATE TABLE `room_equipment` (
  `id` int(11) NOT NULL auto_increment,
  `room_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `room_id` (`room_id`),
  KEY `equipment_id` (`equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `room_equipment`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `room_group`
-- 

CREATE TABLE `room_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- dump ตาราง `room_group`
-- 

INSERT INTO `room_group` VALUES (1, 'Class Room', '');
INSERT INTO `room_group` VALUES (2, 'Meeting Room', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `room_type`
-- 

CREATE TABLE `room_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- 
-- dump ตาราง `room_type`
-- 

INSERT INTO `room_type` VALUES (1, 'Classroom', '');
INSERT INTO `room_type` VALUES (2, 'Conference', '');
INSERT INTO `room_type` VALUES (3, 'Without MUIC', '');
INSERT INTO `room_type` VALUES (4, 'Other', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `semester`
-- 

CREATE TABLE `semester` (
  `id` int(11) NOT NULL auto_increment,
  `academic_year` int(11) NOT NULL,
  `semester_number` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- dump ตาราง `semester`
-- 

INSERT INTO `semester` VALUES (1, 2555, 2, '2 / 2555', '', '2012-11-05', '2013-03-15', 'ACTIVE');
INSERT INTO `semester` VALUES (2, 2555, 3, '3 / 2555', '', '2013-03-18', '2013-05-17', 'ACTIVE');
INSERT INTO `semester` VALUES (3, 2556, 1, '1 / 2556', '', '2013-06-03', '2013-09-27', 'ACTIVE');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `status`
-- 

CREATE TABLE `status` (
  `status_code` varchar(255) NOT NULL,
  `status_group_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `active` varchar(1) NOT NULL,
  PRIMARY KEY  (`status_code`),
  KEY `status_group_id` (`status_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- dump ตาราง `status`
-- 

INSERT INTO `status` VALUES ('ACTIVE', 'USER_LOGIN_STATUS', 'Active', '', 'Y');
INSERT INTO `status` VALUES ('INACTIVE', 'USER_LOGIN_STATUS', 'Inactive', '', 'Y');
INSERT INTO `status` VALUES ('PERIOD_ACTIVE', 'PERIOD_STATUS', 'Active', '', 'Y');
INSERT INTO `status` VALUES ('PERIOD_GROUP_ACTIVE', 'PERIOD_GROUP_STATUS', 'Active', '', 'Y');
INSERT INTO `status` VALUES ('PERIOD_GROUP_INACTIVE', 'PERIOD_GROUP_STATUS', 'Inactive', '', 'Y');
INSERT INTO `status` VALUES ('PERIOD_INACTIVE', 'PERIOD_STATUS', 'Inactive', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_APPROVE', 'REQUEST_STATUS', 'Approve', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_BORROW_APPROVED', 'REQUEST_BORROW_STATUS', 'Approved', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_BORROW_CANCELLED', 'REQUEST_BORROW_STATUS', 'Cancelled', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_BORROW_COMPLETED', 'REQUEST_BORROW_STATUS', 'Completed', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_BORROW_CONFIRMED', 'REQUEST_BORROW_STATUS', 'Confirmed', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_BORROW_CREATED', 'REQUEST_BORROW_STATUS', 'Created', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_BORROW_DENIED', 'REQUEST_BORROW_STATUS', 'Denied', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_CANCEL', 'REQUEST_STATUS', 'Cancel', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_CREATED', 'REQUEST_STATUS', 'Created', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_DISAPPROVE', 'REQUEST_STATUS', 'Disapprove', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_WAIT_APPROVE', 'REQUEST_STATUS', 'Wait for approve', '', 'Y');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `status_group`
-- 

CREATE TABLE `status_group` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `active` varchar(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- dump ตาราง `status_group`
-- 

INSERT INTO `status_group` VALUES ('PERIOD_GROUP_STATUS', 'Period Group Status', '', 'Y');
INSERT INTO `status_group` VALUES ('PERIOD_STATUS', 'Period Status', '', 'Y');
INSERT INTO `status_group` VALUES ('REQUEST_BORROW_STATUS', 'Request Borrow Status', '', 'Y');
INSERT INTO `status_group` VALUES ('REQUEST_STATUS', 'Request Status', '', 'Y');
INSERT INTO `status_group` VALUES ('USER_LOGIN_STATUS', 'User Login Status', '', 'Y');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `user_forget_password_request`
-- 

CREATE TABLE `user_forget_password_request` (
  `id` int(11) NOT NULL auto_increment,
  `user_login_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `request_date` datetime NOT NULL,
  `status` varchar(5) default NULL,
  PRIMARY KEY  (`id`),
  KEY `user_login_id` (`user_login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `user_forget_password_request`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `user_information`
-- 

CREATE TABLE `user_information` (
  `id` int(11) NOT NULL auto_increment COMMENT 'รหัส',
  `personal_card_id` varchar(13) NOT NULL COMMENT 'เลขประจำตัวประชาชน',
  `personal_title` varchar(255) NOT NULL COMMENT 'คำนำหน้า',
  `first_name` varchar(255) NOT NULL COMMENT 'ชื่อ',
  `last_name` varchar(255) NOT NULL COMMENT 'นามสกุล',
  `gender` varchar(5) NOT NULL COMMENT 'เพศ',
  `birth_date` date NOT NULL COMMENT 'วันเกิด',
  `address1` varchar(255) NOT NULL COMMENT 'ที่อยู่ 1',
  `address2` varchar(255) NOT NULL COMMENT 'ที่อยู่ 2',
  `sub_district` varchar(255) NOT NULL COMMENT 'ตำบล',
  `district` varchar(255) NOT NULL COMMENT 'อำเภอ',
  `province` varchar(255) NOT NULL COMMENT 'จังหวัด',
  `postal_code` varchar(5) NOT NULL COMMENT 'รหัสไปรษณีย์',
  `phone` varchar(50) NOT NULL COMMENT 'โทรศัพท์',
  `mobile` varchar(50) NOT NULL COMMENT 'โทรศัพท์มือถือ',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `personal_card_id` (`personal_card_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- 
-- dump ตาราง `user_information`
-- 

INSERT INTO `user_information` VALUES (1, '', '', 'Admin', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (2, '1809922837593', 'นาย', 'สมศักดิ์', 'รักษ์ไทย', 'M', '1986-02-14', '43/2', 'ถ.รามอินทรา ซอย 14 แยก 4', 'ท่าแร้ง', 'บางเขน', 'กรุงเทพ', '10233', '', '0874456733');
INSERT INTO `user_information` VALUES (4, '1809922837595', 'นาย', 'สมปอง', 'สุขใจ', 'M', '0000-00-00', '234', 'ถ.รามอินทรา ซอย 14 แยก 4', 'ท่าแร้ง', 'บางเขน', 'กรุงเทพ', '10020', '', '');
INSERT INTO `user_information` VALUES (12, 'dfasdf', 'asdfasda', 'sdfadsf', 'sadf', 'F', '0000-00-00', 'adf', 'adsfa', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (13, 'sdfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', '', '0000-00-00', 'sdfsdf', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (14, '1111', 'dsfads', 'adsf', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (15, '11112', 'dsfads', 'adsf', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (17, 'dsfewrsf', 'sdaf', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (18, 'wefsdfvsd', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `user_login`
-- 

CREATE TABLE `user_login` (
  `id` int(11) NOT NULL auto_increment COMMENT 'รหัส',
  `role_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL COMMENT 'ชื่อผู้ใช้สำหรับ login',
  `password` varchar(100) NOT NULL COMMENT 'รหัสผ่าน',
  `status` varchar(255) NOT NULL COMMENT 'สถานะ',
  `create_by` int(11) default NULL COMMENT 'สร้างโดย',
  `latest_login` datetime NOT NULL COMMENT 'เข้าสู่ระบบครั้งล่าสุด',
  `department_id` int(11) default NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `role_id` (`role_id`),
  KEY `department_id` (`department_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- 
-- dump ตาราง `user_login`
-- 

INSERT INTO `user_login` VALUES (1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', NULL, '2013-05-18 08:03:27', NULL, 'gritchaphat88@gmail.com');
INSERT INTO `user_login` VALUES (2, 2, 'staff01', '5f4dcc3b5aa765d61d8327deb882cf99', 'ACTIVE', 1, '0000-00-00 00:00:00', NULL, 'test@email.com');
INSERT INTO `user_login` VALUES (4, 3, 'user01', '5f4dcc3b5aa765d61d8327deb882cf99', 'ACTIVE', 1, '0000-00-00 00:00:00', NULL, 'test2@email.com');
INSERT INTO `user_login` VALUES (12, 3, 'xxxsdfads', '3640536b683d5276cbe1174768aed8df', 'ACTIVE', NULL, '0000-00-00 00:00:00', NULL, 'test3@email.com');
INSERT INTO `user_login` VALUES (13, 2, 'sdfasdf', '6fbfd5e68d3306e51350bea0232f8fa5', 'INACTIVE', NULL, '0000-00-00 00:00:00', NULL, 'test4@email.com');
INSERT INTO `user_login` VALUES (14, 3, 'xcfdsafad', '845636a064eb69ed8a22c1c27536dd39', 'INACTIVE', NULL, '0000-00-00 00:00:00', NULL, 'asdfdf@dfg.dfgs');
INSERT INTO `user_login` VALUES (15, 3, 'werwere', 'd41d8cd98f00b204e9800998ecf8427e', 'INACTIVE', NULL, '0000-00-00 00:00:00', NULL, 'asdfdf@dfg.dfgs1');
INSERT INTO `user_login` VALUES (16, 3, 'dfdddd', '5f5a2aceda732249a607cc2b7eee2f3c', 'INACTIVE', NULL, '0000-00-00 00:00:00', NULL, 'sdfadsf@gdfsg.dfgsfd');
INSERT INTO `user_login` VALUES (17, 3, 'asdafasd', 'd29aaa0b9cd402b4bfe2395a805f9ada', 'INACTIVE', NULL, '0000-00-00 00:00:00', NULL, 'fewrewre');
INSERT INTO `user_login` VALUES (18, 3, 'rtwrtrwt', '852fbd08c187f4c237c4243bf3de494b', 'INACTIVE', NULL, '0000-00-00 00:00:00', NULL, 'wtrwetrw');

-- 
-- Constraints for dumped tables
-- 

-- 
-- Constraints for table `enumeration`
-- 
ALTER TABLE `enumeration`
  ADD CONSTRAINT `enumeration_ibfk_1` FOREIGN KEY (`enumeration_type_code`) REFERENCES `enumeration_type` (`code`);

-- 
-- Constraints for table `equipment`
-- 
ALTER TABLE `equipment`
  ADD CONSTRAINT `equipment_ibfk_1` FOREIGN KEY (`equipment_type_id`) REFERENCES `equipment_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `equipment_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`);

-- 
-- Constraints for table `period`
-- 
ALTER TABLE `period`
  ADD CONSTRAINT `period_ibfk_1` FOREIGN KEY (`preriod_group_id`) REFERENCES `period_group` (`id`),
  ADD CONSTRAINT `period_ibfk_2` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

-- 
-- Constraints for table `period_group`
-- 
ALTER TABLE `period_group`
  ADD CONSTRAINT `period_group_ibfk_1` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

-- 
-- Constraints for table `permission`
-- 
ALTER TABLE `permission`
  ADD CONSTRAINT `permission_ibfk_1` FOREIGN KEY (`permission_group_id`) REFERENCES `permission_group` (`id`);

-- 
-- Constraints for table `request_booking`
-- 
ALTER TABLE `request_booking`
  ADD CONSTRAINT `request_booking_ibfk_30` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_31` FOREIGN KEY (`request_booking_type_id`) REFERENCES `request_booking_type` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_32` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_33` FOREIGN KEY (`semester_id`) REFERENCES `semester` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_34` FOREIGN KEY (`period_start`) REFERENCES `period` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_35` FOREIGN KEY (`period_end`) REFERENCES `period` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_36` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

-- 
-- Constraints for table `request_booking_activity_detail`
-- 
ALTER TABLE `request_booking_activity_detail`
  ADD CONSTRAINT `request_booking_activity_detail_ibfk_10` FOREIGN KEY (`event_type_id`) REFERENCES `event_type` (`id`),
  ADD CONSTRAINT `request_booking_activity_detail_ibfk_11` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`),
  ADD CONSTRAINT `request_booking_activity_detail_ibfk_9` FOREIGN KEY (`id`) REFERENCES `request_booking` (`id`) ON DELETE CASCADE;

-- 
-- Constraints for table `request_booking_activity_present_type`
-- 
ALTER TABLE `request_booking_activity_present_type`
  ADD CONSTRAINT `request_booking_activity_present_type_ibfk_3` FOREIGN KEY (`request_booking_activity_id`) REFERENCES `request_booking_activity_detail` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `request_booking_activity_present_type_ibfk_4` FOREIGN KEY (`present_type_id`) REFERENCES `present_type` (`id`);

-- 
-- Constraints for table `request_booking_equipment_type`
-- 
ALTER TABLE `request_booking_equipment_type`
  ADD CONSTRAINT `request_booking_equipment_type_ibfk_5` FOREIGN KEY (`request_booking_id`) REFERENCES `request_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `request_booking_equipment_type_ibfk_6` FOREIGN KEY (`equipment_type_id`) REFERENCES `equipment_type` (`id`);

-- 
-- Constraints for table `request_borrow`
-- 
ALTER TABLE `request_borrow`
  ADD CONSTRAINT `request_borrow_ibfk_11` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`),
  ADD CONSTRAINT `request_borrow_ibfk_12` FOREIGN KEY (`event_type_id`) REFERENCES `event_type` (`id`),
  ADD CONSTRAINT `request_borrow_ibfk_13` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

-- 
-- Constraints for table `request_borrow_equipment_type`
-- 
ALTER TABLE `request_borrow_equipment_type`
  ADD CONSTRAINT `request_borrow_equipment_type_ibfk_3` FOREIGN KEY (`request_borrow_id`) REFERENCES `request_borrow` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `request_borrow_equipment_type_ibfk_4` FOREIGN KEY (`equipment_type_id`) REFERENCES `equipment_type` (`id`);

-- 
-- Constraints for table `request_service`
-- 
ALTER TABLE `request_service`
  ADD CONSTRAINT `request_service_ibfk_6` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`),
  ADD CONSTRAINT `request_service_ibfk_7` FOREIGN KEY (`time_period`) REFERENCES `period` (`id`),
  ADD CONSTRAINT `request_service_ibfk_8` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

-- 
-- Constraints for table `request_service_detail`
-- 
ALTER TABLE `request_service_detail`
  ADD CONSTRAINT `request_service_detail_ibfk_3` FOREIGN KEY (`request_service_id`) REFERENCES `request_service` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `request_service_detail_ibfk_4` FOREIGN KEY (`request_service_type_detail_id`) REFERENCES `request_service_type_detail` (`id`);

-- 
-- Constraints for table `request_service_type_detail`
-- 
ALTER TABLE `request_service_type_detail`
  ADD CONSTRAINT `request_service_type_detail_ibfk_1` FOREIGN KEY (`request_service_type_id`) REFERENCES `request_service_type` (`id`);

-- 
-- Constraints for table `role_permission`
-- 
ALTER TABLE `role_permission`
  ADD CONSTRAINT `role_permission_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_permission_ibfk_2` FOREIGN KEY (`permission_code`) REFERENCES `permission` (`permission_code`) ON DELETE CASCADE;

-- 
-- Constraints for table `room`
-- 
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`room_group_id`) REFERENCES `room_group` (`id`);

-- 
-- Constraints for table `room_equipment`
-- 
ALTER TABLE `room_equipment`
  ADD CONSTRAINT `room_equipment_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `room_equipment_ibfk_2` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`);

-- 
-- Constraints for table `status`
-- 
ALTER TABLE `status`
  ADD CONSTRAINT `status_ibfk_1` FOREIGN KEY (`status_group_id`) REFERENCES `status_group` (`id`);

-- 
-- Constraints for table `user_forget_password_request`
-- 
ALTER TABLE `user_forget_password_request`
  ADD CONSTRAINT `user_forget_password_request_ibfk_1` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`);

-- 
-- Constraints for table `user_information`
-- 
ALTER TABLE `user_information`
  ADD CONSTRAINT `user_information_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user_login` (`id`) ON DELETE CASCADE;

-- 
-- Constraints for table `user_login`
-- 
ALTER TABLE `user_login`
  ADD CONSTRAINT `user_login_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `user_login_ibfk_4` FOREIGN KEY (`status`) REFERENCES `status` (`status_code`),
  ADD CONSTRAINT `user_login_ibfk_5` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);
