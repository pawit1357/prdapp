﻿using System;

namespace ALS.ALSI.Biz
{
    public class RawDataPivotForHpa3
    {
        /*
         * Feature	
         * Area	Field	
         * Rank	
         * Al-Ti-O (0.5<=ECD<=2.0 um)	
         * Al-Si-O (0.5<=ECD<=2.0 um)	
         * Si-O	Ti-C	
         * Ti-O	Al-O (0.5<=ECD<=2.0 um)	
         * Fe-Cr	
         * Fe-Cr-Ni	
         * Fe-Cr-Ni-Si	
         * Fe-Cr-Ni-Mn	
         * Fe-O	
         * No Element	
         * Ti-O/Al-Si-Fe	
         * Ni	
         * Sn base	
         * Other	
         * Al	
         * Al-Mg	
         * Al-Si Based	
         * Al-Si-Cu	
         * Al-Si-Fe	
         * Al-Si-Mg	
         * Zn base	
         * Ca base	
         * Ni base	
         * Cl base	
         * Cu	
         * Au-Ni	
         * Au	
         * Cu-Zn	
         * F-O	
         * Al-Si(1)	
         * Al-Si(2)	
         * Rejected (Manual)	
         * Rejected (ED)	
         * Rejected (Morph)	
         * Area (sq. µm)	
         * Aspect Ratio	
         * Beam X (pixels)	
         * Beam Y (pixels)	
         * Breadth (µm)	
         * Direction (degrees)	
         * ECD (µm)	
         * Length (µm)	
         * Perimeter (µm)	
         * Shape	
         * Mean grey	
         * Spectrum Area	
         * Stage X (mm)	
         * Stage Y (mm)	
         * Stage Z (mm)	
         * O (Wt%)	
         * F (Wt%)	
         * Na (Wt%)	
         * Mg (Wt%)	
         * Al (Wt%)	
         * Si (Wt%)	
         * P (Wt%)	
         * S (Wt%)	
         * Cl (Wt%)	
         * Ar (Wt%)	
         * K (Wt%)	
         * Ca (Wt%)	
         * Ti (Wt%)	
         * V (Wt%)	
         * Cr (Wt%)	
         * Mn (Wt%)	
         * Fe (Wt%)	
         * Co (Wt%)	
         * Ni (Wt%)	
         * Cu (Wt%)	
         * Zn (Wt%)	
         * Br (Wt%)
         * Mo (Wt%)
         * Ag (Wt%)	
         * Sn (Wt%)	
         * Sb (Wt%)	
         * Ba (Wt%)	
         * W (Wt%)	
         * Au (Wt%)	
         * Pb (Wt%)	
         * Ra (Wt%)	
         * Np (Wt%)
         */
        public String Feature { get; set; }
        public String Area_Field { get; set; }
        public String Rank { get; set; }
        public String Al_Ti_O { get; set; }
        public String Al_Si_O { get; set; }
        public String Si_O_Ti_C { get; set; }
        public String Ti_O_Al_O { get; set; }
        public String Fe_Cr_ { get; set; }
        public String Fe_Cr_Ni_ { get; set; }
        public String Fe_Cr_Ni_Si { get; set; }
        public String Fe_Cr_Ni_Mn { get; set; }
        public String Fe_O { get; set; }
        public String No_Element { get; set; }
        public String Ti_O_Al_Si_Fe { get; set; }
        public String Ni { get; set; }
        public String Sn_base { get; set; }
        public String Other { get; set; }
        public String Al { get; set; }
        public String Al_Mg { get; set; }
        public String Al_Si_Based { get; set; }
        public String Al_Si_Cu { get; set; }
        public String Al_Si_Fe { get; set; }
        public String Al_Si_Mg { get; set; }
        public String Zn_base { get; set; }
        public String Ca_base { get; set; }
        public String Ni_base { get; set; }
        public String Cl_base { get; set; }
        public String Cu { get; set; }
        public String Au_Ni { get; set; }
        public String Au { get; set; }
        public String Cu_Zn { get; set; }
        public String F_O { get; set; }
        public String Al_Si1 { get; set; }
        public String Al_Si2 { get; set; }
        public String Rejected_Manual { get; set; }
        public String Rejected_ED { get; set; }
        public String Rejected_Morph { get; set; }
        public String Area_squm { get; set; }
        public String Aspect_Ratio { get; set; }
        public String Beam_X_pixels { get; set; }
        public String Beam_Y_pixels { get; set; }
        public String Breadth { get; set; }
        public String Direction_degrees { get; set; }
        public String ECD { get; set; }
        public String Length { get; set; }
        public String Perimeter { get; set; }
        public String Shape { get; set; }
        public String Mean_grey_ { get; set; }
        public String Spectrum_Area { get; set; }
        public String Stage_X { get; set; }
        public String Stage_Y { get; set; }
        public String Stage_Z { get; set; }
        public String O_Wt { get; set; }
        public String F_Wt { get; set; }
        public String Na_Wt { get; set; }
        public String Mg_Wt { get; set; }
        public String Al_Wt { get; set; }
        public String Si_Wt { get; set; }
        public String P_Wt { get; set; }
        public String S_Wt { get; set; }
        public String Cl_Wt { get; set; }
        public String Ar_Wt { get; set; }
        public String K_Wt { get; set; }
        public String Ca_Wt { get; set; }
        public String Ti_Wt { get; set; }
        public String V_Wt { get; set; }
        public String Cr_Wt { get; set; }
        public String Mn_Wt { get; set; }
        public String Fe_Wt { get; set; }
        public String Co_Wt { get; set; }
        public String Ni_Wt { get; set; }
        public String Cu_Wt { get; set; }
        public String Zn_Wt { get; set; }
        public String Br_Wt { get; set; }
        public String Mo_Wt { get; set; }
        public String Ag_Wt { get; set; }
        public String Sn_Wt { get; set; }
        public String Sb_Wt { get; set; }
        public String Ba_Wt { get; set; }
        public String W_Wt { get; set; }
        public String Au_Wt { get; set; }
        public String Pb_Wt { get; set; }
        public String Ra_Wt { get; set; }
        public String Np_Wt { get; set; }

    }
}
