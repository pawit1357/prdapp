﻿using ALS.ALIS.Repository.Interface;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ALS.ALSI.Biz.DataAccess
{
    public partial class template_42_detail_spec : IDefaultDao<template_42_detail_spec>
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(template_42_detail_spec));

        private static IRepository<template_42_detail_spec> _repository
        {
            get { return ObjectFactory.GetInstance<IRepository<template_42_detail_spec>>(); }
        }

        #region "Property"

        #endregion


        public IEnumerable<template_42_detail_spec> SelectAll()
        {
            return _repository.GetAll().ToList();
        }

        public template_42_detail_spec SelectByID(int _id)
        {
            return _repository.First(x => x.ID == _id);
        }

        public void Insert()
        {
            _repository.Add(this);
        }

        public void Update()
        {
            template_42_detail_spec existing = _repository.Find(x => x.ID == this.ID).FirstOrDefault();
            _repository.Edit(existing, this);
        }

        public void Delete()
        {
            _repository.Delete(this);
        }

        #region "Custom"

        public void InsertList(List<template_42_detail_spec> _lists)
        {
            foreach (template_42_detail_spec tmp in _lists)
            {
                _repository.Add(tmp);
            }
        }

        public void EmptyDB()
        {
            List<template_42_detail_spec> lists = _repository.Find(x => x.ID > 0).ToList();
            foreach (template_42_detail_spec tmp in lists)
            {
                _repository.Delete(tmp);
            }
        }

        public IEnumerable SearchData()
        {
            using (ALSIEntities ctx = new ALSIEntities())
            {
                var result = from j in ctx.template_42_detail_spec select j;

                if (this.ID > 0)
                {
                    result = result.Where(x => x.ID == this.ID);
                }
                //if (!String.IsNullOrEmpty(this.name))
                //{
                //    result = result.Where(x => x.name == this.name);
                //}
                return result.ToList();
            }
        }

        #endregion
    }
}
