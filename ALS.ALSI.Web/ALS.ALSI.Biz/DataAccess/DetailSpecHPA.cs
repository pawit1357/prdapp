﻿using ALS.ALIS.Repository.Interface;
using ALS.ALSI.Biz.Constant;
using StructureMap;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ALS.ALSI.Biz.DataAccess
{

    public partial class tb_m_hpa_specification : IDefaultDao<tb_m_hpa_specification>
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(tb_m_hpa_specification));

        private static IRepository<tb_m_hpa_specification> _repository
        {
            get { return ObjectFactory.GetInstance<IRepository<tb_m_hpa_specification>>(); }
        }

        #region "Property"
        public CommandNameEnum RowState { get; set; }
        #endregion


        public IEnumerable<tb_m_hpa_specification> SelectAll()
        {
            return _repository.GetAll().ToList();
        }

        public tb_m_hpa_specification SelectByID(int _id)
        {
            return _repository.First(x => x.ID == _id);
        }

        public void Insert()
        {
            _repository.Add(this);
        }

        public void Update()
        {
            tb_m_hpa_specification existing = _repository.Find(x => x.ID == this.ID).FirstOrDefault();
            _repository.Edit(existing, this);
        }

        public void Delete()
        {
            _repository.Delete(this);
        }

        #region "Custom"

        public void InsertList(List<tb_m_hpa_specification> _lists)
        {
            foreach (tb_m_hpa_specification tmp in _lists)
            {
                switch (tmp.RowState)
                {
                    case CommandNameEnum.Add:
                        _repository.Add(tmp);
                        break;
                    case CommandNameEnum.Edit:
                        tb_m_hpa_specification existing = _repository.Find(x => x.ID == tmp.ID).FirstOrDefault();
                        _repository.Edit(existing, tmp);
                        break;
                }

            }
        }


        public List<tb_m_hpa_specification> SelectBySpecificationID(int _specification_id)
        {
            return _repository.Find(x => x.specification_id == _specification_id).ToList();
        }
        

        public void DeleteBySpecificationID(int _specification_id)
        {
            List<tb_m_hpa_specification> lists = _repository.Find(x => x.specification_id == _specification_id).ToList();
            foreach (tb_m_hpa_specification tmp in lists)
            {
                _repository.Delete(tmp);
            }
        }
        public IEnumerable SearchData()
        {
            using (ALSIEntities ctx = new ALSIEntities())
            {
                var result = from j in ctx.tb_m_hpa_specification select j;

                //if (this.ID > 0)
                //{
                //    result = result.Where(x => x.ID == this.ID);
                //}
                //if (!String.IsNullOrEmpty(this.name))
                //{
                //    result = result.Where(x => x.name == this.name);
                //}
                return result.ToList();
            }
        }

        #endregion
    }
}
