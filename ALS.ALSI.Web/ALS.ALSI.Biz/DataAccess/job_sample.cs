//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALS.ALSI.Biz.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class job_sample
    {
        public job_sample()
        {
            this.job_sample_logs = new HashSet<job_sample_logs>();
            this.template_16_coverpage = new HashSet<template_16_coverpage>();
            this.template_seagate_ic_coverpage = new HashSet<template_seagate_ic_coverpage>();
            this.template_wd_ic_coverpage = new HashSet<template_wd_ic_coverpage>();
            this.template_42_coverpage = new HashSet<template_42_coverpage>();
            this.template_wd_hpa_for1_coverpage = new HashSet<template_wd_hpa_for1_coverpage>();
            this.template_wd_hpa_for3_coverpage = new HashSet<template_wd_hpa_for3_coverpage>();
            this.template_seagate_lpc_coverpage = new HashSet<template_seagate_lpc_coverpage>();
            this.template_seagate_hpa_coverpage = new HashSet<template_seagate_hpa_coverpage>();
            this.template_wd_lpc_coverpage = new HashSet<template_wd_lpc_coverpage>();
        }
    
        public int ID { get; set; }
        public int job_id { get; set; }
        public int specification_id { get; set; }
        public int type_of_test_id { get; set; }
        public int template_id { get; set; }
        public string job_number { get; set; }
        public string description { get; set; }
        public string model { get; set; }
        public string surface_area { get; set; }
        public string remarks { get; set; }
        public Nullable<int> no_of_report { get; set; }
        public string uncertainty { get; set; }
        public Nullable<int> job_status { get; set; }
        public Nullable<int> job_role { get; set; }
        public Nullable<System.DateTime> due_date { get; set; }
        public string path_word { get; set; }
        public string path_pdf { get; set; }
        public Nullable<int> status_completion_scheduled { get; set; }
        public Nullable<int> step1owner { get; set; }
        public Nullable<int> step2owner { get; set; }
        public Nullable<int> step3owner { get; set; }
        public Nullable<int> step4owner { get; set; }
        public Nullable<int> step5owner { get; set; }
        public Nullable<int> step6owner { get; set; }
        public string internal_reference_remark { get; set; }
    
        public virtual ICollection<job_sample_logs> job_sample_logs { get; set; }
        public virtual ICollection<template_16_coverpage> template_16_coverpage { get; set; }
        public virtual ICollection<template_seagate_ic_coverpage> template_seagate_ic_coverpage { get; set; }
        public virtual ICollection<template_wd_ic_coverpage> template_wd_ic_coverpage { get; set; }
        public virtual ICollection<template_42_coverpage> template_42_coverpage { get; set; }
        public virtual ICollection<template_wd_hpa_for1_coverpage> template_wd_hpa_for1_coverpage { get; set; }
        public virtual ICollection<template_wd_hpa_for3_coverpage> template_wd_hpa_for3_coverpage { get; set; }
        public virtual ICollection<template_seagate_lpc_coverpage> template_seagate_lpc_coverpage { get; set; }
        public virtual ICollection<template_seagate_hpa_coverpage> template_seagate_hpa_coverpage { get; set; }
        public virtual ICollection<template_wd_lpc_coverpage> template_wd_lpc_coverpage { get; set; }
        public virtual m_specification m_specification { get; set; }
        public virtual m_template m_template { get; set; }
        public virtual m_type_of_test m_type_of_test { get; set; }
    }
}
