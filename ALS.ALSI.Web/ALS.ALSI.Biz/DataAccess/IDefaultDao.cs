﻿using System.Collections.Generic;
namespace ALS.ALSI.Biz.DataAccess
{
    public interface IDefaultDao<T> where T : class
    {
        IEnumerable<T> SelectAll();
        T SelectByID(int id);
        void Insert();
        void Update();
        void Delete();
    }
}
