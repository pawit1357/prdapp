//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALS.ALSI.Biz.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class template_42_coverpage
    {
        public int ID { get; set; }
        public Nullable<int> sample_id { get; set; }
        public Nullable<int> detail_spec_id { get; set; }
        public string analytes { get; set; }
        public string specification_limits { get; set; }
        public string result { get; set; }
        public string result_pass_or_false { get; set; }
    
        public virtual job_sample job_sample { get; set; }
        public virtual template_42_detail_spec template_42_detail_spec { get; set; }
    }
}
