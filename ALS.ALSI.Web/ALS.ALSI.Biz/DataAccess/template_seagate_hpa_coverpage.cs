//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALS.ALSI.Biz.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class template_seagate_hpa_coverpage
    {
        public int ID { get; set; }
        public Nullable<int> sample_id { get; set; }
        public Nullable<int> detail_spec_id { get; set; }
        public string lpc_type { get; set; }
        public string particle_type { get; set; }
        public string cvp_e19 { get; set; }
        public string cvp_c19 { get; set; }
        public string cvp_e20 { get; set; }
        public string cvp_c20 { get; set; }
        public string ws_b3 { get; set; }
        public string ws_b4 { get; set; }
        public string ws_b5 { get; set; }
        public string ws_b6 { get; set; }
        public string ws_b7 { get; set; }
        public string ws_b8 { get; set; }
        public string ws_b9 { get; set; }
        public string ws_b13 { get; set; }
        public string ws_b14 { get; set; }
        public string ws_b15 { get; set; }
        public string ws_b16 { get; set; }
        public string ws_b17 { get; set; }
        public string ws_b18 { get; set; }
        public string ws_b19 { get; set; }
        public string ws_b20 { get; set; }
        public string ws_b21 { get; set; }
        public string ws_b22 { get; set; }
        public string ws_b23 { get; set; }
        public string ws_b24 { get; set; }
        public string ws_b25 { get; set; }
        public string ws_b26 { get; set; }
        public string ws_b27 { get; set; }
        public string ws_b28 { get; set; }
        public string ws_b29 { get; set; }
        public string ws_b30 { get; set; }
        public string ws_b31 { get; set; }
        public string ws_b32 { get; set; }
        public string ws_b33 { get; set; }
        public string ws_b34 { get; set; }
        public string ws_b35 { get; set; }
        public string ws_b36 { get; set; }
        public string ws_b37 { get; set; }
        public string ws_b38 { get; set; }
        public string ws_b39 { get; set; }
        public string ws_b40 { get; set; }
        public string ws_b41 { get; set; }
        public string ws_b42 { get; set; }
        public string ws_b43 { get; set; }
        public string ws_b44 { get; set; }
        public string ws_b45 { get; set; }
        public string ws_b46 { get; set; }
        public string ws_b47 { get; set; }
        public string ws_b48 { get; set; }
        public string ws_b49 { get; set; }
        public string ws_b50 { get; set; }
        public string ws_b51 { get; set; }
        public string ws_b52 { get; set; }
        public string ws_b53 { get; set; }
        public string ws_b54 { get; set; }
        public string ws_b55 { get; set; }
        public string ws_b56 { get; set; }
        public string ws_b57 { get; set; }
        public string ws_b58 { get; set; }
        public string ws_b59 { get; set; }
        public string ws_b60 { get; set; }
        public string ws_b61 { get; set; }
        public string ws_b62 { get; set; }
        public string ws_b63 { get; set; }
        public string ws_b64 { get; set; }
        public string ws_b66 { get; set; }
        public string ws_b65 { get; set; }
        public string ws_b67 { get; set; }
        public string ws_b68 { get; set; }
        public string ws_b69 { get; set; }
        public string ws_b70 { get; set; }
        public string ws_b71 { get; set; }
        public string ws_b72 { get; set; }
        public string ws_b73 { get; set; }
        public string ws_b74 { get; set; }
        public string ws_b75 { get; set; }
        public string ws_b76 { get; set; }
        public string ws_c13 { get; set; }
        public string ws_c14 { get; set; }
        public string ws_c15 { get; set; }
        public string ws_c16 { get; set; }
        public string ws_c17 { get; set; }
        public string ws_c18 { get; set; }
        public string ws_c19 { get; set; }
        public string ws_c20 { get; set; }
        public string ws_c21 { get; set; }
        public string ws_c22 { get; set; }
        public string ws_c23 { get; set; }
        public string ws_c24 { get; set; }
        public string ws_c25 { get; set; }
        public string ws_c26 { get; set; }
        public string ws_c27 { get; set; }
        public string ws_c28 { get; set; }
        public string ws_c29 { get; set; }
        public string ws_c30 { get; set; }
        public string ws_c31 { get; set; }
        public string ws_c32 { get; set; }
        public string ws_c33 { get; set; }
        public string ws_c34 { get; set; }
        public string ws_c35 { get; set; }
        public string ws_c36 { get; set; }
        public string ws_c37 { get; set; }
        public string ws_c38 { get; set; }
        public string ws_c39 { get; set; }
        public string ws_c40 { get; set; }
        public string ws_c41 { get; set; }
        public string ws_c42 { get; set; }
        public string ws_c43 { get; set; }
        public string ws_c44 { get; set; }
        public string ws_c45 { get; set; }
        public string ws_c46 { get; set; }
        public string ws_c47 { get; set; }
        public string ws_c48 { get; set; }
        public string ws_c49 { get; set; }
        public string ws_c50 { get; set; }
        public string ws_c51 { get; set; }
        public string ws_c52 { get; set; }
        public string ws_c53 { get; set; }
        public string ws_c54 { get; set; }
        public string ws_c55 { get; set; }
        public string ws_c56 { get; set; }
        public string ws_c57 { get; set; }
        public string ws_c58 { get; set; }
        public string ws_c59 { get; set; }
        public string ws_c60 { get; set; }
        public string ws_c61 { get; set; }
        public string ws_c62 { get; set; }
        public string ws_c63 { get; set; }
        public string ws_c64 { get; set; }
        public string ws_c66 { get; set; }
        public string ws_c65 { get; set; }
        public string ws_c67 { get; set; }
        public string ws_c68 { get; set; }
        public string ws_c69 { get; set; }
        public string ws_c70 { get; set; }
        public string ws_c71 { get; set; }
        public string ws_c72 { get; set; }
        public string ws_c73 { get; set; }
        public string ws_c74 { get; set; }
        public string ws_c75 { get; set; }
        public string ws_c76 { get; set; }
        public string ws_d13 { get; set; }
        public string ws_d14 { get; set; }
        public string ws_d15 { get; set; }
        public string ws_d16 { get; set; }
        public string ws_d17 { get; set; }
        public string ws_d18 { get; set; }
        public string ws_d19 { get; set; }
        public string ws_d20 { get; set; }
        public string ws_d21 { get; set; }
        public string ws_d22 { get; set; }
        public string ws_d23 { get; set; }
        public string ws_d24 { get; set; }
        public string ws_d25 { get; set; }
        public string ws_d26 { get; set; }
        public string ws_d27 { get; set; }
        public string ws_d28 { get; set; }
        public string ws_d29 { get; set; }
        public string ws_d30 { get; set; }
        public string ws_d31 { get; set; }
        public string ws_d32 { get; set; }
        public string ws_d33 { get; set; }
        public string ws_d34 { get; set; }
        public string ws_d35 { get; set; }
        public string ws_d36 { get; set; }
        public string ws_d37 { get; set; }
        public string ws_d38 { get; set; }
        public string ws_d39 { get; set; }
        public string ws_d40 { get; set; }
        public string ws_d41 { get; set; }
        public string ws_d42 { get; set; }
        public string ws_d43 { get; set; }
        public string ws_d44 { get; set; }
        public string ws_d45 { get; set; }
        public string ws_d46 { get; set; }
        public string ws_d47 { get; set; }
        public string ws_d48 { get; set; }
        public string ws_d49 { get; set; }
        public string ws_d50 { get; set; }
        public string ws_d51 { get; set; }
        public string ws_d52 { get; set; }
        public string ws_d53 { get; set; }
        public string ws_d54 { get; set; }
        public string ws_d55 { get; set; }
        public string ws_d56 { get; set; }
        public string ws_d57 { get; set; }
        public string ws_d58 { get; set; }
        public string ws_d59 { get; set; }
        public string ws_d60 { get; set; }
        public string ws_d61 { get; set; }
        public string ws_d62 { get; set; }
        public string ws_d63 { get; set; }
        public string ws_d64 { get; set; }
        public string ws_d66 { get; set; }
        public string ws_d65 { get; set; }
        public string ws_d67 { get; set; }
        public string ws_d68 { get; set; }
        public string ws_d69 { get; set; }
        public string ws_d70 { get; set; }
        public string ws_d71 { get; set; }
        public string ws_d72 { get; set; }
        public string ws_d73 { get; set; }
        public string ws_d74 { get; set; }
        public string ws_d75 { get; set; }
        public string ws_d76 { get; set; }
        public string us_b14 { get; set; }
        public string us_b15 { get; set; }
        public string us_b16 { get; set; }
        public string us_b17 { get; set; }
        public string us_c14 { get; set; }
        public string us_c15 { get; set; }
        public string us_c16 { get; set; }
        public string us_c17 { get; set; }
        public string us_d14 { get; set; }
        public string us_d15 { get; set; }
        public string us_d16 { get; set; }
        public string us_d17 { get; set; }
        public string us_e14 { get; set; }
        public string us_e15 { get; set; }
        public string us_e16 { get; set; }
        public string us_e17 { get; set; }
        public string us_f14 { get; set; }
        public string us_f15 { get; set; }
        public string us_f16 { get; set; }
        public string us_f17 { get; set; }
        public string us_g14 { get; set; }
        public string us_g15 { get; set; }
        public string us_g16 { get; set; }
        public string us_g17 { get; set; }
        public string us_b25 { get; set; }
        public string us_d25 { get; set; }
        public string us_f25 { get; set; }
        public string item_visible { get; set; }
    
        public virtual job_sample job_sample { get; set; }
        public virtual tb_m_detail_spec_lpc tb_m_detail_spec_lpc { get; set; }
    }
}
