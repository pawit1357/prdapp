﻿using ALS.ALIS.Repository.Interface;
using ALS.ALSI.Biz.Constant;
using StructureMap;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ALS.ALSI.Biz.DataAccess
{

    public partial class tb_detailspec_hpa_for3 : IDefaultDao<tb_detailspec_hpa_for3>
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(tb_detailspec_hpa_for3));

        private static IRepository<tb_detailspec_hpa_for3> _repository
        {
            get { return ObjectFactory.GetInstance<IRepository<tb_detailspec_hpa_for3>>(); }
        }

        #region "Property"
        public CommandNameEnum RowState { get; set; }
        #endregion


        public IEnumerable<tb_detailspec_hpa_for3> SelectAll()
        {
            return _repository.GetAll().ToList();
        }

        public tb_detailspec_hpa_for3 SelectByID(int _id)
        {
            return _repository.First(x => x.ID == _id);
        }

        public void Insert()
        {
            _repository.Add(this);
        }

        public void Update()
        {
            tb_detailspec_hpa_for3 existing = _repository.Find(x => x.ID == this.ID).FirstOrDefault();
            _repository.Edit(existing, this);
        }

        public void Delete()
        {
            _repository.Delete(this);
        }

        #region "Custom"

        public void InsertList(List<tb_detailspec_hpa_for3> _lists)
        {
            foreach (tb_detailspec_hpa_for3 tmp in _lists)
            {
                switch (tmp.RowState)
                {
                    case CommandNameEnum.Add:
                        _repository.Add(tmp);
                        break;
                    case CommandNameEnum.Edit:
                        tb_detailspec_hpa_for3 existing = _repository.Find(x => x.ID == tmp.ID).FirstOrDefault();
                        _repository.Edit(existing, tmp);
                        break;
                }

            }
        }


        public List<tb_detailspec_hpa_for3> SelectBySpecificationID(int _specification_id)
        {
            return _repository.Find(x => x.specification_id == _specification_id).ToList();
        }
        

        public void DeleteBySpecificationID(int _specification_id)
        {
            List<tb_detailspec_hpa_for3> lists = _repository.Find(x => x.specification_id == _specification_id).ToList();
            foreach (tb_detailspec_hpa_for3 tmp in lists)
            {
                _repository.Delete(tmp);
            }
        }
        public IEnumerable SearchData()
        {
            using (ALSIEntities ctx = new ALSIEntities())
            {
                var result = from j in ctx.tb_detailspec_hpa_for3 select j;

                //if (this.ID > 0)
                //{
                //    result = result.Where(x => x.ID == this.ID);
                //}
                //if (!String.IsNullOrEmpty(this.name))
                //{
                //    result = result.Where(x => x.name == this.name);
                //}
                return result.ToList();
            }
        }

        #endregion
    }
}
