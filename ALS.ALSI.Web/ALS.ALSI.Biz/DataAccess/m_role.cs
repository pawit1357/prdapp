//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALS.ALSI.Biz.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class m_role
    {
        public m_role()
        {
            this.user_login = new HashSet<user_login>();
            this.m_role_permission = new HashSet<m_role_permission>();
            this.m_status = new HashSet<m_status>();
        }
    
        public int ID { get; set; }
        public string name { get; set; }
        public string status { get; set; }
    
        public virtual ICollection<user_login> user_login { get; set; }
        public virtual ICollection<m_role_permission> m_role_permission { get; set; }
        public virtual ICollection<m_status> m_status { get; set; }
    }
}
