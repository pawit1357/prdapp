﻿using ALS.ALIS.Repository.Interface;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ALS.ALSI.Biz.DataAccess
{
    public partial class user_login : IDefaultDao<user_login>
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(user_login));

        private static IRepository<user_login> _repository
        {
            get { return ObjectFactory.GetInstance<IRepository<user_login>>(); }
        }

        #region "Property"

        #endregion


        public IEnumerable<user_login> SelectAll()
        {
            return _repository.GetAll().ToList();
        }

        public user_login SelectByID(int _id)
        {
            return _repository.First(x => x.id == _id);
        }

        public void Insert()
        {
            _repository.Add(this);
        }

        public void Update()
        {
            user_login existing = _repository.Find(x => x.id == this.id).FirstOrDefault();
            _repository.Edit(existing, this);
        }

        public void Delete()
        {
            _repository.Delete(this);
        }

        #region "Custom"

        public user_login Login()
        {
            user_login existing = _repository.Find(x => x.username == this.username && x.password == this.password).FirstOrDefault();
            return existing;
        }
        public user_login SelectByEmail(String _email)
        {

            user_login existing = _repository.Find(x => x.email == this.email).FirstOrDefault();
            return existing;

        }
        public IEnumerable SearchData()
        {
            using (ALSIEntities ctx = new ALSIEntities())
            {

                var result = from u in ctx.user_login
                             //join p in ctx.user_profile on u.id equals p.user_login_id
                             join r in ctx.m_role on u.role_id equals r.ID
                             select new
                             {
                                 id = u.id,
                                 role = r.name,
                                 username = u.username,
                                 email = u.email,
                                 latest_login = u.latest_login,
                                 create_date = u.create_date,
                                 status = u.status,
                                 phone = u.mobile_phone
                             };
                if (!String.IsNullOrEmpty(this.username))
                {
                    result = result.Where(x => x.username == this.username);
                }

                //if (!String.IsNullOrEmpty(this.userProfile.phone))
                //{
                //    result = result.Where(x => x.phone == this.userProfile.phone);
                //}

                return result.ToList();
            }
        }

        #endregion
    }
}
