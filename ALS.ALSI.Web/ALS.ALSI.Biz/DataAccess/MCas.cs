﻿using ALS.ALIS.Repository.Interface;
using ALS.ALSI.Biz.Constant;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ALS.ALSI.Biz.DataAccess
{
    public partial class tb_cas
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(tb_cas));

        #region "Property"
        public String SelectedText { get { return classification + (String.IsNullOrEmpty(library_id) ? String.Empty : " ( " + library_id + " )"); } }
        public String ref_ { get; set; }
        public CommandNameEnum RowState { get; set; }
        #endregion

        private static IRepository<tb_cas> _repository
        {
            get { return ObjectFactory.GetInstance<IRepository<tb_cas>>(); }
        }

        #region "Custom"

        public IEnumerable<tb_cas> SelectAll()
        {
            return _repository.GetAll().ToList();
        }

        public tb_cas SelectByID(int _id)
        {
            return _repository.Find(x => x.ID == _id).FirstOrDefault();
        }

        public void Insert()
        {
            _repository.Add(this);
        }

        public void Update()
        {
            tb_cas existing = _repository.Find(x => x.ID == this.ID).FirstOrDefault();
            _repository.Edit(existing, this);
        }

        public void Delete()
        {
            _repository.Delete(this);
        }

        public static void DeleteBySampleID(int _sampleID)
        {
            List<tb_cas> lists = _repository.Find(x => x.sample_id == _sampleID).ToList();
            foreach (tb_cas tmp in lists)
            {
                _repository.Delete(tmp);
            }
        }

        public static void InsertList(List<tb_cas> _lists)
        {
            foreach (tb_cas tmp in _lists)
            {

                _repository.Add(tmp);

            }
        }
        public static void UpdateList(List<tb_cas> _lists)
        {
            foreach (tb_cas tmp in _lists)
            {
                tmp.Update();
            }
        }
        public static List<tb_cas> FindAllBySampleID(int _sampleID)
        {
            return _repository.GetAll().Where(x => x.sample_id == _sampleID).ToList();
        }

        #endregion

    }
}
