﻿using ALS.ALIS.Repository.Interface;
using ALS.ALSI.Biz.Constant;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ALS.ALSI.Biz.DataAccess
{
    public partial class job_info : IDefaultDao<job_info>
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(job_info));

        private static IRepository<job_info> _repository
        {
            get { return ObjectFactory.GetInstance<IRepository<job_info>>(); }
        }

        #region "Property"

        public String[] responsible_test { get; set; }
        public List<job_sample> jobSample { get; set; }
        public CommandNameEnum RowState { get; set; }
        public int sample_id { get; set; }

        #endregion


        public IEnumerable<job_info> SelectAll()
        {
            return _repository.GetAll().ToList();
        }

        public job_info SelectByID(int _id)
        {
            return _repository.First(x => x.ID == _id);
        }

        public void Insert()
        {
            _repository.Add(this);
            GeneralManager.Commit();

            job_running.IncrementRunning(this.job_prefix);

            foreach (job_sample sample in this.jobSample)
            {
                sample.job_id = this.ID;

                switch (sample.RowState)
                {
                    case CommandNameEnum.Add:
                        sample.Insert();
                        break;
                    case CommandNameEnum.Edit:
                        sample.Update();
                        break;
                    case CommandNameEnum.Delete:
                        sample.Delete();
                        break;
                }
            }
        }

        public void Update()
        {
            job_info existing = _repository.Find(x => x.ID == this.ID).FirstOrDefault();
            _repository.Edit(existing, this);
            if(this.jobSample !=null){
                foreach (job_sample sample in this.jobSample)
                {
                    sample.job_id = this.ID;

                    switch (sample.RowState)
                    {
                        case CommandNameEnum.Add:
                            sample.Insert();
                            break;
                        case CommandNameEnum.Edit:
                            sample.Update();
                            break;
                        case CommandNameEnum.Delete:
                            sample.Delete();
                            break;
                    }
                }
            }
        }

        public void Delete()
        {
            _repository.Delete(this);
        }

        #region "Custom"
        public IEnumerable SearchData()
        {
            using (ALSIEntities ctx = new ALSIEntities())
            {
                var result = from j in ctx.job_info
                             join s in ctx.job_sample on j.ID equals s.job_id
                             join sp in ctx.m_specification on s.specification_id equals sp.ID
                             join tt in ctx.m_type_of_test on s.type_of_test_id equals tt.ID
                             join c in ctx.m_customer on j.customer_id equals c.ID
                             join cp in ctx.m_customer_contract_person on j.contract_person_id equals cp.ID
                             orderby s.due_date ascending
                             select new
                             {
                                 ID = j.ID,
                                 job_number = s.job_number,
                                 create_date = j.create_date,
                                 customer_ref_no = j.customer_ref_no,
                                 s_pore_ref_no = j.s_pore_ref_no,
                                 //customer_po_ref = s.customer_po_ref,
                                 customer = c.company_name,
                                 contract_person = cp.name,
                                 sn = s.ID,
                                 description = s.description,
                                 model = s.model,
                                 surface_area = s.surface_area,
                                 remarks = s.remarks,
                                 specification = sp.name,
                                 type_of_test = tt.name,
                                 receive_date = j.date_of_receive,
                                 customer_id = c.ID,
                                 contract_person_id = cp.ID,
                                 job_status = s.job_status,
                                 job_role = s.job_role,
                                 due_date = s.due_date,
                                 status_completion_scheduled = s.status_completion_scheduled,
                                 s.step1owner,
                                 s.step2owner,
                                 s.step3owner,
                                 s.step4owner,
                                 s.step5owner,
                                 s.step6owner,
                                 j.job_prefix
                             };

                if (this.ID > 0)
                {
                    result = result.Where(x => x.ID == this.ID);
                }
                if (this.sample_id > 0)
                {
                    result = result.Where(x => x.sn == this.sample_id);
                }

                if (this.job_prefix > 0)
                {
                    result = result.Where(x => x.job_prefix == this.job_prefix);
                }

                //if (this.date_of_receive != null && this.date_of_receive !=DateTime.MinValue)
                //{
                //    result = result.Where(x => x.receive_date == this.date_of_receive);
                //}
                if (this.customer_id > 0)
                {
                    result = result.Where(x => x.customer_id == this.customer_id);
                }
                if (this.contract_person_id > 0)
                {
                    result = result.Where(x => x.contract_person_id == this.contract_person_id);
                }

                //show type of test by user.
                if (responsible_test != null && responsible_test.Length > 0)
                {
                    result = result.Where(x => responsible_test.Contains(x.type_of_test));
                }

                return result.ToList();
            }
        }


        #endregion

        //private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(job_info));

        //public List<job_sample> jobSample { get; set; }

        //private ALSIEntities ctx = new ALSIEntities();
        //public int sample_id { get; set; }
        //public String[] responsible_test { get; set; }

        //public CommandNameEnum RowState { get; set; }

        //public IEnumerable<job_info> SelectAll()
        //{

        //    return (from c in ctx.job_info select c).ToList();

        //}

        //public job_info SelectByID(int id)
        //{

        //    var result = from j in ctx.job_info
        //                 where j.ID == id
        //                 select j;
        //    return result.FirstOrDefault();

        //}

        //public void Insert()
        //{

        //    //try
        //    //{
        //    //    ctx.job_info.Add(this);
        //    //    ctx.SaveChanges();
        //    //    using (job_running running = new job_running())
        //    //    {
        //    //        running.IncrementRunning(this.job_prefix);
        //    //    }

        //    //    foreach (job_sample sample in this.jobSample)
        //    //    {
        //    //        switch (sample.RowState)
        //    //        {
        //    //            case CommandNameEnum.Add:
        //    //                sample.job_id = this.ID;
        //    //                sample.Insert();
        //    //                break;
        //    //            case CommandNameEnum.Edit:
        //    //                if (sample.SelectByID(sample.ID) != null)
        //    //                {
        //    //                    sample.Update();
        //    //                }
        //    //                else
        //    //                {
        //    //                    sample.Insert();
        //    //                }
        //    //                break;
        //    //            case CommandNameEnum.Delete:
        //    //                if (sample.SelectByID(sample.ID) != null)
        //    //                {
        //    //                    sample.Delete();
        //    //                }
        //    //                break;
        //    //        }
        //    //    }
        //    //}
        //    //catch (DbEntityValidationException e)
        //    //{
        //    //    foreach (var eve in e.EntityValidationErrors)
        //    //    {

        //    //        logger.Error(String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
        //    //             eve.Entry.Entity.GetType().Name, eve.Entry.State));
        //    //        foreach (var ve in eve.ValidationErrors)
        //    //        {
        //    //            logger.Error(String.Format("- Property: \"{0}\", Error: \"{1}\"",
        //    //                ve.PropertyName, ve.ErrorMessage));
        //    //        }
        //    //    }
        //    //    throw;
        //    //}


        //}

        //public void Update()
        //{

        //    //try
        //    //{
        //    //    var result = from j in ctx.job_info where j.ID == this.ID select j;
        //    //    job_info job = result.FirstOrDefault();
        //    //    if (job != null)
        //    //    {
        //    //        setEditData(job);
        //    //    }
        //    //    ctx.SaveChanges();
        //    //    if (this.jobSample != null)
        //    //    {
        //    //        foreach (job_sample sample in this.jobSample)
        //    //        {
        //    //            switch (sample.RowState)
        //    //            {
        //    //                case CommandNameEnum.Add:
        //    //                    sample.job_id = this.ID;
        //    //                    sample.Insert();
        //    //                    break;
        //    //                case CommandNameEnum.Edit:
        //    //                    if (sample.SelectByID(sample.ID) != null)
        //    //                    {
        //    //                        sample.Update();
        //    //                    }
        //    //                    else
        //    //                    {
        //    //                        sample.Insert();
        //    //                    }
        //    //                    break;
        //    //                case CommandNameEnum.Delete:
        //    //                    if (sample.SelectByID(sample.ID) != null)
        //    //                    {
        //    //                        sample.Delete();
        //    //                    }
        //    //                    break;
        //    //            }
        //    //        }
        //    //    }
        //    //}
        //    //catch (DbEntityValidationException e)
        //    //{
        //    //    foreach (var eve in e.EntityValidationErrors)
        //    //    {

        //    //        logger.Error(String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
        //    //             eve.Entry.Entity.GetType().Name, eve.Entry.State));
        //    //        foreach (var ve in eve.ValidationErrors)
        //    //        {
        //    //            logger.Error(String.Format("- Property: \"{0}\", Error: \"{1}\"",
        //    //                ve.PropertyName, ve.ErrorMessage));
        //    //        }
        //    //    }
        //    //    throw;
        //    //}

        //}

        //public void Delete()
        //{
        //    throw new NotImplementedException();
        //}


        //#region "Custom"
        //private void setEditData(job_info _jobInfo)
        //{
        //    _jobInfo.ID = (this.ID > 0) ? this.ID : _jobInfo.ID;
        //    _jobInfo.contract_person_id = (this.contract_person_id > 0) ? this.contract_person_id : _jobInfo.contract_person_id;
        //    _jobInfo.customer_id = (this.customer_id > 0) ? this.customer_id : _jobInfo.customer_id;
        //    _jobInfo.date_of_request = Convert.ToDateTime(this.date_of_request);
        //    _jobInfo.job_invoice = !String.IsNullOrEmpty(this.job_invoice) ? this.job_invoice : _jobInfo.job_invoice;
        //    _jobInfo.customer_ref_no = !String.IsNullOrEmpty(this.customer_ref_no) ? this.customer_ref_no : _jobInfo.customer_ref_no;
        //    _jobInfo.company_name_to_state_in_report = !String.IsNullOrEmpty(this.company_name_to_state_in_report) ? this.company_name_to_state_in_report : _jobInfo.company_name_to_state_in_report;
        //    _jobInfo.job_number = (this.job_number > 0) ? this.job_number : _jobInfo.job_number;
        //    _jobInfo.date_of_receive = (this.date_of_receive != null) ? this.date_of_receive : _jobInfo.date_of_receive;
        //    _jobInfo.s_pore_ref_no = !String.IsNullOrEmpty(this.s_pore_ref_no) ? this.s_pore_ref_no : _jobInfo.s_pore_ref_no;
        //    _jobInfo.spec_ref_rev_no = !String.IsNullOrEmpty(this.spec_ref_rev_no) ? this.spec_ref_rev_no : _jobInfo.spec_ref_rev_no;
        //    _jobInfo.customer_po_ref = !String.IsNullOrEmpty(this.customer_po_ref) ? this.customer_po_ref : _jobInfo.customer_po_ref;
        //    _jobInfo.sample_diposition = !String.IsNullOrEmpty(this.sample_diposition) ? this.sample_diposition : _jobInfo.sample_diposition;
        //    _jobInfo.status_sample_enough = !String.IsNullOrEmpty(this.status_sample_enough) ? this.status_sample_enough : _jobInfo.status_sample_enough;
        //    _jobInfo.status_sample_full = !String.IsNullOrEmpty(this.status_sample_full) ? this.status_sample_full : _jobInfo.status_sample_full;
        //    _jobInfo.status_personel_and_workload = !String.IsNullOrEmpty(this.status_personel_and_workload) ? this.status_personel_and_workload : _jobInfo.status_personel_and_workload;
        //    _jobInfo.status_test_tool = !String.IsNullOrEmpty(this.status_test_tool) ? this.status_test_tool : _jobInfo.status_test_tool;
        //    _jobInfo.status_test_method = !String.IsNullOrEmpty(this.status_test_method) ? this.status_test_method : _jobInfo.status_test_method;
        //    _jobInfo.update_by = (Convert.ToInt32(this.update_by) > 0) ? this.update_by : _jobInfo.update_by;
        //    _jobInfo.update_date = (this.update_date != null) ? this.update_date : _jobInfo.update_date;
        //    _jobInfo.document_type = !String.IsNullOrEmpty(this.document_type) ? this.document_type : _jobInfo.document_type;
        //    //_jobInfo.jobSample = this.jobSample;
        //}

        //public IEnumerable SearchData()
        //{

        //    var result = from j in ctx.job_info
        //                 join s in ctx.job_sample on j.ID equals s.job_id
        //                 join sp in ctx.m_specification on s.specification_id equals sp.ID
        //                 join tt in ctx.m_type_of_test on s.type_of_test_id equals tt.ID
        //                 join c in ctx.m_customer on j.customer_id equals c.ID
        //                 join cp in ctx.m_customer_contract_person on j.contract_person_id equals cp.ID
        //                 orderby s.due_date ascending
        //                 select new
        //                 {
        //                     ID = j.ID,
        //                     job_number = s.job_number,
        //                     create_date = j.create_date,
        //                     customer_ref_no = j.customer_ref_no,
        //                     s_pore_ref_no = j.s_pore_ref_no,
        //                     //customer_po_ref = s.customer_po_ref,
        //                     customer = c.company_name,
        //                     contract_person = cp.name,
        //                     sn = s.ID,
        //                     description = s.description,
        //                     model = s.model,
        //                     surface_area = s.surface_area,
        //                     remarks = s.remarks,
        //                     specification = sp.name,
        //                     type_of_test = tt.name,
        //                     receive_date = j.date_of_receive,
        //                     customer_id = c.ID,
        //                     contract_person_id = cp.ID,
        //                     job_status = s.job_status,
        //                     job_role = s.job_role,
        //                     due_date = s.due_date,
        //                     status_completion_scheduled = s.status_completion_scheduled,
        //                     s.step1owner,
        //                     s.step2owner,
        //                     s.step3owner,
        //                     s.step4owner,
        //                     s.step5owner,
        //                     s.step6owner,
        //                     j.job_prefix
        //                 };

        //    if (this.ID > 0)
        //    {
        //        result = result.Where(x => x.ID == this.ID);
        //    }
        //    if (this.sample_id > 0)
        //    {
        //        result = result.Where(x => x.sn == this.sample_id);
        //    }

        //    if (this.job_prefix > 0)
        //    {
        //        result = result.Where(x => x.job_prefix == this.job_prefix);
        //    }

        //    //if (this.date_of_receive != null && this.date_of_receive !=DateTime.MinValue)
        //    //{
        //    //    result = result.Where(x => x.receive_date == this.date_of_receive);
        //    //}
        //    if (this.customer_id > 0)
        //    {
        //        result = result.Where(x => x.customer_id == this.customer_id);
        //    }
        //    if (this.contract_person_id > 0)
        //    {
        //        result = result.Where(x => x.contract_person_id == this.contract_person_id);
        //    }

        //    //show type of test by user.
        //    if (responsible_test != null && responsible_test.Length > 0)
        //    {
        //        result = result.Where(x => responsible_test.Contains(x.type_of_test));
        //    }

        //    return result.ToList();
        //}

        //#endregion

        //#region "IDisposable Support"
        //void IDisposable.Dispose()
        //{
        //    GC.SuppressFinalize(this);
        //}
        //#endregion
    }
}
