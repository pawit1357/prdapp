﻿using ALS.ALIS.Repository.Interface;
using StructureMap;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ALS.ALSI.Biz.DataAccess
{
    public partial class template_16_component : IDefaultDao<template_16_component>
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(template_16_component));

        private static IRepository<template_16_component> _repository
        {
            get { return ObjectFactory.GetInstance<IRepository<template_16_component>>(); }
        }

        #region "Property"

        #endregion


        public IEnumerable<template_16_component> SelectAll()
        {
            return _repository.GetAll().ToList();
        }

        public template_16_component SelectByID(int _id)
        {
            return _repository.First(x => x.ID == _id);
        }

        public void Insert()
        {
            _repository.Add(this);
        }

        public void Update()
        {
            template_16_component existing = _repository.Find(x => x.ID == this.ID).FirstOrDefault();
            _repository.Edit(existing, this);
        }

        public void Delete()
        {
            _repository.Delete(this);
        }

        #region "Custom"

        public void InsertList(List<template_16_component> _lists)
        {
            foreach (template_16_component tmp in _lists)
            {
                _repository.Add(tmp);
            }
        }

        public void EmptyDB()
        {
            List<template_16_component> lists = _repository.Find(x => x.ID > 0).ToList();
            foreach (template_16_component tmp in lists)
            {
                _repository.Delete(tmp);
            }
        }

        public IEnumerable SearchData()
        {
            using (ALSIEntities ctx = new ALSIEntities())
            {
                var result = from j in ctx.template_16_component select j;

                //if (this.ID > 0)
                //{
                //    result = result.Where(x => x.ID == this.ID);
                //}
                //if (!String.IsNullOrEmpty(this.name))
                //{
                //    result = result.Where(x => x.name == this.name);
                //}
                return result.ToList();
            }

        }

        #endregion
    }
}
