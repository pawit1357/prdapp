﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace ALS.ALSI.Biz.Constant
{
    public class Constants
    {

        public const String SUM_OF_CORRECTED_AREAS = "Sum of corrected areas:";
        public const String WD_M = "WD.M";
        public const String DHS = "DHS";

        public const String PREVIOUS_PATH = "PreviousPath";
        public const String COMMAND_NAME = "CommandName";
        public const String SESSION_USER = "UserData";
        public const String SORT_DIRECTION = "SortDirection";
        public const String PLEASE_SELECT = "Please Select";
        public const String TOTAL_RECORDS = "Total {0} Records";
        public const char CHAR_COMMA = ',';
        public const char CHAR_COLON = ':';
        public const String CHAR_DASH = "-";

        public const String BASE_TEMPLATE_PATH = "~/view/template/";

        public const String LINK_SEARCH_TEMPLATE = "~/view/template/SearchTemplate.aspx";
        public const String LINK_TEMPLATE = "~/view/template/Template.aspx";


        public const String LINK_JOB_WORK_FLOW = "~/view/request/JobWorkFlow.aspx";
        public const String LINK_JOB_CONVERT_TEMPLATE = "~/view/request/JobConvertTemplate.aspx";
        public const String LINK_JOB_REQUEST = "~/view/request/JobRequest.aspx";
        public const String LINK_SEARCH_JOB_REQUEST = "~/view/request/SearchJobRequest.aspx";
        public const String LINK_JOB_CHANGE_STATUS = "~/view/request/ChangeJobsStatus.aspx";
        public const String LINK_JOB_CHANGE_DUEDATE = "~/view/request/ChangeJobDueDate.aspx";
        public const String LINK_JOB_CHANGE_PO = "~/view/request/ChangeJobPo.aspx";
        public const String LINK_JOB_CHANGE_INVOICE = "~/view/request/ChangeJobInvoice.aspx";
        public const String LINK_JOB_PRINT_LABEL = "~/view/request/PrintSticker.aspx";
        public const String LINK_SEARCH_USER = "~/view/user/SearchUser.aspx";
        public const String LINK_USER = "~/view/user/User.aspx";

        public const String LINK_SEARCH_ROLE = "~/view/role/SearchRole.aspx";
        public const String LINK_ROLE = "~/view/role/Role.aspx";

        public const String LINK_SEARCH_PERMISSION = "~/view/permission/SearchPermission.aspx";
        public const String LINK_PERMISSION = "~/view/permission/Permission.aspx";

        public const String LINK_SEARCH_CUSTOMER = "~/view/customer/SearchCustomer.aspx";
        public const String LINK_CUSTOMER = "~/view/customer/Customer.aspx";

        public const String LINK_SEARCH_CONTRACT_PERSON = "~/view/customerContract/SearchContractPerson.aspx";
        public const String LINK_CONTRACT_PERSON = "~/view/customerContract/ContractPerson.aspx";

        public const String LINK_SEARCH_SPECIFICATION = "~/view/specification/SearchSpecification.aspx";
        public const String LINK_SPECIFICATION = "~/view/specification/Specification.aspx";

        public const String LINK_SEARCH_LIBRARY = "~/view/library/SearchLibrary.aspx";
        public const String LINK_LIBRARY = "~/view/library/Library.aspx";

        public const String LINK_SEARCH_TYPE_OF_TEST = "~/view/type_of_test/SearchTypeOfTest.aspx";
        public const String LINK_TYPE_OF_TEST = "~/view/type_of_test/TypeOfTest.aspx";


        public static string GetEnumDescription(Enum value)
        {
            // Get the Description attribute value for the enum value
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }

        public static string[] WD_DHS = {
                        "Total Acrylate and Methacrylate",
                        "Total Acrylate",
                        "Total Methacrylate",
                        "Total Siloxane",
                        "Total Silane",//new
                        "Total Unknown",
                        "Total Sulfur Compound",
                        "Total Phthalate",
                        "Total Phenol",
                        "Total Alcohol",//Change position
                        "Total Others",
                        "Total Initiator",
                        "Total Antioxidant",
                        "Total Hydrocarbon",
                        "Total Aromatic Hydrocarbon",
                        "Total Aliphatic Hydrocarbon",
                        "Hydrocarbon Hump",
                        "Total Outgassing",
                        //"Total Outgassing (no of arms: >= 4)",
                        //"Total Outgassing (no of arms: <= 3)",
                        //"Total Outgassing (no of arms: >= 4)"
        };

    }

    public enum CommandNameEnum : int
    {
        Add = 1,
        Edit = 2,
        Delete = 3,
        View = 4,
        Workflow = 5,
        Print = 6,
        Page = 7,
        ConvertTemplate = 8,
        ChangeStatus = 9,
        ChangeDueDate = 10,
        ChangePo = 11,
        ChangeInvoice = 12,
        ReUse = 13,
        Update = 14

    }

    public enum RoleEnum
    {
        ROOT = 1,
        LOGIN = 2,
        CHEMIST = 3,
        SR_CHEMIST = 4,
        ADMIN = 5,
        LABMANAGER = 6,
        ACCOUNT = 7
    }

    public enum ResultEnum
    {
        [Description("-")]
        DASH = 1,
        [Description("NA")]
        NA = 2,
        [Description("ND")]
        ND = 3,
        [Description("Not Detected")]
        NOT_DETECTED = 4,
        [Description("PASS")]
        PASS = 5,
        [Description("FAIL")]
        FAIL = 6,
    }

    public enum StatusEnum
    {
        [Description("CANCEL")]
        JOB_CANCEL = 1,
        [Description("HOLD")]
        JOB_HOLD = 2,
        [Description("COMPLETE")]
        JOB_COMPLETE = 3,


        [Description("SR_CHEMIST_CHECKING")]
        SR_CHEMIST_CHECKING = 4,
        [Description("APPROVE")]
        SR_CHEMIST_APPROVE = 5,
        [Description("DISAPPROVE")]
        SR_CHEMIST_DISAPPROVE = 6,



        [Description("APPROVE")]
        LABMANAGER_APPROVE = 7,
        [Description("DISAPPROVE")]
        LABMANAGER_DISAPPROVE = 8,
        [Description("LABMANAGER_CHECKING")]
        LABMANAGER_CHECKING = 9,

        [Description("CONVERT_TEMPLATE")]
        LOGIN_CONVERT_TEMPLATE = 10,
        [Description("SELECT_SPEC")]
        LOGIN_SELECT_SPEC = 11,

        [Description("CHEMIST_TESTING")]
        CHEMIST_TESTING = 12,


        [Description("CONVERT_WORD")]
        ADMIN_CONVERT_WORD = 13,
        [Description("CONVERT_PDF")]
        ADMIN_CONVERT_PDF = 14,


    }

    public enum CompletionScheduledEnum
    {
        NORMAL = 1,
        URGENT = 2,
        EXPRESS = 3
    }

    public enum SpecificationEnum
    {
        [Description("Seagate")]
        SEAGATE = 1,
        [Description("WD")]
        WD = 2
    }

    public enum LPCTypeEnum
    {
        [Description("68 KHz")]
        KHz_68 = 1,
        [Description("132 KHz")]
        KHz_132 = 2
    }
    public enum ParticleTypeEnum
    {
        [Description("0.3")]
        PAR_03 = 1,
        [Description("0.6")]
        PAR_06 = 2
    }
}

