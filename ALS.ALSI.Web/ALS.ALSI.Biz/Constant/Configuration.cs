﻿using System;
using System.Configuration;

namespace ALS.ALSI.Biz.Constant
{
    public class Configuration
    {

        public static String CompanyName
        {
            get { return ConfigurationManager.AppSettings["COMPANY_NAME"]; }
        }

        public static String AppTitle
        {
            get { return ConfigurationManager.AppSettings["APP_TITLE"]; }
        }

        public static String PATH_SOURCE_DRIVE
        {
            get { return ConfigurationManager.AppSettings["PATH_SOURCE_DRIVE"]; }
        }
        public static String PATH_TEMPLATE
        {
            get { return ConfigurationManager.AppSettings["PATH_TEMPLATE"]; }
        }

        public static String PATH_SOURCE
        {
            get { return ConfigurationManager.AppSettings["PATH_SOURCE"]; }
        }

        public static String PATH_DOWNLOAD
        {
            get { return ConfigurationManager.AppSettings["PATH_DOWNLOAD"]; }
        }

    }
}
