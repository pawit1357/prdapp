﻿using System;

namespace ALS.ALSI.Biz
{
    public class RawDataSwageForHpa3
    {
        /*
         * Feature	
         * Area	
         * Field	
         * Rank	
         * Al-O (0.5<=ECD<=2.0 um)	
         * Fe-Cr	
         * Fe-Cr-Ni	
         * Fe-Cr-Ni-Si	
         * Fe-Cr-Ni-Mn	
         * Fe	
         * Fe-O	
         * Ti-O/Al-Si-Fe	
         * Sn base	
         * Other	
         * Al	
         * Al-Mg	
         * Al-Ti	
         * Al-Si Based	
         * Al-Si-Cu	
         * Al-Si-Mg	
         * Au	
         * Cu-Zn	
         * Al-Si(2)	
         * Rejected (Manual)	
         * Rejected (ED)	
         * Rejected (Morph)	
         * Area (sq. µm)	
         * Aspect Ratio	
         * Beam X (pixels)	
         * Beam Y (pixels)	
         * Breadth (µm)	
         * Direction (degrees)	
         * ECD (µm)	
         * Length (µm)	
         * Perimeter (µm)	
         * Shape	
         * Mean grey	
         * Spectrum Area	
         * Stage X (mm)	
         * Stage Y (mm)	
         * Stage Z (mm)	
         * O (Wt%)	
         * F (Wt%)	
         * Na (Wt%)	
         * Mg (Wt%)	
         * Al (Wt%)	
         * Si (Wt%)	
         * P (Wt%)	
         * S (Wt%)	
         * Cl (Wt%)	
         * K (Wt%)	
         * Ca (Wt%)	
         * Ti (Wt%)	
         * V (Wt%)	
         * Cr (Wt%)	
         * Mn (Wt%)	
         * Fe (Wt%)	
         * Ni (Wt%)	
         * Cu (Wt%)	
         * Zn (Wt%)	
         * Br (Wt%)	
         * Mo (Wt%)	
         * Ag (Wt%)	
         * Sn (Wt%)	
         * Au (Wt%)
         */
        public String Feature { get; set; }
        public String Area { get; set; }
        public String Field { get; set; }
        public String Rank { get; set; }
        public String Al_O { get; set; }
        public String Fe_Cr { get; set; }
        public String Fe_Cr_Ni { get; set; }
        public String Fe_Cr_Ni_Si { get; set; }
        public String Fe_Cr_Ni_Mn { get; set; }
        public String Fe { get; set; }
        public String Fe_O { get; set; }
        public String Ti_O_Al_Si_Fe { get; set; }
        public String Sn_base { get; set; }
        public String Other { get; set; }
        public String Al { get; set; }
        public String Al_Mg { get; set; }
        public String Al_Ti { get; set; }
        public String Al_Si_Based { get; set; }
        public String Al_Si_Cu { get; set; }
        public String Al_Si_Mg { get; set; }
        public String Au { get; set; }
        public String Cu_Zn { get; set; }
        public String Al_Si2 { get; set; }
        public String Rejected_Manual { get; set; }
        public String Rejected_ED { get; set; }
        public String Rejected_Morph { get; set; }
        public String Area_squm { get; set; }
        public String Aspect_Ratio { get; set; }
        public String Beam_X { get; set; }
        public String Beam_Y { get; set; }
        public String Breadth { get; set; }
        public String Direction_degrees { get; set; }
        public String ECD { get; set; }
        public String Length { get; set; }
        public String Perimeter { get; set; }
        public String Shape { get; set; }
        public String Mean_grey { get; set; }
        public String Spectrum_Area { get; set; }
        public String Stage_X { get; set; }
        public String Stage_Y { get; set; }
        public String Stage_Z { get; set; }
        public String O_Wt { get; set; }
        public String F_Wt { get; set; }
        public String Na_Wt { get; set; }
        public String Mg_Wt { get; set; }
        public String Al_Wt { get; set; }
        public String Si_Wt { get; set; }
        public String P_Wt { get; set; }
        public String S_Wt { get; set; }
        public String Cl_Wt { get; set; }
        public String K_Wt { get; set; }
        public String Ca_Wt { get; set; }
        public String Ti_Wt { get; set; }
        public String V_Wt { get; set; }
        public String Cr_Wt { get; set; }
        public String Mn_Wt { get; set; }
        public String Fe_Wt { get; set; }
        public String Ni_Wt { get; set; }
        public String Cu_Wt { get; set; }
        public String Zn_Wt { get; set; }
        public String Br_Wt { get; set; }
        public String Mo_Wt { get; set; }
        public String Ag_Wt { get; set; }
        public String Sn_Wt { get; set; }
        public String Au_Wt { get; set; }


    }
}
