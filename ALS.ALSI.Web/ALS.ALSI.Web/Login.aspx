﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ALS.ALSI.Web.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title><%=ALS.ALSI.Biz.Constant.Configuration.AppTitle %></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<%= ResolveClientUrl("~/css/bootstrap.min.css") %> " rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/css/metro.css") %> " rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/css/font-awesome.css") %> " rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/css/style.css") %> " rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/css/style_responsive.css") %> " rel="stylesheet" />
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <img id="Img1" src="~/img/logo.png" width="140" alt="logo" runat="server" />
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->

        <form class="form-vertical login-form" method="post" runat="server">

            <h3 class="form-title">Login to your account</h3>
            <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
            <%=Message %>
            <div class="control-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Username</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="icon-user"></i>
                        <input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" name="username" value="admin" />
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="icon-lock"></i>
                        <input class="m-wrap placeholder-no-fix" type="password" placeholder="Password" name="password" value="admin" />
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <label class="checkbox">
                    <asp:CheckBox ID="ckRemember" runat="server" Checked="false" />
                    Remember me
                </label>
                <asp:LinkButton ID="lbLogin" class="btn green pull-right" runat="server" OnClick="lbLogin_Click"> Login <i class="m-icon-swapright m-icon-white"></i></asp:LinkButton>

            </div>
            <div class="forget-password">

                <h4>Forgot your password ?</h4>
                <p>
                    no worries, click <a href="ForgotPassword.aspx" class="" id="forget-password">here</a>
                    to reset your password.
                </p>
            </div>
            <%--           <div class="create-account">
                <p>
                    Don't have an account yet ?&nbsp; 
          <a href="javascript:;" id="register-btn" class="">Create an account</a>
                </p>
            </div>--%>
        </form>

        <!-- END REGISTRATION FORM -->
    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
        <%=ALS.ALSI.Biz.Constant.Configuration.CompanyName %>
    </div>
    <!-- END COPYRIGHT -->
    <!-- BEGIN JAVASCRIPTS -->


    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery.validate.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/app.js") %>"></script>

    <script>
        jQuery(document).ready(function () {
            //App.initLogin();
        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
