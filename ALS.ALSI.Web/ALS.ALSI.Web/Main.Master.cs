﻿using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace ALS.ALSI.Web
{
    public partial class Main : System.Web.UI.MasterPage
    {

        #region "Property"
        protected String notificationList
        {
            get { return (String)Session[GetType().Name + "notificationList"]; }
            set { Session[GetType().Name + "notificationList"] = value; }
        }
        public int PKID { get; set; }
        #endregion


        public user_login userLogin
        {
            get
            {
                return ((Session[Constants.SESSION_USER] != null) ? (user_login)Session[Constants.SESSION_USER] : null);
            }
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name + "notificationList");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
            if (!Page.IsPostBack)
            {
                if (userLogin != null)
                {
                    //Clear old Notification
                    removeSession();

                    RoleEnum roleEnum = (RoleEnum)Enum.Parse(typeof(RoleEnum), userLogin.role_id.ToString());


                    litUserName.Text = String.Format("Login by ::  {0} {1} ( {2} )", userLogin.first_name, userLogin.last_name, roleEnum.ToString());

                    //Generate Alert
                    renderAlert();
                }
            }
        }

        private void renderAlert()
        {
            String htmlNotification =
                    "<li><a href=\"javascript:;\" onclick=\"App.onNotificationClick(1)\">" +
                    "<span class=\"label label-success\"><i class=\"icon-plus\"></i></span>" +
                    "{0}.{1}-{2} <span class=\"time\">{3}</span></a></li>";

            int countBadge = 0;
            List<job_sample_logs> logs = new job_sample_logs().SelectNotification();

            foreach (job_sample_logs log in logs)
            {
                //Show only 5 Row
                if (countBadge < 5)
                {

                    //2. Show by responsible.
                    if (userLogin.responsible_test != null)
                    {
                        if (userLogin.responsible_test.StartsWith(log.job_sample.m_type_of_test.name))
                        {
                            notificationList += String.Format(htmlNotification, (countBadge + 1), log.m_status.name, (String.IsNullOrEmpty(log.job_remark) ? String.Empty : log.job_remark), Convert.ToDateTime(log.date).ToString("MM/dd/yyyy"));
                            countBadge++;
                        }
                    }
                    else
                    {
                        //1. Show By Role if not having responsible.
                        if (userLogin.role_id == log.m_status.m_role.ID)
                        {
                            notificationList += String.Format(htmlNotification, (countBadge + 1), log.m_status.name, (String.IsNullOrEmpty(log.job_remark) ? String.Empty : log.job_remark), Convert.ToDateTime(log.date).ToString("MM/dd/yyyy"));
                            countBadge++;
                        }
                    }
                }
            }

            lbCountBadge.Text = countBadge.ToString();
            lbCountBadge_1.Text = lbCountBadge.Text;

        }
    }
}