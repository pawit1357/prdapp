﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Customer.aspx.cs" Inherits="ALS.ALSI.Web.view.customer.Customer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.date-picker').datepicker();

            // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#Form1');
            var error1 = $('.alert-error', form1);
            var error2 = $('.alert-error2', form1);
            var success1 = $('.alert-success', form1);


            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    ctl00$ContentPlaceHolder2$txtCompanyName: {
                        minlength: 2,
                        required: true,
                    },
                    //ctl00$ContentPlaceHolder2$txtDepartment: {
                    //    minlength: 2,
                    //    required: true
                    //},
                    ctl00$ContentPlaceHolder2$txtTelNumber: {
                        minlength: 2,
                        required: true,
                    },

                    //ctl00$ContentPlaceHolder2$txtFax: {
                    //    minlength: 2,
                    //    required: true,
                    //},
                    ctl00$ContentPlaceHolder2$txtEmail: {
                        required: true,
                        email: true
                    },
                    ctl00$ContentPlaceHolder2$txtAddress: {
                        minlength: 2,
                        required: true,
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },


                submitHandler: function (form) {
                    var rowCount = $('#gvSample tr').length;
                    if (rowCount >= 2) {
                        error1.hide();
                        error2.hide();
                        form.submit();
                    } else {
                        error2.show();
                    }

                }
            });
            App.init(); // init the rest of plugins and elements
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <form id="Form1" method="post" runat="server" class="form-horizontal">
        <div class="alert alert-error hide">
            <button class="close" data-dismiss="alert"></button>
            You have some form errors. Please check below.
        </div>
        <div class="alert alert-success hide">
            <button class="close" data-dismiss="alert"></button>
            Your form validation is successful!
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <h4><i class="icon-bar-chart"></i>
                            <asp:Label ID="lbCommandName" runat="server" Text=""></asp:Label>&nbsp;Customer</h4>
                    </div>
                    <br />
                    <div class="row-fluid">
                        <div class="span12">


                            <h3 class="form-section">&nbsp;Company Info</h3>
                            <!--/span-->
                            <div class="row-fluid">
                                <div class="span6 ">
                                    <div class="control-group">
                                        <label class="control-label" for="CompanyName">Company Name:<span class="required">*</span></label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtCompanyName" runat="server" class="m-wrap span6"></asp:TextBox>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!--/span-->

                            <!--/row-->
                            <div class="row-fluid">
                                <div class="span6 ">
                                    <div class="control-group">
                                        <label class="control-label" for="Department">Department:</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtDepartment" runat="server" class="m-wrap span6"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="span6 ">
                                    <div class="control-group">
                                        <label class="control-label" for="TelNumber">Tel. Number:<span class="required">*</span></label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtTelNumber" runat="server" class="m-wrap span6"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row-fluid">
                                <!--/span-->
                                <div class="span6 ">
                                    <div class="control-group">
                                        <label class="control-label" for="Fax">Fax Number:</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtFax" runat="server" class="m-wrap span6"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6 ">
                                    <div class="control-group">
                                        <label class="control-label" for="Email">Email:<span class="required">*</span></label>
                                        <div class="controls">
                                            <div class="input-prepend">
                                                <span class="add-on">@</span>
                                                <asp:TextBox ID="txtEmail" runat="server" class="m-wrap"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row-fluid">
                                <div class="span12 ">
                                    <div class="control-group">
                                        <label class="control-label" for="Address">Address:<span class="required">*</span></label>
                                        <div class="controls">
                                            <textarea id="txtAddress" class="m-wrap span6" runat="server"></textarea>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <h3 class="form-section">&nbsp;Contract Person:</h3>
                            <div class="row-fluid">


                                <div class="portlet-body">
                                    <div class="clearfix">
                                        <div class="btn-group">
                                            <asp:LinkButton ID="btnAdd" class="btn green" runat="server" OnClick="btnAdd_Click"> Add New <i class="icon-plus"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="alert alert-error2 hide">
                                        <button class="close" data-dismiss="alert"></button>
                                        You have some form errors. Please add contract
                                    </div>
                                    <asp:GridView ID="gvSample" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="table table-striped table-hover table-bordered" ShowHeaderWhenEmpty="True" DataKeyNames="ID" OnRowCancelingEdit="gvSample_RowCancelingEdit" OnRowDataBound="gvSample_RowDataBound" OnRowDeleting="gvSample_RowDeleting" OnRowEditing="gvSample_RowEditing" OnRowUpdating="gvSample_RowUpdating" OnSelectedIndexChanging="gvSample_SelectedIndexChanging">
                                        <Columns>

                                            <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Literal ID="litName" runat="server" Text='<%# Eval("name")%>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("name")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phone_number" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Literal ID="litPhone_number" runat="server" Text='<%# Eval("phone_number")%>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtPhone_number" runat="server" Text='<%# Eval("phone_number")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEdit" runat="server" ToolTip="Edit" CommandName="Edit" CommandArgument='<%# Eval("ID")%>'><i class="icon-edit"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnDelete" runat="server" ToolTip="Delete" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                        CommandArgument='<%# Eval("ID")%>'><i class="icon-trash"></i></asp:LinkButton>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="btnUpdate" runat="server" ToolTip="Update" ValidationGroup="CreditLineGrid"
                                                        CommandName="Update"><i class="icon-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkCancel" runat="server" ToolTip="Cancel" CausesValidation="false"
                                                        CommandName="Cancel"><i class="icon-remove"></i></asp:LinkButton>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerTemplate>
                                            <div class="pagination">
                                                <ul>
                                                    <li>
                                                        <asp:LinkButton ID="btnFirst" runat="server" CommandName="Page" CommandArgument="First"
                                                            CausesValidation="false" ToolTip="First Page"><i class="icon-fast-backward"></i></asp:LinkButton>
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="btnPrev" runat="server" CommandName="Page" CommandArgument="Prev"
                                                            CausesValidation="false" ToolTip="Previous Page"><i class="icon-backward"></i> Prev</asp:LinkButton>
                                                    </li>
                                                    <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                                                    <li>
                                                        <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" CommandArgument="Next"
                                                            CausesValidation="false" ToolTip="Next Page">Next <i class="icon-forward"></i></asp:LinkButton>
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="btnLast" runat="server" CommandName="Page" CommandArgument="Last"
                                                            CausesValidation="false" ToolTip="Last Page"><i class="icon-fast-forward"></i></asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </div>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            <div class="data-not-found">
                                                <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                                            </div>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>

                            </div>


                            <!-- END FORM-->
                        </div>
                    </div>
                    <div class="form-actions">
                        <asp:Button ID="btnSave" runat="server" class="btn green" Text="Save" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" class="cancel btn" Text="Cancel" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </div>

        </div>
    </form>
</asp:Content>
