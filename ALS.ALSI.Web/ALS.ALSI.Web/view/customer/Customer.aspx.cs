﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ALS.ALSI.Web.view.customer
{
    public partial class Customer : System.Web.UI.Page
    {

        #region "Property"
        public user_login userLogin
        {
            get { return ((Session[Constants.SESSION_USER] != null) ? (user_login)Session[Constants.SESSION_USER] : null); }
        }
        public CommandNameEnum CommandName
        {
            get { return (CommandNameEnum)ViewState[Constants.COMMAND_NAME]; }
            set { ViewState[Constants.COMMAND_NAME] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int PKID
        {
            get { return (int)Session[GetType().Name + "PKID"]; }
            set { Session[GetType().Name + "PKID"] = value; }
        }

        public int CONTRACT_PKID
        {
            get { return (int)Session[GetType().Name + "CONTRACT_PKID"]; }
            set { Session[GetType().Name + "CONTRACT_PKID"] = value; }
        }
        public List<m_customer_contract_person> List
        {
            get { return (List<m_customer_contract_person>)Session[GetType().Name + "m_customer_contract_person"]; }
            set { Session[GetType().Name + "m_customer_contract_person"] = value; }
        }

        public List<m_customer_contract_person> ListShow
        {
            get { return List.FindAll(x => x.RowState != CommandNameEnum.Delete); }
        }

        public m_customer objCustomer
        {
            get
            {
                m_customer cus = new m_customer();
                cus.ID = PKID;
                cus.customer_code = string.Empty;
                cus.company_name = txtCompanyName.Text;
                cus.address = txtAddress.InnerText;
                cus.sub_district = string.Empty;
                cus.mobile_number = string.Empty;
                cus.email_address = txtEmail.Text;
                cus.branch = string.Empty;
                cus.district = string.Empty;
                cus.ext = string.Empty;
                cus.department = txtDepartment.Text;
                cus.province = string.Empty;
                cus.code = string.Empty;
                cus.tel_number = txtTelNumber.Text;
                cus.create_by = userLogin.id;
                cus.create_date = DateTime.Now;
                cus.update_by = userLogin.id;
                cus.update_date = DateTime.Now;
                cus.status = "A";
                cus.contractPersonList = List;
                return cus;
            }
        }

        private void initialGrid()
        {

            m_customer_contract_person tmp = new m_customer_contract_person();
            tmp.ID = CustomUtils.GetRandomNumberID();
            tmp.company_id = this.PKID;
            tmp.name = string.Empty;
            tmp.phone_number = string.Empty;
            tmp.status = "A";
            tmp.RowState = CommandNameEnum.Add;
            List.Add(tmp);

            gvSample.DataSource = ListShow;
            gvSample.PageIndex = 0;
            gvSample.DataBind();
        }

        private void initialPage()
        {
            lbCommandName.Text = CommandName.ToString();
            this.List = new List<m_customer_contract_person>();
            switch (CommandName)
            {
                case CommandNameEnum.Add:

                    initialGrid();
                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;


                    break;
                case CommandNameEnum.Edit:
                    fillinScreen();


                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;


                    break;
                case CommandNameEnum.View:
                    fillinScreen();


                    btnSave.Enabled = false;
                    btnCancel.Enabled = true;

                    break;
            }
        }

        private void fillinScreen()
        {

            m_customer cus = new m_customer().SelectByID(this.PKID);
            PKID = cus.ID;
            //cus.customer_code =string.Empty;
            txtCompanyName.Text = cus.company_name;
            txtAddress.InnerText = cus.address;
            //cus.sub_district =string.Empty;
            //cus.mobile_number =string.Empty;
            txtEmail.Text = cus.email_address;
            //cus.branch =string.Empty;
            //cus.district =string.Empty;
            //cus.ext =string.Empty;
            txtDepartment.Text = cus.department;
            //cus.province=string.Empty;
            //cus.code =string.Empty;
            txtTelNumber.Text = cus.tel_number;

            //cus.update_by = userLogin.ID;
            //cus.update_date = DateTime.Now;
            //cus.status = "A";


            List<m_customer_contract_person> contractPersonList = new m_customer_contract_person().FindAllByCompanyID(cus.ID);
            if (contractPersonList != null && contractPersonList.Count > 0)
            {
                foreach (m_customer_contract_person tmp in contractPersonList)
                {
                    tmp.RowState = CommandNameEnum.Edit;
                }
                this.List = contractPersonList;
                gvSample.DataSource = ListShow;
                gvSample.DataBind();

            }

        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "PKID");
            Session.Remove(GetType().Name + "CONTRACT_PKID");
            Session.Remove(GetType().Name + "m_customer_contract_person");
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            SearchCustomer prvPage = Page.PreviousPage as SearchCustomer;
            this.CommandName = (prvPage == null) ? this.CommandName : prvPage.CommandName;
            this.PKID = (prvPage == null) ? this.PKID : prvPage.PKID;
            this.PreviousPath = Constants.LINK_SEARCH_CUSTOMER;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            switch (CommandName)
            {
                case CommandNameEnum.Add:
                    objCustomer.Insert();
                    break;
                case CommandNameEnum.Edit:
                    objCustomer.Update();
                    break;
            }
            //Commit
            GeneralManager.Commit();
            removeSession();
            Response.Redirect(PreviousPath);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            initialGrid();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtCompanyName.Text = string.Empty;
            txtDepartment.Text = string.Empty;
            txtTelNumber.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtAddress.InnerText = string.Empty;
            removeSession();
            Response.Redirect(PreviousPath);
        }

        protected void gvSample_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
        {
            gvSample.EditIndex = -1;
            gvSample.DataSource = ListShow;
            gvSample.DataBind();
            btnAdd.Enabled = true;
        }

        protected void gvSample_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            gvSample.EditIndex = e.NewEditIndex;
            gvSample.DataSource = ListShow;
            gvSample.DataBind();
        }

        protected void gvSample_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {
            this.CONTRACT_PKID = int.Parse(gvSample.DataKeys[e.RowIndex].Values[0].ToString());
            TextBox _txtName = (TextBox)gvSample.Rows[e.RowIndex].FindControl("txtName");
            TextBox _txtPhone = (TextBox)gvSample.Rows[e.RowIndex].FindControl("txtPhone_number");

            m_customer_contract_person contract = List.Find(x => x.ID == this.CONTRACT_PKID);
            if (contract != null)
            {
                contract.company_id = objCustomer.ID;
                contract.name = _txtName.Text;
                contract.phone_number = _txtPhone.Text;
            }



            gvSample.EditIndex = -1;
            gvSample.DataSource = ListShow;
            gvSample.DataBind();
            btnAdd.Enabled = true;
        }

        protected void gvSample_SelectedIndexChanging(object sender, System.Web.UI.WebControls.GridViewSelectEventArgs e)
        {

        }

        protected void gvSample_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnDelete = (LinkButton)e.Row.FindControl("btnDelete");
                LinkButton btnEdit = (LinkButton)e.Row.FindControl("btnEdit");
                if (btnDelete != null)
                {
                    btnDelete.Visible = !(this.CommandName == CommandNameEnum.View);
                    btnEdit.Visible = !(this.CommandName == CommandNameEnum.View);
                }
            }
        }

        protected void gvSample_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.CONTRACT_PKID = int.Parse(gvSample.DataKeys[e.RowIndex].Values[0].ToString());

            m_customer_contract_person js = List.Find(x => x.ID == this.CONTRACT_PKID);
            if (js != null)
            {
                js.RowState = CommandNameEnum.Delete;
                gvSample.DataSource = ListShow;
                gvSample.DataBind();
            }
            btnAdd.Enabled = true;
        }


    }
}