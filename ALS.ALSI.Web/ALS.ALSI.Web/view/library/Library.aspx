﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Library.aspx.cs" Inherits="ALS.ALSI.Web.view.library.Library" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //App.setPage("form_specification_validate");  // set current page
            App.init(); // init the rest of plugins and elements

            var form1 = $('#Form1');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    //ctl00$ContentPlaceHolder2$txtName: {
                    //    minlength: 2,
                    //    required: true,
                    //},
                    //ctl00$ContentPlaceHolder2$txtPathUrl: {
                    //    minlength: 2,
                    //    required: true,
                    //},
                    //ctl00$ContentPlaceHolder2$txtPathSourceFile: {
                    //    minlength: 2,
                    //    required: true,
                    //},


                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },


                submitHandler: function (form) {
                    form.submit();
                }
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <form id="Form1" method="post" runat="server" class="form-horizontal">
        <div class="alert alert-error hide">
            <button class="close" data-dismiss="alert"></button>
            You have some form errors. Please check below.
        </div>
        <div class="alert alert-success hide">
            <button class="close" data-dismiss="alert"></button>
            Your form validation is successful!
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <h4><i class="icon-bar-chart"></i>
                            <asp:Label ID="lbCommandName" runat="server" Text=""></asp:Label>&nbsp;Lirary</h4>
                    </div>
                    <br />
                    <%--<div class="row-fluid">
                        <div class="span10">
                            <div class="control-group">
                                <label class="control-label" for="txtName">Name:<span class="required">*</span></label>
                                <div class="controls">
                                    <asp:TextBox ID="txtName" runat="server" class="mini span12"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span10">
                            <div class="control-group">
                                <label class="control-label" for="txtPathUrl">Application Path:<span class="required">*</span></label>
                                <div class="controls">
                                    <asp:TextBox ID="txtPathUrl" runat="server" class="mini span12"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>--%>

                    <div class="row-fluid">
                        <div class="span10">
                            <div class="control-group">
                                <label class="control-label" for="txtPathSourceFile">Path Source File:<span class="required">*</span></label>
                                <div class="controls">
                                    <asp:TextBox ID="txtPathSourceFile" runat="server" class="mini span12"></asp:TextBox>
                                    <br />
                                    <br />
                                    <span class="btn green fileinput-button">
                                        <i class="icon-plus icon-white"></i>
                                        <span>Add files...</span>
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span6 ">
                            <ul>
                                <li>The maximum file size for uploads in this demo is <strong>5 MB</strong> (default file size is unlimited).</li>
                                <li>Only Excel files (<strong>xls</strong>) are allowed in this demo (by default there is no file type restriction).</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <%=Message %>

                <div class="form-actions">
                    <asp:Button ID="btnSave" runat="server" class="btn green" Text="Upload" OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" class="cancel btn" Text="Cancel" OnClick="btnCancel_Click" />
                </div>
            </div>
        </div>
    </form>
</asp:Content>

