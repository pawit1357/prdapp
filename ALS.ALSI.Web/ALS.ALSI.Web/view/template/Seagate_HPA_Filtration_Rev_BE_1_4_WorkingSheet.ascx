﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Seagate_HPA_Filtration_Rev_BE_1_4_WorkingSheet.ascx.cs" Inherits="ALS.ALSI.Web.view.template.Seagate_HPA_Filtration_Rev_BE_1_4_WorkingSheet" %>

<style type="text/css">
    .auto-style1 {
        height: 23px;
    }
</style>

<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="alert alert-error hide">
                <button class="close" data-dismiss="alert"></button>
                You have some form errors. Please check below.
            </div>
            <div class="alert alert-success hide">
                <button class="close" data-dismiss="alert"></button>
                Your form validation is successful!
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="row-fluid fileupload-buttonbar">
                        <div class="span6">
                            <%--<label class="control-label" for="ddlSpecification">Uplod file:</label>--%>
                            <span class="btn green fileinput-button">
                                <i class="icon-plus icon-white"></i>
                                <span>Choose DSH files...</span>
                                <asp:FileUpload ID="btnUpload" runat="server" AllowMultiple="True" />
                            </span>

                        </div>
                    </div>
                    <div class="row-fluid">
                        <ul>
                            <li>The maximum file size for uploads is <strong>5 MB</strong> (default file size is unlimited).</li>
                            <li>Only text files (<strong>txt</strong>) are allowed.</li>
                        </ul>
                        <br />
                        <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                        <br />

                    </div>
                    <br />
                    <br />
                    <asp:CheckBox ID="cbLPCType68" runat="server" Checked="true" AutoPostBack="true" OnCheckedChanged="cbLPCType68_CheckedChanged" />LPC 68 KHz
                    <asp:CheckBox ID="cbLPCType132" runat="server" AutoPostBack="true" OnCheckedChanged="cbLPCType132_CheckedChanged" />LPC 132 KHZ
                                            
                </div>
            </div>
            </div>
            <br />
            <div class="row-fluid">
                <div class="span10">
                    <div class="tabbable tabbable-custom boxless">
                        <ul class="nav nav-tabs">
                            <asp:Button ID="btnUsLPC03" runat="server" Text="US-LPC(0.3)" CssClass="btn blue" OnClick="btnUsLPC_Click" />
                            <asp:Button ID="btnUsLPC06" runat="server" Text="US-LPC(0.6)" CssClass="btn blue" OnClick="btnUsLPC_Click" />
                            <asp:Button ID="btnWorksheetForHPAFiltration" runat="server" Text="Worksheet for HPA - Filtration" CssClass="btn blue" OnClick="btnUsLPC_Click" />
                        </ul>
                        <div class="tab-content">
                            <%--US-LPC(0.3)--%>
                            <asp:Panel ID="pUS_LPC03" runat="server">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="row-fluid">
                                        <div class="span8">
                                            <h6>Results:</h6>
                                            <div class="row-fluid">
                                                <div class="span8">
                                                    <table class="table table-striped table-hover small" id="tb1" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th colspan="6">No. of Particles ≥ 0.3μm (Counts/ml)</th>
                                                            </tr>
                                                            <tr>
                                                                <th>RUN</th>
                                                                <th>Blank 1</th>
                                                                <th>Sample 1</th>
                                                                <th>Blank 2</th>
                                                                <th>Sample 2</th>
                                                                <th>Blank 3</th>
                                                                <th>Sample 3</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr runat="server" id="tr1">
                                                                <td>1</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_B14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_C14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_D14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_E14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_F14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_G14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr2">
                                                                <td>2</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_B15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_C15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_D15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_E15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_F15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_G15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr3">
                                                                <td>3</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_B16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_C16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_D16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_E16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_F16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_G16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr4">
                                                                <td>4</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_B17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_C17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_D17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_E17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_F17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_G17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr5">
                                                                <td>Average of last 3</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_B18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_C18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_D18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_E18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_F18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_G18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr6">
                                                                <td>Extraction Vol. (ml)</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC03_B20" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr7">
                                                                <td>Surface Area (cm2)</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC03_B21" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr8">
                                                                <td>No. of Parts Used</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC03_B22" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr9">
                                                                <td>Sample</td>
                                                                <td colspan="2">1</td>
                                                                <td colspan="2">2</td>
                                                                <td colspan="2">3</td>
                                                            </tr>
                                                            <tr runat="server" id="tr10">
                                                                <td>No. of Particles ≥ 0.3µm<br />
                                                                    (Counts/cm2)</td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC03_B25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC03_D25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC03_F25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr11">
                                                                <td>Average</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC03_B26" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--US-LPC(0.6)--%>
                            <asp:Panel ID="pUS_LPC06" runat="server">
                                <div class="tab-pane active" id="Div1">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <h6>Results:</h6>
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <table class="table table-striped table-hover small" id="Table1" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th colspan="6">No. of Particles ≥ 0.6μm (Counts/ml)</th>
                                                            </tr>
                                                            <tr>
                                                                <th>RUN</th>
                                                                <th>Blank 1</th>
                                                                <th>Sample 1</th>
                                                                <th>Blank 2</th>
                                                                <th>Sample 2</th>
                                                                <th>Blank 3</th>
                                                                <th>Sample 3</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr runat="server" id="tr12">
                                                                <td>1</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_B14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_C14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_D14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_E14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_F14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_G14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr13">
                                                                <td>2</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_B15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_C15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_D15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_E15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_F15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_G15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr14">
                                                                <td>3</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_B16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_C16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_D16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_E16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_F16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_G16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr15">
                                                                <td>4</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_B17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_C17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_D17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_E17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_F17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_G17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr16">
                                                                <td>Average of last 3</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_B18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_C18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_D18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_E18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_F18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_G18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr17">
                                                                <td>Extraction Vol. (ml)</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC06_B20" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr18">
                                                                <td>Surface Area (cm2)</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC06_B21" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr19">
                                                                <td>No. of Parts Used</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC06_B22" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr20">
                                                                <td>Sample</td>
                                                                <td colspan="2">1</td>
                                                                <td colspan="2">2</td>
                                                                <td colspan="2">3</td>
                                                            </tr>
                                                            <tr runat="server" id="tr21">
                                                                <td>No. of Particles ≥ 0.6µm<br />
                                                                    (Counts/cm2)</td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC06_B25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC06_D25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC06_F25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr22">
                                                                <td>Average</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC06_B26" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--Worksheet for HPA - Filtration--%>
                            <asp:Panel ID="pWorksheetForHPAFiltration" runat="server">
                                <div class="tab-pane active" id="Div2">
                                    <div class="row-fluid">
                                        <div class="span10">
                                            <h6>Worksheet - Filtration HPA Method</h6>
                                            <div class="row-fluid">
                                                <div class="span10">
                                                    <table class="table table-striped table-hover small" id="Table3" runat="server">
                                                        <tbody>
                                                            <tr runat="server" id="tr29">
                                                                <td>Volume of Extraction (ml), Vt</td>
                                                                <td runat="server" id="td7">
                                                                    <asp:TextBox ID="txtB3" runat="server" CssClass="m-wrap small">900</asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr30">
                                                                <td>Surface Area (cm2), C</td>
                                                                <td runat="server" id="td8">
                                                                    <asp:TextBox ID="txtB4" runat="server" CssClass="m-wrap small">0.7322566</asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr31">
                                                                <td>Number of Parts Extracted, N</td>
                                                                <td runat="server" id="td9">
                                                                    <asp:TextBox ID="txtB5" runat="server" CssClass="m-wrap small">10</asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr32">
                                                                <td>Volume of Filtration (ml), Vf</td>
                                                                <td runat="server" id="td10">
                                                                    <asp:TextBox ID="txtB6" runat="server" CssClass="m-wrap small">50</asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr33">
                                                                <td>Filter Area (sqmm), At</td>
                                                                <td runat="server" id="td11">
                                                                    <asp:TextBox ID="txtB7" runat="server" CssClass="m-wrap small" AutoPostBack="True" OnTextChanged="txtB8_TextChanged">7.071</asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr34">
                                                                <td>Percent Area Coverage (%)</td>
                                                                <td runat="server" id="td12">
                                                                    <asp:TextBox ID="txtB8" runat="server" CssClass="m-wrap small" ReadOnly="True">50.21</asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr35">
                                                                <td>Area of Filter Surveyed (sqmm), As</td>
                                                                <td runat="server" id="td13">
                                                                    <asp:TextBox ID="txtB9" runat="server" CssClass="m-wrap small" AutoPostBack="True" OnTextChanged="txtB8_TextChanged">3.55</asp:TextBox></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <br />
                                                    <table class="table table-striped table-hover small" id="Table2" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th>&nbsp;Types of Particles </th>
                                                                <th>Blank Counts (B)</th>
                                                                <th>Raw Counts (Y)</th>
                                                                <th>Counts/sqcm (X)</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr runat="server" id="tr45">
                                                                <td>AlMgO</td>
                                                                <td runat="server" id="td45">
                                                                    <asp:TextBox ID="txt_WsB13" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC13" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD13" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr46">
                                                                <td>AlO</td>
                                                                <td runat="server" id="td46">
                                                                    <asp:TextBox ID="txt_WsB14" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC14" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD14" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr47">
                                                                <td>AlOTiC</td>
                                                                <td runat="server" id="td47">
                                                                    <asp:TextBox ID="txt_WsB15" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC15" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD15" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr48">
                                                                <td>CrO</td>
                                                                <td runat="server" id="td48">
                                                                    <asp:TextBox ID="txt_WsB16" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC16" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD16" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr49">
                                                                <td>FeSiO</td>
                                                                <td runat="server" id="td49">
                                                                    <asp:TextBox ID="txt_WsB17" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC17" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD17" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr50">
                                                                <td>NbB</td>
                                                                <td runat="server" id="td50">
                                                                    <asp:TextBox ID="txt_WsB18" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC18" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD18" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr51">
                                                                <td>SiC</td>
                                                                <td runat="server" id="td51">
                                                                    <asp:TextBox ID="txt_WsB19" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC19" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD19" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr52">
                                                                <td>SiO</td>
                                                                <td runat="server" id="td52">
                                                                    <asp:TextBox ID="txt_WsB20" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC20" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD20" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr53">
                                                                <td>TiB</td>
                                                                <td runat="server" id="td53">
                                                                    <asp:TextBox ID="txt_WsB21" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC21" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD21" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr54">
                                                                <td>TiC</td>
                                                                <td runat="server" id="td54">
                                                                    <asp:TextBox ID="txt_WsB22" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC22" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD22" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr55">
                                                                <td>TiO</td>
                                                                <td runat="server" id="td55">
                                                                    <asp:TextBox ID="txt_WsB23" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC23" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD23" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr56">
                                                                <td>TiSn</td>
                                                                <td runat="server" id="td56">
                                                                    <asp:TextBox ID="txt_WsB24" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC24" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD24" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr57">
                                                                <td>TiV</td>
                                                                <td runat="server" id="td57">
                                                                    <asp:TextBox ID="txt_WsB25" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC25" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD25" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr58">
                                                                <td>WC</td>
                                                                <td runat="server" id="td58">
                                                                    <asp:TextBox ID="txt_WsB26" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC26" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD26" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr59">
                                                                <td>ZrC</td>
                                                                <td runat="server" id="td59">
                                                                    <asp:TextBox ID="txt_WsB27" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC27" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD27" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr60">
                                                                <td>ZrO</td>
                                                                <td runat="server" id="td60">
                                                                    <asp:TextBox ID="txt_WsB28" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC28" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD28" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr23">
                                                                <td>Total Hard Particles</td>
                                                                <td runat="server" id="td1">
                                                                    <asp:TextBox ID="txt_WsB29" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC29" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD29" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                            <tr runat="server" id="tr61">
                                                                <td>Nd based</td>
                                                                <td runat="server" id="td61">
                                                                    <asp:TextBox ID="txt_WsB30" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC30" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD30" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr62">
                                                                <td>Sm based</td>
                                                                <td runat="server" id="td62">
                                                                    <asp:TextBox ID="txt_WsB31" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC31" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD31" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr63">
                                                                <td>Sr based</td>
                                                                <td runat="server" id="td63">
                                                                    <asp:TextBox ID="txt_WsB32" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC32" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD32" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr24">
                                                                <td>Total Magnetic Particles</td>
                                                                <td runat="server" id="td2">
                                                                    <asp:TextBox ID="txt_WsB33" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC33" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD33" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr64">
                                                                <td>SST300s (Fe/Cr/Ni) </td>
                                                                <td runat="server" id="td64">
                                                                    <asp:TextBox ID="txt_WsB34" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC34" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD34" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                            <tr runat="server" id="tr65">
                                                                <td>SST400s (Fe/Cr) </td>
                                                                <td runat="server" id="td65">
                                                                    <asp:TextBox ID="txt_WsB35" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC35" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD35" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr25">
                                                                <td>Total SST</td>
                                                                <td runat="server" id="td3">
                                                                    <asp:TextBox ID="txt_WsB36" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC36" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD36" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr66">
                                                                <td>Ag based</td>
                                                                <td runat="server" id="td66">
                                                                    <asp:TextBox ID="txt_WsB37" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC37" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD37" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr67">
                                                                <td>Al based</td>
                                                                <td runat="server" id="td67">
                                                                    <asp:TextBox ID="txt_WsB38" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC38" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD38" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr68">
                                                                <td>Au based</td>
                                                                <td runat="server" id="td68">
                                                                    <asp:TextBox ID="txt_WsB39" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC39" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD39" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr69">
                                                                <td>Cu based</td>
                                                                <td runat="server" id="td69">
                                                                    <asp:TextBox ID="txt_WsB40" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC40" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD40" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr70">
                                                                <td>Fe based</td>
                                                                <td runat="server" id="td70">
                                                                    <asp:TextBox ID="txt_WsB41" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC41" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD41" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr71">
                                                                <td>MnCrS</td>
                                                                <td runat="server" id="td71">
                                                                    <asp:TextBox ID="txt_WsB42" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC42" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD42" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr72">
                                                                <td>Ni based</td>
                                                                <td runat="server" id="td72">
                                                                    <asp:TextBox ID="txt_WsB43" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC43" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD43" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr73">
                                                                <td>NiP</td>
                                                                <td runat="server" id="td73">
                                                                    <asp:TextBox ID="txt_WsB44" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC44" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD44" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr74">
                                                                <td>Pt based</td>
                                                                <td runat="server" id="td74">
                                                                    <asp:TextBox ID="txt_WsB45" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC45" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD45" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr75">
                                                                <td>Sb based</td>
                                                                <td runat="server" id="td75">
                                                                    <asp:TextBox ID="txt_WsB46" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC46" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD46" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr76">
                                                                <td>Sn based </td>
                                                                <td runat="server" id="td76">
                                                                    <asp:TextBox ID="txt_WsB47" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC47" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD47" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr77">
                                                                <td>SnPb</td>
                                                                <td runat="server" id="td77">
                                                                    <asp:TextBox ID="txt_WsB48" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC48" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD48" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr78">
                                                                <td>Zn based</td>
                                                                <td runat="server" id="td78">
                                                                    <asp:TextBox ID="txt_WsB49" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC49" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD49" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr79">
                                                                <td>AlSi(FeCrCuZnMn)</td>
                                                                <td runat="server" id="td79">
                                                                    <asp:TextBox ID="txt_WsB50" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC50" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD50" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr80">
                                                                <td>NiFe</td>
                                                                <td runat="server" id="td80">
                                                                    <asp:TextBox ID="txt_WsB51" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC51" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD51" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr81">
                                                                <td>ZnPFe</td>
                                                                <td runat="server" id="td81">
                                                                    <asp:TextBox ID="txt_WsB52" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC52" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD52" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr82">
                                                                <td>CrCoNiP (disc material)</td>
                                                                <td runat="server" id="td82">
                                                                    <asp:TextBox ID="txt_WsB53" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC53" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD53" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr83">
                                                                <td>NiPCr</td>
                                                                <td runat="server" id="td83">
                                                                    <asp:TextBox ID="txt_WsB54" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC54" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD54" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr84">
                                                                <td>NiPCrFe</td>
                                                                <td runat="server" id="td84">
                                                                    <asp:TextBox ID="txt_WsB55" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC55" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD55" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr85">
                                                                <td>CuZn</td>
                                                                <td runat="server" id="td85">
                                                                    <asp:TextBox ID="txt_WsB56" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC56" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD56" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr26">
                                                                <td>Total Metal Particle</td>
                                                                <td runat="server" id="td4">
                                                                    <asp:TextBox ID="txt_WsB57" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC57" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD57" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr86">
                                                                <td>FeO</td>
                                                                <td runat="server" id="td86">
                                                                    <asp:TextBox ID="txt_WsB58" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC58" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD58" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr87">
                                                                <td>AlFeO</td>
                                                                <td runat="server" id="td87">
                                                                    <asp:TextBox ID="txt_WsB59" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC59" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD59" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr88">
                                                                <td>AlNiO</td>
                                                                <td runat="server" id="td88">
                                                                    <asp:TextBox ID="txt_WsB60" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC60" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD60" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr89">
                                                                <td>AlSiO</td>
                                                                <td runat="server" id="td89">
                                                                    <asp:TextBox ID="txt_WsB61" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC61" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD61" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr90">
                                                                <td>Cl based</td>
                                                                <td runat="server" id="td90">
                                                                    <asp:TextBox ID="txt_WsB62" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC62" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD62" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr91">
                                                                <td>FeMgSiO</td>
                                                                <td runat="server" id="td91">
                                                                    <asp:TextBox ID="txt_WsB63" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC63" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD63" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr92">
                                                                <td>MgCaO</td>
                                                                <td runat="server" id="td92">
                                                                    <asp:TextBox ID="txt_WsB64" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC64" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD64" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr93">
                                                                <td>MgSiO</td>
                                                                <td runat="server" id="td93">
                                                                    <asp:TextBox ID="txt_WsB65" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC65" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD65" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr94">
                                                                <td>S based</td>
                                                                <td runat="server" id="td94">
                                                                    <asp:TextBox ID="txt_WsB66" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC66" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD66" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr95">
                                                                <td>F based</td>
                                                                <td runat="server" id="td95">
                                                                    <asp:TextBox ID="txt_WsB67" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC67" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD67" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr96">
                                                                <td>Ca based</td>
                                                                <td runat="server" id="td96">
                                                                    <asp:TextBox ID="txt_WsB68" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC68" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD68" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr97">
                                                                <td>Na based</td>
                                                                <td runat="server" id="td97">
                                                                    <asp:TextBox ID="txt_WsB69" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC69" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD69" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr98">
                                                                <td>K based</td>
                                                                <td runat="server" id="td98">
                                                                    <asp:TextBox ID="txt_WsB70" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC70" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD70" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr99">
                                                                <td>Anodised Al</td>
                                                                <td runat="server" id="td99">
                                                                    <asp:TextBox ID="txt_WsB71" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC71" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD71" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr100">
                                                                <td>PZT</td>
                                                                <td runat="server" id="td100">
                                                                    <asp:TextBox ID="txt_WsB72" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC72" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD72" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr101">
                                                                <td>Pb base</td>
                                                                <td runat="server" id="td101">
                                                                    <asp:TextBox ID="txt_WsB73" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC73" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD73" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr102">
                                                                <td>Others</td>
                                                                <td runat="server" id="td102">
                                                                    <asp:TextBox ID="txt_WsB74" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC74" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD74" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr27">
                                                                <td>Total Other Particles</td>
                                                                <td runat="server" id="td5">
                                                                    <asp:TextBox ID="txt_WsB75" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC75" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD75" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="tr28">
                                                                <td>Total Particles</td>
                                                                <td runat="server" id="td6">
                                                                    <asp:TextBox ID="txt_WsB76" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsC76" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_WsD76" runat="server" CssClass="m-wrap small"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-actions clearfix">
                <asp:Button ID="btnLoadFile" runat="server" Text="Load" CssClass="btn blue" OnClick="btnLoadFile_Click" />
                <asp:Button ID="btnCalculate" runat="server" Text="Calculate" CssClass="btn green" OnClick="btnCalculate_Click" />
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="disable btn" OnClick="btnCancel_Click" />
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoadFile" />
            <asp:PostBackTrigger ControlID="btnSubmit" />
        </Triggers>
    </asp:UpdatePanel>
</form>
