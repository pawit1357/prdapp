﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Web.view.request;
using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ALS.ALSI.Web.view.template
{
    public partial class WD_IC_Component_1_3 : System.Web.UI.UserControl
    {

        #region "Property"

        public user_login userLogin
        {
            get
            {
                return ((Session[Constants.SESSION_USER] != null) ? (user_login)Session[Constants.SESSION_USER] : null);
            }
        }

        public job_sample jobSample
        {
            get { return (job_sample)Session["job_sample"]; }
            set { Session["job_sample"] = value; }
        }

        public CommandNameEnum CommandName
        {
            get { return (CommandNameEnum)ViewState[Constants.COMMAND_NAME]; }
            set { ViewState[Constants.COMMAND_NAME] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int JobID
        {
            get { return (int)Session[GetType().Name + "JobID"]; }
            set { Session[GetType().Name + "JobID"] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        public template_wd_ic_coverpage objWork
        {
            get
            {
                template_wd_ic_coverpage work = new template_wd_ic_coverpage();
                work.sample_id = this.SampleID;
                work.specification_id = int.Parse(ddlSpecification.SelectedValue);
                work.item_visible = getItemStatus();
                return work;
            }
        }

        private void initialPage()
        {
            ddlAssignTo.Items.Clear();
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LOGIN_SELECT_SPEC), Convert.ToInt16(StatusEnum.LOGIN_SELECT_SPEC) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.CHEMIST_TESTING), Convert.ToInt16(StatusEnum.CHEMIST_TESTING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_CHECKING), Convert.ToInt16(StatusEnum.SR_CHEMIST_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_WORD), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_WORD) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_CHECKING), Convert.ToInt16(StatusEnum.LABMANAGER_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_PDF), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_PDF) + ""));
            //ddlAssignTo.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, ""));


            ddlSpecification.Items.Clear();
            ddlSpecification.DataSource = new tb_m_ic_specification().SelectBySpecificationID(Convert.ToInt16(SpecificationEnum.WD));
            ddlSpecification.DataBind();
            ddlSpecification.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));

            /*INFO*/
            #region "job Info"
            job_info job = new job_info().SelectByID(this.JobID);

            lbPoNo.Text = (job.customer_po_ref == null) ? String.Empty : job.customer_po_ref.ToString();
            lbDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
            lbCompany.Text = String.Format("{0}<br />{1}", job.m_customer.company_name, job.m_customer.address);
            lbDateSampleReceived.Text = Convert.ToDateTime(job.date_of_receive).ToString("MM/dd/yyyy");

            this.jobSample = new job_sample().SelectByID(this.SampleID);

            if (this.jobSample != null)
            {


                lbRefNo.Text = this.jobSample.job_number.ToString();
                lbDownloadName.Text = this.jobSample.job_number.ToString();
                lbDateTestCompleted.Text = Convert.ToDateTime(this.jobSample.due_date).ToString("MM/dd/yyyy");

                lbSampleDescription.Text = String.Format("Description:{0}<br />Model:{1}<br />Surface Area:{2}<br />Remark:{3}<br />", this.jobSample.description, this.jobSample.model, this.jobSample.surface_area, this.jobSample.remarks);

                RoleEnum userRole = (RoleEnum)Enum.Parse(typeof(RoleEnum), userLogin.role_id.ToString(), true);

                StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);

                lbJobStatus.Text = Constants.GetEnumDescription(status);
                ddlStatus.Items.Clear();
                switch (status)
                {
                    case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                        break;
                    case StatusEnum.LOGIN_SELECT_SPEC:
                        pSpecification.Visible = true;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.LOGIN) ? ((this.jobSample.step1owner == null) ? true : ((this.jobSample.step1owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.CHEMIST_TESTING:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.CHEMIST) ? ((this.jobSample.step2owner == null) ? true : ((this.jobSample.step2owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.SR_CHEMIST_CHECKING:
                    case StatusEnum.SR_CHEMIST_APPROVE:
                    case StatusEnum.SR_CHEMIST_DISAPPROVE:

                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_APPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_DISAPPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_WORD:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.LABMANAGER_CHECKING:
                    case StatusEnum.LABMANAGER_APPROVE:
                    case StatusEnum.LABMANAGER_DISAPPROVE:
                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_APPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_DISAPPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_PDF:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        break;
                }

            }
            #endregion

            #region "WorkSheet"
            template_wd_ic_coverpage ic = new template_wd_ic_coverpage().SelectBySampleID(this.SampleID);
            if (ic != null)
            {
                this.CommandName = CommandNameEnum.Edit;

                ddlSpecification.SelectedValue = ic.specification_id.ToString();
                tb_m_ic_specification temp29_spec = new tb_m_ic_specification().SelectByID(Convert.ToInt32(ic.specification_id));

                if (temp29_spec != null)
                {
                    /*METHOD/PROCEDURE:*/
                    txtC18.Text = ic.b13.ToString();//"Number of pieces used for extraction"
                    txtE18.Text = String.Format("{0}ml", (1000 * Convert.ToDecimal(ic.b11)));//"Number of pieces used for extraction"

                    #region "Header Label"
                    lbDocRev.Text = temp29_spec.B;
                    lbDesc.Text = temp29_spec.A;
                    lbB24.Text = temp29_spec.C;
                    lbC24.Text = lbB24.Text;
                    lbB33.Text = lbB24.Text;
                    lbC33.Text = lbB24.Text;
                    lbB24_1.Text = lbB24.Text;
                    lbB24_2.Text = lbB24.Text;
                    #endregion

                    #region "*Anionic*"

                    /*Anionic*/
                    lbB25.Text = temp29_spec.D;//Fluoride as F
                    lbB26.Text = temp29_spec.E;//Chloride as Cl
                    lbB28.Text = temp29_spec.G;//Bromide as Br
                    lbB29.Text = temp29_spec.H;//Nitrate as NO3
                    lbB30.Text = temp29_spec.I;//Sulphate as SO4
                    lbB31.Text = temp29_spec.J;//Phosphate as PO4
                    lbB32.Text = temp29_spec.K;//Total of 7Anions

                    lbC25.Text = ic.i17.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i17));
                    lbC26.Text = ic.i18.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i18));
                    lbC28.Text = ic.i19.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i19));
                    lbC29.Text = ic.i20.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i20));
                    lbC30.Text = ic.i21.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i21));
                    lbC31.Text = ic.i22.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i22));
                    lbC32.Text = String.IsNullOrEmpty(ic.i23) ? "" : ic.i23.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i23));

                    lbD25.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f17));
                    lbD26.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f18));
                    lbD28.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f19));
                    lbD29.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f20));
                    lbD30.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f21));
                    lbD31.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f22));
                    lbD32.Text = Constants.CHAR_DASH;
                    //Result
                    if (!String.IsNullOrEmpty(lbC25.Text))
                    {

                        lbE25.Text = (lbB25.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i17) ? "0" : (ic.i17.Equals("Not Detected") ? "0" : ic.i17)) < Convert.ToDecimal(lbB25.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE26.Text = (lbB26.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i18) ? "0" : (ic.i18.Equals("Not Detected") ? "0" : ic.i18)) < Convert.ToDecimal(lbB26.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE28.Text = (lbB28.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i19) ? "0" : (ic.i19.Equals("Not Detected") ? "0" : ic.i19)) < Convert.ToDecimal(lbB28.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE29.Text = (lbB29.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i20) ? "0" : (ic.i20.Equals("Not Detected") ? "0" : ic.i20)) < Convert.ToDecimal(lbB29.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE30.Text = (lbB30.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i21) ? "0" : (ic.i21.Equals("Not Detected") ? "0" : ic.i21)) < Convert.ToDecimal(lbB30.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE31.Text = (lbB31.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i22) ? "0" : (ic.i22.Equals("Not Detected") ? "0" : ic.i22)) < Convert.ToDecimal(lbB31.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE32.Text = (lbB32.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i23) ? "0" : (ic.i23.Equals("Not Detected") ? "0" : ic.i23)) < Convert.ToDecimal(lbB32.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                    }
                    #endregion

                    #region "*Cationic*"
                    lbB34.Text = temp29_spec.M;//Lithium as Li
                    lbB35.Text = temp29_spec.P;//Sodium as Na
                    lbB36.Text = temp29_spec.L;//Ammonium as NH4
                    lbB37.Text = temp29_spec.O;//Potassium as K
                    lbB38.Text = temp29_spec.Q;//Magnesium as Mg
                    lbB39.Text = temp29_spec.N;//Calcium as Ca
                    lbB40.Text = temp29_spec.R;//Total Cations

                    lbC34.Text = ic.i26.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i26));
                    lbC35.Text = ic.i27.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i27));
                    lbC36.Text = ic.i28.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i28));
                    lbC37.Text = ic.i29.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i29));
                    lbC38.Text = ic.i30.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i30));
                    lbC39.Text = ic.i31.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : String.Format("{0:n2}", Convert.ToDouble(ic.i31));
                    //lbC40.Text = ic.i32.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "" : String.Format("{0:n4}", Convert.ToDouble(ic.i32));

                    lbD34.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f26));
                    lbD35.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f27));
                    lbD36.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f28));
                    lbD37.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f29));
                    lbD38.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f30));
                    lbD39.Text = String.Format("{0:n2}", Convert.ToDouble(ic.f31));
                    lbD40.Text = Constants.CHAR_DASH;
                    //Result
                    if (!String.IsNullOrEmpty(lbC34.Text))
                    {

                        lbE34.Text = (lbB34.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i26) ? "0" : (ic.i26.Equals("Not Detected") ? "0" : ic.i26)) < Convert.ToDecimal(lbB34.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE35.Text = (lbB35.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i27) ? "0" : (ic.i27.Equals("Not Detected") ? "0" : ic.i27)) < Convert.ToDecimal(lbB35.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE36.Text = (lbB36.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i28) ? "0" : (ic.i28.Equals("Not Detected") ? "0" : ic.i28)) < Convert.ToDecimal(lbB36.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE37.Text = (lbB37.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i29) ? "0" : (ic.i29.Equals("Not Detected") ? "0" : ic.i29)) < Convert.ToDecimal(lbB37.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE38.Text = (lbB38.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i30) ? "0" : (ic.i30.Equals("Not Detected") ? "0" : ic.i30)) < Convert.ToDecimal(lbB38.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE39.Text = (lbB39.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i31) ? "0" : (ic.i31.Equals("Not Detected") ? "0" : ic.i31)) < Convert.ToDecimal(lbB39.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));
                        lbE40.Text = (lbB40.Text.Equals("NA") ? "NA" : (Convert.ToDecimal(String.IsNullOrEmpty(ic.i32) ? "0" : (ic.i32.Equals("Not Detected") ? "0" : ic.i32)) < Convert.ToDecimal(lbB40.Text.Replace('<', ' ').Trim().Split(' ')[0]) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL)));

                    }

                    //=IF(C26="NA","NA",IF(IC!J17<INDEX('Detail Spec'!$A$3:$R$379,$F$1,4),"PASS","FAIL"))
                    #endregion
                }

                ShowItem(ic.item_visible);
            }
            else
            {
                this.CommandName = CommandNameEnum.Add;
            }
            #endregion

            //init
            pRemark.Visible = false;
            pDisapprove.Visible = false;
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "JobID");
            Session.Remove(GetType().Name + "job_sample");
            Session.Remove(GetType().Name + "SampleID");
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.CommandName = (prvPage == null) ? this.CommandName : prvPage.CommandName;
            this.JobID = (prvPage == null) ? this.JobID : prvPage.JobID;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void ddlSpecification_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_m_ic_specification tem = new tb_m_ic_specification().SelectByID(int.Parse(ddlSpecification.SelectedValue));
            if (tem != null)
            {
                /*METHOD/PROCEDURE:*/
                txtC18.Text = string.Empty;//"Number of pieces used for extraction"
                txtE18.Text = string.Empty;//"Number of pieces used for extraction"
                /*RESULT*/
                lbDocRev.Text = tem.B;
                lbDesc.Text = tem.A;
                lbB24.Text = tem.C;
                lbC24.Text = lbB24.Text;
                lbB33.Text = lbB24.Text;
                lbC33.Text = lbB24.Text;

                /*Anionic*/
                lbB25.Text = tem.D;//Fluoride as F
                lbB26.Text = tem.E;//Chloride as Cl
                lbB28.Text = tem.G;//Bromide as Br
                lbB29.Text = tem.H;//Nitrate as NO3
                lbB30.Text = tem.I;//Sulphate as SO4
                lbB31.Text = tem.J;//Phosphate as PO4
                lbB32.Text = tem.K;//Total of 7Anions

                /*Cationic*/
                lbB34.Text = tem.M;//Lithium as Li
                lbB35.Text = tem.P;//Sodium as Na
                lbB36.Text = tem.L;//Ammonium as NH4
                lbB37.Text = tem.O;//Potassium as K
                lbB38.Text = tem.Q;//Magnesium as Mg
                lbB39.Text = tem.N;//Calcium as Ca
                lbB40.Text = tem.R;//Total Cations
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            Boolean isValid = true;

            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                    this.jobSample.step1owner = userLogin.id;
                    switch (this.CommandName)
                    {
                        case CommandNameEnum.Add:
                            objWork.Insert();
                            break;
                        case CommandNameEnum.Edit:
                            objWork.Update();
                            break;
                    }

                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                    StatusEnum srChemistApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (srChemistApproveStatus)
                    {
                        case StatusEnum.SR_CHEMIST_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_WORD);
                            break;
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                            break;
                    }
                    this.jobSample.step3owner = userLogin.id;
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                    StatusEnum labApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (labApproveStatus)
                    {
                        case StatusEnum.LABMANAGER_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_PDF);
                            break;
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(ddlAssignTo.SelectedValue);
                            break;
                    }
                    this.jobSample.step5owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".doc") || Path.GetExtension(btnUpload.FileName).Equals(".docx")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_word = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.LABMANAGER_CHECKING);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .doc|.docx";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step4owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".pdf")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_pdf = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.JOB_COMPLETE);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .pdf";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step6owner = userLogin.id;
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    this.jobSample.step2owner = userLogin.id;
                    break;
            }
            //Stamp date
            job_sample_logs tmp = new job_sample_logs
            {
                ID = 0,
                job_sample_id = this.jobSample.ID,
                job_status = Convert.ToInt32(this.jobSample.job_status),
                job_remark = txtRemark.Text,
                get_alerts = "0",
                date = DateTime.Now
            };
            tmp.Insert();

            this.jobSample.Update();
            //Commit
            GeneralManager.Commit();
            //Return to main.
            if (isValid)
            {
                Response.Redirect(this.PreviousPath);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void lbDownload_Click(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                case StatusEnum.SR_CHEMIST_APPROVE:
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    else
                    {
                        downloadWord();
                    }
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                case StatusEnum.LABMANAGER_APPROVE:
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (!String.IsNullOrEmpty(this.jobSample.path_pdf))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_pdf));
                    }
                    else
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
            }


        }

        private void downloadWord()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string strFileName = string.Format("{0}_{1}.doc", lbRefNo.Text.Replace("-", "_"), DateTime.Now.ToString("yyyyMMddhhmmss"));

            HttpContext.Current.Response.ContentType = "application/vnd.ms-word";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

            StringWriter sw = new StringWriter();
            HtmlTextWriter h = new HtmlTextWriter(sw);
            invDiv.RenderControl(h);
            string strHTMLContent = sw.GetStringBuilder().ToString();
            String html = "<html><header><style>body {max-width: 800px;margin:initial;font-family: \'Arial Unicode MS\';font-size: 10px;}table {border-collapse: collapse;}th {background: #666;color: #fff;border: 1px solid #999;padding: 0.5rem;text-align: center;}td { border: 1px solid #999;padding: 0.5rem;text-align: left;}h6 {font-weight:initial;}</style></header><body>" + strHTMLContent + "</body></html>";


            HttpContext.Current.Response.Write(html);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush();
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue.ToString(), true);
            switch (status)
            {
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = false;
                    break;
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = true;
                    break;
                default:
                    pRemark.Visible = false;
                    pDisapprove.Visible = false;
                    break;
            }

        }

        private String getItemStatus()
        {
            String result = String.Empty;
            result = ((CheckBox1.Checked) ? "1" : "0") +
                ((CheckBox2.Checked) ? "1" : "0") +
                 "1" +
                ((CheckBox4.Checked) ? "1" : "0") +
                ((CheckBox5.Checked) ? "1" : "0") +
                ((CheckBox6.Checked) ? "1" : "0") +
                ((CheckBox7.Checked) ? "1" : "0") +
                ((CheckBox8.Checked) ? "1" : "0") +
                ((CheckBox9.Checked) ? "1" : "0") +
                ((CheckBox10.Checked) ? "1" : "0") +
                ((CheckBox11.Checked) ? "1" : "0") +
                ((CheckBox12.Checked) ? "1" : "0") +
                ((CheckBox13.Checked) ? "1" : "0") +
                ((CheckBox14.Checked) ? "1" : "0") +
                ((CheckBox15.Checked) ? "1" : "0");
            return result;
        }

        private void ShowItem(String _itemVisible)
        {
            if (_itemVisible != null)
            {
                char[] item = _itemVisible.ToCharArray();
                if (item.Length == 15)
                {


                    StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
                    switch (status)
                    {
                        case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                            break;
                        case StatusEnum.LOGIN_SELECT_SPEC:
                            CheckBox1.Checked = item[0] == '1' ? true : false;
                            CheckBox2.Checked = item[1] == '1' ? true : false;
                            //CheckBox3.Checked = item[2] == '1' ? true : false;
                            CheckBox4.Checked = item[3] == '1' ? true : false;
                            CheckBox5.Checked = item[4] == '1' ? true : false;
                            CheckBox6.Checked = item[5] == '1' ? true : false;
                            CheckBox7.Checked = item[6] == '1' ? true : false;
                            CheckBox8.Checked = item[7] == '1' ? true : false;
                            CheckBox9.Checked = item[8] == '1' ? true : false;
                            CheckBox10.Checked = item[9] == '1' ? true : false;
                            CheckBox11.Checked = item[10] == '1' ? true : false;
                            CheckBox12.Checked = item[11] == '1' ? true : false;
                            CheckBox13.Checked = item[12] == '1' ? true : false;
                            CheckBox14.Checked = item[13] == '1' ? true : false;
                            CheckBox15.Checked = item[14] == '1' ? true : false;
                            break;
                        case StatusEnum.CHEMIST_TESTING:
                        case StatusEnum.SR_CHEMIST_CHECKING:
                        case StatusEnum.SR_CHEMIST_APPROVE:
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_WORD:
                        case StatusEnum.LABMANAGER_CHECKING:
                        case StatusEnum.LABMANAGER_APPROVE:
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_PDF:
                            tr1.Visible = item[0] == '1' ? true : false;
                            tr2.Visible = item[1] == '1' ? true : false;
                            //tr3.Visible = item[2] == '1' ? true : false;
                            tr4.Visible = item[3] == '1' ? true : false;
                            tr5.Visible = item[4] == '1' ? true : false;
                            tr6.Visible = item[5] == '1' ? true : false;
                            tr7.Visible = item[6] == '1' ? true : false;
                            tr8.Visible = item[7] == '1' ? true : false;
                            tr9.Visible = item[8] == '1' ? true : false;
                            tr10.Visible = item[9] == '1' ? true : false;
                            tr11.Visible = item[10] == '1' ? true : false;
                            tr12.Visible = item[11] == '1' ? true : false;
                            tr13.Visible = item[12] == '1' ? true : false;
                            tr14.Visible = item[13] == '1' ? true : false;
                            tr15.Visible = item[14] == '1' ? true : false;

                            th1.Visible = false;
                            td1.Visible = false;
                            td2.Visible = false;
                            //td3.Visible = false;
                            td4.Visible = false;
                            td5.Visible = false;
                            td6.Visible = false;
                            td7.Visible = false;
                            td8.Visible = false;
                            th2.Visible = false;
                            td9.Visible = false;
                            td10.Visible = false;
                            td11.Visible = false;
                            td12.Visible = false;
                            td13.Visible = false;
                            td14.Visible = false;
                            td15.Visible = false;
                            break;
                    }
                }
            }

        }
    
    }
}