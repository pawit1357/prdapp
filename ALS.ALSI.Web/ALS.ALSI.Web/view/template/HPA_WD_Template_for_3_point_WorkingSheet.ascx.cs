﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using ALS.ALSI.Web.view.request;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;

namespace ALS.ALSI.Web.view.template
{
    public partial class HPA_WD_Template_for_3_point_WorkingSheet : System.Web.UI.UserControl
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(HPA_WD_Template_for_1_point_WorkingSheet));

        #region "Property"

        public template_wd_hpa_for3_coverpage HpaFor3
        {
            get { return (template_wd_hpa_for3_coverpage)Session[GetType().Name + "HpaFor3"]; }
            set { Session[GetType().Name + "HpaFor3"] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "HpaFor3");
            Session.Remove(GetType().Name + Constants.PREVIOUS_PATH);
            Session.Remove(GetType().Name + "SampleID");
        }

        private void initialPage()
        {
            this.HpaFor3 = new template_wd_hpa_for3_coverpage().SelectBySampleID(this.SampleID);
            if (this.HpaFor3 != null)
            {

                CalculateCas();
            }
            //initial component
            btnSubmit.Enabled = false;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            job_sample sample = new job_sample().SelectByID(this.SampleID);
            sample.job_status = Convert.ToInt32(StatusEnum.SR_CHEMIST_CHECKING);
            sample.Update();

            this.HpaFor3.Update();
            //Commit
            GeneralManager.Commit();

            Response.Redirect(this.PreviousPath);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void btnLoadFile_Click(object sender, EventArgs e)
        {
            #region "LOAD"
            String yyyMMdd = DateTime.Now.ToString("yyyyMMdd");

            List<String> imgs = new List<string>();
            List<RawDataArmForHpa3> listDA = new List<RawDataArmForHpa3>();
            List<RawDataPivotForHpa3> listPV = new List<RawDataPivotForHpa3>();
            List<RawDataSwageForHpa3> listSW = new List<RawDataSwageForHpa3>();
            for (int i = 0; i < btnUpload.PostedFiles.Count; i++)
            {
                HttpPostedFile _postedFile = btnUpload.PostedFiles[i];
                try
                {
                    if (_postedFile.ContentLength > 0)
                    {
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, _postedFile.FileName);
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }

                        _postedFile.SaveAs(savefilePath);

                        if (Path.GetExtension(savefilePath).Equals(".jpg"))
                        {
                            //Set image path
                            imgs.Add(String.Format("{0}/{1}", yyyMMdd, Path.GetFileName(savefilePath)));
                        }
                        else
                        {

                            using (FileStream fs = new FileStream(savefilePath, FileMode.Open, FileAccess.Read))
                            {
                                HSSFWorkbook wd = new HSSFWorkbook(fs);
                                //- โหลด Component,detail Spec
                                //- เหลือทำหน้า cover Page column C
                                //- Find Value from list listDA,listPV,listSW

                                #region "Raw Data-Arm"
                                ISheet sheet = wd.GetSheet("Raw Data-Arm");
                                if (sheet != null)
                                {
                                    for (int row = 0; row <= sheet.LastRowNum; row++)
                                    {

                                        RawDataArmForHpa3 rawDataArm = new RawDataArmForHpa3();
                                        rawDataArm.Feature = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(0));
                                        rawDataArm.Area = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(1));
                                        rawDataArm.Field = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(2));
                                        rawDataArm.Rank = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(3));
                                        rawDataArm.Fe_Cr = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(4));
                                        rawDataArm.Fe_Cr_Ni_Mn = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(5));
                                        rawDataArm.Cu_S_Al_O_Base = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(6));
                                        rawDataArm.Ti_O_Al_Si_Fe = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(7));
                                        rawDataArm.Sn_base = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(8));
                                        rawDataArm.Other = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(9));
                                        rawDataArm.Al_Ti = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(10));
                                        rawDataArm.Al_Si_Based = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(11));
                                        rawDataArm.Al_Si_Mg = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(12));
                                        rawDataArm.Ca_base = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(13));
                                        rawDataArm.Au_Rejected_Manual = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(14));
                                        rawDataArm.Rejected_Morph = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(15));
                                        rawDataArm.Area_sq_µm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(16));
                                        rawDataArm.Aspect_Ratio = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(17));
                                        rawDataArm.Beam_X_pixels = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(18));
                                        rawDataArm.Beam_Y_pixels = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(19));
                                        rawDataArm.Breadth = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(20));
                                        rawDataArm.Direction_degrees = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(21));
                                        rawDataArm.ECD = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(22));
                                        rawDataArm.Length = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(23));
                                        rawDataArm.Perimeter = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(24));
                                        rawDataArm.Shape = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(25));
                                        rawDataArm.Mean_grey = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(26));
                                        rawDataArm.Spectrum_Area = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(27));
                                        rawDataArm.Stage_X = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(28));
                                        rawDataArm.Stage_Y = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(29));
                                        rawDataArm.Stage_Z = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(30));
                                        rawDataArm.O_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(31));
                                        rawDataArm.Mg_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(32));
                                        rawDataArm.Al_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(33));
                                        rawDataArm.Si_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(34));
                                        rawDataArm.S_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(35));
                                        rawDataArm.Cl_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(36));
                                        rawDataArm.K_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(37));
                                        rawDataArm.Ca_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(38));
                                        rawDataArm.Ti_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(39));
                                        rawDataArm.V_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(40));
                                        rawDataArm.Cr_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(41));
                                        rawDataArm.Mn_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(42));
                                        rawDataArm.Fe_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(43));
                                        rawDataArm.Ni_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(44));
                                        rawDataArm.Cu_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(45));
                                        rawDataArm.Zn_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(46));
                                        rawDataArm.Br_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(47));
                                        rawDataArm.Sn_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(48));
                                        rawDataArm.Au_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(49));
                                        listDA.Add(rawDataArm);

                                    }
                                }
                                #endregion
                                #region "Raw Data-Pivot"
                                ISheet sheetPV = wd.GetSheet("Raw Data-Pivot");
                                if (sheetPV != null)
                                {
                                    for (int row = 0; row <= sheetPV.LastRowNum; row++)
                                    {

                                        RawDataPivotForHpa3 rawDataPV = new RawDataPivotForHpa3();
                                        rawDataPV.Feature = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(0));
                                        rawDataPV.Area_Field = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(1));
                                        rawDataPV.Rank = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(2));
                                        rawDataPV.Al_Ti_O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(3));
                                        rawDataPV.Al_Si_O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(4));
                                        rawDataPV.Si_O_Ti_C = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(5));
                                        rawDataPV.Ti_O_Al_O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(6));
                                        rawDataPV.Fe_Cr_ = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(7));
                                        rawDataPV.Fe_Cr_Ni_ = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(8));
                                        rawDataPV.Fe_Cr_Ni_Si = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(9));
                                        rawDataPV.Fe_Cr_Ni_Mn = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(10));
                                        rawDataPV.Fe_O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(11));
                                        rawDataPV.No_Element = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(12));
                                        rawDataPV.Ti_O_Al_Si_Fe = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(13));
                                        rawDataPV.Ni = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(14));
                                        rawDataPV.Sn_base = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(15));
                                        rawDataPV.Other = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(16));
                                        rawDataPV.Al = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(17));
                                        rawDataPV.Al_Mg = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(18));
                                        rawDataPV.Al_Si_Based = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(19));
                                        rawDataPV.Al_Si_Cu = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(20));
                                        rawDataPV.Al_Si_Fe = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(21));
                                        rawDataPV.Al_Si_Mg = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(22));
                                        rawDataPV.Zn_base = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(23));
                                        rawDataPV.Ca_base = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(24));
                                        rawDataPV.Ni_base = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(25));
                                        rawDataPV.Cl_base = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(26));
                                        rawDataPV.Cu = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(27));
                                        rawDataPV.Au_Ni = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(28));
                                        rawDataPV.Au = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(29));
                                        rawDataPV.Cu_Zn = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(30));
                                        rawDataPV.F_O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(31));
                                        rawDataPV.Al_Si1 = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(32));
                                        rawDataPV.Al_Si2 = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(33));
                                        rawDataPV.Rejected_Manual = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(34));
                                        rawDataPV.Rejected_ED = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(35));
                                        rawDataPV.Rejected_Morph = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(36));
                                        rawDataPV.Area_squm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(37));
                                        rawDataPV.Aspect_Ratio = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(38));
                                        rawDataPV.Beam_X_pixels = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(39));
                                        rawDataPV.Beam_Y_pixels = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(40));
                                        rawDataPV.Breadth = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(41));
                                        rawDataPV.Direction_degrees = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(42));
                                        rawDataPV.ECD = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(43));
                                        rawDataPV.Length = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(44));
                                        rawDataPV.Perimeter = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(45));
                                        rawDataPV.Shape = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(46));
                                        rawDataPV.Mean_grey_ = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(47));
                                        rawDataPV.Spectrum_Area = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(48));
                                        rawDataPV.Stage_X = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(49));
                                        rawDataPV.Stage_Y = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(50));
                                        rawDataPV.Stage_Z = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(51));
                                        rawDataPV.O_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(52));
                                        rawDataPV.F_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(53));
                                        rawDataPV.Na_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(54));
                                        rawDataPV.Mg_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(55));
                                        rawDataPV.Al_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(56));
                                        rawDataPV.Si_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(57));
                                        rawDataPV.P_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(58));
                                        rawDataPV.S_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(59));
                                        rawDataPV.Cl_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(60));
                                        rawDataPV.Ar_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(61));
                                        rawDataPV.K_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(62));
                                        rawDataPV.Ca_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(63));
                                        rawDataPV.Ti_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(64));
                                        rawDataPV.V_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(65));
                                        rawDataPV.Cr_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(66));
                                        rawDataPV.Mn_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(67));
                                        rawDataPV.Fe_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(68));
                                        rawDataPV.Co_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(69));
                                        rawDataPV.Ni_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(70));
                                        rawDataPV.Cu_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(71));
                                        rawDataPV.Zn_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(72));
                                        rawDataPV.Br_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(73));
                                        rawDataPV.Mo_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(74));
                                        rawDataPV.Ag_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(75));
                                        rawDataPV.Sn_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(76));
                                        rawDataPV.Sb_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(77));
                                        rawDataPV.Ba_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(78));
                                        rawDataPV.W_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(79));
                                        rawDataPV.Au_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(80));
                                        rawDataPV.Pb_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(81));
                                        rawDataPV.Ra_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(82));
                                        rawDataPV.Np_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(83));

                                        listPV.Add(rawDataPV);

                                    }
                                }
                                #endregion
                                #region "Raw Data-Swage"
                                ISheet sheetSW = wd.GetSheet("Raw Data-Swage");
                                if (sheetSW != null)
                                {
                                    for (int row = 0; row <= sheetSW.LastRowNum; row++)
                                    {
                                        RawDataSwageForHpa3 rawDataSW = new RawDataSwageForHpa3();
                                        rawDataSW.Feature = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(0));
                                        rawDataSW.Area = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(1));
                                        rawDataSW.Field = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(2));
                                        rawDataSW.Rank = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(3));
                                        rawDataSW.Al_O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(4));
                                        rawDataSW.Fe_Cr = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(5));
                                        rawDataSW.Fe_Cr_Ni = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(6));
                                        rawDataSW.Fe_Cr_Ni_Si = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(7));
                                        rawDataSW.Fe_Cr_Ni_Mn = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(8));
                                        rawDataSW.Fe = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(9));
                                        rawDataSW.Fe_O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(10));
                                        rawDataSW.Ti_O_Al_Si_Fe = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(11));
                                        rawDataSW.Sn_base = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(12));
                                        rawDataSW.Other = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(13));
                                        rawDataSW.Al = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(14));
                                        rawDataSW.Al_Mg = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(15));
                                        rawDataSW.Al_Ti = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(16));
                                        rawDataSW.Al_Si_Based = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(17));
                                        rawDataSW.Al_Si_Cu = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(18));
                                        rawDataSW.Al_Si_Mg = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(19));
                                        rawDataSW.Au = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(20));
                                        rawDataSW.Cu_Zn = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(21));
                                        rawDataSW.Al_Si2 = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(22));
                                        rawDataSW.Rejected_Manual = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(23));
                                        rawDataSW.Rejected_ED = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(24));
                                        rawDataSW.Rejected_Morph = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(25));
                                        rawDataSW.Area_squm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(26));
                                        rawDataSW.Aspect_Ratio = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(27));
                                        rawDataSW.Beam_X = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(28));
                                        rawDataSW.Beam_Y = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(29));
                                        rawDataSW.Breadth = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(30));
                                        rawDataSW.Direction_degrees = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(31));
                                        rawDataSW.ECD = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(32));
                                        rawDataSW.Length = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(33));
                                        rawDataSW.Perimeter = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(34));
                                        rawDataSW.Shape = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(35));
                                        rawDataSW.Mean_grey = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(36));
                                        rawDataSW.Spectrum_Area = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(37));
                                        rawDataSW.Stage_X = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(38));
                                        rawDataSW.Stage_Y = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(39));
                                        rawDataSW.Stage_Z = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(40));
                                        rawDataSW.O_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(41));
                                        rawDataSW.F_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(42));
                                        rawDataSW.Na_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(43));
                                        rawDataSW.Mg_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(44));
                                        rawDataSW.Al_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(45));
                                        rawDataSW.Si_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(46));
                                        rawDataSW.P_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(47));
                                        rawDataSW.S_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(48));
                                        rawDataSW.Cl_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(49));
                                        rawDataSW.K_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(50));
                                        rawDataSW.Ca_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(51));
                                        rawDataSW.Ti_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(52));
                                        rawDataSW.V_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(53));
                                        rawDataSW.Cr_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(54));
                                        rawDataSW.Mn_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(55));
                                        rawDataSW.Fe_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(56));
                                        rawDataSW.Ni_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(57));
                                        rawDataSW.Cu_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(58));
                                        rawDataSW.Zn_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(59));
                                        rawDataSW.Br_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(60));
                                        rawDataSW.Mo_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(61));
                                        rawDataSW.Ag_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(62));
                                        rawDataSW.Sn_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(63));
                                        rawDataSW.Au_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(64));

                                        listSW.Add(rawDataSW);

                                    }
                                }
                                #endregion

                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    logger.Error(Ex.Message);
                    Console.WriteLine();
                }
            }

            #region "Images"
            if (imgs.Count == 3)
            {
                this.HpaFor3.img_path1 = imgs[0];
                this.HpaFor3.img_path2 = imgs[1];
                this.HpaFor3.img_path3 = imgs[2];
            }
            #endregion
            #endregion

            #region "SET DATA TO FORM"
            //this.HpaFor3.ws_b53 = listDA.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Al_Ti_O)).ToString();//Al-Ti-O (0.5<=ECD<=2.0 um)
            /// ===================================== ///
            this.HpaFor3.ws_b57 = "0";//Al-O
            this.HpaFor3.ws_b58 = "0";//Al-Si-O
            this.HpaFor3.ws_b59 = "0";//Si-O
            this.HpaFor3.ws_b60 = "0";//Si-C
            this.HpaFor3.ws_b61 = "0";//Al-Cu-O
            this.HpaFor3.ws_b62 = "0";//Al-Mg-O
            this.HpaFor3.ws_b63 = "0";//Al-Si-Cu-O
            this.HpaFor3.ws_b64 = "0";//Al-Si-Fe-O
            this.HpaFor3.ws_b65 = "0";//Al-Si-Mg-O
            this.HpaFor3.ws_b66 = "0";//Al-Ti-O
            this.HpaFor3.ws_b67 = "0";//Ti-O
            this.HpaFor3.ws_b68 = "0";//Ti-C
            this.HpaFor3.ws_b69 = "0";//Ti-B
            this.HpaFor3.ws_b70 = "0";//Ti-N
            this.HpaFor3.ws_b71 = "0";//W-O
            this.HpaFor3.ws_b72 = "0";//W-C
            this.HpaFor3.ws_b73 = "0";//Zr-O
            this.HpaFor3.ws_b74 = "0";//Zr-C
            this.HpaFor3.ws_b75 = "0";//Pb-Zr-Ti-O
            //Subtotal - Hard Particles Only
            this.HpaFor3.ws_b78 = "0";//Ce-Co
            this.HpaFor3.ws_b79 = "0";//Fe-Nd
            this.HpaFor3.ws_b80 = "0";//Fe-Sm
            this.HpaFor3.ws_b81 = "0";//Fe-Sr
            this.HpaFor3.ws_b82 = "0";//Nd-Pr
            this.HpaFor3.ws_b83 = "0";//Ni-Co
            this.HpaFor3.ws_b84 = "0";//Sm-Co
            //Subtotal - Magnetic Particles Only
            //Subtotal- Hard Particles including Magnetic Partilces
            this.HpaFor3.ws_b102 = "0";//SS 300- Fe-Cr-Ni
            this.HpaFor3.ws_b103 = "0";//SS 300- Fe-Cr-Ni-Mn
            this.HpaFor3.ws_b104 = "0";//SS 300- Fe-Cr-Ni-Si
            this.HpaFor3.ws_b105 = "0";//SS 400- Fe-Cr 
            this.HpaFor3.ws_b106 = "0";//SS 400- Fe-Cr-Mn
            this.HpaFor3.ws_b107 = "0";//Other Steel - Fe
            this.HpaFor3.ws_b108 = "0";//Other Steel - Fe-Mn
            this.HpaFor3.ws_b109 = "0";//Other Steel - Fe-Ni
            this.HpaFor3.ws_b110 = "0";//Other Steel - Fe-O
            //Subtotal - Steel Particle Only
            this.HpaFor3.ws_b113 = "0";//No Element
            this.HpaFor3.ws_b114 = "0";//Ni
            this.HpaFor3.ws_b115 = "0";//Ni-P
            this.HpaFor3.ws_b116 = "0";//Sn base
            this.HpaFor3.ws_b117 = "0";//Other
            this.HpaFor3.ws_b118 = "0";//Al
            this.HpaFor3.ws_b119 = "0";//Al-Mg
            this.HpaFor3.ws_b120 = "0";//Al-Ti
            this.HpaFor3.ws_b121 = "0";//Al-Si (1)
            this.HpaFor3.ws_b122 = "0";//Al-Si (2)
            this.HpaFor3.ws_b123 = "0";//Si
            this.HpaFor3.ws_b124 = "0";//Al-Cu
            this.HpaFor3.ws_b125 = "0";//Al-Si-Cu
            this.HpaFor3.ws_b126 = "0";//Al-Si-Fe
            this.HpaFor3.ws_b127 = "0";//Al-Si-Mg
            this.HpaFor3.ws_b128 = "0";//Mg-Si-O-Al
            this.HpaFor3.ws_b129 = "0";//Mg-Si-O
            this.HpaFor3.ws_b130 = "0";//Ti
            this.HpaFor3.ws_b131 = "0";//Nd
            this.HpaFor3.ws_b132 = "0";//S-Cr-Mn
            this.HpaFor3.ws_b133 = "0";//Zn base
            this.HpaFor3.ws_b134 = "0";//Ca base
            this.HpaFor3.ws_b135 = "0";//Ni base
            this.HpaFor3.ws_b136 = "0";//Cr base
            this.HpaFor3.ws_b137 = "0";//Zr base
            this.HpaFor3.ws_b138 = "0";//Cl base
            this.HpaFor3.ws_b139 = "0";//Na-Cl
            this.HpaFor3.ws_b140 = "0";//Cu
            this.HpaFor3.ws_b141 = "0";//Cu-Au
            this.HpaFor3.ws_b142 = "0";//Ag-S
            this.HpaFor3.ws_b143 = "0";//Au-Ni
            this.HpaFor3.ws_b144 = "0";//Ag
            this.HpaFor3.ws_b145 = "0";//Au
            this.HpaFor3.ws_b146 = "0";//Cu-Au-Ni
            this.HpaFor3.ws_b147 = "0";//Cu-Zn
            this.HpaFor3.ws_b148 = "0";//Cu-Zn-Ni
            this.HpaFor3.ws_b149 = "0";//Cu-Zn-Au-Ni
            this.HpaFor3.ws_b150 = "0";//Zn-O
            this.HpaFor3.ws_b151 = "0";//Pb
            this.HpaFor3.ws_b152 = "0";//Fe-Cu
            this.HpaFor3.ws_b153 = "0";//Cr-Mn
            this.HpaFor3.ws_b154 = "0";//Al-Si Base
            this.HpaFor3.ws_b155 = "0";//F-O
            this.HpaFor3.ws_b156 = "0";//Cu-S-Al-O Base
            this.HpaFor3.ws_b157 = "0";//Ti-O/Al-Si-Fe
            this.HpaFor3.ws_b158 = "0";//Cr-Rich (Cr Base + Cr-Mn)
            //Subtotal - Other Particle Only
            //Grand Total of Particles
            this.HpaFor3.ws_b173 = "0";//Al-O
            this.HpaFor3.ws_b174 = "0";//Al-Si-O
            this.HpaFor3.ws_b175 = "0";//Si-O
            this.HpaFor3.ws_b176 = "0";//Si-C
            this.HpaFor3.ws_b177 = "0";//Al-Cu-O
            this.HpaFor3.ws_b178 = "0";//Al-Mg-O
            this.HpaFor3.ws_b179 = "0";//Al-Si-Cu-O
            this.HpaFor3.ws_b180 = "0";//Al-Si-Fe-O
            this.HpaFor3.ws_b181 = "0";//Al-Si-Mg-O
            this.HpaFor3.ws_b182 = "0";//Al-Ti-O
            this.HpaFor3.ws_b183 = "0";//Ti-O
            this.HpaFor3.ws_b184 = "0";//Ti-C
            this.HpaFor3.ws_b185 = "0";//Ti-B
            this.HpaFor3.ws_b186 = "0";//Ti-N
            this.HpaFor3.ws_b187 = "0";//W-O
            this.HpaFor3.ws_b188 = "0";//W-C
            this.HpaFor3.ws_b189 = "0";//Zr-O
            this.HpaFor3.ws_b190 = "0";//Zr-C
            this.HpaFor3.ws_b191 = "0";//Pb-Zr-Ti-O
            //Subtotal - Hard Particles Only
            this.HpaFor3.ws_b194 = "0";//Ce-Co
            this.HpaFor3.ws_b195 = "0";//Fe-Nd
            this.HpaFor3.ws_b196 = "0";//Fe-Sm
            this.HpaFor3.ws_b197 = "0";//Fe-Sr
            this.HpaFor3.ws_b198 = "0";//Nd-Pr
            this.HpaFor3.ws_b199 = "0";//Ni-Co
            this.HpaFor3.ws_b200 = "0";//Sm-Co
            //Subtotal - Magnetic Particles Only
            //Subtotal- Hard Particles including Magnetic Partilces
            this.HpaFor3.ws_b218 = "0";//SS 300- Fe-Cr-Ni
            this.HpaFor3.ws_b219 = "0";//SS 300- Fe-Cr-Ni-Mn
            this.HpaFor3.ws_b220 = "0";//SS 300- Fe-Cr-Ni-Si
            this.HpaFor3.ws_b221 = "0";//SS 400- Fe-Cr 
            this.HpaFor3.ws_b222 = "0";//SS 400- Fe-Cr-Mn
            this.HpaFor3.ws_b223 = "0";//Other Steel - Fe
            this.HpaFor3.ws_b224 = "0";//Other Steel - Fe-Mn
            this.HpaFor3.ws_b225 = "0";//Other Steel - Fe-Ni
            this.HpaFor3.ws_b226 = "0";//Other Steel - Fe-O
            //Subtotal - Steel Particle Only
            this.HpaFor3.ws_b229 = "0";//No Element
            this.HpaFor3.ws_b230 = "0";//Ni
            this.HpaFor3.ws_b231 = "0";//Ni-P
            this.HpaFor3.ws_b232 = "0";//Sn base
            this.HpaFor3.ws_b233 = "0";//Other
            this.HpaFor3.ws_b234 = "0";//Al
            this.HpaFor3.ws_b235 = "0";//Al-Mg
            this.HpaFor3.ws_b236 = "0";//Al-Ti
            this.HpaFor3.ws_b237 = "0";//Al-Si (1)
            this.HpaFor3.ws_b238 = "0";//Al-Si (2)
            this.HpaFor3.ws_b239 = "0";//Si
            this.HpaFor3.ws_b240 = "0";//Al-Cu
            this.HpaFor3.ws_b241 = "0";//Al-Si-Cu
            this.HpaFor3.ws_b242 = "0";//Al-Si-Fe
            this.HpaFor3.ws_b243 = "0";//Al-Si-Mg
            this.HpaFor3.ws_b244 = "0";//Mg-Si-O-Al
            this.HpaFor3.ws_b245 = "0";//Mg-Si-O
            this.HpaFor3.ws_b246 = "0";//Ti
            this.HpaFor3.ws_b247 = "0";//Nd
            this.HpaFor3.ws_b248 = "0";//S-Cr-Mn
            this.HpaFor3.ws_b249 = "0";//Zn base
            this.HpaFor3.ws_b250 = "0";//Ca base
            this.HpaFor3.ws_b251 = "0";//Ni base
            this.HpaFor3.ws_b252 = "0";//Cr base
            this.HpaFor3.ws_b253 = "0";//Zr base
            this.HpaFor3.ws_b254 = "0";//Cl base
            this.HpaFor3.ws_b255 = "0";//Na-Cl
            this.HpaFor3.ws_b256 = "0";//Cu
            this.HpaFor3.ws_b257 = "0";//Cu-Au
            this.HpaFor3.ws_b258 = "0";//Ag-S
            this.HpaFor3.ws_b259 = "0";//Au-Ni
            this.HpaFor3.ws_b260 = "0";//Ag
            this.HpaFor3.ws_b261 = "0";//Au
            this.HpaFor3.ws_b262 = "0";//Cu-Au-Ni
            this.HpaFor3.ws_b263 = "0";//Cu-Zn
            this.HpaFor3.ws_b264 = "0";//Cu-Zn-Ni
            this.HpaFor3.ws_b265 = "0";//Cu-Zn-Au-Ni
            this.HpaFor3.ws_b266 = "0";//Zn-O
            this.HpaFor3.ws_b267 = "0";//Pb
            this.HpaFor3.ws_b268 = "0";//Fe-Cu
            this.HpaFor3.ws_b269 = "0";//Cr-Mn
            this.HpaFor3.ws_b270 = "0";//Al-Si Base
            this.HpaFor3.ws_b271 = "0";//F-O
            this.HpaFor3.ws_b272 = "0";//Cu-S-Al-O Base
            this.HpaFor3.ws_b273 = "0";//Ti-O/Al-Si-Fe
            this.HpaFor3.ws_b274 = "0";//Cr-Rich (Cr Base + Cr-Mn)
            //Subtotal - Other Particle Only
            //Grand Total of Particles
            this.HpaFor3.ws_b289 = "0";//Al-O
            this.HpaFor3.ws_b290 = "0";//Al-Si-O
            this.HpaFor3.ws_b291 = "0";//Si-O
            this.HpaFor3.ws_b292 = "0";//Si-C
            this.HpaFor3.ws_b293 = "0";//Al-Cu-O
            this.HpaFor3.ws_b294 = "0";//Al-Mg-O
            this.HpaFor3.ws_b295 = "0";//Al-Si-Cu-O
            this.HpaFor3.ws_b296 = "0";//Al-Si-Fe-O
            this.HpaFor3.ws_b297 = "0";//Al-Si-Mg-O
            this.HpaFor3.ws_b298 = "0";//Al-Ti-O
            this.HpaFor3.ws_b299 = "0";//Ti-O
            this.HpaFor3.ws_b300 = "0";//Ti-C
            this.HpaFor3.ws_b301 = "0";//Ti-B
            this.HpaFor3.ws_b302 = "0";//Ti-N
            this.HpaFor3.ws_b303 = "0";//W-O
            this.HpaFor3.ws_b304 = "0";//W-C
            this.HpaFor3.ws_b305 = "0";//Zr-O
            this.HpaFor3.ws_b306 = "0";//Zr-C
            this.HpaFor3.ws_b307 = "0";//Pb-Zr-Ti-O
            //Subtotal - Hard Particles Only
            this.HpaFor3.ws_b310 = "0";//Ce-Co
            this.HpaFor3.ws_b311 = "0";//Fe-Nd
            this.HpaFor3.ws_b312 = "0";//Fe-Sm
            this.HpaFor3.ws_b313 = "0";//Fe-Sr
            this.HpaFor3.ws_b314 = "0";//Nd-Pr
            this.HpaFor3.ws_b315 = "0";//Ni-Co
            this.HpaFor3.ws_b316 = "0";//Sm-Co
            //Subtotal - Magnetic Particles Only
            //Subtotal- Hard Particles including Magnetic Partilces
            this.HpaFor3.ws_b334 = "0";//SS 300- Fe-Cr-Ni
            this.HpaFor3.ws_b335 = "0";//SS 300- Fe-Cr-Ni-Mn
            this.HpaFor3.ws_b336 = "0";//SS 300- Fe-Cr-Ni-Si
            this.HpaFor3.ws_b337 = "0";//SS 400- Fe-Cr 
            this.HpaFor3.ws_b338 = "0";//SS 400- Fe-Cr-Mn
            this.HpaFor3.ws_b339 = "0";//Other Steel - Fe
            this.HpaFor3.ws_b340 = "0";//Other Steel - Fe-Mn
            this.HpaFor3.ws_b341 = "0";//Other Steel - Fe-Ni
            this.HpaFor3.ws_b342 = "0";//Other Steel - Fe-O
            //Subtotal - Steel Particle Only
            this.HpaFor3.ws_b345 = "0";//No Element
            this.HpaFor3.ws_b346 = "0";//Ni
            this.HpaFor3.ws_b347 = "0";//Ni-P
            this.HpaFor3.ws_b348 = "0";//Sn base
            this.HpaFor3.ws_b349 = "0";//Other
            this.HpaFor3.ws_b350 = "0";//Al
            this.HpaFor3.ws_b351 = "0";//Al-Mg
            this.HpaFor3.ws_b352 = "0";//Al-Ti
            this.HpaFor3.ws_b353 = "0";//Al-Si (1)
            this.HpaFor3.ws_b354 = "0";//Al-Si (2)
            this.HpaFor3.ws_b355 = "0";//Si
            this.HpaFor3.ws_b356 = "0";//Al-Cu
            this.HpaFor3.ws_b357 = "0";//Al-Si-Cu
            this.HpaFor3.ws_b358 = "0";//Al-Si-Fe
            this.HpaFor3.ws_b359 = "0";//Al-Si-Mg
            this.HpaFor3.ws_b360 = "0";//Mg-Si-O-Al
            this.HpaFor3.ws_b361 = "0";//Mg-Si-O
            this.HpaFor3.ws_b362 = "0";//Ti
            this.HpaFor3.ws_b363 = "0";//Nd
            this.HpaFor3.ws_b364 = "0";//S-Cr-Mn
            this.HpaFor3.ws_b365 = "0";//Zn base
            this.HpaFor3.ws_b366 = "0";//Ca base
            this.HpaFor3.ws_b367 = "0";//Ni base
            this.HpaFor3.ws_b368 = "0";//Cr base
            this.HpaFor3.ws_b369 = "0";//Zr base
            this.HpaFor3.ws_b370 = "0";//Cl base
            this.HpaFor3.ws_b371 = "0";//Na-Cl
            this.HpaFor3.ws_b372 = "0";//Cu
            this.HpaFor3.ws_b373 = "0";//Cu-Au
            this.HpaFor3.ws_b374 = "0";//Ag-S
            this.HpaFor3.ws_b375 = "0";//Au-Ni
            this.HpaFor3.ws_b376 = "0";//Ag
            this.HpaFor3.ws_b377 = "0";//Au
            this.HpaFor3.ws_b378 = "0";//Cu-Au-Ni
            this.HpaFor3.ws_b379 = "0";//Cu-Zn
            this.HpaFor3.ws_b380 = "0";//Cu-Zn-Ni
            this.HpaFor3.ws_b381 = "0";//Cu-Zn-Au-Ni
            this.HpaFor3.ws_b382 = "0";//Zn-O
            this.HpaFor3.ws_b383 = "0";//Pb
            this.HpaFor3.ws_b384 = "0";//Fe-Cu
            this.HpaFor3.ws_b385 = "0";//Cr-Mn
            this.HpaFor3.ws_b386 = "0";//Al-Si Base
            this.HpaFor3.ws_b387 = "0";//F-O
            this.HpaFor3.ws_b388 = "0";//Cu-S-Al-O Base
            this.HpaFor3.ws_b389 = "0";//Ti-O/Al-Si-Fe
            this.HpaFor3.ws_b390 = "0";//Cr-Rich (Cr Base + Cr-Mn)
            //Subtotal - OrtherParticle Only
            //Grand Total of Particles
            #endregion
            CalculateCas();
            btnSubmit.Enabled = true;

        }




        #region "Custom method"

        private String validateDSHFile(IList<HttpPostedFile> _files)
        {
            Boolean isFound_b1 = false;
            Boolean isFound_s1 = false;
            Boolean isFound_hb1 = false;
            Boolean isFound_hs1 = false;
            Boolean isFoundWrongExtension = false;

            String result = String.Empty;

            String[] files = new String[_files.Count];
            if (files.Length == 4)
            {
                for (int i = 0; i < _files.Count; i++)
                {
                    files[i] = _files[i].FileName;
                    if (!Path.GetExtension(_files[i].FileName).Trim().ToLower().Equals(".xls"))
                    {
                        isFoundWrongExtension = true;
                        break;
                    }
                }
                if (!isFoundWrongExtension)
                {

                    //Find B1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B"))
                        {
                            isFound_b1 = true;
                            break;
                        }
                    }

                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S"))
                        {
                            isFound_s1 = true;
                            break;
                        }
                    }

                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("HPA(B)"))
                        {
                            isFound_hb1 = true;
                            break;
                        }
                    }

                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("HPA(S)"))
                        {
                            isFound_hs1 = true;
                            break;
                        }
                    }
                    result = (!isFound_b1) ? result += "File not found B.xls" :
                                (!isFound_s1) ? result += "File not found S.xls" :
                                (!isFound_hb1) ? result += "File not found HPA(B).xls" :
                                (!isFound_hs1) ? result += "File not found HPA(S)xls" : String.Empty;
                }
                else
                {
                    result = "File extension must be *.xls";
                }
            }
            else
            {
                result = "You must to select 4 files for upload.";
            }
            return result;
        }

        private void CalculateCas()
        {

        }

        #endregion

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            CalculateCas();
            btnSubmit.Enabled = true;
        }

    }
}