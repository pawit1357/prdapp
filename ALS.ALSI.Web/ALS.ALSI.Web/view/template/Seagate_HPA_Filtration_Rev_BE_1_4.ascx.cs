﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using ALS.ALSI.Web.view.request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ALS.ALSI.Web.view.template
{
    public partial class Seagate_HPA_Filtration_Rev_BE_1_4 : System.Web.UI.UserControl
    {

        #region "Property"

        public user_login userLogin
        {
            get
            {
                return ((Session[Constants.SESSION_USER] != null) ? (user_login)Session[Constants.SESSION_USER] : null);
            }
        }

        public job_sample jobSample
        {
            get { return (job_sample)Session["job_sample"]; }
            set { Session["job_sample"] = value; }
        }

        public CommandNameEnum CommandName
        {
            get { return (CommandNameEnum)ViewState[Constants.COMMAND_NAME]; }
            set { ViewState[Constants.COMMAND_NAME] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int JobID
        {
            get { return (int)Session[GetType().Name + "JobID"]; }
            set { Session[GetType().Name + "JobID"] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        public List<template_seagate_hpa_coverpage> Hpas
        {
            get { return (List<template_seagate_hpa_coverpage>)Session[GetType().Name + "Hpas"]; }
            set { Session[GetType().Name + "Hpas"] = value; }
        }

        private void initialPage()
        {
            ddlAssignTo.Items.Clear();
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LOGIN_SELECT_SPEC), Convert.ToInt16(StatusEnum.LOGIN_SELECT_SPEC) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.CHEMIST_TESTING), Convert.ToInt16(StatusEnum.CHEMIST_TESTING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_CHECKING), Convert.ToInt16(StatusEnum.SR_CHEMIST_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_WORD), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_WORD) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_CHECKING), Convert.ToInt16(StatusEnum.LABMANAGER_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_PDF), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_PDF) + ""));
            //ddlAssignTo.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, ""));


            ddlSpecification.Items.Clear();
            ddlSpecification.DataSource = new tb_m_hpa_specification().SelectBySpecificationID(Convert.ToInt16(SpecificationEnum.SEAGATE));
            ddlSpecification.DataBind();
            ddlSpecification.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));

            /*INFO*/
            #region "job Info"
            job_info job = new job_info().SelectByID(this.JobID);

            lbPoNo.Text = (job.customer_po_ref == null) ? String.Empty : job.customer_po_ref.ToString();
            lbDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
            lbCompany.Text = String.Format("{0}<br />{1}", job.m_customer.company_name, job.m_customer.address);
            lbDateSampleReceived.Text = Convert.ToDateTime(job.date_of_receive).ToString("MM/dd/yyyy");

            this.jobSample = new job_sample().SelectByID(this.SampleID);

            if (this.jobSample != null)
            {


                lbRefNo.Text = this.jobSample.job_number.ToString();
                lbDownloadName.Text = this.jobSample.job_number.ToString();
                lbDateTestCompleted.Text = Convert.ToDateTime(this.jobSample.due_date).ToString("MM/dd/yyyy");

                lbSampleDescription.Text = String.Format("Description:{0}<br />Model:{1}<br />Surface Area:{2}<br />Remark:{3}<br />", this.jobSample.description, this.jobSample.model, this.jobSample.surface_area, this.jobSample.remarks);

                RoleEnum userRole = (RoleEnum)Enum.Parse(typeof(RoleEnum), userLogin.role_id.ToString(), true);

                StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);

                lbJobStatus.Text = Constants.GetEnumDescription(status);
                ddlStatus.Items.Clear();
                switch (status)
                {
                    case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                        break;
                    case StatusEnum.LOGIN_SELECT_SPEC:
                        pSpecification.Visible = true;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.LOGIN) ? ((this.jobSample.step1owner == null) ? true : ((this.jobSample.step1owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.CHEMIST_TESTING:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.CHEMIST) ? ((this.jobSample.step2owner == null) ? true : ((this.jobSample.step2owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.SR_CHEMIST_CHECKING:
                    case StatusEnum.SR_CHEMIST_APPROVE:
                    case StatusEnum.SR_CHEMIST_DISAPPROVE:

                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_APPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_DISAPPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_WORD:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.LABMANAGER_CHECKING:
                    case StatusEnum.LABMANAGER_APPROVE:
                    case StatusEnum.LABMANAGER_DISAPPROVE:
                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_APPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_DISAPPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_PDF:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        break;
                }

            }
            #endregion

            #region "WorkSheet"
            Hpas = new template_seagate_hpa_coverpage().SelectBySampleID(this.SampleID);
            if (Hpas != null && Hpas.Count > 0)
            {
                this.CommandName = CommandNameEnum.Edit;

                ddlSpecification.SelectedValue = Hpas[0].detail_spec_id.ToString();
                tb_m_hpa_specification tem = new tb_m_hpa_specification().SelectByID(Convert.ToInt32(Hpas[0].detail_spec_id));

                if (tem != null)
                {

                    #region "METHOD/PROCEDURE"
                    lbB19.Text = tem.B;
                    lbB20.Text = tem.B;
                    lbA19.Text = "US-LPC(0.3)";
                    //=IF('US-LPC(0.3)'!J3=1,CONCATENATE("LPC (68 KHz)"),IF('US-LPC(0.3)'!J3=2,CONCATENATE("LPC (132 KHz)")))
                    #endregion
                    #region "RESULT"
                    lbDocNo.Text = tem.B;
                    lbCommodity.Text = tem.A;
                    lbUnit1.Text = tem.C;//unit
                    lbUnit2.Text = tem.C;//unit
                    lbUnit3.Text = tem.C;//unit
                    lbUnit4.Text = tem.C;//unit
                    lbUnit5.Text = tem.C;//unit
                    lbUnit6.Text = tem.C;//unit
                    lbUnit7.Text = tem.C;//unit
                    lbUnit8.Text = tem.C;//unit
                    lbUnit9.Text = tem.C;//unit
                    lbUnit10.Text = tem.C;//unit
                    lbUnit11.Text = tem.C;//unit
                    lbUnit12.Text = tem.C;//unit
                    #endregion
                    #region "Liquid Particle Count (68 KHz) 0.3"
                    lbB29.Text = tem.G;
                    #endregion
                    #region "Liquid Particle Count (68 KHz) 0.6"
                    lbB36.Text = tem.H;
                    #endregion
                    #region "Liquid Particle Count (132 KHz) 0.3 "
                    lbB43.Text = tem.I;
                    #endregion
                    #region "Liquid Particle Count (132 KHz) 0.6"
                    lbB50.Text = tem.J;
                    #endregion

                    #region "Hard Particle Analysis(LPC 68 KHz)"
                    lbB54.Text = tem.L;
                    lbB55.Text = tem.T;
                    lbB56.Text = tem.N;
                    lbB57.Text = tem.AI;
                    lbB58.Text = tem.P;
                    lbB59.Text = tem.V;
                    lbB60.Text = tem.X;
                    lbB61.Text = tem.AA;
                    lbB62.Text = tem.R;
                    lbB63.Text = tem.AC;
                    lbB64.Text = tem.AE;
                    lbB65.Text = tem.AG;

                    #endregion
                    #region "Hard Particle Analysis(LPC 132 KHz)"
                    lbB69.Text = tem.M;
                    lbB70.Text = tem.U;
                    lbB71.Text = tem.O;
                    lbB72.Text = tem.AJ;
                    lbB73.Text = tem.Q;
                    lbB74.Text = tem.W;
                    lbB75.Text = tem.Y;
                    lbB76.Text = tem.AB;
                    lbB77.Text = tem.AD;
                    lbB78.Text = tem.S;
                    lbB79.Text = tem.AF;
                    lbB80.Text = tem.AH;
                    #endregion

                    #region "US-LPC(0.3)"
                    template_seagate_hpa_coverpage khz68_03 = Hpas.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_03).ToString());
                    if (khz68_03 != null)
                    {
                        if (!String.IsNullOrEmpty(khz68_03.us_b25))
                        {
                            lbC26.Text = String.Format("{0:n0}", Math.Round(CustomUtils.GetDefaultZero(khz68_03.us_b25)));//='US-LPC(0.3)'!B25
                            lbC27.Text = String.Format("{0:n0}", Math.Round(CustomUtils.GetDefaultZero(khz68_03.us_d25)));//='US-LPC(0.3)'!D25
                            lbC28.Text = String.Format("{0:n0}", Math.Round(CustomUtils.GetDefaultZero(khz68_03.us_f25)));//='US-LPC(0.3)'!F25

                            lbC29.Text = String.Format("{0:n0}", Math.Round((Convert.ToDecimal(lbC26.Text) +
                                           Convert.ToDecimal(lbC27.Text) +
                                           Convert.ToDecimal(lbC28.Text)) / 1).ToString());

                            lbC40.Text = lbC26.Text;
                            lbC41.Text = lbC27.Text;
                            lbC42.Text = lbC28.Text;
                            lbC43.Text = lbC29.Text;

                            LPCTypeEnum lpcType = (LPCTypeEnum)Enum.ToObject(typeof(LPCTypeEnum), Convert.ToInt32(khz68_03.lpc_type));
                            lbA19.Text = String.Format("LPC ({0})", Constants.GetEnumDescription(lpcType));

                            lbC19.Text = khz68_03.cvp_c19;
                            lbE19.Text = khz68_03.cvp_e19;
                            lbC20.Text = khz68_03.cvp_c20;
                            lbE20.Text = khz68_03.cvp_e20;
                            txtCVP_C19.Text = khz68_03.cvp_c19;
                            txtCVP_E19.Text = khz68_03.cvp_e19;
                            txtCVP_C20.Text = khz68_03.cvp_c20;
                            txtCVP_E20.Text = khz68_03.cvp_e20;
                        }

                    }
                    #endregion
                    #region "US-LPC(0.6)"
                    template_seagate_hpa_coverpage khz68_06 = Hpas.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_06).ToString());
                    if (khz68_06 != null)
                    {
                        lbC33.Text = String.Format("{0:n0}", Math.Round(CustomUtils.GetDefaultZero(khz68_06.us_b25)));//='US-LPC(0.6)'!B25
                        lbC34.Text = String.Format("{0:n0}", Math.Round(CustomUtils.GetDefaultZero(khz68_06.us_d25)));//='US-LPC(0.6)'!D25
                        lbC35.Text = String.Format("{0:n0}", Math.Round(CustomUtils.GetDefaultZero(khz68_06.us_f25)));//='US-LPC(0.6)'!F25

                        lbC36.Text = String.Format("{0:n0}", Math.Round(Convert.ToDecimal(((Convert.ToDecimal(lbC33.Text) +
                                       Convert.ToDecimal(lbC34.Text) +
                                       Convert.ToDecimal(lbC35.Text)) / 1).ToString())));

                        lbC47.Text = lbC33.Text;
                        lbC48.Text = lbC34.Text;
                        lbC49.Text = lbC35.Text;
                        lbC50.Text = lbC36.Text;
                    }
                    #endregion

                    #region "Working Sheet"
                    template_seagate_hpa_coverpage ws = this.Hpas[0];
                    //From Working Sheet
                    lbC69.Text = ws.ws_d29;
                    lbC70.Text = ws.ws_d33;
                    lbC71.Text = ws.ws_d36;

                    lbC73.Text = ws.ws_d65;
                    lbC74.Text = ws.ws_d72;
                    lbC75.Text = ws.ws_d47;
                    lbC76.Text = ws.ws_d57;
                    lbC77.Text = ws.ws_d43;

                    lbC79.Text = ws.ws_d75;
                    lbC80.Text = ws.ws_d76;

                    lbC54.Text = ws.ws_d29;
                    lbC55.Text = ws.ws_d33;
                    lbC56.Text = ws.ws_d36;

                    lbC58.Text = ws.ws_d65;
                    lbC59.Text = ws.ws_d72;
                    lbC60.Text = ws.ws_d47;
                    lbC61.Text = ws.ws_d57;

                    lbC63.Text = ws.ws_d43;
                    lbC64.Text = ws.ws_d75;
                    lbC65.Text = ws.ws_d76;
                    #region "Classification"
                    lbC84.Text = ws.ws_d13;
                    lbC85.Text = ws.ws_d14;
                    lbC86.Text = ws.ws_d15;
                    lbC87.Text = ws.ws_d16;
                    lbC88.Text = ws.ws_d17;
                    lbC89.Text = ws.ws_d18;
                    lbC90.Text = ws.ws_d19;
                    lbC91.Text = ws.ws_d20;
                    lbC92.Text = ws.ws_d21;
                    lbC93.Text = ws.ws_d22;
                    lbC94.Text = ws.ws_d23;
                    lbC95.Text = ws.ws_d24;
                    lbC96.Text = ws.ws_d25;
                    lbC97.Text = ws.ws_d26;
                    lbC98.Text = ws.ws_d27;
                    lbC99.Text = ws.ws_d28;
                    lbC100.Text = ws.ws_d30;
                    lbC101.Text = ws.ws_d31;
                    lbC102.Text = ws.ws_d32;
                    lbC103.Text = ws.ws_d34;
                    lbC104.Text = ws.ws_d35;
                    lbC105.Text = ws.ws_d37;
                    lbC106.Text = ws.ws_d38;
                    lbC107.Text = ws.ws_d39;
                    lbC108.Text = ws.ws_d40;
                    lbC109.Text = ws.ws_d41;
                    lbC110.Text = ws.ws_d42;
                    lbC111.Text = ws.ws_d43;
                    lbC112.Text = ws.ws_d44;
                    lbC113.Text = ws.ws_d45;
                    lbC114.Text = ws.ws_d46;
                    lbC115.Text = ws.ws_d47;
                    lbC116.Text = ws.ws_d48;
                    lbC117.Text = ws.ws_d49;
                    lbC118.Text = ws.ws_d50;
                    lbC119.Text = ws.ws_d51;
                    lbC120.Text = ws.ws_d52;
                    lbC121.Text = ws.ws_d53;
                    lbC122.Text = ws.ws_d54;
                    lbC123.Text = ws.ws_d55;
                    lbC124.Text = ws.ws_d56;
                    lbC125.Text = ws.ws_d58;
                    lbC126.Text = ws.ws_d59;
                    lbC127.Text = ws.ws_d60;
                    lbC128.Text = ws.ws_d61;
                    lbC129.Text = ws.ws_d62;
                    lbC130.Text = ws.ws_d63;
                    lbC131.Text = ws.ws_d64;
                    lbC132.Text = ws.ws_d65;
                    lbC133.Text = ws.ws_d66;
                    lbC134.Text = ws.ws_d67;
                    lbC135.Text = ws.ws_d68;
                    lbC136.Text = ws.ws_d69;
                    lbC137.Text = ws.ws_d70;
                    lbC138.Text = ws.ws_d71;
                    lbC139.Text = ws.ws_d72;
                    lbC140.Text = ws.ws_d73;
                    lbC141.Text = ws.ws_d74;
                    #endregion
                    #region  "Analysis Details"
                    
                    lbC144.Text = String.Format("{0:n2}",ws.ws_b8);
                    lbC145.Text = String.Format("{0:n2}",ws.ws_b9);
                    lbC146.Text = String.Format("{0:n2}",ws.ws_b3);
                    lbC147.Text = String.Format("{0:n2}",ws.ws_b6);
                    lbC148.Text = String.Format("{0:n2}",ws.ws_b5);
                    lbC149.Text = "446";                         
                    #endregion
                    #endregion
                }

                ShowItem(Hpas[0].item_visible);
            }
            else
            {
                #region "Initial coverpage value"
                template_seagate_hpa_coverpage lpc = new template_seagate_hpa_coverpage();
                lpc.sample_id = this.SampleID;
                lpc.detail_spec_id = Convert.ToInt16(ddlSpecification.SelectedValue);
                lpc.lpc_type = Convert.ToInt16(LPCTypeEnum.KHz_68).ToString();
                lpc.particle_type = Convert.ToInt16(ParticleTypeEnum.PAR_03).ToString();
                lpc.item_visible = getItemStatus();
                Hpas.Add(lpc);
                lpc = new template_seagate_hpa_coverpage();
                lpc.sample_id = this.SampleID;
                lpc.detail_spec_id = Convert.ToInt16(ddlSpecification.SelectedValue);
                lpc.lpc_type = Convert.ToInt16(LPCTypeEnum.KHz_68).ToString();
                lpc.particle_type = Convert.ToInt16(ParticleTypeEnum.PAR_06).ToString();
                lpc.item_visible = getItemStatus();
                Hpas.Add(lpc);

                this.CommandName = CommandNameEnum.Add;
                #endregion
            }
            #endregion

            //init
            pRemark.Visible = false;
            pDisapprove.Visible = false;
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "JobID");
            Session.Remove(GetType().Name + "job_sample");
            Session.Remove(GetType().Name + "SampleID");
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.CommandName = (prvPage == null) ? this.CommandName : prvPage.CommandName;
            this.JobID = (prvPage == null) ? this.JobID : prvPage.JobID;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void ddlSpecification_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_m_hpa_specification tem = new tb_m_hpa_specification().SelectByID(int.Parse(ddlSpecification.SelectedValue));
            if (tem != null)
            {

                #region "METHOD/PROCEDURE"
                lbB19.Text = tem.B;
                lbB20.Text = tem.B;
                lbA19.Text = "US-LPC(0.3)";
                //=IF('US-LPC(0.3)'!J3=1,CONCATENATE("LPC (68 KHz)"),IF('US-LPC(0.3)'!J3=2,CONCATENATE("LPC (132 KHz)")))
                #endregion
                #region "RESULT"
                lbDocNo.Text = tem.B;
                lbCommodity.Text = tem.A;
                lbUnit1.Text = tem.C;//unit
                lbUnit2.Text = tem.C;//unit
                lbUnit3.Text = tem.C;//unit
                lbUnit4.Text = tem.C;//unit
                lbUnit5.Text = tem.C;//unit
                lbUnit6.Text = tem.C;//unit
                lbUnit7.Text = tem.C;//unit
                lbUnit8.Text = tem.C;//unit
                lbUnit9.Text = tem.C;//unit
                lbUnit10.Text = tem.C;//unit
                lbUnit11.Text = tem.C;//unit
                lbUnit12.Text = tem.C;//unit
                #endregion
                #region "Liquid Particle Count (68 KHz) 0.3"
                lbB29.Text = tem.G;
                #endregion
                #region "Liquid Particle Count (68 KHz) 0.6"
                lbB36.Text = tem.H;
                #endregion
                #region "Liquid Particle Count (132 KHz) 0.3 "
                lbB43.Text = tem.I;
                #endregion
                #region "Liquid Particle Count (132 KHz) 0.6"
                lbB50.Text = tem.J;
                #endregion

                #region "Hard Particle Analysis(LPC 68 KHz)"
                lbB54.Text = tem.L;
                lbB55.Text = tem.T;
                lbB56.Text = tem.N;
                lbB57.Text = tem.AI;
                lbB58.Text = tem.P;
                lbB59.Text = tem.V;
                lbB60.Text = tem.X;
                lbB61.Text = tem.AA;
                lbB62.Text = tem.R;
                lbB63.Text = tem.AC;
                lbB64.Text = tem.AE;
                lbB65.Text = tem.AG;

                #endregion
                #region "Hard Particle Analysis(LPC 132 KHz)"
                lbB69.Text = tem.M;
                lbB70.Text = tem.U;
                lbB71.Text = tem.O;
                lbB72.Text = tem.AJ;
                lbB73.Text = tem.Q;
                lbB74.Text = tem.W;
                lbB75.Text = tem.Y;
                lbB76.Text = tem.AB;
                lbB77.Text = tem.AD;
                lbB78.Text = tem.S;
                lbB79.Text = tem.AF;
                lbB80.Text = tem.AH;
                #endregion

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            Boolean isValid = true;

            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                    this.jobSample.step1owner = userLogin.id;

                    template_seagate_hpa_coverpage objWork = new template_seagate_hpa_coverpage();
                    foreach (template_seagate_hpa_coverpage _tmp in this.Hpas)
                    {
                        _tmp.detail_spec_id = Convert.ToInt16(ddlSpecification.SelectedValue);
                        _tmp.item_visible = getItemStatus();
                    }
                    switch (this.CommandName)
                    {
                        case CommandNameEnum.Add:
                            objWork.InsertList(this.Hpas);
                            break;
                        case CommandNameEnum.Edit:

                            objWork.UpdateList(this.Hpas);
                            break;
                    }

                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                    StatusEnum srChemistApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (srChemistApproveStatus)
                    {
                        case StatusEnum.SR_CHEMIST_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_WORD);
                            break;
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                            break;
                    }
                    this.jobSample.step3owner = userLogin.id;
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                    StatusEnum labApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (labApproveStatus)
                    {
                        case StatusEnum.LABMANAGER_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_PDF);
                            break;
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(ddlAssignTo.SelectedValue);
                            break;
                    }
                    this.jobSample.step5owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".doc") || Path.GetExtension(btnUpload.FileName).Equals(".docx")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_word = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.LABMANAGER_CHECKING);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .doc|.docx";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step4owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".pdf")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_pdf = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.JOB_COMPLETE);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .pdf";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step6owner = userLogin.id;
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    this.jobSample.step2owner = userLogin.id;
                    break;
            }
            //Stamp date
            job_sample_logs tmp = new job_sample_logs
            {
                ID = 0,
                job_sample_id = this.jobSample.ID,
                job_status = Convert.ToInt32(this.jobSample.job_status),
                job_remark = txtRemark.Text,
                get_alerts = "0",
                date = DateTime.Now
            };
            tmp.Insert();

            this.jobSample.Update();
            //Commit
            GeneralManager.Commit();
            //Return to main.
            if (isValid)
            {
                Response.Redirect(this.PreviousPath);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void lbDownload_Click(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                case StatusEnum.SR_CHEMIST_APPROVE:
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    else
                    {
                        downloadWord();
                    }
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                case StatusEnum.LABMANAGER_APPROVE:
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (!String.IsNullOrEmpty(this.jobSample.path_pdf))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_pdf));
                    }
                    else
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
            }


        }

        private void downloadWord()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string strFileName = string.Format("{0}_{1}.doc", lbRefNo.Text.Replace("-", "_"), DateTime.Now.ToString("yyyyMMddhhmmss"));

            HttpContext.Current.Response.ContentType = "application/vnd.ms-word";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

            StringWriter sw = new StringWriter();
            HtmlTextWriter h = new HtmlTextWriter(sw);
            invDiv.RenderControl(h);
            string strHTMLContent = sw.GetStringBuilder().ToString();
            String html = "<html><header><style>body {max-width: 800px;margin:initial;font-family: \'Arial Unicode MS\';font-size: 10px;}table {border-collapse: collapse;}th {background: #666;color: #fff;border: 1px solid #999;padding: 0.5rem;text-align: center;}td { border: 1px solid #999;padding: 0.5rem;text-align: left;}h6 {font-weight:initial;}</style></header><body>" + strHTMLContent + "</body></html>";


            HttpContext.Current.Response.Write(html);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush();
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue.ToString(), true);
            switch (status)
            {
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = false;
                    break;
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = true;
                    break;
                default:
                    pRemark.Visible = false;
                    pDisapprove.Visible = false;
                    break;
            }

        }

        private String getItemStatus()
        {
            String result = String.Empty;
            result = ((CheckBox1.Checked) ? "1" : "0") +//Liquid Particle Count (68 KHz) 0.3
                        ((CheckBox2.Checked) ? "1" : "0") +
                        ((CheckBox3.Checked) ? "1" : "0") +
                        ((CheckBox4.Checked) ? "1" : "0") +
                        ((CheckBox5.Checked) ? "1" : "0") +//Liquid Particle Count (68 KHz) 0.6
                        ((CheckBox6.Checked) ? "1" : "0") +
                        ((CheckBox7.Checked) ? "1" : "0") +
                        ((CheckBox8.Checked) ? "1" : "0") +
                        ((CheckBox9.Checked) ? "1" : "0") +//Liquid Particle Count (132 KHz) 0.3
                        ((CheckBox10.Checked) ? "1" : "0") +
                        ((CheckBox11.Checked) ? "1" : "0") +
                        ((CheckBox12.Checked) ? "1" : "0") +
                        ((CheckBox13.Checked) ? "1" : "0") +//Liquid Particle Count (132 KHz) 0.6
                        ((CheckBox14.Checked) ? "1" : "0") +
                        ((CheckBox15.Checked) ? "1" : "0") +
                        ((CheckBox16.Checked) ? "1" : "0") +
                        ((CheckBox17.Checked) ? "1" : "0") +//Hard Particle Analysis(LPC 68 KHz)
                        ((CheckBox18.Checked) ? "1" : "0") +
                        ((CheckBox19.Checked) ? "1" : "0") +
                        ((CheckBox20.Checked) ? "1" : "0") +
                        ((CheckBox21.Checked) ? "1" : "0") +
                        ((CheckBox22.Checked) ? "1" : "0") +
                        ((CheckBox23.Checked) ? "1" : "0") +
                        ((CheckBox24.Checked) ? "1" : "0") +
                        ((CheckBox25.Checked) ? "1" : "0") +
                        ((CheckBox26.Checked) ? "1" : "0") +
                        ((CheckBox27.Checked) ? "1" : "0") +
                        ((CheckBox28.Checked) ? "1" : "0") +
                        ((CheckBox29.Checked) ? "1" : "0") +//Hard Particle Analysis(LPC 132 KHz)
                        ((CheckBox30.Checked) ? "1" : "0") +
                        ((CheckBox31.Checked) ? "1" : "0") +
                        ((CheckBox32.Checked) ? "1" : "0") +
                        ((CheckBox33.Checked) ? "1" : "0") +
                        ((CheckBox34.Checked) ? "1" : "0") +
                        ((CheckBox35.Checked) ? "1" : "0") +
                        ((CheckBox36.Checked) ? "1" : "0") +
                        ((CheckBox37.Checked) ? "1" : "0") +
                        ((CheckBox38.Checked) ? "1" : "0") +
                        ((CheckBox39.Checked) ? "1" : "0") +
                        ((CheckBox40.Checked) ? "1" : "0");
            return result;
        }

        private void ShowItem(String _itemVisible)
        {
            if (_itemVisible != null)
            {
                char[] item = _itemVisible.ToCharArray();
                if (item.Length == 40)
                {
                    StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
                    switch (status)
                    {
                        case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                            break;
                        case StatusEnum.LOGIN_SELECT_SPEC:
                            CheckBox1.Checked = item[0] == '1' ? true : false;
                            CheckBox2.Checked = item[1] == '1' ? true : false;
                            CheckBox3.Checked = item[2] == '1' ? true : false;
                            CheckBox4.Checked = item[3] == '1' ? true : false;
                            CheckBox5.Checked = item[4] == '1' ? true : false;
                            CheckBox6.Checked = item[5] == '1' ? true : false;
                            CheckBox7.Checked = item[6] == '1' ? true : false;
                            CheckBox8.Checked = item[7] == '1' ? true : false;
                            CheckBox9.Checked = item[8] == '1' ? true : false;
                            CheckBox10.Checked = item[9] == '1' ? true : false;
                            CheckBox11.Checked = item[10] == '1' ? true : false;
                            CheckBox12.Checked = item[11] == '1' ? true : false;
                            CheckBox13.Checked = item[12] == '1' ? true : false;
                            CheckBox14.Checked = item[13] == '1' ? true : false;
                            CheckBox15.Checked = item[14] == '1' ? true : false;
                            CheckBox16.Checked = item[15] == '1' ? true : false;
                            CheckBox17.Checked = item[16] == '1' ? true : false;
                            CheckBox18.Checked = item[17] == '1' ? true : false;
                            CheckBox19.Checked = item[18] == '1' ? true : false;
                            CheckBox20.Checked = item[19] == '1' ? true : false;
                            CheckBox21.Checked = item[20] == '1' ? true : false;
                            CheckBox22.Checked = item[21] == '1' ? true : false;
                            CheckBox23.Checked = item[22] == '1' ? true : false;
                            CheckBox24.Checked = item[23] == '1' ? true : false;
                            CheckBox25.Checked = item[24] == '1' ? true : false;
                            CheckBox26.Checked = item[25] == '1' ? true : false;
                            CheckBox27.Checked = item[26] == '1' ? true : false;
                            CheckBox28.Checked = item[27] == '1' ? true : false;
                            CheckBox29.Checked = item[28] == '1' ? true : false;
                            CheckBox30.Checked = item[29] == '1' ? true : false;
                            CheckBox31.Checked = item[30] == '1' ? true : false;
                            CheckBox32.Checked = item[31] == '1' ? true : false;
                            CheckBox33.Checked = item[32] == '1' ? true : false;
                            CheckBox34.Checked = item[33] == '1' ? true : false;
                            CheckBox35.Checked = item[34] == '1' ? true : false;
                            CheckBox36.Checked = item[35] == '1' ? true : false;
                            CheckBox37.Checked = item[36] == '1' ? true : false;
                            CheckBox38.Checked = item[37] == '1' ? true : false;
                            CheckBox39.Checked = item[38] == '1' ? true : false;
                            CheckBox40.Checked = item[39] == '1' ? true : false;

                            lbC19.Visible = false;
                            lbE19.Visible = false;
                            lbC20.Visible = false;
                            lbE20.Visible = false;
                            txtCVP_C19.Visible = true;
                            txtCVP_E19.Visible = true;
                            txtCVP_C20.Visible = true;
                            txtCVP_E20.Visible = true;
                            break;
                        case StatusEnum.CHEMIST_TESTING:
                        case StatusEnum.SR_CHEMIST_CHECKING:
                        case StatusEnum.SR_CHEMIST_APPROVE:
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_WORD:
                        case StatusEnum.LABMANAGER_CHECKING:
                        case StatusEnum.LABMANAGER_APPROVE:
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_PDF:
                            tb1.Visible = item[0] == '1' ? true : false;
                            tr2.Visible = item[1] == '1' ? true : false;
                            tr3.Visible = item[2] == '1' ? true : false;
                            tr4.Visible = item[3] == '1' ? true : false;
                            tb5.Visible = item[4] == '1' ? true : false;
                            tr6.Visible = item[5] == '1' ? true : false;
                            tr7.Visible = item[6] == '1' ? true : false;
                            tr8.Visible = item[7] == '1' ? true : false;
                            tb9.Visible = item[8] == '1' ? true : false;
                            tr10.Visible = item[9] == '1' ? true : false;
                            tr11.Visible = item[10] == '1' ? true : false;
                            tr12.Visible = item[11] == '1' ? true : false;
                            tb13.Visible = item[12] == '1' ? true : false;
                            tr14.Visible = item[13] == '1' ? true : false;
                            tr15.Visible = item[14] == '1' ? true : false;
                            tr16.Visible = item[15] == '1' ? true : false;
                            tb17.Visible = item[16] == '1' ? true : false;
                            tr18.Visible = item[17] == '1' ? true : false;
                            tr19.Visible = item[18] == '1' ? true : false;
                            tr20.Visible = item[19] == '1' ? true : false;
                            tr21.Visible = item[20] == '1' ? true : false;
                            tr22.Visible = item[21] == '1' ? true : false;
                            tr23.Visible = item[22] == '1' ? true : false;
                            tr24.Visible = item[23] == '1' ? true : false;
                            tr25.Visible = item[24] == '1' ? true : false;
                            tr26.Visible = item[25] == '1' ? true : false;
                            tr27.Visible = item[26] == '1' ? true : false;
                            tr28.Visible = item[27] == '1' ? true : false;
                            tb29.Visible = item[28] == '1' ? true : false;
                            tr30.Visible = item[29] == '1' ? true : false;
                            tr31.Visible = item[30] == '1' ? true : false;
                            tr32.Visible = item[31] == '1' ? true : false;
                            tr33.Visible = item[32] == '1' ? true : false;
                            tr34.Visible = item[33] == '1' ? true : false;
                            tr35.Visible = item[34] == '1' ? true : false;
                            tr36.Visible = item[35] == '1' ? true : false;
                            tr37.Visible = item[36] == '1' ? true : false;
                            tr38.Visible = item[37] == '1' ? true : false;
                            tr39.Visible = item[38] == '1' ? true : false;
                            tr40.Visible = item[39] == '1' ? true : false;


                            lbC19.Visible = true;
                            lbE19.Visible = true;
                            lbC20.Visible = true;
                            lbE20.Visible = true;
                            txtCVP_C19.Visible = false;
                            txtCVP_E19.Visible = false;
                            txtCVP_C20.Visible = false;
                            txtCVP_E20.Visible = false;

                            CheckBox1.Visible = false;
                            CheckBox2.Visible = false;
                            CheckBox3.Visible = false;
                            CheckBox4.Visible = false;
                            CheckBox5.Visible = false;
                            CheckBox6.Visible = false;
                            CheckBox7.Visible = false;
                            CheckBox8.Visible = false;
                            CheckBox9.Visible = false;
                            CheckBox10.Visible = false;
                            CheckBox11.Visible = false;
                            CheckBox12.Visible = false;
                            CheckBox13.Visible = false;
                            CheckBox14.Visible = false;
                            CheckBox15.Visible = false;
                            CheckBox16.Visible = false;
                            CheckBox17.Visible = false;
                            CheckBox18.Visible = false;
                            CheckBox19.Visible = false;
                            CheckBox20.Visible = false;
                            CheckBox21.Visible = false;
                            CheckBox22.Visible = false;
                            CheckBox23.Visible = false;
                            CheckBox24.Visible = false;
                            CheckBox25.Visible = false;
                            CheckBox26.Visible = false;
                            CheckBox27.Visible = false;
                            CheckBox28.Visible = false;
                            CheckBox29.Visible = false;
                            CheckBox30.Visible = false;
                            CheckBox31.Visible = false;
                            CheckBox32.Visible = false;
                            CheckBox33.Visible = false;
                            CheckBox34.Visible = false;
                            CheckBox35.Visible = false;
                            CheckBox36.Visible = false;
                            CheckBox37.Visible = false;
                            CheckBox38.Visible = false;
                            CheckBox39.Visible = false;
                            CheckBox40.Visible = false;

                            break;
                    }
                }
            }

        }

    }
}

