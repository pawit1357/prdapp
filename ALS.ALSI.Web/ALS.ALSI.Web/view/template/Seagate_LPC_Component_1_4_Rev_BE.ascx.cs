﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Web.view.request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ALS.ALSI.Web.view.template
{

    public partial class Seagate_LPC_Component_1_4_Rev_BE : System.Web.UI.UserControl
    {

        #region "Property"

        public user_login userLogin
        {
            get
            {
                return ((Session[Constants.SESSION_USER] != null) ? (user_login)Session[Constants.SESSION_USER] : null);
            }
        }

        public job_sample jobSample
        {
            get { return (job_sample)Session["job_sample"]; }
            set { Session["job_sample"] = value; }
        }

        public CommandNameEnum CommandName
        {
            get { return (CommandNameEnum)ViewState[Constants.COMMAND_NAME]; }
            set { ViewState[Constants.COMMAND_NAME] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int JobID
        {
            get { return (int)Session[GetType().Name + "JobID"]; }
            set { Session[GetType().Name + "JobID"] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        public List<template_seagate_lpc_coverpage> Lpcs
        {
            get { return (List<template_seagate_lpc_coverpage>)Session[GetType().Name + "Lpcs"]; }
            set { Session[GetType().Name + "Lpcs"] = value; }
        }

        private void initialPage()
        {
            ddlAssignTo.Items.Clear();
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LOGIN_SELECT_SPEC), Convert.ToInt16(StatusEnum.LOGIN_SELECT_SPEC) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.CHEMIST_TESTING), Convert.ToInt16(StatusEnum.CHEMIST_TESTING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_CHECKING), Convert.ToInt16(StatusEnum.SR_CHEMIST_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_WORD), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_WORD) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_CHECKING), Convert.ToInt16(StatusEnum.LABMANAGER_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_PDF), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_PDF) + ""));

            ddlSpecification.Items.Clear();
            ddlSpecification.DataSource = new tb_m_detail_spec_lpc().SelectBySpecificationID(Convert.ToInt16(SpecificationEnum.SEAGATE));
            ddlSpecification.DataBind();
            ddlSpecification.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));

            /*INFO*/
            #region "job Info"
            job_info job = new job_info().SelectByID(this.JobID);

            lbPoNo.Text = (job.customer_po_ref == null) ? String.Empty : job.customer_po_ref.ToString();
            lbDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
            lbCompany.Text = String.Format("{0}<br />{1}", job.m_customer.company_name, job.m_customer.address);
            lbDateSampleReceived.Text = Convert.ToDateTime(job.date_of_receive).ToString("MM/dd/yyyy");

            this.jobSample = new job_sample().SelectByID(this.SampleID);

            if (this.jobSample != null)
            {


                lbRefNo.Text = this.jobSample.job_number.ToString();
                lbDownloadName.Text = this.jobSample.job_number.ToString();
                lbDateTestCompleted.Text = Convert.ToDateTime(this.jobSample.due_date).ToString("MM/dd/yyyy");

                lbSampleDescription.Text = String.Format("Description:{0}<br />Model:{1}<br />Surface Area:{2}<br />Remark:{3}<br />", this.jobSample.description, this.jobSample.model, this.jobSample.surface_area, this.jobSample.remarks);

                RoleEnum userRole = (RoleEnum)Enum.Parse(typeof(RoleEnum), userLogin.role_id.ToString(), true);

                StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);

                lbJobStatus.Text = Constants.GetEnumDescription(status);
                ddlStatus.Items.Clear();
                switch (status)
                {
                    case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                        break;
                    case StatusEnum.LOGIN_SELECT_SPEC:
                        pSpecification.Visible = true;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.LOGIN) ? ((this.jobSample.step1owner == null) ? true : ((this.jobSample.step1owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.CHEMIST_TESTING:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.CHEMIST) ? ((this.jobSample.step2owner == null) ? true : ((this.jobSample.step2owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.SR_CHEMIST_CHECKING:
                    case StatusEnum.SR_CHEMIST_APPROVE:
                    case StatusEnum.SR_CHEMIST_DISAPPROVE:

                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_APPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_DISAPPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_WORD:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.LABMANAGER_CHECKING:
                    case StatusEnum.LABMANAGER_APPROVE:
                    case StatusEnum.LABMANAGER_DISAPPROVE:
                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_APPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_DISAPPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_PDF:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        break;
                }

            }
            #endregion

            #region "WorkSheet"
            Lpcs = new template_seagate_lpc_coverpage().SelectBySampleID(this.SampleID);
            if (Lpcs != null && Lpcs.Count > 0)
            {
                this.CommandName = CommandNameEnum.Edit;

                ddlSpecification.SelectedValue = Lpcs[0].detail_spec_id.ToString();
                tb_m_detail_spec_lpc tem = new tb_m_detail_spec_lpc().SelectByID(Convert.ToInt32(Lpcs[0].detail_spec_id));

                if (tem != null)
                {
                    #region "METHOD/PROCEDURE"
                    //=IF('US-LPC(0.3)'!J3=1,CONCATENATE("LPC (68 KHz)"),IF('US-LPC(0.3)'!J3=2,CONCATENATE("LPC (132 KHz)")))
                    lbA18.Text = string.Empty;
                    #endregion
                    #region "HEADER"
                    lbDocNo.Text = tem.C;
                    lbDocRev.Text = tem.D;
                    lbCommodity.Text = tem.B;
                    lbUnit1.Text = tem.E;//unit
                    lbUnit2.Text = tem.E;//unit
                    lbUnit3.Text = tem.E;//unit
                    lbUnit4.Text = tem.E;//unit
                    lbUnit5.Text = tem.E;//unit
                    lbUnit6.Text = tem.E;//unit
                    lbUnit7.Text = tem.E;//unit
                    lbUnit8.Text = tem.E;//unit
                    #endregion
                    #region "Liquid Particle Count (68 KHz) 0.3"
                    lbB28.Text = tem.F;
                    #endregion
                    #region "Liquid Particle Count (68 KHz) 0.6"
                    lbB35.Text = tem.G;
                    #endregion
                    #region "Liquid Particle Count (132 KHz) 0.3 "
                    lbB42.Text = tem.H;
                    #endregion
                    #region "Liquid Particle Count (132 KHz) 0.6"
                    lbB49.Text = tem.I;
                    #endregion

                    #region "US-LPC(0.3)"
                    template_seagate_lpc_coverpage khz68_03 = Lpcs.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_03).ToString());
                    if (khz68_03 != null)
                    {
                        if (!String.IsNullOrEmpty(khz68_03.b25))
                        {
                            lbC25.Text = String.Format("{0:n0}", Math.Round(Convert.ToDecimal(khz68_03.b25)));//='US-LPC(0.3)'!B25
                            lbC26.Text = String.Format("{0:n0}", Math.Round(Convert.ToDecimal(khz68_03.d25)));//='US-LPC(0.3)'!D25
                            lbC27.Text = String.Format("{0:n0}", Math.Round(Convert.ToDecimal(khz68_03.f25)));//='US-LPC(0.3)'!F25
                            lbC28.Text = String.Format("{0:n0}", Math.Round(Convert.ToDecimal(((Convert.ToDecimal(khz68_03.b25) +
                                           Convert.ToDecimal(khz68_03.d25) +
                                           Convert.ToDecimal(khz68_03.f25)) / 3).ToString())));

                            //132 KHz
                            lbC39.Text = lbC25.Text;
                            lbC40.Text = lbC26.Text;
                            lbC41.Text = lbC27.Text;
                            lbC42.Text = lbC28.Text;
                        }

                        LPCTypeEnum lpcType = (LPCTypeEnum)Enum.ToObject(typeof(LPCTypeEnum), Convert.ToInt32(khz68_03.lpc_type));
                        lbA18.Text = String.Format("LPC ({0})", Constants.GetEnumDescription(lpcType));
                        txtCVP_C19.Text = khz68_03.cvp_c19;
                        txtCVP_E19.Text = khz68_03.cvp_e19;
                        lbCVP_C19.Text = txtCVP_C19.Text;
                        lbCVP_E19.Text = txtCVP_E19.Text;
                    }
                    #endregion

                    #region "US-LPC(0.6)"
                    template_seagate_lpc_coverpage khz68_06 = Lpcs.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_06).ToString());
                    if (khz68_06 != null)
                    {
                        if (!String.IsNullOrEmpty(khz68_06.b25))
                        {
                            lbC32.Text = String.Format("{0:n0}", Math.Round(Convert.ToDecimal(khz68_06.b25)));//='US-LPC(0.6)'!B25
                            lbC33.Text = String.Format("{0:n0}", Math.Round(Convert.ToDecimal(khz68_06.d25)));//='US-LPC(0.6)'!D25
                            lbC34.Text = String.Format("{0:n0}", Math.Round(Convert.ToDecimal(khz68_06.f25)));//='US-LPC(0.6)'!F25
                            lbC35.Text = String.Format("{0:n0}", Math.Round(Convert.ToDecimal(((Convert.ToDecimal(khz68_06.b25) +
                                           Convert.ToDecimal(khz68_06.d25) +
                                           Convert.ToDecimal(khz68_06.f25)) / 3).ToString())));

                            //132 KHz
                            lbC46.Text = lbC32.Text;
                            lbC47.Text = lbC33.Text;
                            lbC48.Text = lbC34.Text;
                            lbC49.Text = lbC35.Text;
                        }
                    }
                    #endregion


                    ShowItem(Lpcs[0].item_visible);
                }
            #endregion
            }
            else
            {
                #region "Initial coverpage value"
                template_seagate_lpc_coverpage lpc = new template_seagate_lpc_coverpage();
                lpc.sample_id = this.SampleID;
                lpc.detail_spec_id = Convert.ToInt16(ddlSpecification.SelectedValue);
                lpc.lpc_type = Convert.ToInt16(LPCTypeEnum.KHz_68).ToString();
                lpc.particle_type = Convert.ToInt16(ParticleTypeEnum.PAR_03).ToString();
                lpc.item_visible = getItemStatus();
                Lpcs.Add(lpc);
                lpc = new template_seagate_lpc_coverpage();
                lpc.sample_id = this.SampleID;
                lpc.detail_spec_id = Convert.ToInt16(ddlSpecification.SelectedValue);
                lpc.lpc_type = Convert.ToInt16(LPCTypeEnum.KHz_68).ToString();
                lpc.particle_type = Convert.ToInt16(ParticleTypeEnum.PAR_06).ToString();
                lpc.item_visible = getItemStatus();
                Lpcs.Add(lpc);

                this.CommandName = CommandNameEnum.Add;
                #endregion
            }
            //init
            pRemark.Visible = false;
            pDisapprove.Visible = false;
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "JobID");
            Session.Remove(GetType().Name + "job_sample");
            Session.Remove(GetType().Name + "SampleID");
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.CommandName = (prvPage == null) ? this.CommandName : prvPage.CommandName;
            this.JobID = (prvPage == null) ? this.JobID : prvPage.JobID;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void ddlSpecification_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_m_detail_spec_lpc tem = new tb_m_detail_spec_lpc().SelectByID(int.Parse(ddlSpecification.SelectedValue));
            if (tem != null)
            {

                #region "METHOD/PROCEDURE"
                //=IF('US-LPC(0.3)'!J3=1,CONCATENATE("LPC (68 KHz)"),IF('US-LPC(0.3)'!J3=2,CONCATENATE("LPC (132 KHz)")))
                lbA18.Text = string.Empty;
                #endregion
                #region "HEADER"
                lbDocNo.Text = tem.C;
                lbDocRev.Text = tem.D;
                lbCommodity.Text = tem.B;
                lbUnit1.Text = tem.E;//unit
                lbUnit2.Text = tem.E;//unit
                lbUnit3.Text = tem.E;//unit
                lbUnit4.Text = tem.E;//unit
                lbUnit5.Text = tem.E;//unit
                lbUnit6.Text = tem.E;//unit
                lbUnit7.Text = tem.E;//unit
                lbUnit8.Text = tem.E;//unit
                #endregion
                #region "Liquid Particle Count (68 KHz) 0.3"
                lbB28.Text = tem.F;
                #endregion
                #region "Liquid Particle Count (68 KHz) 0.6"
                lbB35.Text = tem.G;
                #endregion
                #region "Liquid Particle Count (132 KHz) 0.3 "
                lbB42.Text = tem.H;
                #endregion
                #region "Liquid Particle Count (132 KHz) 0.6"
                lbB49.Text = tem.I;
                #endregion

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            Boolean isValid = true;

            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                    this.jobSample.step1owner = userLogin.id;

                    template_seagate_lpc_coverpage objWork = new template_seagate_lpc_coverpage();
                    foreach (template_seagate_lpc_coverpage _tmp in this.Lpcs)
                    {
                        _tmp.detail_spec_id = Convert.ToInt16(ddlSpecification.SelectedValue);
                        _tmp.cvp_c19 = txtCVP_C19.Text;
                        _tmp.cvp_e19 = txtCVP_E19.Text;
                        _tmp.item_visible = getItemStatus();
                    }
                    switch (this.CommandName)
                    {
                        case CommandNameEnum.Add:
                            objWork.InsertList(this.Lpcs);
                            break;
                        case CommandNameEnum.Edit:

                            objWork.UpdateList(this.Lpcs);
                            break;
                    }

                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                    StatusEnum srChemistApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (srChemistApproveStatus)
                    {
                        case StatusEnum.SR_CHEMIST_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_WORD);
                            break;
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                            break;
                    }
                    this.jobSample.step3owner = userLogin.id;
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                    StatusEnum labApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (labApproveStatus)
                    {
                        case StatusEnum.LABMANAGER_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_PDF);
                            break;
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(ddlAssignTo.SelectedValue);
                            break;
                    }
                    this.jobSample.step5owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".doc") || Path.GetExtension(btnUpload.FileName).Equals(".docx")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_word = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.LABMANAGER_CHECKING);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .doc|.docx";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step4owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".pdf")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_pdf = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.JOB_COMPLETE);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .pdf";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step6owner = userLogin.id;
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    this.jobSample.step2owner = userLogin.id;
                    break;
            }
            //Stamp date
            job_sample_logs tmp = new job_sample_logs
            {
                ID = 0,
                job_sample_id = this.jobSample.ID,
                job_status = Convert.ToInt32(this.jobSample.job_status),
                job_remark = txtRemark.Text,
                get_alerts = "0",
                date = DateTime.Now
            };
            tmp.Insert();

            this.jobSample.Update();
            //Commit
            GeneralManager.Commit();
            //Return to main.
            if (isValid)
            {
                Response.Redirect(this.PreviousPath);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void lbDownload_Click(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                case StatusEnum.SR_CHEMIST_APPROVE:
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    else
                    {
                        downloadWord();
                    }
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                case StatusEnum.LABMANAGER_APPROVE:
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (!String.IsNullOrEmpty(this.jobSample.path_pdf))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_pdf));
                    }
                    else
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
            }


        }

        private void downloadWord()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string strFileName = string.Format("{0}_{1}.doc", lbRefNo.Text.Replace("-", "_"), DateTime.Now.ToString("yyyyMMddhhmmss"));

            HttpContext.Current.Response.ContentType = "application/vnd.ms-word";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

            StringWriter sw = new StringWriter();
            HtmlTextWriter h = new HtmlTextWriter(sw);
            invDiv.RenderControl(h);
            string strHTMLContent = sw.GetStringBuilder().ToString();
            String html = "<html><header><style>body {max-width: 800px;margin:initial;font-family: \'Arial Unicode MS\';font-size: 10px;}table {border-collapse: collapse;}th {background: #666;color: #fff;border: 1px solid #999;padding: 0.5rem;text-align: center;}td { border: 1px solid #999;padding: 0.5rem;text-align: left;}h6 {font-weight:initial;}</style></header><body>" + strHTMLContent + "</body></html>";


            HttpContext.Current.Response.Write(html);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush();
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue.ToString(), true);
            switch (status)
            {
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = false;
                    break;
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = true;
                    break;
                default:
                    pRemark.Visible = false;
                    pDisapprove.Visible = false;
                    break;
            }

        }

        private String getItemStatus()
        {
            String result = String.Empty;
            result = ((CheckBox1.Checked) ? "1" : "0") +
                        ((CheckBox2.Checked) ? "1" : "0") +
                        ((CheckBox3.Checked) ? "1" : "0") +
                        ((CheckBox4.Checked) ? "1" : "0");
            return result;
        }

        private void ShowItem(String _itemVisible)
        {
            if (_itemVisible != null)
            {
                char[] item = _itemVisible.ToCharArray();
                if (item.Length == 4)
                {
                    StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
                    switch (status)
                    {
                        case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                            break;
                        case StatusEnum.LOGIN_SELECT_SPEC:
                            CheckBox1.Checked = item[0] == '1' ? true : false;
                            CheckBox2.Checked = item[1] == '1' ? true : false;
                            CheckBox3.Checked = item[2] == '1' ? true : false;
                            CheckBox4.Checked = item[3] == '1' ? true : false;
                            lbCVP_C19.Visible = false;
                            lbCVP_E19.Visible = false;
                            txtCVP_C19.Visible = true;
                            txtCVP_E19.Visible = true;
                            break;
                        case StatusEnum.CHEMIST_TESTING:
                        case StatusEnum.SR_CHEMIST_CHECKING:
                        case StatusEnum.SR_CHEMIST_APPROVE:
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_WORD:
                        case StatusEnum.LABMANAGER_CHECKING:
                        case StatusEnum.LABMANAGER_APPROVE:
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_PDF:
                            tb1.Visible = item[0] == '1' ? true : false;
                            tb2.Visible = item[1] == '1' ? true : false;
                            tb3.Visible = item[2] == '1' ? true : false;
                            tb4.Visible = item[3] == '1' ? true : false;
                            lbCVP_C19.Visible = true;
                            lbCVP_E19.Visible = true;
                            txtCVP_C19.Visible = false;
                            txtCVP_E19.Visible = false;
                            CheckBox1.Visible = false;
                            CheckBox2.Visible = false;
                            CheckBox3.Visible = false;
                            CheckBox4.Visible = false;
                            break;
                    }
                }
            }

        }

    }
}