﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Web.view.request;
using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ALS.ALSI.Web.view.template
{
    public partial class WD_LPC_Component_2_2 : System.Web.UI.UserControl
    {

        #region "Property"

        public user_login userLogin
        {
            get
            {
                return ((Session[Constants.SESSION_USER] != null) ? (user_login)Session[Constants.SESSION_USER] : null);
            }
        }

        public job_sample jobSample
        {
            get { return (job_sample)Session["job_sample"]; }
            set { Session["job_sample"] = value; }
        }

        public CommandNameEnum CommandName
        {
            get { return (CommandNameEnum)ViewState[Constants.COMMAND_NAME]; }
            set { ViewState[Constants.COMMAND_NAME] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int JobID
        {
            get { return (int)Session[GetType().Name + "JobID"]; }
            set { Session[GetType().Name + "JobID"] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        public template_wd_lpc_coverpage Lpc
        {
            get { return (template_wd_lpc_coverpage)Session[GetType().Name + "Lpc"]; }
            set { Session[GetType().Name + "Lpc"] = value; }
        }

        private void initialPage()
        {
            #region "Initial UI Component"
            ddlAssignTo.Items.Clear();
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LOGIN_SELECT_SPEC), Convert.ToInt16(StatusEnum.LOGIN_SELECT_SPEC) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.CHEMIST_TESTING), Convert.ToInt16(StatusEnum.CHEMIST_TESTING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_CHECKING), Convert.ToInt16(StatusEnum.SR_CHEMIST_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_WORD), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_WORD) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_CHECKING), Convert.ToInt16(StatusEnum.LABMANAGER_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_PDF), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_PDF) + ""));


            ddlComponent.Items.Clear();
            ddlComponent.DataSource = new tb_component_lpc().SelectBySpecificationID(Convert.ToInt16(SpecificationEnum.WD));
            ddlComponent.DataBind();
            ddlComponent.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));

            ddlSpecification.Items.Clear();
            ddlSpecification.DataSource = new tb_m_detail_spec_lpc().SelectBySpecificationID(Convert.ToInt16(SpecificationEnum.WD));
            ddlSpecification.DataBind();
            ddlSpecification.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));
            #endregion

            #region "job Info"
            job_info job = new job_info().SelectByID(this.JobID);

            lbPoNo.Text = (job.customer_po_ref == null) ? String.Empty : job.customer_po_ref.ToString();
            lbDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
            lbCompany.Text = String.Format("{0}<br />{1}", job.m_customer.company_name, job.m_customer.address);
            lbDateSampleReceived.Text = Convert.ToDateTime(job.date_of_receive).ToString("MM/dd/yyyy");

            this.jobSample = new job_sample().SelectByID(this.SampleID);

            if (this.jobSample != null)
            {


                lbRefNo.Text = this.jobSample.job_number.ToString();
                lbDownloadName.Text = this.jobSample.job_number.ToString();
                lbDateTestCompleted.Text = Convert.ToDateTime(this.jobSample.due_date).ToString("MM/dd/yyyy");

                lbSampleDescription.Text = String.Format("Description:{0}<br />Model:{1}<br />Surface Area:{2}<br />Remark:{3}<br />", this.jobSample.description, this.jobSample.model, this.jobSample.surface_area, this.jobSample.remarks);

                RoleEnum userRole = (RoleEnum)Enum.Parse(typeof(RoleEnum), userLogin.role_id.ToString(), true);

                StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);

                lbJobStatus.Text = Constants.GetEnumDescription(status);
                ddlStatus.Items.Clear();
                switch (status)
                {
                    case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                        break;
                    case StatusEnum.LOGIN_SELECT_SPEC:
                        pSpecification.Visible = true;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.LOGIN) ? ((this.jobSample.step1owner == null) ? true : ((this.jobSample.step1owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.CHEMIST_TESTING:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.CHEMIST) ? ((this.jobSample.step2owner == null) ? true : ((this.jobSample.step2owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.SR_CHEMIST_CHECKING:
                    case StatusEnum.SR_CHEMIST_APPROVE:
                    case StatusEnum.SR_CHEMIST_DISAPPROVE:

                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_APPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_DISAPPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_WORD:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.LABMANAGER_CHECKING:
                    case StatusEnum.LABMANAGER_APPROVE:
                    case StatusEnum.LABMANAGER_DISAPPROVE:
                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_APPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_DISAPPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_PDF:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        break;
                }

            }
            #endregion

            #region "WorkSheet"
            this.Lpc = new template_wd_lpc_coverpage().SelectBySampleID(this.SampleID);

            if (this.Lpc != null)
            {
                #region "Initial Spec Detail & Component"
                ddlSpecification.SelectedValue = this.Lpc.detail_spec_id.ToString();
                ddlComponent.SelectedValue = this.Lpc.component_id.ToString();

                tb_m_detail_spec_lpc detailSpec = new tb_m_detail_spec_lpc().SelectByID(int.Parse(ddlSpecification.SelectedValue));
                if (detailSpec != null)
                {
                    lbSpecRev.Text = detailSpec.C;
                    lbComponent.Text = detailSpec.B;
                    lbC27.Text = detailSpec.F;
                    lbC28.Text = detailSpec.G;
                    lbC29.Text = detailSpec.H;
                }
                tb_component_lpc comp = new tb_component_lpc().SelectByID(int.Parse(ddlComponent.SelectedValue));
                if (comp != null)
                {
                    lbB21.Text = comp.B;
                    lbC21.Text = comp.D;
                    lbD21.Text = comp.E;
                    lbE21.Text = comp.F;
                }
                #endregion

                lbD27.Text = this.Lpc.ws_f55;
                lbD28.Text = this.Lpc.ws_f56;
                //=IF(C27="NA","NA",IF(D27<INDEX('Detail Spec'!$A$3:$H$229,$F$1,5),"PASS","FAIL"))
                lbE27.Text = (lbC27.Text.Equals(Constants.GetEnumDescription(ResultEnum.NA)) ? Constants.GetEnumDescription(ResultEnum.NA) :
                (Convert.ToDouble(lbD27.Text) < Convert.ToDouble(lbC27.Text.Replace("<", "").Trim())) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL));
                lbE28.Text = (lbC28.Text.Equals(Constants.GetEnumDescription(ResultEnum.NA)) ? Constants.GetEnumDescription(ResultEnum.NA) :
                (Convert.ToDouble(lbD28.Text) < Convert.ToDouble(lbC28.Text.Replace("<", "").Trim())) ? Constants.GetEnumDescription(ResultEnum.PASS) : Constants.GetEnumDescription(ResultEnum.FAIL));

                #region "Test Method: 92-004230 Rev. AK"
                lbB48.Text = this.Lpc.ws_b15;
                lbB49.Text = this.Lpc.ws_b16;
                lbB50.Text = this.Lpc.ws_b17;
                lbB51.Text = this.Lpc.ws_b18;
                #endregion
                #region "Tank Conditions"
                lbB54.Text = this.Lpc.ws_b21;
                lbC54.Text = this.Lpc.ws_c21;
                lbD54.Text = this.Lpc.ws_d21;
                #endregion
                #region "RUN 1"
                lbC57.Text = this.Lpc.ws_c24;
                lbC58.Text = this.Lpc.ws_c25;
                lbC59.Text = this.Lpc.ws_c26;
                lbC60.Text = this.Lpc.ws_c27;
                lbC61.Text = this.Lpc.ws_c28;

                lbD57.Text = this.Lpc.ws_d24;
                lbD58.Text = this.Lpc.ws_d25;
                lbD59.Text = this.Lpc.ws_d26;
                lbD60.Text = this.Lpc.ws_d27;
                lbD61.Text = this.Lpc.ws_d28;

                lbE57.Text = this.Lpc.ws_e24;
                lbE58.Text = this.Lpc.ws_e25;
                lbE59.Text = this.Lpc.ws_e26;
                lbE60.Text = this.Lpc.ws_e27;
                lbE61.Text = this.Lpc.ws_e28;

                lbF57.Text = this.Lpc.ws_f24;
                lbF58.Text = this.Lpc.ws_f25;
                lbF59.Text = this.Lpc.ws_f26;
                lbF60.Text = this.Lpc.ws_f27;
                lbF61.Text = this.Lpc.ws_f28;
                #endregion
                #region "RUN 2"
                lbC63.Text = this.Lpc.ws_c30;
                lbC64.Text = this.Lpc.ws_c31;
                lbC65.Text = this.Lpc.ws_c32;
                lbC66.Text = this.Lpc.ws_c33;
                lbC67.Text = this.Lpc.ws_c34;

                lbD63.Text = this.Lpc.ws_d30;
                lbD64.Text = this.Lpc.ws_d31;
                lbD65.Text = this.Lpc.ws_d32;
                lbD66.Text = this.Lpc.ws_d33;
                lbD67.Text = this.Lpc.ws_d34;

                lbE63.Text = this.Lpc.ws_e30;
                lbE64.Text = this.Lpc.ws_e31;
                lbE65.Text = this.Lpc.ws_e32;
                lbE66.Text = this.Lpc.ws_e33;
                lbE67.Text = this.Lpc.ws_e34;

                lbF63.Text = this.Lpc.ws_f30;
                lbF64.Text = this.Lpc.ws_f31;
                lbF65.Text = this.Lpc.ws_f32;
                lbF66.Text = this.Lpc.ws_f33;
                lbF67.Text = this.Lpc.ws_f34;
                #endregion
                #region "RUN 3"
                lbC69.Text = this.Lpc.ws_c36;
                lbC70.Text = this.Lpc.ws_c37;
                lbC71.Text = this.Lpc.ws_c38;
                lbC72.Text = this.Lpc.ws_c39;
                lbC73.Text = this.Lpc.ws_c40;

                lbD69.Text = this.Lpc.ws_d36;
                lbD70.Text = this.Lpc.ws_d37;
                lbD71.Text = this.Lpc.ws_d38;
                lbD72.Text = this.Lpc.ws_d39;
                lbD73.Text = this.Lpc.ws_d40;

                lbE69.Text = this.Lpc.ws_e36;
                lbE70.Text = this.Lpc.ws_e37;
                lbE71.Text = this.Lpc.ws_e38;
                lbE72.Text = this.Lpc.ws_e39;
                lbE73.Text = this.Lpc.ws_e40;

                lbF69.Text = this.Lpc.ws_f36;
                lbF70.Text = this.Lpc.ws_f37;
                lbF71.Text = this.Lpc.ws_f38;
                lbF72.Text = this.Lpc.ws_f39;
                lbF73.Text = this.Lpc.ws_f40;
                #endregion
                #region "RUN 4"
                lbC81.Text = this.Lpc.ws_c42;
                lbC82.Text = this.Lpc.ws_c43;
                lbC83.Text = this.Lpc.ws_c44;
                lbC84.Text = this.Lpc.ws_c45;
                lbC85.Text = this.Lpc.ws_c46;

                lbD81.Text = this.Lpc.ws_d42;
                lbD82.Text = this.Lpc.ws_d43;
                lbD83.Text = this.Lpc.ws_d44;
                lbD84.Text = this.Lpc.ws_d45;
                lbD85.Text = this.Lpc.ws_d46;

                lbE81.Text = this.Lpc.ws_e42;
                lbE82.Text = this.Lpc.ws_e43;
                lbE83.Text = this.Lpc.ws_e44;
                lbE84.Text = this.Lpc.ws_e45;
                lbE85.Text = this.Lpc.ws_e46;

                lbF81.Text = this.Lpc.ws_f42;
                lbF82.Text = this.Lpc.ws_f43;
                lbF83.Text = this.Lpc.ws_f44;
                lbF84.Text = this.Lpc.ws_f45;
                lbF85.Text = this.Lpc.ws_f46;
                #endregion
                #region "RUN 5"
                lbC87.Text = this.Lpc.ws_c48;
                lbC88.Text = this.Lpc.ws_c49;
                lbC89.Text = this.Lpc.ws_c50;
                lbC90.Text = this.Lpc.ws_c51;
                lbC91.Text = this.Lpc.ws_c52;

                lbD87.Text = this.Lpc.ws_d48;
                lbD88.Text = this.Lpc.ws_d49;
                lbD89.Text = this.Lpc.ws_d50;
                lbD90.Text = this.Lpc.ws_d51;
                lbD91.Text = this.Lpc.ws_d52;

                lbE87.Text = this.Lpc.ws_e48;
                lbE88.Text = this.Lpc.ws_e49;
                lbE89.Text = this.Lpc.ws_e50;
                lbE90.Text = this.Lpc.ws_e51;
                lbE91.Text = this.Lpc.ws_e52;

                lbF87.Text = this.Lpc.ws_f48;
                lbF88.Text = this.Lpc.ws_f49;
                lbF89.Text = this.Lpc.ws_f50;
                lbF90.Text = this.Lpc.ws_f51;
                lbF91.Text = this.Lpc.ws_f52;
                #endregion
                #region "Average"
                lbC94.Text = this.Lpc.ws_e55;
                lbC95.Text = this.Lpc.ws_e56;
                lbC96.Text = this.Lpc.ws_e57;
                lbC97.Text = this.Lpc.ws_e58;
                lbC98.Text = this.Lpc.ws_e59;

                lbD94.Text = this.Lpc.ws_f55;
                lbD95.Text = this.Lpc.ws_f56;
                lbD96.Text = this.Lpc.ws_f57;
                lbD97.Text = this.Lpc.ws_f58;
                lbD98.Text = this.Lpc.ws_f59;
                #endregion
                #region "Standard Deviation"
                lbC100.Text = this.Lpc.ws_e61;
                lbC101.Text = this.Lpc.ws_e62;
                lbC102.Text = this.Lpc.ws_e63;
                lbC103.Text = this.Lpc.ws_e64;
                lbC104.Text = this.Lpc.ws_e65;

                lbD100.Text = this.Lpc.ws_f61;
                lbD101.Text = this.Lpc.ws_f62;
                lbD102.Text = this.Lpc.ws_f63;
                lbD103.Text = this.Lpc.ws_f64;
                lbD104.Text = this.Lpc.ws_f65;
                #endregion
                #region "%RSD Deviation"
                lbC106.Text = String.Format("{0}%",this.Lpc.ws_e67);
                lbC107.Text = String.Format("{0}%",this.Lpc.ws_e68);
                lbC108.Text = String.Format("{0}%",this.Lpc.ws_e69);
                lbC109.Text = String.Format("{0}%",this.Lpc.ws_e70);
                lbC110.Text = String.Format("{0}%",this.Lpc.ws_e71);
                                        
                lbD106.Text = String.Format("{0}%",this.Lpc.ws_f67);
                lbD107.Text = String.Format("{0}%",this.Lpc.ws_f68);
                lbD108.Text = String.Format("{0}%",this.Lpc.ws_f69);
                lbD109.Text = String.Format("{0}%",this.Lpc.ws_f70);
                lbD110.Text = String.Format("{0}%",this.Lpc.ws_f71);
                #endregion

                this.CommandName = CommandNameEnum.Edit;
                ShowItem(this.Lpc.item_visible);
            }
            else
            {
                this.Lpc = new template_wd_lpc_coverpage();
                this.CommandName = CommandNameEnum.Add;
            }
            #endregion

            //init
            pRemark.Visible = false;
            pDisapprove.Visible = false;
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "JobID");
            Session.Remove(GetType().Name + "job_sample");
            Session.Remove(GetType().Name + "SampleID");
            Session.Remove(GetType().Name + "Lpc");
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.CommandName = (prvPage == null) ? this.CommandName : prvPage.CommandName;
            this.JobID = (prvPage == null) ? this.JobID : prvPage.JobID;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void ddlSpecification_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_m_detail_spec_lpc tem = new tb_m_detail_spec_lpc().SelectByID(int.Parse(ddlSpecification.SelectedValue));
            if (tem != null)
            {
                lbSpecRev.Text = tem.C;
                lbComponent.Text = tem.B;
                lbC27.Text = tem.F;
                lbC28.Text = tem.G;
                lbC29.Text = tem.H;
            }
        }
        protected void ddlComponent_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_component_lpc tem = new tb_component_lpc().SelectByID(int.Parse(ddlComponent.SelectedValue));
            if (tem != null)
            {
                lbB21.Text = tem.B;
                lbC21.Text = tem.D;
                lbD21.Text = tem.E;
                lbE21.Text = tem.F;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            Boolean isValid = true;

            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                    this.jobSample.step1owner = userLogin.id;

                    this.Lpc.sample_id = this.SampleID;
                    this.Lpc.detail_spec_id = Convert.ToInt32(ddlSpecification.SelectedValue);
                    this.Lpc.component_id = Convert.ToInt32(ddlComponent.SelectedValue);
                    this.Lpc.item_visible = getItemStatus();
                    switch (this.CommandName)
                    {
                        case CommandNameEnum.Add:
                            this.Lpc.Insert();
                            break;
                        case CommandNameEnum.Edit:
                            this.Lpc.Update();
                            break;
                    }

                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                    StatusEnum srChemistApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (srChemistApproveStatus)
                    {
                        case StatusEnum.SR_CHEMIST_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_WORD);
                            break;
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                            break;
                    }
                    this.jobSample.step3owner = userLogin.id;
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                    StatusEnum labApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (labApproveStatus)
                    {
                        case StatusEnum.LABMANAGER_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_PDF);
                            break;
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(ddlAssignTo.SelectedValue);
                            break;
                    }
                    this.jobSample.step5owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".doc") || Path.GetExtension(btnUpload.FileName).Equals(".docx")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_word = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.LABMANAGER_CHECKING);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .doc|.docx";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step4owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".pdf")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_pdf = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.JOB_COMPLETE);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .pdf";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step6owner = userLogin.id;
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    this.jobSample.step2owner = userLogin.id;
                    break;
            }
            //Stamp date
            job_sample_logs tmp = new job_sample_logs
            {
                ID = 0,
                job_sample_id = this.jobSample.ID,
                job_status = Convert.ToInt32(this.jobSample.job_status),
                job_remark = txtRemark.Text,
                get_alerts = "0",
                date = DateTime.Now
            };
            tmp.Insert();

            this.jobSample.Update();
            //Commit
            GeneralManager.Commit();
            //Return to main.
            if (isValid)
            {
                Response.Redirect(this.PreviousPath);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void lbDownload_Click(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                case StatusEnum.SR_CHEMIST_APPROVE:
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    else
                    {
                        downloadWord();
                    }
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                case StatusEnum.LABMANAGER_APPROVE:
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (!String.IsNullOrEmpty(this.jobSample.path_pdf))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_pdf));
                    }
                    else
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
            }


        }

        private void downloadWord()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string strFileName = string.Format("{0}_{1}.doc", lbRefNo.Text.Replace("-", "_"), DateTime.Now.ToString("yyyyMMddhhmmss"));

            HttpContext.Current.Response.ContentType = "application/vnd.ms-word";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

            StringWriter sw = new StringWriter();
            HtmlTextWriter h = new HtmlTextWriter(sw);
            invDiv.RenderControl(h);
            string strHTMLContent = sw.GetStringBuilder().ToString();
            String html = "<html><header><style>body {max-width: 800px;margin:initial;font-family: \'Arial Unicode MS\';font-size: 10px;}table {border-collapse: collapse;}th {background: #666;color: #fff;border: 1px solid #999;padding: 0.5rem;text-align: center;}td { border: 1px solid #999;padding: 0.5rem;text-align: left;}h6 {font-weight:initial;}</style></header><body>" + strHTMLContent + "</body></html>";


            HttpContext.Current.Response.Write(html);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush();
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue.ToString(), true);
            switch (status)
            {
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = false;
                    break;
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = true;
                    break;
                default:
                    pRemark.Visible = false;
                    pDisapprove.Visible = false;
                    break;
            }

        }

        private String getItemStatus()
        {
            String result = String.Empty;
            result = ((CheckBox1.Checked) ? "1" : "0") +
                        ((CheckBox2.Checked) ? "1" : "0") +
                        ((CheckBox3.Checked) ? "1" : "0") +
                        ((CheckBox4.Checked) ? "1" : "0");
            return result;
        }

        private void ShowItem(String _itemVisible)
        {
            if (_itemVisible != null)
            {
                char[] item = _itemVisible.ToCharArray();
                if (item.Length == 4)
                {
                    StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
                    switch (status)
                    {
                        case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                            break;
                        case StatusEnum.LOGIN_SELECT_SPEC:
                            CheckBox1.Checked = item[0] == '1' ? true : false;
                            CheckBox2.Checked = item[1] == '1' ? true : false;
                            CheckBox3.Checked = item[2] == '1' ? true : false;
                            CheckBox4.Checked = item[3] == '1' ? true : false;
                            break;
                        case StatusEnum.CHEMIST_TESTING:
                        case StatusEnum.SR_CHEMIST_CHECKING:
                        case StatusEnum.SR_CHEMIST_APPROVE:
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_WORD:
                        case StatusEnum.LABMANAGER_CHECKING:
                        case StatusEnum.LABMANAGER_APPROVE:
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_PDF:
                            tb1.Visible = item[0] == '1' ? true : false;
                            tr1.Visible = item[1] == '1' ? true : false;
                            tr2.Visible = item[2] == '1' ? true : false;
                            tr3.Visible = item[3] == '1' ? true : false;
                            CheckBox1.Visible = false;
                            CheckBox2.Visible = false;
                            CheckBox3.Visible = false;
                            CheckBox4.Visible = false;
                            break;
                    }
                }
            }

        }



    }
}

