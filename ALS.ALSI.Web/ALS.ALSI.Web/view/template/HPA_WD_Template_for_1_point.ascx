﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HPA_WD_Template_for_1_point.ascx.cs" Inherits="ALS.ALSI.Web.view.template.HPA_WD_Template_for_1_point" %>

<script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date-picker').datepicker();
    });
</script>
<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <!-- BEGIN COVERPAGE-TH CONTENT-->
            <div class="row-fluid invoice" id="invDiv" runat="server">
                <div>
                    CUSTOMER PO NO.:
                        <asp:Label ID="lbPoNo" runat="server" Text=""></asp:Label><br />
                    ALS THAILAND REF NO.:
                        <asp:Label ID="lbRefNo" runat="server" Text=""></asp:Label><br />
                    DATE:
                        <asp:Label ID="lbDate" runat="server" Text=""></asp:Label><br />
                    COMPANY:
                        <asp:Label ID="lbCompany" runat="server" Text=""></asp:Label><br />
                    <br />
                    DATE SAMPLE RECEIVED:
                        <asp:Label ID="lbDateSampleReceived" runat="server" Text=""></asp:Label><br />
                    DATE TEST COMPLETED:
                        <asp:Label ID="lbDateTestCompleted" runat="server" Text=""></asp:Label><br />
                    <br />
                    SAMPLE DESCRIPTION: One lot of sample was received with references:<br />
                    <asp:Label ID="lbSampleDescription" runat="server" Text=""></asp:Label>
                </div>

                <div class="row-fluid">
                    <div class="span12 ">
                        <h5>METHOD:</h5>
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Particle Analysis by SEM EDX</th>
                                    <th>Taped Area For Drive Parts</th>
                                    <th>No. of Times Taped</th>
                                    <th>Surface Area Analysed (cm2)</th>
                                    <th>Particle Ranges</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbA23" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB23" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC23" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtCVP_D23" runat="server" Text="0.0209"></asp:TextBox>
                                        <asp:Label ID="lbD23" runat="server" Text="0.0209"></asp:Label>
                                    </td>
                                    <td>0.5um - 5.0um</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span6 ">
                        <h6>Results:</h6>
                        <h6>The Specification is based on WD's specification Doc No 
                            <asp:Label ID="lbDocNo" runat="server" Text=""></asp:Label>
                            for
                            <asp:Label ID="lbComponent" runat="server" Text=""></asp:Label>
                        </h6>
                    </div>
                </div>
                <%-- RESULT--%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb1" runat="server">
                            <thead>

                                <tr>
                                    <th>Test</th>
                                    <th>Particle Classification</th>
                                    <th>Result<br />
                                        (Particles/cm2)</th>
                                    <th>Specification Limit<br />
                                        (Particles/cm2)</th>
                                    <th>PASS / FAIL</th>
                                    <th runat="server" id="th1">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr id="tr1">
                                    <td>Particle Analysis by SEM EDX</td>
                                    <td>Total Hard Particles</td>
                                    <td>
                                        <asp:Label ID="lbC28" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD28" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE28" runat="server" Text=""></asp:Label></td>
                                    <td runat="server">
                                        <asp:CheckBox ID="CheckBox2" runat="server" Checked="True" /></td>
                                </tr>
                                <tr id="tr2">
                                    <td></td>
                                    <td>Total MgSiO Particles</td>
                                    <td>
                                        <asp:Label ID="lbC29" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD29" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE29" runat="server" Text=""></asp:Label></td>
                                    <td id="Td1" runat="server">
                                        <asp:CheckBox ID="CheckBox3" runat="server" Checked="True" /></td>
                                </tr>
                                <tr id="tr3">
                                    <td></td>
                                    <td>Total Steel Particles</td>
                                    <td>
                                        <asp:Label ID="lbC30" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD30" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE30" runat="server" Text=""></asp:Label></td>
                                    <td id="Td2" runat="server">
                                        <asp:CheckBox ID="CheckBox4" runat="server" Checked="True" /></td>
                                </tr>
                                <tr id="tr4">
                                    <td></td>
                                    <td>Total Magnetic Particles</td>
                                    <td>
                                        <asp:Label ID="lbC31" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD31" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE31" runat="server" Text=""></asp:Label></td>
                                    <td id="Td3" runat="server">
                                        <asp:CheckBox ID="CheckBox5" runat="server" Checked="True" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <h6>Total hard particles/cm2 = total no. of hard particles/(no. of times taped x surface area analysed)</h6>
                <br />

                <h5>Stage Mimic of
                    <asp:Label ID="lbA34" runat="server" Text=""></asp:Label><br />
                    <asp:Label ID="lbImgPath1" runat="server" Text=""></asp:Label></h5>

                <asp:Image ID="img1" runat="server" Width="120" Height="120" />
                <%--<asp:FileUpload ID="FileUpload1" runat="server" />--%>
                <h6>Please refer to attachment for details of results</h6>
                <h6>Note: This report was performed test by ALS Singapore.</h6>
                <br />

                <h5>Analysis Table on
                    <asp:Label ID="lbA48" runat="server" Text=""></asp:Label></h5>
                <%-- Analysis Table --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="Table3" runat="server">
                            <thead>
                                <tr>
                                    <th>Elemental Composition</th>
                                    <th>Count</th>
                                    <th>Particle/cm2</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr runat="server" id="tr51">
                                    <td>
                                        <asp:Label ID="lbA51" runat="server" Text="Hard Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB51" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC51" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr52">
                                    <td>
                                        <asp:Label ID="lbA52" runat="server" Text="Al-Mg-O (0.5<=ECD<=2.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB52" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC52" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr53">
                                    <td>
                                        <asp:Label ID="lbA53" runat="server" Text="Al-Ti-O (0.5<=ECD<=2.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB53" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC53" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr54">
                                    <td>
                                        <asp:Label ID="lbA54" runat="server" Text="Al-Si-O (0.5<=ECD<=2.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB54" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC54" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr55">
                                    <td>
                                        <asp:Label ID="lbA55" runat="server" Text="Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB55" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC55" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr56">
                                    <td>
                                        <asp:Label ID="lbA56" runat="server" Text="Al-Si-Cu-O (0.5<=ECD<=2.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB56" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC56" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr57">
                                    <td>
                                        <asp:Label ID="lbA57" runat="server" Text="Al-Si-Fe-O  (0.5<=ECD<=2.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB57" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC57" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr58">
                                    <td>
                                        <asp:Label ID="lbA58" runat="server" Text="Ti-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB58" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC58" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr59">
                                    <td>
                                        <asp:Label ID="lbA59" runat="server" Text="Ti-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB59" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC59" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr60">
                                    <td>
                                        <asp:Label ID="lbA60" runat="server" Text="W-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB60" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC60" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr61">
                                    <td>
                                        <asp:Label ID="lbA61" runat="server" Text="Al-O (0.5<=ECD<=2.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB61" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC61" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr62">
                                    <td>
                                        <asp:Label ID="lbA62" runat="server" Text="Al-Si-Mg-O  (0.5<=ECD<=2.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB62" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC62" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr63">
                                    <td>
                                        <asp:Label ID="lbA63" runat="server" Text="Al-Cu-O (0.5<=ECD<=2.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB63" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC63" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr64">
                                    <td>
                                        <asp:Label ID="lbA64" runat="server" Text="Zr-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB64" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC64" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr65">
                                    <td>
                                        <asp:Label ID="lbA65" runat="server" Text="Zr-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB65" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC65" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr66">
                                    <td>
                                        <asp:Label ID="lbA66" runat="server" Text="Ti-B"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB66" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC66" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr67">
                                    <td>
                                        <asp:Label ID="lbA67" runat="server" Text="Ti-N"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB67" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC67" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr68">
                                    <td>
                                        <asp:Label ID="lbA68" runat="server" Text="W-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB68" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC68" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr69">
                                    <td>
                                        <asp:Label ID="lbA69" runat="server" Text="Al-Cu-O (2.0<ECD<=5.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB69" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC69" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr70">
                                    <td>
                                        <asp:Label ID="lbA70" runat="server" Text="Al-Mg-O (2.0<ECD<=5.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB70" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC70" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr71">
                                    <td>
                                        <asp:Label ID="lbA71" runat="server" Text="Al-O (2.0<ECD<=5.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB71" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC71" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr72">
                                    <td>
                                        <asp:Label ID="lbA72" runat="server" Text="Al-Si-Cu-O (2.0<ECD<=5.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB72" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC72" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr73">
                                    <td>
                                        <asp:Label ID="lbA73" runat="server" Text="Al-Si-Fe-O (2.0<ECD<=5.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB73" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC73" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr74">
                                    <td>
                                        <asp:Label ID="lbA74" runat="server" Text="Al-Si-Mg-O (2.0<ECD<=5.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB74" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC74" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr75">
                                    <td>
                                        <asp:Label ID="lbA75" runat="server" Text="Al-Si-O (2.0<ECD<=5.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB75" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC75" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr76">
                                    <td>
                                        <asp:Label ID="lbA76" runat="server" Text="Al-Ti-O (2.0<ECD<=5.0 um)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB76" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC76" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr77">
                                    <td>
                                        <asp:Label ID="lbA77" runat="server" Text="Pb-Zr-Ti (PZT)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB77" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC77" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr78">
                                    <td>
                                        <asp:Label ID="lbA78" runat="server" Text="Si-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB78" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC78" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr79">
                                    <td>
                                        <asp:Label ID="lbA79" runat="server" Text="Subtotal - Hard Particles Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB79" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC79" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr80">
                                    <td>
                                        <asp:Label ID="lbA80" runat="server" Text="Magnetic Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB80" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC80" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr81">
                                    <td>
                                        <asp:Label ID="lbA81" runat="server" Text="Fe-Sm"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB81" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC81" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr82">
                                    <td>
                                        <asp:Label ID="lbA82" runat="server" Text="Fe-Nd"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB82" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC82" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr83">
                                    <td>
                                        <asp:Label ID="lbA83" runat="server" Text="Fe-Sr"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB83" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC83" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr84">
                                    <td>
                                        <asp:Label ID="lbA84" runat="server" Text="Ni-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB84" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC84" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr85">
                                    <td>
                                        <asp:Label ID="lbA85" runat="server" Text="Sm-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB85" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC85" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr86">
                                    <td>
                                        <asp:Label ID="lbA86" runat="server" Text="Ce-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB86" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC86" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr87">
                                    <td>
                                        <asp:Label ID="lbA87" runat="server" Text="Nd-Pr"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB87" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC87" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr88">
                                    <td>
                                        <asp:Label ID="lbA88" runat="server" Text="Subtotal - Magnetic Particles Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB88" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC88" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr89">
                                    <td>
                                        <asp:Label ID="lbA89" runat="server" Text="Subtotal- Hard Particles including Magnetic Partilces"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB89" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC89" runat="server" Text=""></asp:Label></td>
                                </tr>
                               
                                <tr runat="server" id="tr104">
                                    <td>
                                        <asp:Label ID="lbA104" runat="server" Text="Steel Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB104" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC104" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr105">
                                    <td>
                                        <asp:Label ID="lbA105" runat="server" Text="Fe-Cr"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB105" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC105" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr106">
                                    <td>
                                        <asp:Label ID="lbA106" runat="server" Text="Fe-Cr-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB106" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC106" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr107">
                                    <td>
                                        <asp:Label ID="lbA107" runat="server" Text="Fe-Cr-Ni-Si"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB107" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC107" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr108">
                                    <td>
                                        <asp:Label ID="lbA108" runat="server" Text="Fe-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB108" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC108" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr109">
                                    <td>
                                        <asp:Label ID="lbA109" runat="server" Text="Fe-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB109" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC109" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr110">
                                    <td>
                                        <asp:Label ID="lbA110" runat="server" Text="Fe-Cr-Mn-S"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB110" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC110" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr111">
                                    <td>
                                        <asp:Label ID="lbA111" runat="server" Text="Fe-Cr-Ni-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB111" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC111" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr112">
                                    <td>
                                        <asp:Label ID="lbA112" runat="server" Text="Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB112" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC112" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr113">
                                    <td>
                                        <asp:Label ID="lbA113" runat="server" Text="Fe-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB113" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC113" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr114">
                                    <td>
                                        <asp:Label ID="lbA114" runat="server" Text="Subtotal - Steel Particle Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB114" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC114" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr115">
                                    <td>
                                        <asp:Label ID="lbA115" runat="server" Text="Other Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB115" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC115" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr116">
                                    <td>
                                        <asp:Label ID="lbA116" runat="server" Text="Cu-S-Al-O Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB116" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC116" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr117">
                                    <td>
                                        <asp:Label ID="lbA117" runat="server" Text="No Element"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB117" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC117" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr118">
                                    <td>
                                        <asp:Label ID="lbA118" runat="server" Text="SCrMn/Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB118" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC118" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr119">
                                    <td>
                                        <asp:Label ID="lbA119" runat="server" Text="Ti-O/Al-Si-Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB119" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC119" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr120">
                                    <td>
                                        <asp:Label ID="lbA120" runat="server" Text="Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB120" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC120" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr121">
                                    <td>
                                        <asp:Label ID="lbA121" runat="server" Text="Ni-P"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB121" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC121" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr122">
                                    <td>
                                        <asp:Label ID="lbA122" runat="server" Text="Sn base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB122" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC122" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr123">
                                    <td>
                                        <asp:Label ID="lbA123" runat="server" Text="Other"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB123" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC123" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr124">
                                    <td>
                                        <asp:Label ID="lbA124" runat="server" Text="Al"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB124" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC124" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr125">
                                    <td>
                                        <asp:Label ID="lbA125" runat="server" Text="Al-Mg"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB125" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC125" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr126">
                                    <td>
                                        <asp:Label ID="lbA126" runat="server" Text="Al-Ti-Si"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB126" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC126" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr127">
                                    <td>
                                        <asp:Label ID="lbA127" runat="server" Text="Al-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB127" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC127" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr128">
                                    <td>
                                        <asp:Label ID="lbA128" runat="server" Text="Al-Si-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB128" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC128" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr129">
                                    <td>
                                        <asp:Label ID="lbA129" runat="server" Text="Al-Si/Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB129" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC129" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr130">
                                    <td>
                                        <asp:Label ID="lbA130" runat="server" Text="Al-Si-Mg"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB130" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC130" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr131">
                                    <td>
                                        <asp:Label ID="lbA131" runat="server" Text="Mg-Si-O-Al"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB131" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC131" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr132">
                                    <td>
                                        <asp:Label ID="lbA132" runat="server" Text="Mg-Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB132" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC132" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr133">
                                    <td>
                                        <asp:Label ID="lbA133" runat="server" Text="Ti Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB133" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC133" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr134">
                                    <td>
                                        <asp:Label ID="lbA134" runat="server" Text="Cr-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB134" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC134" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr135">
                                    <td>
                                        <asp:Label ID="lbA135" runat="server" Text="Na-Cl"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB135" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC135" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr136">
                                    <td>
                                        <asp:Label ID="lbA136" runat="server" Text="Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB136" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC136" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr137">
                                    <td>
                                        <asp:Label ID="lbA137" runat="server" Text="Cu-Au"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB137" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC137" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr138">
                                    <td>
                                        <asp:Label ID="lbA138" runat="server" Text="Ag-S"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB138" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC138" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr139">
                                    <td>
                                        <asp:Label ID="lbA139" runat="server" Text="Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB139" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC139" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr140">
                                    <td>
                                        <asp:Label ID="lbA140" runat="server" Text="Cu-Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB140" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC140" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr141">
                                    <td>
                                        <asp:Label ID="lbA141" runat="server" Text="Cu-Zn Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB141" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC141" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr142">
                                    <td>
                                        <asp:Label ID="lbA142" runat="server" Text="Cu-Zn-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB142" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC142" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr143">
                                    <td>
                                        <asp:Label ID="lbA143" runat="server" Text="Cu-Zn-Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB143" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC143" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr144">
                                    <td>
                                        <asp:Label ID="lbA144" runat="server" Text="Zn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB144" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC144" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr145">
                                    <td>
                                        <asp:Label ID="lbA145" runat="server" Text="Pb"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB145" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC145" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr146">
                                    <td>
                                        <asp:Label ID="lbA146" runat="server" Text="Fe-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB146" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC146" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr147">
                                    <td>
                                        <asp:Label ID="lbA147" runat="server" Text="Cr-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB147" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC147" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr148">
                                    <td>
                                        <asp:Label ID="lbA148" runat="server" Text="F-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB148" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC148" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr149">
                                    <td>
                                        <asp:Label ID="lbA149" runat="server" Text="Al-Si Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB149" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC149" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr150">
                                    <td>
                                        <asp:Label ID="lbA150" runat="server" Text="NiP/Al"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB150" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC150" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr151">
                                    <td>
                                        <asp:Label ID="lbA151" runat="server" Text="NiP/Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB151" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC151" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr152">
                                    <td>
                                        <asp:Label ID="lbA152" runat="server" Text="NiP Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB152" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC152" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr153">
                                    <td>
                                        <asp:Label ID="lbA153" runat="server" Text="Ba-S Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB153" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC153" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr154">
                                    <td>
                                        <asp:Label ID="lbA154" runat="server" Text="Al-S/Si"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB154" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC154" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr155">
                                    <td>
                                        <asp:Label ID="lbA155" runat="server" Text="AlSi/K"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB155" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC155" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr156">
                                    <td>
                                        <asp:Label ID="lbA156" runat="server" Text="Ca"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB156" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC156" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr157">
                                    <td>
                                        <asp:Label ID="lbA157" runat="server" Text="AlSi/Fe-Cr-Mn-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB157" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC157" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr158">
                                    <td>
                                        <asp:Label ID="lbA158" runat="server" Text="Cu-Zn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB158" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC158" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr159">
                                    <td>
                                        <asp:Label ID="lbA159" runat="server" Text="Cu-S"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB159" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC159" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr160">
                                    <td>
                                        <asp:Label ID="lbA160" runat="server" Text="Fe-Cr/S"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB160" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC160" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr161">
                                    <td>
                                        <asp:Label ID="lbA161" runat="server" Text="Sb Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB161" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC161" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr162">
                                    <td>
                                        <asp:Label ID="lbA162" runat="server" Text="Subtotal - Orther Particle Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB162" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC162" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr163">
                                    <td>
                                        <asp:Label ID="lbA163" runat="server" Text="Grand Total of Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB163" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC163" runat="server" Text=""></asp:Label></td>
                                </tr>

                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>

            <div class="well">
                <h3><i class="icon-tasks"></i>Operations (<asp:Label ID="lbJobStatus" runat="server" Text=""></asp:Label>)</h3>
                <asp:Panel ID="pSpecification" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <label class="control-label" for="ddlSpecification">Specification:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSpecification" runat="server" class="span12 chosen" DataTextField="A" DataValueField="ID" OnSelectedIndexChanged="ddlSpecification_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>

                    </div>
                                        <div class="row-fluid">
                        <div class="span6 ">
                            <label class="control-label" for="ddlComponent">Component:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlComponent" runat="server" class="span12 chosen" DataTextField="A" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="ddlComponent_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pStatus" runat="server">
                    <div class="row-fluid">

                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="ddlStatus">
                                    <asp:Label ID="lbStatus" runat="server" Text="Approve Status:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="span6" DataTextField="name" DataValueField="ID" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pRemark" runat="server">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="txtRemark">
                                    <asp:Label ID="Label1" runat="server" Text="Remark:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtRemark" name="txtRemark" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pDisapprove" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <asp:Label ID="lbAssignTo" runat="server" Text="Assign To:" class="control-label"></asp:Label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAssignTo" runat="server" class="span12 chosen" DataTextField="name" DataValueField="ID" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>


                </asp:Panel>
                <asp:Panel ID="pDownload" runat="server">
                    <ul>
                        <li>Link for download Cover Page
                            <asp:LinkButton ID="lbDownload" runat="server" OnClick="lbDownload_Click">
                                <i class="icon-download-alt"></i>
                                <asp:Label ID="lbDownloadName" runat="server" Text="Label"></asp:Label>
                            </asp:LinkButton>
                        </li>
                    </ul>
                </asp:Panel>
                <asp:Panel ID="pUploadfile" runat="server">
                    <div class="row-fluid fileupload-buttonbar">
                        <div class="span6">
                            <label class="control-label" for="ddlSpecification">Uplod file:</label>
                            <span class="btn green fileinput-button">
                                <i class="icon-plus icon-white"></i>
                                <span>Add files...</span>
                                <asp:FileUpload ID="btnUpload" runat="server" />
                            </span>
                        </div>
                    </div>
                    <ul>
                        <li>The maximum file size for uploads in this demo is <strong>5 MB</strong> (default file size is unlimited).</li>
                        <li>Only word or pdf files (<strong>DOC, DOCX, PDF</strong>) are allowed in this demo (by default there is no file type restriction).</li>
                    </ul>
                    <br />
                    <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                    <br />
                </asp:Panel>

            </div>

            <div class="form-actions clearfix">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn" />
            </div>
            <!-- END PAGE CONTENT-->

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <asp:PostBackTrigger ControlID="lbDownload" />
        </Triggers>
    </asp:UpdatePanel>
</form>
