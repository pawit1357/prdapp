﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Seagate_HPA_Filtration_Rev_BE_1_4.ascx.cs" Inherits="ALS.ALSI.Web.view.template.Seagate_HPA_Filtration_Rev_BE_1_4" %>

<script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date-picker').datepicker();
    });
</script>
<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <!-- BEGIN COVERPAGE-TH CONTENT-->
            <div class="row-fluid invoice" id="invDiv" runat="server">
                <div>
                    CUSTOMER PO NO.:
                        <asp:Label ID="lbPoNo" runat="server" Text=""></asp:Label><br />
                    ALS THAILAND REF NO.:
                        <asp:Label ID="lbRefNo" runat="server" Text=""></asp:Label><br />
                    DATE:
                        <asp:Label ID="lbDate" runat="server" Text=""></asp:Label><br />
                    COMPANY:
                        <asp:Label ID="lbCompany" runat="server" Text=""></asp:Label><br />
                    <br />
                    DATE SAMPLE RECEIVED:
                        <asp:Label ID="lbDateSampleReceived" runat="server" Text=""></asp:Label><br />
                    DATE TEST COMPLETED:
                        <asp:Label ID="lbDateTestCompleted" runat="server" Text=""></asp:Label><br />
                    <br />
                    SAMPLE DESCRIPTION: One lot of sample was received with references:<br />
                    <asp:Label ID="lbSampleDescription" runat="server" Text=""></asp:Label>
                </div>

                <div class="row-fluid">
                    <div class="span12 ">
                        <h5>METHOD/PROCEDURE:</h5>
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Analysis</th>
                                    <th>Procedure No</th>
                                    <th>Number of pieces used<br />
                                        for extraction</th>
                                    <th>Extraction<br />
                                        Medium</th>
                                    <th>Extraction<br />
                                        Volume</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbA19" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB19" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtCVP_C19" runat="server" Text="1"></asp:TextBox>
                                        <asp:Label ID="lbC19" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>0.1 um Filtered
                                        <br />
                                        Degassed DI Water</td>
                                    <td>
                                        <asp:TextBox ID="txtCVP_E19" runat="server" Text="500"></asp:TextBox>
                                        <asp:Label ID="lbE19" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbA20" runat="server" Text="HPA (Filtration Method)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB20" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtCVP_C20" runat="server" Text="1"></asp:TextBox>
                                        <asp:Label ID="lbC20" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>0.1 um Filtered
                                        <br />
                                        Degassed DI Water</td>
                                    <td>
                                        <asp:TextBox ID="txtCVP_E20" runat="server" Text="500"></asp:TextBox>
                                        <asp:Label ID="lbE20" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span6 ">
                        <h6>Results:</h6>
                        <h6>The Specification is based on Seagate's Doc
                            <asp:Label ID="lbDocNo" runat="server" Text=""></asp:Label>
                            for
                            <asp:Label ID="lbCommodity" runat="server" Text=""></asp:Label>
                        </h6>
                    </div>
                </div>
                <%-- Liquid Particle Count (68 KHz) 0.3 --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb1" runat="server">
                            <thead>

                                <tr>
                                    <th>Liquid Particle Count (68 KHz)</th>
                                    <th>Specification Limits<br />
                                        (<asp:Label ID="lbUnit1" runat="server" Text="" />)</th>
                                    <th>Results<br />
                                        (<asp:Label ID="lbUnit2" runat="server" Text="" />)</th>
                                    <th runat="server" id="th1">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Total number of particles ≥ 0.3μm"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td runat="server" id="td1">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr2">
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text="1st Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC26" runat="server" Text="" /></td>
                                    <td runat="server" id="td2">
                                        <asp:CheckBox ID="CheckBox2" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr3">
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Text="2nd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC27" runat="server" Text="" />
                                    </td>
                                    <td runat="server" id="td3">
                                        <asp:CheckBox ID="CheckBox3" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr4">
                                    <td>
                                        <asp:Label ID="Label18" runat="server" Text="3rd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC28" runat="server" Text="" /></td>
                                    <td runat="server" id="td4">
                                        <asp:CheckBox ID="CheckBox4" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label21" runat="server" Text="Average"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB29" runat="server" /></td>
                                    <td>
                                        <asp:Label ID="lbC29" runat="server" />
                                    </td>
                                    <td runat="server" id="td5">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%-- Liquid Particle Count (68 KHz) 0.6 --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb5" runat="server">
                            <thead>
                                <tr>
                                    <th>Liquid Particle Count (68 KHz)</th>
                                    <th>Specification Limits<br />
                                        (<asp:Label ID="lbUnit3" runat="server" Text="" />)</th>
                                    <th>Results<br />
                                        (<asp:Label ID="lbUnit4" runat="server" Text="" />)</th>
                                    <th runat="server" id="th2">
                                        <asp:CheckBox ID="CheckBox5" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>
                                        <asp:Label ID="Label9" runat="server" Text="Total number of particles ≥ 0.6μm"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td runat="server" id="td6">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr6">
                                    <td>
                                        <asp:Label ID="Label10" runat="server" Text="1st Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC33" runat="server" Text="" /></td>
                                    <td runat="server" id="td7">
                                        <asp:CheckBox ID="CheckBox6" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr7">
                                    <td>
                                        <asp:Label ID="Label12" runat="server" Text="2nd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC34" runat="server" Text="" />
                                    </td>
                                    <td runat="server" id="td8">
                                        <asp:CheckBox ID="CheckBox7" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr8">
                                    <td>
                                        <asp:Label ID="Label14" runat="server" Text="3rd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC35" runat="server" Text="" /></td>
                                    <td runat="server" id="td9">
                                        <asp:CheckBox ID="CheckBox8" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label16" runat="server" Text="Average"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB36" runat="server" /></td>
                                    <td>
                                        <asp:Label ID="lbC36" runat="server" />
                                    </td>
                                    <td runat="server" id="td10">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%-- Liquid Particle Count (132 KHz) 0.3 --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb9" runat="server">
                            <thead>

                                <tr>
                                    <th>Liquid Particle Count (132 KHz)</th>
                                    <th>Specification Limits<br />
                                        (<asp:Label ID="lbUnit5" runat="server" Text="" />)</th>
                                    <th>Results<br />
                                        (<asp:Label ID="lbUnit6" runat="server" Text="" />)</th>
                                    <th runat="server" id="th3">
                                        <asp:CheckBox ID="CheckBox9" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>
                                        <asp:Label ID="Label15" runat="server" Text="Total number of particles ≥ 0.3μm"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td runat="server" id="td11">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr10">
                                    <td>
                                        <asp:Label ID="Label17" runat="server" Text="1st Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC40" runat="server" Text="" /></td>
                                    <td runat="server" id="td12">
                                        <asp:CheckBox ID="CheckBox10" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr11">
                                    <td>
                                        <asp:Label ID="Label20" runat="server" Text="2nd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC41" runat="server" Text="" />
                                    </td>
                                    <td runat="server" id="td13">
                                        <asp:CheckBox ID="CheckBox11" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr12">
                                    <td>
                                        <asp:Label ID="Label23" runat="server" Text="3rd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC42" runat="server" Text="" /></td>
                                    <td runat="server" id="td14">
                                        <asp:CheckBox ID="CheckBox12" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label25" runat="server" Text="Average"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB43" runat="server" /></td>
                                    <td>
                                        <asp:Label ID="lbC43" runat="server" />
                                    </td>
                                    <td runat="server" id="td15">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%-- Liquid Particle Count (132 KHz) 0.6 --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb13" runat="server">
                            <thead>

                                <tr>
                                    <th>Liquid Particle Count (132 KHz)</th>
                                    <th>Specification Limits<br />
                                        (<asp:Label ID="lbUnit7" runat="server" Text="" />)</th>
                                    <th>Results<br />
                                        (<asp:Label ID="lbUnit8" runat="server" Text="" />)</th>
                                    <th runat="server" id="th4">
                                        <asp:CheckBox ID="CheckBox13" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>
                                        <asp:Label ID="Label24" runat="server" Text="Total number of particles ≥ 0.6μm"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td runat="server" id="td16">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr14">
                                    <td>
                                        <asp:Label ID="Label26" runat="server" Text="1st Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC47" runat="server" Text="" /></td>
                                    <td runat="server" id="td17">
                                        <asp:CheckBox ID="CheckBox14" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr15">
                                    <td>
                                        <asp:Label ID="Label28" runat="server" Text="2nd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC48" runat="server" Text="" />
                                    </td>
                                    <td runat="server" id="td18">
                                        <asp:CheckBox ID="CheckBox15" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr16">
                                    <td>
                                        <asp:Label ID="Label30" runat="server" Text="3rd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC49" runat="server" Text="" /></td>
                                    <td runat="server" id="td19">
                                        <asp:CheckBox ID="CheckBox16" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label32" runat="server" Text="Average"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB50" runat="server" /></td>
                                    <td>
                                        <asp:Label ID="lbC50" runat="server" />
                                    </td>
                                    <td runat="server" id="td20">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%-- Hard Particle Analysis (LPC 68 KHz) --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb17" runat="server">
                            <thead>

                                <tr>
                                    <th>Hard Particle Analysis<br />
                                        (LPC 68 KHz)</th>
                                    <th>Specification Limits<br />
                                        (<asp:Label ID="lbUnit9" runat="server" Text="" />)</th>
                                    <th>Results<br />
                                        (<asp:Label ID="lbUnit10" runat="server" Text="" />)</th>
                                    <th runat="server" id="th5">
                                        <asp:CheckBox ID="CheckBox17" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr runat="server" id="tr18">
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Text="Hard Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB54" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC54" runat="server" />
                                    </td>
                                    <td runat="server" id="td21">
                                        <asp:CheckBox ID="CheckBox18" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr19">
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Text="Magnetic Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB55" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC55" runat="server" />
                                    </td>
                                    <td runat="server" id="td22">
                                        <asp:CheckBox ID="CheckBox19" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr20">
                                    <td>
                                        <asp:Label ID="Label13" runat="server" Text="SST Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB56" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC56" runat="server" />
                                    </td>
                                    <td runat="server" id="td23">
                                        <asp:CheckBox ID="CheckBox20" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr21">
                                    <td>
                                        <asp:Label ID="Label22" runat="server" Text="SST + Fe based + FeO"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB57" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC57" runat="server" />
                                    </td>
                                    <td runat="server" id="td24">
                                        <asp:CheckBox ID="CheckBox21" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr22">
                                    <td>
                                        <asp:Label ID="Label29" runat="server" Text="MgSiO Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB58" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC58" runat="server" />
                                    </td>
                                    <td runat="server" id="td25">
                                        <asp:CheckBox ID="CheckBox22" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr23">
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="PZT Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB59" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC59" runat="server" />
                                    </td>
                                    <td runat="server" id="td26">
                                        <asp:CheckBox ID="CheckBox23" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr24">
                                    <td>
                                        <asp:Label ID="Label35" runat="server" Text="Tin Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB60" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC60" runat="server" />
                                    </td>
                                    <td runat="server" id="td27">
                                        <asp:CheckBox ID="CheckBox24" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr25">
                                    <td>
                                        <asp:Label ID="Label38" runat="server" Text="Metal Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB61" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC61" runat="server" />
                                    </td>
                                    <td runat="server" id="td28">
                                        <asp:CheckBox ID="CheckBox25" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr26">
                                    <td>
                                        <asp:Label ID="Label41" runat="server" Text="Individual Foreign Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB62" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC62" runat="server" />
                                    </td>
                                    <td runat="server" id="td29">
                                        <asp:CheckBox ID="CheckBox26" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr27">
                                    <td>
                                        <asp:Label ID="Label44" runat="server" Text="Ni Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB63" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC63" runat="server" />
                                    </td>
                                    <td runat="server" id="td30">
                                        <asp:CheckBox ID="CheckBox27" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr28">
                                    <td>
                                        <asp:Label ID="Label47" runat="server" Text="Other Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB64" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC64" runat="server" />
                                    </td>
                                    <td runat="server" id="td31">
                                        <asp:CheckBox ID="CheckBox28" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label50" runat="server" Text="Totals"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB65" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC65" runat="server" />
                                    </td>
                                    <td runat="server" id="td32">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%-- Hard Particle Analysis (LPC 132 KHz) --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb29" runat="server">
                            <thead>

                                <tr>
                                    <th>Hard Particle Analysis<br />
                                        (LPC 132 KHz)</th>
                                    <th>Specification Limits<br />
                                        (<asp:Label ID="lbUnit11" runat="server" Text="" />)</th>
                                    <th>Results<br />
                                        (<asp:Label ID="lbUnit12" runat="server" Text="" />)</th>
                                    <th runat="server" id="th6">
                                        <asp:CheckBox ID="CheckBox29" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr runat="server" id="tr30">
                                    <td>
                                        <asp:Label ID="Label19" runat="server" Text="Hard Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB69" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC69" runat="server" />
                                    </td>
                                    <td runat="server" id="td33">
                                        <asp:CheckBox ID="CheckBox30" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr31">
                                    <td>
                                        <asp:Label ID="Label33" runat="server" Text="Magnetic Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB70" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC70" runat="server" />
                                    </td>
                                    <td runat="server" id="td34">
                                        <asp:CheckBox ID="CheckBox31" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr32">
                                    <td>
                                        <asp:Label ID="Label37" runat="server" Text="SST Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB71" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC71" runat="server" />
                                    </td>
                                    <td runat="server" id="td35">
                                        <asp:CheckBox ID="CheckBox32" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr33">
                                    <td>
                                        <asp:Label ID="Label42" runat="server" Text="SST + Fe based + FeO"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB72" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC72" runat="server" />
                                    </td>
                                    <td runat="server" id="td36">
                                        <asp:CheckBox ID="CheckBox33" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr34">
                                    <td>
                                        <asp:Label ID="Label46" runat="server" Text="MgSiO Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB73" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC73" runat="server" />
                                    </td>
                                    <td runat="server" id="td37">
                                        <asp:CheckBox ID="CheckBox34" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr35">
                                    <td>
                                        <asp:Label ID="Label51" runat="server" Text="PZT Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB74" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC74" runat="server" />
                                    </td>
                                    <td runat="server" id="td38">
                                        <asp:CheckBox ID="CheckBox35" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr36">
                                    <td>
                                        <asp:Label ID="Label54" runat="server" Text="Tin Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB75" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC75" runat="server" />
                                    </td>
                                    <td runat="server" id="td39">
                                        <asp:CheckBox ID="CheckBox36" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr37">
                                    <td>
                                        <asp:Label ID="Label57" runat="server" Text="Metal Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB76" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC76" runat="server" />
                                    </td>
                                    <td runat="server" id="td40">
                                        <asp:CheckBox ID="CheckBox37" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr38">
                                    <td>
                                        <asp:Label ID="Label60" runat="server" Text="Ni Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB77" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC77" runat="server" />
                                    </td>
                                    <td runat="server" id="td41">
                                        <asp:CheckBox ID="CheckBox38" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr39">
                                    <td>
                                        <asp:Label ID="Label63" runat="server" Text="Individual Foreign Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB78" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC78" runat="server" />
                                    </td>
                                    <td runat="server" id="td42">
                                        <asp:CheckBox ID="CheckBox39" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr40">
                                    <td>
                                        <asp:Label ID="Label66" runat="server" Text="Other Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB79" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC79" runat="server" />
                                    </td>
                                    <td runat="server" id="td43">
                                        <asp:CheckBox ID="CheckBox40" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label69" runat="server" Text="Totals"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB80" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbC80" runat="server" />
                                    </td>
                                    <td runat="server" id="td44">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <h6>Remark: ** PZT Specification only applicable to Headstacks with micro-actuators.</h6>
                <h6>Note: This report was performed HPA Filtration test by ALS Singapore.</h6>
                <%-- Classification--%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="Table3" runat="server">
                            <thead>
                                <tr>
                                    <th>Classification</th>
                                    <th>Type of Particles</th>
                                    <th>Results, Particle/sqcm</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr runat="server" id="tr45">
                                    <td>Hard Particles</td>
                                    <td>AlMgO</td>
                                    <td runat="server" id="td45">
                                        <asp:Label ID="lbC84" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr46">
                                    <td>&nbsp;</td>
                                    <td>AlO</td>
                                    <td runat="server" id="td46">
                                        <asp:Label ID="lbC85" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr47">
                                    <td>&nbsp;</td>
                                    <td>AlOTiC</td>
                                    <td runat="server" id="td47">
                                        <asp:Label ID="lbC86" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr48">
                                    <td>&nbsp;</td>
                                    <td>CrO</td>
                                    <td runat="server" id="td48">
                                        <asp:Label ID="lbC87" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr49">
                                    <td>&nbsp;</td>
                                    <td>FeSiO</td>
                                    <td runat="server" id="td49">
                                        <asp:Label ID="lbC88" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr50">
                                    <td>&nbsp;</td>
                                    <td>NbB</td>
                                    <td runat="server" id="td50">
                                        <asp:Label ID="lbC89" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr51">
                                    <td>&nbsp;</td>
                                    <td>SiC</td>
                                    <td runat="server" id="td51">
                                        <asp:Label ID="lbC90" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr52">
                                    <td>&nbsp;</td>
                                    <td>SiO</td>
                                    <td runat="server" id="td52">
                                        <asp:Label ID="lbC91" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr53">
                                    <td>&nbsp;</td>
                                    <td>TiB</td>
                                    <td runat="server" id="td53">
                                        <asp:Label ID="lbC92" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr54">
                                    <td>&nbsp;</td>
                                    <td>TiC</td>
                                    <td runat="server" id="td54">
                                        <asp:Label ID="lbC93" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr55">
                                    <td>&nbsp;</td>
                                    <td>TiO</td>
                                    <td runat="server" id="td55">
                                        <asp:Label ID="lbC94" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr56">
                                    <td>&nbsp;</td>
                                    <td>TiSn</td>
                                    <td runat="server" id="td56">
                                        <asp:Label ID="lbC95" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr57">
                                    <td>&nbsp;</td>
                                    <td>TiV</td>
                                    <td runat="server" id="td57">
                                        <asp:Label ID="lbC96" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr58">
                                    <td>&nbsp;</td>
                                    <td>WC</td>
                                    <td runat="server" id="td58">
                                        <asp:Label ID="lbC97" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr59">
                                    <td>&nbsp;</td>
                                    <td>ZrC</td>
                                    <td runat="server" id="td59">
                                        <asp:Label ID="lbC98" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr60">
                                    <td>&nbsp;</td>
                                    <td>ZrO</td>
                                    <td runat="server" id="td60">
                                        <asp:Label ID="lbC99" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr61">
                                    <td>Magnetic particles</td>
                                    <td>Nd based</td>
                                    <td runat="server" id="td61">
                                        <asp:Label ID="lbC100" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr62">
                                    <td>&nbsp;</td>
                                    <td>Sm based</td>
                                    <td runat="server" id="td62">
                                        <asp:Label ID="lbC101" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr63">
                                    <td>&nbsp;</td>
                                    <td>Sr based</td>
                                    <td runat="server" id="td63">
                                        <asp:Label ID="lbC102" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr64">
                                    <td>SST Particles</td>
                                    <td>SST300s (Fe/Cr/Ni) </td>
                                    <td runat="server" id="td64">
                                        <asp:Label ID="lbC103" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr65">
                                    <td>&nbsp;</td>
                                    <td>SST400s (Fe/Cr) </td>
                                    <td runat="server" id="td65">
                                        <asp:Label ID="lbC104" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr66">
                                    <td>Metal Particles</td>
                                    <td>Ag based</td>
                                    <td runat="server" id="td66">
                                        <asp:Label ID="lbC105" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr67">
                                    <td>&nbsp;</td>
                                    <td>Al based</td>
                                    <td runat="server" id="td67">
                                        <asp:Label ID="lbC106" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr68">
                                    <td>&nbsp;</td>
                                    <td>Au based</td>
                                    <td runat="server" id="td68">
                                        <asp:Label ID="lbC107" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr69">
                                    <td>&nbsp;</td>
                                    <td>Cu based</td>
                                    <td runat="server" id="td69">
                                        <asp:Label ID="lbC108" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr70">
                                    <td>&nbsp;</td>
                                    <td>Fe based</td>
                                    <td runat="server" id="td70">
                                        <asp:Label ID="lbC109" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr71">
                                    <td>&nbsp;</td>
                                    <td>MnCrS</td>
                                    <td runat="server" id="td71">
                                        <asp:Label ID="lbC110" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr72">
                                    <td>&nbsp;</td>
                                    <td>Ni based</td>
                                    <td runat="server" id="td72">
                                        <asp:Label ID="lbC111" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr73">
                                    <td>&nbsp;</td>
                                    <td>NiP</td>
                                    <td runat="server" id="td73">
                                        <asp:Label ID="lbC112" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr74">
                                    <td>&nbsp;</td>
                                    <td>Pt based</td>
                                    <td runat="server" id="td74">
                                        <asp:Label ID="lbC113" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr75">
                                    <td>&nbsp;</td>
                                    <td>Sb based</td>
                                    <td runat="server" id="td75">
                                        <asp:Label ID="lbC114" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr76">
                                    <td>&nbsp;</td>
                                    <td>Sn based </td>
                                    <td runat="server" id="td76">
                                        <asp:Label ID="lbC115" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr77">
                                    <td>&nbsp;</td>
                                    <td>SnPb</td>
                                    <td runat="server" id="td77">
                                        <asp:Label ID="lbC116" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr78">
                                    <td>&nbsp;</td>
                                    <td>Zn based</td>
                                    <td runat="server" id="td78">
                                        <asp:Label ID="lbC117" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr79">
                                    <td>&nbsp;</td>
                                    <td>AlSi(FeCrCuZnMn)</td>
                                    <td runat="server" id="td79">
                                        <asp:Label ID="lbC118" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr80">
                                    <td>&nbsp;</td>
                                    <td>NiFe</td>
                                    <td runat="server" id="td80">
                                        <asp:Label ID="lbC119" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr81">
                                    <td>&nbsp;</td>
                                    <td>ZnPFe</td>
                                    <td runat="server" id="td81">
                                        <asp:Label ID="lbC120" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr82">
                                    <td>&nbsp;</td>
                                    <td>CrCoNiP (disc material)</td>
                                    <td runat="server" id="td82">
                                        <asp:Label ID="lbC121" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr83">
                                    <td>&nbsp;</td>
                                    <td>NiPCr</td>
                                    <td runat="server" id="td83">
                                        <asp:Label ID="lbC122" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr84">
                                    <td>&nbsp;</td>
                                    <td>NiPCrFe</td>
                                    <td runat="server" id="td84">
                                        <asp:Label ID="lbC123" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr85">
                                    <td>&nbsp;</td>
                                    <td>CuZn</td>
                                    <td runat="server" id="td85">
                                        <asp:Label ID="lbC124" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr86">
                                    <td>Other Particles</td>
                                    <td>FeO</td>
                                    <td runat="server" id="td86">
                                        <asp:Label ID="lbC125" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr87">
                                    <td>&nbsp;</td>
                                    <td>AlFeO</td>
                                    <td runat="server" id="td87">
                                        <asp:Label ID="lbC126" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr88">
                                    <td>&nbsp;</td>
                                    <td>AlNiO</td>
                                    <td runat="server" id="td88">
                                        <asp:Label ID="lbC127" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr89">
                                    <td>&nbsp;</td>
                                    <td>AlSiO</td>
                                    <td runat="server" id="td89">
                                        <asp:Label ID="lbC128" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr90">
                                    <td>&nbsp;</td>
                                    <td>Cl based</td>
                                    <td runat="server" id="td90">
                                        <asp:Label ID="lbC129" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr91">
                                    <td>&nbsp;</td>
                                    <td>FeMgSiO</td>
                                    <td runat="server" id="td91">
                                        <asp:Label ID="lbC130" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr92">
                                    <td>&nbsp;</td>
                                    <td>MgCaO</td>
                                    <td runat="server" id="td92">
                                        <asp:Label ID="lbC131" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr93">
                                    <td>&nbsp;</td>
                                    <td>MgSiO</td>
                                    <td runat="server" id="td93">
                                        <asp:Label ID="lbC132" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr94">
                                    <td>&nbsp;</td>
                                    <td>S based</td>
                                    <td runat="server" id="td94">
                                        <asp:Label ID="lbC133" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr95">
                                    <td>&nbsp;</td>
                                    <td>F based</td>
                                    <td runat="server" id="td95">
                                        <asp:Label ID="lbC134" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr96">
                                    <td>&nbsp;</td>
                                    <td>Ca based</td>
                                    <td runat="server" id="td96">
                                        <asp:Label ID="lbC135" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr97">
                                    <td>&nbsp;</td>
                                    <td>Na based</td>
                                    <td runat="server" id="td97">
                                        <asp:Label ID="lbC136" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr98">
                                    <td>&nbsp;</td>
                                    <td>K based</td>
                                    <td runat="server" id="td98">
                                        <asp:Label ID="lbC137" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr99">
                                    <td>&nbsp;</td>
                                    <td>Anodised Al</td>
                                    <td runat="server" id="td99">
                                        <asp:Label ID="lbC138" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr100">
                                    <td>&nbsp;</td>
                                    <td>PZT</td>
                                    <td runat="server" id="td100">
                                        <asp:Label ID="lbC139" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr101">
                                    <td>&nbsp;</td>
                                    <td>Pb base</td>
                                    <td runat="server" id="td101">
                                        <asp:Label ID="lbC140" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr102">
                                    <td>&nbsp;</td>
                                    <td>Others</td>
                                    <td runat="server" id="td102">
                                        <asp:Label ID="lbC141" runat="server" />
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr runat="server" id="tr109">
                                    <td colspan="3"></td>
                                </tr>
                                <tr runat="server" id="tr110">
                                    <td colspan="3"></td>
                                </tr>
                                <tr runat="server" id="tr103">
                                    <td>Analysis Details</td>
                                    <td>% Area Analysed (A/7.07mm2)</td>
                                    <td runat="server" id="td103">
                                        <asp:Label ID="lbC144" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr104">
                                    <td></td>
                                    <td>Area Analysed (mm2)</td>
                                    <td runat="server" id="td104">
                                        <asp:Label ID="lbC145" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr105">
                                    <td></td>
                                    <td>Extraction Volume (mL)</td>
                                    <td runat="server" id="td105">
                                        <asp:Label ID="lbC146" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr106">
                                    <td></td>
                                    <td>Filtered Volume (mL)</td>
                                    <td runat="server" id="td106">
                                        <asp:Label ID="lbC147" runat="server" />
                                    </td>
                                </tr>

                                <tr runat="server" id="tr107">
                                    <td></td>
                                    <td>No of Parts Extracted</td>
                                    <td runat="server" id="td107">
                                        <asp:Label ID="lbC148" runat="server" />
                                    </td>
                                </tr>

                                <tr runat="server" id="tr108">
                                    <td></td>
                                    <td>Magnification</td>
                                    <td runat="server" id="td108">
                                        <asp:Label ID="lbC149" runat="server" />
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <br />


            </div>

            <div class="well">
                <h3><i class="icon-tasks"></i>Operations (<asp:Label ID="lbJobStatus" runat="server" Text=""></asp:Label>)</h3>
                <asp:Panel ID="pSpecification" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <label class="control-label" for="ddlSpecification">Specification:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSpecification" runat="server" class="span12 chosen" DataTextField="A" DataValueField="ID" OnSelectedIndexChanged="ddlSpecification_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pStatus" runat="server">
                    <div class="row-fluid">

                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="ddlStatus">
                                    <asp:Label ID="lbStatus" runat="server" Text="Approve Status:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="span6" DataTextField="name" DataValueField="ID" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pRemark" runat="server">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="txtRemark">
                                    <asp:Label ID="Label1" runat="server" Text="Remark:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtRemark" name="txtRemark" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pDisapprove" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <asp:Label ID="lbAssignTo" runat="server" Text="Assign To:" class="control-label"></asp:Label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAssignTo" runat="server" class="span12 chosen" DataTextField="name" DataValueField="ID" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>


                </asp:Panel>
                <asp:Panel ID="pDownload" runat="server">
                    <ul>
                        <li>Link for download Cover Page
                            <asp:LinkButton ID="lbDownload" runat="server" OnClick="lbDownload_Click">
                                <i class="icon-download-alt"></i>
                                <asp:Label ID="lbDownloadName" runat="server" Text="Label"></asp:Label>
                            </asp:LinkButton>
                        </li>
                    </ul>
                </asp:Panel>
                <asp:Panel ID="pUploadfile" runat="server">
                    <div class="row-fluid fileupload-buttonbar">
                        <div class="span6">
                            <label class="control-label" for="ddlSpecification">Uplod file:</label>
                            <span class="btn green fileinput-button">
                                <i class="icon-plus icon-white"></i>
                                <span>Add files...</span>
                                <asp:FileUpload ID="btnUpload" runat="server" />
                            </span>
                        </div>
                    </div>
                    <ul>
                        <li>The maximum file size for uploads in this demo is <strong>5 MB</strong> (default file size is unlimited).</li>
                        <li>Only word or pdf files (<strong>DOC, DOCX, PDF</strong>) are allowed in this demo (by default there is no file type restriction).</li>
                    </ul>
                    <br />
                    <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                    <br />
                </asp:Panel>

            </div>

            <div class="form-actions clearfix">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn" />
            </div>
            <!-- END PAGE CONTENT-->

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <asp:PostBackTrigger ControlID="lbDownload" />
        </Triggers>
    </asp:UpdatePanel>
</form>
