﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WD_IC_Component_1_3.ascx.cs" Inherits="ALS.ALSI.Web.view.template.WD_IC_Component_1_3" %>

<script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date-picker').datepicker();
    });
</script>
<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <!-- BEGIN COVERPAGE-TH CONTENT-->
            <div class="row-fluid invoice" id="invDiv" runat="server">
                <div>
                    CUSTOMER PO NO.:
                        <asp:Label ID="lbPoNo" runat="server" Text=""></asp:Label><br />
                    ALS THAILAND REF NO.:
                        <asp:Label ID="lbRefNo" runat="server" Text=""></asp:Label><br />
                    DATE:
                        <asp:Label ID="lbDate" runat="server" Text=""></asp:Label><br />
                    COMPANY:
                        <asp:Label ID="lbCompany" runat="server" Text=""></asp:Label><br />
                    <br />
                    DATE SAMPLE RECEIVED:
                        <asp:Label ID="lbDateSampleReceived" runat="server" Text=""></asp:Label><br />
                    DATE TEST COMPLETED:
                        <asp:Label ID="lbDateTestCompleted" runat="server" Text=""></asp:Label><br />
                    <br />
                    SAMPLE DESCRIPTION: One lot of sample was received with references:<br />
                    <asp:Label ID="lbSampleDescription" runat="server" Text=""></asp:Label>
                </div>

                <div class="row-fluid">
                    <div class="span12 ">
                        <h5>METHOD/PROCEDURE:</h5>
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Test</th>
                                    <th>Procedure No</th>
                                    <th>Number of pieces used for extraction</th>
                                    <th>Extraction Medium</th>
                                    <th>Extraction Volume</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>IC</td>
                                    <td>2092-000121 Rev AB</td>
                                    <td>
                                        <asp:Label ID="txtC18" runat="server" Text=""></asp:Label>

                                    </td>
                                    <td>Ultrapure Water at 80oC</td>
                                    <td>
                                        <asp:Label ID="txtE18" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span6 ">
                        <h6>Results:</h6>
                        <h6>The specification is based on Western Digital's document no.
                                                    <asp:Label ID="lbDocRev" runat="server" Text=""></asp:Label>
                            <asp:Label ID="lbDesc" runat="server" Text=""></asp:Label></h6>
                        <%--<h6>This sample is no Seagate specification reference.</h6>--%>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Required Test</th>
                                    <th>Analytes</th>
                                    <th>Specification Limits<br /> (<asp:Label ID="lbB24" runat="server" Text="" />)</th>
                                    <th>Results<br />(<asp:Label ID="lbC24" runat="server" Text="" />)</th>
                                    <th>Method Detection <br />Limit (<asp:Label ID="lbB24_1" runat="server" Text="" />)</th>
                                    <th>PASS / FAIL</th>
                                    <th runat="server" id="th1">
                                        <asp:Label ID="Label2" runat="server" Text="Show"></asp:Label>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr runat="server" id="tr1">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Fluoride as F"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB25" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC25" runat="server" Text="" /></td>

                                    <td>
                                        <asp:Label ID="lbD25" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE25" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td1">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr2">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text="Chloride as Cl"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB26" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC26" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD26" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE26" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td2">
                                        <asp:CheckBox ID="CheckBox2" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <%--                                <tr runat="server" id="tr3">
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Text="Nitrite as NO2"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB27" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC27" runat="server" Text="" /></td>
                                    <td runat="server" id="td3">
                                        <asp:CheckBox ID="CheckBox3" runat="server" Checked="True" />
                                    </td>
                                </tr>--%>
                                <tr runat="server" id="tr4">
                                    <td></td>
                                    <td>Bromide as Br</td>
                                    <td>
                                        <asp:Label ID="lbB28" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC28" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD28" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE28" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td4">
                                        <asp:CheckBox ID="CheckBox4" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr5">
                                    <td>IC (Anions)</td>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Text="Nitrate as NO3"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB29" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC29" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD29" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE29" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td5">
                                        <asp:CheckBox ID="CheckBox5" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr6">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Text="Sulphate as SO4"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB30" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC30" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD30" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE30" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td6">
                                        <asp:CheckBox ID="CheckBox6" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr7">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label9" runat="server" Text="Phosphate as PO4"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB31" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC31" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD31" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE31" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td7">
                                        <asp:CheckBox ID="CheckBox7" runat="server" Checked="True" />
                                    </td>
                                </tr>

                                <tr runat="server" id="tr8">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label10" runat="server" Text="Total Anions"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB32" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC32" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD32" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE32" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td8">
                                        <asp:CheckBox ID="CheckBox8" runat="server" Checked="True" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Required Test</th>
                                    <th>Analytes</th>
                                    <th>Specification Limits<br /> (<asp:Label ID="lbB33" runat="server" Text="" />)</th>
                                    <th>Results<br />(<asp:Label ID="lbC33" runat="server" Text="" />)</th>
                                    <th>Method Detection Limit<br /> (<asp:Label ID="lbB24_2" runat="server" Text="" />)</th>
                                    <th>PASS / FAIL</th>
                                    <th runat="server" id="th2">
                                        <asp:Label ID="Label3" runat="server" Text="Show"></asp:Label>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr runat="server" id="tr9">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label11" runat="server" Text="Lithium as Li"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB34" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC34" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD34" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE34" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td9">
                                        <asp:CheckBox ID="CheckBox9" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr10">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label12" runat="server" Text="Sodium as Na"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB35" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC35" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD35" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE35" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td10">
                                        <asp:CheckBox ID="CheckBox10" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr11">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label13" runat="server" Text="Ammonium as NH4"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB36" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC36" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD36" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE36" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td11">
                                        <asp:CheckBox ID="CheckBox11" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr12">
                                    <td>IC (Cations)</td>
                                    <td>
                                        <asp:Label ID="Label14" runat="server" Text="Potassium as K"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB37" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC37" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD37" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE37" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td12">
                                        <asp:CheckBox ID="CheckBox12" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr13">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label15" runat="server" Text="Magnesium as Mg"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB38" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC38" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD38" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE38" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td13">
                                        <asp:CheckBox ID="CheckBox13" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr14">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label16" runat="server" Text="Calcium as Ca"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB39" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC39" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD39" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE39" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td14">
                                        <asp:CheckBox ID="CheckBox14" runat="server" Checked="True" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr15">
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label17" runat="server" Text="Total Cations"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB40" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC40" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbD40" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbE40" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="td15">
                                        <asp:CheckBox ID="CheckBox15" runat="server" Checked="True" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="well">
                <h3><i class="icon-tasks"></i>Operations (<asp:Label ID="lbJobStatus" runat="server" Text=""></asp:Label>)</h3>
                <asp:Panel ID="pSpecification" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <label class="control-label" for="ddlSpecification">Specification:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSpecification" runat="server" class="span12 chosen" DataTextField="A" DataValueField="ID" OnSelectedIndexChanged="ddlSpecification_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pStatus" runat="server">
                    <div class="row-fluid">

                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="ddlStatus">
                                    <asp:Label ID="lbStatus" runat="server" Text="Approve Status:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="span6" DataTextField="name" DataValueField="ID" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pRemark" runat="server">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="txtRemark">
                                    <asp:Label ID="Label1" runat="server" Text="Remark:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtRemark" name="txtRemark" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pDisapprove" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <asp:Label ID="lbAssignTo" runat="server" Text="Assign To:" class="control-label"></asp:Label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAssignTo" runat="server" class="span12 chosen" DataTextField="name" DataValueField="ID" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>


                </asp:Panel>
                <asp:Panel ID="pDownload" runat="server">
                    <ul>
                        <li>Link for download Cover Page
                            <asp:LinkButton ID="lbDownload" runat="server" OnClick="lbDownload_Click">
                                <i class="icon-download-alt"></i>
                                <asp:Label ID="lbDownloadName" runat="server" Text="Label"></asp:Label>
                            </asp:LinkButton>
                        </li>
                    </ul>
                </asp:Panel>
                <asp:Panel ID="pUploadfile" runat="server">
                    <div class="row-fluid fileupload-buttonbar">
                        <div class="span6">
                            <label class="control-label" for="ddlSpecification">Uplod file:</label>
                            <span class="btn green fileinput-button">
                                <i class="icon-plus icon-white"></i>
                                <span>Add files...</span>
                                <asp:FileUpload ID="btnUpload" runat="server" />
                            </span>
                        </div>
                    </div>
                    <ul>
                        <li>The maximum file size for uploads in this demo is <strong>5 MB</strong> (default file size is unlimited).</li>
                        <li>Only word or pdf files (<strong>DOC, DOCX, PDF</strong>) are allowed in this demo (by default there is no file type restriction).</li>
                    </ul>
                    <br />
                    <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                    <br />
                </asp:Panel>

            </div>

            <div class="form-actions clearfix">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn" />
            </div>
            <!-- END PAGE CONTENT-->

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <asp:PostBackTrigger ControlID="lbDownload" />
        </Triggers>
    </asp:UpdatePanel>
</form>
