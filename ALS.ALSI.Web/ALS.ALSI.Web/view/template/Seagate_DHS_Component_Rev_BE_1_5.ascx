﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Seagate_DHS_Component_Rev_BE_1_5.ascx.cs" Inherits="ALS.ALSI.Web.view.template.Seagate_DHS_Component_Rev_BE_1_5" %>
<script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.date-picker').datepicker();
    });
</script>

<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <!-- BEGIN COVERPAGE-TH CONTENT-->
            <div class="row-fluid invoice" id="invDiv" runat="server">
                <div>
                    CUSTOMER PO NO.:
                        <asp:Label ID="lbPoNo" runat="server" Text=""></asp:Label><br />
                    ALS THAILAND REF NO.:
                        <asp:Label ID="lbRefNo" runat="server" Text=""></asp:Label><br />
                    DATE:
                        <asp:Label ID="lbDate" runat="server" Text=""></asp:Label><br />
                    COMPANY:
                        <asp:Label ID="lbCompany" runat="server" Text=""></asp:Label><br />
                    <br />
                    DATE SAMPLE RECEIVED:
                        <asp:Label ID="lbDateSampleReceived" runat="server" Text=""></asp:Label><br />
                    DATE TEST COMPLETED:
                        <asp:Label ID="lbDateTestCompleted" runat="server" Text=""></asp:Label><br />
                    <br />
                    SAMPLE DESCRIPTION: One lot of sample was received with references:<br />
                    <asp:Label ID="lbSampleDescription" runat="server" Text=""></asp:Label>
                </div>

                <div class="row-fluid">
                    <div class="span12 ">
                        <h5>METHOD/PROCEDURE:</h5>
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Analysis</th>
                                    <th>Procedure No</th>
                                    <th>Sample Size</th>
                                    <th>Sampling Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>DHS</td>
                                    <td>20800020-001 Rev K</td>
                                    <td>1 piece</td>
                                    <td>
                                        <asp:Label ID="lbD18" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span6 ">
                        <h6>Results:</h6>
                        <h6>
                            <asp:Label ID="lbResultDesc" runat="server" Text=""></asp:Label></h6>

                        <asp:GridView ID="gvResult" runat="server" AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered mini" ShowHeaderWhenEmpty="True" DataKeyNames="ID,component_id">
                            <Columns>
                                <asp:BoundField HeaderText="Compound" DataField="name" ItemStyle-HorizontalAlign="Left" SortExpression="name" />
                                <asp:BoundField HeaderText="Maximum Allowable Amount" DataField="ng_part" ItemStyle-HorizontalAlign="Center" SortExpression="ng_part" />
                                <asp:TemplateField HeaderText="Results" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Literal ID="litResult" runat="server" Text='<%# Eval("result")%>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtResult" runat="server" Text='<%# Eval("result")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <div class="pagination">
                                    <ul>
                                        <li>
                                            <asp:LinkButton ID="btnFirst" runat="server" CommandName="Page" CommandArgument="First"
                                                CausesValidation="false" ToolTip="First Page"><i class="icon-fast-backward"></i></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="btnPrev" runat="server" CommandName="Page" CommandArgument="Prev"
                                                CausesValidation="false" ToolTip="Previous Page"><i class="icon-backward"></i> Prev</asp:LinkButton>
                                        </li>
                                        <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                                        <li>
                                            <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" CommandArgument="Next"
                                                CausesValidation="false" ToolTip="Next Page">Next <i class="icon-forward"></i></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="btnLast" runat="server" CommandName="Page" CommandArgument="Last"
                                                CausesValidation="false" ToolTip="Last Page"><i class="icon-fast-forward"></i></asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                <div class="data-not-found">
                                    <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                                </div>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>

            </div>

            <div class="well">
                <h3><i class="icon-tasks"></i>Operations (<asp:Label ID="lbJobStatus" runat="server" Text=""></asp:Label>)</h3>
                <asp:Panel ID="pSpecification" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <label class="control-label" for="ddlSpecification">Specification:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSpecification" runat="server" class="span12 chosen" DataTextField="A" DataValueField="ID" OnSelectedIndexChanged="ddlSpecification_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pStatus" runat="server">
                    <div class="row-fluid">

                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="ddlStatus">
                                    <asp:Label ID="lbStatus" runat="server" Text="Approve Status:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="span6" DataTextField="name" DataValueField="ID" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pRemark" runat="server">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="txtRemark">
                                    <asp:Label ID="Label1" runat="server" Text="Remark:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtRemark" name="txtRemark" runat="server" CssClass="large m-wrap"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pDisapprove" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <asp:Label ID="lbAssignTo" runat="server" Text="Assign To:" class="control-label"></asp:Label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAssignTo" runat="server" class="span12 chosen" DataTextField="name" DataValueField="ID" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>


                </asp:Panel>
                <asp:Panel ID="pDownload" runat="server">
                    <ul>
                        <li>Link for download Cover Page
                            <asp:LinkButton ID="lbDownload" runat="server" OnClick="lbDownload_Click">
                                <i class="icon-download-alt"></i>
                                <asp:Label ID="lbDownloadName" runat="server" Text="Label"></asp:Label>
                            </asp:LinkButton>
                        </li>
                    </ul>
                </asp:Panel>
                <asp:Panel ID="pUploadfile" runat="server">
                    <div class="row-fluid fileupload-buttonbar">
                        <div class="span6">
                            <label class="control-label" for="ddlSpecification">Uplod file:</label>
                            <span class="btn green fileinput-button">
                                <i class="icon-plus icon-white"></i>
                                <span>Add files...</span>
                                <asp:FileUpload ID="btnUpload" runat="server" />
                            </span>
                        </div>
                    </div>
                    <ul>
                        <li>The maximum file size for uploads in this demo is <strong>5 MB</strong> (default file size is unlimited).</li>
                        <li>Only word or pdf files (<strong>DOC, DOCX, PDF</strong>) are allowed in this demo (by default there is no file type restriction).</li>
                    </ul>
                    <br />
                    <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                    <br />
                </asp:Panel>

            </div>


            <div class="form-actions clearfix">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn" />
            </div>

            <!-- END PAGE CONTENT-->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <asp:PostBackTrigger ControlID="lbDownload" />
        </Triggers>
    </asp:UpdatePanel>
</form>

