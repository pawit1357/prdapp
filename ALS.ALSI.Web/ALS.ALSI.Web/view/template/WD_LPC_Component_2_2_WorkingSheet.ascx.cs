﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using ALS.ALSI.Web.view.request;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;

namespace ALS.ALSI.Web.view.template
{
    public partial class WD_LPC_Component_2_2_WorkingSheet : System.Web.UI.UserControl
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Seagate_DHS_Component_Rev_BE_1_5_WorkingSheet));

        #region "Property"

        public template_wd_lpc_coverpage Lpc
        {
            get { return (template_wd_lpc_coverpage)Session[GetType().Name + "Lpc"]; }
            set { Session[GetType().Name + "Lpc"] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }



        public Double[] B1
        {
            get { return (Double[])Session[GetType().Name + "B1"]; }
            set { Session[GetType().Name + "B1"] = value; }
        }
        public Double[] B2
        {
            get { return (Double[])Session[GetType().Name + "B2"]; }
            set { Session[GetType().Name + "B2"] = value; }
        }
        public Double[] B3
        {
            get { return (Double[])Session[GetType().Name + "B3"]; }
            set { Session[GetType().Name + "B3"] = value; }
        }
        public Double[] B4
        {
            get { return (Double[])Session[GetType().Name + "B4"]; }
            set { Session[GetType().Name + "B4"] = value; }
        }
        public Double[] B5
        {
            get { return (Double[])Session[GetType().Name + "B5"]; }
            set { Session[GetType().Name + "B5"] = value; }
        }

        public Double[] S1
        {
            get { return (Double[])Session[GetType().Name + "S1"]; }
            set { Session[GetType().Name + "S1"] = value; }
        }
        public Double[] S2
        {
            get { return (Double[])Session[GetType().Name + "S2"]; }
            set { Session[GetType().Name + "S2"] = value; }
        }
        public Double[] S3
        {
            get { return (Double[])Session[GetType().Name + "S3"]; }
            set { Session[GetType().Name + "S3"] = value; }
        }
        public Double[] S4
        {
            get { return (Double[])Session[GetType().Name + "S4"]; }
            set { Session[GetType().Name + "S4"] = value; }
        }
        public Double[] S5
        {
            get { return (Double[])Session[GetType().Name + "S5"]; }
            set { Session[GetType().Name + "S5"] = value; }
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "Lpc");
            Session.Remove(GetType().Name + Constants.PREVIOUS_PATH);
            Session.Remove(GetType().Name + "SampleID");
            Session.Remove(GetType().Name + "B1");
            Session.Remove(GetType().Name + "B2");
            Session.Remove(GetType().Name + "B3");
            Session.Remove(GetType().Name + "B4");
            Session.Remove(GetType().Name + "B5");
            Session.Remove(GetType().Name + "S1");
            Session.Remove(GetType().Name + "S2");
            Session.Remove(GetType().Name + "S3");
            Session.Remove(GetType().Name + "S4");
            Session.Remove(GetType().Name + "S5");
        }

        private void initialPage()
        {

            B1 = new Double[5];
            B2 = new Double[5];
            B3 = new Double[5];
            B4 = new Double[5];
            B5 = new Double[5];
            S1 = new Double[5];
            S2 = new Double[5];
            S3 = new Double[5];
            S4 = new Double[5];
            S5 = new Double[5];

            #region "WorkSheet"
            this.Lpc = new template_wd_lpc_coverpage().SelectBySampleID(this.SampleID);
            tb_component_lpc comp = new tb_component_lpc().SelectByID(Convert.ToInt32(this.Lpc.component_id));
            if (comp != null)
            {
                txtB49.Text = comp.F;//Extraction Vol, ml
                txtB50.Text = "10";//Sample Vol, ml
                txtB51.Text = comp.D;//No. of Parts
            }

            if (this.Lpc != null)
            {
                #region "Test Method: 92-004230 Rev. AK"
                txtB48.Text = this.Lpc.ws_b15;//Surface Area, cm²
                #endregion
                #region "Tank Conditions"
                lbB54.Text = this.Lpc.ws_b21;
                lbC54.Text = this.Lpc.ws_c21;
                lbD54.Text = this.Lpc.ws_d21;
                #endregion
                #region "RUN 1"
                lbC57.Text = this.Lpc.ws_c24;
                lbC58.Text = this.Lpc.ws_c25;
                lbC59.Text = this.Lpc.ws_c26;
                lbC60.Text = this.Lpc.ws_c27;
                lbC61.Text = this.Lpc.ws_c28;

                lbD57.Text = this.Lpc.ws_d24;
                lbD58.Text = this.Lpc.ws_d25;
                lbD59.Text = this.Lpc.ws_d26;
                lbD60.Text = this.Lpc.ws_d27;
                lbD61.Text = this.Lpc.ws_d28;

                lbE57.Text = this.Lpc.ws_e24;
                lbE58.Text = this.Lpc.ws_e25;
                lbE59.Text = this.Lpc.ws_e26;
                lbE60.Text = this.Lpc.ws_e27;
                lbE61.Text = this.Lpc.ws_e28;

                lbF57.Text = this.Lpc.ws_f24;
                lbF58.Text = this.Lpc.ws_f25;
                lbF59.Text = this.Lpc.ws_f26;
                lbF60.Text = this.Lpc.ws_f27;
                lbF61.Text = this.Lpc.ws_f28;
                #endregion
                #region "RUN 2"
                lbC63.Text = this.Lpc.ws_c30;
                lbC64.Text = this.Lpc.ws_c31;
                lbC65.Text = this.Lpc.ws_c32;
                lbC66.Text = this.Lpc.ws_c33;
                lbC67.Text = this.Lpc.ws_c34;

                lbD63.Text = this.Lpc.ws_d30;
                lbD64.Text = this.Lpc.ws_d31;
                lbD65.Text = this.Lpc.ws_d32;
                lbD66.Text = this.Lpc.ws_d33;
                lbD67.Text = this.Lpc.ws_d34;

                lbE63.Text = this.Lpc.ws_e30;
                lbE64.Text = this.Lpc.ws_e31;
                lbE65.Text = this.Lpc.ws_e32;
                lbE66.Text = this.Lpc.ws_e33;
                lbE67.Text = this.Lpc.ws_e34;

                lbF63.Text = this.Lpc.ws_f30;
                lbF64.Text = this.Lpc.ws_f31;
                lbF65.Text = this.Lpc.ws_f32;
                lbF66.Text = this.Lpc.ws_f33;
                lbF67.Text = this.Lpc.ws_f34;
                #endregion
                #region "RUN 3"
                lbC69.Text = this.Lpc.ws_c36;
                lbC70.Text = this.Lpc.ws_c37;
                lbC71.Text = this.Lpc.ws_c38;
                lbC72.Text = this.Lpc.ws_c39;
                lbC73.Text = this.Lpc.ws_c40;

                lbD69.Text = this.Lpc.ws_d36;
                lbD70.Text = this.Lpc.ws_d37;
                lbD71.Text = this.Lpc.ws_d38;
                lbD72.Text = this.Lpc.ws_d39;
                lbD73.Text = this.Lpc.ws_d40;

                lbE69.Text = this.Lpc.ws_e36;
                lbE70.Text = this.Lpc.ws_e37;
                lbE71.Text = this.Lpc.ws_e38;
                lbE72.Text = this.Lpc.ws_e39;
                lbE73.Text = this.Lpc.ws_e40;

                lbF69.Text = this.Lpc.ws_f36;
                lbF70.Text = this.Lpc.ws_f37;
                lbF71.Text = this.Lpc.ws_f38;
                lbF72.Text = this.Lpc.ws_f39;
                lbF73.Text = this.Lpc.ws_f40;
                #endregion
                #region "RUN 4"
                lbC81.Text = this.Lpc.ws_c42;
                lbC82.Text = this.Lpc.ws_c43;
                lbC83.Text = this.Lpc.ws_c44;
                lbC84.Text = this.Lpc.ws_c45;
                lbC85.Text = this.Lpc.ws_c46;

                lbD81.Text = this.Lpc.ws_d42;
                lbD82.Text = this.Lpc.ws_d43;
                lbD83.Text = this.Lpc.ws_d44;
                lbD84.Text = this.Lpc.ws_d45;
                lbD85.Text = this.Lpc.ws_d46;

                lbE81.Text = this.Lpc.ws_e42;
                lbE82.Text = this.Lpc.ws_e43;
                lbE83.Text = this.Lpc.ws_e44;
                lbE84.Text = this.Lpc.ws_e45;
                lbE85.Text = this.Lpc.ws_e46;

                lbF81.Text = this.Lpc.ws_f42;
                lbF82.Text = this.Lpc.ws_f43;
                lbF83.Text = this.Lpc.ws_f44;
                lbF84.Text = this.Lpc.ws_f45;
                lbF85.Text = this.Lpc.ws_f46;
                #endregion
                #region "RUN 5"
                lbC87.Text = this.Lpc.ws_c48;
                lbC88.Text = this.Lpc.ws_c49;
                lbC89.Text = this.Lpc.ws_c50;
                lbC90.Text = this.Lpc.ws_c51;
                lbC91.Text = this.Lpc.ws_c52;

                lbD87.Text = this.Lpc.ws_d48;
                lbD88.Text = this.Lpc.ws_d49;
                lbD89.Text = this.Lpc.ws_d50;
                lbD90.Text = this.Lpc.ws_d51;
                lbD91.Text = this.Lpc.ws_d52;

                lbE87.Text = this.Lpc.ws_e48;
                lbE88.Text = this.Lpc.ws_e49;
                lbE89.Text = this.Lpc.ws_e50;
                lbE90.Text = this.Lpc.ws_e51;
                lbE91.Text = this.Lpc.ws_e52;

                lbF87.Text = this.Lpc.ws_f48;
                lbF88.Text = this.Lpc.ws_f49;
                lbF89.Text = this.Lpc.ws_f50;
                lbF90.Text = this.Lpc.ws_f51;
                lbF91.Text = this.Lpc.ws_f52;
                #endregion
                #region "Average"
                lbC94.Text = this.Lpc.ws_e55;
                lbC95.Text = this.Lpc.ws_e56;
                lbC96.Text = this.Lpc.ws_e57;
                lbC97.Text = this.Lpc.ws_e58;
                lbC98.Text = this.Lpc.ws_e59;

                lbD94.Text = this.Lpc.ws_f55;
                lbD95.Text = this.Lpc.ws_f56;
                lbD96.Text = this.Lpc.ws_f57;
                lbD97.Text = this.Lpc.ws_f58;
                lbD98.Text = this.Lpc.ws_f59;
                #endregion
                #region "Standard Deviation"
                lbC100.Text = this.Lpc.ws_e61;
                lbC101.Text = this.Lpc.ws_e62;
                lbC102.Text = this.Lpc.ws_e63;
                lbC103.Text = this.Lpc.ws_e64;
                lbC104.Text = this.Lpc.ws_e65;

                lbD100.Text = this.Lpc.ws_f61;
                lbD101.Text = this.Lpc.ws_f62;
                lbD102.Text = this.Lpc.ws_f63;
                lbD103.Text = this.Lpc.ws_f64;
                lbD104.Text = this.Lpc.ws_f65;
                #endregion
                #region "%RSD Deviation"
                lbC106.Text = this.Lpc.ws_e67;
                lbC107.Text = this.Lpc.ws_e68;
                lbC108.Text = this.Lpc.ws_e69;
                lbC109.Text = this.Lpc.ws_e70;
                lbC110.Text = this.Lpc.ws_e71;

                lbD106.Text = this.Lpc.ws_f67;
                lbD107.Text = this.Lpc.ws_f68;
                lbD108.Text = this.Lpc.ws_f69;
                lbD109.Text = this.Lpc.ws_f70;
                lbD110.Text = this.Lpc.ws_f71;
                #endregion

                B1[0] = Convert.ToDouble(this.Lpc.ws_c24);
                B1[1] = Convert.ToDouble(this.Lpc.ws_c25);
                B1[2] = Convert.ToDouble(this.Lpc.ws_c26);
                B1[3] = Convert.ToDouble(this.Lpc.ws_c27);
                B1[4] = Convert.ToDouble(this.Lpc.ws_c28);

                S1[0] = Convert.ToDouble(this.Lpc.ws_d24);
                S1[1] = Convert.ToDouble(this.Lpc.ws_d25);
                S1[2] = Convert.ToDouble(this.Lpc.ws_d26);
                S1[3] = Convert.ToDouble(this.Lpc.ws_d27);
                S1[4] = Convert.ToDouble(this.Lpc.ws_d28);


                B2[0] = Convert.ToDouble(this.Lpc.ws_c30);
                B2[1] = Convert.ToDouble(this.Lpc.ws_c31);
                B2[2] = Convert.ToDouble(this.Lpc.ws_c32);
                B2[3] = Convert.ToDouble(this.Lpc.ws_c33);
                B2[4] = Convert.ToDouble(this.Lpc.ws_c34);

                S2[0] = Convert.ToDouble(this.Lpc.ws_d30);
                S2[1] = Convert.ToDouble(this.Lpc.ws_d31);
                S2[2] = Convert.ToDouble(this.Lpc.ws_d32);
                S2[3] = Convert.ToDouble(this.Lpc.ws_d33);
                S2[4] = Convert.ToDouble(this.Lpc.ws_d34);


                B3[0] = Convert.ToDouble(this.Lpc.ws_c36);
                B3[1] = Convert.ToDouble(this.Lpc.ws_c37);
                B3[2] = Convert.ToDouble(this.Lpc.ws_c38);
                B3[3] = Convert.ToDouble(this.Lpc.ws_c39);
                B3[4] = Convert.ToDouble(this.Lpc.ws_c40);

                S3[0] = Convert.ToDouble(this.Lpc.ws_d36);
                S3[1] = Convert.ToDouble(this.Lpc.ws_d37);
                S3[2] = Convert.ToDouble(this.Lpc.ws_d38);
                S3[3] = Convert.ToDouble(this.Lpc.ws_d39);
                S3[4] = Convert.ToDouble(this.Lpc.ws_d40);


                B4[0] = Convert.ToDouble(this.Lpc.ws_c42);
                B4[1] = Convert.ToDouble(this.Lpc.ws_c43);
                B4[2] = Convert.ToDouble(this.Lpc.ws_c44);
                B4[3] = Convert.ToDouble(this.Lpc.ws_c45);
                B4[4] = Convert.ToDouble(this.Lpc.ws_c46);

                S4[0] = Convert.ToDouble(this.Lpc.ws_d42);
                S4[1] = Convert.ToDouble(this.Lpc.ws_d43);
                S4[2] = Convert.ToDouble(this.Lpc.ws_d44);
                S4[3] = Convert.ToDouble(this.Lpc.ws_d45);
                S4[4] = Convert.ToDouble(this.Lpc.ws_d46);


                B5[0] = Convert.ToDouble(this.Lpc.ws_c48);
                B5[1] = Convert.ToDouble(this.Lpc.ws_c49);
                B5[2] = Convert.ToDouble(this.Lpc.ws_c50);
                B5[3] = Convert.ToDouble(this.Lpc.ws_c51);
                B5[4] = Convert.ToDouble(this.Lpc.ws_c52);

                S5[0] = Convert.ToDouble(this.Lpc.ws_d48);
                S5[1] = Convert.ToDouble(this.Lpc.ws_d49);
                S5[2] = Convert.ToDouble(this.Lpc.ws_d50);
                S5[3] = Convert.ToDouble(this.Lpc.ws_d51);
                S5[4] = Convert.ToDouble(this.Lpc.ws_d52);

            }
            else
            {
                #region "Tank Conditions"
                lbB54.Text = "20.2 L";
                lbC54.Text = "68 kHz";
                lbD54.Text = "4.8 W/L";
                #endregion
            }

            #endregion

            CalculateCas();
            //initial component
            btnSubmit.Enabled = false;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            job_sample sample = new job_sample().SelectByID(this.SampleID);
            sample.job_status = Convert.ToInt32(StatusEnum.SR_CHEMIST_CHECKING);
            sample.Update();

            #region "Test Method: 92-004230 Rev. AK"
            this.Lpc.ws_b15 = txtB48.Text;
            this.Lpc.ws_b16 = txtB49.Text;
            this.Lpc.ws_b17 = txtB50.Text;
            this.Lpc.ws_b18 = txtB51.Text;
            #endregion
            #region "Tank Conditions"
            this.Lpc.ws_b21 = lbB54.Text;
            this.Lpc.ws_c21 = lbC54.Text;
            this.Lpc.ws_d21 = lbD54.Text;
            #endregion
            #region "RUN 1"
            this.Lpc.ws_c24 = lbC57.Text;
            this.Lpc.ws_c25 = lbC58.Text;
            this.Lpc.ws_c26 = lbC59.Text;
            this.Lpc.ws_c27 = lbC60.Text;
            this.Lpc.ws_c28 = lbC61.Text;

            this.Lpc.ws_d24 = lbD57.Text;
            this.Lpc.ws_d25 = lbD58.Text;
            this.Lpc.ws_d26 = lbD59.Text;
            this.Lpc.ws_d27 = lbD60.Text;
            this.Lpc.ws_d28 = lbD61.Text;

            this.Lpc.ws_e24 = lbE57.Text;
            this.Lpc.ws_e25 = lbE58.Text;
            this.Lpc.ws_e26 = lbE59.Text;
            this.Lpc.ws_e27 = lbE60.Text;
            this.Lpc.ws_e28 = lbE61.Text;

            this.Lpc.ws_f24 = lbF57.Text;
            this.Lpc.ws_f25 = lbF58.Text;
            this.Lpc.ws_f26 = lbF59.Text;
            this.Lpc.ws_f27 = lbF60.Text;
            this.Lpc.ws_f28 = lbF61.Text;
            #endregion
            #region "RUN 2"
            this.Lpc.ws_c30 = lbC63.Text;
            this.Lpc.ws_c31 = lbC64.Text;
            this.Lpc.ws_c32 = lbC65.Text;
            this.Lpc.ws_c33 = lbC66.Text;
            this.Lpc.ws_c34 = lbC67.Text;

            this.Lpc.ws_d30 = lbD63.Text;
            this.Lpc.ws_d31 = lbD64.Text;
            this.Lpc.ws_d32 = lbD65.Text;
            this.Lpc.ws_d33 = lbD66.Text;
            this.Lpc.ws_d34 = lbD67.Text;

            this.Lpc.ws_e30 = lbE63.Text;
            this.Lpc.ws_e31 = lbE64.Text;
            this.Lpc.ws_e32 = lbE65.Text;
            this.Lpc.ws_e33 = lbE66.Text;
            this.Lpc.ws_e34 = lbE67.Text;

            this.Lpc.ws_f30 = lbF63.Text;
            this.Lpc.ws_f31 = lbF64.Text;
            this.Lpc.ws_f32 = lbF65.Text;
            this.Lpc.ws_f33 = lbF66.Text;
            this.Lpc.ws_f34 = lbF67.Text;
            #endregion
            #region "RUN 3"
            this.Lpc.ws_c36 = lbC69.Text;
            this.Lpc.ws_c37 = lbC70.Text;
            this.Lpc.ws_c38 = lbC71.Text;
            this.Lpc.ws_c39 = lbC72.Text;
            this.Lpc.ws_c40 = lbC73.Text;

            this.Lpc.ws_d36 = lbD69.Text;
            this.Lpc.ws_d37 = lbD70.Text;
            this.Lpc.ws_d38 = lbD71.Text;
            this.Lpc.ws_d39 = lbD72.Text;
            this.Lpc.ws_d40 = lbD73.Text;

            this.Lpc.ws_e36 = lbE69.Text;
            this.Lpc.ws_e37 = lbE70.Text;
            this.Lpc.ws_e38 = lbE71.Text;
            this.Lpc.ws_e39 = lbE72.Text;
            this.Lpc.ws_e40 = lbE73.Text;

            this.Lpc.ws_f36 = lbF69.Text;
            this.Lpc.ws_f37 = lbF70.Text;
            this.Lpc.ws_f38 = lbF71.Text;
            this.Lpc.ws_f39 = lbF72.Text;
            this.Lpc.ws_f40 = lbF73.Text;
            #endregion
            #region "RUN 4"
            this.Lpc.ws_c42 = lbC81.Text;
            this.Lpc.ws_c43 = lbC82.Text;
            this.Lpc.ws_c44 = lbC83.Text;
            this.Lpc.ws_c45 = lbC84.Text;
            this.Lpc.ws_c46 = lbC85.Text;

            this.Lpc.ws_d42 = lbD81.Text;
            this.Lpc.ws_d43 = lbD82.Text;
            this.Lpc.ws_d44 = lbD83.Text;
            this.Lpc.ws_d45 = lbD84.Text;
            this.Lpc.ws_d46 = lbD85.Text;

            this.Lpc.ws_e42 = lbE81.Text;
            this.Lpc.ws_e43 = lbE82.Text;
            this.Lpc.ws_e44 = lbE83.Text;
            this.Lpc.ws_e45 = lbE84.Text;
            this.Lpc.ws_e46 = lbE85.Text;

            this.Lpc.ws_f42 = lbF81.Text;
            this.Lpc.ws_f43 = lbF82.Text;
            this.Lpc.ws_f44 = lbF83.Text;
            this.Lpc.ws_f45 = lbF84.Text;
            this.Lpc.ws_f46 = lbF85.Text;
            #endregion
            #region "RUN 5"
            this.Lpc.ws_c48 = lbC87.Text;
            this.Lpc.ws_c49 = lbC88.Text;
            this.Lpc.ws_c50 = lbC89.Text;
            this.Lpc.ws_c51 = lbC90.Text;
            this.Lpc.ws_c52 = lbC91.Text;

            this.Lpc.ws_d48 = lbD87.Text;
            this.Lpc.ws_d49 = lbD88.Text;
            this.Lpc.ws_d50 = lbD89.Text;
            this.Lpc.ws_d51 = lbD90.Text;
            this.Lpc.ws_d52 = lbD91.Text;

            this.Lpc.ws_e48 = lbE87.Text;
            this.Lpc.ws_e49 = lbE88.Text;
            this.Lpc.ws_e50 = lbE89.Text;
            this.Lpc.ws_e51 = lbE90.Text;
            this.Lpc.ws_e52 = lbE91.Text;

            this.Lpc.ws_f48 = lbF87.Text;
            this.Lpc.ws_f49 = lbF88.Text;
            this.Lpc.ws_f50 = lbF89.Text;
            this.Lpc.ws_f51 = lbF90.Text;
            this.Lpc.ws_f52 = lbF91.Text;
            #endregion
            #region "Average"
            this.Lpc.ws_e55 = lbC94.Text;
            this.Lpc.ws_e56 = lbC95.Text;
            this.Lpc.ws_e57 = lbC96.Text;
            this.Lpc.ws_e58 = lbC97.Text;
            this.Lpc.ws_e59 = lbC98.Text;

            this.Lpc.ws_f55 = lbD94.Text;
            this.Lpc.ws_f56 = lbD95.Text;
            this.Lpc.ws_f57 = lbD96.Text;
            this.Lpc.ws_f58 = lbD97.Text;
            this.Lpc.ws_f59 = lbD98.Text;
            #endregion
            #region "Standard Deviation"
            this.Lpc.ws_e61 = lbC100.Text;
            this.Lpc.ws_e62 = lbC101.Text;
            this.Lpc.ws_e63 = lbC102.Text;
            this.Lpc.ws_e64 = lbC103.Text;
            this.Lpc.ws_e65 = lbC104.Text;

            this.Lpc.ws_f61 = lbD100.Text;
            this.Lpc.ws_f62 = lbD101.Text;
            this.Lpc.ws_f63 = lbD102.Text;
            this.Lpc.ws_f64 = lbD103.Text;
            this.Lpc.ws_f65 = lbD104.Text;
            #endregion
            #region "%RSD Deviation"
            this.Lpc.ws_e67 = lbC106.Text;
            this.Lpc.ws_e68 = lbC107.Text;
            this.Lpc.ws_e69 = lbC108.Text;
            this.Lpc.ws_e70 = lbC109.Text;
            this.Lpc.ws_e71 = lbC110.Text;

            this.Lpc.ws_f67 = lbD106.Text;
            this.Lpc.ws_f68 = lbD107.Text;
            this.Lpc.ws_f69 = lbD108.Text;
            this.Lpc.ws_f70 = lbD109.Text;
            this.Lpc.ws_f71 = lbD110.Text;
            #endregion

            this.Lpc.Update();
            //Commit
            GeneralManager.Commit();

            Response.Redirect(this.PreviousPath);
        }
        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            CalculateCas();
            btnSubmit.Enabled = true;
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void btnLoadFile_Click(object sender, EventArgs e)
        {
            String error = validateDSHFile(btnUpload.PostedFiles);

            if (String.IsNullOrEmpty(error))
            {
                #region "GET VALUE FROM XLS"
                String yyyMMdd = DateTime.Now.ToString("yyyyMMdd");

                for (int i = 0; i < btnUpload.PostedFiles.Count; i++)
                {
                    HttpPostedFile _postedFile = btnUpload.PostedFiles[i];
                    try
                    {
                        if (_postedFile.ContentLength > 0)
                        {
                            String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                            String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, _postedFile.FileName);
                            if (!Directory.Exists(sourceFileFolder))
                            {
                                Directory.CreateDirectory(sourceFileFolder);
                            }

                            _postedFile.SaveAs(savefilePath);


                            using (FileStream fs = new FileStream(savefilePath, FileMode.Open, FileAccess.Read))
                            {
                                HSSFWorkbook wd = new HSSFWorkbook(fs);
                                ISheet sheetSeagateIc = wd.GetSheet("Sheet1");

                                if (sheetSeagateIc != null)
                                {
                                    for (int row = 87; row <= sheetSeagateIc.LastRowNum; row++)
                                    {
                                        if (sheetSeagateIc.GetRow(row) != null) //null is when the row only contains empty cells 
                                        {
                                            String CS = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(6));
                                            String CD = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(16));


                                            switch (Path.GetFileNameWithoutExtension(savefilePath))
                                            {
                                                case "B1":
                                                    switch (CS)
                                                    {
                                                        case "0.300":
                                                            B1[0] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.500":
                                                            B1[1] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.700":
                                                            B1[2] = Convert.ToDouble(CD);
                                                            break;
                                                        case "1.000":
                                                            B1[3] = Convert.ToDouble(CD);
                                                            break;
                                                        case "2.000":
                                                            B1[4] = Convert.ToDouble(CD);
                                                            break;
                                                    }
                                                    break;
                                                case "B2":
                                                    switch (CS)
                                                    {
                                                        case "0.300":
                                                            B2[0] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.500":
                                                            B2[1] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.700":
                                                            B2[2] = Convert.ToDouble(CD);
                                                            break;
                                                        case "1.000":
                                                            B2[3] = Convert.ToDouble(CD);
                                                            break;
                                                        case "2.000":
                                                            B2[4] = Convert.ToDouble(CD);
                                                            break;
                                                    }
                                                    break;
                                                case "B3":
                                                    switch (CS)
                                                    {
                                                        case "0.300":
                                                            B3[0] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.500":
                                                            B3[1] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.700":
                                                            B3[2] = Convert.ToDouble(CD);
                                                            break;
                                                        case "1.000":
                                                            B3[3] = Convert.ToDouble(CD);
                                                            break;
                                                        case "2.000":
                                                            B3[4] = Convert.ToDouble(CD);
                                                            break;
                                                    }
                                                    break;
                                                case "B4":
                                                    switch (CS)
                                                    {
                                                        case "0.300":
                                                            B4[0] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.500":
                                                            B4[1] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.700":
                                                            B4[2] = Convert.ToDouble(CD);
                                                            break;
                                                        case "1.000":
                                                            B4[3] = Convert.ToDouble(CD);
                                                            break;
                                                        case "2.000":
                                                            B4[4] = Convert.ToDouble(CD);
                                                            break;
                                                    }
                                                    break;
                                                case "B5":
                                                    switch (CS)
                                                    {
                                                        case "0.300":
                                                            B5[0] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.500":
                                                            B5[1] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.700":
                                                            B5[2] = Convert.ToDouble(CD);
                                                            break;
                                                        case "1.000":
                                                            B5[3] = Convert.ToDouble(CD);
                                                            break;
                                                        case "2.000":
                                                            B5[4] = Convert.ToDouble(CD);
                                                            break;
                                                    }
                                                    break;
                                                case "S1":
                                                    switch (CS)
                                                    {
                                                        case "0.300":
                                                            S1[0] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.500":
                                                            S1[1] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.700":
                                                            S1[2] = Convert.ToDouble(CD);
                                                            break;
                                                        case "1.000":
                                                            S1[3] = Convert.ToDouble(CD);
                                                            break;
                                                        case "2.000":
                                                            S1[4] = Convert.ToDouble(CD);
                                                            break;
                                                    }
                                                    break;
                                                case "S2":
                                                    switch (CS)
                                                    {
                                                        case "0.300":
                                                            S2[0] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.500":
                                                            S2[1] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.700":
                                                            S2[2] = Convert.ToDouble(CD);
                                                            break;
                                                        case "1.000":
                                                            S2[3] = Convert.ToDouble(CD);
                                                            break;
                                                        case "2.000":
                                                            S2[4] = Convert.ToDouble(CD);
                                                            break;
                                                    }
                                                    break;
                                                case "S3":
                                                    switch (CS)
                                                    {
                                                        case "0.300":
                                                            S3[0] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.500":
                                                            S3[1] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.700":
                                                            S3[2] = Convert.ToDouble(CD);
                                                            break;
                                                        case "1.000":
                                                            S3[3] = Convert.ToDouble(CD);
                                                            break;
                                                        case "2.000":
                                                            S3[4] = Convert.ToDouble(CD);
                                                            break;
                                                    }
                                                    break;
                                                case "S4":
                                                    switch (CS)
                                                    {
                                                        case "0.300":
                                                            S4[0] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.500":
                                                            S4[1] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.700":
                                                            S4[2] = Convert.ToDouble(CD);
                                                            break;
                                                        case "1.000":
                                                            S4[3] = Convert.ToDouble(CD);
                                                            break;
                                                        case "2.000":
                                                            S4[4] = Convert.ToDouble(CD);
                                                            break;
                                                    }
                                                    break;
                                                case "S5":
                                                    switch (CS)
                                                    {
                                                        case "0.300":
                                                            S5[0] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.500":
                                                            S5[1] = Convert.ToDouble(CD);
                                                            break;
                                                        case "0.700":
                                                            S5[2] = Convert.ToDouble(CD);
                                                            break;
                                                        case "1.000":
                                                            S5[3] = Convert.ToDouble(CD);
                                                            break;
                                                        case "2.000":
                                                            S5[4] = Convert.ToDouble(CD);
                                                            break;
                                                    }
                                                    break;
                                            }


                                        }
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception Ex)
                    {
                        logger.Error(Ex.Message);
                        Console.WriteLine();
                    }
                }
                #endregion

                #region "SET VALUE FROM XLS"
                #region "RUN 1"
                lbC57.Text = String.Format("{0:n2}", B1[0]);
                lbC58.Text = String.Format("{0:n2}", B1[1]);
                lbC59.Text = String.Format("{0:n2}", B1[2]);
                lbC60.Text = String.Format("{0:n2}", B1[3]);
                lbC61.Text = String.Format("{0:n2}", B1[4]);

                lbD57.Text = String.Format("{0:n2}", S1[0]);
                lbD58.Text = String.Format("{0:n2}", S1[1]);
                lbD59.Text = String.Format("{0:n2}", S1[2]);
                lbD60.Text = String.Format("{0:n2}", S1[3]);
                lbD61.Text = String.Format("{0:n2}", S1[4]);
                #endregion
                #region "RUN 2"
                lbC63.Text = String.Format("{0:n2}", B2[0]);
                lbC64.Text = String.Format("{0:n2}", B2[1]);
                lbC65.Text = String.Format("{0:n2}", B2[2]);
                lbC66.Text = String.Format("{0:n2}", B2[3]);
                lbC67.Text = String.Format("{0:n2}", B2[4]);

                lbD63.Text = String.Format("{0:n2}", S2[0]);
                lbD64.Text = String.Format("{0:n2}", S2[1]);
                lbD65.Text = String.Format("{0:n2}", S2[2]);
                lbD66.Text = String.Format("{0:n2}", S2[3]);
                lbD67.Text = String.Format("{0:n2}", S2[4]);
                #endregion
                #region "RUN 3"
                lbC69.Text = String.Format("{0:n2}", B3[0]);
                lbC70.Text = String.Format("{0:n2}", B3[1]);
                lbC71.Text = String.Format("{0:n2}", B3[2]);
                lbC72.Text = String.Format("{0:n2}", B3[3]);
                lbC73.Text = String.Format("{0:n2}", B3[4]);

                lbD69.Text = String.Format("{0:n2}", S3[0]);
                lbD70.Text = String.Format("{0:n2}", S3[1]);
                lbD71.Text = String.Format("{0:n2}", S3[2]);
                lbD72.Text = String.Format("{0:n2}", S3[3]);
                lbD73.Text = String.Format("{0:n2}", S3[4]);
                #endregion
                #region "RUN 4"
                lbC81.Text = String.Format("{0:n2}", B4[0]);
                lbC82.Text = String.Format("{0:n2}", B4[1]);
                lbC83.Text = String.Format("{0:n2}", B4[2]);
                lbC84.Text = String.Format("{0:n2}", B4[3]);
                lbC85.Text = String.Format("{0:n2}", B4[4]);

                lbD81.Text = String.Format("{0:n2}", S4[0]);
                lbD82.Text = String.Format("{0:n2}", S4[1]);
                lbD83.Text = String.Format("{0:n2}", S4[2]);
                lbD84.Text = String.Format("{0:n2}", S4[3]);
                lbD85.Text = String.Format("{0:n2}", S4[4]);
                #endregion
                #region "RUN 5"
                lbC87.Text = String.Format("{0:n2}", B5[0]);
                lbC88.Text = String.Format("{0:n2}", B5[1]);
                lbC89.Text = String.Format("{0:n2}", B5[2]);
                lbC90.Text = String.Format("{0:n2}", B5[3]);
                lbC91.Text = String.Format("{0:n2}", B5[4]);

                lbD87.Text = String.Format("{0:n2}", S5[0]);
                lbD88.Text = String.Format("{0:n2}", S5[1]);
                lbD89.Text = String.Format("{0:n2}", S5[2]);
                lbD90.Text = String.Format("{0:n2}", S5[3]);
                lbD91.Text = String.Format("{0:n2}", S5[4]);
                #endregion
                #endregion

                CalculateCas();
                btnSubmit.Enabled = false;
                Console.WriteLine("-END-");
            }
            else
            {
                lbMessage.Text = error;
                lbMessage.Attributes["class"] = "alert alert-error";
            }

        }

        #region "Custom method"

        private String validateDSHFile(IList<HttpPostedFile> _files)
        {
            Boolean isFound_b1 = false;
            Boolean isFound_b2 = false;
            Boolean isFound_b3 = false;
            Boolean isFound_b4 = false;
            Boolean isFound_b5 = false;

            Boolean isFound_s1 = false;
            Boolean isFound_s2 = false;
            Boolean isFound_s3 = false;
            Boolean isFound_s4 = false;
            Boolean isFound_s5 = false;
            Boolean isFoundWrongExtension = false;

            String result = String.Empty;

            String[] files = new String[_files.Count];
            if (files.Length == 10)
            {
                for (int i = 0; i < _files.Count; i++)
                {
                    files[i] = _files[i].FileName;
                    if (!Path.GetExtension(_files[i].FileName).Trim().Equals(".xls"))
                    {
                        isFoundWrongExtension = true;
                        break;
                    }
                }
                if (!isFoundWrongExtension)
                {

                    //Find B1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B1"))
                        {
                            isFound_b1 = true;
                            break;
                        }
                    }

                    //Find B2
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B2"))
                        {
                            isFound_b2 = true;
                            break;
                        }
                    }
                    //Find B3
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B3"))
                        {
                            isFound_b3 = true;
                            break;
                        }
                    }
                    //Find B4
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B4"))
                        {
                            isFound_b4 = true;
                            break;
                        }
                    }
                    //Find B5
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B5"))
                        {
                            isFound_b5 = true;
                            break;
                        }
                    }
                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S1"))
                        {
                            isFound_s1 = true;
                            break;
                        }
                    }

                    //Find S2
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S2"))
                        {
                            isFound_s2 = true;
                            break;
                        }
                    }
                    //Find S3
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S3"))
                        {
                            isFound_s3 = true;
                            break;
                        }
                    }
                    //Find S4
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S4"))
                        {
                            isFound_s4 = true;
                            break;
                        }
                    }
                    //Find S5
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S5"))
                        {
                            isFound_s5 = true;
                            break;
                        }
                    }
                    result = (!isFound_b1) ? result += "File not found B1.xls" :
                                (!isFound_b2) ? result += "File not found B2.xls" :
                                (!isFound_b3) ? result += "File not found B3.xls" :
                                 (!isFound_b4) ? result += "File not found B3.xls" :
                                  (!isFound_b5) ? result += "File not found B3.xls" :
                                (!isFound_s1) ? result += "File not found S1.xls" :
                                (!isFound_s2) ? result += "File not found S2.xls" :
                                (!isFound_s3) ? result += "File not found S3.xls" :
                                 (!isFound_s4) ? result += "File not found S3.xls" :
                                  (!isFound_s5) ? result += "File not found S3.xls" : String.Empty;
                }
                else
                {
                    result = "File extension must be *.txt";
                }
            }
            else
            {
                result = "You must to select 6 files for upload.";
            }
            return result;
        }

        private void CalculateCas()
        {
            if (!String.IsNullOrEmpty(txtB48.Text) &&
                !String.IsNullOrEmpty(txtB51.Text))
            {
                Double mul = Convert.ToDouble(txtB49.Text) / Convert.ToDouble(txtB51.Text);
                Double div = Convert.ToDouble(txtB48.Text);
                #region "RUN 1"
                Double E57 = (CustomUtils.GetMax(S1[0] - B1[0]) * mul);
                Double E58 = (CustomUtils.GetMax(S1[1] - B1[1]) * mul);
                Double E59 = (CustomUtils.GetMax(S1[2] - B1[2]) * mul);
                Double E60 = (CustomUtils.GetMax(S1[3] - B1[3]) * mul);
                Double E61 = (CustomUtils.GetMax(S1[4] - B1[4]) * mul);
                lbE57.Text = String.Format("{0:n0}", E57);
                lbE58.Text = String.Format("{0:n0}", E58);
                lbE59.Text = String.Format("{0:n0}", E59);
                lbE60.Text = String.Format("{0:n0}", E60);
                lbE61.Text = String.Format("{0:n0}", E61);
                Double F57 = (E57 / div);
                Double F58 = (E58 / div);
                Double F59 = (E59 / div);
                Double F60 = (E60 / div);
                Double F61 = (E61 / div);
                lbF57.Text = String.Format("{0:n0}", F57);
                lbF58.Text = String.Format("{0:n0}", F58);
                lbF59.Text = String.Format("{0:n0}", F59);
                lbF60.Text = String.Format("{0:n0}", F60);
                lbF61.Text = String.Format("{0:n0}", F61);
                #endregion
                #region "RUN 2"
                Double E63 = (CustomUtils.GetMax(S2[0] - B2[0]) * mul);
                Double E64 = (CustomUtils.GetMax(S2[1] - B2[1]) * mul);
                Double E65 = (CustomUtils.GetMax(S2[2] - B2[2]) * mul);
                Double E66 = (CustomUtils.GetMax(S2[3] - B2[3]) * mul);
                Double E67 = (CustomUtils.GetMax(S2[4] - B2[4]) * mul);
                lbE63.Text = String.Format("{0:n0}", E63);
                lbE64.Text = String.Format("{0:n0}", E64);
                lbE65.Text = String.Format("{0:n0}", E65);
                lbE66.Text = String.Format("{0:n0}", E66);
                lbE67.Text = String.Format("{0:n0}", E67);
                Double F63 = (E63 / div);
                Double F64 = (E64 / div);
                Double F65 = (E65 / div);
                Double F66 = (E66 / div);
                Double F67 = (E67 / div);
                lbF63.Text = String.Format("{0:n0}", F63);
                lbF64.Text = String.Format("{0:n0}", F64);
                lbF65.Text = String.Format("{0:n0}", F65);
                lbF66.Text = String.Format("{0:n0}", F66);
                lbF67.Text = String.Format("{0:n0}", F67);
                #endregion
                #region "RUN 3"
                Double E69 = (CustomUtils.GetMax(S3[0] - B3[0]) * mul);
                Double E70 = (CustomUtils.GetMax(S3[1] - B3[1]) * mul);
                Double E71 = (CustomUtils.GetMax(S3[2] - B3[2]) * mul);
                Double E72 = (CustomUtils.GetMax(S3[3] - B3[3]) * mul);
                Double E73 = (CustomUtils.GetMax(S3[4] - B3[4]) * mul);
                lbE69.Text = String.Format("{0:n0}", E69);
                lbE70.Text = String.Format("{0:n0}", E70);
                lbE71.Text = String.Format("{0:n0}", E71);
                lbE72.Text = String.Format("{0:n0}", E72);
                lbE73.Text = String.Format("{0:n0}", E73);
                Double F69 = (E69 / div);
                Double F70 = (E70 / div);
                Double F71 = (E71 / div);
                Double F72 = (E72 / div);
                Double F73 = (E73 / div);
                lbF69.Text = String.Format("{0:n0}", F69);
                lbF70.Text = String.Format("{0:n0}", F70);
                lbF71.Text = String.Format("{0:n0}", F71);
                lbF72.Text = String.Format("{0:n0}", F72);
                lbF73.Text = String.Format("{0:n0}", F73);
                #endregion
                #region "RUN 4"
                Double E81 = (CustomUtils.GetMax(S4[0] - B4[0]) * mul);
                Double E82 = (CustomUtils.GetMax(S4[1] - B4[1]) * mul);
                Double E83 = (CustomUtils.GetMax(S4[2] - B4[2]) * mul);
                Double E84 = (CustomUtils.GetMax(S4[3] - B4[3]) * mul);
                Double E85 = (CustomUtils.GetMax(S4[4] - B4[4]) * mul);
                lbE81.Text = String.Format("{0:n0}", E81);
                lbE82.Text = String.Format("{0:n0}", E82);
                lbE83.Text = String.Format("{0:n0}", E83);
                lbE84.Text = String.Format("{0:n0}", E84);
                lbE85.Text = String.Format("{0:n0}", E85);
                Double F81 = (E81 / div);
                Double F82 = (E82 / div);
                Double F83 = (E83 / div);
                Double F84 = (E84 / div);
                Double F85 = (E85 / div);
                lbF81.Text = String.Format("{0:n0}", F81);
                lbF82.Text = String.Format("{0:n0}", F82);
                lbF83.Text = String.Format("{0:n0}", F83);
                lbF84.Text = String.Format("{0:n0}", F84);
                lbF85.Text = String.Format("{0:n0}", F85);
                #endregion
                #region "RUN 5"
                Double E87 = (CustomUtils.GetMax(S5[0] - B5[0]) * mul);
                Double E88 = (CustomUtils.GetMax(S5[1] - B5[1]) * mul);
                Double E89 = (CustomUtils.GetMax(S5[2] - B5[2]) * mul);
                Double E90 = (CustomUtils.GetMax(S5[3] - B5[3]) * mul);
                Double E91 = (CustomUtils.GetMax(S5[4] - B5[4]) * mul);
                lbE87.Text = String.Format("{0:n0}", E87);
                lbE88.Text = String.Format("{0:n0}", E88);
                lbE89.Text = String.Format("{0:n0}", E89);
                lbE90.Text = String.Format("{0:n0}", E90);
                lbE91.Text = String.Format("{0:n0}", E91);
                Double F87 = (E87 / div);
                Double F88 = (E88 / div);
                Double F89 = (E89 / div);
                Double F90 = (E90 / div);
                Double F91 = (E91 / div);
                lbF87.Text = String.Format("{0:n0}", F87);
                lbF88.Text = String.Format("{0:n0}", F88);
                lbF89.Text = String.Format("{0:n0}", F89);
                lbF90.Text = String.Format("{0:n0}", F90);
                lbF91.Text = String.Format("{0:n0}", F91);
                #endregion

                #region "SUMMARY"

                Double[] E_AS03 = { E57, E63, E69, E81, E87 };
                Double[] E_AS05 = { E58, E64, E70, E82, E88 };
                Double[] E_AS07 = { E59, E65, E71, E83, E89 };
                Double[] E_AS10 = { E60, E66, E72, E84, E90 };
                Double[] E_AS20 = { E61, E67, E73, E85, E91 };

                Double[] F_AS03 = { F57, F63, F69, F81, F87 };
                Double[] F_AS05 = { F58, F64, F70, F82, F88 };
                Double[] F_AS07 = { F59, F65, F71, F83, F89 };
                Double[] F_AS10 = { F60, F66, F72, F84, F90 };
                Double[] F_AS20 = { F61, F67, F73, F85, F91 };

                #region "Averages"
                double C94 = Math.Round(CustomUtils.Average(E_AS03));
                double C95 = Math.Round(CustomUtils.Average(E_AS05));
                double C96 = Math.Round(CustomUtils.Average(E_AS07));
                double C97 = Math.Round(CustomUtils.Average(E_AS10));
                double C98 = Math.Round(CustomUtils.Average(E_AS20));
                double D94 = Math.Round(CustomUtils.Average(F_AS03));
                double D95 = Math.Round(CustomUtils.Average(F_AS05));
                double D96 = Math.Round(CustomUtils.Average(F_AS07));
                double D97 = Math.Round(CustomUtils.Average(F_AS10));
                double D98 = Math.Round(CustomUtils.Average(F_AS20));

                lbC94.Text = String.Format("{0:n0}", C94);
                lbC95.Text = String.Format("{0:n0}", C95);
                lbC96.Text = String.Format("{0:n0}", C96);
                lbC97.Text = String.Format("{0:n0}", C97);
                lbC98.Text = String.Format("{0:n0}", C98);

                lbD94.Text = String.Format("{0:n0}", D94);
                lbD95.Text = String.Format("{0:n0}", D95);
                lbD96.Text = String.Format("{0:n0}", D96);
                lbD97.Text = String.Format("{0:n0}", D97);
                lbD98.Text = String.Format("{0:n0}", D98);
                #endregion
                #region "Standard Deviation"
                double C100 = Math.Round(CustomUtils.StandardDeviation(E_AS03));
                double C101 = Math.Round(CustomUtils.StandardDeviation(E_AS05));
                double C102 = Math.Round(CustomUtils.StandardDeviation(E_AS07));
                double C103 = Math.Round(CustomUtils.StandardDeviation(E_AS10));
                double C104 = Math.Round(CustomUtils.StandardDeviation(E_AS20));

                double D100 = Math.Round(CustomUtils.StandardDeviation(F_AS03));
                double D101 = Math.Round(CustomUtils.StandardDeviation(F_AS05));
                double D102 = Math.Round(CustomUtils.StandardDeviation(F_AS07));
                double D103 = Math.Round(CustomUtils.StandardDeviation(F_AS10));
                double D104 = Math.Round(CustomUtils.StandardDeviation(F_AS20));

                lbC100.Text = String.Format("{0:n0}", C100);
                lbC101.Text = String.Format("{0:n0}", C101);
                lbC102.Text = String.Format("{0:n0}", C102);
                lbC103.Text = String.Format("{0:n0}", C103);
                lbC104.Text = String.Format("{0:n0}", C104);

                lbD100.Text = String.Format("{0:n0}", D100);
                lbD101.Text = String.Format("{0:n0}", D101);
                lbD102.Text = String.Format("{0:n0}", D102);
                lbD103.Text = String.Format("{0:n0}", D103);
                lbD104.Text = String.Format("{0:n0}", D104);
                #endregion
                #region "%RSD Deviation"
                lbC106.Text = String.Format("{0:n0}", Math.Round(((C100 / C94) * 100)));
                lbC107.Text = String.Format("{0:n0}", Math.Round(((C101 / C95) * 100)));
                lbC108.Text = String.Format("{0:n0}", Math.Round(((C102 / C96) * 100)));
                lbC109.Text = String.Format("{0:n0}", Math.Round(((C103 / C97) * 100)));
                lbC110.Text = String.Format("{0:n0}", Math.Round(((C104 / C98) * 100)));

                lbD106.Text = String.Format("{0:n0}", Math.Round(((D100 / D94) * 100)));
                lbD107.Text = String.Format("{0:n0}", Math.Round(((D101 / D95) * 100)));
                lbD108.Text = String.Format("{0:n0}", Math.Round(((D102 / D96) * 100)));
                lbD109.Text = String.Format("{0:n0}", Math.Round(((D103 / D97) * 100)));
                lbD110.Text = String.Format("{0:n0}", Math.Round(((D104 / D98) * 100)));
                #endregion
                #endregion
            }
        }
        #endregion




    }
}