﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Seagate_LPC_Component_1_4_Rev_BE.ascx.cs" Inherits="ALS.ALSI.Web.view.template.Seagate_LPC_Component_1_4_Rev_BE" %>

<script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date-picker').datepicker();
    });
</script>
<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <!-- BEGIN COVERPAGE-TH CONTENT-->
            <div class="row-fluid invoice" id="invDiv" runat="server">
                <div>
                    CUSTOMER PO NO.:
                        <asp:Label ID="lbPoNo" runat="server" Text=""></asp:Label><br />
                    ALS THAILAND REF NO.:
                        <asp:Label ID="lbRefNo" runat="server" Text=""></asp:Label><br />
                    DATE:
                        <asp:Label ID="lbDate" runat="server" Text=""></asp:Label><br />
                    COMPANY:
                        <asp:Label ID="lbCompany" runat="server" Text=""></asp:Label><br />
                    <br />
                    DATE SAMPLE RECEIVED:
                        <asp:Label ID="lbDateSampleReceived" runat="server" Text=""></asp:Label><br />
                    DATE TEST COMPLETED:
                        <asp:Label ID="lbDateTestCompleted" runat="server" Text=""></asp:Label><br />
                    <br />
                    SAMPLE DESCRIPTION: One lot of sample was received with references:<br />
                    <asp:Label ID="lbSampleDescription" runat="server" Text=""></asp:Label>
                </div>

                <div class="row-fluid">
                    <div class="span12 ">
                        <h5>METHOD/PROCEDURE:</h5>
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Analysis</th>
                                    <th>Procedure No</th>
                                    <th>Number of pieces used<br />
                                        for extraction</th>
                                    <th>Extraction<br />
                                        Medium</th>
                                    <th>Extraction<br />
                                        Volume</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbA18" runat="server" Text=""> </asp:Label></td>
                                    <td>20800040-001  Rev.AC</td>
                                    <td>
                                        <asp:TextBox ID="txtCVP_C19" runat="server" Text="1"></asp:TextBox>
                                        <asp:Label ID="lbCVP_C19" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>0.1 um Filtered Degassed DI Water</td>
                                    <td><asp:TextBox ID="txtCVP_E19" runat="server" Text="500"></asp:TextBox>
                                        <asp:Label ID="lbCVP_E19" runat="server" Text=""></asp:Label>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span6 ">
                        <h6>Results:</h6>
                        <h6>The Specification is based on Seagate's Doc .
                            <asp:Label ID="lbDocNo" runat="server" Text=""></asp:Label>
                            <asp:Label ID="lbDocRev" runat="server" Text=""></asp:Label>for
                            <asp:Label ID="lbCommodity" runat="server" Text=""></asp:Label>
                        </h6>
                    </div>
                </div>

                <%-- Liquid Particle Count (68 KHz) 0.3 --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb1" runat="server">
                            <thead>

                                <tr>
                                    <th>Liquid Particle Count (68 KHz)</th>
                                    <th>Specification Limits<br />
                                        (<asp:Label ID="lbUnit1" runat="server" Text="" />)</th>
                                    <th>Results<br />
                                        (<asp:Label ID="lbUnit2" runat="server" Text="" />)</th>
                                    <th runat="server" id="th1">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr runat="server" id="tr1">
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Total number of particles ≥ 0.3μm"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td runat="server" id="td1">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr2">
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text="1st Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC25" runat="server" Text="" /></td>
                                    <td runat="server" id="td2">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr3">
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Text="2nd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC26" runat="server" Text="" /></td>
                                    <td runat="server" id="td3">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr4">
                                    <td>
                                        <asp:Label ID="Label18" runat="server" Text="3rd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC27" runat="server" Text="" /></td>
                                    <td runat="server" id="td4">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr5">
                                    <td>
                                        <asp:Label ID="Label21" runat="server" Text="Average"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB28" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC28" runat="server" Text="" /></td>
                                    <td runat="server" id="td5">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%-- Liquid Particle Count (68 KHz) 0.6 --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb2" runat="server">
                            <thead>

                                <tr>
                                    <th>Liquid Particle Count (68 KHz)</th>
                                    <th>Specification Limits<br />
                                        (<asp:Label ID="lbUnit3" runat="server" Text="" />)</th>
                                    <th>Results<br />
                                        (<asp:Label ID="lbUnit4" runat="server" Text="" />)</th>
                                    <th runat="server" id="th2">
                                        <asp:CheckBox ID="CheckBox2" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr runat="server" id="tr6">
                                    <td>
                                        <asp:Label ID="Label9" runat="server" Text="Total number of particles ≥ 0.6μm"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td runat="server" id="td6">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr7">
                                    <td>
                                        <asp:Label ID="Label10" runat="server" Text="1st Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC32" runat="server" Text="" /></td>
                                    <td runat="server" id="td7">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr8">
                                    <td>
                                        <asp:Label ID="Label12" runat="server" Text="2nd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC33" runat="server" Text="" /></td>
                                    <td runat="server" id="td8">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr9">
                                    <td>
                                        <asp:Label ID="Label14" runat="server" Text="3rd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC34" runat="server" Text="" /></td>
                                    <td runat="server" id="td9">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr10">
                                    <td>
                                        <asp:Label ID="Label16" runat="server" Text="Average"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB35" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC35" runat="server" Text="" /></td>
                                    <td runat="server" id="td10">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%-- Liquid Particle Count (132 KHz) 0.3 --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb3" runat="server">
                            <thead>

                                <tr>
                                    <th>Liquid Particle Count (132 KHz)</th>
                                    <th>Specification Limits<br />
                                        (<asp:Label ID="lbUnit5" runat="server" Text="" />)</th>
                                    <th>Results<br />
                                        (<asp:Label ID="lbUnit6" runat="server" Text="" />)</th>
                                    <th runat="server" id="th3">
                                        <asp:CheckBox ID="CheckBox3" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr runat="server" id="tr11">
                                    <td>
                                        <asp:Label ID="Label15" runat="server" Text="Total number of particles ≥ 0.3μm"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td runat="server" id="td11">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr12">
                                    <td>
                                        <asp:Label ID="Label17" runat="server" Text="1st Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC39" runat="server" Text="" /></td>
                                    <td runat="server" id="td12">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr13">
                                    <td>
                                        <asp:Label ID="Label20" runat="server" Text="2nd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC40" runat="server" Text="" /></td>
                                    <td runat="server" id="td13">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr14">
                                    <td>
                                        <asp:Label ID="Label23" runat="server" Text="3rd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC41" runat="server" Text="" /></td>
                                    <td runat="server" id="td14">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr15">
                                    <td>
                                        <asp:Label ID="Label25" runat="server" Text="Average"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB42" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC42" runat="server" Text="" /></td>
                                    <td runat="server" id="td15">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%-- Liquid Particle Count (132 KHz) 0.6 --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb4" runat="server">
                            <thead>

                                <tr>
                                    <th>Liquid Particle Count (132 KHz)</th>
                                    <th>Specification Limits<br />
                                        (<asp:Label ID="lbUnit7" runat="server" Text="" />)</th>
                                    <th>Results<br />
                                        (<asp:Label ID="lbUnit8" runat="server" Text="" />)</th>
                                    <th runat="server" id="th4">
                                        <asp:CheckBox ID="CheckBox4" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr runat="server" id="tr16">
                                    <td>
                                        <asp:Label ID="Label24" runat="server" Text="Total number of particles ≥ 0.6μm"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td runat="server" id="td16">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr17">
                                    <td>
                                        <asp:Label ID="Label26" runat="server" Text="1st Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC46" runat="server" Text="" /></td>
                                    <td runat="server" id="td17">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr18">
                                    <td>
                                        <asp:Label ID="Label28" runat="server" Text="2nd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC47" runat="server" Text="" /></td>
                                    <td runat="server" id="td18">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr19">
                                    <td>
                                        <asp:Label ID="Label30" runat="server" Text="3rd Run"></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbC48" runat="server" Text="" /></td>
                                    <td runat="server" id="td19">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr20">
                                    <td>
                                        <asp:Label ID="Label32" runat="server" Text="Average"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB49" runat="server" Text="" /></td>
                                    <td>
                                        <asp:Label ID="lbC49" runat="server" Text="" /></td>
                                    <td runat="server" id="td20">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="well">
                <h3><i class="icon-tasks"></i>Operations (<asp:Label ID="lbJobStatus" runat="server" Text=""></asp:Label>)</h3>
                <asp:Panel ID="pSpecification" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <label class="control-label" for="ddlSpecification">Specification:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSpecification" runat="server" class="span12 chosen" DataTextField="B" DataValueField="ID" OnSelectedIndexChanged="ddlSpecification_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pStatus" runat="server">
                    <div class="row-fluid">

                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="ddlStatus">
                                    <asp:Label ID="lbStatus" runat="server" Text="Approve Status:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="span6" DataTextField="name" DataValueField="ID" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pRemark" runat="server">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="txtRemark">
                                    <asp:Label ID="Label1" runat="server" Text="Remark:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtRemark" name="txtRemark" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pDisapprove" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <asp:Label ID="lbAssignTo" runat="server" Text="Assign To:" class="control-label"></asp:Label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAssignTo" runat="server" class="span12 chosen" DataTextField="name" DataValueField="ID" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>


                </asp:Panel>
                <asp:Panel ID="pDownload" runat="server">
                    <ul>
                        <li>Link for download Cover Page
                            <asp:LinkButton ID="lbDownload" runat="server" OnClick="lbDownload_Click">
                                <i class="icon-download-alt"></i>
                                <asp:Label ID="lbDownloadName" runat="server" Text="Label"></asp:Label>
                            </asp:LinkButton>
                        </li>
                    </ul>
                </asp:Panel>
                <asp:Panel ID="pUploadfile" runat="server">
                    <div class="row-fluid fileupload-buttonbar">
                        <div class="span6">
                            <label class="control-label" for="ddlSpecification">Uplod file:</label>
                            <span class="btn green fileinput-button">
                                <i class="icon-plus icon-white"></i>
                                <span>Add files...</span>
                                <asp:FileUpload ID="btnUpload" runat="server" />
                            </span>
                        </div>
                    </div>
                    <ul>
                        <li>The maximum file size for uploads in this demo is <strong>5 MB</strong> (default file size is unlimited).</li>
                        <li>Only word or pdf files (<strong>DOC, DOCX, PDF</strong>) are allowed in this demo (by default there is no file type restriction).</li>
                    </ul>
                    <br />
                    <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                    <br />
                </asp:Panel>

            </div>

            <div class="form-actions clearfix">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn" />
            </div>
            <!-- END PAGE CONTENT-->

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <asp:PostBackTrigger ControlID="lbDownload" />
        </Triggers>
    </asp:UpdatePanel>
</form>
