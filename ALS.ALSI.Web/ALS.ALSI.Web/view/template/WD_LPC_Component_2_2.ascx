﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WD_LPC_Component_2_2.ascx.cs" Inherits="ALS.ALSI.Web.view.template.WD_LPC_Component_2_2" %>

<script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date-picker').datepicker();
    });
</script>
<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <!-- BEGIN COVERPAGE-TH CONTENT-->
            <div class="row-fluid invoice" id="invDiv" runat="server">
                <div>
                    CUSTOMER PO NO.:
                        <asp:Label ID="lbPoNo" runat="server" Text=""></asp:Label><br />
                    ALS THAILAND REF NO.:
                        <asp:Label ID="lbRefNo" runat="server" Text=""></asp:Label><br />
                    DATE:
                        <asp:Label ID="lbDate" runat="server" Text=""></asp:Label><br />
                    COMPANY:
                        <asp:Label ID="lbCompany" runat="server" Text=""></asp:Label><br />
                    <br />
                    DATE SAMPLE RECEIVED:
                        <asp:Label ID="lbDateSampleReceived" runat="server" Text=""></asp:Label><br />
                    DATE TEST COMPLETED:
                        <asp:Label ID="lbDateTestCompleted" runat="server" Text=""></asp:Label><br />
                    <br />
                    SAMPLE DESCRIPTION: One lot of sample was received with references:<br />
                    <asp:Label ID="lbSampleDescription" runat="server" Text=""></asp:Label>
                </div>

                <div class="row-fluid">
                    <div class="span12 ">
                        <h5>METHOD/PROCEDURE:</h5>
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Test</th>
                                    <th>Procedure No</th>
                                    <th>Number of pieces used<br />
                                        for extraction</th>
                                    <th>Extraction<br />
                                        Medium</th>
                                    <th>Extraction Volume<br />
                                        (mL)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>LPC</td>
                                    <td>
                                        <asp:Label ID="lbB21" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC21" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbD21" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE21" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span6 ">
                        <h6>Results:</h6>
                        <h6>The specification is based on Western Digital's document no.
                            <asp:Label ID="lbSpecRev" runat="server" Text=""></asp:Label>for
                            <asp:Label ID="lbComponent" runat="server" Text=""></asp:Label>
                        </h6>
                    </div>
                </div>
                <br />
                <%-- Liquid Particle Count (68 KHz) 0.3 --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb1" runat="server">
                            <thead>
                                <tr>
                                    <th>Required Test</th>
                                    <th>Particle Bin Size (µm)</th>
                                    <th>Specification Limits<br />
                                        (Particles/cm2)</th>
                                    <th>Average of 5 data points<br />
                                        (Particles/cm2)</th>
                                    <th>PASS / FAIL</th>
                                    <th runat="server" id="th1">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr runat="server" id="tr1">
                                    <td>LPC</td>
                                    <td>≥ 0.3</td>
                                    <td>
                                        <asp:Label ID="lbC27" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD27" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE27" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox2" runat="server" Checked="True" /></td>
                                </tr>
                                <tr runat="server" id="tr2">
                                    <td>LPC</td>
                                    <td>≥ 0.5</td>
                                    <td>
                                        <asp:Label ID="lbC28" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD28" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE28" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox3" runat="server" Checked="True" /></td>
                                </tr>
                                <tr runat="server" id="tr3">
                                    <td>LPC</td>
                                    <td>≥ 1.0</td>
                                    <td>
                                        <asp:Label ID="lbC29" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD29" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE29" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox4" runat="server" Checked="True" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <%-- Test Method: 92-004230 Rev. AK	--%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tab2" runat="server">
                            <tbody>
                                <tr runat="server" id="tr4">
                                    <td colspan="2">Test Method: 92-004230 Rev. AK</td>
                                </tr>
                                <tr>
                                    <td>Surface Area, cm²</td>
                                    <td>
                                        <asp:Label ID="lbB48" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Extraction Vol, ml</td>
                                    <td>
                                        <asp:Label ID="lbB49" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Sample Vol, ml</td>
                                    <td>
                                        <asp:Label ID="lbB50" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>No. of Parts</td>
                                    <td>
                                        <asp:Label ID="lbB51" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br />
                <%--Tank Conditions--%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="Table1" runat="server">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Total Tank  Volume</th>
                                    <th>Sonication Freq.</th>
                                    <th>Ultrasonic Power</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tank Conditions</td>
                                    <td>
                                        <asp:Label ID="lbB54" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC54" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD54" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br />
                <%--Run--%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="Table2" runat="server">
                            <thead>
                                <tr>
                                    <th>Run</th>
                                    <th>Accumulative Size (µm)</th>
                                    <th>Blank
                                    <br />
                                        (Counts/ml)</th>
                                    <th>Sample
                                    <br />
                                        (Counts/ml)</th>
                                    <th>Blank-corrected
                                    <br />
                                        (Counts/part)</th>
                                    <th>Blank-corrected
                                    <br />
                                        (Counts/cm²)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="0.3"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC57" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD57" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE57" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF57" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Text="0.5"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC58" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD58" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE58" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF58" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label12" runat="server" Text="0.7"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC59" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD59" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE59" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF59" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label17" runat="server" Text="1.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC60" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD60" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE60" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF60" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label22" runat="server" Text="2.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC61" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD61" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE61" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF61" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>
                                        <asp:Label ID="Label27" runat="server" Text="0.3"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC63" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD63" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE63" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF63" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label32" runat="server" Text="0.5"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC64" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD64" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE64" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF64" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label37" runat="server" Text="0.7"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC65" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD65" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE65" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF65" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label42" runat="server" Text="1.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC66" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD66" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE66" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF66" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label47" runat="server" Text="2.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC67" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD67" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE67" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF67" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>
                                        <asp:Label ID="Label52" runat="server" Text="0.3"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC69" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD69" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE69" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF69" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label57" runat="server" Text="0.5"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC70" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD70" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE70" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF70" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label62" runat="server" Text="0.7"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC71" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD71" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE71" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF71" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label67" runat="server" Text="1.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC72" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD72" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE72" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF72" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label72" runat="server" Text="2.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC73" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD73" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE73" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF73" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>
                                        <asp:Label ID="Label77" runat="server" Text="0.3"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC81" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD81" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE81" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF81" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label82" runat="server" Text="0.5"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC82" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD82" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE82" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF82" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label87" runat="server" Text="0.7"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC83" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD83" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE83" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF83" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label92" runat="server" Text="1.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC84" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD84" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE84" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF84" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label97" runat="server" Text="2.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC85" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD85" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE85" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF85" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>
                                        <asp:Label ID="Label102" runat="server" Text="0.3"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC87" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD87" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE87" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF87" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="lbC881" runat="server" Text="0.5"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC88" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD88" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE88" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF88" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label112" runat="server" Text="0.7"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC89" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD89" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE89" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF89" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label117" runat="server" Text="1.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC90" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD90" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE90" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF90" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label122" runat="server" Text="2.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC91" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD91" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE91" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbF91" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br />
                <%--Statistics--%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="Table3" runat="server">
                            <thead>
                                <tr>
                                    <th>Statistics</th>
                                    <th>Accumulative Size (µm)</th>
                                    <th>Blank-corrected<br />
                                        (Counts/part)</th>
                                    <th>Blank-corrected<br />
                                        (Counts/cm²)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Average</td>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="0.3"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC94" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD94" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label9" runat="server" Text="0.5"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC95" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD95" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label15" runat="server" Text="0.7"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC96" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD96" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label21" runat="server" Text="1.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC97" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD97" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label28" runat="server" Text="2.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC98" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD98" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Standard Deviation</td>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Text="0.3"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC100" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD100" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label14" runat="server" Text="0.5"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC101" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD101" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label25" runat="server" Text="0.7"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC102" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD102" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label33" runat="server" Text="1.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC103" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD103" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label36" runat="server" Text="2.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC104" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD104" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>%RSD  Deviation</td>
                                    <td>
                                        <asp:Label ID="Label40" runat="server" Text="0.3"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC106" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD106" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label44" runat="server" Text="0.5"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC107" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD107" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label48" runat="server" Text="0.7"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC108" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD108" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label51" runat="server" Text="1.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC109" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD109" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="Label55" runat="server" Text="2.0"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC110" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD110" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="well">
                <h3><i class="icon-tasks"></i>Operations (<asp:Label ID="lbJobStatus" runat="server" Text=""></asp:Label>)</h3>
                <asp:Panel ID="pSpecification" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <label class="control-label" for="ddlSpecification">Specification:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSpecification" runat="server" class="span12 chosen" DataTextField="B" DataValueField="ID" OnSelectedIndexChanged="ddlSpecification_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6 ">
                            <label class="control-label" for="ddlComponent">Component:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlComponent" runat="server" class="span12 chosen" DataTextField="A" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="ddlComponent_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pStatus" runat="server">
                    <div class="row-fluid">

                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="ddlStatus">
                                    <asp:Label ID="lbStatus" runat="server" Text="Approve Status:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="span6" DataTextField="name" DataValueField="ID" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pRemark" runat="server">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="txtRemark">
                                    <asp:Label ID="Label1" runat="server" Text="Remark:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtRemark" name="txtRemark" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pDisapprove" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <asp:Label ID="lbAssignTo" runat="server" Text="Assign To:" class="control-label"></asp:Label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAssignTo" runat="server" class="span12 chosen" DataTextField="name" DataValueField="ID" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>


                </asp:Panel>
                <asp:Panel ID="pDownload" runat="server">
                    <ul>
                        <li>Link for download Cover Page
                            <asp:LinkButton ID="lbDownload" runat="server" OnClick="lbDownload_Click">
                                <i class="icon-download-alt"></i>
                                <asp:Label ID="lbDownloadName" runat="server" Text="Label"></asp:Label>
                            </asp:LinkButton>
                        </li>
                    </ul>
                </asp:Panel>
                <asp:Panel ID="pUploadfile" runat="server">
                    <div class="row-fluid fileupload-buttonbar">
                        <div class="span6">
                            <label class="control-label" for="ddlSpecification">Uplod file:</label>
                            <span class="btn green fileinput-button">
                                <i class="icon-plus icon-white"></i>
                                <span>Add files...</span>
                                <asp:FileUpload ID="btnUpload" runat="server" />
                            </span>
                        </div>
                    </div>
                    <ul>
                        <li>The maximum file size for uploads in this demo is <strong>5 MB</strong> (default file size is unlimited).</li>
                        <li>Only word or pdf files (<strong>DOC, DOCX, PDF</strong>) are allowed in this demo (by default there is no file type restriction).</li>
                    </ul>
                    <br />
                    <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                    <br />
                </asp:Panel>

            </div>

            <div class="form-actions clearfix">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn" />
            </div>
            <!-- END PAGE CONTENT-->

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <asp:PostBackTrigger ControlID="lbDownload" />
        </Triggers>
    </asp:UpdatePanel>
</form>
