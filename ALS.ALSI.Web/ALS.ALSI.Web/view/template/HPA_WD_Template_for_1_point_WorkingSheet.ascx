﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HPA_WD_Template_for_1_point_WorkingSheet.ascx.cs" Inherits="ALS.ALSI.Web.view.template.HPA_WD_Template_for_1_point_WorkingSheet" %>

<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="alert alert-error hide">
                <button class="close" data-dismiss="alert"></button>
                You have some form errors. Please check below.
            </div>
            <div class="alert alert-success hide">
                <button class="close" data-dismiss="alert"></button>
                Your form validation is successful!
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="row-fluid fileupload-buttonbar">
                        <div class="span6">
                            <%--<label class="control-label" for="ddlSpecification">Uplod file:</label>--%>
                            <span class="btn green fileinput-button">
                                <i class="icon-plus icon-white"></i>
                                <span>Choose DSH files...</span>
                                <asp:FileUpload ID="btnUpload" runat="server" AllowMultiple="True" />
                            </span>

                        </div>
                    </div>
                    <div class="row-fluid">
                        <ul>
                            <li>The maximum file size for uploads is <strong>5 MB</strong> (default file size is unlimited).</li>
                            <li>Only text files (<strong>txt</strong>) are allowed.</li>
                        </ul>
                        <br />
                        <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                        <br />

                    </div>                     
                </div>
            </div>
            </div>
            <br />
            <div class="row-fluid">
                <div class="span10">
                    <div class="tabbable tabbable-custom boxless">
                        <ul class="nav nav-tabs">

                        </ul>
                        <div class="tab-content">
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-actions clearfix">
                <asp:Button ID="btnLoadFile" runat="server" Text="Load" CssClass="btn blue" OnClick="btnLoadFile_Click" />
                <%--<asp:Button ID="btnCalculate" runat="server" Text="Calculate" CssClass="btn green" OnClick="btnCalculate_Click" />--%>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="disable btn" OnClick="btnCancel_Click" />
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoadFile" />
            <asp:PostBackTrigger ControlID="btnSubmit" />
        </Triggers>
    </asp:UpdatePanel>
</form>
