﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Web.view.request;
using System;
using System.Web.UI;

namespace ALS.ALSI.Web.view.template
{
    public partial class WD_IC_Component_1_3_WorkingSheet : System.Web.UI.UserControl
    {

        #region "Property"

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        public template_wd_ic_coverpage objWork
        {
            get
            {
                template_wd_ic_coverpage work = new template_wd_ic_coverpage();
                work.sample_id = this.SampleID;
                work.specification_id = int.Parse(hDdlSpec.Value);

                work.b11 = txtB11.Text;
                work.b12 = txtB12.Text;
                work.b13 = txtB13.Text;

                #region "B"
                work.b17 = txtB17.Text;
                work.b18 = txtB18.Text;
                work.b19 = txtB19.Text;
                work.b20 = txtB20.Text;
                work.b21 = txtB21.Text;
                work.b22 = txtB22.Text;

                work.b26 = txtB26.Text;
                work.b27 = txtB27.Text;
                work.b28 = txtB28.Text;
                work.b29 = txtB29.Text;
                work.b30 = txtB30.Text;
                work.b31 = txtB31.Text;
                #endregion

                #region "C"
                work.c17 = txtC17.Text;
                work.c18 = txtC18.Text;
                work.c19 = txtC19.Text;
                work.c20 = txtC20.Text;
                work.c21 = txtC21.Text;
                work.c22 = txtC22.Text;

                work.c26 = txtC26.Text;
                work.c27 = txtC27.Text;
                work.c28 = txtC28.Text;
                work.c29 = txtC29.Text;
                work.c30 = txtC30.Text;
                work.c31 = txtC31.Text;
                #endregion

                #region "C"
                work.d17 = txtD17.Text;
                work.d18 = txtD18.Text;
                work.d19 = txtD19.Text;
                work.d20 = txtD20.Text;
                work.d21 = txtD21.Text;
                work.d22 = txtD22.Text;

                work.d26 = txtD26.Text;
                work.d27 = txtD27.Text;
                work.d28 = txtD28.Text;
                work.d29 = txtD29.Text;
                work.d30 = txtD30.Text;
                work.d31 = txtD31.Text;
                #endregion

                #region "F(Method Detection Limit)"
                work.f17 = lbAnF17.Text;
                work.f18 = lbAnF18.Text;
                work.f19 = lbAnF19.Text;
                work.f20 = lbAnF20.Text;
                work.f21 = lbAnF21.Text;
                work.f22 = lbAnF22.Text;

                work.f26 = lbAnF26.Text;
                work.f27 = lbAnF27.Text;
                work.f28 = lbAnF28.Text;
                work.f29 = lbAnF29.Text;
                work.f30 = lbAnF30.Text;
                work.f31 = lbAnF31.Text;
                #endregion

                #region "I(Final Conc. of Sample)"
                work.i17 = lbAnI17.Text;
                work.i18 = lbAnI18.Text;
                work.i19 = lbAnI19.Text;
                work.i20 = lbAnI20.Text;
                work.i21 = lbAnI21.Text;
                work.i22 = lbAnI22.Text;
                work.i23 = lbAnI23.Text;

                work.i26 = lbAnI26.Text;
                work.i27 = lbAnI27.Text;
                work.i28 = lbAnI28.Text;
                work.i29 = lbAnI29.Text;
                work.i30 = lbAnI30.Text;
                work.i31 = lbAnI31.Text;
                work.i32 = lbAnI32.Text;
                #endregion

                work.item_visible = hItemVisible.Value;
                return work;
            }
        }

        private void initialPage()
        {

            template_wd_ic_coverpage work = new template_wd_ic_coverpage().SelectBySampleID(this.SampleID);
            if (work != null)
            {
                hDdlSpec.Value = work.specification_id.ToString();

                #region "B"
                txtB11.Text = work.b11;
                txtB12.Text = work.b12;
                txtB13.Text = work.b13;

                txtB17.Text = work.b17;
                txtB18.Text = work.b18;
                txtB19.Text = work.b19;
                txtB20.Text = work.b20;
                txtB21.Text = work.b21;
                txtB22.Text = work.b22;

                txtB26.Text = work.b26;
                txtB27.Text = work.b27;
                txtB28.Text = work.b28;
                txtB29.Text = work.b29;
                txtB30.Text = work.b30;
                txtB31.Text = work.b31;
                #endregion

                #region "C"
                txtC17.Text = work.c17;
                txtC18.Text = work.c18;
                txtC19.Text = work.c19;
                txtC20.Text = work.c20;
                txtC21.Text = work.c21;
                txtC22.Text = work.c22;

                txtC26.Text = work.c26;
                txtC27.Text = work.c27;
                txtC28.Text = work.c28;
                txtC29.Text = work.c29;
                txtC30.Text = work.c30;
                txtC31.Text = work.c31;
                #endregion

                #region "C"
                txtD17.Text = work.d17;
                txtD18.Text = work.d18;
                txtD19.Text = work.d19;
                txtD20.Text = work.d20;
                txtD21.Text = work.d21;
                txtD22.Text = work.d22;

                txtD26.Text = work.d26;
                txtD27.Text = work.d27;
                txtD28.Text = work.d28;
                txtD29.Text = work.d29;
                txtD30.Text = work.d30;
                txtD31.Text = work.d31;
                #endregion

                #region "F(Method Detection Limit)"
                lbAnF17.Text = work.f17;
                lbAnF18.Text = work.f18;
                lbAnF19.Text = work.f19;
                lbAnF20.Text = work.f20;
                lbAnF21.Text = work.f21;
                lbAnF22.Text = work.f22;

                lbAnF26.Text = work.f26;
                lbAnF27.Text = work.f27;
                lbAnF28.Text = work.f28;
                lbAnF29.Text = work.f29;
                lbAnF30.Text = work.f30;
                lbAnF31.Text = work.f31;
                #endregion

                #region "I(Final Conc. of Sample)"
                lbAnI17.Text = work.i17;
                lbAnI18.Text = work.i18;
                lbAnI19.Text = work.i19;
                lbAnI20.Text = work.i20;
                lbAnI21.Text = work.i21;
                lbAnI22.Text = work.i22;
                lbAnI23.Text = work.i23;

                lbAnI26.Text = work.i26;
                lbAnI27.Text = work.i27;
                lbAnI28.Text = work.i28;
                lbAnI29.Text = work.i29;
                lbAnI30.Text = work.i30;
                lbAnI31.Text = work.i31;
                lbAnI32.Text = work.i32;
                #endregion


                hItemVisible.Value = work.item_visible;
                //CAL
                calculateByFormular();
            }

            //Disable Save button
            btnSubmit.Enabled = false;
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            job_sample sample = new job_sample().SelectByID(this.SampleID);
            sample.job_status = Convert.ToInt32(StatusEnum.SR_CHEMIST_CHECKING);
            sample.Update();

            objWork.UpdateBySampleID();
            //Commit
            GeneralManager.Commit();
            Response.Redirect(this.PreviousPath);
        }

        protected void btnCalulate_Click(object sender, EventArgs e)
        {
            btnSubmit.Enabled = true;
            calculateByFormular();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.PreviousPath);
        }

        private void calculateByFormular()
        {

            Decimal b11 = Convert.ToDecimal(String.IsNullOrEmpty(txtB11.Text) ? "0" : txtB11.Text);
            Decimal b12 = Convert.ToDecimal(String.IsNullOrEmpty(txtB12.Text) ? "0" : txtB12.Text);
            Decimal b13 = Convert.ToDecimal(String.IsNullOrEmpty(txtB13.Text) ? "0" : txtB13.Text);

            if (b11 != 0 && b12 != 0 && b13 != 0)
            {
                #region "B"
                Decimal b17 = Convert.ToDecimal(String.IsNullOrEmpty(txtB17.Text) ? "0" : txtB17.Text);
                Decimal b18 = Convert.ToDecimal(String.IsNullOrEmpty(txtB18.Text) ? "0" : txtB18.Text);
                Decimal b19 = Convert.ToDecimal(String.IsNullOrEmpty(txtB19.Text) ? "0" : txtB19.Text);
                Decimal b20 = Convert.ToDecimal(String.IsNullOrEmpty(txtB20.Text) ? "0" : txtB20.Text);
                Decimal b21 = Convert.ToDecimal(String.IsNullOrEmpty(txtB21.Text) ? "0" : txtB21.Text);
                Decimal b22 = Convert.ToDecimal(String.IsNullOrEmpty(txtB22.Text) ? "0" : txtB22.Text);

                Decimal b26 = Convert.ToDecimal(String.IsNullOrEmpty(txtB26.Text) ? "0" : txtB26.Text);
                Decimal b27 = Convert.ToDecimal(String.IsNullOrEmpty(txtB27.Text) ? "0" : txtB27.Text);
                Decimal b28 = Convert.ToDecimal(String.IsNullOrEmpty(txtB28.Text) ? "0" : txtB28.Text);
                Decimal b29 = Convert.ToDecimal(String.IsNullOrEmpty(txtB29.Text) ? "0" : txtB29.Text);
                Decimal b30 = Convert.ToDecimal(String.IsNullOrEmpty(txtB30.Text) ? "0" : txtB30.Text);
                Decimal b31 = Convert.ToDecimal(String.IsNullOrEmpty(txtB31.Text) ? "0" : txtB31.Text);
                #endregion

                #region "C"
                Decimal c17 = Convert.ToDecimal(String.IsNullOrEmpty(txtC17.Text) ? "0" : txtC17.Text);
                Decimal c18 = Convert.ToDecimal(String.IsNullOrEmpty(txtC18.Text) ? "0" : txtC18.Text);
                Decimal c19 = Convert.ToDecimal(String.IsNullOrEmpty(txtC19.Text) ? "0" : txtC19.Text);
                Decimal c20 = Convert.ToDecimal(String.IsNullOrEmpty(txtC20.Text) ? "0" : txtC20.Text);
                Decimal c21 = Convert.ToDecimal(String.IsNullOrEmpty(txtC21.Text) ? "0" : txtC21.Text);
                Decimal c22 = Convert.ToDecimal(String.IsNullOrEmpty(txtC22.Text) ? "0" : txtC22.Text);

                Decimal c26 = Convert.ToDecimal(String.IsNullOrEmpty(txtC26.Text) ? "0" : txtC26.Text);
                Decimal c27 = Convert.ToDecimal(String.IsNullOrEmpty(txtC27.Text) ? "0" : txtC27.Text);
                Decimal c28 = Convert.ToDecimal(String.IsNullOrEmpty(txtC28.Text) ? "0" : txtC28.Text);
                Decimal c29 = Convert.ToDecimal(String.IsNullOrEmpty(txtC29.Text) ? "0" : txtC29.Text);
                Decimal c30 = Convert.ToDecimal(String.IsNullOrEmpty(txtC30.Text) ? "0" : txtC30.Text);
                Decimal c31 = Convert.ToDecimal(String.IsNullOrEmpty(txtC31.Text) ? "0" : txtC31.Text);
                #endregion

                #region "D"
                Decimal d17 = Convert.ToDecimal(String.IsNullOrEmpty(txtD17.Text) ? "0" : txtD17.Text);
                Decimal d18 = Convert.ToDecimal(String.IsNullOrEmpty(txtD18.Text) ? "0" : txtD18.Text);
                Decimal d19 = Convert.ToDecimal(String.IsNullOrEmpty(txtD19.Text) ? "0" : txtD19.Text);
                Decimal d20 = Convert.ToDecimal(String.IsNullOrEmpty(txtD20.Text) ? "0" : txtD20.Text);
                Decimal d21 = Convert.ToDecimal(String.IsNullOrEmpty(txtD21.Text) ? "0" : txtD21.Text);
                Decimal d22 = Convert.ToDecimal(String.IsNullOrEmpty(txtD22.Text) ? "0" : txtD22.Text);

                Decimal d26 = Convert.ToDecimal(String.IsNullOrEmpty(txtD26.Text) ? "0" : txtD26.Text);
                Decimal d27 = Convert.ToDecimal(String.IsNullOrEmpty(txtD27.Text) ? "0" : txtD27.Text);
                Decimal d28 = Convert.ToDecimal(String.IsNullOrEmpty(txtD28.Text) ? "0" : txtD28.Text);
                Decimal d29 = Convert.ToDecimal(String.IsNullOrEmpty(txtD29.Text) ? "0" : txtD29.Text);
                Decimal d30 = Convert.ToDecimal(String.IsNullOrEmpty(txtD30.Text) ? "0" : txtD30.Text);
                Decimal d31 = Convert.ToDecimal(String.IsNullOrEmpty(txtD31.Text) ? "0" : txtD31.Text);
                #endregion

                #region "E (Raw Results)"
                //=IF(C17="ND", 0, (C17-B17)*D17*$B$11/$B$12/$B$13*1000)

                lbAnE17.Text = txtC17.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c17 - b17) * d17 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnE18.Text = txtC18.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c18 - b18) * d18 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnE19.Text = txtC19.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c19 - b19) * d19 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnE20.Text = txtC20.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c20 - b20) * d20 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnE21.Text = txtC21.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c21 - b21) * d21 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnE22.Text = txtC22.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c22 - b22) * d22 * b11 / b12 / b13 * 1000), 2).ToString();

                lbAnE26.Text = txtC26.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c26 - b26) * d26 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnE27.Text = txtC27.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c27 - b27) * d27 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnE28.Text = txtC28.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c28 - b28) * d28 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnE29.Text = txtC29.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c29 - b29) * d29 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnE30.Text = txtC30.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c30 - b30) * d30 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnE31.Text = txtC31.Equals(Constants.GetEnumDescription(ResultEnum.ND)) ? "0" : Math.Round(((c31 - b31) * d31 * b11 / b12 / b13 * 1000), 2).ToString();
                #endregion

                #region "G (Instrument Detection Limit)"
                Decimal g17 = new Decimal(0.5);
                Decimal g18 = new Decimal(0.5);
                Decimal g19 = new Decimal(0.5);
                Decimal g20 = new Decimal(0.5);
                Decimal g21 = new Decimal(0.5);
                Decimal g22 = new Decimal(0.5);

                Decimal g26 = new Decimal(0.6);
                Decimal g27 = new Decimal(0.6);
                Decimal g28 = new Decimal(0.6);
                Decimal g29 = new Decimal(0.6);
                Decimal g30 = new Decimal(0.6);
                Decimal g31 = new Decimal(0.6);
                #endregion

                #region "F (Method Detection Limit)"
                //=+G17*$B$11/$B$12/$B$13*1000

                lbAnF17.Text = Math.Round((g17 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnF18.Text = Math.Round((g18 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnF19.Text = Math.Round((g19 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnF20.Text = Math.Round((g20 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnF21.Text = Math.Round((g21 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnF22.Text = Math.Round((g22 * b11 / b12 / b13 * 1000), 2).ToString();

                lbAnF26.Text = Math.Round((g26 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnF27.Text = Math.Round((g27 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnF28.Text = Math.Round((g28 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnF29.Text = Math.Round((g29 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnF30.Text = Math.Round((g30 * b11 / b12 / b13 * 1000), 2).ToString();
                lbAnF31.Text = Math.Round((g31 * b11 / b12 / b13 * 1000), 2).ToString();

                #endregion

                #region "H (Below MDL)"
                //=IF(E17<F17,1,0)
                lbAnH17.Text = (Convert.ToDecimal(lbAnE17.Text) < Convert.ToDecimal(lbAnF17.Text)) ? "1" : "0";
                lbAnH18.Text = (Convert.ToDecimal(lbAnE18.Text) < Convert.ToDecimal(lbAnF18.Text)) ? "1" : "0";
                lbAnH19.Text = (Convert.ToDecimal(lbAnE19.Text) < Convert.ToDecimal(lbAnF19.Text)) ? "1" : "0";
                lbAnH20.Text = (Convert.ToDecimal(lbAnE20.Text) < Convert.ToDecimal(lbAnF20.Text)) ? "1" : "0";
                lbAnH21.Text = (Convert.ToDecimal(lbAnE21.Text) < Convert.ToDecimal(lbAnF21.Text)) ? "1" : "0";
                lbAnH22.Text = (Convert.ToDecimal(lbAnE22.Text) < Convert.ToDecimal(lbAnF22.Text)) ? "1" : "0";

                lbAnH26.Text = (Convert.ToDecimal(lbAnE26.Text) < Convert.ToDecimal(lbAnF26.Text)) ? "1" : "0";
                lbAnH27.Text = (Convert.ToDecimal(lbAnE27.Text) < Convert.ToDecimal(lbAnF27.Text)) ? "1" : "0";
                lbAnH28.Text = (Convert.ToDecimal(lbAnE28.Text) < Convert.ToDecimal(lbAnF28.Text)) ? "1" : "0";
                lbAnH29.Text = (Convert.ToDecimal(lbAnE29.Text) < Convert.ToDecimal(lbAnF29.Text)) ? "1" : "0";
                lbAnH30.Text = (Convert.ToDecimal(lbAnE30.Text) < Convert.ToDecimal(lbAnF30.Text)) ? "1" : "0";
                lbAnH31.Text = (Convert.ToDecimal(lbAnE31.Text) < Convert.ToDecimal(lbAnF31.Text)) ? "1" : "0";
                #endregion

                #region "I (Final Conc. of Sample)"
                //=IF(H17=1,"Not Detected",FIXED(E17,2,1))
                lbAnI17.Text = lbAnH17.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE17.Text), 2).ToString();
                lbAnI18.Text = lbAnH18.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE18.Text), 2).ToString();
                lbAnI19.Text = lbAnH19.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE19.Text), 2).ToString();
                lbAnI20.Text = lbAnH20.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE20.Text), 2).ToString();
                lbAnI21.Text = lbAnH21.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE21.Text), 2).ToString();
                lbAnI22.Text = lbAnH22.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE22.Text), 2).ToString();

                lbAnI23.Text = (Convert.ToDecimal(lbAnI17.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI17.Text) +
                    Convert.ToDecimal(lbAnI18.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI18.Text) +
                    Convert.ToDecimal(lbAnI19.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI19.Text) +
                    Convert.ToDecimal(lbAnI20.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI20.Text) +
                    Convert.ToDecimal(lbAnI21.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI21.Text) +
                    Convert.ToDecimal(lbAnI22.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI22.Text)).ToString();
                lbAnJ23.Text = lbAnI23.Text;

                lbAnI26.Text = lbAnH26.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE26.Text), 2).ToString();
                lbAnI27.Text = lbAnH27.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE27.Text), 2).ToString();
                lbAnI28.Text = lbAnH28.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE28.Text), 2).ToString();
                lbAnI29.Text = lbAnH29.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE29.Text), 2).ToString();
                lbAnI30.Text = lbAnH30.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE30.Text), 2).ToString();
                lbAnI31.Text = lbAnH31.Text.Equals("1") ? Constants.GetEnumDescription(ResultEnum.NOT_DETECTED) : Math.Round(Convert.ToDecimal(lbAnE31.Text), 2).ToString();
                lbAnI32.Text = (Convert.ToDecimal(lbAnI26.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI26.Text) +
                                Convert.ToDecimal(lbAnI27.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI27.Text) +
                                Convert.ToDecimal(lbAnI28.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI28.Text) +
                                Convert.ToDecimal(lbAnI29.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI29.Text) +
                                Convert.ToDecimal(lbAnI30.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI30.Text) +
                                Convert.ToDecimal(lbAnI31.Text.Equals(Constants.GetEnumDescription(ResultEnum.NOT_DETECTED)) ? "0" : lbAnI31.Text)).ToString();
                lbAnJ32.Text = lbAnI32.Text;
                #endregion

                #region "J (For Use in Total)"
                lbAnJ17.Text = lbAnH17.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE17.Text), 2).ToString();
                lbAnJ18.Text = lbAnH18.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE18.Text), 2).ToString();
                lbAnJ19.Text = lbAnH19.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE19.Text), 2).ToString();
                lbAnJ20.Text = lbAnH20.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE20.Text), 2).ToString();
                lbAnJ21.Text = lbAnH21.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE21.Text), 2).ToString();
                lbAnJ22.Text = lbAnH22.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE22.Text), 2).ToString();

                lbAnJ26.Text = lbAnH26.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE26.Text), 2).ToString();
                lbAnJ27.Text = lbAnH27.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE27.Text), 2).ToString();
                lbAnJ28.Text = lbAnH28.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE28.Text), 2).ToString();
                lbAnJ29.Text = lbAnH29.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE29.Text), 2).ToString();
                lbAnJ30.Text = lbAnH30.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE30.Text), 2).ToString();
                lbAnJ31.Text = lbAnH31.Text.Equals("1") ? "0" : Math.Round(Convert.ToDecimal(lbAnE31.Text), 2).ToString();

                #endregion

            }
        }
    }
}