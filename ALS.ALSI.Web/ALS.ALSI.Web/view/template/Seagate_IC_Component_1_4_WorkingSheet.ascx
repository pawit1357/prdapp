﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Seagate_IC_Component_1_4_WorkingSheet.ascx.cs" Inherits="ALS.ALSI.Web.view.template.Seagate_IC_Component_1_4_WorkingSheet" %>
<script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var form1 = $('#Form1');
        var error1 = $('.alert-error', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                ctl00$ContentPlaceHolder2$ctl00$txtB9: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB10: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB11: {
                    required: true,
                    number: true
                },

                ctl00$ContentPlaceHolder2$ctl00$txtB14_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB15_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB16_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB17_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB18_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB19_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB20_Chem: {
                    required: true,
                    number: true
                },

                ctl00$ContentPlaceHolder2$ctl00$txtC14_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC15_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC16_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC17_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC18_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC19_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC20_Chem: {
                    required: true,
                    number: true
                },


                ctl00$ContentPlaceHolder2$ctl00$txtB23_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB24_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB25_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB26_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB27_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB28_Chem: {
                    required: true,
                    number: true
                },

                ctl00$ContentPlaceHolder2$ctl00$txtC23_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC24_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC25_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC26_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC27_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC28_Chem: {
                    required: true,
                    number: true
                },

            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.help-inline').removeClass('ok'); // display OK icon
                $(element)
                    .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            },


            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>
<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="alert alert-error hide">
                <button class="close" data-dismiss="alert"></button>
                You have some form errors. Please check below.
            </div>
            <div class="alert alert-success hide">
                <button class="close" data-dismiss="alert"></button>
                Your form validation is successful!
            </div>

            <div class="row-fluid invoice">
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover">
                            <tbody>
                                <tr>
                                    <td>Total volume (TV) :</td>
                                    <td>
                                        <asp:TextBox ID="txtB9" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Surface area (A) :</td>
                                    <td>
                                        <asp:TextBox ID="txtB10" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>No of parts extracted (N)</td>
                                    <td>
                                        <asp:TextBox ID="txtB11" runat="server"></asp:TextBox></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hDdlSpec" runat="server" Value="0" />
            <asp:HiddenField ID="hItemVisible" runat="server" Value="000000000000000" />
            <br />
            <div class="row-fluid">
                <div class="span12 ">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Anions</th>
                                <th>Conc of water blank,ug/L (B)</th>
                                <th>Conc of sample,ug/L (C)</th>
                                <th>Dilution Factor</th>
                                <th>Raw Result (ug/sq cm)</th>
                                <th>Instrument Detection Limit (ug/L)</th>
                                <th>Method Detection Limit (ug/sq cm)</th>
                                <th>Below Detection? (1=Yes, 0=No)</th>
                                <th>Conc of sample, (ug/sq cm)</th>
                                <th>Result for use in Total (ug/sq cm)</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>Fluoride, F</td>
                                <td>
                                    <asp:TextBox ID="txtB14_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC14_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE14" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnG14" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH14" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI14" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ14" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Chloride, Cl</td>
                                <td>
                                    <asp:TextBox ID="txtB15_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC15_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE15" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnG15" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH15" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI15" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ15" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Nitrite as NO2</td>
                                <td>
                                    <asp:TextBox ID="txtB16_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC16_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE16" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnG16" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH16" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI16" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ16" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Bromide, Br</td>
                                <td>
                                    <asp:TextBox ID="txtB17_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC17_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE17" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnG17" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH17" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI17" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ17" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Nitrate, NO3</td>
                                <td>
                                    <asp:TextBox ID="txtB18_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC18_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE18" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnG18" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH18" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI18" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ18" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Sulfate, SO4</td>
                                <td>
                                    <asp:TextBox ID="txtB19_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC19_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE19" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnG19" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH19" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI19" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ19" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Phosphate, PO4</td>
                                <td>
                                    <asp:TextBox ID="txtB20_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC20_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE20" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnG20" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH20" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI20" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ20" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:Label ID="lbAnH21" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI21" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ21" runat="server"></asp:Label></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <br />
            <div class="row-fluid">
                <div class="span12 ">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Cations</th>
                                <th>Conc of water blank,ug/L (B)</th>
                                <th>Conc of sample,ug/L (C)</th>
                                <th>Dilution Factor</th>
                                <th>Raw Result (ug/sq cm)</th>
                                <th>Instrument Detection Limit (ug/L)</th>
                                <th>Method Detection Limit (ug/sq cm)</th>
                                <th>Below Detection? (1=Yes, 0=No)</th>
                                <th>Conc of sample, (ug/sq cm)</th>
                                <th>Result for use in Total (ug/sq cm)</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>Lithium, Li</td>
                                <td>
                                    <asp:TextBox ID="txtB23_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC23_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE23" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnG23" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH23" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI23" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ23" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Sodium, Na</td>
                                <td>
                                    <asp:TextBox ID="txtB24_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC24_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE24" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnG24" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH24" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI24" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ24" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Ammonium, NH4</td>


                                <td>
                                    <asp:TextBox ID="txtB25_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC25_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE25" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnG25" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH25" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI25" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ25" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Potassium, K</td>


                                <td>
                                    <asp:TextBox ID="txtB26_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC26_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE26" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnG26" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH26" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI26" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ26" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Magnesium, Mg
                                </td>
                                <td>
                                    <asp:TextBox ID="txtB27_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC27_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE27" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnG27" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH27" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI27" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ27" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Calcium, Ca
                                </td>
                                <td>
                                    <asp:TextBox ID="txtB28_Chem" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC28_Chem" runat="server"></asp:TextBox></td>
                                <td>1</td>
                                <td>
                                    <asp:Label ID="lbAnE28" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnG28" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH28" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI28" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ28" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:Label ID="lbAnH29" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI29" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ29" runat="server"></asp:Label></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-actions clearfix">
                    <asp:Button ID="btnCalulate" runat="server" Text="Calculate" CssClass="btn blue" OnClick="btnCalulate_Click" />
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="disable btn" OnClick="btnCancel_Click" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <%--         <asp:PostBackTrigger ControlID="btnCalulate" />--%>
        </Triggers>
    </asp:UpdatePanel>

</form>
