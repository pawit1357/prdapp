﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using ALS.ALSI.Web.view.request;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Linq;

namespace ALS.ALSI.Web.view.template
{
    public partial class HPA_WD_Template_for_1_point_WorkingSheet : System.Web.UI.UserControl
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(HPA_WD_Template_for_1_point_WorkingSheet));

        #region "Property"

        public template_wd_hpa_for1_coverpage HpaFor1
        {
            get { return (template_wd_hpa_for1_coverpage)Session[GetType().Name + "HpaFor1"]; }
            set { Session[GetType().Name + "HpaFor1"] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "HpaFor1");
            Session.Remove(GetType().Name + Constants.PREVIOUS_PATH);
            Session.Remove(GetType().Name + "SampleID");
        }

        private void initialPage()
        {
            this.HpaFor1 = new template_wd_hpa_for1_coverpage().SelectBySampleID(this.SampleID);
            if (this.HpaFor1 != null)
            {

                CalculateCas();
            }
            //initial component
            btnSubmit.Enabled = false;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            job_sample sample = new job_sample().SelectByID(this.SampleID);
            sample.job_status = Convert.ToInt32(StatusEnum.SR_CHEMIST_CHECKING);
            sample.Update();

            //#region "Worksheet"
            //if (this.HpaFor1 != null)
            //{

            //}
            //#endregion

            this.HpaFor1.Update();
            //Commit
            GeneralManager.Commit();

            Response.Redirect(this.PreviousPath);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void btnLoadFile_Click(object sender, EventArgs e)
        {
            #region "LOAD"
            String yyyMMdd = DateTime.Now.ToString("yyyyMMdd");

            List<RawDataArmForHpa1> listDA = new List<RawDataArmForHpa1>();
            for (int i = 0; i < btnUpload.PostedFiles.Count; i++)
            {
                HttpPostedFile _postedFile = btnUpload.PostedFiles[i];
                try
                {
                    if (_postedFile.ContentLength > 0)
                    {
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, _postedFile.FileName);
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }

                        _postedFile.SaveAs(savefilePath);

                        if (Path.GetExtension(savefilePath).Equals(".jpg"))
                        {
                            //Set image path
                            this.HpaFor1.img_path = String.Format("{0}/{1}", yyyMMdd, Path.GetFileName(savefilePath));
                        }
                        else
                        {

                            using (FileStream fs = new FileStream(savefilePath, FileMode.Open, FileAccess.Read))
                            {
                                HSSFWorkbook wd = new HSSFWorkbook(fs);

                                ISheet sheet = wd.GetSheet("Raw Data-Arm");
                                if (sheet != null)
                                {
                                    for (int row = 0; row <= sheet.LastRowNum; row++)
                                    {

                                        RawDataArmForHpa1 RawDataArmForHpa1 = new RawDataArmForHpa1();

                                        RawDataArmForHpa1.Feature = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(0));
                                        RawDataArmForHpa1.Area = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(1));
                                        RawDataArmForHpa1.Field = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(2));
                                        RawDataArmForHpa1.Rank = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(3));
                                        RawDataArmForHpa1.Al_Ti_O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(4));
                                        RawDataArmForHpa1.Si_O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(5));
                                        RawDataArmForHpa1.Fe_Cr_Ni = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(6));
                                        RawDataArmForHpa1.Fe = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(7));
                                        RawDataArmForHpa1.No_Element = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(8));
                                        RawDataArmForHpa1.Other = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(9));
                                        RawDataArmForHpa1.Cr_O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(10));
                                        RawDataArmForHpa1.Rejected_Manual = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(11));
                                        RawDataArmForHpa1.Rejected_ED = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(12));
                                        RawDataArmForHpa1.Rejected_Morph = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(13));
                                        RawDataArmForHpa1.Area_squm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(14));
                                        RawDataArmForHpa1.Aspect_Ratio = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(15));
                                        RawDataArmForHpa1.Beam_X = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(16));
                                        RawDataArmForHpa1.Beam_Y = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(17));
                                        RawDataArmForHpa1.Breadth = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(18));
                                        RawDataArmForHpa1.Direction = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(19));
                                        RawDataArmForHpa1.ECD = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(20));
                                        RawDataArmForHpa1.Length = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(21));
                                        RawDataArmForHpa1.Perimeter = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(22));
                                        RawDataArmForHpa1.Shape = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(23));
                                        RawDataArmForHpa1.Mean_grey = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(24));
                                        RawDataArmForHpa1.Spectrum_Area = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(25));
                                        RawDataArmForHpa1.Stage_X = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(26));
                                        RawDataArmForHpa1.Stage_Y = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(27));
                                        RawDataArmForHpa1.Stage_Z = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(28));
                                        RawDataArmForHpa1.O_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(29));
                                        RawDataArmForHpa1.Mg_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(30));
                                        RawDataArmForHpa1.Al_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(31));
                                        RawDataArmForHpa1.Si_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(32));
                                        RawDataArmForHpa1.P_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(33));
                                        RawDataArmForHpa1.S_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(34));
                                        RawDataArmForHpa1.K_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(35));
                                        RawDataArmForHpa1.Ca_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(36));
                                        RawDataArmForHpa1.Ti_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(37));
                                        RawDataArmForHpa1.Cr_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(38));
                                        RawDataArmForHpa1.Fe_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(39));
                                        RawDataArmForHpa1.Ni_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(40));
                                        RawDataArmForHpa1.Zn_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(41));
                                        RawDataArmForHpa1.Se_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(42));
                                        RawDataArmForHpa1.Cd_Wt = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(43));


                                        listDA.Add(RawDataArmForHpa1);

                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    logger.Error(Ex.Message);
                    Console.WriteLine();
                }
            }
            #endregion

            #region "SET DATA TO FORM"
            this.HpaFor1.ws_b52 = "0";//Al-Mg-O (0.5<=ECD<=2.0 um)
            this.HpaFor1.ws_b53 = listDA.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Al_Ti_O)).ToString();//Al-Ti-O (0.5<=ECD<=2.0 um)
            this.HpaFor1.ws_b54 = "0";//Al-Si-O (0.5<=ECD<=2.0 um)
            this.HpaFor1.ws_b55 = listDA.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Si_O)).ToString();//Si-O
            this.HpaFor1.ws_b56 = "0";//Al-Si-Cu-O (0.5<=ECD<=2.0 um)
            this.HpaFor1.ws_b57 = "0";//Al-Si-Fe-O  (0.5<=ECD<=2.0 um)
            this.HpaFor1.ws_b58 = "0";//Ti-C
            this.HpaFor1.ws_b59 = "0";//Ti-O
            this.HpaFor1.ws_b60 = "0";//W-O
            this.HpaFor1.ws_b61 = "0";//Al-O (0.5<=ECD<=2.0 um)
            this.HpaFor1.ws_b62 = "0";//Al-Si-Mg-O  (0.5<=ECD<=2.0 um)
            this.HpaFor1.ws_b63 = "0";//Al-Cu-O (0.5<=ECD<=2.0 um)
            this.HpaFor1.ws_b64 = "0";//Zr-C
            this.HpaFor1.ws_b65 = "0";//Zr-O
            this.HpaFor1.ws_b66 = "0";//Ti-B
            this.HpaFor1.ws_b67 = "0";//Ti-N
            this.HpaFor1.ws_b68 = "0";//W-C
            this.HpaFor1.ws_b69 = "0";//Al-Cu-O (2.0<ECD<=5.0 um)
            this.HpaFor1.ws_b70 = "0";//Al-Mg-O (2.0<ECD<=5.0 um)
            this.HpaFor1.ws_b71 = "0";//Al-O (2.0<ECD<=5.0 um)
            this.HpaFor1.ws_b72 = "0";//Al-Si-Cu-O (2.0<ECD<=5.0 um)
            this.HpaFor1.ws_b73 = "0";//Al-Si-Fe-O (2.0<ECD<=5.0 um)
            this.HpaFor1.ws_b74 = "0";//Al-Si-Mg-O (2.0<ECD<=5.0 um)
            this.HpaFor1.ws_b75 = "0";//Al-Si-O (2.0<ECD<=5.0 um)
            this.HpaFor1.ws_b76 = "0";//Al-Ti-O (2.0<ECD<=5.0 um)
            this.HpaFor1.ws_b77 = "0";//Pb-Zr-Ti (PZT)
            this.HpaFor1.ws_b78 = "0";//Si-C
            /// ===================================== ///
            this.HpaFor1.ws_b81 = "0";//Fe-Sm
            this.HpaFor1.ws_b82 = "0";//Fe-Nd
            this.HpaFor1.ws_b83 = "0";//Fe-Sr
            this.HpaFor1.ws_b84 = "0";//Ni-Co
            this.HpaFor1.ws_b85 = "0";//Sm-Co
            this.HpaFor1.ws_b86 = "0";//Ce-Co
            this.HpaFor1.ws_b87 = "0";//Nd-Pr
            /// ===================================== ///
            this.HpaFor1.ws_b105 = "0";//Fe-Cr
            this.HpaFor1.ws_b106 = listDA.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Fe_Cr_Ni)).ToString();//Fe-Cr-Ni
            this.HpaFor1.ws_b107 = "0";//Fe-Cr-Ni-Si
            this.HpaFor1.ws_b108 = "0";//Fe-Mn
            this.HpaFor1.ws_b109 = "0";//Fe-Ni
            this.HpaFor1.ws_b110 = "0";//Fe-Cr-Mn-S
            this.HpaFor1.ws_b111 = "0";//Fe-Cr-Ni-Mn
            this.HpaFor1.ws_b112 = listDA.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Fe)).ToString();//Fe
            this.HpaFor1.ws_b113 = "0";//Fe-O
            /// ===================================== ///
            this.HpaFor1.ws_b116 = "0";//Cu-S-Al-O Base
            this.HpaFor1.ws_b117 = "0";//No Element
            this.HpaFor1.ws_b118 = "0";//SCrMn/Fe
            this.HpaFor1.ws_b119 = "0";//Ti-O/Al-Si-Fe
            this.HpaFor1.ws_b120 = "0";//Ni
            this.HpaFor1.ws_b121 = "0";//Ni-P
            this.HpaFor1.ws_b122 = "0";//Sn base
            this.HpaFor1.ws_b123 = listDA.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Other)).ToString();//Other
            this.HpaFor1.ws_b124 = "0";//Al
            this.HpaFor1.ws_b125 = "0";//Al-Mg
            this.HpaFor1.ws_b126 = "0";//Al-Ti-Si
            this.HpaFor1.ws_b127 = "0";//Al-Cu
            this.HpaFor1.ws_b128 = "0";//Al-Si-Cu
            this.HpaFor1.ws_b129 = "0";//Al-Si/Fe
            this.HpaFor1.ws_b130 = "0";//Al-Si-Mg
            this.HpaFor1.ws_b131 = "0";//Mg-Si-O-Al
            /// ===================================== ///
            this.HpaFor1.ws_b132 = "0";//Mg-Si-O
            /// ===================================== ///
            this.HpaFor1.ws_b133 = "0";//Ti Base
            this.HpaFor1.ws_b134 = listDA.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Cr_O)).ToString();//Cr-O
            this.HpaFor1.ws_b135 = "0";//Na-Cl
            this.HpaFor1.ws_b136 = "0";//Cu
            this.HpaFor1.ws_b137 = "0";//Cu-Au
            this.HpaFor1.ws_b138 = "0";//Ag-S
            this.HpaFor1.ws_b139 = "0";//Au-Ni
            this.HpaFor1.ws_b140 = "0";//Cu-Au-Ni
            this.HpaFor1.ws_b141 = "0";//Cu-Zn Base
            this.HpaFor1.ws_b142 = "0";//Cu-Zn-Ni
            this.HpaFor1.ws_b143 = "0";//Cu-Zn-Au-Ni
            this.HpaFor1.ws_b144 = "0";//Zn
            this.HpaFor1.ws_b145 = "0";//Pb
            this.HpaFor1.ws_b146 = "0";//Fe-Cu
            this.HpaFor1.ws_b147 = "0";//Cr-Mn
            this.HpaFor1.ws_b148 = "0";//F-O
            this.HpaFor1.ws_b149 = "0";//Al-Si Base
            this.HpaFor1.ws_b150 = "0";//NiP/Al
            this.HpaFor1.ws_b151 = "0";//NiP/Fe
            this.HpaFor1.ws_b152 = "0";//NiP Base
            this.HpaFor1.ws_b153 = "0";//Ba-S Base
            this.HpaFor1.ws_b154 = "0";//Al-S/Si
            this.HpaFor1.ws_b155 = "0";//AlSi/K
            this.HpaFor1.ws_b156 = "0";//Ca
            this.HpaFor1.ws_b157 = "0";//AlSi/Fe-Cr-Mn-Cu
            this.HpaFor1.ws_b158 = "0";//Cu-Zn
            this.HpaFor1.ws_b159 = "0";//Cu-S
            this.HpaFor1.ws_b160 = "0";//Fe-Cr/S
            this.HpaFor1.ws_b161 = "0";//Sb Base

            #endregion
            CalculateCas();
            btnSubmit.Enabled = true;

        }




        #region "Custom method"

        private String validateDSHFile(IList<HttpPostedFile> _files)
        {
            Boolean isFound_b1 = false;
            Boolean isFound_s1 = false;
            Boolean isFound_hb1 = false;
            Boolean isFound_hs1 = false;
            Boolean isFoundWrongExtension = false;

            String result = String.Empty;

            String[] files = new String[_files.Count];
            if (files.Length == 4)
            {
                for (int i = 0; i < _files.Count; i++)
                {
                    files[i] = _files[i].FileName;
                    if (!Path.GetExtension(_files[i].FileName).Trim().ToLower().Equals(".xls"))
                    {
                        isFoundWrongExtension = true;
                        break;
                    }
                }
                if (!isFoundWrongExtension)
                {

                    //Find B1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B"))
                        {
                            isFound_b1 = true;
                            break;
                        }
                    }

                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S"))
                        {
                            isFound_s1 = true;
                            break;
                        }
                    }

                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("HPA(B)"))
                        {
                            isFound_hb1 = true;
                            break;
                        }
                    }

                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("HPA(S)"))
                        {
                            isFound_hs1 = true;
                            break;
                        }
                    }
                    result = (!isFound_b1) ? result += "File not found B.xls" :
                                (!isFound_s1) ? result += "File not found S.xls" :
                                (!isFound_hb1) ? result += "File not found HPA(B).xls" :
                                (!isFound_hs1) ? result += "File not found HPA(S)xls" : String.Empty;
                }
                else
                {
                    result = "File extension must be *.xls";
                }
            }
            else
            {
                result = "You must to select 4 files for upload.";
            }
            return result;
        }

        private void CalculateCas()
        {

        }

        #endregion

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            CalculateCas();
            btnSubmit.Enabled = true;
        }

    }
}