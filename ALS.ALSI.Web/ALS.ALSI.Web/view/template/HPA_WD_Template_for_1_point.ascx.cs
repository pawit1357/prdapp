﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Web.view.request;
using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ALS.ALSI.Web.view.template
{
    public partial class HPA_WD_Template_for_1_point : System.Web.UI.UserControl
    {

        #region "Property"

        public user_login userLogin
        {
            get
            {
                return ((Session[Constants.SESSION_USER] != null) ? (user_login)Session[Constants.SESSION_USER] : null);
            }
        }

        public job_sample jobSample
        {
            get { return (job_sample)Session["job_sample"]; }
            set { Session["job_sample"] = value; }
        }

        public CommandNameEnum CommandName
        {
            get { return (CommandNameEnum)ViewState[Constants.COMMAND_NAME]; }
            set { ViewState[Constants.COMMAND_NAME] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int JobID
        {
            get { return (int)Session[GetType().Name + "JobID"]; }
            set { Session[GetType().Name + "JobID"] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        public template_wd_hpa_for1_coverpage HpaFor1
        {
            get { return (template_wd_hpa_for1_coverpage)Session[GetType().Name + "HpaFor1"]; }
            set { Session[GetType().Name + "HpaFor1"] = value; }
        }

        private void initialPage()
        {
            #region "Initial UI Component"
            ddlAssignTo.Items.Clear();
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LOGIN_SELECT_SPEC), Convert.ToInt16(StatusEnum.LOGIN_SELECT_SPEC) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.CHEMIST_TESTING), Convert.ToInt16(StatusEnum.CHEMIST_TESTING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_CHECKING), Convert.ToInt16(StatusEnum.SR_CHEMIST_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_WORD), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_WORD) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_CHECKING), Convert.ToInt16(StatusEnum.LABMANAGER_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_PDF), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_PDF) + ""));


            ddlComponent.Items.Clear();
            ddlComponent.DataSource = new tb_component_hpa_for1().SelectBySpecificationID(Convert.ToInt16(SpecificationEnum.WD));
            ddlComponent.DataBind();
            ddlComponent.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));

            ddlSpecification.Items.Clear();
            ddlSpecification.DataSource = new tb_detailspec_hpa_for1().SelectBySpecificationID(Convert.ToInt16(SpecificationEnum.WD));
            ddlSpecification.DataBind();
            ddlSpecification.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));
            #endregion

            #region "job Info"
            job_info job = new job_info().SelectByID(this.JobID);

            lbPoNo.Text = (job.customer_po_ref == null) ? String.Empty : job.customer_po_ref.ToString();
            lbDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
            lbCompany.Text = String.Format("{0}<br />{1}", job.m_customer.company_name, job.m_customer.address);
            lbDateSampleReceived.Text = Convert.ToDateTime(job.date_of_receive).ToString("MM/dd/yyyy");

            this.jobSample = new job_sample().SelectByID(this.SampleID);

            if (this.jobSample != null)
            {


                lbRefNo.Text = this.jobSample.job_number.ToString();
                lbDownloadName.Text = this.jobSample.job_number.ToString();
                lbDateTestCompleted.Text = Convert.ToDateTime(this.jobSample.due_date).ToString("MM/dd/yyyy");

                lbSampleDescription.Text = String.Format("Description:{0}<br />Model:{1}<br />Surface Area:{2}<br />Remark:{3}<br />", this.jobSample.description, this.jobSample.model, this.jobSample.surface_area, this.jobSample.remarks);

                RoleEnum userRole = (RoleEnum)Enum.Parse(typeof(RoleEnum), userLogin.role_id.ToString(), true);

                StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);

                lbJobStatus.Text = Constants.GetEnumDescription(status);
                ddlStatus.Items.Clear();
                switch (status)
                {
                    case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                        break;
                    case StatusEnum.LOGIN_SELECT_SPEC:
                        pSpecification.Visible = true;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.LOGIN) ? ((this.jobSample.step1owner == null) ? true : ((this.jobSample.step1owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.CHEMIST_TESTING:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.CHEMIST) ? ((this.jobSample.step2owner == null) ? true : ((this.jobSample.step2owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.SR_CHEMIST_CHECKING:
                    case StatusEnum.SR_CHEMIST_APPROVE:
                    case StatusEnum.SR_CHEMIST_DISAPPROVE:

                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_APPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_DISAPPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_WORD:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.LABMANAGER_CHECKING:
                    case StatusEnum.LABMANAGER_APPROVE:
                    case StatusEnum.LABMANAGER_DISAPPROVE:
                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_APPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_DISAPPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_PDF:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        break;
                }

            }
            #endregion

            #region "WorkSheet"
            this.HpaFor1 = new template_wd_hpa_for1_coverpage().SelectBySampleID(this.SampleID);

            if (this.HpaFor1 != null)
            {
                #region "Initial Spec Detail & Component"
                ddlSpecification.SelectedValue = this.HpaFor1.detail_spec_id.ToString();
                ddlComponent.SelectedValue = this.HpaFor1.component_id.ToString();

                tb_detailspec_hpa_for1 detailSpec = new tb_detailspec_hpa_for1().SelectByID(int.Parse(ddlSpecification.SelectedValue));
                if (detailSpec != null)
                {
                    lbDocNo.Text = detailSpec.B;
                    lbComponent.Text = detailSpec.A;
                    //=IF(INDEX('Detail Spec'!$A$3:$G$238,$F$1,4)="NA","NA",(IF(INDEX('Detail Spec'!$A$3:$G$238,$F$1,4)=0,0,CONCATENATE("<",INDEX('Detail Spec'!$A$3:$G$238,$F$1,4)))))
                    lbD28.Text = (detailSpec.D.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.D)));
                    lbD29.Text = (detailSpec.E.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.E)));
                    lbD30.Text = (detailSpec.F.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.F)));
                    lbD31.Text = (detailSpec.G.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.G)));

                }
                tb_component_hpa_for1 comp = new tb_component_hpa_for1().SelectByID(int.Parse(ddlComponent.SelectedValue));
                if (comp != null)
                {
                    lbA23.Text = comp.B;
                    lbB23.Text = comp.G;
                    lbC23.Text = comp.D;
                    lbA34.Text = comp.G;
                    lbA48.Text = comp.G;
                }
                #endregion

                #region "COLUMN B"
                lbB52.Text = this.HpaFor1.ws_b52;
                lbB53.Text = this.HpaFor1.ws_b53;
                lbB54.Text = this.HpaFor1.ws_b54;
                lbB55.Text = this.HpaFor1.ws_b55;
                lbB56.Text = this.HpaFor1.ws_b56;
                lbB57.Text = this.HpaFor1.ws_b57;
                lbB58.Text = this.HpaFor1.ws_b58;
                lbB59.Text = this.HpaFor1.ws_b59;
                lbB60.Text = this.HpaFor1.ws_b60;
                lbB61.Text = this.HpaFor1.ws_b61;
                lbB62.Text = this.HpaFor1.ws_b62;
                lbB63.Text = this.HpaFor1.ws_b63;
                lbB64.Text = this.HpaFor1.ws_b64;
                lbB65.Text = this.HpaFor1.ws_b65;
                lbB66.Text = this.HpaFor1.ws_b66;
                lbB67.Text = this.HpaFor1.ws_b67;
                lbB68.Text = this.HpaFor1.ws_b68;
                lbB69.Text = this.HpaFor1.ws_b69;
                lbB70.Text = this.HpaFor1.ws_b70;
                lbB71.Text = this.HpaFor1.ws_b71;
                lbB72.Text = this.HpaFor1.ws_b72;
                lbB73.Text = this.HpaFor1.ws_b73;
                lbB74.Text = this.HpaFor1.ws_b74;
                lbB75.Text = this.HpaFor1.ws_b75;
                lbB76.Text = this.HpaFor1.ws_b76;
                lbB77.Text = this.HpaFor1.ws_b77;
                lbB78.Text = this.HpaFor1.ws_b78;
                //Subtotal - Hard Particles Only
                if (!String.IsNullOrEmpty(this.HpaFor1.ws_b53) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b54) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b55) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b56) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b57) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b58) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b59) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b60) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b61) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b62) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b63) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b64) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b65) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b66) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b67) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b68) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b69) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b70) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b71) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b72) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b73) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b74) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b75) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b76) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b77) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b78))
                {
                    lbB79.Text = (Convert.ToDouble(this.HpaFor1.ws_b53) +
                                 Convert.ToDouble(this.HpaFor1.ws_b54) +
                                 Convert.ToDouble(this.HpaFor1.ws_b55) +
                                 Convert.ToDouble(this.HpaFor1.ws_b56) +
                                 Convert.ToDouble(this.HpaFor1.ws_b57) +
                                 Convert.ToDouble(this.HpaFor1.ws_b58) +
                                 Convert.ToDouble(this.HpaFor1.ws_b59) +
                                 Convert.ToDouble(this.HpaFor1.ws_b60) +
                                 Convert.ToDouble(this.HpaFor1.ws_b61) +
                                 Convert.ToDouble(this.HpaFor1.ws_b62) +
                                 Convert.ToDouble(this.HpaFor1.ws_b63) +
                                 Convert.ToDouble(this.HpaFor1.ws_b64) +
                                 Convert.ToDouble(this.HpaFor1.ws_b65) +
                                 Convert.ToDouble(this.HpaFor1.ws_b66) +
                                 Convert.ToDouble(this.HpaFor1.ws_b67) +
                                 Convert.ToDouble(this.HpaFor1.ws_b68) +
                                 Convert.ToDouble(this.HpaFor1.ws_b69) +
                                 Convert.ToDouble(this.HpaFor1.ws_b70) +
                                 Convert.ToDouble(this.HpaFor1.ws_b71) +
                                 Convert.ToDouble(this.HpaFor1.ws_b72) +
                                 Convert.ToDouble(this.HpaFor1.ws_b73) +
                                 Convert.ToDouble(this.HpaFor1.ws_b74) +
                                 Convert.ToDouble(this.HpaFor1.ws_b75) +
                                 Convert.ToDouble(this.HpaFor1.ws_b76) +
                                 Convert.ToDouble(this.HpaFor1.ws_b77) +
                                 Convert.ToDouble(this.HpaFor1.ws_b78)).ToString();
                }
                lbB81.Text = this.HpaFor1.ws_b81;
                lbB82.Text = this.HpaFor1.ws_b82;
                lbB83.Text = this.HpaFor1.ws_b83;
                lbB84.Text = this.HpaFor1.ws_b84;
                lbB85.Text = this.HpaFor1.ws_b85;
                lbB86.Text = this.HpaFor1.ws_b86;
                lbB87.Text = this.HpaFor1.ws_b87;
                //Subtotal - Magnetic Particles Only
                if (!String.IsNullOrEmpty(this.HpaFor1.ws_b81) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b82) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b83) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b84) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b85) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b86) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b87))
                {
                    lbB88.Text = (Convert.ToDouble(this.HpaFor1.ws_b81) +
                                 Convert.ToDouble(this.HpaFor1.ws_b82) +
                                 Convert.ToDouble(this.HpaFor1.ws_b83) +
                                 Convert.ToDouble(this.HpaFor1.ws_b84) +
                                 Convert.ToDouble(this.HpaFor1.ws_b85) +
                                 Convert.ToDouble(this.HpaFor1.ws_b86) +
                                 Convert.ToDouble(this.HpaFor1.ws_b87)).ToString();
                }
                //Subtotal- Hard Particles including Magnetic Partilces
                if (!String.IsNullOrEmpty(lbB79.Text) && !String.IsNullOrEmpty(lbB88.Text))
                {
                    lbB89.Text = (Convert.ToDouble(lbB79.Text) + Convert.ToDouble(lbB88.Text)).ToString();
                }

                lbB105.Text = this.HpaFor1.ws_b105;
                lbB106.Text = this.HpaFor1.ws_b106;
                lbB107.Text = this.HpaFor1.ws_b107;
                lbB108.Text = this.HpaFor1.ws_b108;
                lbB109.Text = this.HpaFor1.ws_b109;
                lbB110.Text = this.HpaFor1.ws_b110;
                lbB111.Text = this.HpaFor1.ws_b111;
                lbB112.Text = this.HpaFor1.ws_b112;
                lbB113.Text = this.HpaFor1.ws_b113;
                //Subtotal - Steel Particle Only
                if (!String.IsNullOrEmpty(this.HpaFor1.ws_b105) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b106) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b107) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b108) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b109) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b110) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b111) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b112) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b113) )
                {
                    lbB114.Text = (Convert.ToDouble(this.HpaFor1.ws_b105) +
                                   Convert.ToDouble(this.HpaFor1.ws_b106) +
                                   Convert.ToDouble(this.HpaFor1.ws_b107) +
                                   Convert.ToDouble(this.HpaFor1.ws_b108) +
                                   Convert.ToDouble(this.HpaFor1.ws_b109) +
                                   Convert.ToDouble(this.HpaFor1.ws_b110) +
                                   Convert.ToDouble(this.HpaFor1.ws_b111) +
                                   Convert.ToDouble(this.HpaFor1.ws_b112) +
                                   Convert.ToDouble(this.HpaFor1.ws_b113) ).ToString();
                }
                lbB116.Text = this.HpaFor1.ws_b116;
                lbB117.Text = this.HpaFor1.ws_b117;
                lbB118.Text = this.HpaFor1.ws_b118;
                lbB119.Text = this.HpaFor1.ws_b119;
                lbB120.Text = this.HpaFor1.ws_b120;
                lbB121.Text = this.HpaFor1.ws_b121;
                lbB122.Text = this.HpaFor1.ws_b122;
                lbB123.Text = this.HpaFor1.ws_b123;
                lbB124.Text = this.HpaFor1.ws_b124;
                lbB125.Text = this.HpaFor1.ws_b125;
                lbB126.Text = this.HpaFor1.ws_b126;
                lbB127.Text = this.HpaFor1.ws_b127;
                lbB128.Text = this.HpaFor1.ws_b128;
                lbB129.Text = this.HpaFor1.ws_b129;
                lbB130.Text = this.HpaFor1.ws_b130;
                lbB131.Text = this.HpaFor1.ws_b131;
                lbB132.Text = this.HpaFor1.ws_b132;
                lbB133.Text = this.HpaFor1.ws_b133;
                lbB134.Text = this.HpaFor1.ws_b134;
                lbB135.Text = this.HpaFor1.ws_b135;
                lbB136.Text = this.HpaFor1.ws_b136;
                lbB137.Text = this.HpaFor1.ws_b137;
                lbB138.Text = this.HpaFor1.ws_b138;
                lbB139.Text = this.HpaFor1.ws_b139;
                lbB140.Text = this.HpaFor1.ws_b140;
                lbB141.Text = this.HpaFor1.ws_b141;
                lbB142.Text = this.HpaFor1.ws_b142;
                lbB143.Text = this.HpaFor1.ws_b143;
                lbB144.Text = this.HpaFor1.ws_b144;
                lbB145.Text = this.HpaFor1.ws_b145;
                lbB146.Text = this.HpaFor1.ws_b146;
                lbB147.Text = this.HpaFor1.ws_b147;
                lbB148.Text = this.HpaFor1.ws_b148;
                lbB149.Text = this.HpaFor1.ws_b149;
                lbB150.Text = this.HpaFor1.ws_b150;
                lbB151.Text = this.HpaFor1.ws_b151;
                lbB152.Text = this.HpaFor1.ws_b152;
                lbB153.Text = this.HpaFor1.ws_b153;
                lbB154.Text = this.HpaFor1.ws_b154;
                lbB155.Text = this.HpaFor1.ws_b155;
                lbB156.Text = this.HpaFor1.ws_b156;
                lbB157.Text = this.HpaFor1.ws_b157;
                lbB158.Text = this.HpaFor1.ws_b158;
                lbB159.Text = this.HpaFor1.ws_b159;
                lbB160.Text = this.HpaFor1.ws_b160;
                lbB161.Text = this.HpaFor1.ws_b161;
                //Subtotal - Orther Particle Only
                if (!String.IsNullOrEmpty(this.HpaFor1.ws_b116) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b117) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b118) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b119) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b120) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b121) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b122) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b123) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b124) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b125) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b126) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b127) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b128) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b129) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b130) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b131) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b132) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b133) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b134) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b135) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b136) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b137) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b138) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b139) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b140) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b141) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b142) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b143) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b144) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b145) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b146) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b147) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b148) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b149) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b150) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b151) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b152) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b153) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b154) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b155) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b156) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b157) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b158) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b159) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b160) &&
                   !String.IsNullOrEmpty(this.HpaFor1.ws_b161))
                {
                    lbB162.Text = (Convert.ToDouble(this.HpaFor1.ws_b116) +
                                 Convert.ToDouble(this.HpaFor1.ws_b117) +
                                 Convert.ToDouble(this.HpaFor1.ws_b118) +
                                 Convert.ToDouble(this.HpaFor1.ws_b119) +
                                 Convert.ToDouble(this.HpaFor1.ws_b120) +
                                 Convert.ToDouble(this.HpaFor1.ws_b121) +
                                 Convert.ToDouble(this.HpaFor1.ws_b122) +
                                 Convert.ToDouble(this.HpaFor1.ws_b123) +
                                 Convert.ToDouble(this.HpaFor1.ws_b124) +
                                 Convert.ToDouble(this.HpaFor1.ws_b125) +
                                 Convert.ToDouble(this.HpaFor1.ws_b126) +
                                 Convert.ToDouble(this.HpaFor1.ws_b127) +
                                 Convert.ToDouble(this.HpaFor1.ws_b128) +
                                 Convert.ToDouble(this.HpaFor1.ws_b129) +
                                 Convert.ToDouble(this.HpaFor1.ws_b130) +
                                 Convert.ToDouble(this.HpaFor1.ws_b131) +
                                 Convert.ToDouble(this.HpaFor1.ws_b133) +
                                 Convert.ToDouble(this.HpaFor1.ws_b134) +
                                 Convert.ToDouble(this.HpaFor1.ws_b135) +
                                 Convert.ToDouble(this.HpaFor1.ws_b136) +
                                 Convert.ToDouble(this.HpaFor1.ws_b137) +
                                 Convert.ToDouble(this.HpaFor1.ws_b138) +
                                 Convert.ToDouble(this.HpaFor1.ws_b139) +
                                 Convert.ToDouble(this.HpaFor1.ws_b140) +
                                 Convert.ToDouble(this.HpaFor1.ws_b141) +
                                 Convert.ToDouble(this.HpaFor1.ws_b142) +
                                 Convert.ToDouble(this.HpaFor1.ws_b143) +
                                 Convert.ToDouble(this.HpaFor1.ws_b144) +
                                 Convert.ToDouble(this.HpaFor1.ws_b145) +
                                 Convert.ToDouble(this.HpaFor1.ws_b146) +
                                 Convert.ToDouble(this.HpaFor1.ws_b147) +
                                 Convert.ToDouble(this.HpaFor1.ws_b148) +
                                 Convert.ToDouble(this.HpaFor1.ws_b149) +
                                 Convert.ToDouble(this.HpaFor1.ws_b150) +
                                 Convert.ToDouble(this.HpaFor1.ws_b151) +
                                 Convert.ToDouble(this.HpaFor1.ws_b152) +
                                 Convert.ToDouble(this.HpaFor1.ws_b153) +
                                 Convert.ToDouble(this.HpaFor1.ws_b154) +
                                 Convert.ToDouble(this.HpaFor1.ws_b155) +
                                 Convert.ToDouble(this.HpaFor1.ws_b156) +
                                 Convert.ToDouble(this.HpaFor1.ws_b157) +
                                 Convert.ToDouble(this.HpaFor1.ws_b158) +
                                 Convert.ToDouble(this.HpaFor1.ws_b159) +
                                 Convert.ToDouble(this.HpaFor1.ws_b160) +
                                 Convert.ToDouble(this.HpaFor1.ws_b161)).ToString();
                }
                //Grand Total of Particles
                if (!String.IsNullOrEmpty(lbB79.Text) &&
                   !String.IsNullOrEmpty(lbB88.Text) &&
                   !String.IsNullOrEmpty(lbB114.Text) &&
                   !String.IsNullOrEmpty(lbB132.Text) &&
                   !String.IsNullOrEmpty(lbB162.Text))
                {
                    lbB163.Text = (Convert.ToDouble(lbB79.Text) +
                                    Convert.ToDouble(lbB88.Text) +
                                    Convert.ToDouble(lbB114.Text) +
                                    Convert.ToDouble(lbB132.Text) +
                                    Convert.ToDouble(lbB162.Text)).ToString();
                }

                #endregion
                #region "COLUMN C"
                //=(ROUND((B52/$C$23/$D$23),0))
                if (!String.IsNullOrEmpty(this.HpaFor1.ws_b53) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b54) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b55) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b56) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b57) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b58) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b59) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b60) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b61) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b62) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b63) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b64) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b65) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b66) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b67) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b68) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b69) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b70) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b71) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b72) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b73) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b74) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b75) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b76) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b77) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b78))
                {
                    lbC52.Text = this.HpaFor1.ws_b52 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b52) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC53.Text = this.HpaFor1.ws_b53 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b53) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC54.Text = this.HpaFor1.ws_b54 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b54) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC55.Text = this.HpaFor1.ws_b55 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b55) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC56.Text = this.HpaFor1.ws_b56 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b56) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC57.Text = this.HpaFor1.ws_b57 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b57) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC58.Text = this.HpaFor1.ws_b58 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b58) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC59.Text = this.HpaFor1.ws_b59 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b59) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC60.Text = this.HpaFor1.ws_b60 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b60) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC61.Text = this.HpaFor1.ws_b61 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b61) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC62.Text = this.HpaFor1.ws_b62 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b62) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC63.Text = this.HpaFor1.ws_b63 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b63) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC64.Text = this.HpaFor1.ws_b64 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b64) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC65.Text = this.HpaFor1.ws_b65 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b65) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC66.Text = this.HpaFor1.ws_b66 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b66) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC67.Text = this.HpaFor1.ws_b67 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b67) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC68.Text = this.HpaFor1.ws_b68 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b68) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC69.Text = this.HpaFor1.ws_b69 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b69) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC70.Text = this.HpaFor1.ws_b70 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b70) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC71.Text = this.HpaFor1.ws_b71 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b71) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC72.Text = this.HpaFor1.ws_b72 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b72) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC73.Text = this.HpaFor1.ws_b73 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b73) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC74.Text = this.HpaFor1.ws_b74 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b74) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC75.Text = this.HpaFor1.ws_b75 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b75) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC76.Text = this.HpaFor1.ws_b76 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b76) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC77.Text = this.HpaFor1.ws_b77 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b77) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC78.Text = this.HpaFor1.ws_b78 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b78) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC79.Text = (Convert.ToDouble(lbC53.Text) +
                                 Convert.ToDouble(lbC54.Text) +
                                 Convert.ToDouble(lbC55.Text) +
                                 Convert.ToDouble(lbC56.Text) +
                                 Convert.ToDouble(lbC57.Text) +
                                 Convert.ToDouble(lbC58.Text) +
                                 Convert.ToDouble(lbC59.Text) +
                                 Convert.ToDouble(lbC60.Text) +
                                 Convert.ToDouble(lbC61.Text) +
                                 Convert.ToDouble(lbC62.Text) +
                                 Convert.ToDouble(lbC63.Text) +
                                 Convert.ToDouble(lbC64.Text) +
                                 Convert.ToDouble(lbC65.Text) +
                                 Convert.ToDouble(lbC66.Text) +
                                 Convert.ToDouble(lbC67.Text) +
                                 Convert.ToDouble(lbC68.Text) +
                                 Convert.ToDouble(lbC69.Text) +
                                 Convert.ToDouble(lbC70.Text) +
                                 Convert.ToDouble(lbC71.Text) +
                                 Convert.ToDouble(lbC72.Text) +
                                 Convert.ToDouble(lbC73.Text) +
                                 Convert.ToDouble(lbC74.Text) +
                                 Convert.ToDouble(lbC75.Text) +
                                 Convert.ToDouble(lbC76.Text) +
                                 Convert.ToDouble(lbC77.Text) +
                                 Convert.ToDouble(lbC78.Text)).ToString();
                }
                if (!String.IsNullOrEmpty(this.HpaFor1.ws_b82) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b83) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b84) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b85) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b86) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b87))
                {
                    lbC81.Text = this.HpaFor1.ws_b81;
                    lbC82.Text = this.HpaFor1.ws_b82 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b82) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC83.Text = this.HpaFor1.ws_b83 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b83) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC84.Text = this.HpaFor1.ws_b84 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b84) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC85.Text = this.HpaFor1.ws_b85 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b85) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC86.Text = this.HpaFor1.ws_b86 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b86) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC87.Text = this.HpaFor1.ws_b87 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b87) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    //Subtotal - Magnetic Particles Only
                    lbC88.Text = (Convert.ToDouble(lbC81.Text) +
                                 Convert.ToDouble(lbC82.Text) +
                                 Convert.ToDouble(lbC83.Text) +
                                 Convert.ToDouble(lbC84.Text) +
                                 Convert.ToDouble(lbC85.Text) +
                                 Convert.ToDouble(lbC86.Text) +
                                 Convert.ToDouble(lbC87.Text)).ToString();
                }

                //Subtotal- Hard Particles including Magnetic Partilces
                if (!String.IsNullOrEmpty(lbC79.Text) && !String.IsNullOrEmpty(lbC88.Text))
                {
                    lbC89.Text = (Convert.ToDouble(lbC79.Text) + Convert.ToDouble(lbC88.Text)).ToString();
                }
                if (!String.IsNullOrEmpty(this.HpaFor1.ws_b105) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b106) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b107) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b108) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b109) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b110) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b111) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b112) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b113))
                {
                    lbC105.Text = this.HpaFor1.ws_b105 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b105) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC106.Text = this.HpaFor1.ws_b106 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b106) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC107.Text = this.HpaFor1.ws_b107 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b107) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC108.Text = this.HpaFor1.ws_b108 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b108) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC109.Text = this.HpaFor1.ws_b109 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b109) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC110.Text = this.HpaFor1.ws_b110 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b110) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC111.Text = this.HpaFor1.ws_b111 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b111) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC112.Text = this.HpaFor1.ws_b112 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b112) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC113.Text = this.HpaFor1.ws_b113 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b113) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC114.Text = (Convert.ToDouble(lbC105.Text) +
                                   Convert.ToDouble(lbC106.Text) +
                                   Convert.ToDouble(lbC107.Text) +
                                   Convert.ToDouble(lbC108.Text) +
                                   Convert.ToDouble(lbC109.Text) +
                                   Convert.ToDouble(lbC110.Text) +
                                   Convert.ToDouble(lbC111.Text) +
                                   Convert.ToDouble(lbC112.Text) +
                                   Convert.ToDouble(lbC113.Text)).ToString();
                }
                if (!String.IsNullOrEmpty(this.HpaFor1.ws_b116) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b117) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b118) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b119) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b120) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b121) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b122) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b123) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b124) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b125) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b126) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b127) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b128) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b129) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b130) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b131) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b132) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b133) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b134) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b135) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b136) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b137) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b138) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b139) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b140) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b141) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b142) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b143) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b144) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b145) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b146) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b147) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b148) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b149) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b150) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b151) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b152) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b153) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b154) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b155) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b156) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b157) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b158) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b159) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b160) &&
   !String.IsNullOrEmpty(this.HpaFor1.ws_b161))
                {
                    lbC116.Text = this.HpaFor1.ws_b116 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b116) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC117.Text = this.HpaFor1.ws_b117 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b117) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC118.Text = this.HpaFor1.ws_b118 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b118) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC119.Text = this.HpaFor1.ws_b119 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b119) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC120.Text = this.HpaFor1.ws_b120 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b120) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC121.Text = this.HpaFor1.ws_b121 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b121) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC122.Text = this.HpaFor1.ws_b122 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b122) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC123.Text = this.HpaFor1.ws_b123 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b123) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC124.Text = this.HpaFor1.ws_b124 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b124) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC125.Text = this.HpaFor1.ws_b125 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b125) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC126.Text = this.HpaFor1.ws_b126 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b126) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC127.Text = this.HpaFor1.ws_b127 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b127) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC128.Text = this.HpaFor1.ws_b128 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b128) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC129.Text = this.HpaFor1.ws_b129 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b129) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC130.Text = this.HpaFor1.ws_b130 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b130) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC131.Text = this.HpaFor1.ws_b131 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b131) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC132.Text = this.HpaFor1.ws_b132 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b132) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC133.Text = this.HpaFor1.ws_b133 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b133) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC134.Text = this.HpaFor1.ws_b134 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b134) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC135.Text = this.HpaFor1.ws_b135 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b135) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC136.Text = this.HpaFor1.ws_b136 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b136) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC137.Text = this.HpaFor1.ws_b137 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b137) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC138.Text = this.HpaFor1.ws_b138 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b138) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC139.Text = this.HpaFor1.ws_b139 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b139) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC140.Text = this.HpaFor1.ws_b140 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b140) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC141.Text = this.HpaFor1.ws_b141 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b141) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC142.Text = this.HpaFor1.ws_b142 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b142) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC143.Text = this.HpaFor1.ws_b143 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b143) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC144.Text = this.HpaFor1.ws_b144 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b144) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC145.Text = this.HpaFor1.ws_b145 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b145) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC146.Text = this.HpaFor1.ws_b146 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b146) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC147.Text = this.HpaFor1.ws_b147 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b147) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC148.Text = this.HpaFor1.ws_b148 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b148) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC149.Text = this.HpaFor1.ws_b149 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b149) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC150.Text = this.HpaFor1.ws_b150 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b150) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC151.Text = this.HpaFor1.ws_b151 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b151) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC152.Text = this.HpaFor1.ws_b152 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b152) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC153.Text = this.HpaFor1.ws_b153 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b153) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC154.Text = this.HpaFor1.ws_b154 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b154) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC155.Text = this.HpaFor1.ws_b155 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b155) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC156.Text = this.HpaFor1.ws_b156 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b156) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC157.Text = this.HpaFor1.ws_b157 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b157) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC158.Text = this.HpaFor1.ws_b158 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b158) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC159.Text = this.HpaFor1.ws_b159 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b159) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC160.Text = this.HpaFor1.ws_b160 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b160) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC161.Text = this.HpaFor1.ws_b161 = Math.Round(Convert.ToDouble(this.HpaFor1.ws_b161) / Convert.ToDouble(lbC23.Text) / Convert.ToDouble(lbD23.Text), 0).ToString();
                    lbC162.Text = (Convert.ToDouble(lbC116.Text) +
                                 Convert.ToDouble(lbC117.Text) +
                                 Convert.ToDouble(lbC118.Text) +
                                 Convert.ToDouble(lbC119.Text) +
                                 Convert.ToDouble(lbC120.Text) +
                                 Convert.ToDouble(lbC121.Text) +
                                 Convert.ToDouble(lbC122.Text) +
                                 Convert.ToDouble(lbC123.Text) +
                                 Convert.ToDouble(lbC124.Text) +
                                 Convert.ToDouble(lbC125.Text) +
                                 Convert.ToDouble(lbC126.Text) +
                                 Convert.ToDouble(lbC127.Text) +
                                 Convert.ToDouble(lbC128.Text) +
                                 Convert.ToDouble(lbC129.Text) +
                                 Convert.ToDouble(lbC130.Text) +
                                 Convert.ToDouble(lbC131.Text) +
                                 Convert.ToDouble(lbC133.Text) +
                                 Convert.ToDouble(lbC134.Text) +
                                 Convert.ToDouble(lbC135.Text) +
                                 Convert.ToDouble(lbC136.Text) +
                                 Convert.ToDouble(lbC137.Text) +
                                 Convert.ToDouble(lbC138.Text) +
                                 Convert.ToDouble(lbC139.Text) +
                                 Convert.ToDouble(lbC140.Text) +
                                 Convert.ToDouble(lbC141.Text) +
                                 Convert.ToDouble(lbC142.Text) +
                                 Convert.ToDouble(lbC143.Text) +
                                 Convert.ToDouble(lbC144.Text) +
                                 Convert.ToDouble(lbC145.Text) +
                                 Convert.ToDouble(lbC146.Text) +
                                 Convert.ToDouble(lbC147.Text) +
                                 Convert.ToDouble(lbC148.Text) +
                                 Convert.ToDouble(lbC149.Text) +
                                 Convert.ToDouble(lbC150.Text) +
                                 Convert.ToDouble(lbC151.Text) +
                                 Convert.ToDouble(lbC152.Text) +
                                 Convert.ToDouble(lbC153.Text) +
                                 Convert.ToDouble(lbC154.Text) +
                                 Convert.ToDouble(lbC155.Text) +
                                 Convert.ToDouble(lbC156.Text) +
                                 Convert.ToDouble(lbC157.Text) +
                                 Convert.ToDouble(lbC158.Text) +
                                 Convert.ToDouble(lbC159.Text) +
                                 Convert.ToDouble(lbC160.Text) +
                                 Convert.ToDouble(lbC161.Text)).ToString();
                }
                if (!String.IsNullOrEmpty(lbC79.Text) &&
                    !String.IsNullOrEmpty(lbC88.Text) &&
                    !String.IsNullOrEmpty(lbC114.Text) &&
                    !String.IsNullOrEmpty(lbC132.Text) &&
                    !String.IsNullOrEmpty(lbC162.Text))
                {

                    lbC163.Text = (Convert.ToDouble(lbC79.Text) +
                        Convert.ToDouble(lbC88.Text) +
                        Convert.ToDouble(lbC114.Text) +
                        Convert.ToDouble(lbC132.Text) +
                        Convert.ToDouble(lbC162.Text)).ToString();
                }
                #endregion

                #region "PASS / FAIL"
                lbC28.Text = lbC79.Text;
                lbC29.Text = lbC132.Text;
                lbC30.Text = lbC114.Text;
                lbC31.Text = lbC88.Text;

                //=IF(D28="NA","NA",IF(C28>=INDEX('Detail Spec'!$A$3:$G$238,$F$1,4),"FAIL","PASS"))
                if (detailSpec != null)
                {
                    lbE28.Text = (lbD28.Text.Equals("NA") ? "NA" : (Convert.ToDouble(lbC28.Text) >= Convert.ToDouble(detailSpec.D)) ? "FAIL" : "PASS");
                    lbE29.Text = (lbD29.Text.Equals("NA") ? "NA" : (Convert.ToDouble(lbC29.Text) >= Convert.ToDouble(detailSpec.E)) ? "FAIL" : "PASS");
                    lbE30.Text = (lbD30.Text.Equals("NA") ? "NA" : (Convert.ToDouble(lbC30.Text) >= Convert.ToDouble(detailSpec.F)) ? "FAIL" : "PASS");
                    lbE31.Text = (lbD31.Text.Equals("NA") ? "NA" : (Convert.ToDouble(lbC31.Text) >= Convert.ToDouble(detailSpec.G)) ? "FAIL" : "PASS");
                }
                #endregion

                if (!String.IsNullOrEmpty(this.HpaFor1.img_path))
                {
                    lbImgPath1.Text = String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.HpaFor1.img_path);
                    img1.ImageUrl = lbImgPath1.Text;
                }
                this.CommandName = CommandNameEnum.Edit;
                ShowItem(this.HpaFor1.item_visible);
            }
            else
            {
                this.HpaFor1 = new template_wd_hpa_for1_coverpage();
                this.CommandName = CommandNameEnum.Add;
            }
            #endregion

            //init
            pRemark.Visible = false;
            pDisapprove.Visible = false;
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "JobID");
            Session.Remove(GetType().Name + "job_sample");
            Session.Remove(GetType().Name + "SampleID");
            Session.Remove(GetType().Name + "HpaFor1");
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.CommandName = (prvPage == null) ? this.CommandName : prvPage.CommandName;
            this.JobID = (prvPage == null) ? this.JobID : prvPage.JobID;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void ddlSpecification_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_detailspec_hpa_for1 detailSpec = new tb_detailspec_hpa_for1().SelectByID(int.Parse(ddlSpecification.SelectedValue));
            if (detailSpec != null)
            {
                lbDocNo.Text = detailSpec.B;
                lbComponent.Text = detailSpec.A;
                lbD28.Text = (detailSpec.D.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.D)));
                lbD29.Text = (detailSpec.E.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.E)));
                lbD30.Text = (detailSpec.F.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.F)));
                lbD31.Text = (detailSpec.G.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.G)));
            }
        }
        protected void ddlComponent_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_component_hpa_for1 comp = new tb_component_hpa_for1().SelectByID(int.Parse(ddlComponent.SelectedValue));
            if (comp != null)
            {
                lbA23.Text = comp.B;
                lbB23.Text = comp.G;
                lbC23.Text = comp.C;
                lbA34.Text = comp.G;
                lbA48.Text = comp.G;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            Boolean isValid = true;

            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                    this.jobSample.step1owner = userLogin.id;

                    this.HpaFor1.sample_id = this.SampleID;
                    this.HpaFor1.detail_spec_id = Convert.ToInt32(ddlSpecification.SelectedValue);
                    this.HpaFor1.component_id = Convert.ToInt32(ddlComponent.SelectedValue);
                    this.HpaFor1.item_visible = getItemStatus();
                    //insert filed
                    this.HpaFor1.ws_b52 = lbB52.Text;
                    this.HpaFor1.ws_b53 = lbB53.Text;
                    this.HpaFor1.ws_b54 = lbB54.Text;
                    this.HpaFor1.ws_b55 = lbB55.Text;
                    this.HpaFor1.ws_b56 = lbB56.Text;
                    this.HpaFor1.ws_b57 = lbB57.Text;
                    this.HpaFor1.ws_b58 = lbB58.Text;
                    this.HpaFor1.ws_b59 = lbB59.Text;
                    this.HpaFor1.ws_b60 = lbB60.Text;
                    this.HpaFor1.ws_b61 = lbB61.Text;
                    this.HpaFor1.ws_b62 = lbB62.Text;
                    this.HpaFor1.ws_b63 = lbB63.Text;
                    this.HpaFor1.ws_b64 = lbB64.Text;
                    this.HpaFor1.ws_b65 = lbB65.Text;
                    this.HpaFor1.ws_b66 = lbB66.Text;
                    this.HpaFor1.ws_b67 = lbB67.Text;
                    this.HpaFor1.ws_b68 = lbB68.Text;
                    this.HpaFor1.ws_b69 = lbB69.Text;
                    this.HpaFor1.ws_b70 = lbB70.Text;
                    this.HpaFor1.ws_b71 = lbB71.Text;
                    this.HpaFor1.ws_b72 = lbB72.Text;
                    this.HpaFor1.ws_b73 = lbB73.Text;
                    this.HpaFor1.ws_b74 = lbB74.Text;
                    this.HpaFor1.ws_b75 = lbB75.Text;
                    this.HpaFor1.ws_b76 = lbB76.Text;
                    this.HpaFor1.ws_b77 = lbB77.Text;
                    this.HpaFor1.ws_b78 = lbB78.Text;
                    //this.HpaFor1.ws_b79 = lbB79.Text;
                    this.HpaFor1.ws_b81 = lbB81.Text;
                    this.HpaFor1.ws_b82 = lbB82.Text;
                    this.HpaFor1.ws_b83 = lbB83.Text;
                    this.HpaFor1.ws_b84 = lbB84.Text;
                    this.HpaFor1.ws_b85 = lbB85.Text;
                    this.HpaFor1.ws_b86 = lbB86.Text;
                    this.HpaFor1.ws_b87 = lbB87.Text;
                    this.HpaFor1.ws_b88 = lbB88.Text;
                    this.HpaFor1.ws_b89 = lbB89.Text;
                    this.HpaFor1.ws_b105 = lbB105.Text;
                    this.HpaFor1.ws_b106 = lbB106.Text;
                    this.HpaFor1.ws_b107 = lbB107.Text;
                    this.HpaFor1.ws_b108 = lbB108.Text;
                    this.HpaFor1.ws_b109 = lbB109.Text;
                    this.HpaFor1.ws_b110 = lbB110.Text;
                    this.HpaFor1.ws_b111 = lbB111.Text;
                    this.HpaFor1.ws_b112 = lbB112.Text;
                    this.HpaFor1.ws_b113 = lbB113.Text;
                    this.HpaFor1.ws_b114 = lbB114.Text;
                    this.HpaFor1.ws_b116 = lbB116.Text;
                    this.HpaFor1.ws_b117 = lbB117.Text;
                    this.HpaFor1.ws_b118 = lbB118.Text;
                    this.HpaFor1.ws_b119 = lbB119.Text;
                    this.HpaFor1.ws_b120 = lbB120.Text;
                    this.HpaFor1.ws_b121 = lbB121.Text;
                    this.HpaFor1.ws_b122 = lbB122.Text;
                    this.HpaFor1.ws_b123 = lbB123.Text;
                    this.HpaFor1.ws_b124 = lbB124.Text;
                    this.HpaFor1.ws_b125 = lbB125.Text;
                    this.HpaFor1.ws_b126 = lbB126.Text;
                    this.HpaFor1.ws_b127 = lbB127.Text;
                    this.HpaFor1.ws_b128 = lbB128.Text;
                    this.HpaFor1.ws_b129 = lbB129.Text;
                    this.HpaFor1.ws_b130 = lbB130.Text;
                    this.HpaFor1.ws_b131 = lbB131.Text;
                    this.HpaFor1.ws_b132 = lbB132.Text;
                    this.HpaFor1.ws_b133 = lbB133.Text;
                    this.HpaFor1.ws_b134 = lbB134.Text;
                    this.HpaFor1.ws_b135 = lbB135.Text;
                    this.HpaFor1.ws_b136 = lbB136.Text;
                    this.HpaFor1.ws_b137 = lbB137.Text;
                    this.HpaFor1.ws_b138 = lbB138.Text;
                    this.HpaFor1.ws_b139 = lbB139.Text;
                    this.HpaFor1.ws_b140 = lbB140.Text;
                    this.HpaFor1.ws_b141 = lbB141.Text;
                    this.HpaFor1.ws_b142 = lbB142.Text;
                    this.HpaFor1.ws_b143 = lbB143.Text;
                    this.HpaFor1.ws_b144 = lbB144.Text;
                    this.HpaFor1.ws_b145 = lbB145.Text;
                    this.HpaFor1.ws_b146 = lbB146.Text;
                    this.HpaFor1.ws_b147 = lbB147.Text;
                    this.HpaFor1.ws_b148 = lbB148.Text;
                    this.HpaFor1.ws_b149 = lbB149.Text;
                    this.HpaFor1.ws_b150 = lbB150.Text;
                    this.HpaFor1.ws_b151 = lbB151.Text;
                    this.HpaFor1.ws_b152 = lbB152.Text;
                    this.HpaFor1.ws_b153 = lbB153.Text;
                    this.HpaFor1.ws_b154 = lbB154.Text;
                    this.HpaFor1.ws_b155 = lbB155.Text;
                    this.HpaFor1.ws_b156 = lbB156.Text;
                    this.HpaFor1.ws_b157 = lbB157.Text;
                    this.HpaFor1.ws_b158 = lbB158.Text;
                    this.HpaFor1.ws_b159 = lbB159.Text;
                    this.HpaFor1.ws_b160 = lbB160.Text;
                    this.HpaFor1.ws_b161 = lbB161.Text;
                    this.HpaFor1.ws_b162 = lbB162.Text;
                    this.HpaFor1.ws_b163 = lbB163.Text;

                    switch (this.CommandName)
                    {
                        case CommandNameEnum.Add:
                            this.HpaFor1.Insert();
                            break;
                        case CommandNameEnum.Edit:
                            this.HpaFor1.Update();
                            break;
                    }

                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                    StatusEnum srChemistApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (srChemistApproveStatus)
                    {
                        case StatusEnum.SR_CHEMIST_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_WORD);
                            break;
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                            break;
                    }
                    this.jobSample.step3owner = userLogin.id;
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                    StatusEnum labApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (labApproveStatus)
                    {
                        case StatusEnum.LABMANAGER_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_PDF);
                            break;
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(ddlAssignTo.SelectedValue);
                            break;
                    }
                    this.jobSample.step5owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".doc") || Path.GetExtension(btnUpload.FileName).Equals(".docx")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_word = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.LABMANAGER_CHECKING);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .doc|.docx";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step4owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".pdf")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_pdf = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.JOB_COMPLETE);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .pdf";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step6owner = userLogin.id;
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    this.jobSample.step2owner = userLogin.id;
                    break;
            }
            //Stamp date
            job_sample_logs tmp = new job_sample_logs
            {
                ID = 0,
                job_sample_id = this.jobSample.ID,
                job_status = Convert.ToInt32(this.jobSample.job_status),
                job_remark = txtRemark.Text,
                get_alerts = "0",
                date = DateTime.Now
            };
            tmp.Insert();

            this.jobSample.Update();
            //Commit
            GeneralManager.Commit();
            //Return to main.
            if (isValid)
            {
                Response.Redirect(this.PreviousPath);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void lbDownload_Click(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                case StatusEnum.SR_CHEMIST_APPROVE:
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    else
                    {
                        downloadWord();
                    }
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                case StatusEnum.LABMANAGER_APPROVE:
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (!String.IsNullOrEmpty(this.jobSample.path_pdf))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_pdf));
                    }
                    else
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
            }


        }

        private void downloadWord()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string strFileName = string.Format("{0}_{1}.doc", lbRefNo.Text.Replace("-", "_"), DateTime.Now.ToString("yyyyMMddhhmmss"));

            HttpContext.Current.Response.ContentType = "application/vnd.ms-word";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

            StringWriter sw = new StringWriter();
            HtmlTextWriter h = new HtmlTextWriter(sw);
            invDiv.RenderControl(h);
            string strHTMLContent = sw.GetStringBuilder().ToString();
            String html = "<html><header><style>body {max-width: 800px;margin:initial;font-family: \'Arial Unicode MS\';font-size: 10px;}table {border-collapse: collapse;}th {background: #666;color: #fff;border: 1px solid #999;padding: 0.5rem;text-align: center;}td { border: 1px solid #999;padding: 0.5rem;text-align: left;}h6 {font-weight:initial;}</style></header><body>" + strHTMLContent + "</body></html>";


            HttpContext.Current.Response.Write(html);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush();
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue.ToString(), true);
            switch (status)
            {
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = false;
                    break;
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = true;
                    break;
                default:
                    pRemark.Visible = false;
                    pDisapprove.Visible = false;
                    break;
            }

        }

        private String getItemStatus()
        {
            String result = String.Empty;
            result = ((CheckBox1.Checked) ? "1" : "0") +
                        ((CheckBox2.Checked) ? "1" : "0") +
                        ((CheckBox3.Checked) ? "1" : "0") +
                        ((CheckBox4.Checked) ? "1" : "0")+
                        ((CheckBox5.Checked) ? "1" : "0");
            return result;
        }

        private void ShowItem(String _itemVisible)
        {
            if (_itemVisible != null)
            {
                char[] item = _itemVisible.ToCharArray();
                if (item.Length == 5)
                {
                    StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
                    switch (status)
                    {
                        case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                            break;
                        case StatusEnum.LOGIN_SELECT_SPEC:
                            CheckBox1.Checked = item[0] == '1' ? true : false;
                            CheckBox2.Checked = item[1] == '1' ? true : false;
                            CheckBox3.Checked = item[2] == '1' ? true : false;
                            CheckBox4.Checked = item[3] == '1' ? true : false;
                            CheckBox5.Checked = item[4] == '1' ? true : false;
                            txtCVP_D23.Visible = true;
                            lbD23.Visible = false;
                            break;
                        case StatusEnum.CHEMIST_TESTING:
                        case StatusEnum.SR_CHEMIST_CHECKING:
                        case StatusEnum.SR_CHEMIST_APPROVE:
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_WORD:
                        case StatusEnum.LABMANAGER_CHECKING:
                        case StatusEnum.LABMANAGER_APPROVE:
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_PDF:
                            tb1.Visible = item[0] == '1' ? true : false;
                            tr1.Visible = item[1] == '1' ? true : false;
                            tr2.Visible = item[2] == '1' ? true : false;
                            tr3.Visible = item[3] == '1' ? true : false;
                            tr4.Visible = item[4] == '1' ? true : false;
                            CheckBox1.Visible = false;
                            CheckBox2.Visible = false;
                            CheckBox3.Visible = false;
                            CheckBox4.Visible = false;
                            CheckBox5.Visible = false;
                            txtCVP_D23.Visible = false;
                            lbD23.Visible = true;
                            break;
                    }
                }
            }

        }



    }
}