﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="JobWorkFlow.aspx.cs" Inherits="ALS.ALSI.Web.view.request.JobWorkFlow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            App.init();

            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index) {
                    return false;
                },
                onNext: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;

                    // set wizard title
                    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#form_wizard_1')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {

                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#form_wizard_1').find('.button-previous').hide();
                    } else {
                        $('#form_wizard_1').find('.button-previous').show();
                    }

                    if (current >= total) {
                        $('#form_wizard_1').find('.button-next').hide();
                        $('#form_wizard_1').find('.button-submit').show();
                    } else {
                        $('#form_wizard_1').find('.button-next').show();
                        $('#form_wizard_1').find('.button-submit').hide();
                    }
                    App.scrollTo($('.page-title'));
                },
                onPrevious: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    // set wizard title
                    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#form_wizard_1')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#form_wizard_1').find('.button-previous').hide();
                    } else {
                        $('#form_wizard_1').find('.button-previous').show();
                    }

                    if (current >= total) {
                        $('#form_wizard_1').find('.button-next').hide();
                        $('#form_wizard_1').find('.button-submit').show();
                    } else {
                        $('#form_wizard_1').find('.button-next').show();
                        $('#form_wizard_1').find('.button-submit').hide();
                    }

                    App.scrollTo($('.page-title'));
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.bar').css({
                        width: $percent + '%'
                    });
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box blue" id="form_wizard_1">
                <div class="portlet-title">
                    <h4>
                        <i class="icon-reorder"></i>Work Flow - <span class="step-title">Step <asp:Label ID="lbWorkFlowStep" runat="server" Text="1"></asp:Label> of 6</span>
                    </h4>
                    <div class="tools hidden-phone">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-wizard">
                        <div class="navbar steps">
                            <div class="navbar-inner">
                                <ul class="row-fluid">
                                    <li class="span2<%=Step1Status%>">
                                        
                                        <a href="#tab1" data-toggle="tab" class="step">
                                            <span class="number">1</span>
                                            <span class="desc"><i class="icon-ok"></i>Login <asp:Label ID="lbStep1UseDate" runat="server" Text=""></asp:Label></span>
                                        </a>
                                    </li>
                                    <li class="span2<%=Step2Status%>">
                                        <a href="#tab2" data-toggle="tab" class="step">
                                            <span class="number">2</span>
                                            <span class="desc"><i class="icon-ok"></i>Chemist <asp:Label ID="lbStep2UseDate" runat="server" Text=""></asp:Label></span>
                                        </a>
                                    </li>
                                    <li class="span2<%=Step3Status%>">
                                        <a href="#tab3" data-toggle="tab" class="step">
                                            <span class="number">3</span>
                                            <span class="desc"><i class="icon-ok"></i>Sr.Chemist <asp:Label ID="lbStep3UseDate" runat="server" Text=""></asp:Label></span>
                                        </a>
                                    </li>
                                    <li class="span2<%=Step4Status%>">
                                        <a href="#tab4" data-toggle="tab" class="step">
                                            <span class="number">4</span>
                                            <span class="desc"><i class="icon-ok"></i>Admin <asp:Label ID="lbStep4UseDate" runat="server" Text=""></asp:Label></span>
                                        </a>
                                    </li>
                                    <li class="span2<%=Step5Status%>">
                                        <a href="#tab5" data-toggle="tab" class="step">
                                            <span class="number">5</span>
                                            <span class="desc"><i class="icon-ok"></i>Lab Manager <asp:Label ID="lbStep5UseDate" runat="server" Text=""></asp:Label></span>
                                        </a>
                                    </li>
                                    <li class="span2<%=Step6Status%>">
                                        <a href="#tab6" data-toggle="tab" class="step">
                                            <span class="number">6</span>
                                            <span class="desc"><i class="icon-ok"></i>Admin <asp:Label ID="lbStep6UseDate" runat="server" Text=""></asp:Label></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="bar" class="progress progress-success progress-striped">
                            <div class="bar"></div>
                        </div>
                        <asp:PlaceHolder runat="server" ID="plhCoverPage" />
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1" runat="server">
                                <!-- COVER PAGE -->
                            </div>
                            <div class="tab-pane" id="tab2">
                                <!-- WORKING -->
                            </div>
                            <div class="tab-pane" id="tab3">
                                <!-- SR CHEMIST -->
                            </div>
                            <div class="tab-pane" id="tab4">
                                <!-- ADMIN CONVERT WORD-->
                            </div>
                            <div class="tab-pane" id="tab5">
                                <!-- LABMANAGER -->
                            </div>
                            <div class="tab-pane" id="tab6">
                                <!-- ADMIN CONVERT PDF -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

