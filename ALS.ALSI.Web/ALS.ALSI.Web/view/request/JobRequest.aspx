﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="JobRequest.aspx.cs" Inherits="ALS.ALSI.Web.view.request.JobRequest" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.date-picker').datepicker();
            //App.setPage("form_request_validate");  // set current page
            App.init(); // init the rest of plugins and elements

            var form1 = $('#Form1');
            var error1 = $('.alert-error', form1);
            var error2 = $('.alert-error2', form1);
            var success1 = $('.alert-success', form1);

            $("#btnAddSampleInfo").click(function () {
                var _txtJob_number = $.trim($("#txtJob_number").val());
                var _ddlSecification_id = $('#ddlSecification_id :selected').val();
                var _lstTypeOfTest = $.trim($("#lstTypeOfTest").val());
                var _txtDescriptoin = $.trim($("#txtDescriptoin").val());
                var _txtModel = $.trim($("#txtModel").val());
                var _txtSurfaceArea = $.trim($("#txtSurfaceArea").val());
                var _ddlNoOfReport = $('#ddlNoOfReport :selected').val();

                if (_txtJob_number == "") {
                    $("#txtJob_number").closest('.control-group').removeClass('success').addClass('error');
                    $("#txtJob_number").focus();
                    return false;
                } else {
                    $("#txtJob_number").closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                }

                if (_ddlSecification_id == "") {
                    $("#ddlSecification_id").closest('.control-group').removeClass('success').addClass('error');
                    $("#ddlSecification_id").focus();
                    return false;
                } else {
                    $("#ddlSecification_id").closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                }

                if (_lstTypeOfTest == "") {
                    $("#lstTypeOfTest").closest('.control-group').removeClass('success').addClass('error');
                    $("#lstTypeOfTest").focus();
                    return false;
                } else {
                    $("#lstTypeOfTest").closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                }

                if (_txtDescriptoin == "") {
                    $("#txtDescriptoin").closest('.control-group').removeClass('success').addClass('error');
                    $("#txtDescriptoin").focus();
                    return false;
                } else {
                    $("#txtDescriptoin").closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                }

                if (_txtModel == "") {
                    $("#txtModel").closest('.control-group').removeClass('success').addClass('error');
                    $("#txtModel").focus();
                    return false;
                } else {
                    $("#txtModel").closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                }

                if (_txtSurfaceArea == "") {
                    $("#txtSurfaceArea").closest('.control-group').removeClass('success').addClass('error');
                    $("#txtSurfaceArea").focus();
                    return false;
                } else {
                    $("#txtSurfaceArea").closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                }

                if (_ddlNoOfReport == "") {
                    $("#ddlNoOfReport").closest('.control-group').removeClass('success').addClass('error');
                    $("#ddlNoOfReport").focus();
                    return false;
                } else {
                    $("#ddlNoOfReport").closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                }
            });


            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    ctl00$ContentPlaceHolder2$txtDateOfRequest: {
                        required: true,
                    },
                    ctl00$ContentPlaceHolder2$txtCompanyName: {
                        minlength: 2,
                        required: true
                    },
                    ctl00$ContentPlaceHolder2$ddlContract_person_id: {
                        required: true,
                    },

                    ctl00$ContentPlaceHolder2$ddlJobNumber: {
                        required: true,
                    },
                    ctl00$ContentPlaceHolder2$txtDate_of_receive: {
                        required: true,
                    },
                    ctl00$ContentPlaceHolder2$btnAddSampleInfo: {
                        required: true,
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },


                submitHandler: function (form) {
                    var rowCount = $('#gvSample tr').length;
                    if (rowCount >= 2) {
                        error1.hide();
                        error2.hide();
                        form.submit();
                    } else {
                        error2.show();
                    }

                }
            });
        });


    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">


    <form id="Form1" method="post" runat="server" class="form-horizontal">
        <div class="alert alert-error hide">
            <button class="close" data-dismiss="alert"></button>
            You have some form errors. Please check below.
        </div>
        <div class="alert alert-success hide">
            <button class="close" data-dismiss="alert"></button>
            Your form validation is successful!
        </div>

        <asp:HiddenField ID="hJobID" Value="0" runat="server" />
        <div class="row-fluid">
            <div class="span12">
                <div class="tabbable tabbable-custom boxless">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Analysis
						Requistion Form</a>
                        </li>
                        <li><a href="#tab_2" data-toggle="tab">Convert Template</a>
                        </li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_1">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <h4>
                                        <i class="icon-reorder"></i>ANALYSIS REQUISITION FORM
                                    </h4>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->

                                    <h3 class="form-section">Customer Information</h3>
                                    <div class="row-fluid">

                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="DateOfReceive">
                                                    Date of
												Request:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <div class="input-prepend">
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                        <asp:TextBox ID="txtDateOfRequest" name="txtDateOfRequest" runat="server" class="m-wrap span12 date-picker" ValidationGroup="GJobInfo"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <label class="control-label" for="ContractPerson">
                                                Contract
											Person:<span class="required">*</span></label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddlContract_person_id" runat="server" class="span12 chosen" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <!--/span-->

                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="CompanyName">Company Name:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtCompanyName" runat="server" class="m-wrap span6" ReadOnly="true"></asp:TextBox>
                                                    <a href="#myModal1" id="aModal" role="button" class="btn btn-primary" data-toggle="modal">...</a>


                                                    <div id="myModal1" class="modal hide fade" tabindex="-1"
                                                        role="dialog" aria-labelledby="myModalLabel1"
                                                        aria-hidden="true">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">
                                                            </button>
                                                            <h3 id="myModalLabel1">Search Company</h3>
                                                        </div>
                                                        <div class="modal-body">

                                                            <asp:HiddenField ID="hCompanyId" runat="server" />
                                                            <asp:DropDownList ID="ddlCustomer_id" runat="server" class="span6" DataTextField="company_name" DataValueField="ID" OnSelectedIndexChanged="ddlCustomer_id_SelectedIndexChanged"></asp:DropDownList>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <asp:LinkButton ID="btnSelectCompany" class="btn green" runat="server" OnClick="btnSelectCompany_Click"> Select <i class="icon-check"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="Department">Department:</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtDepartment" runat="server" class="m-wrap span12" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="TelNumber">Tel. Number:</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtTelNumber" runat="server" class="m-wrap span12" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row-fluid">
                                        <!--/span-->
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="Email">Email:</label>
                                                <div class="controls">
                                                    <div class="input-prepend">
                                                        <span class="add-on">@</span>
                                                        <asp:TextBox ID="txtEmail" runat="server" class="m-wrap span12" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="Fax">Fax Number:</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtFax" runat="server" class="m-wrap span12" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>


                                        <!--/span-->
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="control-group">
                                                <label class="control-label" for="Address">Address:</label>
                                                <div class="controls">
                                                    <textarea id="txtAddress" class="m-wrap span12" runat="server" readonly="readonly"></textarea>

                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="CustomerRefNo">
                                                    Customer
												Ref. No.:</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtCustomer_ref_no" runat="server" class="m-wrap span12"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="control-group">
                                                <label class="control-label" for="CompanyNametoStateInReport">
                                                    Company
												Name to State in Report:</label>
                                                <div class="controls">
                                                    <textarea id="txtCompany_name_to_state_in_report" class="m-wrap span12" runat="server"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Sample Infomation:</h3>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="JobNumber">Job Number:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlJobNumber" runat="server" class="span3 chosen" DataTextField="prefix" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="ddlJobNumber_SelectedIndexChanged"></asp:DropDownList>
                                                    &nbsp;<asp:TextBox ID="txtJob_number" name="txtJob_number" runat="server" class="m-wrap span6" ReadOnly="true"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="DateOfReceive">
                                                    Date of
												Receive:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <div class="input-prepend">
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                        <asp:TextBox ID="txtDate_of_receive" runat="server" class="m-wrap span12 date-picker"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="JobNumber">S'pore Ref No:</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtS_pore_ref_no" runat="server" class="m-wrap span12"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="JobNumber">Spec. Ref. & Rev. No. :</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtSpecRefRevNo" runat="server" class="m-wrap span12"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="form-section">Sample Requisition:</h5>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Sample Disposition:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdSample_dipositionYes" GroupName="Sample_diposition" runat="server" Checked="true" />Discard
                                                    </label>
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdSample_dipositionNo" GroupName="Sample_diposition" runat="server" />Return all
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Specification:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlSecification_id" runat="server" class="span12 chosen" DataTextField="name" DataValueField="ID" OnSelectedIndexChanged="ddlSecification_id_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">
                                                    <asp:Label ID="lbOther" runat="server" Text="Other:"></asp:Label></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtSpecification_other" runat="server" class="m-wrap span12"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Type Of Test:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <asp:ListBox ID="lstTypeOfTest" runat="server" DataTextField="customName" DataValueField="ID" SelectionMode="Multiple" class="chosen span12"></asp:ListBox>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Sample Description:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtDescriptoin" CssClass="m-wrap span12" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Model:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtModel" CssClass="m-wrap span12" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Surface Area:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtSurfaceArea" CssClass="m-wrap span12" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Remark:</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtRemark" CssClass="m-wrap span12" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">No Of Report:<span class="required">*</span></label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlNoOfReport" runat="server" class="span12 chosen"></asp:DropDownList>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Uncertainty:</label>
                                                <div class="controls">
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdUncertaintyYes" GroupName="rdUncertainty" runat="server" />Y
                                                    </label>
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdUncertaintyNo" GroupName="rdUncertainty" runat="server" Checked="true" />N
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12 ">

                                            <div class="portlet-body">
                                                <div class="clearfix">
                                                    <div class="btn-group">
                                                        <div class="control-group">
                                                            <label class="control-label" for="DateOfReceive"></label>
                                                            <asp:LinkButton ID="btnAddSampleInfo" class="btn green" runat="server" OnClick="btnAddSampleInfo_Click"> Add Sample <i class="icon-plus"></i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="alert alert-error2 hide">
                                                    <button class="close" data-dismiss="alert"></button>
                                                    You have some form errors. Please add sample
                                                </div>
                                                <div class="alert alert-error3 hide">
                                                    <button class="close" data-dismiss="alert"></button>
                                                    You have some form errors. Please select job prefix
                                                </div>
                                                <asp:GridView ID="gvSample" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                                    CssClass="table table-striped table-hover table-bordered" ShowHeaderWhenEmpty="True" DataKeyNames="ID,job_id,job_number,no_of_report,uncertainty" OnRowCancelingEdit="gvSample_RowCancelingEdit" OnRowDataBound="gvSample_RowDataBound" OnRowDeleting="gvSample_RowDeleting" OnRowEditing="gvSample_RowEditing" OnRowUpdating="gvSample_RowUpdating" OnSelectedIndexChanging="gvSample_SelectedIndexChanging" OnPageIndexChanging="gvSample_PageIndexChanging">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Ref No." ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="litRefNo" runat="server" Text='<%# Eval("job_number")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtRefNo" CssClass="m-wrap small" runat="server" Text='<%# Eval("job_number")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Sample Description (Part description, Part no. etc.)" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="litDescriptoin" runat="server" Text='<%# Eval("description")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtDescriptoin" CssClass="m-wrap large" runat="server" Text='<%# Eval("description")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="model" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="litModel" runat="server" Text='<%# Eval("model")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtModel" CssClass="m-wrap small" runat="server" Text='<%# Eval("model")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Surface Area" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="litSurfaceArea" runat="server" Text='<%# Eval("surface_area")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtSurfaceArea" CssClass="m-wrap small" runat="server" Text='<%# Eval("surface_area")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Remark" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="litRemark" runat="server" Text='<%# Eval("remarks")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtRemark" CssClass="m-wrap small" runat="server" Text='<%# Eval("remarks")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No.of Report:" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="litNoOfReport" runat="server" Text='<%# Eval("no_of_report")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlNoOfReport" runat="server" CssClass="m-wrap small"></asp:DropDownList>
                                                                <%--                                                                <asp:TextBox ID="txtNoOfReport" CssClass="m-wrap small" runat="server" Text='<%# Eval("no_of_report")%>'></asp:TextBox>--%>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="UNCERTAINTY" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="litUncertainty" runat="server" Text='<%# Eval("uncertainty")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlUncertaint" runat="server" CssClass="m-wrap small"></asp:DropDownList>
                                                                <%--<asp:TextBox ID="txtUncertainty" CssClass="m-wrap small" runat="server" Text='<%# Eval("uncertainty")%>'></asp:TextBox>--%>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnEdit" runat="server" ToolTip="Edit" CommandName="Edit" CommandArgument='<%# Eval("ID")%>'><i class="icon-edit"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnDelete" runat="server" ToolTip="Delete" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                                    CommandArgument='<%# Eval("ID")%>'><i class="icon-trash"></i></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="btnUpdate" runat="server" ToolTip="Update" ValidationGroup="CreditLineGrid"
                                                                    CommandName="Update"><i class="icon-save"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="LinkCancel" runat="server" ToolTip="Cancel" CausesValidation="false"
                                                                    CommandName="Cancel"><i class="icon-remove"></i></asp:LinkButton>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerTemplate>
                                                        <div class="pagination">
                                                            <ul>
                                                                <li>
                                                                    <asp:LinkButton ID="btnFirst" runat="server" CommandName="Page" CommandArgument="First"
                                                                        CausesValidation="false" ToolTip="First Page"><i class="icon-fast-backward"></i></asp:LinkButton>
                                                                </li>
                                                                <li>
                                                                    <asp:LinkButton ID="btnPrev" runat="server" CommandName="Page" CommandArgument="Prev"
                                                                        CausesValidation="false" ToolTip="Previous Page"><i class="icon-backward"></i> Prev</asp:LinkButton>
                                                                </li>
                                                                <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                                                                <li>
                                                                    <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" CommandArgument="Next"
                                                                        CausesValidation="false" ToolTip="Next Page">Next <i class="icon-forward"></i></asp:LinkButton>
                                                                </li>
                                                                <li>
                                                                    <asp:LinkButton ID="btnLast" runat="server" CommandName="Page" CommandArgument="Last"
                                                                        CausesValidation="false" ToolTip="Last Page"><i class="icon-fast-forward"></i></asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </PagerTemplate>
                                                    <EmptyDataTemplate>
                                                        <div class="data-not-found">
                                                            <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                                                        </div>
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>

                                        </div>
                                    </div>


                                    <h3 class="form-section">Internal Use Only</h3>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">ตัวอย่าง</label>
                                                <div class="controls">
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdSample_enoughYes" GroupName="Sample_enough" runat="server" Checked="true" />เพียงพอ
                                                    </label>
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdSample_enoughNo" GroupName="Sample_enough" runat="server" />ไม่เพียงพอ
                                                    </label>
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdSample_fullYes" GroupName="Sample_full" runat="server" Checked="true" />สมบูรณ์
                                                    </label>
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdSample_fullNo" GroupName="Sample_full" runat="server" />ไม่สมบูรณ์
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">บุคลากรและปริมาณงาน</label>
                                                <div class="controls">
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdPersonel_and_workloadYes" GroupName="Personel_and_workload" runat="server" Checked="true" />พร้อม
                                                    </label>
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdPersonel_and_workloadNo" GroupName="Personel_and_workload" runat="server" />ไม่พร้อม
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">เครื่องมือทดสอบ</label>
                                                <div class="controls">
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdTest_toolYes" GroupName="Test_tool" runat="server" Checked="true" />พร้อม
                                                    </label>
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdTest_toolNo" GroupName="Test_tool" runat="server" />ไม่พร้อม
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">วิธีทดสอบ</label>
                                                <div class="controls">
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdTest_methodYes" GroupName="Test_method" runat="server" Checked="true" />พร้อม
                                                    </label>
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdTest_methodNo" GroupName="Test_method" runat="server" />ไม่พร้อม
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">กำหนดเสร็จ</label>
                                                <div class="controls">
                                                    <asp:RadioButtonList ID="rdCompletion_scheduledNormal" DataTextField="name" DataValueField="id" runat="server" CssClass="m-wrap span12" RepeatDirection="Horizontal" RepeatLayout="Table"></asp:RadioButtonList>

                                                    <%--                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdCompletion_scheduledNormal" GroupName="Completion_scheduled" runat="server" Checked="true" />Normal
                                                    </label>
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdCompletion_scheduledUrgent" GroupName="Completion_scheduled" runat="server" />Urgent
                                                    </label>
                                                    <label class="radio">
                                                        <asp:RadioButton ID="rdCompletion_scheduledExpress" GroupName="Completion_scheduled" runat="server" />Express
                                                    </label>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--/row-->
                                    <div class="form-actions">

                                        <asp:Button ID="btnSave" runat="server" class="btn green" Text="Save" OnClick="btnSave_Click" ValidationGroup="GJobInfo" />
                                        <asp:Button ID="btnR" runat="server" class="btn blue" Text="Retest" ValidationGroup="GJobInfo" OnClick="btnR_Click" />
                                        <asp:Button ID="btnAM" runat="server" class="btn red" Text="Amend" ValidationGroup="GJobInfo" OnClick="btnAM_Click" />
                                        <asp:LinkButton ID="btnCancel" class="btn" runat="server" OnClick="btnCancel_Click"> Cancel <i class="icon-undo"></i></asp:LinkButton>



                                    </div>

                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_2">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <h4>
                                        <i class="icon-reorder"></i>CONVERT TEMPLATE
                                    </h4>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    selected template...
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>


    </form>
</asp:Content>
