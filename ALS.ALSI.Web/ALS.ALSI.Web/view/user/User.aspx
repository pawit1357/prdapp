﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="ALS.ALSI.Web.view.user.User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            App.init();

            var form1 = $('#Form1');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    ctl00$ContentPlaceHolder2$ddlRole: {
                        required: true,
                    },
                    ctl00$ContentPlaceHolder2$txtUser: {
                        minlength: 2,
                        required: true,
                    },
                    ctl00$ContentPlaceHolder2$txtEmail: {
                        minlength: 2,
                        required: true,
                    },
                    ctl00$ContentPlaceHolder2$txtPassword: {
                        minlength: 2,
                        required: true,
                    },
                    ctl00$ContentPlaceHolder2$ddlTitle: {
                        required: true,
                    },
                    ctl00$ContentPlaceHolder2$txtFirstName: {
                        minlength: 2,
                        required: true,
                    },
                    ctl00$ContentPlaceHolder2$txtLastName: {
                        minlength: 2,
                        required: true,
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },


                submitHandler: function (form) {
                    form.submit();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <form id="Form1" method="post" runat="server" class="form-horizontal">

        <div class="alert alert-error hide">
            <button class="close" data-dismiss="alert"></button>
            You have some form errors. Please check below.
        </div>
        <div class="alert alert-success hide">
            <button class="close" data-dismiss="alert"></button>
            Your form validation is successful!
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <h4><i class="icon-bar-chart"></i>
                            <asp:Label ID="lbCommandName" runat="server" Text=""></asp:Label>&nbsp;User</h4>
                    </div>
                    <br />
                    <h3 class="form-section">&nbsp;User Information</h3>
                    <div class="row-fluid">

                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="ddlRole">Role:<span class="required">*</span></label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlRole" runat="server" class="span8 chosen" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="txtUser">User:<span class="required">*</span></label>
                                <div class="controls">
                                    <asp:TextBox ID="txtUser" runat="server" class="m-wrap span8"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="txtEmail">Email:<span class="required">*</span></label>
                                <div class="controls">
                                    <div class="input-prepend">
                                        <span class="add-on">@</span>
                                        <asp:TextBox ID="txtEmail" runat="server" class="m-wrap span12"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="txtPassword">Password:<span class="required">*</span></label>
                                <div class="controls">
                                    <asp:TextBox ID="txtPassword" runat="server" class="m-wrap span8" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                        </div>




                    </div>
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                    <label class="radio">
                                        <asp:RadioButton ID="rdStatusA" GroupName="Status" runat="server" Checked="true" />Active
                                    </label>
                                    <label class="radio">
                                        <asp:RadioButton ID="rdStatusI" GroupName="Status" runat="server" />InAvtive
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label">Responsible</label>
                                <div class="controls">
                                    <asp:ListBox ID="lstTypeOfTest" DataTextField="Id" DataValueField="Name" runat="server" SelectionMode="Multiple" class="chosen span12"></asp:ListBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h3 class="form-section">&nbsp;Personal Information</h3>
                    <div class="row-fluid">

                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="ddlTitle">Title:<span class="required">*</span></label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlTitle" runat="server" class="span8" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">

                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="txtFirstName">First Name:<span class="required">*</span></label>
                                <div class="controls">
                                    <asp:TextBox ID="txtFirstName" runat="server" class="m-wrap span8"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="txtLastName">Last Name<span class="required">*</span></label>

                                <div class="controls">
                                    <asp:TextBox ID="txtLastName" runat="server" class="m-wrap span8"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <asp:Button ID="btnSave" runat="server" class="btn green" Text="Save" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" class="cancel btn blue" Text="Cancel" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </div>

        </div>
    </form>
</asp:Content>
