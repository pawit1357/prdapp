﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="ALS.ALSI.Web.ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title><%=ALS.ALSI.Biz.Constant.Configuration.AppTitle %></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<%= ResolveClientUrl("~/css/bootstrap.min.css") %> " rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/css/metro.css") %> " rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/css/font-awesome.css") %> " rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/css/style.css") %> " rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/css/style_responsive.css") %> " rel="stylesheet" />
    <%--<link href="<%= ResolveClientUrl("~/css/style_default.css") %> " rel="stylesheet" />--%>
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo">
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->

        <!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form id="Form1" class="form-vertical" runat="server">

            <h3 class="">Forget Password ?</h3>
                        <%=Message %>
            <p>Enter your e-mail address below to reset your password.</p>
            <div class="control-group">
                <div class="controls">
                    <div class="input-icon left">
                        <i class="icon-envelope"></i>
                        <asp:TextBox ID="txtEmail" runat="server" class="m-wrap placeholder-no-fix"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <asp:LinkButton ID="btnBack" runat="server" class="btn" OnClick="btnBack_Click">Back <i class="m-icon-swapleft"></i></asp:LinkButton>
                <asp:LinkButton ID="btnSubmit" runat="server" class="btn green pull-right" OnClick="btnSubmit_Click">Submit <i class="m-icon-swapright m-icon-white"></i></asp:LinkButton>
            </div>
        </form>

    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
        <%=ALS.ALSI.Biz.Constant.Configuration.CompanyName %>
    </div>
    <!-- END COPYRIGHT -->
    <!-- BEGIN JAVASCRIPTS -->


    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <%--    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/bootstrap.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery.blockui.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery.cookie.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery.uniform.min.js") %>"></script>--%>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery.validate.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/app.js") %>"></script>

    <script>
        jQuery(document).ready(function () {

        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

