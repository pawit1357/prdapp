﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DPUWebPhet10.Models;

namespace DPUWebPhet10.Controllers
{
    public class RoomController : Controller
    {
        private ChinaPhet10Entities db = new ChinaPhet10Entities();

        //
        // GET: /Room/

        public ActionResult Index()
        {
            var tb_room = db.TB_ROOM.Include("TB_M_BUILDING").Include("TB_M_LEVEL");
            return View(tb_room.ToList());
        }

        //
        // GET: /Room/Details/5

        public ActionResult Details(decimal id = 0)
        {
            TB_ROOM tb_room = db.TB_ROOM.Single(t => t.ROOM_ID == id);
            if (tb_room == null)
            {
                return HttpNotFound();
            }
            return View(tb_room);
        }

        //
        // GET: /Room/Create

        public ActionResult Create()
        {
            ViewBag.ROOM_BUILD = new SelectList(db.TB_M_BUILDING, "BUILDING_ID", "BUILDING_NAME");
            ViewBag.ROOM_FOR_LEVEL = new SelectList(db.TB_M_LEVEL, "LEVEL_ID", "LEVEL_NAME_TH");
            return View();
        }

        //
        // POST: /Room/Create

        [HttpPost]
        public ActionResult Create(TB_ROOM tb_room)
        {
            if (ModelState.IsValid)
            {
                tb_room.ROOM_FLOOR = 0;
                db.TB_ROOM.AddObject(tb_room);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ROOM_BUILD = new SelectList(db.TB_M_BUILDING, "BUILDING_ID", "BUILDING_NAME", tb_room.ROOM_BUILD);
            ViewBag.ROOM_FOR_LEVEL = new SelectList(db.TB_M_LEVEL, "LEVEL_ID", "LEVEL_NAME_TH", tb_room.ROOM_FOR_LEVEL);
            return View(tb_room);
        }

        //
        // GET: /Room/Edit/5

        public ActionResult Edit(decimal id = 0)
        {
            TB_ROOM tb_room = db.TB_ROOM.Single(t => t.ROOM_ID == id);
            if (tb_room == null)
            {
                return HttpNotFound();
            }
            ViewBag.ROOM_BUILD = new SelectList(db.TB_M_BUILDING, "BUILDING_ID", "BUILDING_NAME", tb_room.ROOM_BUILD);
            ViewBag.ROOM_FOR_LEVEL = new SelectList(db.TB_M_LEVEL, "LEVEL_ID", "LEVEL_NAME_TH", tb_room.ROOM_FOR_LEVEL);
            return View(tb_room);
        }

        //
        // POST: /Room/Edit/5

        [HttpPost]
        public ActionResult Edit(TB_ROOM tb_room)
        {
            if (ModelState.IsValid)
            {
                tb_room.ROOM_FLOOR = 0;
                db.TB_ROOM.Attach(tb_room);
                db.ObjectStateManager.ChangeObjectState(tb_room, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ROOM_BUILD = new SelectList(db.TB_M_BUILDING, "BUILDING_ID", "BUILDING_NAME", tb_room.ROOM_BUILD);
            ViewBag.ROOM_FOR_LEVEL = new SelectList(db.TB_M_LEVEL, "LEVEL_ID", "LEVEL_NAME_TH", tb_room.ROOM_FOR_LEVEL);
            return View(tb_room);
        }

        //
        // GET: /Room/Delete/5

        public ActionResult Delete(decimal id = 0)
        {
            TB_ROOM tb_room = db.TB_ROOM.Single(t => t.ROOM_ID == id);
            if (tb_room == null)
            {
                return HttpNotFound();
            }
            return View(tb_room);
        }

        //
        // POST: /Room/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(decimal id)
        {
            TB_ROOM tb_room = db.TB_ROOM.Single(t => t.ROOM_ID == id);
            db.TB_ROOM.DeleteObject(tb_room);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}