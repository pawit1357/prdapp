﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DPUWebPhet10.Models;
using DPUWebPhet10.Models.Common;
using System.Web.Security;

namespace DPUWebPhet10.Controllers
{
    public class SchoolController : Controller
    {
        private ChinaPhet10Entities db = new ChinaPhet10Entities();
        IRepository repository = new DPUPhet10Repository();

        private IFileStore _fileStore = new DiskFileStore();
        //
        // GET: /School/
        public ActionResult Index()
        {
            if (Session["Phet10School"] == null)
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("../");
            }
            TB_APPLICATION_SCHOOL school = (TB_APPLICATION_SCHOOL)Session["Phet10School"];

            var tb_application_school = from a in db.TB_APPLICATION_SCHOOL where a.SCHOOL_ID == school.SCHOOL_ID  select a;

            //a.TB_M_STATUS.STATUS_NAME
            ViewBag.PageContent = "ตรวจสอบผลการสมัคร";

            return View(tb_application_school.ToList());
        }

        //
        // GET: /School/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (Session["Phet10School"] != null)
            {
                TB_APPLICATION_SCHOOL school = (TB_APPLICATION_SCHOOL)Session["Phet10School"];
                id = Convert.ToInt32(school.SCHOOL_ID);
            }
            TB_APPLICATION_SCHOOL tb_application_school = db.TB_APPLICATION_SCHOOL.Single(t => t.SCHOOL_ID == id);
            if (tb_application_school == null)
            {
                return HttpNotFound();
            }
            ViewBag.SCHOOL_ADDR_PROVINCE = new SelectList(db.TB_M_PROVINCE, "PROVINCE_ID", "PROVINCE_NAME", tb_application_school.SCHOOL_ADDR_PROVINCE);
            ViewBag.SCHOOL_ADDR_TOMBON = new SelectList(db.TB_M_DISTRICT, "DISTRICT_ID", "DISTRICT_NAME", tb_application_school.SCHOOL_ADDR_TOMBON);
            ViewBag.SCHOOL_ADDR_AMPHUR = new SelectList(db.TB_M_AMPHUR, "AMPHUR_ID", "AMPHUR_NAME", tb_application_school.SCHOOL_ADDR_AMPHUR);

            /*
             * SCHOOL TYPE
             */
            List<RadioButtonModel> list = new List<RadioButtonModel>();
            list.Add(new RadioButtonModel() { ID = 1, Name = Resources.Application.Application.SCHOOL_TYPE_01 });//สพฐ
            list.Add(new RadioButtonModel() { ID = 2, Name = Resources.Application.Application.SCHOOL_TYPE_02 });//เอกชน
            list.Add(new RadioButtonModel() { ID = 3, Name = Resources.Application.Application.SCHOOL_TYPE_03 });//กทม
            list.Add(new RadioButtonModel() { ID = 4, Name = Resources.Application.Application.SCHOOL_TYPE_04 });//อุดมศึกษา
            list.Add(new RadioButtonModel() { ID = 5, Name = Resources.Application.Application.SCHOOL_TYPE_OTHER });//อื่น ๆ 

            SelectList schoolTypes = new SelectList(list, "ID", "Name", tb_application_school.SCHOOL_TYPE);

            ViewBag.SCHOOL_TYPE = schoolTypes;
            ViewBag.SCHOOL_PROVINCE = new SelectList(db.TB_M_PROVINCE, "PROVINCE_ID", "PROVINCE_NAME", tb_application_school.SCHOOL_PROVINCE);

            ViewBag.PrintComfirmDoc = false;
            return View(tb_application_school);
        }

        //
        // POST: /School/Edit/5

        [HttpPost]
        public ActionResult Edit(TB_APPLICATION_SCHOOL tb_application_school)
        {
            if (ModelState.IsValid)
            {
                db.TB_APPLICATION_SCHOOL.Attach(tb_application_school);
                db.ObjectStateManager.ChangeObjectState(tb_application_school, System.Data.EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tb_application_school);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}