<?php

class Media extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'tb_media';
	}

	public function relations()
	{
		return array(

		);
	}

	public function rules() {
		return array(
				array('id,user_id,catagories,media_key, media_title, media_realurl,media_type', 'safe'),
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}

	public function getUrl($post=null)
	{
		if($post===null)
			$post=$this->post;
		return $post->url.'#c'.$this->id;
	}

	protected function beforeSave()
	{
		return true;
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		if(UserLoginUtil::getUserAppId() == 0) {
		}else {
			$criteria->addCondition(" user_id=".UserLoginUtil::getUsersId());
		}
		return new CActiveDataProvider(get_class($this), array(
				'criteria' => $criteria,
				'sort' => array(
						'defaultOrder' => 't.id',
				),
				'pagination' => array(
						'pageSize' => 15//ConfigUtil::getDefaultPageSize()
				),
		));
	}
}