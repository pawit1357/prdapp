﻿<div class="full_w">
	<div class="h_title">Management-Icon</div>
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'my-model-grid',
			'dataProvider' => $data->search(),
			'htmlOptions' => array('style' => 'width: 500px;'),
			'ajaxUpdate'=>true,
			'columns' => array(
					array(
							'header'=>'#',
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
							'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
					),
					array(
							'name'=>'icon',
							'type'=>'raw',
							'value'=>'CHtml::image(Yii::app()->request->baseUrl."/".$data->icon_path,
							"",
							array(\'width\'=>16, \'height\'=>16))',
							'htmlOptions'=>array('width'=>'20%', 'align'=>'center'),
					),					
					array(            // display a column with "view", "update" and "delete" buttons
							'class'=>'CButtonColumn',
							'template'=>'{delete}',
							'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
							'buttons'=>array
							(
							),
					),
			),
	));
	?>
	<div class="entry">
		<div class="sep"></div>
		<?php echo CHtml::link('Add New',array('AppIcon/create'), array('class'=>'button add'));?>
	</div>
</div>

<div class="clear"></div>
