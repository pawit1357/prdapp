﻿
<script type="text/javascript">
$(function(){
	$('#users-form').submit(function(){
		return (validateForm() && confirm('Confirm ?'));
		});
});
function validateForm(){

	var active = $('#active :selected').val();
	if(active==0){
		$('#txtactive').html('<b style="color:red">*Status invalid</b>');
		isResult = false;
		return false;
	}else{
			$('#txtactive').html('');
	}
}
</script>

<div class="full_w">
	<div class="h_title">Management-Create-Banner</div>
	<?php 
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'users-form',
			'enableAjaxValidation' => true,
			'htmlOptions'=>array('enctype' => 'multipart/form-data')
	));
	?>

<!-- 
	<div class="element">
		<label for="name">Menu <span class="red">(required)</span>
		</label>
		<?//php 
		//$appMenus = AppMenu::model()->findAll(array('condition'=>" app_id=".UserLoginUtil::getUserAppId().' and menu_type=0'));

		?>
		<select id="menu_item" name="AppBanner[menu_id]">
			<?//php foreach($appMenus as $am) {?>
			<option value="<?//php echo $am->id?>" selected><?//php echo $am->menu_item?></option>
			<?//php }?>
		</select>

	</div>
 -->
	<div class="element">
		<label for="name">Image url. <b style="color: red">*</b>
		</label>
		<?php echo $form->textField(AppBanner::model(), 'image_path1', array('size' => 100, 'maxlength' => 255)); ?>
		<?//php echo $form->hiddenField(AppBanner::model(), 'image_path1', array('size' => 50, 'maxlength' => 255)); ?>

		<?/* $this->widget('ext.EAjaxUpload.EAjaxUpload',
				array(
			        'id'=>'image_path1',
			        'config'=>array(
			               'action'=>Yii::app()->createUrl('AppBanner/upload'),
			               'allowedExtensions'=>array("jpg","png"),//array("jpg","jpeg","gif","exe","mov" and etc...
			               'sizeLimit'=>5*1024*1024,// maximum file size in bytes
// 			              'minSizeLimit'=>0.5*1024*1024,// minimum file size in bytes
			              'onComplete'=>"js:function(id, fileName, responseJSON){ $('#AppBanner_image_path1').val(fileName); }",
			               //'messages'=>array(
			               //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
			               //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
			               //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
			               //                  'emptyError'=>"{file} is empty, please select files again without it.",
			               //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
			               //                 ),
			               //'showMessage'=>"js:function(message){ alert(message); }"
			              )
					)); */?>
	</div>
	<!--
	<div class="element">
		<label for="name">IPad Image (Size 1536*2048) <b style="color: red">*Allow
				Only *.jpg,*.png</b>
		</label>
		<?//php echo $form->hiddenField(AppBanner::model(), 'image_path2', array('size' => 50, 'maxlength' => 255)); ?>

		<?
		/*
		 $this->widget('ext.EAjaxUpload.EAjaxUpload',
				array(
			        'id'=>'image_path2',
			        'config'=>array(
			               'action'=>Yii::app()->createUrl('AppBanner/upload'),
			               'allowedExtensions'=>array("jpg","png"),//array("jpg","jpeg","gif","exe","mov" and etc...
			               'sizeLimit'=>5*1024*1024,// maximum file size in bytes
// 			              'minSizeLimit'=>0.5*1024*1024,// minimum file size in bytes
			              'onComplete'=>"js:function(id, fileName, responseJSON){ $('#AppBanner_image_path2').val(fileName); }",
			               //'messages'=>array(
			               //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
			               //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
			               //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
			               //                  'emptyError'=>"{file} is empty, please select files again without it.",
			               //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
			               //                 ),
			               //'showMessage'=>"js:function(message){ alert(message); }"
			              )
					)); */?>
	</div>
	-->
	<div class="element">
		<label for="name">Status<span class="red">(required)</span>
		</label> <select id="active" name="AppBanner[status]">
			<option value="">--Select--</option>
			<option value="A">ACTIVE</option>
			<option value="I">INACTIVE</option>
		</select>
		<div id="txtactive"></div>
	</div>


	<div class="entry">
		<!-- 			<button type="submit">Preview</button> -->
		<button type="submit" class="add">Save</button>
		<button type="reset" class="cancel"
			onClick="javascript:history.back();">Cancel</button>
	</div>
	<?php $this->endWidget(); ?>
</div>
