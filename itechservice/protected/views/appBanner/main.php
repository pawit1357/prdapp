﻿<div class="full_w">
	<div class="h_title">Management-Banner</div>
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'my-model-grid',
			
			'dataProvider' => $data->search(),
			'htmlOptions' => array('style' => 'width: 700px;'),
			'ajaxUpdate'=>true,
			'columns' => array(
					array(
							'header'=>'#',
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
							//'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
					),
					array(
							'name'=>'iPhone image',
							'type'=>'raw',
							'value'=>'CHtml::image($data->image_path1,
							"",
							array(\'width\'=>350, \'height\'=>62))',
							//'htmlOptions'=>array('width'=>'50%', 'align'=>'left'),
					),		
					array(
							'name'=>'status',
							//'htmlOptions'=>array('width'=>'10%', 'align'=>'left'),
					),
					array(            // display a column with "view", "update" and "delete" buttons
							'class'=>'CButtonColumn',
							'template'=>'{update} {delete}',
							//'htmlOptions'=>array('width'=>'35%', 'align'=>'left'),
							'buttons'=>array
							(
							),
					),
			),
	));
	?>
	<div class="entry">
		<div class="sep"></div>
		<?php echo CHtml::link('Add',array('appBanner/create'), array('class'=>'button add'));?>
	</div>
</div>

<div class="clear"></div>
