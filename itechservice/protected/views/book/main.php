﻿<div class="full_w">
	<div class="h_title">Management-Book</div>
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'my-model-grid',
			'dataProvider' => $data->search(),
			'htmlOptions' => array('style' => 'width: 700px;'),
			'ajaxUpdate'=>true,
			'columns' => array(
					array(
							'header'=>'#',
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
							'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
					),
					array(
							'name'=>'Cover',
							'type'=>'raw',
							'value'=>'CHtml::image($data->book_cover1,
							"",
							array(\'width\'=>62, \'height\'=>62))',
							'htmlOptions'=>array('width'=>'20%', 'align'=>'center'),
					),					
					array(
							'name'=>'book_name',
							'htmlOptions'=>array('width'=>'15%'),
					),
					array(
							'name'=>'callNo',
							'htmlOptions'=>array('width'=>'5%'),
					),
					array(
							'name'=>'program',
							'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
					),
					array(
							'name'=>'status',
							'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
					),
					array(            // display a column with "view", "update" and "delete" buttons
							'class'=>'CButtonColumn',
							'template'=>'{update} {delete}',
							'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
							'buttons'=>array
							(
							),
					),
			),
	));
	?>
	<div class="entry">
		<div class="sep"></div>
		<?php echo CHtml::link('Add New',array('book/create'), array('class'=>'button add'));?>
		|
		<?php echo CHtml::link('Import',array('book/importBook'), array('class'=>'button add'));?>
	</div>
</div>

<div class="clear"></div>
