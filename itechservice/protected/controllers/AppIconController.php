<?php

/**
 * Default controller to handle user requests.
 */
class AppIconController extends CController
{
	public $layout='main';
	private $_model;

	public function actionIndex()
	{
		$model = new AppIcon();
		$this->render('main', array(
				'data' => $model,
		));
	}

	public function actionCreate()
	{
		$now = new DateTime();
		$uploadFolder='upload/'.DateTimeUtil::getCurdateYYYYMMDD();// folder for uploaded files
		$model = new AppIcon();

		if(isset($_POST['AppIcon'])){

			$model->attributes = $_POST['AppIcon'];
			$model->create_date = new CDbExpression('NOW()');
			$model->app_id = UserLoginUtil::getUserAppId();

			if(!CommonUtil::IsNullOrEmptyString($model->icon_path)){

				$destSrcPath = 'images/icon_app_menu/'.$model->app_id.'/ico_'.DateTimeUtil::getCurdateYYYYMMDDHHMMSS().'.'.pathinfo($model->icon_path,PATHINFO_EXTENSION);

				if(!file_exists($destSrcPath))
				{
					rename($uploadFolder.'/'.$model->icon_path,$destSrcPath);
				}
				/* Apply new value to model */
				$model->icon_path = $destSrcPath;

				if($model->save()){
					$this->redirect(Yii::app()->createUrl('AppIcon/'));
				}
			}
		}else
		{
			$this->render('create');
		}		
	}


	public function actionDelete()
	{
		$model = $this->loadModel();
		if($model->delete()){
			
			
			$this->redirect(Yii::app()->createUrl('AppIcon/'));
		}

		$this->render('main', array(
				'data' => $model,
		));
	}

	public function actionUpload()
	{

		Yii::import("ext.EAjaxUpload.qqFileUploader");

		$folder='/upload/'.DateTimeUtil::getCurdateYYYYMMDD().'/';// folder for uploaded files

		if (!is_dir($folder)) {
			mkdir($folder,0777,TRUE);
		}

		$allowedExtensions = array("jpg","png","xls");//array("jpg","jpeg","gif","exe","mov" and etc...
		$sizeLimit = 5 * 1024 * 1024;// maximum file size in bytes
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload($folder);
		$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		$fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
		$fileName=$result['filename'];//GETTING FILE NAME

		echo $return;// it's array
	}


	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=AppIcon::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
}