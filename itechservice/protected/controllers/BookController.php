<?php

/**
 * Default controller to handle user requests.
 */
class BookController extends CController
{
	public $layout='main';
	private $_model;

	public function actionIndex()
	{
		$model = new Book();
		$this->render('main', array(
				'data' => $model,
		));

	}

	public function actionCreate()
	{
		$now = new DateTime();
		$uploadFolder='upload/'.DateTimeUtil::getCurdateYYYYMMDD();// folder for uploaded files

		if(isset($_POST['Book'])){
			$model = new Book();
			$model->attributes = $_POST['Book'];
			$model->create_date = new CDbExpression('NOW()');
			$model->isChange = 1;
			if($model->save()){
				$this->redirect(Yii::app()->createUrl('Book/'));
			}
		}
		$this->render('create');
	}


	public function actionUpdate()
	{
		$uploadFolder='upload/'.DateTimeUtil::getCurdateYYYYMMDD();// folder for uploaded files
		$model = $this->loadModel();
		if(isset($_POST['Book'])){
			$model->attributes = $_POST['Book'];
			$model->create_date = new CDbExpression('NOW()');
			$model->isChange = 1;
			if($model->update()){
				$this->redirect(Yii::app()->createUrl('Book/'));
			}
		}
		$this->render('update', array(
				'model' => $model,
		));
	}

	public function actionView()
	{
		$model = $this->loadModel();
		$this->render('view', array(
				'model' => $model,
		));
	}

	public function actionimportBook()
	{
		$uploadFolder='upload/'.DateTimeUtil::getCurdateYYYYMMDD();// folder for uploaded files

		if(isset($_POST['Book'])){
			$model = new Book();
			$model->attributes = $_POST['Book'];
			if(!file_exists($model->book_cover1)){
				$destSrcPath = $uploadFolder.'/4_'.DateTimeUtil::getCurdateYYYYMMDDHHMMSS().'.'.pathinfo($model->book_cover1,PATHINFO_EXTENSION);
				/* Rename */
				rename($uploadFolder.'/'.$model->book_cover1,$destSrcPath);

				if(FeedUtil::libMangazine($destSrcPath)){

					$this->redirect(Yii::app()->createUrl('Book/'));
				}
			}
		}
		$this->render('importBook', array(
				'model' => $model,
		));
	}

	public function actionDelete()
	{
		$model = new Book();
		$model->isChange = 1;
		$model = $this->loadModel();
		if($model->delete()){
			$this->redirect(Yii::app()->createUrl('Book/'));
		}

		$this->render('main', array(
				'data' => $model,
		));
	}

	public function actionUpload()
	{

		Yii::import("ext.EAjaxUpload.qqFileUploader");

		$folder='upload/'.DateTimeUtil::getCurdateYYYYMMDD().'/';// folder for uploaded files

		if (!is_dir($folder)) {
			mkdir($folder,0777,TRUE);
		}

		$allowedExtensions = array("jpg","png","xls");//array("jpg","jpeg","gif","exe","mov" and etc...
		$sizeLimit = 5 * 1024 * 1024;// maximum file size in bytes
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload($folder);
		$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		$fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
		$fileName=$result['filename'];//GETTING FILE NAME

		echo $return;// it's array
	}


	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Book::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
}