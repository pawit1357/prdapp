<?php
error_reporting(E_ALL);
/**
 * Default controller to handle user requests.
 */
class PublishController extends CController
{
	public $layout='main';
	private $_model;

	public function actionIndex()
	{
		$model = new AppStore();
		$this->render('main', array(
				'data' => $model,
		));
	}

	public function actionGeneratePackage()
	{
		$app_id = $_GET['app_id'];
		if(XMLUtil::updatePackageVersion($app_id))
		{
			//APNSUtil::sendPushnotification($app_id,'','MUIC already update content! ');
		}
/*
		if(!CommonUtil::IsNullOrEmptyString($app_id)){
			$uResult = false;

// 			XMLUtil::deleteTmp($app_id,1);
// 			XMLUtil::deleteTmp($app_id,2);


			if(XMLUtil::createPackage($app_id,1) && XMLUtil::createPackage($app_id,2)){
// 				$uResult = XMLUtil::createZip($app_id,1);
// 				$uResult = XMLUtil::createZip($app_id,2);

				if($uResult)
				{
					//Update Package version
// 					if(XMLUtil::updatePackageVersion($app_id))
// 					{
// 						APNSUtil::sendPushnotification($app_id,'','MUIC App update version! ');
// 					}
				}
			}

		}else
		{
			$this->redirect(Yii::app()->createUrl('Pushlish/'));
		}
		*/
		$this->render('result');
	}

	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=AppStore::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
}