<div class="row-fluid">
	<div class="span12">
		<div class="portlet">
			<div class="portlet-title">
				<h4>
					<i class="icon-bell"></i>Advance Table
				</h4>
				<div class="actions">
					<a href="<?php echo Yii::app()->createUrl('Requisition/')?>"
						class="btn blue"><i class="icon-pencil"></i> Add</a>
					<div class="btn-group">
						<a class="btn green" href="#" data-toggle="dropdown"> <i
							class="icon-cogs"></i> Tools <i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu pull-right">
							<li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
							<li><a href="#"><i class="icon-trash"></i> Delete</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="pull-right">
					Filter: <select data-placeholder="Select Category" class="chosen"
						tabindex="-1" id="inputCategory">
						<option value="0"></option>
						<option value="1">All</option>
						<option value="1">Category 1</option>
						<option value="1">Category 2</option>
						<option value="1">Category 3</option>
						<option value="1">Category 4</option>
					</select> <select data-placeholder="Sort By"
						class="chosen input-small" tabindex="-1" id="inputSort">
						<option value="0"></option>
						<option value="1">Date</option>
						<option value="1">Author</option>
						<option value="1">Title</option>
						<option value="1">Hits</option>
					</select>
				</div>
				<table
					class="table table-striped table-bordered table-advance table-hover">
					<thead>
						<tr>
							<th>Date</th>
							<th>Ref No.</th>
							<th>PO Ref</th>
							<th>Comp</th>
							<th>Contact</th>
							<th>Description</th>
							<th>Model</th>
							<th>Surface Area</th>
							<th>Remarks</th>
							<th>Specification</th>
							<th>Type of test</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($x = 0; $x <= 2; $x++) {?>
						<tr>
							<td class="highlight"><div class="success"></div> 2-Sep-13</td>
							<td>ELP-2897-M</td>
							<td></td>
							<td>Eco Green Solutions Co., Ltd</td>
							<td>Patipat</td>
							<td>Greenex HCR</td>
							<td>PCCA,2H, Brink TI</td>
							<td>31.24 sq.cm (2side)</td>
							<td>Thu(1) WW9 FY14</td>
							<td>Seagate</td>
							<td>HPA Filtration</td>
							<td><a href="#modalSelectTemplate" role="button"
								class="btn mini purple" data-toggle="modal"><i class="icon-cogs"></i>
									...</a> <a
								href="<?php echo Yii::app()->createUrl('Process/')?>"
								role="button" class="btn mini purple" data-toggle="modal"><i
									class="icon-beaker"></i> ...</a>
								<div id="modalSelectTemplate" class="modal hide fade"
									tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
									aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-hidden="true"></button>
										<h3 id="myModalLabel1">Select Template</h3>
									</div>
									<div class="modal-body">
										<select class="span12 chosen"
											data-placeholder="Choose a Company" tabindex="1">
											<option value=""></option>
											<option value="D">WD-DHS Component 1.5.xlt</option>
											<option value="R">WD-GCMS 1.8.xlt</option>
										</select>
									</div>
									<div class="modal-footer">
										<a href="<?php echo Yii::app()->createUrl('Process/')?>"
											role="button" class="btn yellow"><i class="icon-tasks"></i>
											Convert</a>

									</div>
								</div>
							</td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
			App.init(); // init the rest of plugins and elements
	});
</script>
