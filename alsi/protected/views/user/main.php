<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4>
					<i class="icon-user"></i>Managed User
				</h4>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a> <a
						href="#portlet-config" data-toggle="modal" class="config"></a> <a
						href="javascript:;" class="reload"></a> <a href="javascript:;"
						class="remove"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="clearfix">
					<div class="btn-group">
						<a href="<?php echo Yii::app()->createUrl('User/Create')?>"
							class="btn green"><i class="icon-plus"></i> Add</a>
							
						<!-- 						<button id="sample_editable_1_new" class="btn green"> -->
						<!-- 							Add New <i class="icon-plus"></i> -->
						<!-- 						</button> -->
					</div>
					<!-- 					<div class="btn-group pull-right"> -->
					<!-- 						<button class="btn dropdown-toggle" data-toggle="dropdown"> -->
					<!-- 							Tools <i class="icon-angle-down"></i> -->
					<!-- 						</button> -->
					<!-- 						<ul class="dropdown-menu"> -->
					<!-- 							<li><a href="#">Print</a></li> -->
					<!-- 							<li><a href="#">Save as PDF</a></li> -->
					<!-- 							<li><a href="#">Export to Excel</a></li> -->
					<!-- 						</ul> -->
					<!-- 					</div> -->
				</div>
				<table class="table table-striped table-bordered table-hover"
					id="sample_1">
					<thead>
						<tr>
							<!--							<th style="width: 8px;"><input type="checkbox"
 								class="group-checkable" data-set="#sample_1 .checkboxes" /></th> -->
							<th>#</th>
							<th class="hidden-480">Role</th>
							<th class="hidden-480">First Name</th>
							<th class="hidden-480">Phone</th>
							<th class="hidden-480">Email</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr class="odd gradeX">
							<!-- 							<td><input type="checkbox" class="checkboxes" value="1" /></td> -->
							<td>1.</td>
							<td class="hidden-480">Login</td>
							<td class="hidden-480">Pawit</td>
							<td class="center hidden-480">0850788702</td>
							<td class="hidden-480"><a href="mailto:shuxer@gmail.com">shuxer@gmail.com</a>
							</td>
							<td><a href="#" class="btn mini blue"><i class="icon-search"></i>
									View</a> <a href="#" class="btn mini purple"><i
									class="icon-edit"></i> Edit</a> <a href="#"
								class="btn mini black"><i class="icon-trash"></i> Delete</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
