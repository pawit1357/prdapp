<div class="row-fluid">

	<div class="span12">
		<div class="span3">
			<ul class="ver-inline-menu tabbable margin-bottom-10">
				<li class="active"><a data-toggle="tab" href="#tab_1-1"> <i
						class="icon-user"></i> User info
				</a> <span class="after"></span></li>
				<li class=""><a data-toggle="tab" href="#tab_2-2"><i
						class="icon-cog"></i> Personal info</a></li>
				<li class=""><a data-toggle="tab" href="#tab_3-3"><i
						class="icon-lock"></i> Change Password</a></li>
			</ul>
		</div>
		<div class="span9">
			<form action="#">
				<div class="tab-content">
					<div id="tab_1-1" class="tab-pane active">
						<div style="height: auto;" id="accordion1-1"
							class="accordion collapse">

							<label class="control-label">Role</label> <select
								class="span4 chosen" name="role_id"
								data-placeholder="Choose a Role" tabindex="1">
								<option value="-- SELECT --"></option>
								<option value="1">Admin</option>
								<option value="2">Chemist</option>
								<option value="3">Sr.Chemist</option>
								<option value="4">LabManager</option>
								<option value="5">Manager</option>
							</select> <label class="control-label">Username</label> <input
								type="text" placeholder="usrxxxx" class="m-wrap span8"
								name="username" /> <label class="control-label">Password</label>
							<input type="password" placeholder="password"
								class="m-wrap span8" name="password" /> <label
								class="control-label">Comfirm Password</label> <input
								type="password" placeholder="confirm password"
								class="m-wrap span8" /> <label class="control-label">Email</label>
							<div class="input-prepend">
								<span class="add-on">@</span><input type="text" name="email"
									class="m-wrap" placeholder="Email Address">
							</div>
						</div>
					</div>
					<div id="tab_2-2" class="tab-pane">
						<div style="height: auto;" id="accordion2-2"
							class="accordion collapse">

							<label class="control-label">Gender</label> <select
								class="span4 " name="personal_title"
								data-placeholder="Choose a Role" tabindex="1">
								<option value="-- SELECT --"></option>
								<option value="M">Male</option>
								<option value="F">Female</option>
							</select> 
						<label class="control-label">ID card</label> <input
								type="text" placeholder="John" class="m-wrap span4" /> <label
								class="control-label">Last Name</label> <input type="text"
								placeholder="ID card" class="m-wrap span4" name="personal_id" /> <label
								class="control-label">Birth Day</label>
							<label class="control-label">First Name</label> <input
								type="text" placeholder="John" class="m-wrap span4" /> <label
								class="control-label">Last Name</label> <input type="text"
								placeholder="Doe" class="m-wrap span4" /> <label
								class="control-label">Birth Day</label>
							<div class="input-prepend">
								<span class="add-on"><i class="icon-calendar"></i> </span> <input
									class="m-wrap m-ctrl-medium date-picker" size="16" type="text"
									id="birth_date" name="birth_date"
									value="<?php echo date('Y-m-d')?>" placeholder="yyyy-MM-dd" />
							</div>

							<label class="control-label">Phone</label> <input type="text"
								placeholder="+XX-XXXX-XXXX-X" class="m-wrap span4" /> <label
								class="control-label">Address</label>
							<textarea rows="4" cols="1" id="address" name="address"
								class="m-wrap span6"></textarea>
						</div>
					</div>
					<div id="tab_3-3" class="tab-pane">
						<div style="height: auto;" id="accordion3-3"
							class="accordion collapse">

							<label class="control-label">Current Password</label> <input
								type="password" class="m-wrap span8" /> <label
								class="control-label">New Password</label> <input
								type="password" class="m-wrap span8" /> <label
								class="control-label">Re-type New Password</label> <input
								type="password" class="m-wrap span8" />

						</div>
					</div>
				</div>
				<div class="span12">
					<div class="submit-btn">
						<a href="#" class="btn green">Save</a> <a href="#" class="btn">Cancel</a>
					</div>
				</div>
			</form>

		</div>
		<!--end span9-->
	</div>

	</form>
</div>

<!-- INCLUDE SCRIPT -->
<script
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.8.3.min.js"></script>
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.uniform.min.js"></script>
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/chosen.jquery.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.date-picker').datepicker();
		App.init(); // init the rest of plugins and elements
	});
</script>
<!-- END INCLUDE SCRIPT -->
