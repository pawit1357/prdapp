
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid invoice">
					<div class="row-fluid invoice-logo">
<!-- 						<div class="span6 invoice-logo-space"><img src="assets/img/invoice/walmart.png" alt="" /> </div> -->
						<div class="span12">
							<p>ALS Testing Services (Thailand) Co., Ltd <span class="muted">The ALS Laboratory Group</span></p>
						</div>
				<div class="pull-right">
				Component:
					<select data-placeholder="Select Category" class="chosen"
						tabindex="-1" id="inputCategory">
						<option value="0"></option>
						<option value="1">All</option>
						
<option value="1">Motor / Base Assembly (3.5 Enterprise)</option>
<option value="1">Motor / Base Assembly (3.5 Desktop)</option>
<option value="1">Motor / Base Assembly (2.5 Enterprise)</option>
<option value="1">Motor / Base Assembly (2.5 Mobile)</option>
<option value="1">Machined / Cast Base - Class 2(3.5 Enterprise)</option>
<option value="1">Machined / Cast Base - Class 2 (3.5 Desktop)</option>
<option value="1">Machined / Cast Base - Class 2 (2.5 Enterprise)</option>
<option value="1">Machined / Cast Base - Class 2 (2.5 Mobile)</option>
<option value="1">Motor Hub - Aluminium - Class 2 (3.5 Enterprise)</option>
<option value="1">Motor Hub - Aluminium - Class 2 (3.5 Desktop)</option>
<option value="1">Motor Hub - Aluminium - Class 2 (2.5 Enterprise)</option>
<option value="1">Motor Hub - Steel - Class 2 (2.5 Mobile)</option>
<option value="1">Motro Stator Assembly and Stator Coil (3.5 Enterprise/Desktop, 2.5 Enterprise/Mobile)</option>
					</select>
				</div>
					</div>
					<hr />
					<div class="row-fluid">
						<div class="span12 invoice-payment">
<!-- 							<h4>Payment Details:</h4> -->
							<ul class="unstyled">
								<li><strong>CUSTOMER PO NO.:</strong> ELP-2995-F</li>
								<li><strong>ALS THAILAND REF NO.:</strong> -</li>
								<li><strong>DATE:</strong> -</li>
								<li><strong>COMPANY:</strong> -</li>
								<li><strong>DATE SAMPLE RECEIVED:</strong> -</li>
								<li><strong>DATE TEST COMPLETED:</strong> -</li>
								<li><strong>SAMPLE DESCRIPTION:</strong> One lot of sample was received with references:</li>
							</ul>
						</div>
					</div>
					
					<div class="row-fluid">
					<h5>METHOD/PROCEDURE:</h5>
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Test</th>
									<th class="hidden-480">Procedure No</th>
									<th class="hidden-480">Number of pieces used for extraction</th>
									<th class="hidden-480">Extraction Medium</th>
									<th>Extraction Volume</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>DHS</td>
									<td class="hidden-480">2092-001026 Rev. AC</td>
									<td class="hidden-480">1</td>
									<td class="hidden-480">Purified Nitrogen</td>
									<td>180mins x 65sccm</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="row-fluid">
						<h6>Results:</h6>
						<h6>The specification is based on Western Digital's document no.96-004575 Rev.AX for Motor / Base Assembly / (Steel hub) (2.5" Enterprise)</h6>	
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Required Test</th>
									<th class="hidden-480">Analytes</th>
									<th class="hidden-480">Specification Limits (ng/part)</th>
									<th class="hidden-480">Results (ng/part)</th>
									<th>PASS / FAIL
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>DHS</td>
									<td class="hidden-480">Total Acrylate and Methacrylate</td>
									<td class="hidden-480">< 5000</td>
									<td class="hidden-480"><input type="text"></td>
									<td>PASS</td>
								</tr>
								<tr>
									<td>2</td>
									<td></td>
									<td class="hidden-480">Total Acrylate
									</td>
									<td class="hidden-480">< 5000</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td></td>
									<td class="hidden-480">Total Methacrylate
									</td>
									<td class="hidden-480">
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td>PASS</td>
								</tr>
								<tr>
									<td>4</td>
									<td></td>
									<td class="hidden-480">Total Siloxane
									</td>
									<td class="hidden-480">< 30</td>
									<td class="hidden-480"><input type="text"></td>
									<td>PASS</td>
								</tr>
								<tr>
									<td>5</td>
									<td></td>
									<td class="hidden-480">Total Silane
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>6</td>
									<td></td>
									<td class="hidden-480">Total Unknown 
									</td>
										<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>7</td>
									<td></td>
									<td class="hidden-480">Total Sulfur Compound
									</td>
										<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>8</td>
									<td></td>
									<td class="hidden-480">Total Phthalate
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>9</td>
									<td></td>
									<td class="hidden-480">Total Phenol
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>10</td>
									<td></td>
									<td class="hidden-480">Total Alcohol
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>11</td>
									<td></td>
									<td class="hidden-480">Total Others
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>12</td>
									<td></td>
									<td class="hidden-480">Total Initiator
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td>PASS</td>
								</tr>
								<tr>
									<td>13</td>
									<td></td>
									<td class="hidden-480">Total Antioxidant
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>14</td>
									<td></td>
									<td class="hidden-480">Total Hydrocarbon
									</td>
										<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>15</td>
									<td></td>
									<td class="hidden-480">Total Aromatic Hydrocarbon
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>16</td>
									<td></td>
									<td class="hidden-480">Total Aliphatic Hydrocarbon
									</td>
										<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>17</td>
									<td></td>
									<td class="hidden-480">Hydrocarbon Hump
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td></td>
								</tr>
								<tr>
									<td>18</td>
									<td></td>
									<td class="hidden-480">Total Outgassing (no of arms: 2)
									</td>
									<td class="hidden-480">< 10000
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td>PASS</td>
								</tr>
								<tr>
									<td>19</td>
									<td></td>
									<td class="hidden-480">Total Outgassing (no of arms: >=4)
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td>NA
									</td>
								</tr>
								<tr>
									<td>20</td>
									<td></td>
									<td class="hidden-480">Total Outgassing (no of arms: <=3)
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td>NA
									</td>
								</tr>
								<tr>
									<td>21</td>
									<td></td>
									<td class="hidden-480">Total Outgassing (no of arms: >=4)
									</td>
									<td class="hidden-480">NA
									</td>
									<td class="hidden-480"><input type="text"></td>
									<td>NA
									</td>
								</tr>
								
								
								
							</tbody>
						</table>
						<h6>DHS Chromatogram</h6>
					</div>
					
					<div class="row-fluid">
						<div class="span6 invoice-block">
							<ul class="unstyled">
								<li><strong>Tested By:</strong> ------------------------------</li>
								<li><strong>Date:</strong> ------------------------------</li>
							</ul>
						</div>
						<div class="span6 invoice-block">
							<ul class="unstyled">
								<li><strong>Approved By:</strong> ------------------------------</li>
								<li><strong>Date:</strong> ------------------------------</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->

