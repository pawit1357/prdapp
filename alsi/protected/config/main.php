<?php
return array(
		'name'=>'ALSI',
		'defaultController'=>'dashboard',
		'import'=>array(
				'application.models.*',
				'application.components.*',
				'application.extensions.smtpmail.PHPMailer',
				'application.extensions.PHPPDO.CPdoDbConnection',
		),

		'components'=>array(
				'urlManager'=>array(
						'urlFormat'=>'path'
				),
				'log'=>array(
						'class'=>'CLogRouter',
						'routes'=>array(
								array(
										'class'=>'CFileLogRoute',
										'levels'=>'error, warning',
								),
								array(
										'class'=>'EFirephp',
										'config'=>array(
												'enabled'=>true,
												'dateFormat'=>'Y/m/d H:i:s',
										),
										'levels'=>'error, warning, trace, profile, info',
								),
						),
				),
				'db'=>array(
						'class'=>'application.extensions.PHPPDO.CPdoDbConnection',
						'pdoClass' => 'PHPPDO',
						'connectionString' => 'mysql:host=localhost;dbname=alsi',
						'emulatePrepare' => true,
						'username' => 'root',
						'password' => 'password',
						'charset' => 'utf8',
				),
				'Smtpmail'=>array(
						'class'=>'application.extensions.smtpmail.PHPMailer',
						'Host'=>"smtp.gmail.com",
						'Username'=>'pawitvaap@gmail.com',
						'Password'=>'pawitsaeeaung',
						'Mailer'=>'smtp',
						'Port'=>587,
						'SMTPAuth'=>true,
						'SMTPSecure' => 'tls',

				),
		),
);
?>
