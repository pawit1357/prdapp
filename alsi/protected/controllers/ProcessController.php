<?php
/**
 * SiteController is the default controller to handle user requests.
 */
class ProcessController extends CController
{
	public $layout='main';
// 	private $_model;

	/**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex()
	{
		
// 		if(isset($_POST['username']) && isset($_POST['username'])){
// 			$this->redirect(Yii::app()->createUrl('Dashboard/'));
// 		}
			
		// Render
		$this->render('main');
	}

	/**
	 * Login Page
	 */
	public function actionLogin()
	{

		// if login redirect to index
// 		if(UserLoginUtil::isLogin()){
// 			$this->redirect(Yii::app()->createUrl(''));
// 		}
		// if post parameters username and password submitted
// 		if(isset($_POST['UserLogin']['username']) && isset($_POST['UserLogin']['password'])){
// 			$username = addslashes($_POST['UserLogin']['username']);
// 			$password = addslashes($_POST['UserLogin']['password']);
// 			// Authen
			
// 			if(UserLoginUtil::authen($username, $password)) {
				
// 				if(UserLoginUtil::hasPermission(array("FULL_ADMIN"))){
// 					$this->redirect(Yii::app()->createUrl('RequestBooking/AllBooking'));
// 				}else{
// 					$this->redirect(Yii::app()->createUrl('RequestBooking/'));
// 				}
				
// 			} else {
// 				$this->redirect(Yii::app()->createUrl('RequestBooking/'));
// 			}
			
// 		}
		$this->render('login');

	}

// 	public function actionRegister()
// 	{
		// if post parameters username and password submitted
// 		if(isset($_POST['add_user'])){
// 			$userLogin = new UserLogin();
// 			$userLogin->attributes = $_POST['UserLogin'];
// 			$userLogin->password = md5($userLogin->password);
// 			$userLogin->role_id = '3';
// 			$validate = true;

// 			try {
// 				if($userLogin->save()){
// 					// Create User Information
// 					$userInfo = new UserInformation();
// 					$userInfo->id = $userLogin->getPrimaryKey();
// 					$userInfo->attributes = $_POST['UserInformation'];
// 					if($userInfo->save()){
// 						$_SESSION['OPERATION_RESULT'] = array('class'=>'success', 'message'=>'The registeration was successful.');
// 						$this->redirect(Yii::app()->createUrl('Management/Register'));
// 					} else {
// 						$this->render('register');
// 					}
// 				}
// 			} catch(CDbException $e) {
// 				// Return error result
// 				$message = $e->getMessage();
// 				if(strpos($message, 'Duplicate') && strpos($message, 'key 1')) {
// 					$message = 'Username is already exists.';
// 				} else if (strpos($message, 'Duplicate') && strpos($message, 'key 2')) {
// 					$message = 'Personal ID Card is already exists.';
// 				} else if (strpos($message, 'Duplicate') && strpos($message, 'key 3')) {
// 					$message = 'Email is already exists.';
// 				}
// 				$_SESSION['OPERATION_RESULT'] = array('class'=>'error', 'message'=>$message);
// 				$this->render('register');
// 			}
// 		} else {
// 			$this->render('register');
// 		}
// 	}


	/**
	 * Logout
	 */
	public function actionLogout()
	{
// 		UserLoginUtil::logout();
		$this->redirect(Yii::app()->createUrl('Auth/'));
	}

// 	public function actionChangePassword()
// 	{
// 		if(!UserLoginUtil::isLogin()){
// 			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
// 					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
// 		}
// 		if(isset($_POST['submit'])) {
// 			$newPassword = $_POST['new-password'];
// 			$reNewPassword = $_POST['re-new-password'];
// 			$oldPassword = $_POST['password'];
// 			$validated = true;
// 			if($newPassword == '') {
// 				$validated = false;
// 				$operationResultClass = 'error';
// 				$operationResultMessage = 'Please Enter New Password';
// 			} else if($newPassword != $reNewPassword) {
// 				$validated = false;
// 				$operationResultClass = 'error';
// 				$operationResultMessage = 'New password must be the same.';
// 			} else if(md5($oldPassword) != UserLoginUtil::getUserLogin()->password) {
// 				$validated = false;
// 				$operationResultClass = 'error';
// 				$operationResultMessage = 'Current password not correct.';
// 			}
// 			if($validated) {
// 				$userLogin = UserLoginUtil::getUserLogin();
// 				$userLogin->password = md5($newPassword);
// 				if($userLogin->update()) {
// 					$operationResultClass = 'success';
// 					$operationResultMessage = 'Password has been changed.';
// 				} else {
// 					$operationResultClass = 'error';
// 					$operationResultMessage = 'Change password fail.';
// 				}
// 			}
// 			$_SESSION['OPERATION_RESULT'] = array('class'=>$operationResultClass, 'message'=>$operationResultMessage);
// 			$this->redirect(Yii::app()->createUrl('Management/ChangePassword'));
// 		}

// 		$this->render('changePassword');
// 	}

// 	public function actionEditProfile()
// 	{
// 		if(!UserLoginUtil::isLogin()){
// 			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
// 					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
// 		}

// 		if(isset($_POST['edit_user'])){
// 			$userLogin = UserLoginUtil::getUserLogin();
// 			$userLogin->attributes = $_POST['UserLogin'];
// 			$validate = true;
// 			if($userLogin->update()){
// 				$userInfo = UserInformation::model()->findByPk($userLogin->id);
// 				$userInfo->attributes = $_POST['UserInformation'];
				
// 				if($userInfo->update()){
// 					$operationResultClass = 'success';
// 					$operationResultMessage = 'Profile has been changed.';
// 				} else {
// 					$operationResultClass = 'error';
// 					$operationResultMessage = 'Change user information failed.';
// 				}
// 			} else {
// 				$operationResultClass = 'error';
// 				$operationResultMessage = 'Change email failed.';
// 			}
// 			$_SESSION['OPERATION_RESULT'] = array('class'=>$operationResultClass, 'message'=>$operationResultMessage);
// 			$this->redirect(Yii::app()->createUrl('Management/EditProfile'));
// 		}
// 		$model = UserLoginUtil::getUserLogin();
// 		$this->render('editProfile', array('model'=>$model));
// 	}

// 	public function actionForgetPassword()
// 	{
// 		if(isset($_POST['email'])) {
// 			// Verify email exists
// 			$email = addslashes($_POST['email']);
// 			$userLogins = UserLogin::model()->findAll(array('condition'=>"email='".$email."'"));
// 			if(isset($userLogins) && count($userLogins) > 0) {
// 				$userLogin = $userLogins[0];

// 				// Random key
// 				$num1 = rand(1, 1000000);
// 				$num2 = rand(1, 1000000);
// 				$num3 = rand(1, 1000000);
// 				$num4 = rand(1, 1000000);
// 				$key = md5($num1).md5($num2).md5($num3).md5($num4);

// 				// Save key and send email to reset password by key
// 				$userForgetPasswordRequest = new UserForgetPasswordRequest();
// 				$userForgetPasswordRequest->user_login_id = $userLogin->id;
// 				$userForgetPasswordRequest->key = $key;
// 				$userForgetPasswordRequest->request_date = date("Y-m-d H:i:s");
// 				$operationResultClass = 'error';
// 				if($userForgetPasswordRequest->save()) {
// 					$mailContent = "Hi ".$userLogin->user_information->first_name.' '.$userLogin->user_information->last_name.'<br />'.
// 							'You have requested to reset your password. Please follow the link below here.<br />'.
// 							'<a href="'.ConfigUtil::getSiteName().'/index.php/Management/ResetPassword/key/'.$key.'/u/'.$userLogin->id.'">>>Click Here to Reset Password<<</a><br />'.
// 							'This Link will be expire within 30 minutes.';
// 					if(MailUtil::sendMail($email, "Support AV-Online, Reset your password", $mailContent)){
// 						$operationResultClass = 'success';
// 						$operationResultMessage = 'Email has been sent. Please check your email, it will expire within 30 minutes.';
// 					} else {
// 						$operationResultMessage = 'Cannot send email';
// 					}
// 				} else {
// 					$operationResultMessage = "Cannot save information.";
// 				}
// 				$_SESSION['OPERATION_RESULT'] = array('class'=>$operationResultClass, 'message'=>$operationResultMessage);
// 				$this->redirect(Yii::app()->createUrl('Management/ForgetPassword'));
// 			} else {
// 				$_SESSION['OPERATION_RESULT'] = array('class'=>'error', 'message'=>'Email not found.');
// 				$this->redirect(Yii::app()->createUrl('Management/ForgetPassword'));
// 			}
// 		}
// 		$this->render('forgetPassword');
// 	}
// 	public function actionResetPassword() {
// 		if(isset($_GET['key']) && isset($_GET['u'])) {
// 			// Get Key and User id
// 			$key = addslashes($_GET['key']);
// 			$userLoginId = addslashes($_GET['u']);

// 			// Find key
// 			$compareExpireTime = date("Y-m-d H:i:s", (time() - 1800));
// 			$condition = "request_date > '".$compareExpireTime."' AND user_login_id = ".$userLoginId." AND `key` = '".$key."'";
// 			$userForgetPasswordRequests = UserForgetPasswordRequest::model()->findAll(array('condition'=>$condition, 'order'=>'id DESC'));
// 			$userForgetPasswordRequest = $userForgetPasswordRequests[0];
// 			if(isset($userForgetPasswordRequest)) {
// 				// If key exists.
// 				if(isset($_POST['password']) && isset($_POST['re-password'])) {
// 					$password = addslashes($_POST['password']);
// 					$rePassword = addslashes($_POST['re-password']);
// 					if($password != $rePassword) {
// 						$operationResultClass = 'error';
// 						$operationResultMessage = 'Password not same.';
// 						$_SESSION['OPERATION_RESULT'] = array('class'=>$operationResultClass, 'message'=>$operationResultMessage);
// 						$userLogin = $userForgetPasswordRequest->user_login;
// 						$this->render('resetPassword', array('userLogin'=>$userLogin, 'status'=>$userForgetPasswordRequest->status));
// 					} else {
// 						$userLogin = $userForgetPasswordRequest->user_login;
// 						$userLogin->password = md5($password);
// 						if($userLogin->update()) {
// 							// Update new password
// 							$operationResultClass = 'success';
// 							$operationResultMessage = 'Your password has been changed.';
// 							$_SESSION['OPERATION_RESULT'] = array('class'=>$operationResultClass, 'message'=>$operationResultMessage);
// 							$userForgetPasswordRequest->delete();
// 							$this->render('resetPassword', array('status'=>'success'));
// 						} else {
// 							$operationResultClass = 'error';
// 							$operationResultMessage = 'Can not update.';
// 							$_SESSION['OPERATION_RESULT'] = array('class'=>$operationResultClass, 'message'=>$operationResultMessage);
// 							$this->render('resetPassword', array('userLogin'=>$userLogin, 'status'=>$userForgetPasswordRequest->status));
// 						}
// 					}
// 				}else {
// 					$userLogin = $userForgetPasswordRequest->user_login;
// 					$this->render('resetPassword', array('userLogin'=>$userLogin, 'status'=>$userForgetPasswordRequest->status));
// 				}
// 			} else {
// 				throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
// 						array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
// 			}
// 		} else {
// 			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
// 					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
// 		}
// 	}
}