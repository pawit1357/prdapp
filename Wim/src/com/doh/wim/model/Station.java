package com.doh.wim.model;

public class Station {
	//http://sunil-android.blogspot.com/2013/07/fragment-list-with-image-and-text-with.html
	public int stationId;
	public String stationName;
	
	public Station(){
		
	}
	
	public Station(int stationId, String stationName) {
		this.stationId = stationId;
		this.stationName = stationName;
	}
	
	public int getStationId() {
		return stationId;
	}
	public void setStationId(int stationId) {
		this.stationId = stationId;
	}
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	
	@Override
	public String toString() {
		return stationName;
	}

}
