﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="LiveView.aspx.cs" Inherits="DOH.VWS.WEB.Views.Live.LiveView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".pic1").colorbox({ rel: 'pic1' });
        });
    </script>
    <script type="text/javascript" src="/Scripts/LiveWeighing.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                ข้อมูลรถเข้าด่านชั่ง<br />
            </div>
            <fieldset>
                <table border="0" cellpadding="0" cellspacing="0" id="id-form" class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            Station:
                        </td>
                        <td>
                            <asp:DropDownList ID="Ddl_Station" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <div class="title2">
                Display Result<br />
            </div>
            <fieldset>
                <div style="width: 100%; height: 800px;">
                    <br />
                    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
                    </asp:ScriptManagerProxy>

                    <div id="tbReport">
                    </div>
                </div>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
