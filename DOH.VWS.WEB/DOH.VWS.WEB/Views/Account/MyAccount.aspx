﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="MyAccount.aspx.cs" Inherits="DOH.VWS.WEB.Views.Account.MyAccount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                แก้ไขข้อมูลผู้ใช้<br />
            </div>
            <fieldset>
                <%if (Session["resultMessage"] != null)
                  { %>
                <div class="<%=Session["resultClass"] %>">
                    <%=Session["resultMessage"]%></div>
                <%
                      Session.Remove("resultMessage");
                  } 
                %>
                <table class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            Username&nbsp;
                        </td>
                        <td>
                            &nbsp; &nbsp;<asp:Label ID="LUsername" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Password
                        </td>
                        <td class="field">
                            &nbsp;
                            <asp:TextBox ID="txtNewPassword" runat="server" required></asp:TextBox>
                            <%--                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewPassword"
                                CssClass="validate" ErrorMessage="RequiredFieldValidator">You must enter new password.</asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Confirm Password&nbsp;
                        </td>
                        <td class="field">
                            &nbsp;
                            <asp:TextBox ID="txtConfirmPassword" runat="server"></asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNewPassword"
                                ControlToValidate="txtConfirmPassword" CssClass="validate" ErrorMessage="Password must be the same."></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Enter your current password
                        </td>
                        <td class="field">
                            &nbsp;
                            <asp:TextBox ID="txtCurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="style1">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Full Name :
                        </td>
                        <td class="field">
                            &nbsp;
                            <asp:TextBox ID="txtFullName" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            E-Mail :
                        </td>
                        <td class="field">
                            &nbsp;
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="Search">
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                            <input id="btClear" type="reset" value="Cancel" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
