﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Dto;
using DOH.VWS.Biz;

namespace DOH.VWS.WEB.Views.Account
{
    public partial class MyAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UserLoginBean userLoginBean = (UserLoginBean)HttpContext.Current.Session["userLoginBean"];
            if (userLoginBean != null)
            {
                LUsername.Text = userLoginBean.Username;
                txtCurrentPassword.Text = userLoginBean.Password;

                txtFullName.Text = userLoginBean.FullName;
                txtEmail.Text = userLoginBean.Email;

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            string newPassword = txtNewPassword.Text;
            string reNewPassword = txtConfirmPassword.Text;
            string oldPassword = txtCurrentPassword.Text;
            string fullName = txtFullName.Text;
            string email = txtEmail.Text;
            bool valdated = true;

            UserLoginService service = new UserLoginService();
            UserLoginBean bean = (UserLoginBean)Session["userLoginBean"];


            if (valdated)
            {
                if (service.editProfile(bean.ID, newPassword, email, fullName))
                {
                    Session["resultMessage"] = "Profile has been updated.";
                    Session["resultClass"] = "success";
                    Response.Redirect("MyAccount.aspx");
                }
                else
                {
                    Session["resultMessage"] = "Update fail.";
                    Session["resultClass"] = "error";
                    Response.Redirect("MyAccount.aspx");
                }
            }

        }
    }
}