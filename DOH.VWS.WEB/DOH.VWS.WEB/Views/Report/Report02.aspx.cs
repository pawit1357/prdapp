﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Dao;
using CrystalDecisions.CrystalReports.Engine;
using System.Text;
using DOH.VWS.Model;
using DOH.VWS.Biz;
using System.Data;

namespace DOH.VWS.WEB.Views.Report
{
    public partial class Report02 : System.Web.UI.Page
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Report02));
        protected Boolean bSearchFound = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/Login.aspx");

            //Load Default
            if (!Page.IsPostBack)
            {
                LoadDefault();
            }
        }

        protected void BtnShowReport_Click(object sender, EventArgs e)
        {
            VehicleRecordDAO dao = new VehicleRecordDAO();


            String startDate = txtStart.Text;
            String endDate = txtEnd.Text;

            int year = Convert.ToDateTime(startDate).Year;
            int stationID = Convert.ToInt16(Ddl_Station.SelectedValue);

            String reportType = RdReportType.SelectedValue;
            StringBuilder sbCri = new StringBuilder();
            String cri = "";
            List<ModelVehicleRecord> lists = null;


            if (startDate != null && !String.IsNullOrWhiteSpace(startDate))
            {
                if (endDate != null && !String.IsNullOrWhiteSpace(endDate))
                {
                    sbCri.Append("CONVERT(VARCHAR(8),TimeStamp,112) Between " + Convert.ToDateTime(startDate).ToString("yyyyMMdd") + " And " + Convert.ToDateTime(endDate).ToString("yyyyMMdd"));
                }
                else
                {
                    sbCri.Append("CONVERT(VARCHAR(8),TimeStamp,112) Between " + Convert.ToDateTime(startDate).ToString("yyyyMMdd") + " And " + Convert.ToDateTime(startDate).ToString("yyyyMMdd"));
                }
            }


            if (!sbCri.ToString().Equals(""))
            {
                cri = " Where " + sbCri.ToString();
            }

            lists = dao.select(cri);
            if (lists.Count > 0)
            {
                /*
                 * 1:ปี,2:เดือน,3:วัน,4:ชั่วโมง,5:นาที
                 */
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/ReportObjects/Rpt02" + DefaultDAO2.getReportPrefix(reportType) + ".rpt"));
                report.SetDataSource(lists);
                report.SetParameterValue("P_START_DATE", startDate);
                report.SetParameterValue("P_END_DATE", endDate);
                Session["P_START_DATE"] = startDate;
                Session["P_END_DATE"] = endDate;
                Session["RptDoc"] = report;


                lblDataNotFound.Text = String.Empty;
                myIframe.Attributes.Add("src", "/RptViewer.aspx");
                bSearchFound = true;
            }
            else
            {
                //logger.Debug("Not found Report.");
                Session["RptDoc"] = null;
                lblDataNotFound.Text = "- ไม่พบข้อมูล -";
                bSearchFound = false;
            }
        }

        private void LoadDefault()
        {
            StationBAO bao = new StationBAO();
            DataSet ds = bao.LoadStation();
            if (ds != null)
            {
                Ddl_Station.DataSource = ds;
                Ddl_Station.DataValueField = "StationCode";
                Ddl_Station.DataTextField = "StationNameTh";
                Ddl_Station.DataBind();
                //Ddl_Station.Items.Insert(0, new ListItem("Select", "-1"));
            }
            else
            {

            }

        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Report02.aspx");
        }
    }
}