﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Report01.aspx.cs" Inherits="DOH.VWS.WEB.Views.Report.Report01" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                ค้นหา - ปริมาณรถ แยกตามประเภท<br />
            </div>
            <fieldset>
                <table class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            Station:
                        </td>
                        <td>
                            <asp:DropDownList ID="Ddl_Station" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="HeadRow">
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Start:
                        </td>
                        <td class="style5">
                            <asp:TextBox ID="txtStart" Text="" runat="server" required></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtStart">
                            </asp:CalendarExtender>
                        </td>
                        <td class="HeadRow" style="width: 119px">
                            End:
                        </td>
                        <td>
                            <asp:TextBox ID="txtEnd" Text="" runat="server" required></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtEnd">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Data Format:
                        </td>
                        <td class="style5">
                            <asp:RadioButtonList ID="RdReportType" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Selected="True">ปกติ</asp:ListItem>
                                <asp:ListItem Value="2">กราฟแท่ง</asp:ListItem>
                                <asp:ListItem Value="3">กราฟเส้น</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td class="HeadRow" style="width: 119px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="2">
                            <asp:Button ID="BtnShowReport" runat="server" Text="แสดงรายงาน" OnClick="BtnShowReport_Click" />
                            <asp:Button ID="BtnCancel" runat="server" OnClick="BtnCancel_Click" 
                                Text="ยกเลิก" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <% 
                if (bSearchFound)
                { 
            %>
            <fieldset>
                <iframe name="myIframe" id="myIframe" width="100%" height="650px" runat="server"
                    visible="True"></iframe>
            </fieldset>
            <%}%>
            <div id="divWrapper">
                <div id="divSaveResult">
                    <h1>
                        <asp:Label ID="lblDataNotFound" runat="server" CssClass="validate" /></h1>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
