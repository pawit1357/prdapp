﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using DOH.VWS.Biz;
using DOH.VWS.Utils;
using DOH.VWS.Dao;
using AjaxControlToolkit;


namespace DOH.VWS.WEB.Views.Master
{
    public partial class UserManagement : System.Web.UI.Page
    {

        protected DataSet UserDataSet
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_USER"))
                Response.Redirect(ConfigurationUtil.getDefaultPage());

            LoadUserDataSet();
        }


        private void LoadUserDataSet()
        {
            UserLoginBAO bao = new UserLoginBAO();

            UserDataSet = null;
            DataSet ds = bao.LoadUsers();
            if (ds != null && ds.Tables.Count > 0)
            {
                UserDataSet = ds;
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
        }

        [WebMethod]
        public static void AjaxTest()
        {
            throw new Exception("You must supply an email address.");
        }

        protected void Img_Command(object sender, CommandEventArgs e)
        {
            Console.WriteLine("");
            String ID = e.CommandArgument.ToString();
            switch (e.CommandName)
            {
                case "Delete":
                    Response.Redirect(String.Format("DeleteUser.aspx?UserID={0}", ID));
                    break;
            }
            Console.WriteLine("");
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                ConfirmButtonExtender btnConfirm = (ConfirmButtonExtender)e.Row.FindControl("ConfirmButton");
                btnConfirm.ConfirmText = "Do you want to delete?";
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
    }
}
