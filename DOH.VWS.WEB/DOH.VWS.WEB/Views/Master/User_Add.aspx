﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="User_Add.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.User_Add" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        function ValidateInputForm() {
            var errMsg = '';
            if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
                errMsg = 'ยืนยันรหัสผ่านไม่ถูกต้อง';
            }

            if (errMsg != '') {
                alert('Error: ' + errMsg);
            }

            return (errMsg == '');
        }


        $(function () {
            $('#btClear').click(function () {
                $('#txtUsername,#txtFullName,#txtPassword,#txtConfirmPassword,#txtEMail').val('');
                $('#lstUserStatus').val('1');
                $('#lstUserLevel').val('2');
            });


            $('#btClear').click();
        });

    </script>
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                เพิ่มรายชื่อผู้ใช้ระบบ (Add New User)<br />
            </div>
            <fieldset>
                <% if (SaveResultMessage.Length > 0)
                   { %>
                <div id="divWrapper">
                    <div id="divSaveResult">
                        <h1>
                            <%=SaveResultMessage%></h1>
                    </div>
                </div>
                <% } %>
                <form action="User_Add.aspx" method="post" onsubmit="return ValidateInputForm()">
                <table class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            Username :
                        </td>
                        <td class="field">
                            <input id="txtUsername" type="text" runat="server" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Password :
                        </td>
                        <td class="field">
                            <input id="txtPassword" type="password" runat="server" enableviewstate="false" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Confirm Password :
                        </td>
                        <td class="field">
                            <input id="txtConfirmPassword" type="password" runat="server" enableviewstate="false"
                                required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Full Name :
                        </td>
                        <td class="field">
                            <input id="txtFullName" type="text" runat="server" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            E-Mail :
                        </td>
                        <td class="field">
                            <input id="txtEMail" type="text" runat="server" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Status :
                        </td>
                        <td class="field">
                            <select id="lstUserStatus" runat="server">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Level :
                        </td>
                        <td class="field">
                            <select id="lstUserLevel" runat="server">
                                <option value="2">Manager</option>
                                <option value="3">Staff</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Station :
                        </td>
                        <td class="field">
                            <select id="cboStation" runat="server">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            &nbsp;
                        </td>
                        <td colspan="2" class="Search">
                            <input id="btSave" type="submit" runat="server" value="Save" onserverclick="btSave_OnClick" />
                            <input id="btnCancel" type="reset" value="Cancel" />
                        </td>
                    </tr>
                </table>
                </form>
                <a href="UserManagement.aspx">ย้อนกลับ (Back)</a>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
