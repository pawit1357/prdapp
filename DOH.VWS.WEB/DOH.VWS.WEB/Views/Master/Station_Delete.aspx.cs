﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DOH.VWS.Biz;
using DOH.VWS.Utils;
using DOH.VWS.Dao;


namespace DOH.VWS.WEB.Views.Master
{
    public partial class Station_Delete : System.Web.UI.Page
    {
        protected Boolean DeleteSucceed
        {
            get;
            set;
        }

        protected string DeleteResultMessage
        {
            get;
            set;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("DELETE_STATION"))
                Response.Redirect(ConfigurationUtil.getDefaultPage());

            DeleteSucceed = false;
            DeleteResultMessage = "";

            int id = Gm.toSafeInt(Request.QueryString["StationID"]);

            if (id > 0)
            {
                DeleteSucceed = DeleteUserLogin(id);
                if (DeleteSucceed)
                    DeleteResultMessage = "ลบรายการสำเร็จ";
                else
                    DeleteResultMessage = (DeleteResultMessage.Length>0) ? DeleteResultMessage : "ลบรายการไม่สำเร็จ (Failed)";
            }
            else
            {
                DeleteResultMessage = "ไม่สามารถลบรายการที่ระบุได้";
            }
        }

        private Boolean DeleteUserLogin(int stationTid)
        {
            return new StationDAO().deleteById(stationTid);
        }
    }
}