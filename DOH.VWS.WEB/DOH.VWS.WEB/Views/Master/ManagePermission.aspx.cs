﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using DOH.VWS.Biz;
using DOH.VWS.Dao;
using DOH.VWS.Utils;


namespace DOH.VWS.WEB.Views.Master
{
    public partial class ManagePermission : System.Web.UI.Page
    {

        protected DataSet UserDataSet
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission(new int[] { ConfigurationUtil.USER_LEVEL_ADMIN }))
                Response.Redirect(ConfigurationUtil.getDefaultPage());

            LoadUserDataSet();
        }


        private void LoadUserDataSet()
        {

            DataTable table = new DataTable();
            table.Columns.Add("UserLevel", typeof(string));
            table.Columns.Add("Permission", typeof(string));

            //
            // Here we add five DataRows.
            //
            table.Rows.Add("Admin", "1");
            table.Rows.Add("Manager", "2");
            table.Rows.Add("Staff", "3");

            UserDataSet = new DataSet();
            UserDataSet.Tables.Add(table);
            GridView1.DataSource = UserDataSet;
            GridView1.DataBind();
            //UserLoginBAO bao = new UserLoginBAO();

            //UserDataSet = null;
            //DataSet ds = bao.LoadUsers();
            //if (ds != null && ds.Tables.Count > 0)
            //{
            //    UserDataSet = ds;
            //}

            //UserDataSet = null;

            //ConnectionDB connection = new ConnectionDB();
            //string sql = "SELECT [ID],[UserName],[FullName], [Email], "
            //            + "'StatusName' = CASE [Status] WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' ELSE '' END, "
            //            + "'UserLevelName' = CASE [UserLevel] WHEN 1 THEN 'Admin' WHEN 2 THEN 'Manager' ELSE 'Staff' END "
            //            + "FROM [UserLogin] "
            //            + "WHERE [Status] IN (1,0) "
            //            + "ORDER BY UserName";
            //SqlCommand cmd = new SqlCommand(sql);
            //cmd.CommandType = System.Data.CommandType.Text;
            //SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            //cmd.Connection = connection.GetConnection();
            //DataSet ds = new DataSet();
            //adpt.Fill(ds);

            //if (ds != null && ds.Tables.Count > 0)
            //{
            //    UserDataSet = ds;
            //}

            //connection.CloseConnection();
        }

        [WebMethod]
        public static void AjaxTest()
        {
            throw new Exception("You must supply an email address.");
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            
        }
    }
}
