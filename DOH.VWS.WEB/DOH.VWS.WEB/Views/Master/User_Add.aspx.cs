﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Utils;
using DOH.VWS.Dto;
using DOH.VWS.Dao;
using DOH.VWS.Biz;




namespace DOH.VWS.WEB.Views.Master
{
    public partial class User_Add : System.Web.UI.Page
    {
        protected Boolean SaveSucceed
        {
            get;
            set;
        }

        protected string SaveResultMessage
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("ADD_USER"))
                Response.Redirect(ConfigurationUtil.getDefaultPage());

            SaveSucceed = false;
            SaveResultMessage = "";

            if (!IsPostBack)
            {
                cboStation.Items.Add(new ListItem("-Select-", ""));
                foreach (StationDTO station in new StationDAO().getAll())
                {
                    cboStation.Items.Add(new ListItem(station.StationNameTh, "" + station.ID));
                }
            }
        }

        protected void btSave_OnClick(object Source, EventArgs e)
        {
            UserLoginDTO user = new UserLoginDTO();
            user.Username = txtUsername.Value;
            user.Password = txtPassword.Value;
            user.FullName = txtFullName.Value;
            user.Email = txtEMail.Value;
            user.Status = Gm.toSafeInt(lstUserStatus.Value);
            user.Level = Gm.toSafeInt(lstUserLevel.Value);

            UserStationDTO userStation = new UserStationDTO();
            userStation.StationId = Gm.toSafeInt(cboStation.Value);

            UserLoginDAO dao = new UserLoginDAO();
            UserLoginDTO userResult = null;

            try
            {
                userResult = dao.save(user) as UserLoginDTO;
                userStation.UserLoginId = userResult.ID;
                userStation.Status = "ACTIVE";
                new UserStationDAO().save(userStation);
            }
            catch (Exception)
            {
                userResult = null;
            }

            SaveSucceed = (userResult != null);
            SaveResultMessage = (SaveSucceed) ? "บันทึกข้อมูลผู้ใช้ใหม่เรียบร้อยแล้ว" : "บันทึกไม่สำเร็จ (Save Failed) !!!";

            if (SaveSucceed)
            {
                // clear input
                txtUsername.Value = "";
                txtEMail.Value = "";
                txtFullName.Value = "";
                txtPassword.Value = "";
                txtConfirmPassword.Value = "";
                lstUserLevel.SelectedIndex = 0;
                lstUserStatus.SelectedIndex = 0;
            }
        }
    }
}