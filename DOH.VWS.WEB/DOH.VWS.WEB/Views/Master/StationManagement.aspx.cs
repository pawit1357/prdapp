﻿using System;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using DOH.VWS.Biz;
using DOH.VWS.Utils;
using AjaxControlToolkit;


namespace DOH.VWS.WEB.Views.Master
{
    public partial class StationManagement : System.Web.UI.Page
    {

        protected DataSet UserDataSet
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_STATION"))
                Response.Redirect(ConfigurationUtil.getDefaultPage());

            LoadStationDataSet();
        }


        private void LoadStationDataSet()
        {

            StationBAO bao = new StationBAO();

            UserDataSet = null;
            DataSet ds = bao.LoadStation();
            if (ds != null && ds.Tables.Count > 0)
            {
                UserDataSet = ds;
                GridView1.DataSource = UserDataSet;
                GridView1.DataBind();
            }
        }

        [WebMethod]
        public static void AjaxTest()
        {
            throw new Exception("You must supply an email address.");
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void Img_Command(object sender, CommandEventArgs e)
        {
            Console.WriteLine("");
            String ID = e.CommandArgument.ToString();
            switch (e.CommandName)
            {
                case "Delete":
                    Response.Redirect(String.Format("Station_Delete.aspx?StationID={0}", ID));
                    break;
            }
            Console.WriteLine("");

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                ConfirmButtonExtender btnConfirm = (ConfirmButtonExtender) e.Row.FindControl("ConfirmButton");
                btnConfirm.ConfirmText = "Do you want to delete?";
            }
        }

    }
}
