﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ManagePermission.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.ManagePermission" %>

<%@ Import Namespace="DOH.VWS.Utils" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                จัดการสิทธิ์การเข้าถึง<br />
            </div>
            <fieldset>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                    BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None"
                    CssClass="tb_ResultList" AllowPaging="True" PageSize="3" Width="400px" OnRowCreated="GridView1_RowCreated">
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:BoundField DataField="UserLevel" HeaderText="User Level" />
                        <asp:TemplateField HeaderText="Permission">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("Permission", "Permission_Edit.aspx?UserLevel={0}") %>'
                                    Text='Manage'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Label ID="lblDataNotFound" runat="server" Text="Data Not found" CssClass="data-not-found" />
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
