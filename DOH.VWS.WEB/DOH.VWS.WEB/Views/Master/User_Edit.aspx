﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="User_Edit.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.User_Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        function ValidateInputForm() {
            var errMsg = '';
            if (($('#txtPassword').val() != '' || $('#txtConfirmPassword').val() != '')
                && $('#txtPassword').val() != $('#txtConfirmPassword').val()) {
                errMsg = 'ยืนยันรหัสผ่านไม่ถูกต้อง';
            }

            if (errMsg != '') {
                alert('Error: ' + errMsg);
            }

            return (errMsg == '');
        }


        $(function () {
            $('#btClear').click(function () {
                window.location = 'UserManagement.aspx';
            });
        });

    </script>
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                แก้ไขข้อมูลผู้ใช้ระบบ (Edit User)<br />
            </div>
            <fieldset>
                <% if (SaveResultMessage.Length > 0)
                   { %>
                <div id="divWrapper">
                    <div id="divSaveResult">
                        <h1>
                            <%=SaveResultMessage%></h1>
                    </div>
                    <br />
                    <a href="UserManagement.aspx">ย้อนกลับ (Back)</a>
                </div>
                <% }
                   else
                   { %>
                <form id="Form1" action="User_Edit.aspx" method="post" onsubmit="return ValidateInputForm()">
                <table class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            Username :
                        </td>
                        <td class="field">
                            <input id="txtUsername" type="text" runat="server" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Password :
                        </td>
                        <td class="field">
                            <input id="txtPassword" type="password" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Confirm Password :
                        </td>
                        <td class="field">
                            <input id="txtConfirmPassword" type="password" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Full Name :
                        </td>
                        <td class="field">
                            <input id="txtFullName" type="text" runat="server" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            E-Mail :
                        </td>
                        <td class="field">
                            <input id="txtEMail" type="text" runat="server" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Status :
                        </td>
                        <td class="field">
                            <select id="lstUserStatus" runat="server">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Level :
                        </td>
                        <td class="field">
                            <select id="lstUserLevel" runat="server">
                                <option value="2">Manager</option>
                                <option value="3">Staff</option>
                                <option value="1">Admin</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Station :
                        </td>
                        <td class="field">
                            <select id="cboStation" runat="server">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            &nbsp;
                        </td>
                        <td class="field">
                            <input id="btSave" type="submit" runat="server" value="Save" onserverclick="btSave_OnClick" />
                            <input id="btClear" type="reset" value="Cancel" />
                        </td>
                    </tr>
                </table>
                </form>
                <a href="UserManagement.aspx">ย้อนกลับ (Back)</a>
                <% } %>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
