﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="StationManagement.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.StationManagement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                Manage Station<br />
            </div>
            <fieldset>
                <a href="Station_Add.aspx">Add New Station</a>
            </fieldset>
            <fieldset>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                    BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None"
                    CssClass="tb_ResultList" AllowPaging="True" 
                    OnPageIndexChanging="GridView1_PageIndexChanging" 
                    onrowdatabound="GridView1_RowDataBound">
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:BoundField DataField="StationCode" HeaderText="Code" />
                        <asp:BoundField DataField="StationNameTh" HeaderText="Name" />
                        <%--                        <asp:BoundField DataField="StationNameEn" HeaderText="NameEn" />--%>
                        <asp:BoundField DataField="Description" HeaderText="Description" />
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("ID", "Station_View.aspx?StationID={0}") %>'
                                    Text='View' ImageUrl="~/Images/view.png"></asp:HyperLink>
                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Eval("ID", "Station_Edit.aspx?StationID={0}") %>'
                                    Text='Edit' ImageUrl="~/Images/edit.png"></asp:HyperLink>
                                <asp:ConfirmButtonExtender ID="ConfirmButton" runat="server" TargetControlID="btnDelete" />
                                <asp:LinkButton ID="btnDelete" runat="server" oncommand="Img_Command" CommandName="Delete" CommandArgument='<%# Eval("ID") %>' ><img src="/Images/cross.png" alt="Delete" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Label ID="lblDataNotFound" runat="server" Text="Data Not found" CssClass="data-not-found" />
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
