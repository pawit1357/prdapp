﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="UserManagement.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.UserManagement" %>

<%@ Import Namespace="DOH.VWS.Utils" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                รายชื่อผู้ใช้ระบบ (User)<br />
            </div>
            <fieldset>
                <table class="tbSearch">
                    <tr>
                        <td>
                            <a href="User_Add.aspx">Add New User</a>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                    BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None"
                    CssClass="tb_ResultList" AllowPaging="True" 
                    OnPageIndexChanging="GridView1_PageIndexChanging" 
                    onrowdatabound="GridView1_RowDataBound">
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" />
                        <asp:BoundField DataField="UserName" HeaderText="Username" />
                        <asp:BoundField DataField="FullName" HeaderText="Full Name" />
                        <asp:BoundField DataField="Email" HeaderText="E-Mail" />
                        <asp:BoundField DataField="StatusName" HeaderText="Status" />
                        <asp:BoundField DataField="UserLevelName" HeaderText="Level" />
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("ID", "User_View.aspx?UserID={0}") %>'
                                    Text='View' ImageUrl="~/Images/view.png"></asp:HyperLink>
                                                                    <asp:HyperLink ID="HyperLink2" 
                                    runat="server" NavigateUrl='<%# Eval("ID", "User_Edit.aspx?UserID={0}") %>'
                                    Text='Edit' ImageUrl="~/Images/edit.png"></asp:HyperLink>
                                <asp:ConfirmButtonExtender ID="ConfirmButton" runat="server" TargetControlID="btnDelete" />
                                <asp:LinkButton ID="btnDelete" runat="server" oncommand="Img_Command" CommandName="Delete" CommandArgument='<%# Eval("ID") %>' ><img src="/Images/cross.png" alt="Delete" /></asp:LinkButton>
<%--                                                                    <asp:HyperLink ID="HyperLink3" 
                                    runat="server" NavigateUrl='<%# Eval("ID", "DeleteUser.aspx?UserID={0}") %>'
                                    Text='Delete' ImageUrl="~/Images/cross.png"></asp:HyperLink>--%>
                            </ItemTemplate>

                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Label ID="lblDataNotFound" runat="server" Text="Data Not found" CssClass="data-not-found" />
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>

            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
   
</asp:Content>
