﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Utils;
using DOH.VWS.Dto;
using DOH.VWS.Dao;
using DOH.VWS.Biz;


namespace DOH.VWS.WEB.Views.Master
{
    public partial class User_View : System.Web.UI.Page
    {
        protected string ErrorMessage
        {
            get;
            set;
        }

        protected UserLoginDTO UserDTO
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_USER"))
                Response.Redirect(ConfigurationUtil.getDefaultPage());

            ErrorMessage = "";
            UserDTO = null;

            if (!IsPostBack)
            {
                cboStation.Items.Add(new ListItem("-Select-", ""));
                foreach (StationDTO station in new StationDAO().getAll())
                {
                    cboStation.Items.Add(new ListItem(station.StationNameTh, "" + station.ID));
                }

                int id = Gm.toSafeInt(Request.QueryString["UserID"]);

                if (id > 0)
                {
                    // Load User Info
                    UserLoginDAO dao = new UserLoginDAO();
                    UserLoginDTO user = dao.getByPrimaryKey(id) as UserLoginDTO;

                    if (user != null)
                    {
                        UserStationDTO userStation = new UserStationDAO().getActiveByUserLoginId(user.ID);
                        if (userStation != null)
                        {
                            StationDTO station = new StationDAO().getByPrimaryKey(userStation.StationId) as StationDTO;
                            cboStation.SelectedIndex = cboStation.Items.IndexOf(new ListItem(station.StationNameTh, "" + station.ID));
                        }

                        txtUsername.Value = user.Username;
                        txtFullName.Value = user.FullName;
                        txtEMail.Value = user.Email;

                        // UserLevel (list order = Manager,Staff,Admin

                        if (user.Level == 1)
                            lstUserLevel.SelectedIndex = 2; // Admin
                        else if (user.Level == 2)
                            lstUserLevel.SelectedIndex = 0; // Manager
                        else if (user.Level == 3)
                            lstUserLevel.SelectedIndex = 1; // Staff


                        // Status (list order = Active, Inactive)
                        if (user.Status == 0)
                            lstUserStatus.SelectedIndex = 1;    // Inactive
                        else
                            lstUserStatus.SelectedIndex = 0;    // Active

                        UserDTO = user;
                    }
                    else
                    {
                        ErrorMessage = "ไม่พบรายการที่ระบุ";
                    }
                }
                else
                {
                    ErrorMessage = "Error";
                }
            }
        }
    }
}