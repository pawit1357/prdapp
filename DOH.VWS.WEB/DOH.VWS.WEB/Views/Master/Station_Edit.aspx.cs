﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using DOH.VWS.Dto;
using DOH.VWS.Dao;
using DOH.VWS.Utils;
using DOH.VWS.Biz;

namespace DOH.VWS.WEB.Views.Master
{
    public partial class Station_Edit : System.Web.UI.Page
    {
        protected Boolean SaveSucceed
        {
            get;
            set;
        }

        protected string SaveResultMessage
        {
            get;
            set;
        }

        protected StationDTO StationDTO
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("EDIT_USER"))
                Response.Redirect(ConfigurationUtil.getDefaultPage());

            SaveSucceed = false;
            SaveResultMessage = "";
            StationDTO = null;

            if (!IsPostBack)
            {
                int id = Gm.toSafeInt(Request.QueryString["StationID"]);

                if (id > 0)
                {
                    // Load User Info
                    StationDAO dao = new StationDAO();
                    StationDTO station = dao.getByPrimaryKey(id) as StationDTO;

                    if (station != null)
                    {
                        ViewState["StationID"] = station.ID;
                        lblStationCode.InnerHtml = station.StationCode;
                        txtNameEN.Value = station.StationNameEn;
                        txtNameTH.Value = station.StationNameTh;
                        txtDescription.Value = station.Description;
                        StationDTO = station;
                    }
                    else
                    {
                        SaveResultMessage = "ไม่พบรายการที่ระบุ";
                    }
                }
                else
                {
                    SaveResultMessage = "ไม่สามารถแก้ไขรายการที่ระบุได้";
                }
            }
        }

        protected void btSave_OnClick(object Source, EventArgs e)
        {
            StationDTO station = new StationDTO();
            station.ID = Gm.toSafeInt(ViewState["StationID"]);
            station.StationCode = lblStationCode.InnerHtml;
            station.StationNameEn = txtNameEN.Value;
            station.StationNameTh = txtNameTH.Value;
            station.Description = txtDescription.Value;

            StationDAO dao = new StationDAO();

            try
            {
                SaveSucceed = dao.update(station);
            }
            catch (Exception)
            {
                SaveSucceed = false;
            }

            SaveResultMessage = (SaveSucceed) ? "บันทึกข้อมูลเรียบร้อยแล้ว" : "บันทึกไม่สำเร็จ (Save Failed) !!!";

        }
    }
}