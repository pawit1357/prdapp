﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using DOH.VWS.Model;




namespace DOH.VWS.Dao
{
    public class VehicleRecordDAO : Connection
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(VehicleRecordDAO));

        public ModelVehicleRecord getByPK(int id)
        {
            List<ModelVehicleRecord> results = select(" Where v.id="+id);

            return (results.Count > 0) ? results[0] : null;
        }

        public List<ModelVehicleRecord> select(String cri)
        {
            List<ModelVehicleRecord> lists = new List<ModelVehicleRecord>();
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    SqlCommand cmd = new SqlCommand(String.Format("select * from dbo.WimData v left join WIMData_Master.dbo.VehicleClass  c on v.VehicleClass = c.vehicleID ") + cri, con);
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    int running = 1;
                    while (dr.Read())
                    {
                        ModelVehicleRecord model = new ModelVehicleRecord
                        {
                            Id = Convert.ToInt64(dr["ID"]),
                            StationID = Convert.ToInt16(dr["StationID"]),
                            TimeStamp = Convert.ToDateTime(dr["TimeStamp"]),
                            VehicleNumber = Convert.ToInt16(dr["VehicleNumber"]),
                            Lane = Convert.ToInt16(dr["Lane"]),
                            Error = Convert.ToInt16(dr["Error"]),
                            StatusCode = Convert.ToInt16(dr["StatusCode"]),

                            GVW = Convert.ToInt32(dr["GVW"]),

                            MaxGVW = Convert.ToInt32(dr["MaxGVW"]),
                            ESAL = Convert.ToDouble(dr["ESAL"]),
                            Speed = Convert.ToInt16(dr["Speed"]),
                            AxleCount = Convert.ToInt16(dr["AxleCount"]),

                            Axle01Seperation = Convert.ToInt16(dr["Axle01Seperation"]),
                            Axle01Weight = Convert.ToInt16(dr["Axle01Weight"]),
                            Axle01Max = Convert.ToInt16(dr["Axle01Max"]),
                            Axle01Group = Convert.ToInt16(dr["Axle01Group"]),
                            Axle01TireCode = Convert.ToInt16(dr["Axle01TireCode"]),
                            Axle02Seperation = Convert.ToInt16(dr["Axle02Seperation"]),
                            Axle02Weight = Convert.ToInt16(dr["Axle02Weight"]),
                            Axle02Max = Convert.ToInt16(dr["Axle02Max"]),
                            Axle02Group = Convert.ToInt16(dr["Axle02Group"]),
                            Axle02TireCode = Convert.ToInt16(dr["Axle02TireCode"]),
                            Axle03Seperation = Convert.ToInt16(dr["Axle03Seperation"]),
                            Axle03Weight = Convert.ToInt16(dr["Axle03Weight"]),
                            Axle03Max = Convert.ToInt16(dr["Axle03Max"]),
                            Axle03Group = Convert.ToInt16(dr["Axle03Group"]),
                            Axle03TireCode = Convert.ToInt16(dr["Axle03TireCode"]),
                            Axle04Seperation = Convert.ToInt16(dr["Axle04Seperation"]),
                            Axle04Weight = Convert.ToInt16(dr["Axle04Weight"]),
                            Axle04Max = Convert.ToInt16(dr["Axle04Max"]),
                            Axle04Group = Convert.ToInt16(dr["Axle04Group"]),
                            Axle04TireCode = Convert.ToInt16(dr["Axle04TireCode"]),
                            Axle05Seperation = Convert.ToInt16(dr["Axle05Seperation"]),
                            Axle05Weight = Convert.ToInt16(dr["Axle05Weight"]),
                            Axle05Max = Convert.ToInt16(dr["Axle05Max"]),
                            Axle05Group = Convert.ToInt16(dr["Axle05Group"]),
                            Axle05TireCode = Convert.ToInt16(dr["Axle05TireCode"]),
                            Axle06Seperation = Convert.ToInt16(dr["Axle06Seperation"]),
                            Axle06Weight = Convert.ToInt16(dr["Axle06Weight"]),
                            Axle06Max = Convert.ToInt16(dr["Axle06Max"]),
                            Axle06Group = Convert.ToInt16(dr["Axle06Group"]),
                            Axle06TireCode = Convert.ToInt16(dr["Axle06TireCode"]),
                            Axle07Seperation = Convert.ToInt16(dr["Axle07Seperation"]),
                            Axle07Weight = Convert.ToInt16(dr["Axle07Weight"]),
                            Axle07Max = Convert.ToInt16(dr["Axle07Max"]),
                            Axle07Group = Convert.ToInt16(dr["Axle07Group"]),
                            Axle07TireCode = Convert.ToInt16(dr["Axle07TireCode"]),
                            Axle08Seperation = Convert.ToInt16(dr["Axle08Seperation"]),
                            Axle08Weight = Convert.ToInt16(dr["Axle08Weight"]),
                            Axle08Max = Convert.ToInt16(dr["Axle08Max"]),
                            Axle08Group = Convert.ToInt16(dr["Axle08Group"]),
                            Axle08TireCode = Convert.ToInt16(dr["Axle08TireCode"]),
                            Axle09Seperation = Convert.ToInt16(dr["Axle09Seperation"]),
                            Axle09Weight = Convert.ToInt16(dr["Axle09Weight"]),
                            Axle09Max = Convert.ToInt16(dr["Axle09Max"]),
                            Axle09Group = Convert.ToInt16(dr["Axle09Group"]),
                            Axle09TireCode = Convert.ToInt16(dr["Axle09TireCode"]),
                            Axle10Seperation = Convert.ToInt16(dr["Axle10Seperation"]),
                            Axle10Weight = Convert.ToInt16(dr["Axle10Weight"]),
                            Axle10Max = Convert.ToInt16(dr["Axle10Max"]),
                            Axle10Group = Convert.ToInt16(dr["Axle10Group"]),
                            Axle10TireCode = Convert.ToInt16(dr["Axle10TireCode"]),
                            Axle11Seperation = Convert.ToInt16(dr["Axle11Seperation"]),
                            Axle11Weight = Convert.ToInt16(dr["Axle11Weight"]),
                            Axle11Max = Convert.ToInt16(dr["Axle11Max"]),
                            Axle11Group = Convert.ToInt16(dr["Axle11Group"]),
                            Axle11TireCode = Convert.ToInt16(dr["Axle11TireCode"]),
                            Axle12Seperation = Convert.ToInt16(dr["Axle12Seperation"]),
                            Axle12Weight = Convert.ToInt16(dr["Axle12Weight"]),
                            Axle12Max = Convert.ToInt16(dr["Axle12Max"]),
                            Axle12Group = Convert.ToInt16(dr["Axle12Group"]),
                            Axle12TireCode = Convert.ToInt16(dr["Axle12TireCode"]),
                            Axle13Seperation = Convert.ToInt16(dr["Axle13Seperation"]),
                            Axle13Weight = Convert.ToInt16(dr["Axle13Weight"]),
                            Axle13Max = Convert.ToInt16(dr["Axle13Max"]),
                            Axle13Group = Convert.ToInt16(dr["Axle13Group"]),
                            Axle13TireCode = Convert.ToInt16(dr["Axle13TireCode"]),
                            Axle14Seperation = Convert.ToInt16(dr["Axle14Seperation"]),
                            Axle14Weight = Convert.ToInt16(dr["Axle14Weight"]),
                            Axle14Max = Convert.ToInt16(dr["Axle14Max"]),
                            Axle14Group = Convert.ToInt16(dr["Axle14Group"]),
                            Axle14TireCode = Convert.ToInt16(dr["Axle14TireCode"]),
                            Length = Convert.ToInt16(dr["Length"]),
                            FrontOverHang = Convert.ToInt16(dr["FrontOverHang"]),
                            RearOverHang = Convert.ToInt16(dr["RearOverHang"]),
                            VehicleType = Convert.ToInt16(dr["VehicleType"]),
                            VehicleClass = Convert.ToInt16(dr["VehicleClass"]),
                            RecordType = Convert.ToInt16(dr["RecordType"]),
                            ImageCount = Convert.ToInt16(dr["ImageCount"]),
                            Image01Name = Convert.ToString(dr["Image01Name"]),
                            Image02Name = Convert.ToString(dr["Image02Name"]),
                            Image03Name = Convert.ToString(dr["Image03Name"]),
                            Image04Name = Convert.ToString(dr["Image04Name"]),
                            Image05Name = Convert.ToString(dr["Image05Name"]),
                            Image06Name = Convert.ToString(dr["Image06Name"]),
                            Image07Name = Convert.ToString(dr["Image07Name"]),
                            Image08Name = Convert.ToString(dr["Image08Name"]),
                            Image09Name = Convert.ToString(dr["Image09Name"]),
                            Image10Name = Convert.ToString(dr["Image10Name"]),
                            SortDecision = Convert.ToInt16(dr["SortDecision"]),
                            LicensePlateNumber = Convert.ToString(dr["LicensePlateNumber"]),
                            LicensePlateProvinceID = Convert.ToInt16(dr["LicensePlateProvinceID"]),
                            LicensePlateImageName = Convert.ToString(dr["LicensePlateImageName"]),
                            LicensePlateConfidence = Convert.ToInt16(dr["LicensePlateConfidence"]),

                        };
                        running++;
                        lists.Add(model);
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message);
                logger.Error(ex.StackTrace);
            }
            return lists;
        }

        //public List<ModelR211> selectR211_1(String period, String cri,String stationID)
        //{
        //    List<ModelR211> lists = new List<ModelR211>();
        //    using (SqlConnection con = new SqlConnection(conStr))
        //    {
        //        String periodSelect = "";
        //        String periodGroupBy = "";
        //        switch (period)
        //        {
        //            case "1": //ปี
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(YEAR, TimeStamp)";
        //                break;
        //            case "2": //เดือน
        //                periodSelect = "CAST(DATEPART(YEAR, TimeStamp) as varchar)+'-'+CAST(DATEPART(MONTH, TimeStamp)as varchar) as 'header'";//"CAST(DATEPART(MONTH, TimeStamp) as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY DATEPART(YEAR, TimeStamp),DATEPART(MONTH, TimeStamp)";
        //                break;
        //            case "3": //วัน
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(DAY, TimeStamp)";
        //                break;
        //            case "4": //ชม.
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar)+' '+CAST(DATEPART(HOUR, TimeStamp) as varchar)+':00' as 'header'";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(HOUR, TimeStamp)";
        //                break;
        //            case "5"://นาที 
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar)+' '+CAST(DATEPART(HOUR, TimeStamp) as varchar)  +':'+CAST(DATEPART(MINUTE, TimeStamp) as varchar)  as 'header'";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(HOUR, TimeStamp) ,DATEPART(MINUTE, TimeStamp)";
        //                break;
        //        }

        //        String sql =
        //                    "SELECT " + periodSelect + "," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 0 AND 10 THEN GVW ELSE NULL END)AS [C0_10]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 10 AND 20 THEN GVW ELSE NULL END)AS [C10_20]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 20 AND 30 THEN GVW ELSE NULL END)AS [C20_30]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 30 AND 40 THEN GVW ELSE NULL END)AS [C30_40]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 40 AND 50 THEN GVW ELSE NULL END)AS [C40_50]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 50 AND 60 THEN GVW ELSE NULL END)AS [C50_60]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 60 AND 70 THEN GVW ELSE NULL END)AS [C60_70]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 70 AND 80 THEN GVW ELSE NULL END)AS [C70_80]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 80 AND 90 THEN GVW ELSE NULL END)AS [C80_90]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 90 AND 100 THEN GVW ELSE NULL END)AS [C90_100]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 100 AND 110 THEN GVW ELSE NULL END)AS [C100_110]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 110 AND 120 THEN GVW ELSE NULL END)AS [C110_120]," +
        //                    "SUM(CASE WHEN GVW/1000 BETWEEN 120 AND 130 THEN GVW ELSE NULL END)AS [C120_130]," +
        //                    "SUM(CASE WHEN GVW/1000 > 130 THEN GVW ELSE NULL END)AS [C130]" +
        //                    String.Format(" FROM WIMData_{0}_2014.dbo.VehicleRecord",Convert.ToInt16(stationID).ToString("00")) + cri + periodGroupBy;


        //        SqlCommand cmd = new SqlCommand(sql, con);
        //        cmd.CommandType = CommandType.Text;
        //        con.Open();
        //        SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //        //int running = 1;
        //        while (dr.Read())
        //        {
        //            ModelR211 model = new ModelR211();
        //            model.h1 = (dr["header"] == DBNull.Value) ? "" : Convert.ToString(dr["header"]);
        //            model.dt = Convert.ToDateTime(model.h1);
        //            model.V0_10 = (dr["C0_10"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C0_10"]);
        //            model.V10_20 = (dr["C10_20"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C10_20"]);
        //            model.V20_30 = (dr["C20_30"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C20_30"]);
        //            model.V30_40 = (dr["C30_40"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C30_40"]);
        //            model.V40_50 = (dr["C40_50"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C40_50"]);
        //            model.V50_60 = (dr["C50_60"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C50_60"]);
        //            model.V60_70 = (dr["C60_70"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C60_70"]);
        //            model.V70_80 = (dr["C70_80"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C70_80"]);
        //            model.V80_90 = (dr["C80_90"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C80_90"]);
        //            model.V90_100 = (dr["C90_100"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C90_100"]);
        //            model.V100_110 = (dr["C100_110"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C100_110"]);
        //            model.V110_120 = (dr["C110_120"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C110_120"]);
        //            model.V120_130 = (dr["C120_130"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C120_130"]);
        //            model.V130_ = (dr["C130"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C130"]);
        //            lists.Add(model);
        //        }
        //    }
        //    return lists;
        //}
       
        //public List<ModelVehicleClass> selectR211_1_C(String period, String cri,String stationID)
        //{
        //    List<ModelVehicleClass> lists = new List<ModelVehicleClass>();
        //    using (SqlConnection con = new SqlConnection(conStr))
        //    {
        //        String periodSelect = "";
        //        String periodGroupBy = "";
        //        switch (period)
        //        {
        //            case "1": //ปี
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(YEAR, TimeStamp)";
        //                break;
        //            case "2": //เดือน
        //                periodSelect = "CAST(DATEPART(YEAR, TimeStamp) as varchar)+'-'+CAST(DATEPART(MONTH, TimeStamp)as varchar) as 'header'";//"CAST(DATEPART(MONTH, TimeStamp) as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY DATEPART(YEAR, TimeStamp),DATEPART(MONTH, TimeStamp)";
        //                break;
        //            case "3": //วัน
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(DAY, TimeStamp)";
        //                break;
        //            case "4": //ชม.
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar)+' '+CAST(DATEPART(HOUR, TimeStamp) as varchar)+':00' as 'header'";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(HOUR, TimeStamp)";
        //                break;
        //            case "5"://นาที 
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar)+' '+CAST(DATEPART(HOUR, TimeStamp) as varchar)  +':'+CAST(DATEPART(MINUTE, TimeStamp) as varchar)  as 'header'";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(HOUR, TimeStamp) ,DATEPART(MINUTE, TimeStamp)";
        //                break;
        //        }

        //        String sql =
        //                    "SELECT " + periodSelect + "," +
        //                    "SUM(CASE WHEN VehicleClass=1 THEN GVW ELSE NULL END)AS [C1]," +
        //                    "SUM(CASE WHEN VehicleClass=2 THEN GVW ELSE NULL END)AS [C2]," +
        //                    "SUM(CASE WHEN VehicleClass=3 THEN GVW ELSE NULL END)AS [C3]," +
        //                    "SUM(CASE WHEN VehicleClass=4 THEN GVW ELSE NULL END)AS [C4]," +
        //                    "SUM(CASE WHEN VehicleClass=5 THEN GVW ELSE NULL END)AS [C5]," +
        //                    "SUM(CASE WHEN VehicleClass=6 THEN GVW ELSE NULL END)AS [C6]," +
        //                    "SUM(CASE WHEN VehicleClass=7 THEN GVW ELSE NULL END)AS [C7]," +
        //                    "SUM(CASE WHEN VehicleClass=8 THEN GVW ELSE NULL END)AS [C8]," +
        //                    "SUM(CASE WHEN VehicleClass=9 THEN GVW ELSE NULL END)AS [C9]," +
        //                    "SUM(CASE WHEN VehicleClass=10 THEN GVW ELSE NULL END)AS [C10]," +
        //                    "SUM(CASE WHEN VehicleClass=11 THEN GVW ELSE NULL END)AS [C11]," +
        //                    "SUM(CASE WHEN VehicleClass=12 THEN GVW ELSE NULL END)AS [C12]," +
        //                    "SUM(CASE WHEN VehicleClass=13 THEN GVW ELSE NULL END)AS [C13]," +
        //                    "SUM(CASE WHEN VehicleClass=14 THEN GVW ELSE NULL END)AS [C14]," +
        //                    "SUM(CASE WHEN VehicleClass=15 THEN GVW ELSE NULL END)AS [C15] " +
        //                    String.Format(" FROM WIMData_{0}_2014.dbo.VehicleRecord", Convert.ToInt16(stationID).ToString("00")) + cri + periodGroupBy;


        //        SqlCommand cmd = new SqlCommand(sql, con);
        //        cmd.CommandType = CommandType.Text;
        //        con.Open();
        //        SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //        //int running = 1;
        //        while (dr.Read())
        //        {
        //            ModelVehicleClass model = new ModelVehicleClass();
        //            model.h1 = (dr["header"] == DBNull.Value) ? "" : Convert.ToString(dr["header"]);
        //            model.dt = Convert.ToDateTime(model.h1);
        //            model.C1 = (dr["C1"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C1"]);
        //            model.C2 = (dr["C2"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C2"]);
        //            model.C3 = (dr["C3"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C3"]);
        //            model.C4 = (dr["C4"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C4"]);
        //            model.C5 = (dr["C5"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C5"]);
        //            model.C6 = (dr["C6"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C6"]);
        //            model.C7 = (dr["C7"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C7"]);
        //            model.C8 = (dr["C8"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C8"]);
        //            model.C9 = (dr["C9"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C9"]);
        //            model.C10 = (dr["C10"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C10"]);
        //            model.C11 = (dr["C11"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C11"]);
        //            model.C12 = (dr["C12"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C12"]);
        //            model.C13 = (dr["C13"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C13"]);
        //            model.C14 = (dr["C14"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C14"]);
        //            model.C15 = (dr["C15"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C15"]);
        //            lists.Add(model);
        //        }
        //    }
        //    return lists;
        //}

        //public List<ModelVehicleClass> selectR212(String period, String cri,String stationID)
        //{
        //    List<ModelVehicleClass> lists = new List<ModelVehicleClass>();
        //    using (SqlConnection con = new SqlConnection(conStr))
        //    {
        //        String periodSelect = "";
        //        String periodGroupBy = "";
        //        switch (period)
        //        {
        //            case "1": //ปี
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(YEAR, TimeStamp)";
        //                break;
        //            case "2": //เดือน
        //                periodSelect = "CAST(DATEPART(YEAR, TimeStamp) as varchar)+'-'+CAST(DATEPART(MONTH, TimeStamp)as varchar) as 'header'";//"CAST(DATEPART(MONTH, TimeStamp) as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY DATEPART(YEAR, TimeStamp),DATEPART(MONTH, TimeStamp)";
        //                break;
        //            case "3": //วัน
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(DAY, TimeStamp)";
        //                break;
        //            case "4": //ชม.
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar)+' '+CAST(DATEPART(HOUR, TimeStamp) as varchar)+':00' as 'header'";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(HOUR, TimeStamp)";
        //                break;
        //            case "5"://นาที 
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar)+' '+CAST(DATEPART(HOUR, TimeStamp) as varchar)  +':'+CAST(DATEPART(MINUTE, TimeStamp) as varchar)  as 'header'";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(HOUR, TimeStamp) ,DATEPART(MINUTE, TimeStamp)";
        //                break;
        //        }

        //        String sql =
        //                    "SELECT " + periodSelect + "," +
        //                    "SUM(CASE WHEN VehicleClass=1 THEN GVW ELSE NULL END)AS [C1]," +
        //                    "SUM(CASE WHEN VehicleClass=2 THEN GVW ELSE NULL END)AS [C2]," +
        //                    "SUM(CASE WHEN VehicleClass=3 THEN GVW ELSE NULL END)AS [C3]," +
        //                    "SUM(CASE WHEN VehicleClass=4 THEN GVW ELSE NULL END)AS [C4]," +
        //                    "SUM(CASE WHEN VehicleClass=5 THEN GVW ELSE NULL END)AS [C5]," +
        //                    "SUM(CASE WHEN VehicleClass=6 THEN GVW ELSE NULL END)AS [C6]," +
        //                    "SUM(CASE WHEN VehicleClass=7 THEN GVW ELSE NULL END)AS [C7]," +
        //                    "SUM(CASE WHEN VehicleClass=8 THEN GVW ELSE NULL END)AS [C8]," +
        //                    "SUM(CASE WHEN VehicleClass=9 THEN GVW ELSE NULL END)AS [C9]," +
        //                    "SUM(CASE WHEN VehicleClass=10 THEN GVW ELSE NULL END)AS [C10]," +
        //                    "SUM(CASE WHEN VehicleClass=11 THEN GVW ELSE NULL END)AS [C11]," +
        //                    "SUM(CASE WHEN VehicleClass=12 THEN GVW ELSE NULL END)AS [C12]," +
        //                    "SUM(CASE WHEN VehicleClass=13 THEN GVW ELSE NULL END)AS [C13]," +
        //                    "SUM(CASE WHEN VehicleClass=14 THEN GVW ELSE NULL END)AS [C14]," +
        //                    "SUM(CASE WHEN VehicleClass=15 THEN GVW ELSE NULL END)AS [C15] " +
        //                    " FROM (select timestamp,VehicleClass,sum(GVW) gvw from " + String.Format(" WIMData_{0}_2014.dbo.VehicleRecord", Convert.ToInt16(stationID).ToString("00")) + " group by timestamp,VehicleClass) tmp " + cri + periodGroupBy;


        //        SqlCommand cmd = new SqlCommand(sql, con);
        //        cmd.CommandType = CommandType.Text;
        //        con.Open();
        //        SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //        //int running = 1;
        //        while (dr.Read())
        //        {
        //            ModelVehicleClass model = new ModelVehicleClass();
        //            model.h1 = (dr["header"] == DBNull.Value) ? "" : Convert.ToString(dr["header"]);
        //            model.C1 = (dr["C1"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C1"]);
        //            model.C2 = (dr["C2"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C2"]);
        //            model.C3 = (dr["C3"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C3"]);
        //            model.C4 = (dr["C4"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C4"]);
        //            model.C5 = (dr["C5"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C5"]);
        //            model.C6 = (dr["C6"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C6"]);
        //            model.C7 = (dr["C7"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C7"]);
        //            model.C8 = (dr["C8"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C8"]);
        //            model.C9 = (dr["C9"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C9"]);
        //            model.C10 = (dr["C10"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C10"]);
        //            model.C11 = (dr["C11"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C11"]);
        //            model.C12 = (dr["C12"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C12"]);
        //            model.C13 = (dr["C13"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C13"]);
        //            model.C14 = (dr["C14"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C14"]);
        //            model.C15 = (dr["C15"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C15"]);
        //            lists.Add(model);
        //        }
        //    }
        //    return lists;
        //}

        //public List<ModelVehicleClass> selectR221(String period, String cri, String stationID)
        //{
        //    List<ModelVehicleClass> lists = new List<ModelVehicleClass>();
        //    using (SqlConnection con = new SqlConnection(conStr))
        //    {
        //        String periodSelect = "";
        //        String periodGroupBy = "";
        //        switch (period)
        //        {
        //            case "1": //ปี
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(YEAR, TimeStamp)";
        //                break;
        //            case "2": //เดือน
        //                periodSelect = "CAST(DATEPART(YEAR, TimeStamp) as varchar)+'-'+CAST(DATEPART(MONTH, TimeStamp)as varchar) as 'header'";//"CAST(DATEPART(MONTH, TimeStamp) as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY DATEPART(YEAR, TimeStamp),DATEPART(MONTH, TimeStamp)";
        //                break;
        //            case "3": //วัน
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar) as 'header' ";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(DAY, TimeStamp)";
        //                break;
        //            case "4": //ชม.
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar)+' '+CAST(DATEPART(HOUR, TimeStamp) as varchar)+':00' as 'header'";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(HOUR, TimeStamp)";
        //                break;
        //            case "5"://นาที 
        //                periodSelect = "CAST(CAST(TimeStamp as DATE)as varchar)+' '+CAST(DATEPART(HOUR, TimeStamp) as varchar)  +':'+CAST(DATEPART(MINUTE, TimeStamp) as varchar)  as 'header'";
        //                periodGroupBy = " GROUP BY CAST(TimeStamp as DATE),DATEPART(HOUR, TimeStamp) ,DATEPART(MINUTE, TimeStamp)";
        //                break;
        //        }

        //        String sql =
        //                    "SELECT " + periodSelect + "," +
        //                    "COUNT(CASE WHEN VehicleClass=1 THEN GVW ELSE NULL END)AS [C1]," +
        //                    "COUNT(CASE WHEN VehicleClass=2 THEN GVW ELSE NULL END)AS [C2]," +
        //                    "COUNT(CASE WHEN VehicleClass=3 THEN GVW ELSE NULL END)AS [C3]," +
        //                    "COUNT(CASE WHEN VehicleClass=4 THEN GVW ELSE NULL END)AS [C4]," +
        //                    "COUNT(CASE WHEN VehicleClass=5 THEN GVW ELSE NULL END)AS [C5]," +
        //                    "COUNT(CASE WHEN VehicleClass=6 THEN GVW ELSE NULL END)AS [C6]," +
        //                    "COUNT(CASE WHEN VehicleClass=7 THEN GVW ELSE NULL END)AS [C7]," +
        //                    "COUNT(CASE WHEN VehicleClass=8 THEN GVW ELSE NULL END)AS [C8]," +
        //                    "COUNT(CASE WHEN VehicleClass=9 THEN GVW ELSE NULL END)AS [C9]," +
        //                    "COUNT(CASE WHEN VehicleClass=10 THEN GVW ELSE NULL END)AS [C10]," +
        //                    "COUNT(CASE WHEN VehicleClass=11 THEN GVW ELSE NULL END)AS [C11]," +
        //                    "COUNT(CASE WHEN VehicleClass=12 THEN GVW ELSE NULL END)AS [C12]," +
        //                    "COUNT(CASE WHEN VehicleClass=13 THEN GVW ELSE NULL END)AS [C13]," +
        //                    "COUNT(CASE WHEN VehicleClass=14 THEN GVW ELSE NULL END)AS [C14]," +
        //                    "COUNT(CASE WHEN VehicleClass=15 THEN GVW ELSE NULL END)AS [C15] " +
        //                    " FROM (select timestamp,VehicleClass,sum(GVW) gvw from " + String.Format("WIMData_{0}_2014.dbo.VehicleRecord", Convert.ToInt16(stationID).ToString("00")) + " group by timestamp,VehicleClass) tmp " + cri + periodGroupBy;


        //        SqlCommand cmd = new SqlCommand(sql, con);
        //        cmd.CommandType = CommandType.Text;
        //        con.Open();
        //        SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //        //int running = 1;
        //        while (dr.Read())
        //        {
        //            ModelVehicleClass model = new ModelVehicleClass();
        //            model.h1 = (dr["header"] == DBNull.Value) ? "" : Convert.ToString(dr["header"]);
        //            model.dt = Convert.ToDateTime(model.h1);
        //            model.C1 = (dr["C1"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C1"]);
        //            model.C2 = (dr["C2"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C2"]);
        //            model.C3 = (dr["C3"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C3"]);
        //            model.C4 = (dr["C4"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C4"]);
        //            model.C5 = (dr["C5"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C5"]);
        //            model.C6 = (dr["C6"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C6"]);
        //            model.C7 = (dr["C7"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C7"]);
        //            model.C8 = (dr["C8"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C8"]);
        //            model.C9 = (dr["C9"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C9"]);
        //            model.C10 = (dr["C10"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C10"]);
        //            model.C11 = (dr["C11"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C11"]);
        //            model.C12 = (dr["C12"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C12"]);
        //            model.C13 = (dr["C13"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C13"]);
        //            model.C14 = (dr["C14"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C14"]);
        //            model.C15 = (dr["C15"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["C15"]);
        //            lists.Add(model);
        //        }
        //    }
        //    return lists;
        //}






        //public List<ModelVehicleRecord> selectChartData(String cri)
        //{
        //    List<ModelVehicleRecord> lists = new List<ModelVehicleRecord>();
        //    using (SqlConnection con = new SqlConnection(Connection.conStr))
        //    {                 
        //        String sql =
        //                "select VehicleClass,TimeStamp,GVW" +
        //                " FROM VehicleRecord" +cri+
        //                //((eDate == null || eDate.Equals("")) ? " where TimeStamp is null or CONVERT(VARCHAR(8),TimeStamp,112) =" + Convert.ToDateTime(sDate).ToString("yyyyMMdd") : " where CONVERT(VARCHAR(8),TimeStamp,112) between " + Convert.ToDateTime(sDate).ToString("yyyyMMdd") + " and " + Convert.ToDateTime(eDate).ToString("yyyyMMdd")) +
        //                " order by TimeStamp asc,VehicleClass asc";

        //        SqlCommand cmd = new SqlCommand(sql, con);
        //        cmd.CommandType = CommandType.Text;
        //        con.Open();
        //        SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //        //int running = 1;
        //        while (dr.Read())
        //        {
        //            ModelVehicleRecord model = new ModelVehicleRecord();
        //            model.TimeStamp = (dr["TimeStamp"] == DBNull.Value) ? DateTime.MaxValue : Convert.ToDateTime(dr["TimeStamp"]);
        //            model.VehicleClass = (dr["VehicleClass"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["VehicleClass"]);
        //            model.GVW = (dr["GVW"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["GVW"]);

        //            lists.Add(model);
        //        }
        //    }
        //    return lists;
        //}

        //public List<ModelVehicleRecord> selectYearMonth(String year, String month)
        //{
        //    List<ModelVehicleRecord> lists = new List<ModelVehicleRecord>();
        //    using (SqlConnection con = new SqlConnection(Connection.conStr))
        //    {
        //        String sql =
        //                "SELECT vehicleName,TimeStamp,GVW" +
        //                " FROM VehicleRecord" +
        //                ((month.Equals("") || month == null) ? " where TimeStamp is null or DATEPART(YEAR,TimeStamp) = " + year :
        //                " where DATEPART(YEAR,TimeStamp)= " + year + " and DATEPART(MONTH,TimeStamp)= " + month) +
        //                " ORDER BY TimeStamp asc,vehicleName asc";

        //        SqlCommand cmd = new SqlCommand(sql, con);
        //        cmd.CommandType = CommandType.Text;
        //        con.Open();
        //        SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //        //int running = 1;
        //        while (dr.Read())
        //        {
        //            ModelVehicleRecord model = new ModelVehicleRecord();
        //            model.TimeStamp = (dr["TimeStamp"] == DBNull.Value) ? DateTime.MaxValue : Convert.ToDateTime(dr["TimeStamp"]);
        //            model.VehicleClass = (dr["vehicleName"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["vehicleName"]);
        //            model.GVW = (dr["GVW"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["GVW"]);

        //            lists.Add(model);
        //        }
        //    }
        //    return lists;
        //}

        public ModelVehicleRecord getByTimeStamp(DateTime dt, String stationID)
        {

            
            List<ModelVehicleRecord> lists = new List<ModelVehicleRecord>();

            using (SqlConnection con = new SqlConnection(conStr))
            {
                string sql = String.Format("SELECT * FROM WIMData_{0}_2014.dbo.VehicleRecord WHERE convert(varchar,TimeStamp,120) = '" + dt.ToString("yyyy'-'MM'-'dd HH':'mm':'ss") + "'",Convert.ToInt16(stationID).ToString("00"));
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    ModelVehicleRecord model = new ModelVehicleRecord
                    {
                        Id = Convert.ToInt64(dr["ID"]),
                        StationID = Convert.ToInt16(dr["StationID"]),
                        TimeStamp = Convert.ToDateTime(dr["TimeStamp"]),
                        VehicleNumber = Convert.ToInt16(dr["VehicleNumber"]),
                        Lane = Convert.ToInt16(dr["Lane"]),
                        Error = Convert.ToInt16(dr["Error"]),
                        StatusCode = Convert.ToInt16(dr["StatusCode"]),

                        GVW = Convert.ToInt32(dr["GVW"]),

                        MaxGVW = Convert.ToInt32(dr["MaxGVW"]),
                        ESAL = Convert.ToDouble(dr["ESAL"]),
                        Speed = Convert.ToInt16(dr["Speed"]),
                        AxleCount = Convert.ToInt16(dr["AxleCount"]),

                        Axle01Seperation = Convert.ToInt16(dr["Axle01Seperation"]),
                        Axle01Weight = Convert.ToInt16(dr["Axle01Weight"]),
                        Axle01Max = Convert.ToInt16(dr["Axle01Max"]),
                        Axle01Group = Convert.ToInt16(dr["Axle01Group"]),
                        Axle01TireCode = Convert.ToInt16(dr["Axle01TireCode"]),
                        Axle02Seperation = Convert.ToInt16(dr["Axle02Seperation"]),
                        Axle02Weight = Convert.ToInt16(dr["Axle02Weight"]),
                        Axle02Max = Convert.ToInt16(dr["Axle02Max"]),
                        Axle02Group = Convert.ToInt16(dr["Axle02Group"]),
                        Axle02TireCode = Convert.ToInt16(dr["Axle02TireCode"]),
                        Axle03Seperation = Convert.ToInt16(dr["Axle03Seperation"]),
                        Axle03Weight = Convert.ToInt16(dr["Axle03Weight"]),
                        Axle03Max = Convert.ToInt16(dr["Axle03Max"]),
                        Axle03Group = Convert.ToInt16(dr["Axle03Group"]),
                        Axle03TireCode = Convert.ToInt16(dr["Axle03TireCode"]),
                        Axle04Seperation = Convert.ToInt16(dr["Axle04Seperation"]),
                        Axle04Weight = Convert.ToInt16(dr["Axle04Weight"]),
                        Axle04Max = Convert.ToInt16(dr["Axle04Max"]),
                        Axle04Group = Convert.ToInt16(dr["Axle04Group"]),
                        Axle04TireCode = Convert.ToInt16(dr["Axle04TireCode"]),
                        Axle05Seperation = Convert.ToInt16(dr["Axle05Seperation"]),
                        Axle05Weight = Convert.ToInt16(dr["Axle05Weight"]),
                        Axle05Max = Convert.ToInt16(dr["Axle05Max"]),
                        Axle05Group = Convert.ToInt16(dr["Axle05Group"]),
                        Axle05TireCode = Convert.ToInt16(dr["Axle05TireCode"]),
                        Axle06Seperation = Convert.ToInt16(dr["Axle06Seperation"]),
                        Axle06Weight = Convert.ToInt16(dr["Axle06Weight"]),
                        Axle06Max = Convert.ToInt16(dr["Axle06Max"]),
                        Axle06Group = Convert.ToInt16(dr["Axle06Group"]),
                        Axle06TireCode = Convert.ToInt16(dr["Axle06TireCode"]),
                        Axle07Seperation = Convert.ToInt16(dr["Axle07Seperation"]),
                        Axle07Weight = Convert.ToInt16(dr["Axle07Weight"]),
                        Axle07Max = Convert.ToInt16(dr["Axle07Max"]),
                        Axle07Group = Convert.ToInt16(dr["Axle07Group"]),
                        Axle07TireCode = Convert.ToInt16(dr["Axle07TireCode"]),
                        Axle08Seperation = Convert.ToInt16(dr["Axle08Seperation"]),
                        Axle08Weight = Convert.ToInt16(dr["Axle08Weight"]),
                        Axle08Max = Convert.ToInt16(dr["Axle08Max"]),
                        Axle08Group = Convert.ToInt16(dr["Axle08Group"]),
                        Axle08TireCode = Convert.ToInt16(dr["Axle08TireCode"]),
                        Axle09Seperation = Convert.ToInt16(dr["Axle09Seperation"]),
                        Axle09Weight = Convert.ToInt16(dr["Axle09Weight"]),
                        Axle09Max = Convert.ToInt16(dr["Axle09Max"]),
                        Axle09Group = Convert.ToInt16(dr["Axle09Group"]),
                        Axle09TireCode = Convert.ToInt16(dr["Axle09TireCode"]),
                        Axle10Seperation = Convert.ToInt16(dr["Axle10Seperation"]),
                        Axle10Weight = Convert.ToInt16(dr["Axle10Weight"]),
                        Axle10Max = Convert.ToInt16(dr["Axle10Max"]),
                        Axle10Group = Convert.ToInt16(dr["Axle10Group"]),
                        Axle10TireCode = Convert.ToInt16(dr["Axle10TireCode"]),
                        Axle11Seperation = Convert.ToInt16(dr["Axle11Seperation"]),
                        Axle11Weight = Convert.ToInt16(dr["Axle11Weight"]),
                        Axle11Max = Convert.ToInt16(dr["Axle11Max"]),
                        Axle11Group = Convert.ToInt16(dr["Axle11Group"]),
                        Axle11TireCode = Convert.ToInt16(dr["Axle11TireCode"]),
                        Axle12Seperation = Convert.ToInt16(dr["Axle12Seperation"]),
                        Axle12Weight = Convert.ToInt16(dr["Axle12Weight"]),
                        Axle12Max = Convert.ToInt16(dr["Axle12Max"]),
                        Axle12Group = Convert.ToInt16(dr["Axle12Group"]),
                        Axle12TireCode = Convert.ToInt16(dr["Axle12TireCode"]),
                        Axle13Seperation = Convert.ToInt16(dr["Axle13Seperation"]),
                        Axle13Weight = Convert.ToInt16(dr["Axle13Weight"]),
                        Axle13Max = Convert.ToInt16(dr["Axle13Max"]),
                        Axle13Group = Convert.ToInt16(dr["Axle13Group"]),
                        Axle13TireCode = Convert.ToInt16(dr["Axle13TireCode"]),
                        Axle14Seperation = Convert.ToInt16(dr["Axle14Seperation"]),
                        Axle14Weight = Convert.ToInt16(dr["Axle14Weight"]),
                        Axle14Max = Convert.ToInt16(dr["Axle14Max"]),
                        Axle14Group = Convert.ToInt16(dr["Axle14Group"]),
                        Axle14TireCode = Convert.ToInt16(dr["Axle14TireCode"]),
                        Length = Convert.ToInt16(dr["Length"]),
                        FrontOverHang = Convert.ToInt16(dr["FrontOverHang"]),
                        RearOverHang = Convert.ToInt16(dr["RearOverHang"]),
                        VehicleType = Convert.ToInt16(dr["VehicleType"]),
                        VehicleClass = Convert.ToInt16(dr["VehicleClass"]),
                        RecordType = Convert.ToInt16(dr["RecordType"]),
                        ImageCount = Convert.ToInt16(dr["ImageCount"]),
                        Image01Name = Convert.ToString(dr["Image01Name"]),
                        Image02Name = Convert.ToString(dr["Image02Name"]),
                        Image03Name = Convert.ToString(dr["Image03Name"]),
                        Image04Name = Convert.ToString(dr["Image04Name"]),
                        Image05Name = Convert.ToString(dr["Image05Name"]),
                        Image06Name = Convert.ToString(dr["Image06Name"]),
                        Image07Name = Convert.ToString(dr["Image07Name"]),
                        Image08Name = Convert.ToString(dr["Image08Name"]),
                        Image09Name = Convert.ToString(dr["Image09Name"]),
                        Image10Name = Convert.ToString(dr["Image10Name"]),
                        SortDecision = Convert.ToInt16(dr["SortDecision"]),
                        LicensePlateNumber = Convert.ToString(dr["LicensePlateNumber"]),
                        LicensePlateProvinceID = Convert.ToInt16(dr["LicensePlateProvinceID"]),
                        LicensePlateImageName = Convert.ToString(dr["LicensePlateImageName"]),
                        LicensePlateConfidence = Convert.ToInt16(dr["LicensePlateConfidence"]),

                    };
                    running++;
                    lists.Add(model);
                }
                dr.Close();
            }
            return lists[0];
        }

        //Insert,Delete,Update
        public int insert(ModelVehicleRecord model)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(conStr))
            {
                String sql = "INSERT INTO [VehicleRecord]" +
           "([StationID]" +
           ",[TimeStamp]" +
           ",[VehicleNumber]" +
           ",[Lane]" +
           ",[Error]" +
           ",[StatusCode]" +
           ",[GVW]" +
           ",[MaxGVW]" +
           ",[ESAL]" +
           ",[Speed]" +
           ",[AxleCount]" +
           ",[Axle01Seperation]" +
           ",[Axle01Weight]" +
           ",[Axle01Max]" +
           ",[Axle01Group]" +
           ",[Axle01TireCode]" +
           ",[Axle02Seperation]" +
           ",[Axle02Weight]" +
           ",[Axle02Max]" +
           ",[Axle02Group]" +
           ",[Axle02TireCode]" +
           ",[Axle03Seperation]" +
           ",[Axle03Weight]" +
           ",[Axle03Max]" +
           ",[Axle03Group]" +
           ",[Axle03TireCode]" +
           ",[Axle04Seperation]" +
           ",[Axle04Weight]" +
           ",[Axle04Max]" +
           ",[Axle04Group]" +
           ",[Axle04TireCode]" +
           ",[Axle05Seperation]" +
           ",[Axle05Weight]" +
           ",[Axle05Max]" +
           ",[Axle05Group]" +
           ",[Axle05TireCode]" +
           ",[Axle06Seperation]" +
           ",[Axle06Weight]" +
           ",[Axle06Max]" +
           ",[Axle06Group]" +
           ",[Axle06TireCode]" +
           ",[Axle07Seperation]" +
           ",[Axle07Weight]" +
           ",[Axle07Max]" +
           ",[Axle07Group]" +
           ",[Axle07TireCode]" +
           ",[Axle08Seperation]" +
           ",[Axle08Weight]" +
           ",[Axle08Max]" +
           ",[Axle08Group]" +
           ",[Axle08TireCode]" +
           ",[Axle09Seperation]" +
           ",[Axle09Weight]" +
           ",[Axle09Max]" +
           ",[Axle09Group]" +
           ",[Axle09TireCode]" +
           ",[Axle10Seperation]" +
           ",[Axle10Weight]" +
           ",[Axle10Max]" +
           ",[Axle10Group]" +
           ",[Axle10TireCode]" +
           ",[Axle11Seperation]" +
           ",[Axle11Weight]" +
           ",[Axle11Max]" +
           ",[Axle11Group]" +
           ",[Axle11TireCode]" +
           ",[Axle12Seperation]" +
           ",[Axle12Weight]" +
           ",[Axle12Max]" +
           ",[Axle12Group]" +
           ",[Axle12TireCode]" +
           ",[Axle13Seperation]" +
           ",[Axle13Weight]" +
           ",[Axle13Max]" +
           ",[Axle13Group]" +
           ",[Axle13TireCode]" +
           ",[Axle14Seperation]" +
           ",[Axle14Weight]" +
           ",[Axle14Max]" +
           ",[Axle14Group]" +
           ",[Axle14TireCode]" +
           ",[Length]" +
           ",[FrontOverHang]" +
           ",[RearOverHang]" +
           ",[VehicleType]" +
           ",[VehicleClass]" +
           ",[RecordType]" +
           ",[ImageCount]" +
           ",[Image01Name]" +
           ",[Image02Name]" +
           ",[Image03Name]" +
           ",[Image04Name]" +
           ",[Image05Name]" +
           ",[Image06Name]" +
           ",[Image07Name]" +
           ",[Image08Name]" +
           ",[Image09Name]" +
           ",[Image10Name]" +
           ",[SortDecision]" +
           ",[LicensePlateNumber]" +
           ",[LicensePlateProvinceID]" +
           ",[LicensePlateImageName]" +
           ",[LicensePlateConfidence])" +
    " VALUES" +
           "@StationID," +
           "@TimeStamp," +
           "@VehicleNumber," +
           "@Lane," +
           "@Error," +
           "@StatusCode," +
           "@GVW," +
           "@MaxGVW," +
           "@ESAL," +
           "@Speed," +
           "@AxleCount," +
           "@Axle01Seperation," +
           "@Axle01Weight," +
           "@Axle01Max," +
           "@Axle01Group," +
           "@Axle01TireCode," +
           "@Axle02Seperation," +
           "@Axle02Weight," +
           "@Axle02Max," +
           "@Axle02Group," +
           "@Axle02TireCode," +
           "@Axle03Seperation," +
           "@Axle03Weight," +
           "@Axle03Max," +
           "@Axle03Group," +
           "@Axle03TireCode," +
           "@Axle04Seperation," +
           "@Axle04Weight," +
           "@Axle04Max," +
           "@Axle04Group," +
           "@Axle04TireCode," +
           "@Axle05Seperation," +
           "@Axle05Weight," +
           "@Axle05Max," +
           "@Axle05Group," +
           "@Axle05TireCode," +
           "@Axle06Seperation," +
           "@Axle06Weight," +
           "@Axle06Max," +
           "@Axle06Group," +
           "@Axle06TireCode," +
           "@Axle07Seperation," +
           "@Axle07Weight," +
           "@Axle07Max," +
           "@Axle07Group," +
           "@Axle07TireCode," +
           "@Axle08Seperation," +
           "@Axle08Weight," +
           "@Axle08Max," +
           "@Axle08Group," +
           "@Axle08TireCode," +
           "@Axle09Seperation," +
           "@Axle09Weight," +
           "@Axle09Max," +
           "@Axle09Group," +
           "@Axle09TireCode," +
           "@Axle10Seperation," +
           "@Axle10Weight," +
           "@Axle10Max," +
           "@Axle10Group," +
           "@Axle10TireCode," +
           "@Axle11Seperation," +
           "@Axle11Weight," +
           "@Axle11Max," +
           "@Axle11Group," +
           "@Axle11TireCode," +
           "@Axle12Seperation," +
           "@Axle12Weight," +
           "@Axle12Max," +
           "@Axle12Group," +
           "@Axle12TireCode," +
           "@Axle13Seperation," +
           "@Axle13Weight," +
           "@Axle13Max," +
           "@Axle13Group," +
           "@Axle13TireCode," +
           "@Axle14Seperation," +
           "@Axle14Weight," +
           "@Axle14Max," +
           "@Axle14Group," +
           "@Axle14TireCode," +
           "@Length," +
           "@FrontOverHang," +
           "@RearOverHang," +
           "@VehicleType," +
           "@VehicleClass," +
           "@RecordType," +
           "@ImageCount," +
           "@Image01Name," +
           "@Image02Name," +
           "@Image03Name," +
           "@Image04Name," +
           "@Image05Name," +
           "@Image06Name," +
           "@Image07Name," +
           "@Image08Name," +
           "@Image09Name," +
           "@Image10Name," +
           "@SortDecision," +
           "@LicensePlateNumber," +
           "@LicensePlateProvinceID," +
           "@LicensePlateImageName," +
           "@LicensePlateConfidence)";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter(@"StationID", model.StationID));
                cmd.Parameters.Add(new SqlParameter(@"TimeStamp", model.TimeStamp));
                cmd.Parameters.Add(new SqlParameter(@"VehicleNumber", model.VehicleNumber));
                cmd.Parameters.Add(new SqlParameter(@"Lane", model.Lane));
                cmd.Parameters.Add(new SqlParameter(@"Error", model.Error));
                cmd.Parameters.Add(new SqlParameter(@"StatusCode", model.StatusCode));
                cmd.Parameters.Add(new SqlParameter(@"GVW", model.GVW));
                cmd.Parameters.Add(new SqlParameter(@"MaxGVW", model.MaxGVW));
                cmd.Parameters.Add(new SqlParameter(@"ESAL", model.ESAL));
                cmd.Parameters.Add(new SqlParameter(@"Speed", model.Speed));
                cmd.Parameters.Add(new SqlParameter(@"AxleCount", model.AxleCount));
                cmd.Parameters.Add(new SqlParameter(@"Axle01Seperation", model.Axle01Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle01Weight", model.Axle01Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle01Max", model.Axle01Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle01Group", model.Axle01Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle01TireCode", model.Axle01TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle02Seperation", model.Axle02Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle02Weight", model.Axle02Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle02Max", model.Axle02Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle02Group", model.Axle02Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle02TireCode", model.Axle02TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle03Seperation", model.Axle03Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle03Weight", model.Axle03Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle03Max", model.Axle03Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle03Group", model.Axle03Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle03TireCode", model.Axle03TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle04Seperation", model.Axle04Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle04Weight", model.Axle04Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle04Max", model.Axle04Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle04Group", model.Axle04Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle04TireCode", model.Axle04TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle05Seperation", model.Axle05Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle05Weight", model.Axle05Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle05Max", model.Axle05Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle05Group", model.Axle05Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle05TireCode", model.Axle05TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle06Seperation", model.Axle06Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle06Weight", model.Axle06Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle06Max", model.Axle06Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle06Group", model.Axle06Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle06TireCode", model.Axle06TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle07Seperation", model.Axle07Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle07Weight", model.Axle07Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle07Max", model.Axle07Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle07Group", model.Axle07Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle07TireCode", model.Axle07TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle08Seperation", model.Axle08Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle08Weight", model.Axle08Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle08Max", model.Axle08Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle08Group", model.Axle08Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle08TireCode", model.Axle08TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle09Seperation", model.Axle09Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle09Weight", model.Axle09Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle09Max", model.Axle09Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle09Group", model.Axle09Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle09TireCode", model.Axle09TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle10Seperation", model.Axle10Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle10Weight", model.Axle10Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle10Max", model.Axle10Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle10Group", model.Axle10Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle10TireCode", model.Axle10TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle11Seperation", model.Axle11Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle11Weight", model.Axle11Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle11Max", model.Axle11Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle11Group", model.Axle11Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle11TireCode", model.Axle11TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle12Seperation", model.Axle12Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle12Weight", model.Axle12Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle12Max", model.Axle12Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle12Group", model.Axle12Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle12TireCode", model.Axle12TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle13Seperation", model.Axle13Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle13Weight", model.Axle13Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle13Max", model.Axle13Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle13Group", model.Axle13Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle13TireCode", model.Axle13TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Axle14Seperation", model.Axle14Seperation));
                cmd.Parameters.Add(new SqlParameter(@"Axle14Weight", model.Axle14Weight));
                cmd.Parameters.Add(new SqlParameter(@"Axle14Max", model.Axle14Max));
                cmd.Parameters.Add(new SqlParameter(@"Axle14Group", model.Axle14Group));
                cmd.Parameters.Add(new SqlParameter(@"Axle14TireCode", model.Axle14TireCode));
                cmd.Parameters.Add(new SqlParameter(@"Length", model.Length));
                cmd.Parameters.Add(new SqlParameter(@"FrontOverHang", model.FrontOverHang));
                cmd.Parameters.Add(new SqlParameter(@"RearOverHang", model.RearOverHang));
                cmd.Parameters.Add(new SqlParameter(@"VehicleType", model.VehicleType));
                cmd.Parameters.Add(new SqlParameter(@"VehicleClass", model.VehicleClass));
                cmd.Parameters.Add(new SqlParameter(@"RecordType", model.RecordType));
                cmd.Parameters.Add(new SqlParameter(@"ImageCount", model.ImageCount));
                cmd.Parameters.Add(new SqlParameter(@"Image01Name", model.Image01Name));
                cmd.Parameters.Add(new SqlParameter(@"Image02Name", model.Image02Name));
                cmd.Parameters.Add(new SqlParameter(@"Image03Name", model.Image03Name));
                cmd.Parameters.Add(new SqlParameter(@"Image04Name", model.Image04Name));
                cmd.Parameters.Add(new SqlParameter(@"Image05Name", model.Image05Name));
                cmd.Parameters.Add(new SqlParameter(@"Image06Name", model.Image06Name));
                cmd.Parameters.Add(new SqlParameter(@"Image07Name", model.Image07Name));
                cmd.Parameters.Add(new SqlParameter(@"Image08Name", model.Image08Name));
                cmd.Parameters.Add(new SqlParameter(@"Image09Name", model.Image09Name));
                cmd.Parameters.Add(new SqlParameter(@"Image10Name", model.Image10Name));
                cmd.Parameters.Add(new SqlParameter(@"SortDecision", model.SortDecision));
                cmd.Parameters.Add(new SqlParameter(@"LicensePlateNumber", model.LicensePlateNumber));
                cmd.Parameters.Add(new SqlParameter(@"LicensePlateProvinceID", model.LicensePlateProvinceID));
                cmd.Parameters.Add(new SqlParameter(@"LicensePlateImageName", model.LicensePlateImageName));
                cmd.Parameters.Add(new SqlParameter(@"LicensePlateConfidence", model.LicensePlateConfidence));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
    
    
    }
}


