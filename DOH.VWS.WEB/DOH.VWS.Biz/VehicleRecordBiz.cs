﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DOH.VWS.Dao;
using DOH.VWS.Model;

namespace DOH.VWS.Biz
{
    public class VehicleRecordBiz : IDisposable
    {

        /// <summary>
        /// This example method generates a DataTable.
        /// </summary>
        public DataTable getCompliance(int id)
        {
            /*                        
              * select VehicleNumber,VehicleClass,Length,AxleCount,Speed,GVW,MaxGVW,                        
              * Axle01Seperation,
              * '' LeftWeight,
              * '' RightWeight,
              * Axle01Weight 'TotalWeight',
              * '0' AllowableWeight,
              * '' WeightViolation,
              * '' GroupType,
              * ''GroupWeight ,
              * '' GroupAllowableWeight
              from VehicleRecord 
             where VehicleNumber = 21084
             order by TimeStamp desc;
              */
            DataTable table = new DataTable();
            table.Columns.Add("Axle", typeof(int));
            table.Columns.Add("Separation", typeof(int));
            table.Columns.Add("LeftWeight", typeof(int));
            table.Columns.Add("RightWeight", typeof(int));
            table.Columns.Add("TotalWeight", typeof(int));
            table.Columns.Add("AllowableWeight", typeof(int));
            table.Columns.Add("WeightViolation", typeof(int));
            table.Columns.Add("GroupType", typeof(string));
            table.Columns.Add("GroupWeight", typeof(int));
            table.Columns.Add("GroupAllowableWeight", typeof(int));

            VehicleRecordDAO dao = new VehicleRecordDAO();
            ModelVehicleRecord model = dao.getByPK(id);

            if (model != null)
            {
                switch (model.AxleCount)
                { 


                    case 1:
                        table.Rows.Add(1, model.Axle01Seperation, 0, 0, model.Axle01Weight,0, 0, "", 0, 0);
                        break;
                    case 2:
                        table.Rows.Add(1, model.Axle01Seperation, 0, 0, model.Axle01Weight,0, 0, "", 0, 0);
                        table.Rows.Add(2, model.Axle02Seperation, 0, 0, model.Axle02Weight,0, 0, "", 0, 0);
                        break;
                    case 3:
                        table.Rows.Add(1, model.Axle01Seperation, 0, 0, model.Axle01Weight,0, 0, "", 0, 0);
                        table.Rows.Add(2, model.Axle02Seperation, 0, 0, model.Axle02Weight,0, 0, "", 0, 0);
                        table.Rows.Add(3, model.Axle03Seperation, 0, 0, model.Axle03Weight,0, 0, "", 0, 0);
                        break;
                    case 4:
                        table.Rows.Add(1, model.Axle01Seperation, 0, 0, model.Axle01Weight,0, 0, "", 0, 0);
                        table.Rows.Add(2, model.Axle02Seperation, 0, 0, model.Axle02Weight,0, 0, "", 0, 0);
                        table.Rows.Add(3, model.Axle03Seperation, 0, 0, model.Axle03Weight,0, 0, "", 0, 0);
                        table.Rows.Add(4, model.Axle04Seperation, 0, 0, model.Axle04Weight,0, 0, "", 0, 0);
                        break;
                    case 5:
                        table.Rows.Add(1, model.Axle01Seperation, 0, 0, model.Axle01Weight,0, 0, "", 0, 0);
                        table.Rows.Add(2, model.Axle02Seperation, 0, 0, model.Axle02Weight,0, 0, "", 0, 0);
                        table.Rows.Add(3, model.Axle03Seperation, 0, 0, model.Axle03Weight,0, 0, "", 0, 0);
                        table.Rows.Add(4, model.Axle04Seperation, 0, 0, model.Axle04Weight,0, 0, "", 0, 0);
                        table.Rows.Add(5, model.Axle05Seperation, 0, 0, model.Axle05Weight,0, 0, "", 0, 0);
                        break;
                    case 6:
                        table.Rows.Add(1, model.Axle01Seperation, 0, 0, model.Axle01Weight,0, 0, "", 0, 0);
                        table.Rows.Add(2, model.Axle02Seperation, 0, 0, model.Axle02Weight,0, 0, "", 0, 0);
                        table.Rows.Add(3, model.Axle03Seperation, 0, 0, model.Axle03Weight,0, 0, "", 0, 0);
                        table.Rows.Add(4, model.Axle04Seperation, 0, 0, model.Axle04Weight,0, 0, "", 0, 0);
                        table.Rows.Add(5, model.Axle05Seperation, 0, 0, model.Axle05Weight,0, 0, "", 0, 0);
                        table.Rows.Add(6, model.Axle06Seperation, 0, 0, model.Axle06Weight,0, 0, "", 0, 0);
                        break;                      
                    case 7:
                        table.Rows.Add(1, model.Axle01Seperation, 0, 0, model.Axle01Weight,0, 0, "", 0, 0);
                        table.Rows.Add(2, model.Axle02Seperation, 0, 0, model.Axle02Weight,0, 0, "", 0, 0);
                        table.Rows.Add(3, model.Axle03Seperation, 0, 0, model.Axle03Weight,0, 0, "", 0, 0);
                        table.Rows.Add(4, model.Axle04Seperation, 0, 0, model.Axle04Weight,0, 0, "", 0, 0);
                        table.Rows.Add(5, model.Axle05Seperation, 0, 0, model.Axle05Weight,0, 0, "", 0, 0);
                        table.Rows.Add(6, model.Axle06Seperation, 0, 0, model.Axle06Weight,0, 0, "", 0, 0);
                        table.Rows.Add(7, model.Axle07Seperation, 0, 0, model.Axle07Weight,0, 0, "", 0, 0);
                        break;
                    case 8:
                        table.Rows.Add(1, model.Axle01Seperation, 0, 0, model.Axle01Weight,0, 0, "", 0, 0);
                        table.Rows.Add(2, model.Axle02Seperation, 0, 0, model.Axle02Weight,0, 0, "", 0, 0);
                        table.Rows.Add(3, model.Axle03Seperation, 0, 0, model.Axle03Weight,0, 0, "", 0, 0);
                        table.Rows.Add(4, model.Axle04Seperation, 0, 0, model.Axle04Weight,0, 0, "", 0, 0);
                        table.Rows.Add(5, model.Axle05Seperation, 0, 0, model.Axle05Weight,0, 0, "", 0, 0);
                        table.Rows.Add(6, model.Axle06Seperation, 0, 0, model.Axle06Weight,0, 0, "", 0, 0);
                        table.Rows.Add(7, model.Axle07Seperation, 0, 0, model.Axle07Weight,0, 0, "", 0, 0);
                        table.Rows.Add(8, model.Axle08Seperation, 0, 0, model.Axle08Weight,0, 0, "", 0, 0);
                        break;
                    case 9:
                        table.Rows.Add(1, model.Axle01Seperation, 0, 0, model.Axle01Weight,0, 0, "", 0, 0);
                        table.Rows.Add(2, model.Axle02Seperation, 0, 0, model.Axle02Weight,0, 0, "", 0, 0);
                        table.Rows.Add(3, model.Axle03Seperation, 0, 0, model.Axle03Weight,0, 0, "", 0, 0);
                        table.Rows.Add(4, model.Axle04Seperation, 0, 0, model.Axle04Weight,0, 0, "", 0, 0);
                        table.Rows.Add(5, model.Axle05Seperation, 0, 0, model.Axle05Weight,0, 0, "", 0, 0);
                        table.Rows.Add(6, model.Axle06Seperation, 0, 0, model.Axle06Weight,0, 0, "", 0, 0);
                        table.Rows.Add(7, model.Axle07Seperation, 0, 0, model.Axle07Weight,0, 0, "", 0, 0);
                        table.Rows.Add(8, model.Axle08Seperation, 0, 0, model.Axle08Weight,0, 0, "", 0, 0);
                        table.Rows.Add(9, model.Axle09Seperation, 0, 0, model.Axle09Weight,0, 0, "", 0, 0);
                        break;
                    case 10: 
                        table.Rows.Add(1, model.Axle01Seperation, 0, 0, model.Axle01Weight,0, 0, "", 0, 0);
                        table.Rows.Add(2, model.Axle02Seperation, 0, 0, model.Axle02Weight,0, 0, "", 0, 0);
                        table.Rows.Add(3, model.Axle03Seperation, 0, 0, model.Axle03Weight,0, 0, "", 0, 0);
                        table.Rows.Add(4, model.Axle04Seperation, 0, 0, model.Axle04Weight,0, 0, "", 0, 0);
                        table.Rows.Add(5, model.Axle05Seperation, 0, 0, model.Axle05Weight,0, 0, "", 0, 0);
                        table.Rows.Add(6, model.Axle06Seperation, 0, 0, model.Axle06Weight,0, 0, "", 0, 0);
                        table.Rows.Add(7, model.Axle07Seperation, 0, 0, model.Axle07Weight,0, 0, "", 0, 0);
                        table.Rows.Add(8, model.Axle08Seperation, 0, 0, model.Axle08Weight,0, 0, "", 0, 0);
                        table.Rows.Add(9, model.Axle09Seperation, 0, 0, model.Axle09Weight,0, 0, "", 0, 0);
                        table.Rows.Add(10, model.Axle10Seperation, 0, 0, model.Axle10Weight, 0, 0, "", 0, 0);
                        break;
                    default:
                        //axle invalid
                        break;
                }
            }
            else { 
               //data Compliance is null.
            }
            return table;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        void IDisposable.Dispose()
        {
            GC.SuppressFinalize(this);
            //throw new NotImplementedException();
        }
    }
}
