﻿namespace ICMS
{
    partial class FrmUploadData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CMD_SAVE = new System.Windows.Forms.Button();
            this.CMD_CANCEL = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.LB_Header = new System.Windows.Forms.Label();
            this.incomeTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.playTimeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TXT_REAL_INCOME = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TXT_SYSTEM_INCOME = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incomeTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playTimeBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CMD_SAVE
            // 
            this.CMD_SAVE.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CMD_SAVE.Image = global::ICMS.Properties.Resources.disk_blue_ok;
            this.CMD_SAVE.Location = new System.Drawing.Point(284, 166);
            this.CMD_SAVE.Name = "CMD_SAVE";
            this.CMD_SAVE.Size = new System.Drawing.Size(106, 46);
            this.CMD_SAVE.TabIndex = 0;
            this.CMD_SAVE.Text = "บันทึก";
            this.CMD_SAVE.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CMD_SAVE.UseVisualStyleBackColor = true;
            this.CMD_SAVE.Click += new System.EventHandler(this.CMD_SAVE_Click);
            // 
            // CMD_CANCEL
            // 
            this.CMD_CANCEL.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CMD_CANCEL.Image = global::ICMS.Properties.Resources.redo;
            this.CMD_CANCEL.Location = new System.Drawing.Point(396, 166);
            this.CMD_CANCEL.Name = "CMD_CANCEL";
            this.CMD_CANCEL.Size = new System.Drawing.Size(106, 46);
            this.CMD_CANCEL.TabIndex = 1;
            this.CMD_CANCEL.Text = "ยกเลิก";
            this.CMD_CANCEL.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CMD_CANCEL.UseVisualStyleBackColor = true;
            this.CMD_CANCEL.Click += new System.EventHandler(this.CMD_SAVE_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::ICMS.Properties.Resources.folder_document;
            this.pictureBox1.Location = new System.Drawing.Point(10, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(39, 36);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(-2, -2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(792, 50);
            this.pictureBox2.TabIndex = 119;
            this.pictureBox2.TabStop = false;
            // 
            // LB_Header
            // 
            this.LB_Header.AutoSize = true;
            this.LB_Header.BackColor = System.Drawing.Color.White;
            this.LB_Header.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LB_Header.ForeColor = System.Drawing.Color.Green;
            this.LB_Header.Location = new System.Drawing.Point(69, 9);
            this.LB_Header.Name = "LB_Header";
            this.LB_Header.Size = new System.Drawing.Size(151, 25);
            this.LB_Header.TabIndex = 120;
            this.LB_Header.Text = "อัพโหลดข้อมูล";
            // 
            // incomeTypeBindingSource
            // 
            this.incomeTypeBindingSource.DataSource = typeof(ICMS.MODEL.IncomeType);
            // 
            // playTimeBindingSource
            // 
            this.playTimeBindingSource.DataSource = typeof(ICMS.MODEL.PlayTime);
            // 
            // TXT_REAL_INCOME
            // 
            this.TXT_REAL_INCOME.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_REAL_INCOME.Location = new System.Drawing.Point(180, 50);
            this.TXT_REAL_INCOME.Name = "TXT_REAL_INCOME";
            this.TXT_REAL_INCOME.Size = new System.Drawing.Size(100, 22);
            this.TXT_REAL_INCOME.TabIndex = 1;
            this.TXT_REAL_INCOME.Text = "0";
            this.TXT_REAL_INCOME.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(34, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 14);
            this.label2.TabIndex = 16;
            this.label2.Text = "จำนวนเงินที่ได้จริง (นับมือ)";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TXT_SYSTEM_INCOME);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TXT_REAL_INCOME);
            this.groupBox1.Location = new System.Drawing.Point(12, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(490, 106);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // TXT_SYSTEM_INCOME
            // 
            this.TXT_SYSTEM_INCOME.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_SYSTEM_INCOME.Location = new System.Drawing.Point(180, 22);
            this.TXT_SYSTEM_INCOME.Name = "TXT_SYSTEM_INCOME";
            this.TXT_SYSTEM_INCOME.Size = new System.Drawing.Size(100, 22);
            this.TXT_SYSTEM_INCOME.TabIndex = 20;
            this.TXT_SYSTEM_INCOME.Text = "0";
            this.TXT_SYSTEM_INCOME.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_SYSTEM_INCOME.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(49, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 14);
            this.label1.TabIndex = 19;
            this.label1.Text = "จำนวนเงินที่มาจากระบบ";
            this.label1.Visible = false;
            // 
            // FrmUploadData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 221);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.LB_Header);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.CMD_CANCEL);
            this.Controls.Add(this.CMD_SAVE);
            this.Name = "FrmUploadData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "อัพโหลดข้อมูล";
            this.Load += new System.EventHandler(this.FrmCheckInCom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incomeTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playTimeBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CMD_CANCEL;
        private System.Windows.Forms.Button CMD_SAVE;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label LB_Header;
        private System.Windows.Forms.BindingSource incomeTypeBindingSource;
        private System.Windows.Forms.BindingSource playTimeBindingSource;
        private System.Windows.Forms.TextBox TXT_REAL_INCOME;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TXT_SYSTEM_INCOME;
        private System.Windows.Forms.Label label1;
    }
}