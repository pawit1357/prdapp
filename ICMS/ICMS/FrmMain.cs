﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KUAssetWeb.DAL;
using ICMS.MODEL;

namespace ICMS
{
    public partial class FrmMain : Form
    {
        private int comIndex = 0;
        private List<PlayTime> lists = null;
        private List<PlayTime> gridlists = null;
        private List<Income> incomes = null;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            refreshIncome();
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            this.comIndex = Convert.ToInt16(listView1.SelectedIndices[0]);
            FrmCheckInCom checkInCom = new FrmCheckInCom(this.comIndex + 1, findPK());
            checkInCom.ShowDialog();
            refreshData();
        }

        public void refreshData()
        {
            String criteria = " where amount = 0 and end_time is null and  DATE_FORMAT (create_date, '%m-%d-%Y') = '" + DateTime.Now.ToString("MM-dd-yyyy") + "'";//แสดงเฉพาะสถานที่ยังไม่ได้จ่ายเงิน

            gridlists = PlayTimeDAO.select(criteria);

            dataGridView1.DataSource = gridlists;
            dataGridView1.Refresh();


            //L_TOTAL_INCOME.Refresh();
            //clear old color
            listView1.Items[this.comIndex].ForeColor = Color.Black;
            listView1.Items[this.comIndex].BackColor = Color.White;
            //แสดง icon ว่าเครื่องว่างไม่ว่าง
            for (int i = 0; i < Constants.COM_TOTAL; i++)
            {
                listView1.Items[i].ImageIndex = (isusing(i)) ? 1 : 0;
            }
            listView1.Items[this.comIndex].Selected = false;
            listView1.Refresh();

            //update total income today
            L_INTERNET_INCOME.Text = String.Format("{0:0,0}", PlayTimeDAO.getIncome(DateTime.Now));
            L_TOTAL_INCOME.Text = String.Format("{0:0,0}", Convert.ToInt16(L_INTERNET_INCOME.Text.Replace(",", "")) + Convert.ToInt16(L_OTHER_INCOME.Text.Replace(",", "")));
        }
        public void refreshIncome()
        {
            String criteria = " where DATE_FORMAT(income_date, '%m-%d-%Y') = '" + DateTime.Now.ToString("MM-dd-yyyy") + "'";



            incomes = IncomeDAO.select(criteria);

            dataGridView2.DataSource = incomes;
            dataGridView2.Refresh();

            double total = 0;
            foreach (Income inc in incomes)
            {
                total += (inc.Income_amount);
            }
            L_OTHER_INCOME.Text = String.Format("{0:0,0}", total);
            L_TOTAL_INCOME.Text = String.Format("{0:0,0}", Convert.ToInt16(L_INTERNET_INCOME.Text.Replace(",", "")) + Convert.ToInt16(L_OTHER_INCOME.Text.Replace(",", "")));
        }

        Boolean bVisible = true;
        private void timer1_Tick(object sender, EventArgs e)
        {
            //แจ้งเตือนเครื่องที่จะหมดเวลาล่วงหน้าก่อน 1 นาที
            lists = PlayTimeDAO.getAlert();
            if (lists.Count > 0)
            {
                foreach (PlayTime pt in lists)
                {

                    TimeSpan diff = pt.Alert_time.Subtract(DateTime.Now);
                    //Console.WriteLine(pt.Alert_time.Minute + "-" + DateTime.Now.Minute + " diff=" + diff.Minutes);
                    if (diff.Minutes <= Constants.ALERT_TIME)
                    {
                        listView1.Items[Convert.ToInt16(pt.Com_no) - 1].ForeColor = (bVisible) ? Color.Yellow : Color.Black;
                        listView1.Items[Convert.ToInt16(pt.Com_no) - 1].BackColor = (bVisible) ? Color.Black : Color.White;
                    }
                }
                bVisible = !bVisible;
            }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.Value != null)
            {
                switch (e.ColumnIndex)
                {
                    case 0:
                        e.Value = (e.RowIndex + 1) + ".";
                        break;
                    case 1:
                        e.Value = "COM-" + e.Value;
                        break;
                    case 2://start time                        
                        //e.Value = "";
                        break;
                    case 3://end time
                        //PlayTime pt = (PlayTime)sender;

                        e.Value = (gridlists[e.RowIndex].Alert_time.Year == 1) ? "-" : e.Value;
                        break;
                }
            }

        }

        private void dataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.Value != null)
            {
                switch (e.ColumnIndex)
                {
                    case 0:
                        break;
                    case 1:
                        e.Value = (e.RowIndex + 1) + "";
                        break;
                    case 2:
                        //IncomeType incomeType = new IncomeType
                        //{
                        //    id = Convert.ToInt16(e.Value)
                        //};
                        //e.Value = IncomeTypeDAO.selectById(incomeType).Incometype_name;
                        break;
                    case 3:
                        break;
                }
            }
        }

        private int findPK()
        {
            int index = 0;
            if (gridlists.Count > 0)
            {
                foreach (PlayTime _pt in gridlists)
                {
                    if (_pt.Com_no.Equals((this.comIndex + 1) + ""))
                    {
                        index = _pt.Id;
                        break;
                    }
                }
            }
            return index;
        }

        private Boolean isusing(int index)
        {
            Boolean result = false;
            foreach (PlayTime tmp in gridlists)
            {
                if ((index + 1).ToString().Equals(tmp.Com_no))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (this.incomes[e.RowIndex].Income_type)
            {
                case 1:
                    //Carcare
                    FrmIncomeCarcare frmIncomeCarcare = new FrmIncomeCarcare(this.incomes[e.RowIndex].id);
                    frmIncomeCarcare.ShowDialog();

                    break;
                case 2:
                    //internet
                    FrmIncome income = new FrmIncome(this.incomes[e.RowIndex].id);
                    income.ShowDialog();
                    break;
            }
            refreshIncome();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            int end_time = (DateTime.Now.Hour * 60) + (DateTime.Now.Minute);

            //int start_time = (pt.Start_time.Hour * 60) + (pt.Start_time.Minute);
            //int total_minute = end_time - start_time;
            //total_amount = total_minute * Constants.PLAY_REATE_PER_MINUTE;
        }


    }
}
