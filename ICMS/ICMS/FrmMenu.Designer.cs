﻿namespace ICMS
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.TSM_01 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_01_01 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.TSM_01_02 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_02 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_02_01 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_03 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_03_01 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_04 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_04_01 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_04_02 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_05 = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM_05_01 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.TSB_01 = new System.Windows.Forms.ToolStripButton();
            this.TSB_03 = new System.Windows.Forms.ToolStripButton();
            this.TSB_02 = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSM_03_02 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_01,
            this.TSM_02,
            this.TSM_03,
            this.TSM_04,
            this.TSM_05});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(691, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // TSM_01
            // 
            this.TSM_01.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_01_01,
            this.toolStripSeparator1,
            this.TSM_01_02});
            this.TSM_01.Name = "TSM_01";
            this.TSM_01.Size = new System.Drawing.Size(38, 20);
            this.TSM_01.Text = "ไฟล์";
            // 
            // TSM_01_01
            // 
            this.TSM_01_01.Name = "TSM_01_01";
            this.TSM_01_01.Size = new System.Drawing.Size(140, 22);
            this.TSM_01_01.Text = "เปลี่ยนผู้ใช้";
            this.TSM_01_01.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(137, 6);
            // 
            // TSM_01_02
            // 
            this.TSM_01_02.Name = "TSM_01_02";
            this.TSM_01_02.Size = new System.Drawing.Size(140, 22);
            this.TSM_01_02.Text = "อออกจากระบบ";
            this.TSM_01_02.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // TSM_02
            // 
            this.TSM_02.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_02_01});
            this.TSM_02.Name = "TSM_02";
            this.TSM_02.Size = new System.Drawing.Size(65, 20);
            this.TSM_02.Text = "ข้อมูลหลัก";
            // 
            // TSM_02_01
            // 
            this.TSM_02_01.Name = "TSM_02_01";
            this.TSM_02_01.Size = new System.Drawing.Size(145, 22);
            this.TSM_02_01.Text = "ประเภทการชำระ";
            this.TSM_02_01.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // TSM_03
            // 
            this.TSM_03.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_03_01,
            this.TSM_03_02});
            this.TSM_03.Name = "TSM_03";
            this.TSM_03.Size = new System.Drawing.Size(52, 20);
            this.TSM_03.Text = "รายงาน";
            // 
            // TSM_03_01
            // 
            this.TSM_03_01.Name = "TSM_03_01";
            this.TSM_03_01.Size = new System.Drawing.Size(224, 22);
            this.TSM_03_01.Text = "สรุปรายรับแยกตามประเภท";
            this.TSM_03_01.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // TSM_04
            // 
            this.TSM_04.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_04_01,
            this.TSM_04_02});
            this.TSM_04.Name = "TSM_04";
            this.TSM_04.Size = new System.Drawing.Size(58, 20);
            this.TSM_04.Text = "เครื่องมือ";
            // 
            // TSM_04_01
            // 
            this.TSM_04_01.Name = "TSM_04_01";
            this.TSM_04_01.Size = new System.Drawing.Size(137, 22);
            this.TSM_04_01.Text = "สำรองข้อมุล";
            this.TSM_04_01.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // TSM_04_02
            // 
            this.TSM_04_02.Name = "TSM_04_02";
            this.TSM_04_02.Size = new System.Drawing.Size(137, 22);
            this.TSM_04_02.Text = "อัพโหลดข้อมูล";
            this.TSM_04_02.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // TSM_05
            // 
            this.TSM_05.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSM_05_01});
            this.TSM_05.Name = "TSM_05";
            this.TSM_05.Size = new System.Drawing.Size(59, 20);
            this.TSM_05.Text = "ช่วยเหลือ";
            // 
            // TSM_05_01
            // 
            this.TSM_05_01.Name = "TSM_05_01";
            this.TSM_05_01.Size = new System.Drawing.Size(132, 22);
            this.TSM_05_01.Text = "เกี่ยวกับระบบ";
            this.TSM_05_01.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSB_01,
            this.TSB_03,
            this.TSB_02});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(691, 39);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // TSB_01
            // 
            this.TSB_01.Image = global::ICMS.Properties.Resources.folder_document;
            this.TSB_01.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TSB_01.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_01.Name = "TSB_01";
            this.TSB_01.Size = new System.Drawing.Size(76, 36);
            this.TSB_01.Text = "รับชำระ";
            this.TSB_01.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // TSB_03
            // 
            this.TSB_03.Image = global::ICMS.Properties.Resources.folder_document;
            this.TSB_03.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TSB_03.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_03.Name = "TSB_03";
            this.TSB_03.Size = new System.Drawing.Size(111, 36);
            this.TSB_03.Text = "รับชำระคาร์แคร์";
            this.TSB_03.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // TSB_02
            // 
            this.TSB_02.Image = global::ICMS.Properties.Resources.printer2;
            this.TSB_02.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TSB_02.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_02.Name = "TSB_02";
            this.TSB_02.Size = new System.Drawing.Size(81, 36);
            this.TSB_02.Text = "สรุปรายรับ";
            this.TSB_02.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 412);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(691, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // TSM_03_02
            // 
            this.TSM_03_02.Name = "TSM_03_02";
            this.TSM_03_02.Size = new System.Drawing.Size(224, 22);
            this.TSM_03_02.Text = "สรุปจำนวนการใช้บริการ (คาร์แคร์)";
            this.TSM_03_02.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 434);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripButton TSB_01;
        private System.Windows.Forms.ToolStripMenuItem TSM_01;
        private System.Windows.Forms.ToolStripMenuItem TSM_01_01;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem TSM_01_02;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripButton TSB_02;
        private System.Windows.Forms.ToolStripMenuItem TSM_02;
        private System.Windows.Forms.ToolStripMenuItem TSM_02_01;
        private System.Windows.Forms.ToolStripMenuItem TSM_03;
        private System.Windows.Forms.ToolStripMenuItem TSM_03_01;
        private System.Windows.Forms.ToolStripMenuItem TSM_04;
        private System.Windows.Forms.ToolStripMenuItem TSM_04_01;
        private System.Windows.Forms.ToolStripMenuItem TSM_05;
        private System.Windows.Forms.ToolStripMenuItem TSM_05_01;
        private System.Windows.Forms.ToolStripMenuItem TSM_04_02;
        private System.Windows.Forms.ToolStripButton TSB_03;
        private System.Windows.Forms.ToolStripMenuItem TSM_03_02;
    }
}

