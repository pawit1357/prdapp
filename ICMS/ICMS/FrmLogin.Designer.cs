﻿namespace ICMS
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OK = new System.Windows.Forms.Button();
            this.LAPPTITLE = new System.Windows.Forms.Label();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.LAPPCOMPANY = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.LUPDATE_DATE = new System.Windows.Forms.Label();
            this.Cancel = new System.Windows.Forms.Button();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.UsernameTextBox = new System.Windows.Forms.TextBox();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // OK
            // 
            this.OK.Font = new System.Drawing.Font("BrowalliaUPC", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.OK.Location = new System.Drawing.Point(299, 217);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(72, 31);
            this.OK.TabIndex = 3;
            this.OK.Text = "OK";
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // LAPPTITLE
            // 
            this.LAPPTITLE.BackColor = System.Drawing.Color.Transparent;
            this.LAPPTITLE.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LAPPTITLE.ForeColor = System.Drawing.Color.Teal;
            this.LAPPTITLE.Location = new System.Drawing.Point(11, 11);
            this.LAPPTITLE.Name = "LAPPTITLE";
            this.LAPPTITLE.Size = new System.Drawing.Size(435, 50);
            this.LAPPTITLE.TabIndex = 13;
            this.LAPPTITLE.Text = "####";
            this.LAPPTITLE.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.PasswordTextBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.PasswordTextBox.Location = new System.Drawing.Point(287, 170);
            this.PasswordTextBox.MaxLength = 20;
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.PasswordChar = '*';
            this.PasswordTextBox.Size = new System.Drawing.Size(159, 27);
            this.PasswordTextBox.TabIndex = 2;
            this.PasswordTextBox.Text = "1234";
            // 
            // LAPPCOMPANY
            // 
            this.LAPPCOMPANY.BackColor = System.Drawing.Color.Transparent;
            this.LAPPCOMPANY.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LAPPCOMPANY.ForeColor = System.Drawing.Color.Teal;
            this.LAPPCOMPANY.Location = new System.Drawing.Point(35, 61);
            this.LAPPCOMPANY.Name = "LAPPCOMPANY";
            this.LAPPCOMPANY.Size = new System.Drawing.Size(411, 25);
            this.LAPPCOMPANY.TabIndex = 10;
            this.LAPPCOMPANY.Text = "####";
            this.LAPPCOMPANY.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("BrowalliaUPC", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label1.ForeColor = System.Drawing.Color.CadetBlue;
            this.Label1.Location = new System.Drawing.Point(5, 23);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(0, 63);
            this.Label1.TabIndex = 9;
            // 
            // GroupBox1
            // 
            this.GroupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GroupBox1.Controls.Add(this.PictureBox1);
            this.GroupBox1.Controls.Add(this.LUPDATE_DATE);
            this.GroupBox1.Controls.Add(this.Cancel);
            this.GroupBox1.Controls.Add(this.UsernameLabel);
            this.GroupBox1.Controls.Add(this.OK);
            this.GroupBox1.Controls.Add(this.LAPPTITLE);
            this.GroupBox1.Controls.Add(this.PasswordTextBox);
            this.GroupBox1.Controls.Add(this.LAPPCOMPANY);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Controls.Add(this.UsernameTextBox);
            this.GroupBox1.Controls.Add(this.PasswordLabel);
            this.GroupBox1.Location = new System.Drawing.Point(2, -6);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(463, 287);
            this.GroupBox1.TabIndex = 8;
            this.GroupBox1.TabStop = false;
            // 
            // PictureBox1
            // 
            this.PictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.PictureBox1.Image = global::ICMS.Properties.Resources.logo;
            this.PictureBox1.Location = new System.Drawing.Point(12, 20);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(108, 251);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox1.TabIndex = 15;
            this.PictureBox1.TabStop = false;
            // 
            // LUPDATE_DATE
            // 
            this.LUPDATE_DATE.BackColor = System.Drawing.Color.Transparent;
            this.LUPDATE_DATE.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LUPDATE_DATE.ForeColor = System.Drawing.Color.Teal;
            this.LUPDATE_DATE.Location = new System.Drawing.Point(299, 251);
            this.LUPDATE_DATE.Name = "LUPDATE_DATE";
            this.LUPDATE_DATE.Size = new System.Drawing.Size(147, 16);
            this.LUPDATE_DATE.TabIndex = 14;
            this.LUPDATE_DATE.Text = "####";
            this.LUPDATE_DATE.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Font = new System.Drawing.Font("BrowalliaUPC", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Cancel.Location = new System.Drawing.Point(377, 217);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(69, 31);
            this.Cancel.TabIndex = 4;
            this.Cancel.Text = "Cancel";
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.BackColor = System.Drawing.Color.Transparent;
            this.UsernameLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.UsernameLabel.ForeColor = System.Drawing.Color.Teal;
            this.UsernameLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.UsernameLabel.Location = new System.Drawing.Point(174, 132);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.UsernameLabel.Size = new System.Drawing.Size(107, 31);
            this.UsernameLabel.TabIndex = 0;
            this.UsernameLabel.Text = "รหัสผู้ใช้";
            this.UsernameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UsernameTextBox
            // 
            this.UsernameTextBox.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.UsernameTextBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.UsernameTextBox.Location = new System.Drawing.Point(287, 132);
            this.UsernameTextBox.MaxLength = 20;
            this.UsernameTextBox.Name = "UsernameTextBox";
            this.UsernameTextBox.Size = new System.Drawing.Size(159, 27);
            this.UsernameTextBox.TabIndex = 1;
            this.UsernameTextBox.Text = "0003";
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.BackColor = System.Drawing.Color.Transparent;
            this.PasswordLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.PasswordLabel.ForeColor = System.Drawing.Color.Teal;
            this.PasswordLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.PasswordLabel.Location = new System.Drawing.Point(163, 174);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PasswordLabel.Size = new System.Drawing.Size(118, 23);
            this.PasswordLabel.TabIndex = 2;
            this.PasswordLabel.Text = "รหัสผ่าน";
            this.PasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 281);
            this.Controls.Add(this.GroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "FrmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmLogin";
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmLogin_KeyDown);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button OK;
        internal System.Windows.Forms.Label LAPPTITLE;
        internal System.Windows.Forms.TextBox PasswordTextBox;
        internal System.Windows.Forms.Label LAPPCOMPANY;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.Label LUPDATE_DATE;
        internal System.Windows.Forms.Button Cancel;
        internal System.Windows.Forms.Label UsernameLabel;
        internal System.Windows.Forms.TextBox UsernameTextBox;
        internal System.Windows.Forms.Label PasswordLabel;
    }
}