﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICMS.DAO;
using ICMS.MODEL;
using KUAssetWeb.DAL;

namespace ICMS
{
    public partial class FrmLogin : Form
    {
        public string click = "";
        //private int count = 1;

        public FrmLogin()
        {
            InitializeComponent();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            LAPPTITLE.Text = Application.ProductName;
            LAPPCOMPANY.Text = Application.CompanyName;
            LUPDATE_DATE.Text = "Version " + Application.ProductVersion;
            UsernameTextBox.Focus();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (UsernameTextBox.Text.Equals("-") && PasswordTextBox.Text.Equals("-"))
            {
                UsernameTextBox.Text = "";
                PasswordTextBox.Text = "";
                UsernameTextBox.Focus();
            }
            else
            {
                //if (checkLogin(UsernameTextBox.Text, PasswordTextBox.Text))
                //{
                click = "OK";
                Close();
                //}
                //else
                //{
                //    MessageBox.Show(Management.LOGIN_FAIL, Application.ProductName);
                //    PasswordTextBox.SelectAll();
                //    PasswordTextBox.Focus();
                //}
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            click = "CANCEL";
            Application.Exit();
        }

        private void FrmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    //String[] item = {"Tablet (I-NET)",	
                    //    "Case Tablet",	
                    //    "เครื่องดื่ม",
                    //    "พริ้นงาน",
                    //    "ขนม",
                    //    "เติมเงินโทรศัพท์	",
                    //    "Flah Drive",
                    //    "ถ่ายเอกสาร",
                    //    "เครื่อบบัตร",
                    //    "ไอติม",
                    //    "write CD"};
                    //for (int i = 0; i < item.Length; i++)
                    //{
                    //    IncomeType invt = new IncomeType
                    //    {
                    //        Incometype_name = item[i],
                    //        Keyuser = ""
                    //    };
                    //    IncomeTypeDAO.insert(invt);
                    //}
                    //MessageBox.Show("บันทึกข้อมูลเริ่มต้นเรียบร้อยแล้ว");
                    //int result = DBUtil.generateTable(Constants.SQL_TBL_INCOME);
                    //int result1 = DBUtil.generateTable(Constants.SQL_TBL_INCOMETYPE);
                    //if (result == 0 && result1 == 0)
                    //{
                    //    MessageBox.Show("Create table success.");
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Can't create table please contact developer.");
                    //}
                    break;
            }
        }



    }
}
