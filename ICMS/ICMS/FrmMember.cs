﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KUAssetWeb.DAL;
using ICMS.MODEL;
using ICMS.DAO;

namespace ICMS
{
    public partial class FrmMember : Form
    {
        public FrmMember()
        {
            InitializeComponent();
        }

        private void FrmMember_Load(object sender, EventArgs e)
        {
            refreshData();
        }

        private void refreshData()
        {
            List<Report01> lists = ReportDAO.report02();
            if (lists.Count > 0)
            {
                Report01 _tmp = new Report01();
                _tmp.Name = "                              รวมทั้งหมด";
                foreach (Report01 rpt01 in lists)
                {
                    _tmp.Amount += rpt01.Amount;
                }
                lists.Add(_tmp);
                dataGridView1.DataSource = lists;
            }

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!txtLicensePlate.Text.Equals(""))
            {
                List<Report01> lists = ReportDAO.searchReport(txtLicensePlate.Text);
                if (lists.Count > 0)
                {
                    dataGridView1.DataSource = lists;
                }
            }
        }

    }
}
