﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICMS.MODEL
{
    class Report01
    {
        private String name;

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        private double amount;

        public double Amount
        {
            get { return amount; }
            set { amount = value; }
        }
    }
}
