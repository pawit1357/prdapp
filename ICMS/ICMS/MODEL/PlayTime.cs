﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICMS.MODEL
{
    class PlayTime
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private String com_no;

        public String Com_no
        {
            get { return com_no; }
            set { com_no = value; }
        }
        private DateTime start_time;

        public DateTime Start_time
        {
            get { return start_time; }
            set { start_time = value; }
        }
        private DateTime end_time;

        public DateTime End_time
        {
            get { return end_time; }
            set { end_time = value; }
        }
        private DateTime create_date;

        public DateTime Create_date
        {
            get { return create_date; }
            set { create_date = value; }
        }

        private String play_type;

        public String Play_type
        {
            get { return play_type; }
            set { play_type = value; }
        }

        private DateTime alert_time;

        public DateTime Alert_time
        {
            get { return alert_time; }
            set { alert_time = value; }
        }
        private double amount;

        public double Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        private String keyuser;

        public String Keyuser
        {
            get { return keyuser; }
            set { keyuser = value; }
        }


    }
}
