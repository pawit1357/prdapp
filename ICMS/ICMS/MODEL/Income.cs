﻿using System;

namespace ICMS.MODEL
{
    class Income : IncomeType
    {
        private int income_type;

        public int Income_type
        {
            get { return income_type; }
            set { income_type = value; }
        }
        private int income_amount;

        public int Income_amount
        {
            get { return income_amount; }
            set { income_amount = value; }
        }
        private int income_count;

        public int Income_count
        {
            get { return income_count; }
            set { income_count = value; }
        }
        private String income_remark;

        public String Income_remark
        {
            get { return income_remark; }
            set { income_remark = value; }
        }
        private DateTime income_date;

        public DateTime Income_date
        {
            get { return income_date; }
            set { income_date = value; }
        }


        public int income_type_car { get; set; }
        public int income_use_count { get; set; }
    }
}
