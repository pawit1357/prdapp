﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KUAssetWeb.DAL;
using ICMS.MODEL;

namespace ICMS
{
    public partial class FrmIncomeType : Form
    {
        //private int incomeID = 0;
        //private Income income;
        private List<IncomeType> lists = null;
        public FrmIncomeType()
        {
            InitializeComponent();
        }

        private void FrmCheckInCom_Load(object sender, EventArgs e)
        {
            refreshData();
            //if (this.incomeID != 0)
            //{
            //    this.income = IncomeDAO.selectById(new Income { Id = this.incomeID });
            //    if (this.income != null)
            //    {
            //        //udpate 
            //        TXT_NAME.Text = this.income.Income_count + "";
            //        TXT_AMOUNT.Text = this.income.Income_amount + "";
            //        TXT_REMARK.Text = this.income.Income_remark;
            //        CMD_SAVE.Text = "แก้ไข";
            //        CMD_DELETE.Enabled = true;
            //        comboBox1.Enabled = false;
            //    }
            //    else
            //    {
            //        MessageBox.Show("ไม่พบข้อมูลที่ต้องการแก้ไข (กรุณาติดต่อผู้พัฒนาระบบ)");
            //    }
            //}
            //else
            //{
            //    //normal insert

            //}
        }

        private void CMD_SAVE_Click(object sender, EventArgs e)
        {
            Button bnt = (Button)sender;
            int result = 0;
            switch (bnt.Name)
            {
                case "CMD_SAVE":
                    IncomeType _invtyp = new IncomeType();
                    _invtyp.id = Convert.ToInt16(TXT_ID.Text);
                    _invtyp.Incometype_name = TXT_NAME.Text;
                    _invtyp.Keyuser = "";

                    switch (bnt.Text)
                    {
                        case "บันทึก":
                            result = IncomeTypeDAO.insert(_invtyp);
                            if (result > 0)
                            {
                                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("เกิดข้อผิดพลาดในการบันทึกข้อมูล (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            break;
                        case "แก้ไข":
                            result = IncomeTypeDAO.update(_invtyp);
                            if (result > 0)
                            {
                                MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("เกิดข้อผิดพลาดในการแก้ไขข้อมูล (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            break;
                    }
                    break;
                case "CMD_DELETE":
                    result = IncomeTypeDAO.delete(new IncomeType { id = Convert.ToInt16(TXT_ID.Text) });
                    if (result > 0)
                    {
                        MessageBox.Show("ลบข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดในการลบข้อมูล (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    break;
                case "CMD_CANCEL":                    
                    //foreach (IncomeType it in lists)
                    //{
                    //    string sql = "INSERT INTO tbl_incometype(ID, incometype_name, keyuser) VALUES ('"+it.Id+"','"+it.Incometype_name+"','')";
                    //    Console.WriteLine( "Result:"+ICMS.DAO.DBUtil.postData("http://prdapp.net/cinetapp/cinet_service.php?action=insert&sql=" + sql));
                    //}
                    break;
            }
            refreshData();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                TXT_ID.Text = lists[e.RowIndex].id + "";
                TXT_NAME.Text = lists[e.RowIndex].Incometype_name;
                CMD_SAVE.Text = "แก้ไข";
                CMD_DELETE.Enabled = true;
            }
            
        }
        private void refreshData()
        {
            lists = IncomeTypeDAO.select("");
            if (lists.Count > 0)
            {
                dataGridView1.DataSource = lists;
            }
            TXT_ID.Text = "0";
            TXT_NAME.Text = "";
            CMD_SAVE.Text = "บันทึก";
            CMD_DELETE.Enabled = false;
        }

    }
}
