﻿using System;
using System.Collections.Generic;

using System.Data;
using ICMS.DAL;
using ICMS.MODEL;
using MySql.Data.MySqlClient;

namespace KUAssetWeb.DAL
{
    class IncomeTypeDAO
    {
        //Get Data
        public static List<IncomeType> select(String _criteria)
        {
            String sql = "select * from tbl_IncomeType "+_criteria;
            List<IncomeType> lists = new List<IncomeType>();
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);

            cmd.CommandType = CommandType.Text;
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            int running = 1;
            while (dr.Read())
            {
                IncomeType incomeType = new IncomeType();
                if (DBNull.Value != dr["id"])
                    incomeType.id = Convert.ToInt16(dr["id"]);
                if (DBNull.Value != dr["incometype_name"])
                    incomeType.Incometype_name = Convert.ToString(dr["incometype_name"]);
                if (DBNull.Value != dr["keyuser"])
                    incomeType.Keyuser = Convert.ToString(dr["keyuser"]);
                lists.Add(incomeType);
                running++;
            }
            dr.Close();
            return lists;
        }

        public static IncomeType selectById(IncomeType _incomeType)
        {
            IncomeType incomeType = new IncomeType();
            String sql = String.Format("select * from tbl_IncomeType where id={0}", _incomeType.id);
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;

            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            while (dr.Read())
            {
                if (DBNull.Value != dr["id"])
                    incomeType.id = Convert.ToInt16(dr["id"]);
                if (DBNull.Value != dr["incometype_name"])
                    incomeType.Incometype_name = Convert.ToString(dr["incometype_name"]);
                if (DBNull.Value != dr["keyuser"])
                    incomeType.Keyuser = Convert.ToString(dr["keyuser"]);
            }
            dr.Close();
            return incomeType;
        }
        //Insert,Delete,Update
        public static int insert(IncomeType _incometype)
        {
            String sql = "insert into tbl_IncomeType(incometype_name,keyuser) values('{0}','{1}')";
            sql = String.Format(sql, _incometype.Incometype_name,_incometype.Keyuser);

            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            return i;
        }
        
        public static int delete(IncomeType _incomeType)
        {
            String sql = "delete from tbl_IncomeType where id={0}";


            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(String.Format(sql, _incomeType.id), connection);

            cmd.CommandType = CommandType.Text;

            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            return i;
        }

        public static int update(IncomeType _incomeType)
        {
            String sql = "update tbl_IncomeType set " +
                "incometype_name='{0}',keyuser='{1}' where id={2}";
            sql = String.Format(sql, _incomeType.Incometype_name, _incomeType.Keyuser, _incomeType.id);

            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            return i;
        }

  
    }
}
