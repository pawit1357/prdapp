﻿
using System.Configuration;
namespace ICMS.DAL
{
    public class Connection
    {
        public static string ConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ICMS.Properties.Settings.cinetdbConnectionString"].ConnectionString;
        }
    }
}


