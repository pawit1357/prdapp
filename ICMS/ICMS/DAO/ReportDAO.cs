﻿using System;
using System.Collections.Generic;
using ICMS.DAL;
using System.Data;
using ICMS.MODEL;
using MySql.Data.MySqlClient;

namespace ICMS.DAO
{
    class ReportDAO
    {
        //รายงานแยกตามประเภท
        public static List<Report01> report01()
        {
            String sql =
                    "select xname,xamount from (SELECT  tbl_incometype.incometype_name as xname,Sum(tbl_income.income_amount) AS xamount " +
                    "FROM tbl_income INNER JOIN tbl_incometype ON tbl_income.income_type = tbl_incometype.ID where format(tbl_income.income_date,'yyyymmdd') = format('" + DateTime.Now + "','yyyymmdd') " +
                    "GROUP BY tbl_income.income_type, tbl_incometype.incometype_name " +
                    "union " +
                    "select 'คอมพิวเตอร์' as xname,sum(amount) as xamount from tbl_playtime  where format(tbl_playtime.create_date,'yyyymmdd') = format('" + DateTime.Now + "','yyyymmdd')) where xamount >0";
            List<Report01> lists = new List<Report01>();
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            int running = 1;
            while (dr.Read())
            {
                Report01 rpt01 = new Report01();
                if (DBNull.Value != dr["xname"])
                    rpt01.Name = Convert.ToString(dr["xname"]);
                if (DBNull.Value != dr["xamount"])
                    rpt01.Amount = Convert.ToDouble(dr["xamount"]);
                lists.Add(rpt01);
                running++;
            }
            dr.Close();
            return lists;
        }

        public static List<Report01> report02()
        {
            String sql ="select income_remark,count(id) total from tbl_income where income_type=1  group by income_remark order by count(id) desc";
            List<Report01> lists = new List<Report01>();
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            int running = 1;
            while (dr.Read())
            {
                Report01 rpt01 = new Report01();
                if (DBNull.Value != dr["income_remark"])
                    rpt01.Name = Convert.ToString(dr["income_remark"]);
                if (DBNull.Value != dr["total"])
                    rpt01.Amount = Convert.ToDouble(dr["total"]);
                lists.Add(rpt01);
                running++;
            }
            dr.Close();
            return lists;
        }
        public static List<Report01> searchReport(String licensePlate)
        {
            String sql = "select income_remark,count(id) total from tbl_income where income_type=1 and income_remark like '%" + licensePlate + "%'  group by income_remark order by count(id) desc";
            List<Report01> lists = new List<Report01>();
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            int running = 1;
            while (dr.Read())
            {
                Report01 rpt01 = new Report01();
                if (DBNull.Value != dr["income_remark"])
                    rpt01.Name = Convert.ToString(dr["income_remark"]);
                if (DBNull.Value != dr["total"])
                    rpt01.Amount = Convert.ToDouble(dr["total"]);
                lists.Add(rpt01);
                running++;
            }
            dr.Close();
            return lists;
        }
    }
}
