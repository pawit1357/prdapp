﻿using System;
using System.Collections.Generic;

using System.Data;
using ICMS.DAL;
using ICMS.MODEL;
using ICMS.DAO;
using MySql.Data.MySqlClient;

namespace KUAssetWeb.DAL
{
    class IncomeDAO
    {
        //Get Data
        public static List<Income> select(String _criteria)
        {
            String sql = "select t1.id,t1.income_type,t2.incometype_name,t1.income_count,t1.income_amount,t1.income_remark,t1.income_date,t1.keyuser," +
                        "(select count(id) from tbl_income  group by income_remark having income_remark=t1.income_remark ) income_use_count" +
                        " from tbl_income t1 left join tbl_incometype t2 on t1.income_type = t2.id " + _criteria;

            List<Income> lists = new List<Income>();

            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);

            cmd.CommandType = CommandType.Text;

            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            int running = 1;
            while (dr.Read())
            {
                Income income = new Income();
                if (DBNull.Value != dr["id"])
                    income.id = Convert.ToInt16(dr["id"]);
                if (DBNull.Value != dr["income_type"])
                    income.Income_type = Convert.ToInt16(dr["income_type"]);
                if (DBNull.Value != dr["incometype_name"])
                    income.Incometype_name = Convert.ToString(dr["incometype_name"]);
                if (DBNull.Value != dr["income_count"])
                    income.Income_count = Convert.ToInt16(dr["income_count"]);
                if (DBNull.Value != dr["income_amount"])
                    income.Income_amount = Convert.ToInt16(dr["income_amount"]);
                if (DBNull.Value != dr["income_remark"])
                    income.Income_remark = Convert.ToString(dr["income_remark"]);
                if (DBNull.Value != dr["income_date"])
                    income.Income_date = Convert.ToDateTime(dr["income_date"]);
                if (DBNull.Value != dr["keyuser"])
                    income.income_use_count = Convert.ToInt16(dr["income_use_count"]);
                if (DBNull.Value != dr["income_use_count"])
                    income.income_use_count = Convert.ToInt16(dr["income_use_count"]);


                lists.Add(income);
                running++;
            }
            dr.Close();
            return lists;
        }



        public static Income selectById(Income _income)
        {
            Income income = new Income();
            String sql = String.Format("select * from tbl_income where id={0}", _income.id);
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;

            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            while (dr.Read())
            {
                if (DBNull.Value != dr["id"])
                    income.id = Convert.ToInt16(dr["id"]);
                if (DBNull.Value != dr["income_type"])
                    income.Income_type = Convert.ToInt16(dr["income_type"]);
                if (DBNull.Value != dr["income_count"])
                    income.Income_count = Convert.ToInt16(dr["income_count"]);
                if (DBNull.Value != dr["income_amount"])
                    income.Income_amount = Convert.ToInt16(dr["income_amount"]);
                if (DBNull.Value != dr["income_remark"])
                    income.Income_remark = Convert.ToString(dr["income_remark"]);
                if (DBNull.Value != dr["income_date"])
                    income.Income_date = Convert.ToDateTime(dr["income_date"]);
                if (DBNull.Value != dr["keyuser"])
                    income.Keyuser = Convert.ToString(dr["keyuser"]);
            }
            dr.Close();
            return income;
        }
        //Insert,Delete,Update
        public static int insert(Income _income)
        {
            String sql = "insert into tbl_income(income_type,income_count,income_amount,income_remark,income_date,keyuser,income_type_car) values({0},{1},{2},'{3}',NOW(),'{4}',{5})";
            sql = String.Format(sql, _income.Income_type, _income.Income_count, _income.Income_amount, _income.Income_remark, _income.Keyuser, _income.income_type_car);

            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            if (i > 0)
            {
                //try
                //{
                //    String pSql = "insert into tbl_income(income_type,income_count,income_amount,income_remark,income_date,keyuser) values({0},{1},{2},'{3}',NOW(),'{5}')";
                //    pSql = String.Format(pSql, _income.Income_type, _income.Income_count, _income.Income_amount, _income.Income_remark, _income.Income_date, _income.Keyuser);
                //    Console.WriteLine("Post data result:" + DBUtil.postData("http://prdapp.net/cinetapp/cinet_service.php?action=insert&sql=" + pSql));
                //}
                //catch (Exception ex)
                //{
                //    Console.WriteLine(ex.Message);
                //}
            }
            return i;
        }

        public static int delete(Income _income)
        {
            String sql = "delete from tbl_income where id={0}";


            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(String.Format(sql, _income.id), connection);
            cmd.CommandType = CommandType.Text;
            //cmd.Parameters.Add(new OleDbParameter("id", id));

            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            if (i > 0)
            {
                try
                {
                    Console.WriteLine("Post data result:" + DBUtil.postData("http://prdapp.net/cinetapp/cinet_service.php?action=deletet&sql=" + sql));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return i;
        }

        public static int update(Income _income)
        {
            String sql = "update tbl_income set " +
                "income_type={0},income_count={1},income_amount={2},income_remark='{3}',keyuser='{4}',income_type_car={6} where id={5}";
            sql = String.Format(sql, _income.Income_type, _income.Income_count, _income.Income_amount, _income.Income_remark, _income.Keyuser, _income.id, _income.income_type_car);

            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(String.Format(sql, _income.id), connection);
            cmd.CommandType = CommandType.Text;
            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            if (i > 0)
            {
                //try
                //{
                //    Console.WriteLine("Post data result:" + DBUtil.postData("http://prdapp.net/cinetapp/cinet_service.php?action=update&sql=" + sql));
                //}
                //catch (Exception ex)
                //{
                //    Console.WriteLine(ex.Message);
                //}
            }
            return i;
        }


    }
}
