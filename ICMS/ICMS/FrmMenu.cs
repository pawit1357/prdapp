﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ICMS
{
    public partial class FrmMenu : Form
    {
        public FrmMain main = null;

        public FrmMenu()
        {
            InitializeComponent();
        }

        private void FrmMenu_Load(object sender, EventArgs e)
        {
            //Show Login Menu
            FrmLogin login = new FrmLogin();
            login.ShowDialog();
            if (login.click.Equals("OK"))
            {
                initial();
            }
        }

        private void initial()
        {
            //if (Authorize.getUser().Equals("")) Application.Exit();//if alt+f4 exit app
            //Set Status
            toolStripStatusLabel1.Text = "ผู้ใช้งานปัจจุบัน: " + "pawit";// Authorize.getUser() + " " + Authorize.getUserName();
            toolStripStatusLabel2.Text = "สถานะ: " + "System-Admin";// Authorize.getUserPermission();

            //Open main
            main = new FrmMain();
            main.MdiParent = this;
            main.WindowState = FormWindowState.Maximized;
            main.Show();
            main.refreshData();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            string name = "";
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem tsm = (ToolStripMenuItem)sender;
                name = tsm.Name;
            }
            if (sender is ToolStripButton)
            {
                ToolStripButton tsb = (ToolStripButton)sender;
                name = tsb.Name;
            }
            switch (name)
            {
                case "TSM_01":
                    break;
                case "TSM_01_01":
                    break;
                case "TSM_01_02":
                    Application.Exit();
                    break;
                case "TSM_02":
                    break;
                case "TSM_02_01":
                    FrmIncomeType invTyp = new FrmIncomeType();
                    invTyp.ShowDialog();
                    break;
                case "TSM_03":
                    break;
                case "TSM_03_01":
                case "TSB_02":
                    FrmReport rpt = new FrmReport();
                    rpt.ShowDialog();
                    break;
                case "TSM_03_02":
                    FrmMember frmMember = new FrmMember();
                    frmMember.ShowDialog();
                    break;
                case "TSM_04":
                    break;
                case "TSM_04_01":
                    break;
                case "TSM_04_02":
                    FrmUploadData uploadData = new FrmUploadData();
                    uploadData.ShowDialog();
                    break;
                case "TSM_05":
                    break;
                case "TSM_05_01":
                    AboutBox ab = new AboutBox();
                    ab.ShowDialog();
                    break;
                case "TSB_01":
                    FrmIncome income = new FrmIncome();
                    income.ShowDialog();
                    main.refreshIncome();
                    break;
                case "TSB_03":
                    FrmIncomeCarcare incomeCarcare = new FrmIncomeCarcare();
                    incomeCarcare.ShowDialog();
                    main.refreshIncome();
                    break;
            }
        }

    }
}
