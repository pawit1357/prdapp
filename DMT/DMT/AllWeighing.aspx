﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="AllWeighing.aspx.cs" Inherits="DMT.AllWeighing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css" media="print">
        #btSearch0, #menu { visibility:hidden !important; }
    </style>
    <script type="text/javascript">
        $(function () {


            $("#from_date").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat:"dd/mm/yy",
            onClose: function (selectedDate) {
                $("#to_date").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#to_date").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: "dd/mm/yy",
            onClose: function (selectedDate) {
                $("#from_date").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("#from_date,#to_date").datepicker("option", "dateFormat", "dd/mm/yy");
    });
    
    function searchByPage(pageno) {
        $('#txtPageNo').val(pageno);
        $('#btSearchByPage').click();
    }

    function printIt() {
        $.ajax({
            url: "PrintAllWeighing.aspx?tmp=" + new Date().getTime(),
            type: "POST",
            data: { station: $("#h_pstation").val(),
                overweight: $("#h_poverweight").val(),
                from_date: $("#h_pFromDate").val(),
                to_date: $("#h_pToDate").val(),
                hr_from: $("#h_phrFrom").val(),
                min_from: $("#h_pminFrom").val(),
                hr_to: $("#h_phrTo").val(),
                min_to: $("#h_pminTo").val()
            },
            dataType: "html",
            success: function (data) {
                if (data != null) {
                    var printContents = data;
                    var html = printContents;

                    var printWin = window.open('', '', 'left=0,top=0,width=900,height=600,toolbar=0,scrollbars=0,status  =0');
                    printWin.document.write(html);
                    printWin.document.close();
                    printWin.focus();
                    printWin.print();
                    printWin.close();
                }
            }
        });

        
    }


</script>
<link rel="stylesheet" href="Styles/colorbox.css" />
<script type="text/javascript" src="Scripts/jquery.colorbox.js"></script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1" ClientIDMode="Static">
    
    <form id="form1" runat="server">

    <h1>รายการข้อมูลรถเข้าชั่ง</h1>
    <br />
    <input type="hidden" value="" id="h_pstation" runat="server" />
    <input type="hidden" value="" id="h_pslane" runat="server" />
    <input type="hidden" value="" id="h_poverweight" runat="server" />
    <input type="hidden" value="" id="h_pFromDate" runat="server" />
    <input type="hidden" value="" id="h_pToDate" runat="server" />
    <input type="hidden" value="" id="h_phrFrom" runat="server" />
    <input type="hidden" value="" id="h_pminFrom" runat="server" />
    <input type="hidden" value="" id="h_phrTo" runat="server" />
    <input type="hidden" value="" id="h_pminTo" runat="server" />
    <div class="box1">
    <div>
        Filter : 
        <select id="station" name="station" runat="server" enableviewstate="true"> </select><select id="lane" name="lane" runat="server" enableviewstate="true"></select> |  | 
        From <input type="text" name="from_date" id="from_date" runat="server" enableviewstate="true" /> 
        <select runat="server" name="hr_from" id="cboHrFrom" enableviewstate="true" ></select>:
        <select runat="server" name="min_from" id="cboMinFrom" enableviewstate="true"></select>
        To 
        <input type="text" runat="server" name="to_date" id="to_date" enableviewstate="true" /> 
                <select runat="server" name="hr_to" id="cboHrTo" enableviewstate="true"></select>:
        <select runat="server" name="min_to" id="cboMinTo" enableviewstate="true"></select>
|
        <select name="overweight" id="overweight" runat="server" enableviewstate="true">
            <option value="">-All Weight-</option>
            <option value="0">Normal Weight</option>
            <option value="1">Over Weight</option>
        </select>
    <%--<input type="button" value="   Print   " onclick="printIt()" />--%>
        <%--<asp:Button ID="Button1" runat="server" Text="  Print  " 
            onclick="Button1_Click" />--%>
    <div style="text-align:center">        <br />
            <asp:Button ID="btSearch" Text="Search" runat="server" 
            onclick="btSearch_Click" /></div>
    </div>

    </div>
    &nbsp;<div id="divHiddenFields" style="display:none;">
        <input type="text" name="txtPageNo" id="txtPageNo" runat="server" enableviewstate="false" style="width:15px;" />
        <asp:Button ID="btSearchByPage" Text="Search By Page" runat="server" onclick="btSearchByPage_Click" />
        <input type="text" id="txtSearchCondDebug" runat="server" enableviewstate="false" style="width:600px;" />
    </div>
    <br />
    <div>
        <table id="tbReport" border="1" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th rowspan="2">Time</th>
                <th rowspan="2">Station</th>
                <th rowspan="2">Lane</th>
                <th rowspan="2">Axle</th>
                <th colspan="10">Weight Axle</th>
                <th rowspan="2">GVW</th>
                <th rowspan="2">M.GVW</th>
                <th rowspan="2">M.AXW</th>
                <th rowspan="2">Img Front</th>
                <th rowspan="2">Img Back</th>
                <th rowspan="2" width="10%">OverWeight</th>
                <th rowspan="2" width="5%">Status</th>
            </tr>
            <tr class="tr_rowspan">
                <th width="5%">1</th>
                <th width="5%">2</th>
                <th width="5%">3</th>
                <th width="5%">4</th>
                <th width="5%">5</th>
                <th width="5%">6</th>
                <th width="5%">7</th>
                <th width="5%">8</th>
                <th width="5%">9</th>
                <th width="5%">10</th>
            </tr>

            <!-- Data -->
            <%
                if (weighings != null && weighings.Count > 0)
                {
                    int counter = 0;
                    int rowClass = 0;
                    foreach (DMT.WS.Dto.WeighingDTO dto in weighings)
                    {
                        string rowClassName = "";
                        if (rowClass == 0)
                        {
                            rowClassName = "class=\"row1\"";
                            rowClass = 1;
                        }
                        else
                        {
                            rowClassName = "class=\"row2\"";
                            rowClass = 0;
                        }
                        counter++;
                %>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $(".pic<%=counter %>").colorbox({ rel: 'pic<%=counter %>', width: '900px' });
                    });

		        </script>
                <tr <%=rowClassName %>>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=dto.Timestamp %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%= ((DMT.WS.Dto.StationDTO)new DMT.WS.Dao.StationDAO().getByPrimaryKey( dto.StationID)).StationNameTh %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=dto.LaneID %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=dto.NumberAxles %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightAxle01.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightAxle02.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightAxle03.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightAxle04.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightAxle05.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightAxle06.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightAxle07.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightAxle08.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightAxle09.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightAxle10.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightGVW.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightMaxGVW.ToString()) %></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(dto.WeightMaxAXW.ToString())%></td>
                <td align="center"><a href="<%= dto.Image01 %>" class="pic<%=counter %>" title="Img Front"><img alt="" border="0" src="<%= dto.Image01 %>" width="80" /></a></td>
                <td align="center"><a href="<%= dto.Image02 %>" class="pic<%=counter %>" title="Img Back"><img alt="" border="0" src="<%= dto.Image02 %>" width="80" /></a></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'" align="center"><img alt="" src="<%=(dto.IsOverWeight == false) ? "images/ico_circle_green.png" : "images/ico_circle_red.png"%>" /></td>
                <td onclick="window.location = 'Weighing_View.aspx?WID=<%=dto.ID %>'" align="center"><%=dto.RecordStatus.Length >=2 ? dto.RecordStatus.Substring(0,2).Equals("OK") ? "NORMAL" : dto.RecordStatus : "" %></td>
            </tr>
                <%
                    }
                }
                else
                {
                %>
                  <tr>
                <td colspan="20"><i>- No item found -</i></td>
            </tr>
                  <%  
                }
                 %>
            
       </table>
       <div style="text-align:right; margin-top:10px">Page : <%=PagingHtml%></div>
    </div>
    </form>
           
</asp:Content>

