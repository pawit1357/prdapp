﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="UserManagement.aspx.cs" Inherits="DMT.UserManagement" %>
<%@ Import Namespace="DMT.WS.Utils" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
       
       
        #tbReport .c-manage img
        {
            width:16px;
            height:16px;
            border: solid 1px transparent;
            cursor: pointer;
            padding:3px;
        }
        
        #tbReport .c-manage img:hover
        {
            border-color: Blue;
        }
        
        #tbReport tr.data:hover td 
        {
            background-color: InfoBackground;
        }
        a:hover 
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <h1>รายชื่อผู้ใช้ระบบ (User)</h1>
    <br />
    <h3><a href="User_Add.aspx">Add New User</a></h3>
    <br />
    <div>
        <table id="tbReport" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th class='c-username'>Username</th>
                <th class='c-email'>E-Mail</th>
                <th class='c-fullname'>Full Name</th>
                <th class='c-status'>Status</th>
                <th class='c-level'>Level</th>
                <th class='c-manage'>Manage</th>
            </tr>
            <!-- Loop to show user's record detail -->
            <% if (UserDataSet != null)
               {
                   string pattern = @"
               <tr class='row{6}'>
                    <td class='c-username'>{0}</td>
                    <td class='c-email'>{1}</td>
                    <td class='c-fullname'>{2}</td>
                    <td class='c-status'>{3}</td>
                    <td class='c-level'>{4}</td>
                    <td class='c-manage' data-userid='{5}'>
                        <img src='images/view.png' class='view-user' /><img src='images/edit.png' class='edit-user' /><img src='images/cross.png' class='del-user' />
                    </td>
               </tr>" + "\n";
                
                   System.Data.DataSet ds = UserDataSet;
                   int rowClass = 1;
                   foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                   {
                       //ID	Username	Password	FullName	Email	        Status	UserLevel
                       //1	admin	    admin	    Test Admin2	admin@email.com	1	    1
                       int id = Gm.toSafeInt(dr["ID"]);
                       string username = Gm.toSafeString(dr["Username"]);
                       string fullName = Gm.toSafeString(dr["FullName"]);
                       string email = Gm.toSafeString(dr["Email"]);
                       string statusName = Gm.toSafeString(dr["StatusName"]);
                       string userLevelName = Gm.toSafeString(dr["UserLevelName"]);
                       string rowClassName = "";
                       if (rowClass == 1)
                       {
                           rowClassName = "1";
                           rowClass = 2;
                       }
                       else
                       {
                           rowClassName = "2";
                           rowClass = 1;
                       }

                       string tr = string.Format(pattern, username, email, fullName, statusName, userLevelName, id, rowClassName);
                       Response.Write(tr);
                   }
               }
            %>
       </table>
    </div>
    <script type="text/javascript">
        $(function () {
            // binding function view, edit, delete to each row
            $('#tbReport').on('click', 'img.view-user', onViewUser);
            $('#tbReport').on('click', 'img.edit-user', onEditUser);
            $('#tbReport').on('click', 'img.del-user', onDeleteUser);
        });

        function onViewUser() {
            var id = $(this).parent().attr('data-userid');
            var url = "User_View.aspx?UserID=" + id + "&refresh=" + (new Date()).getTime();
            window.location = url;
        }

        function onEditUser() {
            var id = $(this).parent().attr('data-userid');
            var url = "User_Edit.aspx?UserID=" + id + "&refresh=" + (new Date()).getTime();
            window.location = url;
        }

        function onDeleteUser() {
            var id = $(this).parent().attr('data-userid');
            var fullname = $(this).parent().parent().find('td.c-fullname').text();
            //alert('onDeleteUser ' + id);

            var r = confirm("ยืนยันการลบรายชื่อผู้ใช้ ["+ fullname +"]");
            if (r == true) {
                var url = "DeleteUser.aspx?UserID=" + id + "&refresh=" + (new Date()).getTime();
                window.location = url;
            }
        }

    </script>
</asp:Content>