﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using DMT.WS.Dto;
using DMT.WS.Dao;
using DMT.WS.Utils;
using DMT.WS.Biz;

namespace DMT
{
    public partial class Report_Edit : System.Web.UI.Page
    {
        protected Boolean SaveSucceed
        {
            get;
            set;
        }

        protected string SaveResultMessage
        {
            get;
            set;
        }

        protected ReportDTO ReportDTO
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("EDIT_USER"))
                Response.Redirect("LiveWeighing.aspx");

            SaveSucceed = false;
            SaveResultMessage = "";
            ReportDTO = null;

            if (!IsPostBack)
            {
                int id = Gm.toSafeInt(Request.QueryString["ReportID"]);

                if (id > 0)
                {
                    // Load User Info
                    ReportDAO dao = new ReportDAO();
                    ReportDTO report = dao.getByPrimaryKey(id) as ReportDTO;

                    if (report != null)
                    {
                        ViewState["ReportID"] = report.ID;
                        txtName.Value = report.Name;
                        txtDescription.Value = report.Description;
                        txtFilePath.Value = report.FilePath;
                        ReportDTO = report;
                    }
                    else
                    {
                        SaveResultMessage = "ไม่พบรายการที่ระบุ";
                    }
                }
                else
                {
                    SaveResultMessage = "ไม่สามารถแก้ไขรายการที่ระบุได้";
                }
            }
        }

        protected void btSave_OnClick(object Source, EventArgs e)
        {
            ReportDTO report = new ReportDTO();
            report.ID = Gm.toSafeInt(ViewState["ReportID"]);
            report.Name = txtName.Value;
            report.Description = txtDescription.Value;
            report.FilePath = txtFilePath.Value;

            ReportDAO dao = new ReportDAO();

            try
            {
                SaveSucceed = dao.update(report);
            }
            catch (Exception)
            {
                SaveSucceed = false;
            }

            SaveResultMessage = (SaveSucceed) ? "บันทึกข้อมูลเรียบร้อยแล้ว" : "บันทึกไม่สำเร็จ (Save Failed) !!!";

        }
    }
}