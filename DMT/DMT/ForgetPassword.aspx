﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="ForgetPassword.aspx.cs" Inherits="DMT._Default" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <h1>Forget Password</h1>
    <table>
    <tr><td align="right">Enter your <b>Username</b> : </td><td><input type="text" /></td></tr>
    <tr><td colspan="2" align="center"><b>OR</b></td></tr>
    <tr><td align="right">Enter your <b>E-mail Address</b> : </td><td><input type="text" /></td></tr> 
    <tr><td colspan="2">System will send link for reset your password to your registered email.</td></tr>
    <tr><td align="right"></td><td><input type="button" value="Submit" /></td></tr> 
    </table>    
</asp:Content>