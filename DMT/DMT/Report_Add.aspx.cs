﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMT.WS.Utils;
using DMT.WS.Dto;
using DMT.WS.Dao;
using DMT.WS.Biz;




namespace DMT
{
    public partial class Report_Add : System.Web.UI.Page
    {
        protected Boolean SaveSucceed
        {
            get;
            set;
        }

        protected string SaveResultMessage
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("ADD_REPORT"))
                Response.Redirect("LiveWeighing.aspx");

            SaveSucceed = false;
            SaveResultMessage = "";
        }

        protected void btSave_OnClick(object Source, EventArgs e)
        {
            ReportDTO report = new ReportDTO();
            report.Name = txtName.Value;
            report.Description = txtDescription.Value;
            report.FilePath = txtFilePath.Value;

            ReportDAO dao = new ReportDAO();
            ReportDTO reportResult = null;

            try
            {
                reportResult = dao.save(report) as ReportDTO;
            }
            catch (Exception)
            {
                reportResult = null;
            }

            SaveSucceed = (reportResult != null);
            SaveResultMessage = (SaveSucceed) ? "บันทึกข้อมูลใหม่เรียบร้อยแล้ว" : "บันทึกไม่สำเร็จ (Save Failed) !!!";

            if (SaveSucceed)
            {
                // clear input
                txtName.Value = "";
                txtDescription.Value = "";
                txtFilePath.Value = "";
            }
        }
    }
}