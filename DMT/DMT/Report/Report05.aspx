﻿<%@ Page Title="Default" Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.Master"
    CodeBehind="Report05.aspx.cs" Inherits="DMT.Report05" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"
    ClientIDMode="Static">
    <script type="text/javascript" src="/DMT/Scripts/Highcharts-3.0.10/js/highcharts.js"></script>
    <script type="text/javascript" src="/DMT/Scripts/Highcharts-3.0.10/js/modules/exporting.js"></script>
    <script type="text/javascript" src="/DMT/Scripts/Report3.js"></script>

    <script type="text/javascript">
        $(function () {
            $("#txtStartDate").datetimepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#txtEndDate").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#txtEndDate").datetimepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#txtStartDate").datepicker("option", "maxDate", selectedDate);
                }
            });

            // Make Default Radio
            RadionButtonSelectedValueSet('periodType', $('#hdPeriodType').val());
            RadionButtonSelectedValueSet('graphType', $('#hdGraphType').val());

        });

        function RadionButtonSelectedValueSet(name, SelectdValue) {
            $('input[name="' + name + '"][value="' + SelectdValue + '"]').prop('checked', true);
        }
    </script>
    <h1>รายงานสรุปปริมาณรถเข้าชั่งเปรียบเทียบแต่ละช่วงเวลา</h1>

    <div class="box1">
        <form id="Form1" runat="server">
            <table align="center">
                <tr>
                    <td align="right">Station : </td>
                    <td>
                        <asp:DropDownList ID="cboStation" runat="server" AutoPostBack="True" DataTextField="StationNameTh" DataValueField="ID" OnSelectedIndexChanged="cboStation_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">Lane :</td>
                    <td>
                        <asp:DropDownList ID="cboLane" runat="server" DataTextField="LaneName" DataValueField="ID">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="style1">Is Over
                    </td>
                    <td class="style1">
                         <asp:DropDownList ID="cboOver" runat="server">
                            <asp:ListItem Value="-1">-ทั้งหมด-</asp:ListItem>
                            <asp:ListItem Value="1">เกิน</asp:ListItem>
                            <asp:ListItem Value="0">ไม่เกิน</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">Start : </td>
                    <td>
                        <input type="text" runat="server" id="txtStartDate" /></td>
                </tr>
                <tr>
                    <td align="right">End : </td>
                    <td>
                        <input type="text" runat="server" id="txtEndDate" /></td>
                </tr>
                <tr>
                    <td align="right">Period:
                    </td>
                    <td>
                        <input type="radio" name="periodType" onclick="$('#hdPeriodType').val('1')" value="1" checked="checked" />
                        ปี 
                <input type="radio" name="periodType" onclick="$('#hdPeriodType').val('2')" value="2" />
                        เดือน 
                <input type="radio" name="periodType" onclick="$('#hdPeriodType').val('3')" value="3" />
                        วัน 
                <input type="radio" name="periodType" onclick="$('#hdPeriodType').val('4')" value="4" />
                        ชั่วโมง 
                <asp:RadioButtonList Style="display: inline" Visible="false" RepeatDirection="Horizontal" ID="rdPeriod"
                    runat="server">
                </asp:RadioButtonList>
                        <input type="hidden" id="hdPeriodType" runat="server" value="1" />
                    </td>
                </tr>
                <tr>
                    <td align="right">Data Format :
                    </td>
                    <td>
<%--                        <input type="radio" name="graphType" onclick="$('#hdGraphType').val('0')" value="0" checked="checked" />
                        ปกติ --%>
                    <input type="radio" name="graphType" onclick="$('#hdGraphType').val('1')" value="1"  checked="checked"  />
                        กราฟเส้น 
                <input type="radio" name="graphType" onclick="$('#hdGraphType').val('2')" value="2" />
                        กราฟแท่ง 
                <asp:RadioButtonList Style="display: inline" Visible="false" RepeatDirection="Horizontal" ID="radioGraphType"
                    runat="server">
                </asp:RadioButtonList>
                        <input type="hidden" id="hdGraphType" runat="server" value="1" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSubmit" Text="Search" runat="server" OnClick="btnSubmit_Click" />

                        &nbsp;<asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div>
        <iframe id="ifContent" width="100%" height="0px" runat="server"></iframe>
    </div>
    <div id="reportArea"></div>
    <br />
    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    <a href="Report.aspx">ย้อนกลับ (Back)</a>

</asp:Content>


<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style1 {
            height: 24px;
        }
    </style>
</asp:Content>
