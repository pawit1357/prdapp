﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using DMT.WS.Dto;
using DMT.WS.Dao;
using DMT.WS.Biz;
using DMT.WS.Utils;

namespace DMT
{
    public partial class ReportListCar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string report = Request.Params["report"];
            //if (report == "1")
            //{
            //    Response.Redirect("ReportEachCar.aspx");
            //}
            //else if (report == "2")
            //{
            //    Response.Redirect("ReportListCar.aspx");
            //}
            //else if (report == "3")
            //{
            //    Response.Redirect("ReportSummaryAllStation.aspx");
            //}
            //else if (report == "0")
            //{
            //    Response.Redirect("Report.aspx");
            //}
            if (!IsPostBack)
            {
                //txtStartDate.Value = DateTime.Now.ToString("dd/mm/yy");
                //txtEndDate.Value = DateTime.Now.ToString("dd/mm/yy");

                prepareItem();
            }
        }

        private void prepareItem()
        {
            cboStation.Items.Add(new ListItem("-ทั้งหมด-", ""));
            List<StationDTO> stations = new StationDAO().getAll();
            foreach (StationDTO station in stations)
            {
                cboStation.Items.Add(new ListItem(station.StationNameEn, "" + station.ID));
            }
            radioGraphType.Items.Add(new ListItem("ปกติ", "1"));
            radioGraphType.Items.Add(new ListItem("กราฟแท่ง", "2"));
            radioGraphType.Items.Add(new ListItem("กราฟเส้น", "3"));
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            prepareDataSet();
        }

        private void prepareDataSet()
        {
            //string stationId = cboStation.Value;
            //string fromDate = txtStartDate.Value;
            //string toDate = txtEndDate.Value;
            //string graphType = radioGraphType.SelectedValue;

            //if (fromDate.Length == 10)
            //    fromDate = Gm.toThaiDateToSqlDate(fromDate);

            //if (toDate.Length == 10)
            //    toDate = Gm.toThaiDateToSqlDate(toDate);

            //List<WeighingBean> beans = new WeighingBAO().getByCondition(stationId, fromDate, toDate);
            //txtDebug.Value = "Station : " + stationId + ", From Date : " + fromDate + ", To Date : " + toDate + ", Graph Type : " + graphType;
        }
    }
}