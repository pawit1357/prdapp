﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CrystalDecisions.CrystalReports.Engine;
using DMT.WS.Utils;
using DMT.WS.Dto;
using DMT.WS.Biz;
using DMT.WS.Dao;

namespace DMT
{
    public partial class Report05 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                initialPage();
            }

        }

        private void initialPage()
        {

            List<StationDTO> stations = new StationDAO().getAll();
            cboStation.Items.Clear();
            cboStation.DataSource = stations;
            cboStation.DataBind();
            cboStation.Items.Insert(0, new ListItem("-ทั้งหมด-", "0"));

            List<LaneDTO> lanes = new LaneDAO().getAll();
            cboLane.Items.Clear();
            cboLane.DataSource = lanes;
            cboLane.DataBind();
            cboLane.Items.Insert(0, new ListItem("-ทั้งหมด-", "0"));

            cboLane.Enabled = false;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //string graphType = hdGraphType.Value;
            //if (graphType != "0")
            //{
            //    this.ifContent.Attributes["height"] = "0px";
            //    this.ifContent.Attributes["src"] = null;
            //    ScriptManager.RegisterStartupScript(this, GetType(), "loadReport", "loadReport();", true);
            //}
            //else
            //{
                beginSearch();
            //}
        
        }

        private void beginSearch()
        {

            List<WeighingDTO> beans = new WeighingDAO().getReportByCondition(cboStation.SelectedValue.ToString(), cboLane.SelectedValue.ToString(), txtStartDate.Value.ToString(), txtEndDate.Value, cboOver.SelectedValue.ToString());
            if (beans != null && beans.Count > 0)
            {

                string graphType = hdGraphType.Value;
                string period = hdPeriodType.Value;

                ReportDocument rptH = new ReportDocument();
                rptH.FileName = Server.MapPath("~/Rpt/Report05_" + period + "_"+graphType+".rpt");


                rptH.SetDataSource(beans);

                rptH.SetParameterValue("START_DATE", (txtStartDate.Value.Length == 10) ? txtStartDate.Value : "-");
                rptH.SetParameterValue("END_DATE", (txtEndDate.Value.Length == 10) ? "" + txtEndDate.Value : "-");
                if (cboStation.Text.Length != 0)
                {
                    rptH.SetParameterValue("P_STATION", cboStation.SelectedItem.Text);
                }
                else
                {
                    rptH.SetParameterValue("P_STATION", "ทั้งหมด");
                }
                string periodText = string.Empty;
                switch (period) {
                    case "1": periodText = "รายปี";
                        break;
                    case "2": periodText = "รายเดือน";
                        break;
                    case "3": periodText = "รายวัน";
                        break;
                    case "4": periodText = "รายชั่วโมง";
                        break;
                }

                rptH.SetParameterValue("P_PERIOD", periodText);
                try
                {
                    string appPath = HttpContext.Current.Request.ApplicationPath;
                    string physicalPath = HttpContext.Current.Request.MapPath(appPath);
                    string filename = "tmpreport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";
                    rptH.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, physicalPath + "\\Rpt_tmp\\" + filename);

                    string url = Configurations.ReportPath + "/" + filename;
                    this.ifContent.Attributes["height"] = "900px";
                    this.ifContent.Attributes["src"] = url;
                    //string s = "window.open('" + url + "', 'popup_window', 'width=800,height=600,left=100,top=100,resizable=yes');";
                    //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                    //rptH.PrintToPrinter(1, false, 0, 0);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
            }
        }

        //protected void btnRptPrint_Click(object sender, EventArgs e)
        //{
        //    printReport();
        //}

        //private void beginSearch()
        //{
            //StationDAO stationDao = new StationDAO();
            //string stationId = cboStation.Value;
            //string fromDate = txtStartDate.Value;
            //string toDate = txtEndDate.Value;
            //string graphType = hdGraphType.Value;
            //string period = hdPeriodType.Value;
            //string hrFrom = cboHrFrom.Value;
            //string hrTo = cboHrTo.Value;
            //string minFrom = cboMinFrom.Value;
            //string minTo = cboMinTo.Value;
            //string overWeight = cboOver.Value;

            //if (stationId == "") stationId = null;
            //if (overWeight == "False") overWeight = "0";
            //else if (overWeight == "True") overWeight = "1";
            //else overWeight = null;

            //if (fromDate.Length == 10)
            //    fromDate = Gm.toThaiDateToSqlDate(fromDate) + " " + Gm.toThaiDateToSqlTime(hrFrom, minFrom);
            //else fromDate = null;
            //if (toDate.Length == 10)
            //    toDate = Gm.toThaiDateToSqlDate(toDate) + " " + Gm.toThaiDateToSqlTime(hrTo, minTo);
            //else toDate = null;
            //List<WeighingSummaryAllStationDTO> beans = new WeighingBAO().getSummaryAllStation(period, stationId, fromDate, toDate, overWeight);
       
            //if (beans != null && beans.Count > 0)
            //{
            //    ReportDocument rptH = new ReportDocument();
            //    if (graphType == "2") rptH.FileName = Server.MapPath("~/Rpt/ReportSummaryAllStation_BarChart.rpt");
            //    else rptH.FileName = Server.MapPath("~/Rpt/ReportSummaryAllStation_LineChart.rpt");
            //    rptH.SetDataSource(beans);
            //    rptH.SetParameterValue("START_DATE", (txtStartDate.Value.Length == 10) ? txtStartDate.Value : "All Time");
            //    rptH.SetParameterValue("END_DATE", (txtEndDate.Value.Length == 10) ? " To " + txtEndDate.Value : "");
            //    if (cboStation.Value.Length != 0)
            //    {
            //        StationDTO stationDto = (StationDTO)stationDao.getByPrimaryKey(Convert.ToInt16(cboStation.Value));
            //        rptH.SetParameterValue("P_STATION", (cboStation.Value.Length != 0) ? stationDto.StationNameEn : "");
            //    }
            //    else {
            //        rptH.SetParameterValue("P_STATION",  "All");
            //    }


            //    try
            //    {
            //        string appPath = HttpContext.Current.Request.ApplicationPath;
            //        string physicalPath = HttpContext.Current.Request.MapPath(appPath);
            //        string filename = "tmpreport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";
            //        rptH.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, physicalPath + "\\Rpt_tmp\\" + filename);

            //        string url = Configurations.ReportPath + "/" + filename;
            //        string s = "window.open('" + url + "', 'popup_window', 'width=800,height=600,left=100,top=100,resizable=yes');";
            //        ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

            //        //rptH.PrintToPrinter(1, false, 0, 0);
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.Message);
            //    }

            //    //Session["rptH"] = rptH;
            //    //CrystalReportViewer1.ReportSource = rptH;
            //    //CrystalReportViewer1.Zoom(80);

            //    //btnPrint.Visible = true;

            //}
            //else {
            //    btnPrint.Visible = false;
            //    Session["resultMessage"] = "ไม่พบข้อมูล";
            //    Session["resultClass"] = "error";
            //    //Write data not found message.
            //}

            //txtDebug.Value = "Station : " + stationId + ", From Date : " + fromDate + ", To Date : " + toDate + ", Graph Type : " + graphType;
        //}

        protected void btnRptPrint_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            cboLane.Enabled = false;
            cboStation.SelectedIndex = 0;
            this.ifContent.Attributes["src"] = null;
        }

        protected void cboStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            int stationId = Convert.ToInt32(cboStation.SelectedValue);
            if (stationId == 0)
            {
                cboLane.Enabled = false;
            }
            else
            {
                List<LaneDTO> lanes = new LaneDAO().getLaneByStation(stationId);
                cboLane.Items.Clear();
                cboLane.DataSource = lanes;
                cboLane.DataBind();
                cboLane.Items.Insert(0, new ListItem("-ทั้งหมด-", "0"));
                cboLane.Enabled = true;
            }
        }

        //private void printReport()
        //{
            //StationDAO stationDao = new StationDAO();
            //string stationId = cboStation.Value;
            //string fromDate = txtStartDate.Value;
            //string toDate = txtEndDate.Value;
            //string graphType = hdGraphType.Value;
            //string period = hdPeriodType.Value;
            //string hrFrom = cboHrFrom.Value;
            //string hrTo = cboHrTo.Value;
            //string minFrom = cboMinFrom.Value;
            //string minTo = cboMinTo.Value;
            //string overWeight = cboOver.Value;

            //if (graphType != "0")
            //{
            //    this.ifContent.Attributes["height"] = "0px";
            //    this.ifContent.Attributes["src"] = null;
            //    ScriptManager.RegisterStartupScript(this, GetType(), "loadReport", "loadReport();", true);
            //}
            //else
            //{

            //    if (stationId == "") stationId = null;
            //    if (overWeight == "False") overWeight = "0";
            //    else if (overWeight == "True") overWeight = "1";
            //    else overWeight = null;

            //    if (fromDate.Length == 10)
            //        fromDate = Gm.toThaiDateToSqlDate(fromDate) + " " + Gm.toThaiDateToSqlTime(hrFrom, minFrom);
            //    else fromDate = null;
            //    if (toDate.Length == 10)
            //        toDate = Gm.toThaiDateToSqlDate(toDate) + " " + Gm.toThaiDateToSqlTime(hrTo, minTo);
            //    else toDate = null;
            //    List<WeighingSummaryAllStationDTO> beans = new WeighingBAO().getSummaryAllStation(period, stationId, fromDate, toDate, overWeight);

            //    if (beans != null && beans.Count > 0)
            //    {

            //        ReportDocument rptH = new ReportDocument();
            //        if (graphType == "2") rptH.FileName = Server.MapPath("~/Rpt/ReportSummaryAllStation_BarChart.rpt");
            //        else rptH.FileName = Server.MapPath("~/Rpt/ReportSummaryAllStation_LineChart.rpt");
            //        rptH.SetDataSource(beans);
            //        rptH.SetParameterValue("START_DATE", (txtStartDate.Value.Length == 10) ? txtStartDate.Value : "-");
            //        rptH.SetParameterValue("END_DATE", (txtEndDate.Value.Length == 10) ? "" + txtEndDate.Value : "-");
            //        if (cboStation.Value.Length != 0)
            //        {
            //            StationDTO stationDto = (StationDTO)stationDao.getByPrimaryKey(Convert.ToInt16(cboStation.Value));
            //            rptH.SetParameterValue("P_STATION", (cboStation.Value.Length != 0) ? stationDto.StationNameEn : "");
            //        }
            //        else
            //        {
            //            rptH.SetParameterValue("P_STATION", "ทั้งหมด");
            //        }


            //        /*
            //        Session["rptH"] = rptH;
            //        CrystalReportViewer1.ReportSource = rptH;
            //        CrystalReportViewer1.Zoom(80);
            //        btnPrint.Visible = true;
            //        */
            //        //rptH.PrintToPrinter(1, false, 0, 0);
            //        try
            //        {
            //            string appPath = HttpContext.Current.Request.ApplicationPath;
            //            string physicalPath = HttpContext.Current.Request.MapPath(appPath);
            //            string filename = "tmpreport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";
            //            rptH.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, physicalPath + "\\Rpt_tmp\\" + filename);

            //            string url = Configurations.ReportPath + "/" + filename;
            //            this.ifContent.Attributes["height"] = "900px";
            //            this.ifContent.Attributes["src"] = url;
            //            //string s = "window.open('" + url + "', 'popup_window', 'width=800,height=600,left=100,top=100,resizable=yes');";
            //            //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

            //            //rptH.PrintToPrinter(1, false, 0, 0);
            //        }
            //        catch (Exception ex)
            //        {
            //            Console.WriteLine(ex.Message);
            //        }
            //    }
            //}
        //}
    }
}