﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Report04.aspx.cs" Inherits="DMT.Report04" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="/DMT/Scripts/Highcharts-3.0.10/js/highcharts.js"></script>
    <script type="text/javascript" src="/DMT/Scripts/Highcharts-3.0.10/js/modules/exporting.js"></script>
    <script type="text/javascript" src="/DMT/Scripts/Report3_1.js"></script>
    <style type="text/css" media="print">
        #btSearch0, #menu {
            visibility: hidden !important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("#txtStartDate").datetimepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#txtEndDate").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#txtEndDate").datetimepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#txtStartDate").datepicker("option", "maxDate", selectedDate);
                }
            });

            RadionButtonSelectedValueSet('graphType', $('#hdGraphType').val());

        });
        function RadionButtonSelectedValueSet(name, SelectdValue) {
            $('input[name="' + name + '"][value="' + SelectdValue + '"]').prop('checked', true);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ClientIDMode="Static">

    <h1>รายงานสรุปปริมาณรถเข้าชั่ง แยกประเภทรถ</h1>
    <br />
    <div class="box1">
        <form id="Form1" runat="server">

            <table align="center">
                <tr>
                    <td align="right">Station : </td>
                    <td>
                        <asp:DropDownList ID="cboStation" runat="server" AutoPostBack="True" DataTextField="StationNameTh" DataValueField="ID" OnSelectedIndexChanged="cboStation_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">Lane :</td>
                    <td>
                        <asp:DropDownList ID="cboLane" runat="server" DataTextField="LaneName" DataValueField="ID">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">Start : </td>
                    <td>
                        <input type="text" runat="server" id="txtStartDate" /></td>
                </tr>
                <tr>
                    <td align="right">End : </td>
                    <td>
                        <input type="text" runat="server" id="txtEndDate" /></td>
                </tr>
                <tr>
                    <td align="right" class="style1">Is Over
                    </td>
                    <td class="style1">

                        &nbsp;<asp:DropDownList ID="cboOver" runat="server">
                            <asp:ListItem Value="-1">-ทั้งหมด-</asp:ListItem>
                            <asp:ListItem Value="1">เกิน</asp:ListItem>
                            <asp:ListItem Value="0">ไม่เกิน</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">Data Format :
                    </td>
                    <td align="left">

                        <input type="radio" name="graphType" onclick="$('#hdGraphType').val('0')" value="0" checked="checked" />
                        ปกติ 
                <input type="radio" name="graphType" onclick="$('#hdGraphType').val('2')" value="2" />
                        กราฟแท่ง 
                        <input type="hidden" id="hdGraphType" runat="server" value="0" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSubmit" Text="Search" runat="server" OnClick="btnSubmit_Click" />
                        &nbsp;<asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
                    </td>
                </tr>
            </table>

        </form>
    </div>
    <div>
        <iframe id="ifContent" width="100%" height="0px" runat="server"></iframe>
    </div>
    <div id="reportArea"></div>
    <br />
    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

</asp:Content>
