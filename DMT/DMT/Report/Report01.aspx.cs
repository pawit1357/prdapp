﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;

using DMT.WS.Dto;
using DMT.WS.Dao;
using CrystalDecisions.CrystalReports.Engine;
using System.Net;

namespace DMT
{
    public partial class Report01 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                initialPage();
            }
        }

        private void initialPage()
        {

            List<StationDTO> stations = new StationDAO().getAll();
            cboStation.Items.Clear();
            cboStation.DataSource = stations;
            cboStation.DataBind();
            cboStation.Items.Insert(0, new ListItem("-ทั้งหมด-", "0"));

            List<LaneDTO> lanes = new LaneDAO().getAll();
            cboLane.Items.Clear();
            cboLane.DataSource = lanes;
            cboLane.DataBind();
            cboLane.Items.Insert(0, new ListItem("-ทั้งหมด-", "0"));

            cboLane.Enabled = false;
            btnPrint.Enabled = false;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            beginSearch();
        }

        private void beginSearch()
        {
            List<WeighingDTO> beans = new WeighingDAO().getReportByCondition(cboStation.SelectedValue.ToString(), cboLane.SelectedValue.ToString(), txtStartDate.Value.ToString(), txtEndDate.Value, cboOver.SelectedValue.ToString());
            if (beans != null && beans.Count > 0)
            {
                GridView1.DataSource = beans;
                GridView1.DataBind();
                Session["SearchResult"] = beans;
                btnPrint.Enabled = true;
                btnPrint.Enabled = true;
            }
            else
            {
                btnPrint.Enabled = false;
                btnPrint.Enabled = false;
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Select")
            {
                int ID = Convert.ToInt32(e.CommandArgument.ToString());
                WeighingDTO bean = (WeighingDTO)new WeighingDAO().getReportByConditionAsFirst(ID);
                if (bean != null)
                {
                    ReportDocument rptH = new ReportDocument();
                    rptH.FileName = Server.MapPath("~/Rpt/ReportEachCar.rpt");
                    List<WeighingDTO> tmps = new List<WeighingDTO>();
                    tmps.Add(bean);

                    rptH.SetDataSource(tmps);
                    rptH.SetParameterValue("P_STATION", (cboStation.SelectedIndex != 0) ? cboStation.Text : "");

                    try
                    {
                        string appPath = HttpContext.Current.Request.ApplicationPath;
                        string physicalPath = HttpContext.Current.Request.MapPath(appPath);
                        string filename = "tmpreport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";
                        rptH.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, physicalPath + "\\Rpt_tmp\\" + filename);

                        string url = Configurations.ReportPath + "/" + filename;
                        string s = "window.open('" + url + "', 'popup_window', 'width=800,height=600,left=100,top=100,resizable=yes');";
                        ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                }
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex < 0) return;
            GridView gv = (GridView)sender;
            gv.DataSource = Session["SearchResult"];
            gv.PageIndex = e.NewPageIndex;
            gv.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            var webClient = new WebClient();
            List<WeighingDTO> beans = (List<WeighingDTO>)Session["SearchResult"];
            if (beans != null && beans.Count > 0)
            {

                ReportDocument rptH = new ReportDocument();
                rptH.FileName = Server.MapPath("~/Rpt/ReportAllCar.rpt");
                rptH.SetDataSource(beans);

                rptH.SetParameterValue("START_DATE", (txtStartDate.Value == null) ? "-" : txtStartDate.Value);
                rptH.SetParameterValue("END_DATE", ((txtEndDate.Value == null) ? "-" : txtEndDate.Value));
                rptH.SetParameterValue("P_STATION", (cboStation.SelectedIndex != 0) ? cboStation.Text : "ทั้งหมด");

                try
                {
                    string appPath = HttpContext.Current.Request.ApplicationPath;
                    string physicalPath = HttpContext.Current.Request.MapPath(appPath);
                    string filename = "tmpreport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";
                    rptH.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, physicalPath + "\\Rpt_tmp\\" + filename);

                    string url = Configurations.ReportPath + "/" + filename;
                    string s = "window.open('" + url + "', 'popup_window', 'width=800,height=600,left=100,top=100,resizable=yes');";
                    ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session["SearchResult"] = null;
            GridView1.DataSource = null;
            GridView1.DataBind();
            btnPrint.Enabled = false;
            cboLane.Enabled = false;
            cboStation.SelectedIndex = 0;
            cboOver.SelectedIndex = 0;
        }

        protected void cboStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            int stationId = Convert.ToInt32(cboStation.SelectedValue);
            if (stationId == 0)
            {
                cboLane.Enabled = false;
            }
            else
            {
                List<LaneDTO> lanes = new LaneDAO().getLaneByStation(stationId);
                cboLane.Items.Clear();
                cboLane.DataSource = lanes;
                cboLane.DataBind();
                cboLane.Enabled = true;
            }
        }

    }
}