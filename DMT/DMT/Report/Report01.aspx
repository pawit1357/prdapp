﻿<%@ Page Title="Default" Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.Master" CodeBehind="Report01.aspx.cs" Inherits="DMT.Report01" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        $(function () {
            $("#txtStartDate").datetimepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#txtEndDate").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#txtEndDate").datetimepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#txtStartDate").datepicker("option", "maxDate", selectedDate);
                }
            });
        });
    </script>
    <h1>รายงานรถเข้าชั่งรายคัน</h1>
    <form id="Form1" runat="server">
        <div class="box1">
            <table align="center">
                <tr>
                    <td align="right">Station : </td>
                    <td>
                        <asp:DropDownList ID="cboStation" runat="server" AutoPostBack="True" DataTextField="StationNameTh" DataValueField="ID" OnSelectedIndexChanged="cboStation_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">Lane :</td>
                    <td>
                        <asp:DropDownList ID="cboLane" runat="server" DataTextField="LaneName" DataValueField="ID">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">Start : </td>
                    <td>
                        <input type="text" runat="server" id="txtStartDate" /></td>
                </tr>
                <tr>
                    <td align="right">End : </td>
                    <td>
                        <input type="text" runat="server" id="txtEndDate" /></td>
                </tr>
                <tr>
                    <td align="right" class="auto-style1">Is Over
                    </td>
                    <td class="auto-style1">
                        &nbsp;<asp:DropDownList ID="cboOver" runat="server">
                            <asp:ListItem Value="-1">-ทั้งหมด-</asp:ListItem>
                            <asp:ListItem Value="1">เกิน</asp:ListItem>
                            <asp:ListItem Value="0">ไม่เกิน</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSubmit" Text="Search" runat="server" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnPrint" runat="server" OnClick="Button1_Click"
                            Text="  Print  " />
                        &nbsp;<asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                            CellPadding="4" ForeColor="#333333" GridLines="None"
                            OnRowCommand="GridView1_RowCommand" EmptyDataText="ไม่พบข้อมูล"
                            AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging"
                            PageSize="15">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="ID" HeaderText="ID" />
                                <asp:TemplateField HeaderText="Image" Visible="False">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("image01") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("image01") %>' Width="50px" Height="50px" />
                                    </ItemTemplate>
                                    <ItemStyle Height="50px" Width="50px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="timestamp" HeaderText="DateTime" />
                                <asp:BoundField DataField="StationName" HeaderText="Station" ItemStyle-HorizontalAlign="left"/>
                                <asp:BoundField DataField="laneID" HeaderText="Lane" />
                                <asp:BoundField DataField="weightAxle01" HeaderText="Axle1" />
                                <asp:BoundField DataField="weightAxle02" HeaderText="Axle2" />
                                <asp:BoundField DataField="weightAxle03" HeaderText="Axle3" />
                                <asp:BoundField DataField="weightAxle04" HeaderText="Axle4" />
                                <asp:BoundField DataField="weightAxle05" HeaderText="Axle5" />
                                <asp:BoundField DataField="weightAxle06" HeaderText="Axle6" />
                                <asp:BoundField DataField="weightAxle07" HeaderText="Axle7" />
                                <asp:BoundField DataField="weightAxle08" HeaderText="Axle8" />
                                <asp:BoundField DataField="weightAxle09" HeaderText="Axle9" />
                                <asp:BoundField DataField="weightAxle10" HeaderText="Axle10" />
                                <asp:BoundField DataField="WeightGVW" HeaderText="GVW" />
                                <asp:CheckBoxField DataField="isOverWeight" HeaderText="OverWeight" />
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False"
                                            CommandName="Select" CommandArgument='<%# Eval("ID") %>' Text="Print"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#2461BF" />
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
    <%--    <a href="Report.aspx">ย้อนกลับ (Back)</a>--%>
</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .auto-style1 {
            height: 29px;
        }
    </style>
</asp:Content>

