﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DMT.WS.Biz;
using DMT.WS.Utils;
using DMT.WS.Dao;


namespace DMT
{
    public partial class DeleteUser : System.Web.UI.Page
    {
        protected Boolean DeleteSucceed
        {
            get;
            set;
        }

        protected string DeleteResultMessage
        {
            get;
            set;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("DELETE_USER"))
                Response.Redirect("LiveWeighing.aspx");

            DeleteSucceed = false;
            DeleteResultMessage = "";

            int id = Gm.toSafeInt(Request.QueryString["UserID"]);

            if (id > 0)
            {
                DeleteSucceed = DeleteUserLogin(id);
                if (DeleteSucceed)
                    DeleteResultMessage = "ลบรายการสำเร็จ";
                else
                    DeleteResultMessage = (DeleteResultMessage.Length>0) ? DeleteResultMessage : "ลบรายการไม่สำเร็จ (Failed)";
            }
            else
            {
                DeleteResultMessage = "ไม่สามารถลบรายการที่ระบุได้";
            }
        }

        private Boolean DeleteUserLogin(int userId)
        {
            try
            {
                ConnectionDB connection = new ConnectionDB();
                string sql = "DELETE [UserLogin] WHERE [ID]=@id;";
                SqlCommand cmd = new SqlCommand(sql);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = userId;

                connection.OpenConnection();
                int n = connection.ExecuteNonQuery(cmd);
                connection.CloseConnection();
                return (n == 1);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                DeleteResultMessage = "Exception Occured : " + ex.GetType().Name;
                return false;
            }
        }
    }
}