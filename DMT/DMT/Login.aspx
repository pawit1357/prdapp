﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DMT.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link rel="Stylesheet" href="/DMT/Styles/Login.css" />
</head>
<body>
    <div id="wrapper">
        <div id="header">บริษัท ทางยกระดับดอนเมือง จำกัด</div>
        <div id="post">
            <form action="Login.aspx" method="post">
                <div id="wrongpassword" runat="server" visible="false" style="color:Red; font-size:13px; position:absolute; top:250px; left:700px">Invalid Username Or Password</div>
                <input type="text" name="username" id="txtUsername" />
                <input type="password" name="password" id="txtPassword" />
                <div id="rememberBox"><input type="checkbox" name="remember" /> Remember Me</div>
                <input type="submit" name="submit" value="Submit" id="btnSubmit" />                
            </form>
            <div id="forgetPasswordBox"><a href="ForgetPassword.aspx">Forget Password</a></div>
        </div>
        <div id="footer"></div>
    </div>
</body>
</html>
