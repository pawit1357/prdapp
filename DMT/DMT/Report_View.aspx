﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Report_View.aspx.cs" Inherits="DMT.Report_View" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tbAddNewUser 
        {
            table-layout: fixed;
            border: 0px;
        }
        
        #tbAddNewUser td
        {
            padding-top: 0.3em;
            padding-bottom: 0.1em;
        }
        
        #tbAddNewUser td.label 
        {
            width: 25em;
            text-align: right;
            padding-right: 2em;
        }
        
        #tbAddNewUser td.field 
        {
            width: 30em;
            text-align: left;
        }
    </style>

    <style type="text/css">
        #divWrapper
        {
            display:inline-block;
            padding:0px;
            margin:0px;
            border:0px;
            width:100%;
            text-align: center;
        }
        
        #divSaveResult
        {
            display: inline-block;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            width: 450px;
            height: auto;
            border: 1px solid #DDD;
            border-radius: 4px;
            margin-top: 50px;
            background-color: #F7F7F7;
            margin-bottom: 10px;
        }
        
        #divSaveResult h1 
        {
            padding: 0px;
            margin: 0px;
            color: #269CCB;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ClientIDMode="Static">
<% if (ErrorMessage.Length > 0)
   { %>
    <div id="divWrapper">
        <div id="divSaveResult">
            <h1><%=ErrorMessage%></h1>
        </div>
        <br />
        <a href="ReportManagement.aspx">ย้อนกลับ (Back)</a>
    </div>
    <% }
   else
   { %>

    <h1>Report Detail</h1>
    <br />
    <table id="tbAddNewUser" cellpadding="0" cellspacing="0">
       <tr>
            <td class="label">Name :</td>
            <td class="field"><label runat="server" id="lblReportName"></label></td>
        </tr>
       <tr>
            <td class="label">Description :</td>
            <td class="field"><label runat="server" id="lblReportDescription"></label></td>
        </tr>
        <tr>
            <td class="label">File Path :</td>
            <td class="field"><label runat="server" id="lblFilePath"></label></td>
        </tr>
    </table>

    <a href="ReportManagement.aspx">ย้อนกลับ (Back)</a>

    <% } %>
</asp:Content>
