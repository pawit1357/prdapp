﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="MyAccount.aspx.cs" Inherits="DMT.MyAccount" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tbAddNewUser 
        {
            table-layout: fixed;
            border: 0px;
        }
        
        #tbAddNewUser td
        {
            padding-top: 0.3em;
            padding-bottom: 0.1em;
        }
        
        #tbAddNewUser td.label 
        {
            width: 15em;
            text-align: right;
            padding-right: 2em;
        }
        
        #tbAddNewUser td.field 
        {
            width: 30em;
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
<%
    DMT.WS.Dto.UserLoginBean userLogin = DMT.WS.Biz.UserLoginUtil.getCurrentUserLogin();
%>
    <h1>แก้ไขข้อมูลส่วนตัว</h1>
    <%if(Session["resultMessage"] != null){ %>
        <div class="<%=Session["resultClass"] %>"><%=Session["resultMessage"]%></div>
    <%
        Session.Remove("resultMessage");
      } 
     %>
    <form action="MyAccount.aspx" method="post">
    <table id="tbAddNewUser" cellpadding="0" cellspacing="0">
        <tr>
            <td class="label">Username :</td>
            <td class="field"><%=userLogin.Username%></td>
        </tr>
        <tr><td colspan="2"><hr /></td></tr>
         <tr>
            <td class="label"></td>
            <td class="field"><u>change password</u></td>
        </tr>
        <tr>
            <td class="label">Password :</td>
            <td class="field"><input type="password" name="new_password" /></td>
        </tr>
        <tr>
            <td class="label">Confirm Password :</td>
            <td class="field"><input type="password" name="re_new_password" /></td>
        </tr>
        <tr><td colspan="2"><hr /></td></tr>       
        <tr>
            <td class="label">Full Name :</td>
            <td class="field"><input type="text" name="fullname" value="<%=userLogin.FullName%>" /></td>
        </tr>
        <tr>
            <td class="label">E-Mail :</td>
            <td class="field"><input type="text" name="email" value="<%=userLogin.Email%>" /></td>
        </tr>
        <tr><td colspan="2"><hr /></td></tr>       
        <tr>
            <td class="label">Enter your current password :</td>
            <td class="field"><input type="password" name="old_password" /></td>
        </tr>
        <tr>
            <td class="label">&nbsp;</td>
            <td class="field">
                <input type="submit" name="submit" value="Save" />
                <input type="reset" value="Cancel" />
            </td>
        </tr>
    </table>
    </form>    
</asp:Content>