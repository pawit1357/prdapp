﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMT.WS.Utils;
using DMT.WS.Dto;
using DMT.WS.Dao;
using DMT.WS.Biz;
using CrystalDecisions.CrystalReports.Engine;


namespace DMT
{
    public partial class Weighing_View : System.Web.UI.Page
    {
        protected string ErrorMessage
        {
            get;
            set;
        }

        protected WeighingDTO WeighingDTO
        {
            get;
            private set;
        }
        //protected StationDTO StationDTO
        //{
        //    get;
        //    private set;
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_STATION"))
                Response.Redirect("LiveWeighing.aspx");

            ErrorMessage = "";
            WeighingDTO = null;

            if (!IsPostBack)
            {
                int id = Gm.toSafeInt(Request.QueryString["WID"]);
                hWid.Value = id+"";
                if (id > 0)
                {
                    // Load User Info
                    WeighingDAO dao = new WeighingDAO();
                    WeighingDTO weighing = dao.getReportByConditionAsFirst(id);

                    if (weighing != null)
                    {

                        WeighingDTO = weighing;
                        Session["WeighingDTO"] = weighing;
                        //StationDTO = new StationDAO().getByPrimaryKey(weighing.StationID) as StationDTO;
                    }
                    else
                    {
                        Session["WeighingDTO"] = null;
                        ErrorMessage = "ไม่พบรายการที่ระบุ";
                    }
                }
                else
                {
                    Session["WeighingDTO"] = null;
                    ErrorMessage = "Error";
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            if (Session["WeighingDTO"] != null)
            {
                WeighingDTO bean = (WeighingDTO)Session["WeighingDTO"];
                if (bean != null)
                {
                    WeighingDTO = bean;
                    ReportDocument rptH = new ReportDocument();
                    rptH.FileName = Server.MapPath("~/Rpt/ReportEachCar.rpt");
                    List<WeighingDTO> tmps = new List<WeighingDTO>();
                    tmps.Add(bean);

                    rptH.SetDataSource(tmps);
                    rptH.SetParameterValue("P_STATION", bean.StationName);


                    try
                    {
                        string appPath = HttpContext.Current.Request.ApplicationPath;
                        string physicalPath = HttpContext.Current.Request.MapPath(appPath);
                        string filename = "tmpreport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";
                        rptH.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, physicalPath + "\\Rpt_tmp\\" + filename);

                        string url = Configurations.ReportPath + "/" + filename;
                        string s = "window.open('" + url + "', 'popup_window', 'width=800,height=600,left=100,top=100,resizable=yes');";
                        ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                        //rptH.PrintToPrinter(1, false, 0, 0);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                    //rptH.PrintToPrinter(1, false, 0, 0);

                }
                else
                {
                    ErrorMessage = "ไม่พบรายการที่ระบุ";
                }
            }
        }
    }
}