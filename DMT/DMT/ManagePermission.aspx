﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="ManagePermission.aspx.cs" Inherits="DMT.ManagePermission" %>
<%@ Import Namespace="DMT.WS.Utils" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        a:hover 
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <h1>จัดการสิทธิ์การเข้าถึง</h1>
    <br />
    <div>
           <table id="tbReport" cellpadding="0" cellspacing="0" width="50%">
            <tr>
                <th width="50%">User Level</th>
                <th>Permission</th>
            </tr>
            <tr class="row1"><td>Admin</td><td><a href="Permission_Edit.aspx?UserLevel=<%=ConfigurationUtil.USER_LEVEL_ADMIN %>">Manage</a></td></tr>
            <tr class="row2"><td>Manager</td><td><a href="Permission_Edit.aspx?UserLevel=<%=ConfigurationUtil.USER_LEVEL_MANAGER %>">Manage</a></td></tr>
            <tr class="row1"><td>Staff</td><td><a href="Permission_Edit.aspx?UserLevel=<%=ConfigurationUtil.USER_LEVEL_STAFF %>">Manage</a></td></tr>
            </table>
    </div>
</asp:Content>