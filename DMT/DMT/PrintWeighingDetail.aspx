﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintWeighingDetail.aspx.cs" Inherits="DMT.PrintWeighingDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="Stylesheet" href="Styles/Print.css" />
     <style type="text/css">
        #tbAddNewUser 
        {
            table-layout: fixed;
            border: 0px;
             background-color:#EEEEEE
        }
        
        #tbAddNewUser td
        {
            padding-top: 0.3em;
            padding-bottom: 0.1em;
        }
        
        #tbAddNewUser td.label 
        {
            width: 25em;
            text-align: right;
            padding-right: 2em;
        }
        
        #tbAddNewUser td.field 
        {
            width: 30em;
            text-align: left;
        }
    </style>

    <style type="text/css">
        #divWrapper
        {
            display:inline-block;
            padding:0px;
            margin:0px;
            border:0px;
            width:100%;
            text-align: center;
        }
        
        #divSaveResult
        {
            display: inline-block;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            width: 450px;
            height: auto;
            border: 1px solid #DDD;
            border-radius: 4px;
            margin-top: 50px;
            background-color: #F7F7F7;
            margin-bottom: 10px;
        }
        
        #divSaveResult h1 
        {
            padding: 0px;
            margin: 0px;
            color: #269CCB;
        }
        
        .tbl1{}
        .tbl1 th {text-align:left; font-weight:normal; font-size:12px}
        .tbl1 td {text-align:left; font-weight:normal; font-size:12px}
        .tbl1 td.number {text-align:left; font-weight:bold; font-size:18px}
    </style>
</head>
<body>

<%if(WeighingDTO != null) { %>
<h1>Weighing Detail<img alt="" src="<%=WeighingDTO.logo%>" /></h1>
    <br />
    <table id="tbAddNewUser" width="100%" cellpadding="0" cellspacing="0">
        <tr>
 
            <td valign="top">
            
                <table width="100%" class="tbl1">
                    <tr>
                        <th>Time</th>
                        <th>Station</th>
                        <th>Lane ID</th>
                        <th>Axles</th>
                        <th>W. GVW</th>
                        <th>W. Max GVW</th>
                        <th>W. Max AXW</th>
                        <th>OverWeight</th>
                        <th>Status</th>
                    </tr>
                    <tr>
                        <td><%=WeighingDTO.Timestamp%></td>
                        <td><%=StationDTO.StationNameTh%></td>
                        <td class="number"><%=WeighingDTO.LaneID%></td>
                        <td class="number"><%=WeighingDTO.NumberAxles%></td>
                        <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightGVW.ToString())%></td>
                        <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightMaxGVW.ToString())%></td>
                        <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightMaxAXW.ToString())%></td>
                        <td>
                        
                        
                        <img alt="" src="<%=WeighingDTO.sign%>" />
                        </td>
                        <td><%=WeighingDTO.RecordStatus.Length >= 2 ? WeighingDTO.RecordStatus.Substring(0, 2).Equals("OK") ? "NORMAL" : WeighingDTO.RecordStatus : ""%></td>
                    </tr>
                    <tr><td colspan="9">
                    <br />
                    <table width="100%">
                    <tr>
                        <th>Axle 1</th>
                        <th>Axle 2</th>
                        <th>Axle 3</th>
                        <th>Axle 4</th>
                        <th>Axle 5</th>
                        <th>Axle 6</th>
                        <th>Axle 7</th>
                        <th>Axle 8</th>
                        <th>Axle 9</th>
                        <th>Axle 10</th>
                    </tr>
                    <tr>
                        <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle01.ToString())%></td>
                <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle02.ToString())%></td>
                <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle03.ToString())%></td>
                <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle04.ToString())%></td>
                <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle05.ToString())%></td>
                <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle06.ToString())%></td>
                <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle07.ToString())%></td>
                <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle08.ToString())%></td>
                <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle09.ToString())%></td>
                <td class="number"><%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle10.ToString())%></td>
                    </tr>
                    </table>
                    </td></tr>
                    
                </table>
            </td>
        </tr>

        <tr>
            <td align="center">

            <div>
 <img id="img1" src="<%= WeighingDTO.Image01 %>" width="300"/>
<img id="img2" src="<%= WeighingDTO.Image02 %>" width="300"/>


<br />
    </div>
            </td>
        </tr>
    </table>
    <%} else { %>
    -- Not Found --
    <%} %>

    </body>
</html>