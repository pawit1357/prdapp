﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using DMT.WS.Dto;
using DMT.WS.Dao;
using DMT.WS.Utils;
using DMT.WS.Biz;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


namespace DMT
{
    public partial class AllWeighing : System.Web.UI.Page
    {
        protected List<WeighingDTO> weighings = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_ALL_WEIGHING"))
                Response.Redirect("LiveWeighing.aspx");
             // first load
            if (!IsPostBack)
            {
                prepareDropdownStation();
                prepareDropdownLane();
                prepareDropdownTimeSearch();
                //doSearch(1);
                
            }

            int pageNo = Gm.toSafeInt(this.txtPageNo.Value);
            if (pageNo == 0) pageNo = 1;

            string btnSubmit = Request.Params["btnSearch"];
            if (btnSubmit == "Search")
            {
                string pstation = Request.Params["ctl00$ContentPlaceHolder1$station"];
                string psLane = Request.Params["ctl00$ContentPlaceHolder1$lane"];
                string poverweight = Request.Params["ctl00$ContentPlaceHolder1$overweight"];
                string pFromDate = Request.Params["ctl00$ContentPlaceHolder1$from_date"];
                string pToDate = Request.Params["ctl00$ContentPlaceHolder1$to_date"];
                string phrFrom = Request.Params["ctl00$ContentPlaceHolder1$cboHrFrom"];
                string pminFrom = Request.Params["ctl00$ContentPlaceHolder1$cboMinFrom"];
                string phrTo = Request.Params["ctl00$ContentPlaceHolder1$cboHrTo"];
                string pminTo = Request.Params["ctl00$ContentPlaceHolder1$cboMinTo"];

                h_pstation.Value = pstation;
                h_pslane.Value = psLane;
                h_poverweight.Value = poverweight;
                h_pFromDate.Value = pFromDate;
                h_pToDate.Value = pToDate;
                h_phrFrom.Value = phrFrom;
                h_pminFrom.Value = pminFrom;
                h_pminTo.Value = pminTo;
                h_phrTo.Value = phrTo;


                StationDTO stationDto = null;
                if (pstation != null && pstation != "")
                {
                    stationDto = (StationDTO)new StationDAO().getByPrimaryKey(Gm.toSafeInt(pstation));
                }
                if (stationDto != null)
                {
                    station.SelectedIndex = station.Items.IndexOf(new ListItem(stationDto.StationNameEn, stationDto.ID.ToString()));
                }

                LaneDTO laneDto = null;
                if (psLane != null && psLane != "")
                {
                    laneDto = (LaneDTO)new LaneDAO().getByPrimaryKey(Gm.toSafeInt(psLane));
                }
                if (laneDto != null)
                {
                    lane.SelectedIndex = lane.Items.IndexOf(new ListItem(laneDto.LaneName, stationDto.ID.ToString()));
                }


                if (poverweight != null)
                {
                    if (poverweight == "0") overweight.SelectedIndex = 1;
                    else if (poverweight == "1") overweight.SelectedIndex = 2;
                }

                from_date.Value = pFromDate;
                to_date.Value = pToDate;
                cboHrFrom.SelectedIndex = cboHrFrom.Items.IndexOf(new ListItem(phrFrom, phrFrom));
                cboMinFrom.SelectedIndex = cboMinFrom.Items.IndexOf(new ListItem(pminFrom, pminFrom));
                cboHrTo.SelectedIndex = cboHrTo.Items.IndexOf(new ListItem(phrTo, phrTo));
                cboMinTo.SelectedIndex = cboMinTo.Items.IndexOf(new ListItem(pminTo, pminTo));
            }

            //doSearch(pageNo);

           
        }

        private void prepareDropdownStation()
        {
            if (UserLoginUtil.getCurrentUserLogin().Level == ConfigurationUtil.USER_LEVEL_ADMIN)
            {
                string requestStation = Request.Params["station"];
                station.Items.Add(new ListItem("All Station", ""));
                StationDAO dao = new StationDAO();
                List<StationDTO> dtos = dao.getAll();
                foreach (StationDTO dto in dtos)
                {
                    ListItem item = new ListItem(dto.StationNameEn, dto.ID.ToString());
                    station.Items.Add(item);
                    if (requestStation != null && requestStation == dto.ID.ToString())
                    {

                        station.SelectedIndex = station.Items.IndexOf(item);
                    }
                }
            }
            else
            {
                UserStationDTO userStation = new UserStationDAO().getActiveByUserLoginId(UserLoginUtil.getCurrentUserLogin().ID);
                //string requestStation = Request.Params["station"];
                //station.Items.Add(new ListItem("All Station", ""));
                if (userStation != null)
                {                    
                    StationDAO dao = new StationDAO();
                    StationDTO dto = dao.getByPrimaryKey(userStation.StationId) as StationDTO;
                    if (dto != null)
                    {
                        ListItem item = new ListItem(dto.StationNameEn, dto.ID.ToString());
                        station.Items.Add(item);
                    }
                }
            }
        }
        private void prepareDropdownLane()
        {
            //if (UserLoginUtil.getCurrentUserLogin().Level == ConfigurationUtil.USER_LEVEL_ADMIN)
            //{
                string requestLane = Request.Params["lane"];
                lane.Items.Add(new ListItem("All Lane", ""));
                LaneDAO dao = new LaneDAO();
                List<LaneDTO> dtos = dao.getAll();
                foreach (LaneDTO dto in dtos)
                {
                    ListItem item = new ListItem(dto.LaneName, dto.ID.ToString());
                    lane.Items.Add(item);
                    if (requestLane != null && requestLane == dto.ID.ToString())
                    {

                        lane.SelectedIndex = lane.Items.IndexOf(item);
                    }
                }
            //}
            //else
            //{
            //    UserStationDTO userStation = new UserStationDAO().getActiveByUserLoginId(UserLoginUtil.getCurrentUserLogin().ID);
            //    //string requestStation = Request.Params["station"];
            //    //station.Items.Add(new ListItem("All Station", ""));
            //    if (userStation != null)
            //    {
            //        LaneDAO dao = new LaneDAO();
            //        LaneDTO dto = dao.getByPrimaryKey(userStation.StationId) as LaneDTO;
            //        if (dto != null)
            //        {
            //            ListItem item = new ListItem(dto.LaneName, dto.ID.ToString());
            //            lane.Items.Add(item);
            //        }
            //    }
            //}
        }
        private void prepareDropdownTimeSearch()
        {
            string hrFrom = Request.Params["hr_from"];
            string minFrom = Request.Params["min_from"];
            string hrTo = Request.Params["hr_to"];
            string minTo = Request.Params["min_to"];
            for (int i = 0; i < 24; i++)
            {
                string value = i.ToString();
                if (i < 10)
                {
                    value = "0" + i;
                }
                ListItem item1 = new ListItem(value, value);
                ListItem item2 = new ListItem(value, value);

                if (hrFrom == value)
                {
                    item1.Selected = true;
                }
                if (hrTo == value)
                {
                    item2.Selected = true;
                }

                cboHrFrom.Items.Add(item1);
                cboHrTo.Items.Add(item2);
            }

            for (int i = 0; i <= 59; i++)
            {
                string value = i.ToString();
                if (i < 10)
                {
                    value = "0" + i;
                }
                ListItem item1 = new ListItem(value, value);
                ListItem item2 = new ListItem(value, value);

                if (minFrom == value)
                {
                    item1.Selected = true;
                }
                if (minTo == value)
                {
                    item2.Selected = true;
                }

                cboMinFrom.Items.Add(item1);
                cboMinTo.Items.Add(item2);
            }
        }

        protected int PageCount
        {
            get;
            set;
        }

        protected string PagingHtml
        {
            get
            {
                string stationId = station.Value;
                string fromDate = from_date.Value;
                string toDate = to_date.Value;
                string overWeigth = overweight.Value;
                int pageNo = Gm.toSafeInt(this.txtPageNo.Value);

                if (fromDate.Length == 10)
                    fromDate = Gm.toThaiDateToSqlDate(fromDate);

                if (toDate.Length == 10)
                    toDate = Gm.toThaiDateToSqlDate(toDate);

                int allPageCount = new WeighingDAO().getTotalPage(stationId, fromDate, toDate, overWeigth);
                int currentPage = pageNo;
                if (currentPage < 1) currentPage = 1;
                string strPageLink = "";

                if (currentPage != 1)
                {
                    strPageLink += "<span class='paging' title='First Page' onclick='searchByPage(1)'>|<</span> ";
                    strPageLink += "<span class='paging' title='Previous Page' onclick='searchByPage(" + (currentPage - 1) + ")'><=</span> ";
                }
                else
                {
                    strPageLink += "|< ";
                    strPageLink += "<= ";
                }

                strPageLink += currentPage + " ";

                if (currentPage != allPageCount)
                {
                    strPageLink += "<span class='paging' title='Next Page' onclick='searchByPage(" + (currentPage + 1) + ")'>=></span> ";
                    strPageLink += "<span class='paging' title='Last Page' onclick='searchByPage(" + allPageCount + ")'>>|</span> ";
                }
                else
                {
                    strPageLink += "=> ";
                    strPageLink += ">| ";
                }

                /*
                if (currentPage <= 3)
                {
                    if (allPageCount <= 5)
                    {
                        for (int i = 1; i <= 5; i++)
                        {
                            if (i == currentPage)
                            {
                                strPageLink += i + " | ";
                            }
                            else
                            {
                                strPageLink += "<span class='paging' onclick='searchByPage(" + i + ")'>" + i + "</span> | ";
                            }
                        }
                        if (strPageLink.Length > 3)
                        {
                            strPageLink = strPageLink.Substring(0, strPageLink.Length - 3);
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= 5; i++)
                        {
                            if (i == currentPage)
                            {
                                strPageLink += i + " | ";
                            }
                            else
                            {
                                strPageLink += "<span class='paging' onclick='searchByPage(" + i + ")'>" + i + "</span> | ";
                            }
                        }
                        if (strPageLink.Length > 3)
                        {
                            strPageLink = strPageLink.Substring(0, strPageLink.Length - 3);
                        }
                        strPageLink += "...<span class='paging' onclick='searchByPage(" + allPageCount + ")'>" + allPageCount + "</span>";
                    }
                }
                else if (currentPage > allPageCount - 3)
                {
                    if (allPageCount <= 5)
                    {
                        for (int i = allPageCount - 4; i <= allPageCount; i++)
                        {
                            if (i == currentPage)
                            {
                                strPageLink += i + " | ";
                            }
                            else
                            {
                                strPageLink += "<span class='paging' onclick='searchByPage(" + i + ")'>" + i + "</span> | ";
                            }
                        }
                        if (strPageLink.Length > 3)
                        {
                            strPageLink = strPageLink.Substring(0, strPageLink.Length - 3);
                        }
                    }
                    else
                    {
                        strPageLink += "<span class='paging' onclick='searchByPage(" + 1 + ")'>" + 1 + "</span>...";
                        for (int i = allPageCount - 4; i <= allPageCount; i++)
                        {
                            if (i == currentPage)
                            {
                                strPageLink += i + " | ";
                            }
                            else
                            {
                                strPageLink += "<span class='paging' onclick='searchByPage(" + i + ")'>" + i + "</span> | ";
                            }
                        }
                        if (strPageLink.Length > 3)
                        {
                            strPageLink = strPageLink.Substring(0, strPageLink.Length - 3);
                        }
                    }
                }
                else
                {
                    if (allPageCount <= 5)
                    {
                        for (int i = 1; i <= 5; i++)
                        {
                            if (i == currentPage)
                            {
                                strPageLink += i + " | ";
                            }
                            else
                            {
                                strPageLink += "<span class='paging' onclick='searchByPage(" + i + ")'>" + i + "</span> | ";
                            }
                        }
                        if (strPageLink.Length > 3)
                        {
                            strPageLink = strPageLink.Substring(0, strPageLink.Length - 3);
                        }
                    }
                    else
                    {
                        strPageLink += "<span class='paging' onclick='searchByPage(" + 1 + ")'>" + 1 + "</span>...";
                        for (int i = currentPage - 2; i <= currentPage + 2; i++)
                        {
                            if (i == currentPage)
                            {
                                strPageLink += i + " | ";
                            }
                            else
                            {
                                strPageLink += "<span class='paging' onclick='searchByPage(" + i + ")'>" + i + "</span> | ";
                            }
                        }
                        if (strPageLink.Length > 3)
                        {
                            strPageLink = strPageLink.Substring(0, strPageLink.Length - 3);
                        }
                        strPageLink += "...<span class='paging' onclick='searchByPage(" + allPageCount + ")'>" + allPageCount + "</span>";
                    }
                }
                






                if (currentPage != allPageCount)
                {
                    strPageLink += " <span class='paging' onclick='searchByPage(" + (currentPage + 1) + ")'>>></span>";
                }
                 * 
                 */
                 
                return strPageLink;
                //return "<span class='paging' onclick='searchByPage(1)'>&lt;&lt;</span> <span class='paging' onclick='searchByPage(1)'>1</span> | <span class='paging' onclick='searchByPage(2)'>2</span> <span class='paging' onclick='searchByPage(2)'>&gt;&gt;</span>";
            }
        }

        protected void btSearch_Click(object sender, EventArgs e)
        {
            string stationId = station.Value;
            string fromDate = from_date.Value;
            string toDate = to_date.Value;
            string overWeigth = overweight.Value;
            string hrFrom = cboHrFrom.Value;
            string hrTo = cboHrTo.Value;
            string minFrom = cboMinFrom.Value;
            string minTo = cboMinTo.Value;
            h_pstation.Value = stationId;
            h_poverweight.Value = overWeigth;
            h_pFromDate.Value = fromDate;
            h_pToDate.Value = toDate;
            h_phrFrom.Value = hrFrom;
            h_pminFrom.Value = minFrom;
            h_pminTo.Value = minTo;
            h_phrTo.Value = hrTo;
            doSearch(0);
        }

        protected void btSearchByPage_Click(object sender, EventArgs e)
        {
            int pageNo = Gm.toSafeInt(this.txtPageNo.Value);
            doSearch(pageNo);
        }

        protected void doSearch(int pageNo)
        {
            string stationId = station.Value;
            string laneId = lane.Value;
            string fromDate = from_date.Value;
            string toDate = to_date.Value;
            string overWeigth = overweight.Value;
            string hrFrom = cboHrFrom.Value;
            string hrTo = cboHrTo.Value;
            string minFrom = cboMinFrom.Value;
            string minTo = cboMinTo.Value;


            if (fromDate.Length == 10)
                fromDate = Gm.toThaiDateToSqlDate(fromDate) + " " + Gm.toThaiDateToSqlTime(hrFrom, minFrom);

            if (toDate.Length == 10)
                toDate = Gm.toThaiDateToSqlDate(toDate) + " " + Gm.toThaiDateToSqlTime(hrTo, minTo);

            txtSearchCondDebug.Value = "StationId=" + stationId
                                + ", overWeight=" + overWeigth
                                + ", fromDate=" + fromDate + ", toDate=" + toDate 
                                + ", page=" + pageNo.ToString();

            if (UserLoginUtil.getCurrentUserLogin().Level != ConfigurationUtil.USER_LEVEL_ADMIN)
            {
                UserStationDTO userStation = new UserStationDAO().getActiveByUserLoginId(UserLoginUtil.getCurrentUserLogin().ID);

                stationId = "-1";
                if (userStation != null)
                {
                    stationId = "" + userStation.StationId;
                }
            }
            weighings = new WeighingDAO().getAll(stationId,laneId, fromDate, toDate, overWeigth, pageNo);
            
        }

        protected void btPrint_Click(object sender, EventArgs e)
        {
            if (weighings.Count > 0)
            {


                string fromDate = from_date.Value;
                string toDate = to_date.Value;
                string hrFrom = cboHrFrom.Value;
                string hrTo = cboHrTo.Value;
                string minFrom = cboMinFrom.Value;
                string minTo = cboMinTo.Value;


                if (fromDate.Length == 10)
                    fromDate = Gm.toThaiDateToSqlDate(fromDate) + " " + Gm.toThaiDateToSqlTime(hrFrom, minFrom);

                if (toDate.Length == 10)
                    toDate = Gm.toThaiDateToSqlDate(toDate) + " " + Gm.toThaiDateToSqlTime(hrTo, minTo);


                ReportDocument rptH = new ReportDocument();
                rptH.FileName = Server.MapPath("~/Rpt/Rpt04.rpt");


                rptH.SetDataSource(weighings);
                if (!fromDate.Equals(""))
                {
                    rptH.SetParameterValue("START_DATE", fromDate);
                    rptH.SetParameterValue("END_DATE", toDate);
                }
                else {
                    rptH.SetParameterValue("START_DATE", "ทั้งหมด");
                    rptH.SetParameterValue("END_DATE", "");
                }
                PageMargins margins;

                margins = rptH.PrintOptions.PageMargins;
                margins.bottomMargin = 350;
                margins.leftMargin = 350;
                margins.rightMargin = 350;
                margins.topMargin = 350;
                rptH.PrintOptions.ApplyPageMargins(margins);

                rptH.PrintOptions.PrinterName = "print1";
                
                rptH.PrintToPrinter(1, false, 0, 0);
                Console.WriteLine("::" + weighings.Count);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            if (weighings.Count > 0)
            {
                ReportDocument rptH = new ReportDocument();


                //beans[0].img1 = CommonUtils.getByteImageBySource(String.Format(Configurations.ImageFilePath, beans[0].Image01));
                //beans[0].img2 = CommonUtils.getByteImageBySource(String.Format(Configurations.ImageFilePath, beans[0].Image02));

                //rptH.FileName = Server.MapPath("~/Rpt/Rpt02_" + graphType + "_" + period + ".rpt");
                Console.WriteLine("");
            }
        }
    }
}

