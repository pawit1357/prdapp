﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Permission_Edit.aspx.cs" Inherits="DMT.Permission_Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tbAddNewUser 
        {
            table-layout: fixed;
            border: 0px;
        }
        
        #tbAddNewUser td
        {
            padding-top: 0.3em;
            padding-bottom: 0.1em;
        }
        
        #tbAddNewUser td.label 
        {
            width: 25em;
            text-align: right;
            padding-right: 2em;
        }
        
        #tbAddNewUser td.field 
        {
            width: 30em;
            text-align: left;
        }
    </style>

    <style type="text/css">
        #divWrapper
        {
            display:inline-block;
            padding:0px;
            margin:0px;
            border:0px;
            width:100%;
            text-align: center;
        }
        
        #divSaveResult
        {
            display: inline-block;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            width: 450px;
            height: auto;
            border: 1px solid #DDD;
            border-radius: 4px;
            margin-top: 50px;
            background-color: #F7F7F7;
            margin-bottom: 10px;
        }
        
        #divSaveResult h1 
        {
            padding: 0px;
            margin: 0px;
            color: #269CCB;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ClientIDMode="Static">
<% if (SaveResultMessage.Length > 0)
   { %>
    <div id="divWrapper">
        <div id="divSaveResult">
            <h1><%=SaveResultMessage%></h1>
        </div>
        <br />
        <a href="ManagePermission.aspx">ย้อนกลับ (Back)</a>
    </div>
    <% }
   else
   { %>

    <form id="Form1" action="Permission_Edit.aspx" method="post" runat="server" onsubmit="return ValidateInputForm()">
    <h1>แก้ไขสิทธิ์การเข้าถึง</h1>
    <br />
   <% if (SaveResultMessage.Length > 0)
       { %>
    <div id="div1">
        <div id="div2">
            <h1><%=SaveResultMessage%></h1>
        </div>
    </div>
    <% } %>
    <table id="tbAddNewUser" cellpadding="0" cellspacing="0">
        <tr>
            <td class="label">User Level :</td>
            <td class="field"><asp:label ID="lblUserLevel" runat="server"></asp:label></td>
        </tr>
        <tr>
            <td class="label" valign="top">Permission :</td>
            <td class="field">
            <asp:CheckBoxList ID="chkPermissions" runat="server">
            </asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td class="label">&nbsp;</td>
            <td class="field">
                <input id="btSave" type="submit" runat="server" value="Save" onserverclick="btSave_OnClick" />
                <input id="btClear" type="reset" value="Cancel" />
            </td>
        </tr>
    </table>
    </form>
    <a href="ManagePermission.aspx">ย้อนกลับ (Back)</a>

    <% } %>

    <script type="text/javascript">
        $(function () {
            $('#btClear').click(function () {
                window.location = 'ManagePermission.aspx';
            });
        });

    </script>
</asp:Content>
