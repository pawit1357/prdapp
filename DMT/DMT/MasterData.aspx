﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="MasterData.aspx.cs" Inherits="DMT.MasterData" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <h1>Menu</h1>
    <br />
    <div>
        <div class="icon ico-user-management"><a href="UserManagement.aspx">Users</a></div>
        <div class="icon ico-permission"><a href="ManagePermission.aspx">Permission</a></div>
        <div class="icon ico-station"><a href="StationManagement.aspx">Station</a></div>
        <%--<div class="icon ico-report"><a href="ReportManagement.aspx">Report</a></div>--%>
    </div>
</asp:Content>