﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true"
    CodeBehind="Weighing_View.aspx.cs" Inherits="DMT.Weighing_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Styles/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
    <script src="scripts/jquery.elevatezoom.min.js" type="text/javascript"></script>
    <script src="scripts/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
    <style type="text/css">
        #tbAddNewUser
        {
            table-layout: fixed;
            border: 0px;
            background-color: #EEEEEE;
        }
        
        #tbAddNewUser td
        {
            padding-top: 0.3em;
            padding-bottom: 0.1em;
        }
        
        #tbAddNewUser td.label
        {
            width: 25em;
            text-align: right;
            padding-right: 2em;
        }
        
        #tbAddNewUser td.field
        {
            width: 30em;
            text-align: left;
        }
    </style>
    <style type="text/css">
        #divWrapper
        {
            display: inline-block;
            padding: 0px;
            margin: 0px;
            border: 0px;
            width: 100%;
            text-align: center;
        }
        
        #divSaveResult
        {
            display: inline-block;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            width: 450px;
            height: auto;
            border: 1px solid #DDD;
            border-radius: 4px;
            margin-top: 50px;
            background-color: #F7F7F7;
            margin-bottom: 10px;
        }
        
        #divSaveResult h1
        {
            padding: 0px;
            margin: 0px;
            color: #269CCB;
        }
        
        .tbl1
        {
        }
        .tbl1 th
        {
            text-align: left;
            font-weight: normal;
            font-size: 12px;
        }
        .tbl1 td
        {
            text-align: left;
            font-weight: normal;
            font-size: 12px;
        }
        .tbl1 td.number
        {
            text-align: left;
            font-weight: bold;
            font-size: 18px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function printIt(id) {

            $.ajax({
                url: "PrintWeighingDetail.aspx?WID=" + id + "&tmp=" + new Date().getTime(),
                type: "POST",
                data: '',
                dataType: "html",
                success: function (data) {
 
                    if (data != null) {
                        var printContents = data;
                        var html = printContents;

                        var printWin = window.open('', '', 'left=0,top=0,width=900,height=600,toolbar=0,scrollbars=0,status  =0');
                        printWin.document.write(html);
                        printWin.document.close();
                        printWin.focus();
                        printWin.print();
                        printWin.close();
                    }
                }
            });


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"
    ClientIDMode="Static">
    <form id="form1" runat="server">
    <% if (ErrorMessage.Length > 0)
       { %>
    <div id="divWrapper">
        <div id="divSaveResult">
            <h1>
                <%=ErrorMessage%></h1>
        </div>
        <br />
        <a href="StationManagement.aspx">ย้อนกลับ (Back)</a>
    </div>
    <% }
       else
       { %>
    <h1>
        Weighing Detail</h1>
    <br />
        <asp:HiddenField ID="hWid" Value="" runat="server"/>
    <div class="box1">
        <%--<input type="button" value="   Print   " onclick="printIt('<%=WeighingDTO.ID %>')" />--%>
        <asp:Button ID="btnPrint" runat="server" Text="Print" OnClick="btnPrint_Click" />
    </div>
    <table id="tbAddNewUser" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                <table width="100%" class="tbl1">
                    <tr>
                        <th>
                            Time
                        </th>
                        <th>
                            Station
                        </th>
                        <th>
                            Lane ID
                        </th>
                        <th>
                            Axles
                        </th>
                        <th>
                            W. GVW
                        </th>
                        <th>
                            W. Max GVW
                        </th>
                        <th>
                            W. Max AXW
                        </th>
                        <th>
                            OverWeight
                        </th>
                        <th>
                            Status
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <%=WeighingDTO.Timestamp%>
                        </td>
                        <td>
                            <%=WeighingDTO.StationName%>
                        </td>
                        <td class="number">
                            <%=WeighingDTO.LaneID%>
                        </td>
                        <td class="number">
                            <%=WeighingDTO.NumberAxles%>
                        </td>
                        <td class="number">
                            <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightGVW.ToString())%>
                        </td>
                        <td class="number">
                            <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightMaxGVW.ToString())%>
                        </td>
                        <td class="number">
                            <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightMaxAXW.ToString())%>
                        </td>
                        <td>
                            <img alt="" src="<%=WeighingDTO.IsOverWeight == false ? "images/ico_circle_green.png" : "images/ico_circle_red.png"%>" />
                        </td>
                        <td>
                            <%=WeighingDTO.RecordStatus.Length >= 2 ? WeighingDTO.RecordStatus.Substring(0, 2).Equals("OK") ? "NORMAL" : WeighingDTO.RecordStatus : ""%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <br />
                            <table width="100%">
                                <tr>
                                    <th>
                                        Axle 1
                                    </th>
                                    <th>
                                        Axle 2
                                    </th>
                                    <th>
                                        Axle 3
                                    </th>
                                    <th>
                                        Axle 4
                                    </th>
                                    <th>
                                        Axle 5
                                    </th>
                                    <th>
                                        Axle 6
                                    </th>
                                    <th>
                                        Axle 7
                                    </th>
                                    <th>
                                        Axle 8
                                    </th>
                                    <th>
                                        Axle 9
                                    </th>
                                    <th>
                                        Axle 10
                                    </th>
                                </tr>
                                <tr>
                                    <td class="number">
                                        <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle01.ToString())%>
                                    </td>
                                    <td class="number">
                                        <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle02.ToString())%>
                                    </td>
                                    <td class="number">
                                        <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle03.ToString())%>
                                    </td>
                                    <td class="number">
                                        <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle04.ToString())%>
                                    </td>
                                    <td class="number">
                                        <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle05.ToString())%>
                                    </td>
                                    <td class="number">
                                        <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle06.ToString())%>
                                    </td>
                                    <td class="number">
                                        <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle07.ToString())%>
                                    </td>
                                    <td class="number">
                                        <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle08.ToString())%>
                                    </td>
                                    <td class="number">
                                        <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle09.ToString())%>
                                    </td>
                                    <td class="number">
                                        <%=DMT.WS.Utils.CommonUtils.customNumberFormat(WeighingDTO.WeightAxle10.ToString())%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <div>
                    <img id="img1" src="<%= WeighingDTO.Image01 %>"
                        width="500" data-zoom-image="<%= WeighingDTO.Image01 %>" />
                    <div id="gallery_01">
                        <a href="#" title="Front" class="active" data-image="<%= WeighingDTO.Image01 %>"
                            data-zoom-image="<%= WeighingDTO.Image01 %>">
                            <br />
                            <img src="<%= WeighingDTO.Image01 %>" width="100" /></a>
                        <a href="#" title="Back" data-image="<%= WeighingDTO.Image02 %>"
                            data-zoom-image="<%= WeighingDTO.Image02 %>">
                            <img src="<%= WeighingDTO.Image02 %>" width="100" /></a>
                        <br />
                    </div>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#img1").elevateZoom({ gallery: 'gallery_01', cursor: 'pointer', galleryActiveClass: "active" });
            $("#img1").bind("click", function (e) {
                var ez = $('#img1').data('elevateZoom');
                ez.closeAll();
                $.fancybox(ez.getGalleryList());
                return false;
            });
        });
    </script>
    <a href="LiveWeighing.aspx">ย้อนกลับ (Back)</a>
    <% } %>
    </form>
</asp:Content>
