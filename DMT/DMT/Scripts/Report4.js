﻿function loadReport() {
    //$('#reportArea').html('Loading Report...');
    $('#container').html('');
    $('#container1').html('');

    var station = $('#cboStation :selected').val();
    var cboLane = $('#cboLane :selected').val();
    var cbLane = $('#cbLane').is(':checked');

    var startDateTime;
    var sDate = $('#txtStartDate').val();
    if (sDate != '') {
        var sDay = sDate.substring(0, 2);
        var sMonth = sDate.substring(3, 5);
        var sYear = sDate.substring(6, 10);

        var sHour = sDate.substring(11, 13);
        var sMin = sDate.substring(14, 16);

        startDateTime = sYear + '-' + sMonth + '-' + sDay + ' ' + sHour + ':' + sMin + ':00.000';
    }

    var endDateTime;
    var eDate = $('#txtEndDate').val();
    if (eDate != '') {
        var sDay = eDate.substring(0, 2);
        var sMonth = eDate.substring(3, 5);
        var sYear = eDate.substring(6, 10);

        var sHour = eDate.substring(11, 13);
        var sMin = eDate.substring(14, 16);

        endDateTime = sYear + '-' + sMonth + '-' + sDay + ' ' + sHour + ':' + sMin + ':00.000';
    }
    //alert(station + ',' + cboLane + ',' + startDateTime + ',' + endDateTime + ',' + cbLane);
    var graphType = $('#hdGraphType').val();
    //alert(graphType);
    $.ajax({
        url: "/DMT/Ajax.aspx?type=GetReport4_1&tmp=" + new Date().getTime(),
        type: "POST",
        data: { stationId: station, laneId: cboLane, startDate: startDateTime, endDate: endDateTime, splitLane: cbLane },
        dataType: "json",
        success: function (data) {
            if (data != null && data != 'null') {

                if (graphType == 2) {
                    loadBarChart(data);
                    loadBarChartPercent(data);
                } else {
                    loadLineChart(data);
                }
            }
        }
    });
}

//function renderReport(data) {

//    if (data != null) {
//        var tbl = $('<table id="tbReport" width="100%"></table>');
//        tbl.append($('<tr><th></th><th colspan="10" align="center">จำนวนเพลา</th><th rowspan="2">รวม</th></tr>'));
//        tbl.append($('<tr class="tr_rowspan"><th>Period</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th></tr>'));
//        var rowId = 0;
//        for (var i = 0; i < data.length; i++) {
//            var item = data[i];
//            var rowClassName = '';
//            if (rowId == 0) {
//                rowClassName = 'class="row1"';
//                rowId = 1;
//            } else {
//                rowClassName = 'class="row2"';
//                rowId = 0;
//            }
//            tbl.append("<tr " + rowClassName + ">" +
//                "<td>" + item['Period'] + "</td>" +
//                "<td>" + item['1'] + "</td>" +
//                "<td>" + item['2'] + "</td>" +
//                "<td>" + item['3'] + "</td>" +
//                "<td>" + item['4'] + "</td>" +
//                "<td>" + item['5'] + "</td>" +
//                "<td>" + item['6'] + "</td>" +
//                "<td>" + item['7'] + "</td>" +
//                "<td>" + item['8'] + "</td>" +
//                "<td>" + item['9'] + "</td>" +
//                "<td>" + item['10'] + "</td>" +
//                "<td>" + item['Total'] + "</td>" +
//            "</tr>");
//        }


//        $('#reportArea').html('');
//        $('#reportArea').append(tbl);
//    }
//}

function loadBarChart(data) {
    var mainData = data;
    var cats = new Array();
    var series = new Array();
    var t = new Array();
    var f = new Array();
    for (var i = 0; i < mainData.length; i++) {
        cats[cats.length] = mainData[i]['StationNameTh'];
        t[t.length] = parseInt(mainData[i]['True']);
        f[f.length] = parseInt(mainData[i]['False']);
    }
    
    $('#container').highcharts({

        chart: {
            type: 'column'
        },

        title: {
            text: 'Don Muang Tollway Public Company Limited'
        },
        subtitle: {
            text: 'รายงานสรุปภาพรวมปริมาณรถเข้าชั่ง'
        },

        xAxis: {
            categories: cats


        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'ปริมาณรถ (คัน)'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },

        series: [{
            name: 'นน.อาจเกิน',
            data: t
        }, {
            name: 'นน.ปกติ',
            data: f

        }]
    });
}
function loadBarChartPercent(data) {
    var mainData = data;
    var cats = new Array();
    var series = new Array();
    var t = new Array();
    var f = new Array();
    for (var i = 0; i < mainData.length; i++) {
        cats[cats.length] = mainData[i]['StationNameTh'];
        t[t.length] = parseInt(mainData[i]['TruePercent']);
        f[f.length] = parseInt(mainData[i]['FalsePercent']);
    }

    $('#container1').highcharts({

        chart: {
            type: 'column'
        },

        title: {
            text: 'Don Muang Tollway Public Company Limited'
        },
        subtitle: {
            text: 'รายงานสรุปภาพรวมปริมาณรถเข้าชั่ง'
        },

        xAxis: {
            categories: cats


        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'ปริมาณรถ (%)'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },

        series: [{
            name: 'นน.อาจเกิน',
            data: t
        }, {
            name: 'นน.ปกติ',
            data: f

        }]
    });
}

function loadLineChart(data) {
    var mainData = data;
    var NumberAxles = new Array();
    var series = new Array();
    var station1 = new Array();
    var station2 = new Array();
    var station4 = new Array();
    var station6 = new Array();
    for (var i = 0; i < mainData.length; i++) {
        NumberAxles[NumberAxles.length] = mainData[i]['NumberAxles'];
        station1[station1.length] = parseInt(mainData[i]['1']);
        station2[station2.length] = parseInt(mainData[i]['2']);
        station4[station4.length] = parseInt(mainData[i]['4']);
        station6[station6.length] = parseInt(mainData[i]['6']);
    }

    $('#container').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Don Muang Tollway Public Company Limited'
        },
        subtitle: {
            text: 'รายงานสรุปภาพรวมปริมาณรถเข้าชั่ง'
        },
        xAxis: {
            categories: NumberAxles
        },
        yAxis: {
            title: {
                text: 'ปริมาณรถ (คัน)'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
        },

        series: [{
            name: 'ดินแดง',
            data: station1
        }, {
            name: 'ดอนเมือง',
            data: station2

        }, {
            name: 'รัชดาภิเษก',
            data: station4

        }, {
            name: 'แจ้งวัฒนะ',
            data: station6

        }]
    });
}


