﻿// Config
var refreshTime = 200000;
var tableId = 'tbReport';

// System Param
var maxId = 0;
var rowId = 0;

function prepareSystem() {
    checkData();

}

function popupCenter(url, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

function checkData() {


    // alert(new Date().getTime());
    $.ajax({
        url: "Ajax.aspx?type=GetNewContent&lane=" + $("#filter_lane").val() + "&station=" + $("#filter_station").val() + "&maxid=" + maxId + "&tmp=" + new Date().getTime(),
        type: "POST",
        data: '',
        dataType: "json",
        success: function (data) {
            //alert(data.size);
            if (data != null) {

                if (data.length + getRowCount() > 3) {

                    removeRow((data.length + getRowCount()) - 6);
                }
                var tContent;
                for (var i = 0; i < data.length ; i++) {

                    var item = data[i];

                    //if (maxId < parseInt(item["ID"])) {
                    //    maxId = parseInt(item["ID"]);
                    //}

                    var overWeight = "";

                    if (item["IsOverWeight"] == "False") {
                        overWeight = "<img src=\"images/ico_circle_green.png\" />";
                    } else {
                        overWeight = "<img src=\"images/ico_circle_red.png\" />";
                    }

                    var recordStatus = "";

                    if (item["RecordStatus"].substring(0, 2) == "OK") {
                        recordStatus = "NORMAL";

                    } else {
                        recordStatus = item["RecordStatus"]; // "<img src=\"images/ico_circle_orange.png\" alt=\"Error\" />";
                    }


                    var picClassName = new Date().getTime();
                    var rowClassName = '';
                    if (rowId == 0) {
                        rowClassName = 'class="row1"';
                        rowId = 1;
                    } else {
                        rowClassName = 'class="row2"';
                        rowId = 0;
                    }
                   // $("." + picClassName).colorbox({ rel: '' + picClassName, width: '900px' });
                    tContent = tContent + ("<tr " + rowClassName + ">" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['TimeStamp'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['StationID'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['LaneID'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['NumberAxles'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_Axle01'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_Axle02'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_Axle03'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_Axle04'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_Axle05'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_Axle06'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_Axle07'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_Axle08'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_Axle09'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_Axle10'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_GVW'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_MaxGVW'] + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\">" + item['Weight_MaxAXW'] + "</td>" +
                "<td align=\"center\"><a href=\"javascript:void(0);\" class=\"group3\" title=\"Img Front\" onclick=\"popupCenter('" + item['Image01'] + "', 'myPop1',352,288);\"><img border=\"0\" src=\"" + item['Image01'] + "\" width=\"80\" /></a></td>" +
                "<td align=\"center\"><a href=\"javascript:void(0);\" class=\"group3\" title=\"Img Front\" onclick=\"popupCenter('" + item['Image02'] + "', 'myPop1',352,288);\"><img border=\"0\" src=\"" + item['Image02'] + "\" width=\"80\" /></a></td>" +

                //"<td align=\"center\"><a href=\"" + item['Image01'] + "?" + new Date().getTime() + "\" class=\"group3\" title=\"Img Front\" onclick = \"javascript:void window.open(this.href,'" + new Date().getTime() + "','width=352,height=288,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;\"><img border=\"0\" src=\"" + item['Image01'] + "\" width=\"80\" /></a></td>" +
                //"<td align=\"center\"><a href=\"" + item['Image02'] + "?" + new Date().getTime() + "\" class=\"group3\" title=\"Img Front\" onclick = \"javascript:void window.open(this.href,'" + new Date().getTime() + "','width=352,height=288,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;\"><img border=\"0\" src=\"" + item['Image02'] + "\" width=\"80\" /></a></td>" +
                //"<td align=\"center\"><a href=\"" + item['Image02'] + "\" class=\"" + picClassName + "\" title=\"Img Back\"><img border=\"0\" src=\"" + item['Image02'] + "\" width=\"80\" /></a></td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\" align=\"center\">" + overWeight + "</td>" +
                "<td onclick=\"window.location = 'Weighing_View.aspx?WID=" + item['ID'] + "'\" align=\"center\">" + recordStatus + "</td>" +

            "</tr>");
                    
                }
                $('#' + tableId + ' tbody').prepend(tContent);
                

            }
            setTimeout(function () { checkData(); }, refreshTime);

        }
    });
}

function getRowCount() {
    return document.getElementById(tableId).getElementsByTagName('tbody')[0].rows.length;
}

function removeRow(rowCount) {
    for (var i = 0; i < rowCount; i++) {
        $('#' + tableId + ' tbody tr:last').remove();
    }
}