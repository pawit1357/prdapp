﻿function loadReport() {
    //$('#reportArea').html('Loading Report...');
    $('#container').html('');


    var station = $('#cboStation :selected').val();
    var cboLane = $('#cboLane :selected').val();
    var cbLane = $('#cbLane').is(':checked');
    var cboOver = $('#cboOver :selected').val();
    
    var startDateTime;
    var sDate = $('#txtStartDate').val();
    if (sDate != '') {
        var sDay = sDate.substring(0, 2);
        var sMonth = sDate.substring(3, 5);
        var sYear = sDate.substring(6, 10);

        var sHour = sDate.substring(11, 13);
        var sMin = sDate.substring(14, 16);

        startDateTime = sYear + '-' + sMonth + '-' + sDay + ' ' + sHour + ':' + sMin + ':00.000';
    }

    var endDateTime;
    var eDate = $('#txtEndDate').val();
    if (eDate != '') {
        var sDay = eDate.substring(0, 2);
        var sMonth = eDate.substring(3, 5);
        var sYear = eDate.substring(6, 10);

        var sHour = eDate.substring(11, 13);
        var sMin = eDate.substring(14, 16);

        endDateTime = sYear + '-' + sMonth + '-' + sDay + ' ' + sHour + ':' + sMin + ':00.000';
    }
    var graphType = $('#hdGraphType').val();
    //alert(graphType);
    $.ajax({
        url: "/DMT/Ajax.aspx?type=GetReport4&tmp=" + new Date().getTime(),
        type: "POST",
        data: { stationId: station, startDate: startDateTime, endDate: endDateTime, weightover: cboOver },
        dataType: "json",
        success: function (data) {
            if (data != null && data != 'null') {
                //renderReport(data);
                if (graphType == 2) {
                    loadBarChart(data);
                } else {
                    loadLineChart(data);
                }
            }
        }
    });
}

function renderReport(data) {

    if (data != null) {
        var tbl = $('<table id="tbReport" width="100%"></table>');
        tbl.append($('<tr><th></th><th colspan="10" align="center">จำนวนเพลา</th><th rowspan="2">รวม</th></tr>'));
        tbl.append($('<tr class="tr_rowspan"><th>Period</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th></tr>'));
        var rowId = 0;
        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            var rowClassName = '';
            if (rowId == 0) {
                rowClassName = 'class="row1"';
                rowId = 1;
            } else {
                rowClassName = 'class="row2"';
                rowId = 0;
            }
            tbl.append("<tr " + rowClassName + ">" +
                "<td>" + item['Period'] + "</td>" +
                "<td>" + item['1'] + "</td>" +
                "<td>" + item['2'] + "</td>" +
                "<td>" + item['3'] + "</td>" +
                "<td>" + item['4'] + "</td>" +
                "<td>" + item['5'] + "</td>" +
                "<td>" + item['6'] + "</td>" +
                "<td>" + item['7'] + "</td>" +
                "<td>" + item['8'] + "</td>" +
                "<td>" + item['9'] + "</td>" +
                "<td>" + item['10'] + "</td>" +
                "<td>" + item['Total'] + "</td>" +
            "</tr>");
        }


        $('#reportArea').html('');
        $('#reportArea').append(tbl);
    }
}

function loadBarChart(data) {
    var mainData = data;

    var stationName = new Array();
    var station1 = new Array();
    var station2 = new Array();
    var station3 = new Array();

    if (mainData.length > 0) {
        stationName[0] = mainData[0]['Station'];
        stationName[1] = '-';
        stationName[2] = '-';
        station1[0] = parseInt(mainData[0]['1']);
        station1[1] = parseInt(mainData[0]['2']);
        station1[2] = parseInt(mainData[0]['3']);
        station1[3] = parseInt(mainData[0]['4']);
        station1[4] = parseInt(mainData[0]['5']);
        station1[5] = parseInt(mainData[0]['6']);
        station1[6] = parseInt(mainData[0]['7']);
        station1[7] = parseInt(mainData[0]['8']);
        station1[8] = parseInt(mainData[0]['9']);
        station1[9] = parseInt(mainData[0]['10']);
    }
    if (mainData.length > 1) {
        stationName[1] = mainData[0]['Station'];
        station2[0] = parseInt(mainData[1]['1']);
        station2[1] = parseInt(mainData[1]['2']);
        station2[2] = parseInt(mainData[1]['3']);
        station2[3] = parseInt(mainData[1]['4']);
        station2[4] = parseInt(mainData[1]['5']);
        station2[5] = parseInt(mainData[1]['6']);
        station2[6] = parseInt(mainData[1]['7']);
        station2[7] = parseInt(mainData[1]['8']);
        station2[8] = parseInt(mainData[1]['9']);
        station2[9] = parseInt(mainData[1]['10']);
    }
    if (mainData.length > 2) {
        stationName[2] = mainData[0]['Station'];
        station3[0] = parseInt(mainData[2]['1']);
        station3[1] = parseInt(mainData[2]['2']);
        station3[2] = parseInt(mainData[2]['3']);
        station3[3] = parseInt(mainData[2]['4']);
        station3[4] = parseInt(mainData[2]['5']);
        station3[5] = parseInt(mainData[2]['6']);
        station3[6] = parseInt(mainData[2]['7']);
        station3[7] = parseInt(mainData[2]['8']);
        station3[8] = parseInt(mainData[2]['9']);
        station3[9] = parseInt(mainData[2]['10']);
    }
    
    $('#container').highcharts({

        chart: {
            type: 'column'
        },

        title: {
            text: 'Don Muang Tollway Public Company Limited'
        },
        subtitle: {
            text: 'รายงานสรุปภาพรวมปริมาณรถเข้าชั่ง'
        },

        xAxis: {
            categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']


        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'ปริมาณรถ (คัน)'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },

        series: [{
            name: stationName[0],
            data: station1
        }
        ,
        {
            name: stationName[1],
            data: station2

        },

        {
            name: stationName[2],
            data: station3

        }

        ]
    });


}

function loadLineChart(data) {
    var mainData = data;
    var NumberAxles = new Array();
    var series = new Array();
    var station1 = new Array();

    var station2 = new Array();
    var station4 = new Array();
    var station6 = new Array();

    for (var i = 0; i < mainData.length; i++) {
        NumberAxles[NumberAxles.length] = mainData[i]['NumberAxles'];
        station1[station1.length] = parseInt(mainData[i]['1']);
        station1[station1.length] = parseInt(mainData[i]['2']);
        station1[station1.length] = parseInt(mainData[i]['3']);
        station1[station1.length] = parseInt(mainData[i]['4']);
        station1[station1.length] = parseInt(mainData[i]['5']);
        station1[station1.length] = parseInt(mainData[i]['6']);
        station1[station1.length] = parseInt(mainData[i]['7']);
        station1[station1.length] = parseInt(mainData[i]['8']);
        station1[station1.length] = parseInt(mainData[i]['9']);
        station1[station1.length] = parseInt(mainData[i]['10']);
    }



    $('#container').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Don Muang Tollway Public Company Limited'
        },
        subtitle: {
            text: 'รายงานสรุปภาพรวมปริมาณรถเข้าชั่ง'
        },
        xAxis: {
            categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
        },
        yAxis: {
            title: {
                text: 'ปริมาณรถ (คัน)'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
        },

        series: [{
            name: 'ดินแดง',
            data: station1
        },  {
            name: 'รัชดาภิเษก',
            data: station1

        }, {
            name: 'แจ้งวัฒนะ',
            data: station1

        }]
    });
}


