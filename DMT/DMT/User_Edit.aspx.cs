﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using DMT.WS.Dto;
using DMT.WS.Dao;
using DMT.WS.Utils;
using DMT.WS.Biz;

namespace DMT
{
    public partial class User_Edit : System.Web.UI.Page
    {
        protected Boolean SaveSucceed
        {
            get;
            set;
        }

        protected string SaveResultMessage
        {
            get;
            set;
        }

        protected UserLoginDTO UserDTO
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("EDIT_USER"))
                Response.Redirect("LiveWeighing.aspx");

            SaveSucceed = false;
            SaveResultMessage = "";
            UserDTO = null;

            if (!IsPostBack)
            {
                cboStation.Items.Add(new ListItem("-Select-", ""));
                foreach (StationDTO station in new StationDAO().getAll())
                {
                    cboStation.Items.Add(new ListItem(station.StationNameTh, "" + station.ID));
                }

                int id = Gm.toSafeInt(Request.QueryString["UserID"]);

                if (id > 0)
                {
                    // Load User Info
                    UserLoginDAO dao = new UserLoginDAO();
                    UserLoginDTO user = dao.getByPrimaryKey(id) as UserLoginDTO;

                    if (user != null)
                    {
                        UserStationDTO userStation = new UserStationDAO().getActiveByUserLoginId(user.ID);
                        if (userStation != null)
                        {
                            StationDTO station = new StationDAO().getByPrimaryKey(userStation.StationId) as StationDTO;
                            cboStation.SelectedIndex = cboStation.Items.IndexOf(new ListItem(station.StationNameTh, "" + station.ID));
                        }

                        ViewState["UserID"] = user.ID;
                        ViewState["Username"] = user.Username;

                        txtUsername.Value = user.Username;
                        txtFullName.Value = user.FullName;
                        txtPassword.Value = "";
                        txtConfirmPassword.Value = "";
                        txtEMail.Value = user.Email;
                        
                        // UserLevel (list order = Manager,Staff,Admin
                        
                        if (user.Level == 1)
                            lstUserLevel.SelectedIndex = 2; // Admin
                        else if (user.Level == 2)
                            lstUserLevel.SelectedIndex = 0; // Manager
                        else if (user.Level == 3)
                            lstUserLevel.SelectedIndex = 1; // Staff


                        // Status (list order = Active, Inactive)
                        if (user.Status == 0)
                            lstUserStatus.SelectedIndex = 1;    // Inactive
                        else
                            lstUserStatus.SelectedIndex = 0;    // Active

                        UserDTO = user;
                    }
                    else
                    {
                        SaveResultMessage = "ไม่พบรายการที่ระบุ";
                    }
                }
                else
                {
                    SaveResultMessage = "ไม่สามารถแก้ไขรายการที่ระบุได้";
                }
            }
        }

        protected void btSave_OnClick(object Source, EventArgs e)
        {
            UserLoginDTO user = new UserLoginDTO();
            user.ID = Gm.toSafeInt(ViewState["UserID"]);
            user.Username = Gm.toSafeString(ViewState["Username"]);
            user.Password = txtPassword.Value;
            user.FullName = txtFullName.Value;
            user.Email = txtEMail.Value;
            user.Status = Gm.toSafeInt(lstUserStatus.Value);
            user.Level = Gm.toSafeInt(lstUserLevel.Value);

            UserLoginDAO dao = new UserLoginDAO();

            try
            {
                SaveSucceed = dao.update(user);
                int userStationId = Gm.toSafeInt(cboStation.Value);
                UserStationDTO userStation = new UserStationDAO().getActiveByUserLoginId(user.ID);
                if ((userStation == null && userStationId > 0) || (userStation != null && userStation.StationId != userStationId))
                {
                    UserStationDAO userStationDao = new UserStationDAO();
                    userStationDao.setInactiveAllByUserLoginId(user.ID);

                    UserStationDTO userStationDTO = new UserStationDTO();
                    userStationDTO.StationId = Gm.toSafeInt(cboStation.Value);
                    userStationDTO.UserLoginId = user.ID;
                    userStationDTO.Status = "ACTIVE";
                    userStation = new UserStationDAO().save(userStationDTO) as UserStationDTO;
                }


                StationDTO station = new StationDAO().getByPrimaryKey(userStation.StationId) as StationDTO;
                cboStation.SelectedIndex = cboStation.Items.IndexOf(new ListItem(station.StationNameTh, "" + station.ID));

            }
            catch (Exception)
            {
                SaveSucceed = false;
            }

            SaveResultMessage = (SaveSucceed) ? "บันทึกข้อมูลผู้ใช้ใหม่เรียบร้อยแล้ว" : "บันทึกไม่สำเร็จ (Save Failed) !!!";

        }
    }
}