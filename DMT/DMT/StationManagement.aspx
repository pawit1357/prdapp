﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="StationManagement.aspx.cs" Inherits="DMT.StationManagement" %>
<%@ Import Namespace="DMT.WS.Utils" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
             #tbReport .c-manage img
        {
            width:16px;
            height:16px;
            border: solid 1px transparent;
            cursor: pointer;
            padding:3px;
        }
        
        #tbReport .c-manage img:hover
        {
            border-color: Blue;
        }
        
        #tbReport tr.data:hover td 
        {
            background-color: InfoBackground;
        }   
        a:hover 
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <h1>Manage Station</h1>
    <br />
    <h3><a href="Station_Add.aspx">Add New Station</a></h3>
    <br />
    <div>
        <table id="tbReport" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th>Code</th>
                <th>Name Th</th>
                <th>Name En</th>
                <th>Manage</th>
            </tr>
            <!-- Loop to show user's record detail -->
            <% if (UserDataSet != null)
               {
                   string pattern = @"
               <tr class='row{4}'>
                    <td>{0}</td>
                    <td>{1}</td>
                    <td>{2}</td>
                    <td class='c-manage' data-stationid='{3}'>
                        <img src='images/view.png' class='view-user' /><img src='images/edit.png' class='edit-user' /><img src='images/cross.png' class='del-user' />
                    </td>
               </tr>" + "\n";
                
                   System.Data.DataSet ds = UserDataSet;
                   int rowClass = 1;
                   foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                   {
                       //ID	Username	Password	FullName	Email	        Status	UserLevel
                       //1	admin	    admin	    Test Admin2	admin@email.com	1	    1
                       int id = Gm.toSafeInt(dr["ID"]);
                       string stationCode = Gm.toSafeString(dr["StationCode"]);
                       string stationNameTh = Gm.toSafeString(dr["StationNameTh"]);
                       string stationNameEn = Gm.toSafeString(dr["StationNameEn"]);
                       string rowClassName = "";
                       if (rowClass == 1)
                       {
                           rowClassName = "1";
                           rowClass = 2;
                       }
                       else
                       {
                           rowClassName = "2";
                           rowClass = 1;
                       }
                       string tr = string.Format(pattern, stationCode, stationNameTh, stationNameEn, id, rowClassName);
                       Response.Write(tr);
                   }
               }
            %>
       </table>
    </div>
    <script type="text/javascript">
        $(function () {
            // binding function view, edit, delete to each row
            $('#tbReport').on('click', 'img.view-user', onViewUser);
            $('#tbReport').on('click', 'img.edit-user', onEditUser);
            $('#tbReport').on('click', 'img.del-user', onDeleteUser);
        });

        function onViewUser() {
            var id = $(this).parent().attr('data-stationid');
            var url = "Station_View.aspx?StationID=" + id + "&refresh=" + (new Date()).getTime();
            window.location = url;
        }

        function onEditUser() {
            var id = $(this).parent().attr('data-stationid');
            var url = "Station_Edit.aspx?StationID=" + id + "&refresh=" + (new Date()).getTime();
            window.location = url;
        }

        function onDeleteUser() {
            var id = $(this).parent().attr('data-stationid');

            var r = confirm("ยืนยันการลบ?");
            if (r == true) {
                var url = "Station_Delete.aspx?StationID=" + id + "&refresh=" + (new Date()).getTime();
                window.location = url;
            }
        }

    </script>
</asp:Content>