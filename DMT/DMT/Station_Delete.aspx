﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" 
    CodeBehind="Station_Delete.aspx.cs" Inherits="DMT.Station_Delete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #divWrapper
        {
            display:inline-block;
            padding:0px;
            margin:0px;
            border:0px;
            width:100%;
            text-align: center;
        }
        
        #divDeleteResult
        {
            display: inline-block;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            width: 450px;
            height: auto;
            border: 1px solid #DDD;
            border-radius: 4px;
            margin-top: 50px;
            background-color: #F7F7F7;
            margin-bottom: 10px;
        }
        
        #divDeleteResult h1 
        {
            padding: 0px;
            margin: 0px;
            color: #269CCB;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divWrapper">
        <div id="divDeleteResult">
            <h1><%=DeleteResultMessage%></h1>
        </div>
        <br />
        <a href="StationManagement.aspx">ย้อนกลับ (Back)</a>
    </div>
</asp:Content>
