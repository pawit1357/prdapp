﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using DMT.WS.Biz;
using DMT.WS.Utils;
using DMT.WS.Dao;


namespace DMT
{
    public partial class StationManagement : System.Web.UI.Page
    {

        protected DataSet UserDataSet
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_STATION"))
                Response.Redirect("LiveWeighing.aspx");

            LoadUserDataSet();
        }


        private void LoadUserDataSet()
        {

            UserDataSet = null;

            ConnectionDB connection = new ConnectionDB();
            string sql = "SELECT [ID],[StationCode],[StationNameTh],[StationNameEn],[Description] "
                        + "FROM [Station] "
                        + "ORDER BY ID";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            cmd.Connection = connection.GetConnection();
            DataSet ds = new DataSet();
            adpt.Fill(ds);

            if (ds != null && ds.Tables.Count > 0)
            {
                UserDataSet = ds;
            }

            connection.CloseConnection();
        }

        [WebMethod]
        public static void AjaxTest()
        {
            throw new Exception("You must supply an email address.");
        }
    }
}
