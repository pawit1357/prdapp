﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DMT.WS.Biz;
using DMT.WS.Utils;
using DMT.WS.Dao;


namespace DMT
{
    public partial class Report_Delete : System.Web.UI.Page
    {
        protected Boolean DeleteSucceed
        {
            get;
            set;
        }

        protected string DeleteResultMessage
        {
            get;
            set;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("DELETE_STATION"))
                Response.Redirect("LiveWeighing.aspx");

            DeleteSucceed = false;
            DeleteResultMessage = "";

            int id = Gm.toSafeInt(Request.QueryString["ReportID"]);

            if (id > 0)
            {
                DeleteSucceed = DeleteUserLogin(id);
                if (DeleteSucceed)
                    DeleteResultMessage = "ลบรายการสำเร็จ";
                else
                    DeleteResultMessage = (DeleteResultMessage.Length>0) ? DeleteResultMessage : "ลบรายการไม่สำเร็จ (Failed)";
            }
            else
            {
                DeleteResultMessage = "ไม่สามารถลบรายการที่ระบุได้";
            }
        }

        private Boolean DeleteUserLogin(int reportTid)
        {
            return new ReportDAO().deleteById(reportTid);
        }
    }
}