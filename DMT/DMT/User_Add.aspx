﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="User_Add.aspx.cs" Inherits="DMT.User_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tbAddNewUser 
        {
            table-layout: fixed;
            border: 0px;
        }
        
        #tbAddNewUser td
        {
            padding-top: 0.3em;
            padding-bottom: 0.1em;
        }
        
        #tbAddNewUser td.label 
        {
            width: 25em;
            text-align: right;
            padding-right: 2em;
        }
        
        #tbAddNewUser td.field 
        {
            width: 30em;
            text-align: left;
        }
    </style>

    <style type="text/css">
        #divWrapper
        {
            display:inline-block;
            padding:0px;
            margin:0px;
            border:0px;
            width:100%;
            text-align: center;
        }
        
        #divSaveResult
        {
            display: inline-block;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            width: 450px;
            height: auto;
            border: 1px solid #DDD;
            border-radius: 4px;
            margin-top: 50px;
            background-color: #F7F7F7;
            margin-bottom: 10px;
        }
        
        #divSaveResult h1 
        {
            padding: 0px;
            margin: 0px;
            color: #269CCB;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ClientIDMode="Static">
    <% if (SaveResultMessage.Length > 0)
       { %>
    <div id="divWrapper">
        <div id="divSaveResult">
            <h1><%=SaveResultMessage%></h1>
        </div>
    </div>
    <% } %>

    <form action="User_Add.aspx" method="post" runat="server" onsubmit="return ValidateInputForm()">
    <h1>เพิ่มรายชื่อผู้ใช้ระบบ (Add New User)</h1>
    <br />
    <table id="tbAddNewUser" cellpadding="0" cellspacing="0">
        <tr>
            <td class="label">Username :</td>
            <td class="field"><input id="txtUsername" type="text" runat="server" required /></td>
        </tr>
        <tr>
            <td class="label">Password :</td>
            <td class="field"><input id="txtPassword"  type="password" runat="server" enableviewstate="false" required /></td>
        </tr>
        <tr>
            <td class="label">Confirm Password :</td>
            <td class="field"><input id="txtConfirmPassword" type="password" runat="server" enableviewstate="false" required /></td>
        </tr>
        <tr>
            <td class="label">Full Name :</td>
            <td class="field"><input id="txtFullName" type="text" runat="server" required /></td>
        </tr>
        <tr>
            <td class="label">E-Mail :</td>
            
            <td class="field"><input id="txtEMail" type="text" runat="server" required /></td>
        </tr>
        <tr>
            <td class="label">Status :</td>
            <td class="field">
                <select id="lstUserStatus" runat="server">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label">Level :</td>
            <td class="field">
                <select id="lstUserLevel" runat="server">
                    <option value="2">Manager</option>
                    <option value="3">Staff</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label">Station :</td>
            <td class="field">
                <select id="cboStation" runat="server">
                </select>
            </td>
        </tr>
        <tr>
            <td class="label">&nbsp;</td>
            <td class="field">
                <input id="btSave" type="submit" runat="server" value="Save" onserverclick="btSave_OnClick" />
                <input id="btClear" type="reset" value="Cancel" />
            </td>
        </tr>
    </table>
    </form>
    <a href="UserManagement.aspx">ย้อนกลับ (Back)</a>

    <script type="text/javascript">
        function ValidateInputForm() {
            var errMsg = '';
            if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
                errMsg = 'ยืนยันรหัสผ่านไม่ถูกต้อง';
            }

            if (errMsg != '') {
                alert('Error: ' + errMsg);
            }

            return (errMsg == '');
        }


        $(function () {
            $('#btClear').click(function () {
                $('#txtUsername,#txtFullName,#txtPassword,#txtConfirmPassword,#txtEMail').val('');
                $('#lstUserStatus').val('1');
                $('#lstUserLevel').val('2');
            });


            $('#btClear').click();
        });

    </script>
</asp:Content>
