﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Station_Add.aspx.cs" Inherits="DMT.Station_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tbAddNewUser 
        {
            table-layout: fixed;
            border: 0px;
        }
        
        #tbAddNewUser td
        {
            padding-top: 0.3em;
            padding-bottom: 0.1em;
        }
        
        #tbAddNewUser td.label 
        {
            width: 25em;
            text-align: right;
            padding-right: 2em;
        }
        
        #tbAddNewUser td.field 
        {
            width: 30em;
            text-align: left;
        }
    </style>

    <style type="text/css">
        #divWrapper
        {
            display:inline-block;
            padding:0px;
            margin:0px;
            border:0px;
            width:100%;
            text-align: center;
        }
        
        #divSaveResult
        {
            display: inline-block;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            width: 450px;
            height: auto;
            border: 1px solid #DDD;
            border-radius: 4px;
            margin-top: 50px;
            background-color: #F7F7F7;
            margin-bottom: 10px;
        }
        
        #divSaveResult h1 
        {
            padding: 0px;
            margin: 0px;
            color: #269CCB;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ClientIDMode="Static">
    <% if (SaveResultMessage.Length > 0)
       { %>
    <div id="divWrapper">
        <div id="divSaveResult">
            <h1><%=SaveResultMessage%></h1>
        </div>
    </div>
    <% } %>

    <form action="Station_Add.aspx" method="post" runat="server" onsubmit="return ValidateInputForm()">
    <h1>Add New Station</h1>
    <br />
    <table id="tbAddNewUser" cellpadding="0" cellspacing="0">
        <tr>
            <td class="label">Station Code :</td>
            <td class="field"><input id="txtCode" type="text" runat="server" required /></td>
        </tr>
        <tr>
            <td class="label">Name TH :</td>
            <td class="field"><input id="txtNameTH"  type="text" runat="server" enableviewstate="false" required /></td>
        </tr>
        <tr>
            <td class="label">Name EN :</td>
            <td class="field"><input id="txtNameEN" type="text" runat="server" enableviewstate="false" required /></td>
        </tr>
        <tr>
            <td class="label">Description :</td>
            <td class="field"><input id="txtDescription" type="text" runat="server" required /></td>
        </tr>
        <tr>
            <td class="label">&nbsp;</td>
            <td class="field">
                <input id="btSave" type="submit" runat="server" value="Save" onserverclick="btSave_OnClick" />
                <input id="btClear" type="reset" value="Cancel" />
            </td>
        </tr>
    </table>
    </form>
    <a href="StationManagement.aspx">ย้อนกลับ (Back)</a>

    <script type="text/javascript">
        function ValidateInputForm() {
            return true;
        }


        $(function () {
            $('#btClear').click(function () {
                $('#txtCode,#txtNameTH,#txtNameEN,#txtDescription').val('');
            });


            $('#btClear').click();
        });

    </script>
</asp:Content>
