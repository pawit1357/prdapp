﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DMT.WS.Biz;
using DMT.WS.Dto;
using DMT.WS.Utils;
using DMT.WS.Dao;
using System.Net;
using System.IO;
using System.Data.SqlClient;


namespace DMT.WS.Biz
{
    //public class WeighingBAO
    //{
    //    public List<WeighingBean> getByCondition(string stationId, string startDate, string endDate)
    //    {
    //        var webClient = new WebClient();

    //        List<WeighingBean> weighings = new List<WeighingBean>();
    //        List<WeighingDTO> dtos = new WeighingDAO().getAll(stationId, startDate, endDate, null);
    //        foreach (WeighingDTO dto in dtos)
    //        {
    //            WeighingBean bean = new WeighingBean();
    //            bean.ID = dto.ID;
    //            bean.Timestamp = dto.Timestamp;
    //            bean.RunningNumber = dto.RunningNumber;
    //            bean.Station = new StationBAO().getById(Gm.toSafeInt(dto.StationID));
    //            bean.LaneID = dto.LaneID;
    //            bean.NumberAxles = dto.NumberAxles;
    //            bean.Axle01 = dto.WeightAxle01;
    //            bean.Axle02 = dto.WeightAxle02;
    //            bean.Axle03 = dto.WeightAxle03;
    //            bean.Axle04 = dto.WeightAxle04;
    //            bean.Axle05 = dto.WeightAxle05;
    //            bean.Axle06 = dto.WeightAxle06;
    //            bean.Axle07 = dto.WeightAxle07;
    //            bean.Axle08 = dto.WeightAxle08;
    //            bean.Axle09 = dto.WeightAxle09;
    //            bean.Axle10 = dto.WeightAxle10;
    //            bean.WeightGVW = dto.WeightGVW;
    //            bean.WeightMaxGVW = dto.WeightMaxGVW;
    //            bean.WeightMaxAXW = dto.WeightMaxAXW;
    //            bean.IsOverWeight = dto.IsOverWeight;
    //            bean.RecordStatus = dto.RecordStatus;
    //            bean.img1 = dto.img1;
    //            bean.img2 = dto.img2;
    //            weighings.Add(bean);
    //        }
    //        return weighings;
    //    }
    //    public List<WeighingBean> getByCondition(string stationId, string startDate, string endDate, string weightover)
    //    {
    //        var webClient = new WebClient();
    //        List<WeighingBean> weighings = new List<WeighingBean>();
    //        List<WeighingDTO> dtos = new WeighingDAO().getAll(stationId, startDate, endDate, weightover);
    //        foreach (WeighingDTO dto in dtos)
    //        {
    //            WeighingBean bean = new WeighingBean();
    //            bean.ID = dto.ID;
    //            bean.Timestamp = dto.Timestamp;
    //            bean.RunningNumber = dto.RunningNumber;
    //            bean.Station = new StationBAO().getById(DMT.WS.Utils.Gm.toSafeInt(dto.StationID));
    //            bean.LaneID = dto.LaneID;
    //            bean.NumberAxles = dto.NumberAxles;
    //            bean.Axle01 = dto.WeightAxle01;
    //            bean.Axle02 = dto.WeightAxle02;
    //            bean.Axle03 = dto.WeightAxle03;
    //            bean.Axle04 = dto.WeightAxle04;
    //            bean.Axle05 = dto.WeightAxle05;
    //            bean.Axle06 = dto.WeightAxle06;
    //            bean.Axle07 = dto.WeightAxle07;
    //            bean.Axle08 = dto.WeightAxle08;
    //            bean.Axle09 = dto.WeightAxle09;
    //            bean.Axle10 = dto.WeightAxle10;
    //            bean.WeightGVW = dto.WeightGVW;
    //            bean.WeightMaxGVW = dto.WeightMaxGVW;
    //            bean.WeightMaxAXW = dto.WeightMaxAXW;
    //            bean.IsOverWeight = dto.IsOverWeight;
    //            bean.RecordStatus = dto.RecordStatus;
    //            bean.Image01 = dto.Image01;
    //            bean.Image02 = dto.Image02;
    //            bean.img1 = dto.img1;
    //            bean.img2 = dto.img2;
    //            weighings.Add(bean);
    //        }
    //        return weighings;
    //    }

    //    public List<WeighingSummaryAllStationDTO> getSummaryAllStation(string periodType, string stationId, string startDate, string endDate, string weightover)
    //    {
    //        var webClient = new WebClient();
    //        List<WeighingSummaryAllStationDTO> weighings = new List<WeighingSummaryAllStationDTO>();

    //        string sql = "EXEC [dbo].[SPC_Report3]	@StationID = @sId, @IsOverWeight = @isOver, @StartDate = @sDate, @EndDate = @eDate, @PeriodType = @pType";
            
    //        SqlCommand cmd = new SqlCommand(sql);
    //        cmd.CommandType = System.Data.CommandType.Text;
    //        cmd.Parameters.Add("pType", System.Data.SqlDbType.Int).Value = periodType;
    //        if (stationId != null)
    //        {
    //            cmd.Parameters.Add("@sId", System.Data.SqlDbType.Int).Value = stationId;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@sId", System.Data.SqlDbType.Int).Value = System.Data.SqlTypes.SqlInt32.Null;
    //        }

    //        if (weightover != null)
    //        {
    //            cmd.Parameters.Add("@isOver", System.Data.SqlDbType.Int).Value = weightover;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@isOver", System.Data.SqlDbType.Int).Value = System.Data.SqlTypes.SqlInt32.Null;
    //        }

    //        if (startDate != null)
    //        {
    //            cmd.Parameters.Add("@sDate", System.Data.SqlDbType.DateTime).Value = startDate;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@sDate", System.Data.SqlDbType.DateTime).Value = System.Data.SqlTypes.SqlDateTime.Null;
    //        }
    //        if (endDate != null)
    //        {
    //            cmd.Parameters.Add("@eDate", System.Data.SqlDbType.DateTime).Value = endDate;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@eDate", System.Data.SqlDbType.DateTime).Value = System.Data.SqlTypes.SqlDateTime.Null;
    //        }

    //        ConnectionDB connection = new ConnectionDB();
    //        connection.OpenConnection();
    //        SqlDataReader data = connection.ExecuteQuery(cmd);

    //        List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();
    //        while (data.Read())
    //        {
    //            WeighingSummaryAllStationDTO w = new WeighingSummaryAllStationDTO();
    //            w.Period = "" + data["Period"];
    //            w.Weight1 = Gm.toSafeInt(data["1"]);
    //            w.Weight2 = Gm.toSafeInt(data["2"]);
    //            w.Weight3 = Gm.toSafeInt(data["3"]);
    //            w.Weight4 = Gm.toSafeInt(data["4"]);
    //            w.Weight5 = Gm.toSafeInt(data["5"]);
    //            w.Weight6 = Gm.toSafeInt(data["6"]);
    //            w.Weight7 = Gm.toSafeInt(data["7"]);
    //            w.Weight8 = Gm.toSafeInt(data["8"]);
    //            w.Weight9 = Gm.toSafeInt(data["9"]);
    //            w.Weight10 = Gm.toSafeInt(data["10"]);
    //            w.Total = Gm.toSafeInt(data["Total"]);
    //            weighings.Add(w);
    //        }
    //        return weighings;
    //    }

    //    public List<WeighingSummaryAllStationDTO> getSummaryAllStationReport3(string type, string stationId, string startDate, string endDate, string weightover)
    //    {
    //        var webClient = new WebClient();
    //        List<WeighingSummaryAllStationDTO> weighings = new List<WeighingSummaryAllStationDTO>();

    //        string sql = "EXEC [dbo].[SPC_Report3_1]	@StationID = @sId, @IsOverWeight = @isOver, @StartDate = @sDate, @EndDate = @eDate, @PeriodType = @pType";

    //        SqlCommand cmd = new SqlCommand(sql);
    //        cmd.CommandType = System.Data.CommandType.Text;
    //        cmd.Parameters.Add("pType", System.Data.SqlDbType.Int).Value = type;
    //        if (stationId != null && !String.IsNullOrEmpty(stationId))
    //        {
    //            cmd.Parameters.Add("@sId", System.Data.SqlDbType.Int).Value = stationId;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@sId", System.Data.SqlDbType.Int).Value = System.Data.SqlTypes.SqlInt32.Null;
    //        }

    //        if (weightover != null && !String.IsNullOrEmpty(weightover))
    //        {
    //            cmd.Parameters.Add("@isOver", System.Data.SqlDbType.Int).Value = weightover;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@isOver", System.Data.SqlDbType.Int).Value = System.Data.SqlTypes.SqlInt32.Null;
    //        }

    //        if (startDate != null && !String.IsNullOrEmpty(startDate))
    //        {
    //            cmd.Parameters.Add("@sDate", System.Data.SqlDbType.DateTime).Value = startDate;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@sDate", System.Data.SqlDbType.DateTime).Value = System.Data.SqlTypes.SqlDateTime.Null;
    //        }
    //        if (endDate != null && !String.IsNullOrEmpty(endDate))
    //        {
    //            cmd.Parameters.Add("@eDate", System.Data.SqlDbType.DateTime).Value = endDate;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@eDate", System.Data.SqlDbType.DateTime).Value = System.Data.SqlTypes.SqlDateTime.Null;
    //        }

    //        ConnectionDB connection = new ConnectionDB();
    //        connection.OpenConnection();
    //        SqlDataReader data = connection.ExecuteQuery(cmd);

    //        List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();
    //        while (data.Read())
    //        {
    //            WeighingSummaryAllStationDTO w = new WeighingSummaryAllStationDTO();
    //            w.Station = ""+data["StationNameTh"];
    //            if (type.Equals("2"))
    //            {
    //                w.Lane = Gm.toSafeInt(data["LaneID"]);
    //            }
    //            //w.Period = "" + data["Period"];
    //            w.Weight1 = Gm.toSafeInt(data["1"]);
    //            w.Weight2 = Gm.toSafeInt(data["2"]);
    //            w.Weight3 = Gm.toSafeInt(data["3"]);
    //            w.Weight4 = Gm.toSafeInt(data["4"]);
    //            w.Weight5 = Gm.toSafeInt(data["5"]);
    //            w.Weight6 = Gm.toSafeInt(data["6"]);
    //            w.Weight7 = Gm.toSafeInt(data["7"]);
    //            w.Weight8 = Gm.toSafeInt(data["8"]);
    //            w.Weight9 = Gm.toSafeInt(data["9"]);
    //            w.Weight10 = Gm.toSafeInt(data["10"]);
    //            w.Total = Gm.toSafeInt(data["Total"]);
    //            weighings.Add(w);
    //        }
    //        return weighings;
    //    }

    //    public List<WeighingSummaryAllStationDTO> getSummaryAllStationReport4(string type, string stationId, string startDate, string endDate, string weightover)
    //    {
    //        var webClient = new WebClient();
    //        List<WeighingSummaryAllStationDTO> weighings = new List<WeighingSummaryAllStationDTO>();

    //        string sql = "EXEC [dbo].[SPC_Report3_1]	@StationID = @sId, @IsOverWeight = @isOver, @StartDate = @sDate, @EndDate = @eDate, @PeriodType = @pType";

    //        SqlCommand cmd = new SqlCommand(sql);
    //        cmd.CommandType = System.Data.CommandType.Text;
    //        cmd.Parameters.Add("pType", System.Data.SqlDbType.Int).Value = type;
    //        if (stationId != null && !String.IsNullOrEmpty(stationId))
    //        {
    //            cmd.Parameters.Add("@sId", System.Data.SqlDbType.Int).Value = stationId;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@sId", System.Data.SqlDbType.Int).Value = System.Data.SqlTypes.SqlInt32.Null;
    //        }

    //        if (weightover != null && !String.IsNullOrEmpty(weightover))
    //        {
    //            cmd.Parameters.Add("@isOver", System.Data.SqlDbType.Int).Value = weightover;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@isOver", System.Data.SqlDbType.Int).Value = System.Data.SqlTypes.SqlInt32.Null;
    //        }

    //        if (startDate != null && !String.IsNullOrEmpty(startDate))
    //        {
    //            cmd.Parameters.Add("@sDate", System.Data.SqlDbType.DateTime).Value = startDate;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@sDate", System.Data.SqlDbType.DateTime).Value = System.Data.SqlTypes.SqlDateTime.Null;
    //        }
    //        if (endDate != null && !String.IsNullOrEmpty(endDate))
    //        {
    //            cmd.Parameters.Add("@eDate", System.Data.SqlDbType.DateTime).Value = endDate;
    //        }
    //        else
    //        {
    //            cmd.Parameters.Add("@eDate", System.Data.SqlDbType.DateTime).Value = System.Data.SqlTypes.SqlDateTime.Null;
    //        }

    //        ConnectionDB connection = new ConnectionDB();
    //        connection.OpenConnection();
    //        SqlDataReader data = connection.ExecuteQuery(cmd);

    //        List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();
    //        while (data.Read())
    //        {
    //            WeighingSummaryAllStationDTO w = new WeighingSummaryAllStationDTO();
    //            w.Station = "" + data["StationNameTh"];
    //            if (type.Equals("4"))
    //            {
    //                w.Lane = Gm.toSafeInt(data["LaneID"]);
    //            }
    //            //w.Period = "" + data["Period"];
    //            w.WeightNormal = Gm.toSafeInt(data["False"]);
    //            w.WeightOver = Gm.toSafeInt(data["True"]);
    //            weighings.Add(w);
    //        }
    //        return weighings;
    //    }
    //    public WeighingBean getByPK(int id)
    //    {
    //        var webClient = new WebClient();
    //        WeighingDTO dto = (WeighingDTO)new WeighingDAO().getByPrimaryKey(id);

    //        WeighingBean bean = new WeighingBean();
    //        bean.ID = dto.ID;
    //        bean.Timestamp = dto.Timestamp;
    //        bean.RunningNumber = dto.RunningNumber;
    //        bean.Station = new StationBAO().getById(Gm.toSafeInt(dto.StationID));
    //        bean.LaneID = dto.LaneID;
    //        bean.NumberAxles = dto.NumberAxles;
    //        bean.Axle01 = dto.WeightAxle01;
    //        bean.Axle02 = dto.WeightAxle02;
    //        bean.Axle03 = dto.WeightAxle03;
    //        bean.Axle04 = dto.WeightAxle04;
    //        bean.Axle05 = dto.WeightAxle05;
    //        bean.Axle06 = dto.WeightAxle06;
    //        bean.Axle07 = dto.WeightAxle07;
    //        bean.Axle08 = dto.WeightAxle08;
    //        bean.Axle09 = dto.WeightAxle09;
    //        bean.Axle10 = dto.WeightAxle10;
    //        bean.WeightGVW = dto.WeightGVW;
    //        bean.WeightMaxGVW = dto.WeightMaxGVW;
    //        bean.WeightMaxAXW = dto.WeightMaxAXW;
    //        bean.IsOverWeight = dto.IsOverWeight;
    //        bean.RecordStatus = dto.RecordStatus;
    //        bean.Image01 = dto.Image01;
    //        bean.Image02 =  dto.Image02;
    //        bean.img1 = dto.img1;
    //        bean.img2 = dto.img2;
    //        return bean;
    //    }
    //}
}