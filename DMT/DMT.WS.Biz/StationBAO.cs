﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DMT.WS.Dto;
using DMT.WS.Dao;


namespace DMT.WS.Biz
{
    public class StationBAO
    {
        public StationBean getById(int id)
        {
            StationDTO dto = (StationDTO)new StationDAO().getByPrimaryKey(id);
            StationBean bean = null;
            if (dto != null)
            {
                bean = new StationBean();
                bean.ID = dto.ID;
                bean.StationCode = dto.StationCode;
                bean.StationNameEn = dto.StationNameEn;
                bean.StationNameTh = dto.StationNameTh;
                bean.Description = dto.Description;
            }
            return bean;
        }
    }
}