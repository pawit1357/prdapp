﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DMT.WS.Dto;
using DMT.WS.Dao;


namespace DMT.WS.Biz
{
    public class UserLoginBAO
    {
        public UserLoginBean getUserLoginByUsernameAndPassword(string username, string password)
        {
            UserLoginBean bean = null;
            UserLoginDAO dao = new UserLoginDAO();
            UserLoginDTO dto = (UserLoginDTO) dao.getByPrimaryUsernameAndPassword(username, password);
            if (dto != null)
            {
                bean = new UserLoginBean();
                bean.ID = dto.ID;
                bean.Username = dto.Username;
                bean.Password = dto.Password;
                bean.Email = dto.Email;
                bean.FullName = dto.FullName;
                bean.Status = dto.Status;
                bean.Level = dto.Level;
            }
            return bean;
        }

        public UserLoginBean updateByPk(int id, string password, string email, string fullName)
        {
            UserLoginBean bean = null;
            UserLoginDAO dao = new UserLoginDAO();
            UserLoginDTO dto = (UserLoginDTO)dao.getByPrimaryKey(id);
            if (dto != null)
            {
                if (password != null && password != "")
                {
                    dto.Password = password;
                }
                dto.Email = email;
                dto.FullName = fullName;
                if (dao.update(dto))
                {
                    bean = new UserLoginBean();
                    bean.ID = dto.ID;
                    bean.Username = dto.Username;
                    bean.Password = dto.Password;
                    bean.Email = dto.Email;
                    bean.FullName = dto.FullName;
                    bean.Status = dto.Status;
                    bean.Level = dto.Level;
                }                
            }
            return bean;
        }
    }
}