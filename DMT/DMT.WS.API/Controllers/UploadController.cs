﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using DMT.WS.Biz;
using DMT.WS.Dao;
using DMT.WS.Dto;
using DMT.WS.Utils;
using System.Globalization;
using System.Threading;

namespace DMT.WS.API.Controllers
{
    public class UploadController : Controller
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(UploadController));
        //
        // GET: /Upload/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(IEnumerable<HttpPostedFileBase> files)
        {
            WeighingDAO dao = new WeighingDAO();
            bool bResult = false;
            string resp = string.Empty;
            try
            {
                if (files != null)
                {

                    ViewData["response"] = string.Empty;
                    foreach (var file in files)
                    {
                        if (file.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(file.FileName);

                            if (fileName.EndsWith("txt"))
                            {
                                //if (file.FileName.Length != 24)
                                //{
                                //    logger.Error("File " + file.FileName + " length must be equest 24 digit in format yyyyMMdd-HHmmss-xxxx.yyy .");
                                //    resp =  "\t" + "1,Fail" + "\r\n";                                    
                                //}
                                //else
                                //{
                                StreamReader stream = new StreamReader(file.InputStream);
                                string data = stream.ReadToEnd();
                                if (!data.Substring(0, 15).Equals(fileName.Substring(0, 15)))
                                {
                                    logger.Error("File " + file.FileName + " the date data in file content is not match with filename prefix.");
                                    //resp = "\t" + "1,Fail" + "\r\n";
                                }
                                else
                                {
                                    //Check duplicate data.
                                    if (dao.getByTimeStamp(CommonUtils.customConvert2Date(data.Substring(0, 15))) == null)
                                    {
                                        
                                        insertDB(dao, data);
                                        bResult = true;
                                    }
                                    else
                                    {
                                        logger.Error(data + "Duplicate data.");
                                        //resp = "\t" + "1,Duplicate data." + "\r\n";
                                    }
                                }
                                //}
                            }
                            else if (fileName.ToLower().EndsWith("jpg") || fileName.ToLower().EndsWith("png"))
                            {
                                if (fileName.Length > 10)
                                {
                                    string[] tmp = fileName.Split('-');
                                    String path = String.Format(Configurations.ImageFilePath, tmp[2], tmp[3], tmp[0]);
                                    var fullpath = Path.Combine(path, fileName);
                                    if (!Directory.Exists(path))
                                    {
                                        Directory.CreateDirectory(path);
                                    }

                                    if (!System.IO.File.Exists(fullpath))
                                    {
                                        file.SaveAs(fullpath);
                                        bResult = true;
                                    }
                                    else
                                    {
                                        //resp = "\t" + "1,Duplicate image." + "\r\n";
                                        logger.Error(fileName + "Duplicate image.");
                                    }
                                }
                            }

                        }
                    }

                }
                else
                {

                }
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                ViewData["response"] = e.Message;
            }

            if (bResult)
            {
                resp += "\t" + "OK" + "\r\n";               
            }
            else
            {
                resp += "\t" + "FAIL" + "\r\n";
            }
            ViewData["response"] = resp;
            return View();
        }

        private void insertDB(WeighingDAO dao, string data)
        {

            String[] tmp = data.Split(',');
            WeighingDTO dto = new WeighingDTO();
            dto.ID = 0;
            dto.Timestamp = CommonUtils.customConvert2Date(tmp[0]);
            dto.RunningNumber = (!CommonUtils.IsNumeric(tmp[1]) ? 0 : Convert.ToInt32(tmp[1]));
            dto.StationID = (!CommonUtils.IsNumeric(tmp[2]) ? 0 : Convert.ToInt32(tmp[2]));
            dto.LaneID = (!CommonUtils.IsNumeric(tmp[3]) ? 0 : Convert.ToInt32(tmp[3]));
            //Skip VW value
            dto.NumberAxles = (!CommonUtils.IsNumeric(tmp[5])? 0:Convert.ToInt32(tmp[5]));
            dto.WeightAxle01 =(!CommonUtils.IsNumeric(tmp[6])? 0: Convert.ToInt32(tmp[6]));
            dto.WeightAxle02 = (!CommonUtils.IsNumeric(tmp[7]) ? 0 : Convert.ToInt32(tmp[7]));
            dto.WeightAxle03 = (!CommonUtils.IsNumeric(tmp[8]) ? 0 : Convert.ToInt32(tmp[8]));
            dto.WeightAxle04 = (!CommonUtils.IsNumeric(tmp[9]) ? 0 : Convert.ToInt32(tmp[9]));
            dto.WeightAxle05 = (!CommonUtils.IsNumeric(tmp[10]) ? 0 : Convert.ToInt32(tmp[10]));
            dto.WeightAxle06 = (!CommonUtils.IsNumeric(tmp[11]) ? 0 : Convert.ToInt32(tmp[11]));
            dto.WeightAxle07 = (!CommonUtils.IsNumeric(tmp[12]) ? 0 : Convert.ToInt32(tmp[12]));
            dto.WeightAxle08 = (!CommonUtils.IsNumeric(tmp[13]) ? 0 : Convert.ToInt32(tmp[13]));
            dto.WeightAxle09 = (!CommonUtils.IsNumeric(tmp[14]) ? 0 : Convert.ToInt32(tmp[14]));
            dto.WeightAxle10 = (!CommonUtils.IsNumeric(tmp[15]) ? 0 : Convert.ToInt32(tmp[15]));
            dto.WeightGVW = (!CommonUtils.IsNumeric(tmp[16]) ? 0 : Convert.ToInt32(tmp[16]));
            dto.WeightMaxGVW = (!CommonUtils.IsNumeric(tmp[17]) ? 0 : Convert.ToInt32(tmp[17]));
            dto.WeightMaxAXW = (!CommonUtils.IsNumeric(tmp[18]) ? 0 : Convert.ToInt32(tmp[18]));
            dto.IsOverWeight = (tmp[19].ToUpper().Equals("Y") ? true : false);
            dto.RecordStatus = tmp[20];

            //String xx = DateTime.ParseExact(dto.Timestamp.ToFileTimeUtc(), "dd/MM/yyyy HH:mm", new CultureInfo("en-US"));

            String HHmmss = tmp[0].Substring(9,2)+tmp[0].Substring(11,2)+tmp[0].Substring(13,2);


            String img1 = dto.Timestamp.ToString("yyyyMMdd") + "-" + HHmmss + "-" + dto.StationID + "-" + dto.LaneID + "-img1.jpg";
            String img2 = dto.Timestamp.ToString("yyyyMMdd") + "-" + HHmmss + "-" + dto.StationID + "-" + dto.LaneID + "-img2.jpg";

            dto.Image01 = String.Format(Configurations.ImageWebPath, dto.StationID, dto.LaneID, dto.Timestamp.ToString("yyyyMMdd"), img1);
            dto.Image02 = String.Format(Configurations.ImageWebPath, dto.StationID, dto.LaneID, dto.Timestamp.ToString("yyyyMMdd"), img2);
            
            //ImageWebPath
            //Begin Insert
            Thread.Sleep(3);
            DefaultDTO rDto = dao.save(dto);
            if (rDto == null)
            {
                logger.Error("Can't insert data to db.");
            }
        }
    }
}
