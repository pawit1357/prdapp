﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DMT.WS.Dto;

namespace DMT.WS.Dto
{
    public class WeighingDTO : DefaultDTO
    {
        private int id;
        private DateTime timestamp;
        private int runningNumber;
        private int stationID;
        private int laneID;
        private int numberAxles;
        private int weightAxle01;
        private int weightAxle02;
        private int weightAxle03;
        private int weightAxle04;
        private int weightAxle05;
        private int weightAxle06;
        private int weightAxle07;
        private int weightAxle08;
        private int weightAxle09;
        private int weightAxle10;
        private int intWeightGVW;
        private int weightMaxGVW;
        private int weightMaxAXW;
        private int total;
        private bool isOverWeight;
        private string recordStatus;
        private string image01;
        private string image02;
        public byte[] img1 { get; set; }
        public byte[] img2 { get; set; }

        public String logo { get; set; }
        public String sign { get; set; }
        //public String stationName { get; set; }
        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public DateTime Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }
        public int RunningNumber
        {
            get { return runningNumber; }
            set { runningNumber = value; }
        }
        public int StationID
        {
            get { return stationID; }
            set { stationID = value; }
        }
        public int LaneID
        {
            get { return laneID; }
            set { laneID = value; }
        }
        public int NumberAxles
        {
            get { return numberAxles; }
            set { numberAxles = value; }
        }
        public int WeightAxle01
        {
            get { return weightAxle01; }
            set { weightAxle01 = value; }
        }
        public int WeightAxle02
        {
            get { return weightAxle02; }
            set { weightAxle02 = value; }
        }
        public int WeightAxle03
        {
            get { return weightAxle03; }
            set { weightAxle03 = value; }
        }
        public int WeightAxle04
        {
            get { return weightAxle04; }
            set { weightAxle04 = value; }
        }
        public int WeightAxle05
        {
            get { return weightAxle05; }
            set { weightAxle05 = value; }
        }
        public int WeightAxle06
        {
            get { return weightAxle06; }
            set { weightAxle06 = value; }
        }
        public int WeightAxle07
        {
            get { return weightAxle07; }
            set { weightAxle07 = value; }
        }
        public int WeightAxle08
        {
            get { return weightAxle08; }
            set { weightAxle08 = value; }
        }
        public int WeightAxle09
        {
            get { return weightAxle09; }
            set { weightAxle09 = value; }
        }
        public int WeightAxle10
        {
            get { return weightAxle10; }
            set { weightAxle10 = value; }
        }
        public int WeightGVW
        {
            get { return intWeightGVW; }
            set { intWeightGVW = value; }
        }
        public int WeightMaxGVW
        {
            get { return weightMaxGVW; }
            set { weightMaxGVW = value; }
        }
        public int WeightMaxAXW
        {
            get { return weightMaxAXW; }
            set { weightMaxAXW = value; }
        }
        public bool IsOverWeight
        {
            get { return isOverWeight; }
            set { isOverWeight = value; }
        }
        public string RecordStatus
        {
            get { return recordStatus; }
            set { recordStatus = value; }
        }
        public int Total
        {
            get { return (weightAxle01+
                weightAxle02+
                weightAxle03+
                weightAxle04+
                weightAxle05+
                weightAxle06+
                weightAxle07+
                weightAxle08+
                weightAxle09+weightAxle10); }
            //set { total = value; }
        }
        public string Image01
        {
            get { return image01; }
            set { image01 = value; }
        }
        public string Image02
        {
            get { return image02; }
            set { image02 = value; }
        }
        public string StationName { get; set; }
    }
}