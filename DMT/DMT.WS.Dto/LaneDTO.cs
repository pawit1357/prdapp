﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DMT.WS.Dto
{
    public class LaneDTO:DefaultDTO
    {
        private int id;
        private int stationId;
        private string laneName;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public int StationId
        {
            get { return stationId; }
            set { stationId = value; }
        }
        public string LaneName
        {
            get { return laneName; }
            set { laneName = value; }
        }
    }
}
