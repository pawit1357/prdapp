﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DMT.WS.Dto
{
    public class WeighingSummaryAllStationDTO
    {
        private string period;

        public string Period
        {
            get { return period; }
            set { period = value; }
        }
        private string station;
        public string Station
        {
            get { return station; }
            set { station = value; }
        }
        private int lane;
        public int Lane
        {
            get { return lane; }
            set { lane = value; }
        }
        private double weight1;

        public double Weight1
        {
            get { return weight1; }
            set { weight1 = value; }
        }
        private double weight2;

        public double Weight2
        {
            get { return weight2; }
            set { weight2 = value; }
        }
        private double weight3;

        public double Weight3
        {
            get { return weight3; }
            set { weight3 = value; }
        }
        private double weight4;


        public double Weight4
        {
            get { return weight4; }
            set { weight4 = value; }
        }
        private double weight5;

        public double Weight5
        {
            get { return weight5; }
            set { weight5 = value; }
        }
        private double weight6;

        public double Weight6
        {
            get { return weight6; }
            set { weight6 = value; }
        }
        private double weight7;

        public double Weight7
        {
            get { return weight7; }
            set { weight7 = value; }
        }
        private double weight8;

        public double Weight8
        {
            get { return weight8; }
            set { weight8 = value; }
        }
        private double weight9;

        public double Weight9
        {
            get { return weight9; }
            set { weight9 = value; }
        }
        private double weight10;

        public double Weight10
        {
            get { return weight10; }
            set { weight10 = value; }
        }

        public double WeightNormal {get;set;}
        public double WeightOver { get; set; }
        private double total;
        public double Total
        {
            get { return total; }
            set { total = value; }
        }


    }
}
