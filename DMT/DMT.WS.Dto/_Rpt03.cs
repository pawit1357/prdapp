﻿namespace DMT.WS.Dto
{
    public class _Rpt03 : WeighingDTO
    {
        public int Lane { get; set; }
        public double CountOfWeightOver { get; set; }
        public double CountOfWeightNormal { get; set; }
        public double WeightTotal { get; set; }

        public double CountOfWeightOverPercent
        {
            get { return ((CountOfWeightOver * 100) / WeightTotal); }
        }
        public double CountOfWeightNormalPercent
        {
            get { return ((CountOfWeightNormal * 100) / WeightTotal); }
        }


        public double CountOfWeightOverTotal { get; set; }
        public double CountOfWeightNormalTotal { get; set; }

        public double GrandTotalWeightOverPercent
        {
            get { return ((CountOfWeightOverTotal * 100) / (CountOfWeightOverTotal + CountOfWeightNormalTotal)); }
        }
        public double GrandTotalWeightNormalPercent
        {
            get { return ((CountOfWeightNormalTotal * 100) / (CountOfWeightOverTotal + CountOfWeightNormalTotal)); }
        }
    }
}
