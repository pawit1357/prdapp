﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DMT.WS.Dto;

namespace DMT.WS.Dto
{
    public class UserLoginDTO : DefaultDTO
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        private string username;

        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        private string fullName;

        public string FullName
        {
            get { return fullName; }
            set { fullName = value; }
        }
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        private int status;

        public int Status
        {
            get { return status; }
            set { status = value; }
        }
        private int level;

        public int Level
        {
            get { return level; }
            set { level = value; }
        }

       
    }
}