﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using DMT.WS.Dto;


namespace DMT.WS.Utils
{
    public class CommonUtils
    {

        private const String NUMBER_FORMAT_1 = "{0:#,##}";


        public static byte[] getByteImage(String filePath)
        {


            if (File.Exists(filePath))
            {
                FileStream fs = new FileStream(filePath, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int length = (int)br.BaseStream.Length;
                byte[] m_byte = new byte[length];
                m_byte = br.ReadBytes(length);
                br.Close();
                fs.Close();
                return m_byte;
            }
            return null;

        }
        public static byte[] getByteImageBySource(String srcImage)
        {
            try
            {
                FileStream fs = new FileStream(srcImage, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int length = (int)br.BaseStream.Length;
                byte[] m_byte = new byte[length];
                m_byte = br.ReadBytes(length);
                br.Close();
                fs.Close();
                return m_byte;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        public static DateTime customConvert2Date(String tmp)
        {
            //20131205-232356
            int y = Convert.ToInt16(tmp.Substring(0, 4));
            int m = Convert.ToInt16(tmp.Substring(4, 2));
            int d = Convert.ToInt16(tmp.Substring(6, 2));
            int h = Convert.ToInt16(tmp.Substring(9, 2));
            int mm = Convert.ToInt16(tmp.Substring(11, 2));
            int s = Convert.ToInt16(tmp.Substring(13, 2));
            return new DateTime(y,m,d,h,mm,s);
        }
        public static Boolean IsNumeric(String tmp)
        {
            return string.IsNullOrEmpty(tmp) ? false :
                        Regex.IsMatch(tmp, @"^\s*\-?\d+(\.\d+)?\s*$");
        }


        public static String customNumberFormat(String _value)
        {

            if (_value == null) return "";
            if (_value == "null") return "";
            if (_value.Equals(String.Empty)) return "";
            return String.Format(CultureInfo.InvariantCulture,
                        NUMBER_FORMAT_1, Convert.ToDouble(_value));
        }
    }
}