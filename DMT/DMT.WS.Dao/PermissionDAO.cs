﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;

using DMT.WS.Dto;


namespace DMT.WS.Dao
{
    public class PermissionDAO : DefaultDAO
    {
        public override DefaultDTO save(DefaultDTO permissionDTO)
        {
                return null;
        }

        public override DefaultDTO getByPrimaryKey(Dictionary<string, string> pks)
        {
            if (!(pks.Count == 1 && pks.ContainsKey("PermissionCode")))
                throw new Exception("PermissionDAO getByPrimaryKey need only PermissionCode.");

            string permissionCode = toStr(pks["PermissionCode"]);
            return getByPrimaryKey(permissionCode);
        }

        public DefaultDTO getByPrimaryKey(string permissionCode)
        {
            string sql = "SELECT * FROM Permission WHERE PermissionCode = @PermissionCode";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@PermissionCode", System.Data.SqlDbType.VarChar).Value = permissionCode;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            PermissionDTO permissionDTO = null;
            if (data.Read())
            {
                permissionDTO = new PermissionDTO();
                permissionDTO.PermissionCode = toStr(data["PermissionCode"]);
                permissionDTO.PermissionGroup = toStr(data["PermissionGroup"]);
                permissionDTO.Name = toStr(data["Name"]);
                permissionDTO.Description = toStr(data["Description"]);
            }
            connection.CloseConnection();
            return permissionDTO;
        }

        public List<PermissionDTO> getAll()
        {
            string sql = "SELECT * FROM Permission ORDER BY PermissionGroup";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);


            List<PermissionDTO> permissions = new List<PermissionDTO>();
            while (data.Read())
            {
                PermissionDTO permissionDTO = permissionDTO = new PermissionDTO();
                permissionDTO.PermissionCode = toStr(data["PermissionCode"]);
                permissionDTO.PermissionGroup = toStr(data["PermissionGroup"]);
                permissionDTO.Name = toStr(data["Name"]);
                permissionDTO.Description = toStr(data["Description"]);
                permissions.Add(permissionDTO);
            }
            connection.CloseConnection();
            return permissions;
        }

        public List<PermissionDTO> getAll(string permissionGroup)
        {
            string sql = "SELECT * FROM Permission WHERE PermissionGroup = @PermissionGroup";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@PermissionGroup", System.Data.SqlDbType.VarChar).Value = permissionGroup;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);


            List<PermissionDTO> permissions = new List<PermissionDTO>();
            while (data.Read())
            {
                PermissionDTO permissionDTO = permissionDTO = new PermissionDTO();
                permissionDTO.PermissionCode = toStr(data["PermissionCode"]);
                permissionDTO.PermissionGroup = toStr(data["PermissionGroup"]);
                permissionDTO.Name = toStr(data["Name"]);
                permissionDTO.Description = toStr(data["Description"]);
                permissions.Add(permissionDTO);
            }
            connection.CloseConnection();
            return permissions;
        }

        public override bool update(DefaultDTO permissionDTO)
        {
            return false;
        }

        public override bool delete(DefaultDTO permissionDTO)
        {
            return false;
        }
    }
}