﻿using System;
using System.Diagnostics;

namespace DMT.WS.Dao
{
    public abstract class DefaultException : Exception
    {
        protected int level = 0;    // 0-5 : 0 is minimum priority

        public int getExceptionLevel()
        {
            return this.level;
        }

        public void writeLog()
        {
            //TODO: check server config before decide to write log.
            Debug.WriteLine(this.Message);
            Debug.WriteLine(this.StackTrace);
        }
    }
}