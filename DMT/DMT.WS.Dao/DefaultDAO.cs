﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DMT.WS.Dto;
using DMT.WS.Utils;


namespace DMT.WS.Dao
{
    public abstract class DefaultDAO
    {
        abstract public DefaultDTO save(DefaultDTO dto);
        abstract public DefaultDTO getByPrimaryKey(Dictionary<string, string> pks);
        abstract public bool update(DefaultDTO dto);
        abstract public bool delete(DefaultDTO dto);

        protected int toInt(object dbobj)
        {
            return Gm.toSafeInt(dbobj);
        }

        protected double toDouble(object dbobj)
        {
            return Gm.toSafeDouble(dbobj);
        }

        protected string toStr(object dbobj)
        {
            return Gm.toSafeString(dbobj);
        }

        protected DateTime toDateTime(object dbobj)
        {
            return Gm.toSafeDateTime(dbobj);
        }

        protected bool toBoolean(object dbobj)
        {
            return Gm.toSafeBoolean(dbobj);
        }
    }
}