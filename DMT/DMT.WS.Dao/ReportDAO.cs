﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using DMT.WS.Dto;


namespace DMT.WS.Dao
{
    public class ReportDAO : DefaultDAO
    {

        public override DefaultDTO save(DefaultDTO reportDTO)
        {
            if (!(reportDTO is ReportDTO))
                throw new WrongTypeException(this, "ReportDTO", reportDTO);

            string insertSQL = @"INSERT INTO [Report]
                                   ([Name]
                                   ,[Description]
                                   ,[FilePath])
                                VALUES
                                   (@Name
                                   ,@Description
                                   ,@FilePath)";

            ReportDTO dto = reportDTO as ReportDTO;

            bool result = false;
            SqlCommand cmd = new SqlCommand(insertSQL);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@Name", System.Data.SqlDbType.NVarChar).Value = dto.Name;
            cmd.Parameters.Add("@Description", System.Data.SqlDbType.NVarChar).Value = dto.Description;
            cmd.Parameters.Add("@FilePath", System.Data.SqlDbType.NVarChar).Value = dto.FilePath;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                result = true;
            }
            connection.CloseConnection();

            if (result)
                return dto;
            else
                return null;
        }

        public override DefaultDTO getByPrimaryKey(Dictionary<string, string> pks)
        {
            if (!(pks.Count == 1 && pks.ContainsKey("ID")))
                throw new Exception("ReportDAO getByPrimaryKey need only ID.");

            int id = toInt(pks["ID"]);
            return getByPrimaryKey(id);
        }

        public DefaultDTO getByPrimaryKey(int id)
        {
            string sql = "SELECT * FROM Report WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            ReportDTO reportDTO = null;
            if (data.Read())
            {
                reportDTO = new ReportDTO();
                reportDTO.ID = toInt(data["ID"]);
                reportDTO.Name = toStr(data["Name"]);
                reportDTO.Description = toStr(data["Description"]);
                reportDTO.FilePath = toStr(data["FilePath"]);
            }
            connection.CloseConnection();
            return reportDTO;
        }
        public override bool update(DefaultDTO reportDTO)
        {
            if (!(reportDTO is ReportDTO))
                throw new WrongTypeException(this, "ReportDTO", reportDTO);

            ReportDTO dto = reportDTO as ReportDTO;

            bool updateResult = false;
            if (dto != null && dto.ID > 0)
            {
                string sql = "UPDATE Report SET [Name] = @Name, " +
                    "[Description] = @Description, " +
                    "[FilePath] = @FilePath " +
                    "WHERE ID = @id";
                SqlCommand cmd = new SqlCommand(sql);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = dto.ID;
                cmd.Parameters.Add("@Name", System.Data.SqlDbType.NVarChar).Value = dto.Name;
                cmd.Parameters.Add("@Description", System.Data.SqlDbType.NVarChar).Value = dto.Description;
                cmd.Parameters.Add("@FilePath", System.Data.SqlDbType.NVarChar).Value = dto.FilePath;
                ConnectionDB connection = new ConnectionDB();
                connection.OpenConnection();
                int row = connection.ExecuteNonQuery(cmd);

                if (row > 0)
                {
                    updateResult = true;
                }
                connection.CloseConnection();
            }
            return updateResult;
        }

        public override bool delete(DefaultDTO reportDTO)
        {
            if (!(reportDTO is ReportDTO))
                throw new WrongTypeException(this, "ReportDTO", reportDTO);

            ReportDTO dto = reportDTO as ReportDTO;
            int id = dto.ID;

            return deleteById(id);
        }


        public bool deleteById(int id)
        {
            bool deleteResult = false;
            string sql = "DELETE Report WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                deleteResult = true;
            }
            connection.CloseConnection();
            return deleteResult;
        }

        public List<ReportDTO> getAll()
        {
            string sql = "SELECT * FROM Report";
            SqlCommand cmd = new SqlCommand(sql);

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            List<ReportDTO> reports = new List<ReportDTO>();

            while (data.Read())
            {
                ReportDTO reportDTO = new ReportDTO();
                reportDTO.ID = toInt(data["ID"]);
                reportDTO.Name = toStr(data["Name"]);
                reportDTO.Description = toStr(data["Description"]);
                reportDTO.FilePath = toStr(data["FilePath"]);
                reports.Add(reportDTO);
            }
            connection.CloseConnection();
            return reports;
            
        }
    }
}