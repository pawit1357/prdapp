﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using DMT.WS.Dao;
using DMT.WS.Dto;



namespace DMT.WS.Dao
{
    public class UserPermissionDAO : DefaultDAO
    {
        public override DefaultDTO save(DefaultDTO userPermissionDTO)
        {
            if (!(userPermissionDTO is UserPermissionDTO))
                throw new WrongTypeException(this, "UserPermissionDTO", userPermissionDTO);

            string insertSQL = @"INSERT INTO [UserPermission]
                                   ([UserTypeID]
                                   ,[PermissionCode])
                                VALUES
                                   (@UserTypeID
                                   ,@PermissionCode)";

            UserPermissionDTO dto = userPermissionDTO as UserPermissionDTO;

            bool result = false;
            SqlCommand cmd = new SqlCommand(insertSQL);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@UserTypeID", System.Data.SqlDbType.Int).Value = dto.UserTypeId;
            cmd.Parameters.Add("@PermissionCode", System.Data.SqlDbType.VarChar).Value = dto.PermissionCode;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                result = true;
            }
            connection.CloseConnection();

            if (result)
                return dto;
            else
                return null;
        }

        public override DefaultDTO getByPrimaryKey(Dictionary<string, string> pks)
        {
            if (!(pks.Count == 1 && pks.ContainsKey("ID")))
                throw new Exception("UserPermissionDAO getByPrimaryKey need only ID.");

            int id = toInt(pks["ID"]);
            return getByPrimaryKey(id);
        }

        public DefaultDTO getByPrimaryKey(int id)
        {
            string sql = "SELECT * FROM UserPermission WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            UserPermissionDTO userPermissionDTO = null;
            if (data.Read())
            {
                userPermissionDTO = new UserPermissionDTO();
                userPermissionDTO.Id = toInt(data["ID"]);
                userPermissionDTO.UserTypeId = toInt(data["UserTypeID"]);
                userPermissionDTO.PermissionCode = toStr(data["PermissionCode"]);
            }
            connection.CloseConnection();
            return userPermissionDTO;
        }

        public List<UserPermissionDTO> getByUserTypeId(int userTypeId)
        {
            string sql = "SELECT * FROM UserPermission WHERE UserTypeID = @UserTypeID";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@UserTypeID", System.Data.SqlDbType.Int).Value = userTypeId;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);
            List<UserPermissionDTO> userPermissionDTOs = new List<UserPermissionDTO>();
            while (data.Read())
            {
                UserPermissionDTO userPermissionDTO = new UserPermissionDTO();
                userPermissionDTO.Id = toInt(data["ID"]);
                userPermissionDTO.UserTypeId = toInt(data["UserTypeID"]);
                userPermissionDTO.PermissionCode = toStr(data["PermissionCode"]);
                userPermissionDTOs.Add(userPermissionDTO);
            }
            connection.CloseConnection();
            return userPermissionDTOs;
        }

        public UserPermissionDTO getByUserTypeIdPermissionCode(int userTypeId, string permissionCode)
        {
            string sql = "SELECT * FROM UserPermission WHERE UserTypeID = @UserTypeID AND PermissionCode = @PermissionCode";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@UserTypeID", System.Data.SqlDbType.Int).Value = userTypeId;
            cmd.Parameters.Add("@PermissionCode", System.Data.SqlDbType.VarChar).Value = permissionCode;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);
            UserPermissionDTO userPermissionDTO = null;
            if (data.Read())
            {
                userPermissionDTO = new UserPermissionDTO();
                userPermissionDTO.Id = toInt(data["ID"]);
                userPermissionDTO.UserTypeId = toInt(data["UserTypeID"]);
                userPermissionDTO.PermissionCode = toStr(data["PermissionCode"]);
            }
            connection.CloseConnection();
            return userPermissionDTO;
        }


        public override bool update(DefaultDTO userPermissionDTO)
        {
            return false;
        }

        public override bool delete(DefaultDTO userPermissionDTO)
        {
            if (!(userPermissionDTO is UserPermissionDTO))
                throw new WrongTypeException(this, "UserPermissionDTO", userPermissionDTO);

            UserPermissionDTO dto = userPermissionDTO as UserPermissionDTO;
            int id = dto.Id;

            return deleteById(id);
        }


        public bool deleteById(int id)
        {
            bool deleteResult = false;
            string sql = "DELETE UserPermission WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                deleteResult = true;
            }
            connection.CloseConnection();
            return deleteResult;
        }

        public bool deleteByUserType(int userTypeId)
        {
            bool deleteResult = false;
            string sql = "DELETE FROM UserPermission WHERE UserTypeID = @UserTypeID";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@UserTypeID", System.Data.SqlDbType.Int).Value = userTypeId;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                deleteResult = true;
            }
            connection.CloseConnection();
            return deleteResult;
        }
    }
}