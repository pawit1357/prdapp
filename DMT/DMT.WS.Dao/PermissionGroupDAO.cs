﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;

using DMT.WS.Dto;


namespace DMT.WS.Dao
{
    public class PermissionGroupDAO : DefaultDAO
    {
        public override DefaultDTO save(DefaultDTO permissionGroupDTO)
        {
                return null;
        }

        public override DefaultDTO getByPrimaryKey(Dictionary<string, string> pks)
        {
            if (!(pks.Count == 1 && pks.ContainsKey("GroupCode")))
                throw new Exception("PermissionGroupDAO getByPrimaryKey need only GroupCode.");

            string permissionCode = toStr(pks["PermissionCode"]);
            return getByPrimaryKey(permissionCode);
        }

        public DefaultDTO getByPrimaryKey(string groupCode)
        {
            string sql = "SELECT * FROM PermissionGroup WHERE GroupCode = @GroupCode";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@GroupCode", System.Data.SqlDbType.VarChar).Value = groupCode;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            PermissionGroupDTO permissionGroupDTO = null;
            if (data.Read())
            {
                permissionGroupDTO = new PermissionGroupDTO();
                permissionGroupDTO.GroupCode = toStr(data["GroupCode"]);
                permissionGroupDTO.Name = toStr(data["Name"]);
                permissionGroupDTO.Description = toStr(data["Description"]);
            }
            connection.CloseConnection();
            return permissionGroupDTO;
        }

        public List<PermissionGroupDTO> getAll()
        {
            string sql = "SELECT * FROM PermissionGroup";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);


            List<PermissionGroupDTO> permissions = new List<PermissionGroupDTO>();
            while (data.Read())
            {
                PermissionGroupDTO permissionGroupDTO = permissionGroupDTO = new PermissionGroupDTO();
                permissionGroupDTO.GroupCode = toStr(data["GroupCode"]);
                permissionGroupDTO.Name = toStr(data["Name"]);
                permissionGroupDTO.Description = toStr(data["Description"]);
                permissions.Add(permissionGroupDTO);
            }
            connection.CloseConnection();
            return permissions;
        }

        public override bool update(DefaultDTO permissionGroupDTO)
        {
            return false;
        }

        public override bool delete(DefaultDTO permissionGroupDTO)
        {
            return false;
        }
    }
}