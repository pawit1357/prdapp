package dom.doh.virsualwim.model;

public class Config {
	
	private int id;
	private String lane;
	private String stationPermission;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLane() {
		return lane;
	}
	public void setLane(String lane) {
		this.lane = lane;
	}
	
	public String getStationPermission() {
		return stationPermission;
	}
	public void setStationPermission(String stationPermission) {
		this.stationPermission = stationPermission;
	}
	
	public Config(){}
	
	public Config(int id, String lane, String stationPermission) {
		super();
		this.id = id;
		this.lane = lane;
		this.stationPermission = stationPermission;
	}
	
	
}
