package dom.doh.virsualwim.model;

import java.util.HashMap;
import java.util.Iterator;



public class Station  extends HashMap<String, String>  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static String KEY_ID = "stationId";
	public static String KEY_STATION = "stationName";
	// http://sunil-android.blogspot.com/2013/07/fragment-list-with-image-and-text-with.html
	public int stationId;
	public String stationName;
	public String stationGroupId;

	public Station() {

	}

	public Station(int stationId, String stationName, String stationGroupId) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.stationGroupId = stationGroupId;
	}

	public int getStationId() {
		return stationId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	@Override
	public String toString() {
		return stationName;
	}

	public String getStationGroupId() {
		return stationGroupId;
	}

	public void setStationGroupId(String stationGroupId) {
		this.stationGroupId = stationGroupId;
	}

	public Iterator<Station> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	public String get(Object k) {
		String key = (String) k;
		if (KEY_ID.equals(key))
			return stationId + "";
		else if (KEY_STATION.equals(key))
			return stationName;
		return null;
	}

}
