package com.doh.virsualwim;

import java.util.ArrayList;

import com.doh.virsualwim.utils.ImageLoader;
import com.doh.virsualwim.utils.Util;

import dom.doh.virsualwim.model.Wim;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WimAdapter extends BaseAdapter {

	private Context activity;
	private ArrayList<Wim> wimList;
	private static LayoutInflater inflater = null;
	public ImageLoader imageLoader;

	public WimAdapter(Context a, ArrayList<Wim> _wimList) {
		if (a != null) {
			activity = a;
			wimList = _wimList;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			imageLoader = new ImageLoader(activity.getApplicationContext());
		}
	}

	public int getCount() {
		return (wimList != null) ? wimList.size() : 0;
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.wim_row_item, null);

		int  fSize = Util.getFrontSize(this.activity);
		
		TextView txtStation = (TextView) vi.findViewById(R.id.txtStation);
		txtStation.setTextSize(fSize);
		TextView txtVehicleNum = (TextView) vi.findViewById(R.id.txtVehicleNum);
		txtVehicleNum.setTextSize(fSize);
		TextView txtLane = (TextView) vi.findViewById(R.id.txtLane);
		txtLane.setTextSize(fSize);
		TextView txtClass = (TextView) vi.findViewById(R.id.txtClass);
		txtClass.setTextSize(fSize);
		
		TextView txtSort = (TextView) vi.findViewById(R.id.txtSort);
		txtSort.setTextSize(fSize);
		TextView txtMax = (TextView) vi.findViewById(R.id.txtMax);
		txtMax.setTextSize(fSize);
		TextView txtGvw = (TextView) vi.findViewById(R.id.txtGvw);
		txtGvw.setTextSize(fSize);
		
		TextView txtDate = (TextView) vi.findViewById(R.id.txtDate);
		txtDate.setTextSize(fSize-2);
		TextView txtLPlateNum = (TextView) vi.findViewById(R.id.txtLPlateNum);
		txtLPlateNum.setTextSize(fSize);
		TextView txtLPlateProvince = (TextView) vi
				.findViewById(R.id.txtLPlateProvince);
		txtLPlateProvince.setTextSize(fSize);

		ImageView imageView = (ImageView) vi.findViewById(R.id.list_image);
		ImageView imageView1 = (ImageView) vi.findViewById(R.id.list_image2);
		// ImageView imgStatus = (ImageView) vi.findViewById(R.id.imgStatus);

		Wim wim = wimList.get(position);
		txtStation.setText(""+wim.getStationName());

		txtVehicleNum.setText(""+wim.getVehicleNumber());
		txtLane.setText("Lane:\t"+wim.getLane());
		txtClass.setText("Class:\t"+wim.getVehicleClass());

		txtSort.setText("Sort:\t"+wim.getSortDecision());
		txtMax.setText("Max GVW:\t"+wim.getMaxGVW());
		txtGvw.setText("GVW:\t"+wim.getGVW());

		txtDate.setText(""+wim.getTimeStamp().toString());

		txtLPlateNum.setText("\t"+wim.getLicensePlateNumber());
		txtLPlateProvince.setText("\t"+wim.getProvinceName());

		imageLoader.DisplayImage(wim.getImage01Name(), imageView);
		imageLoader.DisplayImage(wim.getImage02Name(), imageView1);

		LinearLayout ll = (LinearLayout) vi.findViewById(R.id.thumbnail);
		if (wim.getStatusColor() != null) {

			if (wim.getStatusColor().equals("Color [LawnGreen]")) {
				ll.setBackgroundColor(Color.parseColor("#FAFAFA"));
			} else {
				ll.setBackgroundColor(Color.parseColor("#F6E3CE"));
			}
		} else {
			ll.setBackgroundColor(Color.parseColor("#F6E3CE"));
		}
		return vi;
	}
}