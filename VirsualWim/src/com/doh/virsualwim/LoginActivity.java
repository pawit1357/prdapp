package com.doh.virsualwim;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.doh.virsualwim.db.DatabaseHandler;
import com.doh.virsualwim.utils.WebserviceUtil;

import dom.doh.virsualwim.model.Config;
import dom.doh.virsualwim.model.Station;

public class LoginActivity extends Activity {

	private EditText username = null;
	private EditText password = null;
	// private TextView attempts;
	private Button login;
	// int counter = 3;

	private DatabaseHandler db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		username = (EditText) findViewById(R.id.editText1);
		password = (EditText) findViewById(R.id.editText2);
		// attempts = (TextView) findViewById(R.id.textView5);
		// attempts.setText(Integer.toString(counter));
		login = (Button) findViewById(R.id.button1);

		db = DatabaseHandler.getInstance(this);

	}

	public void login(View view) {

		if (!username.getText().toString().equals("")
				&& !password.getText().toString().equals("")) {
			Toast.makeText(getApplicationContext(), "Redirecting...",
					Toast.LENGTH_SHORT).show();

			new LoginTask(this, username.getText().toString(), password
					.getText().toString()).execute();

			// Toast.makeText(getApplicationContext(), "Wrong Credentials",
			// Toast.LENGTH_SHORT).show();

		} else {
			Toast.makeText(getApplicationContext(), "Wrong Credentials",
					Toast.LENGTH_SHORT).show();
			/*
			 * attempts.setBackgroundColor(Color.RED); counter--;
			 * attempts.setText(Integer.toString(counter)); if (counter == 0) {
			 * login.setEnabled(false); }
			 */
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private class LoginTask extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		Context context;
		String data = "";
		String serverText = "";

		String user = "";
		String pass = "";
		ProgressDialog Dialog;

		public LoginTask(Context _context, String _user, String _pass) {
			this.user = _user;
			this.pass = _pass;
			context = _context;
			Dialog = new ProgressDialog(_context);
		}

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			Dialog.setMessage("Please wait..");
			Dialog.show();
			try {
				// Set Request parameter
				data += "&" + URLEncoder.encode("data", "UTF-8") + "="
						+ serverText;

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(String.format(WebserviceUtil.getUrlLogin(),
						user, pass));

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(data);
				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "");
				}

				// Append Server Response To Content String
				Content = sb.toString();
			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {

				}
			}
			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			Dialog.dismiss();
			if (Error != null) {
				Log.d("Error", Error);
				Toast.makeText(getApplicationContext(), "Wrong Credentials",
						Toast.LENGTH_SHORT).show();
			} else {
				Log.d("Content", Content);
				/****************** Start Parse Response JSON Data *************/
				JSONArray jsonArr;

				// Initial station list.
				try {

					jsonArr = new JSONArray(Content);
					if (jsonArr != null) {
						if (jsonArr.length() > 0) {
							Config config = db.getConfig(1);
							String viewStation ="";
							// Clear all station data.
							int size = db.getStationCount();
							db.deleteAllStation();
							db.deleteAllWim();
							size = db.getStationCount();
							for (int i = 0; i < jsonArr.length(); i++) {

								JSONObject data = jsonArr.getJSONObject(i);

								JSONArray stations = (JSONArray) data.get("stations");
								
								for (int ii = 0; ii < stations.length(); ii++) {
									Station station = new Station();
									JSONObject _data = stations
											.getJSONObject(ii);
									
									station.setStationId(Integer.parseInt(_data
											.getString("StationID")));
									station.setStationName(_data
											.getString("StationName"));
									station.setStationGroupId(_data
											.getString("StationGroupID"));
									viewStation+=station.getStationId()+",";
									db.addStation(station);
								}
							}
							config.setStationPermission(viewStation);
							db.updateConfig(config);


							// / -----------
							Intent i = new Intent(LoginActivity.this,
									ItemListActivity.class);
							startActivity(i);

							// close this activity
							finish();

						} else {
							// Log.d("Error", Error);
							Toast.makeText(getApplicationContext(),
									"Wrong Credentials", Toast.LENGTH_SHORT)
									.show();
						}
					} else {
						// Log.d("Error", Error);
						Toast.makeText(getApplicationContext(),
								"Wrong Credentials", Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
}