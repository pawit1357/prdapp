﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using System.Data;
using DOH.DC.Model;
using DOH.DC.Utils;


namespace DOH.VWS.Biz
{
    public class UserLoginDao
    {

        public UserLogin getUserLoginByUsernameAndPassword(string username, string password)
        {
            UserLogin user = null;
            using (SqlConnection connection = new SqlConnection(GlobalConfiguration.ConnectionString))
            {
                connection.Open();
                string sql = "SELECT * FROM Users WHERE LoginID = @username and Password = @password and Active=1 and userGroupId=1";
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = username;
                cmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = password;

                SqlDataReader data = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                if (data.Read())
                {
                    user = new UserLogin();
                    user.ID = Convert.ToInt16(data["UserID"]);
                    user.Username = data["LoginID"].ToString();
                    user.Password = data["Password"].ToString();
                    //user.Email = data["Email"].ToString();
                    user.FullName = data["FirstName"].ToString() + " " + data["LastName"].ToString() + " " + data["Title"].ToString();
                    //user.Status = Convert.ToInt16(data["Status"]);
                    //user.Level = Convert.ToInt16(data["UserLevel"]);
                }
            }
            return user;
        }




        //public UserLoginBean updateByPk(int id, string password, string email, string fullName)
        //{
        //    UserLoginBean bean = null;
        //    UserLoginDAO dao = new UserLoginDAO();
        //    UserLoginDTO dto = (UserLoginDTO)dao.getByPrimaryKey(id);
        //    if (dto != null)
        //    {
        //        if (password != null && password != "")
        //        {
        //            dto.Password = password;
        //        }
        //        dto.Email = email;
        //        dto.FullName = fullName;
        //        if (dao.update(dto))
        //        {
        //            bean = new UserLoginBean();
        //            bean.ID = dto.ID;
        //            bean.Username = dto.Username;
        //            bean.Password = dto.Password;
        //            bean.Email = dto.Email;
        //            bean.FullName = dto.FullName;
        //            bean.Status = dto.Status;
        //            bean.Level = dto.Level;
        //        }                
        //    }
        //    return bean;
        //}

        //public DataSet LoadUsers()
        //{
        //    DataSet ds = new DataSet();
        //    using (SqlConnection connection = new SqlConnection(conStr))
        //    {
        //        connection.Open();
        //        string sql = "SELECT [ID],[UserName],[FullName], [Email], "
        //                    + "'StatusName' = CASE [Status] WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' ELSE '' END, "
        //                    + "'UserLevelName' = CASE [UserLevel] WHEN 1 THEN 'Admin' WHEN 2 THEN 'Manager' ELSE 'Staff' END "
        //                    + "FROM [UserLogin] "
        //                    + "WHERE [Status] IN (1,0) "
        //                    + "ORDER BY UserName";
        //        SqlCommand cmd = new SqlCommand(sql, connection);
        //        cmd.CommandType = System.Data.CommandType.Text;
        //        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
        //        adpt.Fill(ds);
        //    }
        //    return ds;

        //}
    }
}