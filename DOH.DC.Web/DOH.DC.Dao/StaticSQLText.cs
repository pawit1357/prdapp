﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DOH.DC.Dao
{
    public class StaticSQLText
    {
        public static string sqlWeighing = "SELECT dbo.Weighing.ENFID,   " +
                             "dbo.Weighing.StationID,   " +
                             "dbo.Weighing.SeqNumber,   " +
                             "dbo.Weighing.WIMID,   " +
                             "dbo.Weighing.TimeStamp,   " +
                             "dbo.Weighing.VehicleClassID,  " +
                             "dbo.Weighing.LPR_Number,  " +
                             "dbo.Weighing.LPR_ProvinceID,   " +
                             "dbo.Weighing.LPR_ImageName,   " +
                             "dbo.Weighing.Material,   " +
                             "dbo.Weighing.DepartedFrom,   " +
                             "dbo.Weighing.Destination,   " +
                             "dbo.Weighing.Notes,   " +
                             "dbo.Weighing.ComplyMethod,   " +
                             "dbo.Weighing.SaveMethod,   " +
                             "dbo.Weighing.AG1_Weight_Max,   " +
                             "dbo.Weighing.AG2_Weight_Max,   " +
                             "dbo.Weighing.AG3_Weight_Max,   " +
                             "dbo.Weighing.GVW_Weight_Max,   " +
                             "dbo.Weighing.AG1_Weight_Measured,  " +
                             "dbo.Weighing.AG2_Weight_Measured,   " +
                             "dbo.Weighing.AG3_Weight_Measured,   " +
                             "dbo.Weighing.GVW_Weight_Measured,   " +
                             "dbo.Weighing.AG1_Weight_Over,  " +
                             "dbo.Weighing.AG2_Weight_Over,  " +
                             "dbo.Weighing.AG3_Weight_Over,  " +
                             "dbo.Weighing.GVW_Weight_Over,  " +
                             "dbo.Weighing.GVW_Weight_WIM,  " +
                             "dbo.Weighing.IsOverWeight,  " +
                             "dbo.Weighing.UserID,  " +
                             "dbo.Weighing.VehicleImageName,  " +
                             "dbo.Provinces.ProvinceName,  " +
                             "dbo.Users.FirstName,   " +
                             "dbo.Users.LastName,  " +
                             "dbo.Users.ShiftID,  " +
                             "dbo.Shift.ShiftDescription,  " +
                             "dbo.VehicleClass.VehicleClassDescription  " +
                             "FROM dbo.Weighing   " +
                             "INNER JOIN dbo.Provinces ON dbo.Provinces.ProvinceID = dbo.Weighing.LPR_ProvinceID   " +                            
                             "INNER JOIN dbo.VehicleClass ON dbo.VehicleClass.VehicleClassID = dbo.Weighing.VehicleClassID  " +
                             "INNER JOIN dbo.Users ON dbo.Users.UserID = dbo.Weighing.UserID   " +
                             "INNER JOIN dbo.Shift ON dbo.Shift.ShiftID = dbo.Users.ShiftID ";


        public static string SqlMappedOldSchema = "" + "DECLARE @img1 AS VARBINARY(MAX) " +
                "DECLARE @img2 AS VARBINARY(MAX) " +
                "SELECT dbo.Weighing.ENFID  AS fldTransID,  " +
                "dbo.Weighing.StationID  AS fldStationID, " +
                "CONVERT(NVARCHAR(20), dbo.Weighing.SeqNumber)  AS fldRefID,  " +
                "dbo.Weighing.WIMID  AS fldWIMID, " +
                "dbo.Weighing.TimeStamp  AS fldTransDateTime,  " +
                "CONVERT(NVARCHAR(20), dbo.Weighing.VehicleClassID) AS fldTruckClass,  " +
                "dbo.Weighing.LPR_Number AS fldLicenceNumber,  " +
            //"dbo.Weighing.LPR_ProvinceID  AS fldLPR_ProvinceID,  " +
                "dbo.Weighing.LPR_ImageName  AS fldLPRImagePath,  " +
                "dbo.Weighing.Material  AS fldMaterial,  " +
                "dbo.Weighing.DepartedFrom  AS fldDepartedFrom,  " +
                "dbo.Weighing.Destination  AS fldDestination,  " +
                "dbo.Weighing.Notes  AS fldNotes,  " +
                "CONVERT(NVARCHAR(20), dbo.Weighing.ComplyMethod) AS fldComplyMethod,  " +
                "CONVERT(NVARCHAR(20), dbo.Weighing.SaveMethod) AS fldSaveMethod,  " +
                "dbo.Weighing.AG1_Weight_Max  AS fldLimitedWeightGroup1,  " +
                "dbo.Weighing.AG2_Weight_Max  AS fldLimitedWeightGroup2,  " +
                "dbo.Weighing.AG3_Weight_Max  AS fldLimitedWeightGroup3, " +
                "dbo.Weighing.GVW_Weight_Max  AS fldLimitedGVW,  " +
                "dbo.Weighing.AG1_Weight_Measured AS fldWeightGroup1,  " +
                "dbo.Weighing.AG2_Weight_Measured  AS fldWeightGroup2,  " +
                "dbo.Weighing.AG3_Weight_Measured  AS fldWeightGroup3,  " +
                "dbo.Weighing.GVW_Weight_Measured  AS fldWeightGVW,  " +
                "dbo.Weighing.AG1_Weight_Over AS fldWeightOver1,  " +
                "dbo.Weighing.AG2_Weight_Over AS fldWeightOver2,  " +
                "dbo.Weighing.AG3_Weight_Over AS fldWeightOver3,  " +
                "dbo.Weighing.GVW_Weight_Over AS fldWeightOverGVW, " +
                "dbo.Weighing.GVW_Weight_WIM AS fldGVW_Weight_WIM,  " +
                "CONVERT(NVARCHAR(20), dbo.Weighing.IsOverWeight) AS fldIsOverWeight,  " +
            //"dbo.Weighing.UserID AS fldUserID,  " +
                "dbo.Weighing.VehicleImageName AS fldTruckImagePath,  " +
                "dbo.Provinces.ProvinceName AS fldProvince,  " +
                "(dbo.Users.FirstName + ' ' + dbo.Users.LastName) AS fldOperatorName,  " +
            //"dbo.Users.LastName AS fldUserLastName,  " +
                "dbo.Users.ShiftID AS fldPeriodID,  " +
                "dbo.Shift.ShiftDescription AS fldTeamName,  " +
                "dbo.VehicleClass.VehicleClassDescription AS fldTruckDescription,  " +
                "@img1  AS fldTruckImage,  " +
                "@img2  AS fldClassImage  " +
            //"@img  AS fldLPRImage  " +
                "FROM dbo.Weighing " +
                "INNER JOIN dbo.Provinces ON dbo.Provinces.ProvinceID = dbo.Weighing.LPR_ProvinceID " +
                "INNER JOIN dbo.VehicleClass ON dbo.VehicleClass.VehicleClassID = dbo.Weighing.VehicleClassID " +
                "INNER JOIN dbo.Users ON dbo.Users.UserID = dbo.Weighing.UserID " +
                "INNER JOIN dbo.Shift ON dbo.Shift.ShiftID = dbo.Users.ShiftID ";
        
    }
}
