﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DOH.DC.Utils;
using System.Data;
using DOH.DC.Model;

namespace DOH.DC.Dao
{
    public class ClassDao
    {
        public List<VehicleClass> getAll()
        {
            using (SqlConnection connection = new SqlConnection(GlobalConfiguration.ConnectionString))
            {
                connection.Open();
                string sql = "SELECT * FROM VehicleClass where WIMClassID>0";
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlDataReader data = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                List<VehicleClass> tmp = new List<VehicleClass>();

                while (data.Read())
                {
                    VehicleClass model = new VehicleClass();
                    model.WIMClassID = Convert.ToInt16(data["WIMClassID"]);
                    model.VehicleClassDescription = data["WIMClassID"].ToString();
                    tmp.Add(model);
                }
                return tmp;
            }

        }
    }
}
