﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DOH.DC.Utils;
using System.Data;
using DOH.DC.Model;

namespace DOH.DC.Dao
{
    public class ProvinceDao
    {
        public List<Province> getAll()
        {
            using (SqlConnection connection = new SqlConnection(GlobalConfiguration.ConnectionString))
            {
                connection.Open();
                string sql = "SELECT * FROM Provinces";
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlDataReader data = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                List<Province> tmp = new List<Province>();

                while (data.Read())
                {
                    Province model = new Province();
                    model.ProvinceID = Convert.ToInt16(data["ProvinceID"]);
                    model.ProvinceName = data["ProvinceName"].ToString();
                    tmp.Add(model);
                }
                return tmp;
            }

        }
    }
}
