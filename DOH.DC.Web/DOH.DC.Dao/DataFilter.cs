﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DOH.DC.Dao
{
    public class DataFilter
    {
        public bool hasStarDate { get; set; }
        public bool hasEndDate { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string dateTimeText { get; set; }
        public string summaryType { get; set; }

        public string stationName { get; set; }
        public int stationID { get; set; }

        public int WarningType { get; set; }

        public DataFilter()
        {
            hasStarDate = false;
            hasEndDate = false;
            startDate = DateTime.Now;
            endDate = DateTime.Now;
            dateTimeText = string.Empty;
            summaryType = string.Empty;
            stationName = string.Empty;
            stationID = 0;
        }
    }
}
