﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DOH.DC.Utils;
using System.IO;
using System.Web;
//using System.Drawing;
//using System.Windows.Forms;


namespace DOH.DC.Dao
{
    public class Util
    {
        public static DataTable GetDataTable(string SQLText)
        {
            DataTable table = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(GlobalConfiguration.ConnectionString))
                {
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(SQLText, connection))
                    {
                        adapter.Fill(table);
                    }
                }
            }
            catch (Exception ex)
            {
                //Messaging.LogAndShow("GetDataTable", "Exception_GetDataTable: " + 
                //    "\r\n\r\n" + ex.Message + 
                  //  "\r\n\r\n" + AppParameters.ConnectionString +
                    //"\r\n\r\n" + SQLText);
            }
            GC.Collect();
            return table;
        }

        public static bool ExecuteSQLCommand(string SQLText, out object output)
        {
            bool success = true;
            try
            {
                using (SqlConnection connection = new SqlConnection(GlobalConfiguration.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(SQLText, connection))
                    {
                        output = command.ExecuteScalar();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
                output = ex;
            }
            GC.Collect();
            return success;
        }

        public static string GetImageFolder(string imageName)
        {
            string imageFolder = string.Empty;
            string appname = string.Empty;

            if (imageName.StartsWith("rsd"))
            {
                appname = "RSD";
            }
            else if (imageName.StartsWith("lpr"))
            {
                appname = "STATIC";
            }
            else if (imageName.StartsWith("wim"))
            {
                appname = "WIM";
            }

            if (imageName.EndsWith("jpg"))
            {
                if (imageName.Length > 15)
                {
                    string siteID = imageName.Substring(4, 2);
                    string y = imageName.Substring(7, 4);
                    string m = imageName.Substring(11, 2);
                    string d = imageName.Substring(13, 2);

                    string subFolder = siteID + @"\" + y + @"\" + m + @"\" + d;
                    imageFolder = String.Format(GlobalConfiguration.imageFilePath, siteID, y, m, d, appname);                                
                        //<add key="imageFilePath" value="C:\IMAGES\{0}\{1}\{2}\{3}\{4}" />
                    //imageFolder = AppSettings.DataLocation + @"\IMAGES\" + subFolder + @"\" + appname;
                }
            }

            return imageFolder;
        }

        public static string GetImageURL(string imageName)
        {
            string imageUrl = string.Empty;
            string appname = string.Empty;

            if (imageName.StartsWith("rsd"))
            {
                appname = "RSD";
            }
            else if (imageName.StartsWith("lpr"))
            {
                appname = "STATIC";
            }
            else if (imageName.StartsWith("wim"))
            {
                appname = "WIM";
            }

            if (imageName.EndsWith("jpg"))
            {
                if (imageName.Length > 15)
                {
                    string siteID = imageName.Substring(4, 2);
                    string y = imageName.Substring(7, 4);
                    string m = imageName.Substring(11, 2);
                    string d = imageName.Substring(13, 2);

                    string subFolder = siteID + @"/" + y + @"/" + m + @"/" + d;
                    imageUrl = String.Format(GlobalConfiguration.ImageWebPath, siteID, y, m, d, appname, imageName);       
                    //imageUrl = AppSettings.ImageURL + @"/" + subFolder + @"/" + appname + @"/" + imageName;
                }
            }

            return imageUrl;
        }

        public static string GetStatusCodeText(VRStatusCode status)
        {
            string text = string.Empty;
            foreach (VRStatusCode val in Enum.GetValues(typeof(VRStatusCode)))
            {
                if (val != VRStatusCode.StatusClear)
                {
                    if ((val & status) == val)
                    {
                        text += (text.Length > 0) ? "\r\n" : "";
                        text += val.ToString();
                    }
                }
            }
            return text;
        }

        public static string GetAxleGroupText(byte groupType)
        {
            if (groupType == 1)
            {
                return "single";
            }
            else if (groupType == 2)
            {
                return "tandem";
            }
            else if (groupType == 3)
            {
                return "tridem";
            }
            else if (groupType == 4)
            {
                return "quadrem";
            }
            else
            {
                return "";
            }
        }

        public static string GetLaneName(byte lane)
        {
            if (lane == 0)
            {
                return "WIM";
            }
            else if (lane == 1)
            {
                return "Report";
            }
            else if (lane == 2)
            {
                return "Bypass";
            }
            else
            {
                return "";
            }
        }

        //public static Color GetSortDecisionColorCode(VRSortDecisionCode sortDecision)
        //{
        //    Color green = Color.LawnGreen;
        //    Color red = Color.Crimson;
        //    Color gray = Color.Silver;
        //    Color orange = Color.Orange;

        //    if (sortDecision == VRSortDecisionCode.Bypass)
        //    {
        //        return green;
        //    }
        //    else if (sortDecision == VRSortDecisionCode.CredentialReport)
        //    {
        //        return red;
        //    }
        //    //else if (sortDecision == VRSortDecisionCode.None)
        //    //{
        //    //    return gray;
        //    //}
        //    else if (sortDecision == VRSortDecisionCode.Off)
        //    {
        //        return gray;
        //    }
        //    else if (sortDecision == VRSortDecisionCode.Report)
        //    {
        //        return red;
        //    }
        //    else if (sortDecision == VRSortDecisionCode.Sorting)
        //    {
        //        return orange;
        //    }
        //    else
        //    {
        //        return gray;
        //    }
        //}

        //public static Color GetIsOverWeightColorCode(string status)
        //{
        //    Color green = Color.LawnGreen;
        //    Color red = Color.Crimson;
        //    Color gray = Color.Silver;

        //    if (status.ToUpper() == "Y")
        //    {
        //        return red;
        //    }
        //    else if (status.ToUpper() == "N")
        //    {
        //        return green;
        //    }
        //    else
        //    {
        //        return gray;
        //    }

        //}

        //public static Color GetWarningTypeColorCode(int Type)
        //{
        //    Color avoiding = Color.Yellow;
        //    Color runningscale = Color.Orange;
        //    Color overweight = Color.Crimson;
        //    Color bgopened = Color.BlueViolet;

        //    if (Type == 1)
        //    {
        //        return avoiding;
        //    }
        //    else if (Type == 2)
        //    {
        //        return runningscale;
        //    }
        //    else if (Type == 3)
        //    {
        //        return overweight;
        //    }
        //    else if (Type == 4)
        //    {
        //        return bgopened;
        //    }
        //    else
        //    {
        //        return Color.Gray;
        //    }

        //}

        public static string GetStationName(int stationID)
        {
            string name = string.Empty;
            DataTable table = GetDataTable("SELECT * FROM Station");
            if (table.Rows.Count > 0)
            {
                foreach (DataRow r in table.Rows)
                {
                    if (stationID == (int)r["StationID"])
                    {
                        name = (string)r["StationName"];
                        break;
                    }
                }
            }
            return name;
        }

        public static string GetDateTimeString(DateTime date, DateTime time)
        {
            int y = date.Year;
            int M = date.Month;
            int d = date.Day;
            int h = time.Hour;
            int m = time.Minute;
            return new DateTime(y, M, d, h, m, 0).ToString("s");
        }
        public static string getReportTitle(int id)
        {
            string titleName = string.Empty;

            if (id == 101)
            {
                titleName = "แยกตามประเภทรถ";
            }
            else if (id == 102)
            {
                titleName = "แยกตามประเภทรถ และผลการคัดกรอง";
            }
            else if (id == 103)
            {
                titleName = "แยกตามประเภทรถ และช่วงเวลาต่าง ๆ";
            }
            else if (id == 104)
            {
                titleName = "แยกตามประเภทรถ และช่วงน้ำหนัก";
            }
            else if (id == 201)
            {
                titleName = "แยกตามประเภทรถ";
            }
            else if (id == 202)
            {
                titleName = "น้ำหนักรวมเฉลี่ย แยกตามประเภทรถ และช่วงเวลาต่าง ๆ";
            }
            else if (id == 301)
            {
                titleName = "เพลาเดี่ยว (Single Axle) แยกตามประเภทรถและช่วงน้ำหนัก";
            }
            else if (id == 302)
            {
                titleName = "เพลาคู่ (Tandem Axle) แยกตามประเภทรถและช่วงน้ำหนัก";
            }
            else if (id == 303)
            {
                titleName = "สามเพลา (Tridem Axle) แยกตามประเภทรถและช่วงน้ำหนัก";
            }
            else if (id == 304)
            {
                titleName = "สี่เพลา (Quarderm Axle) แยกตามประเภทรถและช่วงน้ำหนัก";
            }

            return titleName;
        }

        public static String IpAddress(HttpRequest Request)
        {

            String ipAddr = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (ipAddr == null || string.IsNullOrEmpty(ipAddr))
            {
                ipAddr = Request.ServerVariables["REMOTE_ADDR"];
            }
            return ipAddr;
        }

    }
}
