﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DOH.DC.Utils;
using System.Data;
using DOH.DC.Model;

namespace DOH.DC.Dao
{
    public class StationDao
    {
        public List<Station> getAll()
        {
            using (SqlConnection connection = new SqlConnection(GlobalConfiguration.ConnectionString))
            {
                connection.Open();
                string sql = "SELECT * FROM Station where stationid>0";
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlDataReader data = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                List<Station> stations = new List<Station>();

                while (data.Read())
                {
                    Station stationDTO = new Station();
                    stationDTO.StationID = Convert.ToInt16(data["StationID"]);
                    stationDTO.StationName = data["StationName"].ToString();
                    stations.Add(stationDTO);
                }
                return stations;
            }

        }
    }
}
