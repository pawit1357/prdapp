﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace DOH.DC.Dao
{
    public class DataTemplate
    {
        public DataTable DataTable;
        public string SQLCommandText;
        public string TableName;
        public string PKColumnName;


        public string dbConnString = string.Empty;
        private SqlConnection dbConn;
        private SqlCommand sqlCommand;
        private SqlDataAdapter dataAdapter;
        private SqlCommandBuilder cmdBuilder;

        public DataTemplate()
        {
        }

        public int LoadDataFromDB()
        {
            int ret = 0;
            try
            {   
                dbConn = new SqlConnection(dbConnString);
                dbConn.Open();
                sqlCommand = dbConn.CreateCommand();
                sqlCommand.CommandText = SQLCommandText;

                dataAdapter = new SqlDataAdapter(sqlCommand);
                cmdBuilder = new SqlCommandBuilder(dataAdapter);

                this.DataTable = new DataTable();

                dataAdapter.Fill(DataTable);

                this.DataTable.TableName = this.TableName;
                if (PKColumnName != string.Empty)
                {
                    this.DataTable.PrimaryKey = new DataColumn[] { this.DataTable.Columns[PKColumnName] };
                    this.DataTable.Columns[PKColumnName].AutoIncrement = true;
                }
            }
            catch (SqlException ex)
            {
                ret = -1;
                Logx("Error LoadDataFromDB: " + ex.ToString());
            }
            finally
            {
                dbConn.Close();
            }
            return ret;
        }

        public void Refresh()
        {
            LoadDataFromDB();
        }

        private int UpdateTable()
        {
            int output = -1;
            try
            {
                dbConn.Open();
                dataAdapter.UpdateCommand = cmdBuilder.GetUpdateCommand();
                dataAdapter.Update(this.DataTable);
                Refresh();
                output = 0;
            }
            catch (SqlException ex)
            {
                this.DataTable.RejectChanges();
                Logx("Error UpdateTable: " + ex.ToString());
            }
            finally
            {
                dbConn.Close();
            }

            return output;
        }

        public int UpdateRow(string TableXML)
        {
            DataRow dr = this.DataTable.NewRow();
            dr = DeserialzedTable(TableXML).Rows[0];
            return UpdateRow(dr);
        }

        public int UpdateRow(DataRow dataRow)
        {
            int output = -1;

            try
            {
                int id = Convert.ToInt32(dataRow[PKColumnName]);
                this.Refresh();
                DataRow row = this.DataTable.Rows.Find(id);
                foreach (DataColumn c in this.DataTable.Columns)
                {
                    if (c.ColumnName != PKColumnName)
                    {
                        if (dataRow[c.ColumnName] != System.DBNull.Value)
                        {
                            row[c.ColumnName] = dataRow[c.ColumnName];
                        }
                    }
                }
                output = UpdateTable();
                if (output == 0)
                {
                    Logx("UpdateRow: " + "OK");
                }
            }
            catch (Exception ex)
            {
                this.DataTable.RejectChanges();
                Logx("Error UpdateRow: " + ex.Message);
            }

            return output;
        }

        public int UpdateRows(string TableXML)
        {
            int output = -1;

            try
            {
                DataTable dt = DeserialzedTable(TableXML);
                if (dt != null)
                {
                    foreach (DataRow dataRow in dt.Rows)
                    {
                        int id = Convert.ToInt32(dataRow[PKColumnName]);

                        DataRow row = this.DataTable.Rows.Find(id);
                        foreach (DataColumn c in this.DataTable.Columns)
                        {
                            if (c.ColumnName != PKColumnName)
                            {
                                if (dataRow[c.ColumnName] != System.DBNull.Value)
                                {
                                    row[c.ColumnName] = dataRow[c.ColumnName];
                                }
                            }
                        }
                    }
                    output = UpdateTable();
                    if (output == 0)
                    {
                        Logx("UpdateRows: " + "OK");
                    }
                }
            }
            catch (Exception ex)
            {
                this.DataTable.RejectChanges();
                Logx("Error UpdateRows: " + ex.Message);
            }

            return output;
        }

        public int AddNewRow(string TableXML)
        {
            DataRow dr = this.DataTable.NewRow();
            dr = DeserialzedTable(TableXML).Rows[0];
            return AddNewRow(dr);
        }

        public int AddNewRow(DataRow dataRow)
        {
            int output = -1;

            try
            {
                DataRow row = this.DataTable.NewRow();
                foreach (DataColumn c in this.DataTable.Columns)
                {
                    if (dataRow[c.ColumnName] != System.DBNull.Value)
                    {
                        row[c.ColumnName] = dataRow[c.ColumnName];
                    }
                }
                this.DataTable.Rows.Add(row);
                output = UpdateTable();
                if (output == 0)
                {
                    Logx("AddNewRow: " + "OK");
                }
            }
            catch (Exception ex)
            {
                this.DataTable.RejectChanges();
                Logx("Error AddNewRow: " + ex.Message);
            }

            return output;
        }

        public int DeleteRow(int id)
        {
            int output = -1;
            try
            {
                DataRow row = this.DataTable.Rows.Find(id);
                if (row != null)
                {
                    row.Delete();
                    output = UpdateTable();
                    if (output == 0)
                    {
                        Logx("DeleteRow: " + "OK");
                    }
                }
            }
            catch (Exception ex)
            {
                this.DataTable.RejectChanges();
                Logx("Error DeleteRow: " + ex.Message);
            }
            return output;
        }

        public int DeleteRow(string TableXML)
        {
            int output = -1;

            try
            {
                DataRow row0 = DeserialzedTable(TableXML).Rows[0];
                string id_str = (string)row0[PKColumnName].ToString();

                DataRow row1 = this.DataTable.NewRow();
                foreach (DataColumn c in this.DataTable.Columns)
                {
                    if (row0[c.ColumnName] != System.DBNull.Value)
                    {
                        row1[c.ColumnName] = row0[c.ColumnName];
                    }
                }

                DataRow row2 = this.DataTable.Rows.Find(Convert.ToInt32(id_str));

                if (row2 != null)
                {
                    var array1 = row1.ItemArray;
                    var array2 = row2.ItemArray;
                    if (array1.SequenceEqual(array2))
                    {
                        row2.Delete();
                        output = UpdateTable();
                        if (output == 0)
                        {
                            Logx("DeleteRow\t" + "OK");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.DataTable.RejectChanges();
                Logx("Error DeleteRow: " + ex.Message);
            }

            return output;
        }

        public string SerializedTable()
        {
            StringWriter sw = new StringWriter();
            this.DataTable.WriteXml(sw, XmlWriteMode.WriteSchema);
            return sw.ToString();
        }

        private DataTable DeserialzedTable(string TableXML)
        {
            try
            {
                StringReader sr = new StringReader(TableXML);
                DataSet ds = new DataSet();
                ds.ReadXml(sr);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                Logx("DeserializedTable: " + ex.Message);
                return null;
            }
        }

        private void Logx(string msg)
        {
            //Messaging.LogAndShow("DataTemplate", msg);
        }

    }	
	
}
