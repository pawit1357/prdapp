﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DOH.DC.Dao
{
    public class WIMSQLText
    {
        public static string GetQuery_REP_WIM_101(DateTime dateFrom, DateTime dateTo, int stationID)
        {            
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            string sql = string.Empty;

            sql += "    SELECT TOP (100) PERCENT m.VehicleClassID, m.VehicleClassDescription,";
            sql += "    ISNULL(d.Lane, 1) AS LaneID, COUNT(d.VehicleClass) AS VehCount";
            sql += "    FROM dbo.VehicleClass AS m LEFT OUTER JOIN";
            sql += "        (SELECT Lane, VehicleClass FROM dbo.WIM";                    
            sql += "         WHERE (Lane > 0)";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "        ) AS d ";
            sql += "    ON d.VehicleClass = m.VehicleClassID ";
            sql += "    GROUP BY m.VehicleClassID, m.VehicleClassDescription, d.Lane";
            sql += "    ORDER BY m.VehicleClassID";

            return sql;
        }

        public static string GetQuery_REP_WIM_102(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            string sql = string.Empty;

            sql += "    SELECT TOP (100) PERCENT m.VehicleClassID, m.VehicleClassDescription,";
            sql += "    ISNULL(d.Lane, 1) AS LaneID, COUNT(d.VehicleClass) AS VehCount";
            sql += "    FROM dbo.VehicleClass AS m LEFT OUTER JOIN";
            sql += "        (SELECT Lane, VehicleClass FROM dbo.WIM";
            sql += "         WHERE (Lane > 0)";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "        ) AS d ";
            sql += "    ON d.VehicleClass = m.VehicleClassID ";
            sql += "    GROUP BY m.VehicleClassID, m.VehicleClassDescription, d.Lane";
            sql += "    ORDER BY m.VehicleClassID";

            return sql;
        }

        public static string GetQuery_REP_WIM_1031_OLD(DateTime dateFrom, DateTime dateTo, int stationID)
        {            
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            sql += "        DECLARE @HourTable table (hourx int);   ";
            sql += "        DECLARE @h int = 0; ";
            sql += "        WHILE (@h < 24) ";
            sql += "        BEGIN";
            sql += "            INSERT @HourTable values (@h);  ";
            sql += "            SET @h = @h + 1;    ";
            sql += "        END;    ";

            sql += "        SELECT m.VehHour, m.VehClass, COUNT(d.VehicleClass) AS VehCount FROM    ";
            sql += "            (SELECT a.hourx as VehHour, b.VehicleClassID AS VehClass FROM @HourTable a, VehicleClass b) AS m    ";
            sql += "        LEFT OUTER JOIN ";
            
            sql += "            (SELECT DATEPART(hh,TimeStamp) AS hourz, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'" ;
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "            ) AS d    ";
            sql += "        ON m.VehHour = d.hourz AND m.VehClass = d.VehicleClass  ";
            sql += "        GROUP BY m.VehHour, m.VehClass  ";
            sql += "        ORDER BY m.VehHour, m.VehClass  ";
            sql += "        ;";
            return sql;
        }

        public static string GetQuery_REP_WIM_1031(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            sql += "        SELECT m.VehicleClassID AS VehClass, ISNULL(d.dtime,0) AS Duration,      ";
            sql += "           COUNT(d.VehicleClass) AS VehCount FROM VehicleClass AS m     ";
            sql += "        LEFT OUTER JOIN ";
            sql += "            (SELECT DATEPART(hh,TimeStamp) AS dtime, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "            ) AS d    ";
            sql += "        ON m.VehicleClassID = d.VehicleClass  ";
            sql += "        GROUP BY d.dtime, m.VehicleClassID  ";
            sql += "        ORDER BY d.dtime, m.VehicleClassID  ";
            sql += "        ;";
            return sql;
        }

        public static string GetQuery_REP_WIM_1032(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            sql += "        SELECT m.VehicleClassID AS VehClass, d.dtime AS Duration,      ";
            sql += "           COUNT(d.VehicleClass) AS VehCount FROM VehicleClass AS m     ";
            sql += "        LEFT OUTER JOIN ";
            sql += "            (SELECT DATEPART(dd,TimeStamp) AS dtime, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "            ) AS d    ";
            sql += "        ON m.VehicleClassID = d.VehicleClass  ";
            //sql += "        WHERE d.dtime IS NOT NULL"  ;
            sql += "        GROUP BY d.dtime, m.VehicleClassID  ";
            sql += "        ORDER BY d.dtime, m.VehicleClassID  ";
            sql += "        ;";
            return sql;
        }

        public static string GetQuery_REP_WIM_1033(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            sql += "        SELECT m.VehicleClassID AS VehClass, d.dtime AS Duration,      ";
            sql += "           COUNT(d.VehicleClass) AS VehCount FROM VehicleClass AS m     ";
            sql += "        LEFT OUTER JOIN ";
            sql += "            (SELECT DATEPART(MM,TimeStamp) AS dtime, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "            ) AS d    ";
            sql += "        ON m.VehicleClassID = d.VehicleClass  ";
            //sql += "        WHERE d.dtime IS NOT NULL"  ;
            sql += "        GROUP BY d.dtime, m.VehicleClassID  ";
            sql += "        ORDER BY d.dtime, m.VehicleClassID  ";
            sql += "        ;";
            return sql;
        }

        public static string GetQuery_REP_WIM_1034(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            sql += "        SELECT m.VehicleClassID AS VehClass, d.dtime AS Duration,      ";
            sql += "           COUNT(d.VehicleClass) AS VehCount FROM VehicleClass AS m     ";
            sql += "        LEFT OUTER JOIN ";
            sql += "            (SELECT DATEPART(yyyy,TimeStamp) AS dtime, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "            ) AS d    ";
            sql += "        ON m.VehicleClassID = d.VehicleClass  ";
            //sql += "        WHERE d.dtime IS NOT NULL"  ;
            sql += "        GROUP BY d.dtime, m.VehicleClassID  ";
            sql += "        ORDER BY d.dtime, m.VehicleClassID  ";
            sql += "        ;";
            return sql;
        }

        public static string GetQuery_REP_WIM_104(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            sql += "        DECLARE @KTable table (K int);   ";
            sql += "        DECLARE @k int = 0; ";
            sql += "        WHILE (@k < 100) ";
            sql += "        BEGIN";
            sql += "            INSERT @KTable values (@k * 1000);  ";
            sql += "            SET @k = @k + 5;    ";
            sql += "        END;    ";

            sql += "        SELECT m.KLevel, m.VehClass, COUNT(d.VehicleClass) AS VehCount FROM    ";
            sql += "            (SELECT a.K as KLevel, b.VehicleClassID AS VehClass FROM @KTable a, VehicleClass b) AS m    ";
            sql += "        LEFT OUTER JOIN ";

            sql += "            (SELECT GVW, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql +=              filter_stationID;
            sql += "            ) AS d    ";

            sql += "        ON d.GVW >= m.KLevel AND d.GVW < (m.KLevel + 5000) AND d.VehicleClass = m.VehClass  ";
            sql += "        GROUP BY m.KLevel, m.VehClass  ";
            sql += "        ORDER BY m.KLevel, m.VehClass  ";
            sql += "        ;";
            return sql;
        }

        public static string GetQuery_REP_WIM_201(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }
            sql += "        SELECT m.VehicleClassID, m.VehicleClassDescription,";
            sql += "        SUM(d.GVW) AS SumGVW, MAX(d.GVW) AS MaxGVW, m.GVW_Weight_Max AS LimitedGVW,  ";
            sql += "        COUNT(d.VehicleClass) AS VehCount FROM VehicleClass AS m     ";
            sql += "        LEFT OUTER JOIN ";
            sql += "            (SELECT GVW, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql +=              filter_stationID;
            sql += "            ) AS d    ";
            sql += "        ON m.VehicleClassID = d.VehicleClass  ";
            sql += "        GROUP BY m.VehicleClassID, m.VehicleClassDescription, m.GVW_Weight_Max  ";
            sql += "        ORDER BY m.VehicleClassID  ";
            sql += "        ;";
           
            return sql;
        }

        public static string GetQuery_REP_WIM_2021(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }
            sql += "        SELECT m.VehicleClassID, d.dtime AS Duration, ";
            sql += "        SUM(d.GVW) AS SumGVW, ";
            sql += "        COUNT(d.VehicleClass) AS VehCount FROM VehicleClass AS m     ";
            sql += "        LEFT OUTER JOIN ";
            sql += "            (SELECT DATEPART(hh,TimeStamp) AS dtime, GVW, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "            ) AS d    ";
            sql += "        ON m.VehicleClassID = d.VehicleClass ";
            sql += "        WHERE d.dtime IS NOT NULL ";
            sql += "        GROUP BY m.VehicleClassID, d.dtime ";
            sql += "        ORDER BY m.VehicleClassID, d.dtime ";
            sql += "        ;";

            return sql;
        }

        public static string GetQuery_REP_WIM_2022(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }
            sql += "        SELECT m.VehicleClassID, d.dtime AS Duration, ";
            sql += "        SUM(d.GVW) AS SumGVW, ";
            sql += "        COUNT(d.VehicleClass) AS VehCount FROM VehicleClass AS m     ";
            sql += "        LEFT OUTER JOIN ";
            sql += "            (SELECT DATEPART(dd,TimeStamp) AS dtime, GVW, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "            ) AS d    ";
            sql += "        ON m.VehicleClassID = d.VehicleClass ";
            sql += "        WHERE d.dtime IS NOT NULL ";
            sql += "        GROUP BY m.VehicleClassID, d.dtime ";
            sql += "        ORDER BY m.VehicleClassID, d.dtime ";
            sql += "        ;";

            return sql;
        }

        public static string GetQuery_REP_WIM_2023(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }
            sql += "        SELECT m.VehicleClassID, d.dtime AS Duration, ";
            sql += "        SUM(d.GVW) AS SumGVW, ";
            sql += "        COUNT(d.VehicleClass) AS VehCount FROM VehicleClass AS m     ";
            sql += "        LEFT OUTER JOIN ";
            sql += "            (SELECT DATEPART(mm,TimeStamp) AS dtime, GVW, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "            ) AS d    ";
            sql += "        ON m.VehicleClassID = d.VehicleClass ";
            sql += "        WHERE d.dtime IS NOT NULL ";
            sql += "        GROUP BY m.VehicleClassID, d.dtime ";
            sql += "        ORDER BY m.VehicleClassID, d.dtime ";
            sql += "        ;";

            return sql;
        }

        public static string GetQuery_REP_WIM_2024(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }
            sql += "        SELECT m.VehicleClassID, d.dtime AS Duration, ";
            sql += "        SUM(d.GVW) AS SumGVW, ";
            sql += "        COUNT(d.VehicleClass) AS VehCount FROM VehicleClass AS m     ";
            sql += "        LEFT OUTER JOIN ";
            sql += "            (SELECT DATEPART(yyyy,TimeStamp) AS dtime, GVW, VehicleClass FROM dbo.WIM WHERE Lane > 0 ";
            sql += "                AND TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "                AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "            ) AS d    ";
            sql += "        ON m.VehicleClassID = d.VehicleClass ";
            sql += "        WHERE d.dtime IS NOT NULL ";
            sql += "        GROUP BY m.VehicleClassID, d.dtime ";
            sql += "        ORDER BY m.VehicleClassID, d.dtime ";
            sql += "        ;";

            return sql;
        }

        public static string GetQuery_REP_WIM_30X(DateTime dateFrom, DateTime dateTo, int stationID)
        {
            string sql = string.Empty;
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            sql += "        DECLARE @KTable table (K int);   ";
            sql += "        DECLARE @k int = 0; ";
            sql += "        WHILE (@k < 100) ";
            sql += "        BEGIN";
            sql += "            INSERT @KTable values (@k * 1000);  ";
            sql += "            SET @k = @k + 5;    ";
            sql += "        END;    ";

            sql += "        SELECT m.KLevel, m.VehClass ";
            sql += "        ,SUM(d.g1) AS GType1, SUM(d.g2) AS GType2, SUM(d.g3) AS GType3, SUM(d.g4) AS GType4 ";
            sql += "        FROM  ";

            sql += "        (SELECT a.K AS KLevel, b.VehicleClassID AS VehClass FROM @KTable AS a, VehicleClass AS b) AS m "; 

            sql += "        LEFT OUTER JOIN ";

            sql += "        (SELECT VehicleClass, GVW ";
            sql += "         ,SUM(IIF(Axle01Group=1,1,0)+IIF(Axle02Group=1,1,0)+IIF(Axle03Group=1,1,0)+ ";
            sql += "         IIF(Axle04Group=1,1,0)+IIF(Axle05Group=1,1,0)+IIF(Axle06Group=1,1,0)+IIF(Axle07Group=1,1,0)+ ";
            sql += "         IIF(Axle08Group=1,1,0)+IIF(Axle09Group=1,1,0)+IIF(Axle10Group=1,1,0)+IIF(Axle11Group=1,1,0)+ ";
            sql += "         IIF(Axle12Group=1,1,0)+IIF(Axle13Group=1,1,0)+IIF(Axle14Group=1,1,0)) AS g1, ";

            sql += "         SUM(IIF(Axle01Group=2,1,0)+IIF(Axle02Group=2,1,0)+IIF(Axle03Group=2,1,0)+ ";
            sql += "         IIF(Axle04Group=2,1,0)+IIF(Axle05Group=2,1,0)+IIF(Axle06Group=2,1,0)+IIF(Axle07Group=2,1,0)+ ";
            sql += "         IIF(Axle08Group=2,1,0)+IIF(Axle09Group=2,1,0)+IIF(Axle10Group=2,1,0)+IIF(Axle11Group=2,1,0)+ ";
            sql += "         IIF(Axle12Group=2,1,0)+IIF(Axle13Group=2,1,0)+IIF(Axle14Group=2,1,0)) AS g2, ";

            sql += "         SUM(IIF(Axle01Group=3,1,0)+IIF(Axle02Group=3,1,0)+IIF(Axle03Group=3,1,0)+ ";
            sql += "         IIF(Axle04Group=3,1,0)+IIF(Axle05Group=3,1,0)+IIF(Axle06Group=3,1,0)+IIF(Axle07Group=3,1,0)+ ";
            sql += "         IIF(Axle08Group=3,1,0)+IIF(Axle09Group=3,1,0)+IIF(Axle10Group=3,1,0)+IIF(Axle11Group=3,1,0)+ ";
            sql += "         IIF(Axle12Group=3,1,0)+IIF(Axle13Group=3,1,0)+IIF(Axle14Group=3,1,0)) AS g3, ";

            sql += "         SUM(IIF(Axle01Group=4,1,0)+IIF(Axle02Group=4,1,0)+IIF(Axle03Group=4,1,0)+ ";
            sql += "         IIF(Axle04Group=4,1,0)+IIF(Axle05Group=4,1,0)+IIF(Axle06Group=4,1,0)+IIF(Axle07Group=4,1,0)+ ";
            sql += "         IIF(Axle08Group=4,1,0)+IIF(Axle09Group=4,1,0)+IIF(Axle10Group=4,1,0)+IIF(Axle11Group=4,1,0)+ ";
            sql += "         IIF(Axle12Group=4,1,0)+IIF(Axle13Group=4,1,0)+IIF(Axle14Group=4,1,0)) AS g4 ";
 
            sql += "         FROM dbo.WIM WHERE Lane > 0  ";
            sql += "         GROUP BY VehicleClass, GVW ";
            sql += "         ) AS d ";

            sql += "        ON (d.VehicleClass = m.VehClass) AND (d.GVW >= m.KLevel) AND (d.GVW < m.KLevel + 5000) ";
            sql += "        GROUP BY m.KLevel, m.VehClass ";
            sql += "        ORDER BY m.KLevel, m.VehClass ";

            return sql;
        }
    }
}
