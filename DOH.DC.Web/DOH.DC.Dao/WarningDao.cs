﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DOH.DC.Model;
using System.Data.SqlClient;
using System.Data;
using DOH.DC.Utils;
using System.IO;
using DOH.DC.Biz;

namespace DOH.DC.Dao
{
    public class WarningDao
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(WarningDao));

        public static List<WarningVehicle> select(DataFilters df)
        {
            List<WarningVehicle> lists = new List<WarningVehicle>();
            try
            {
                using (SqlConnection con = new SqlConnection(GlobalConfiguration.ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand(SQLText.GetSQLText_WARNING(df), con);
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    int run_no = 1;
                    while (dr.Read())
                    {
                        WarningVehicle model = new WarningVehicle
                        {
                            Type = Convert.ToInt32(dr["Type"].ToString()),
                            RefID = Convert.ToInt64(dr["RefID"].ToString()),
                            StationID =  Convert.ToInt32(dr["StationID"].ToString()),
                            TimeStamp = Convert.ToDateTime(dr["TimeStamp"].ToString()),
                            RefNumber =  Convert.ToInt32(dr["RefNumber"].ToString()),
                            ImageName = dr["ImageName"].ToString()
                        };
                        run_no++;
                        lists.Add(model);
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message);
                logger.Error(ex.StackTrace);
            }
            return lists;
        }
        public static RadarDetection getById(String id)
        {
            List<RadarDetection> lists = new List<RadarDetection>();
            try
            {
                using (SqlConnection con = new SqlConnection(GlobalConfiguration.ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand(SQLText.GetSQLText_RADAR(id), con);
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    int run_no = 1;
                    while (dr.Read())
                    {
                        RadarDetection model = new RadarDetection
                        {
                            Running = run_no,
                            RSID = Convert.ToInt32(dr["RSID"].ToString()),
                            StationID = Convert.ToInt32(dr["StationID"].ToString()),
                            TimeStamp = Convert.ToDateTime(dr["TimeStamp"].ToString()),
                            LaneID = Convert.ToInt32(dr["LaneID"].ToString()),
                            Range = Convert.ToDecimal(dr["Range"].ToString()),
                            SeqNumber = Convert.ToInt32(dr["SeqNumber"].ToString()),
                            Duration = Convert.ToDecimal(dr["Duration"].ToString()),
                            Speed = Convert.ToDecimal(dr["Speed"].ToString()),
                            Class = Convert.ToInt32(dr["Class"].ToString()),
                            Length = Convert.ToDecimal(dr["Length"].ToString()),
                            MinLength = Convert.ToDecimal(dr["MinLength"].ToString()),
                            ImageName = dr["ImageName"].ToString(),
                            StationName = dr["StationName"].ToString()
                        };
                        run_no++;
                        lists.Add(model);
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message);
                logger.Error(ex.StackTrace);
            }
            return (lists.Count > 0) ? lists[0] : null;
        }

    }
}
