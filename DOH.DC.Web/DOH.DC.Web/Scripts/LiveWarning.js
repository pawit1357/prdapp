﻿
setInterval(getWarningType1, 3000);
setInterval(getWarningType2, 2000);
setInterval(getWarningType3, 2000);
setInterval(getWarningType4, 2000);

function getWarningType1() {
    $.ajax({
        type: "POST",
        url: "LiveAjax.aspx/getWanningType1",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnWarningType1Success,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function getWarningType2() {
    $.ajax({
        type: "POST",
        url: "LiveAjax.aspx/getWanningType2",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnWarningType2Success,
        failure: function (response) {
            alert(response.d);
        }
    });
}

function getWarningType3() {
    $.ajax({
        type: "POST",
        url: "LiveAjax.aspx/getWanningType3",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnWarningType3Success,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function getWarningType4() {
    $.ajax({
        type: "POST",
        url: "LiveAjax.aspx/getWanningType4",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnWarningType4Success,
        failure: function (response) {
            alert(response.d);
        }
    });
}


function OnWarningType1Success(response) {
    $("#tbReport1").html(response.d);
}
function OnWarningType2Success(response) {
    $("#tbReport2").html(response.d);
}
function OnWarningType3Success(response) {
    $("#tbReport3").html(response.d);
}
function OnWarningType4Success(response) {
    $("#tbReport4").html(response.d);
}



