﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Biz;
using System.Net.Mail;
using System.Text;
using System.IO;
using DOH.DC.Utils;
using DOH.DC.Dao;

namespace DOH.DC.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String imgPath = "wim-02-20140927-062559-42538-0.jpg";
            String xxx = Function.GetImageURL( imgPath );
            
            
            string submit = Request.Params["submit"];

            if (submit != null && submit != "")
            {
                string username = Request.Params["username"];
                string password = Request.Params["password"];

                UserLoginService service = new UserLoginService();
                if (service.authen(username, password))
                {
                    HttpContext.Current.Session["RequestIP"] = Util.IpAddress(Request);                    
                    Response.Redirect(GlobalConfiguration.getDefaultPage());
                }
                else
                {
                    Session["wrongpass"] = "true";
                    Response.Redirect("Login.aspx");
                }
            }


            if (Session["wrongpass"] != null && Session["wrongpass"].ToString() != "")
            {
                wrongpassword.Visible = true;
                Session.Remove("wrongpass");
            }
            else
            {
                wrongpassword.Visible = false;
            }
        }



    }
}