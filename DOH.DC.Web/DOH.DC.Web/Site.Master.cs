﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Biz;
using DOH.DC.Dao;
using DOH.DC.Model;

namespace DOH.DC.Web
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                initial();
            }
        }

        protected void btnSignin_Click(object sender, EventArgs e)
        {

        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            UserLoginService service = new UserLoginService();
            service.logout();
            Response.Redirect("/dc/Login.aspx");
        }

        private void initial()
        {
            StationDao dao = new StationDao();

            ddlStation.DataSource = dao.getAll();
            ddlStation.DataValueField = "StationID";
            ddlStation.DataTextField = "StationName";
            ddlStation.DataBind();
            ddlStation.Items.Insert(0, new ListItem("Select", ""));

            UserLogin userLogin = (UserLogin)HttpContext.Current.Session["userLoginBean"];
            
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx?stationID=" + ddlStation.SelectedValue);
        }

        protected void ddlStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["stationID"] = ddlStation.SelectedValue;
            Session["stationName"] = ddlStation.SelectedItem.Text;
        }

    }
}
