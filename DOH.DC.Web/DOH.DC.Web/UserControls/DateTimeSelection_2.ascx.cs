﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using DOH.DC.Dao;
using System.Data;
using DOH.DC.Utils;

namespace DOH.DC.Web.UserControl
{
    public partial class DateTimeSelection_2 : System.Web.UI.UserControl
    {

        #region "Variable"
        DayOfWeek firstDayOfWeek = DayOfWeek.Monday;
        private bool hasStartDate = true;
        private bool hasEndDate = true;

        private String currentFormat = "dddd d MMMMyyyy";

        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }

        public string Param_StationName { get; set; }
        public string Param_UserFullName { get; set; }
        public string Param_WeightType { get; set; }
        public string Param_TimeBegin { get; set; }
        public string Param_TimeEnd { get; set; }
        public string Param_FilterTitle { get; set; }

        public string SummaryType { get; set; }
        public string IsWeightOver { get; set; }
        public string UserID { get; set; }
        public string StationID { get; set; }
        List<UserItem> list = new List<UserItem>();

        private class UserItem
        {
            public int UserID { get; set; }
            public string FullName { get; set; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                StationDao dao = new StationDao();

                ddlStation.Items.Clear();
                ddlStation.DataSource = dao.getAll();
                ddlStation.DataBind();
                initial();
                rbPeriod.Items[0].Selected = true;
                rbPeriod.SelectedIndex = 0;
            }
            if (Session["startDate"] != null && Session["endDate"] != null)
            {
                this.startDate = (DateTime)Session["startDate"];
                this.endDate = (DateTime)Session["endDate"];
            }
        }
        protected void rbPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            initial();
        }

        protected void ddlStation_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void llbPrev10_Click(object sender, EventArgs e)
        {

            this.startDate = startDate.AddDays(-70);
            this.endDate = startDate.AddDays(6);
            this.dtFrom.Text = startDate.ToString(currentFormat);
            this.dtTo.Text = endDate.ToString(currentFormat);

            Session["startDate"] = this.startDate;
            Session["endDate"] = this.endDate;
        }
        protected void llbNext10_Click(object sender, EventArgs e)
        {
            this.startDate = startDate.AddDays(70);
            this.endDate = startDate.AddDays(6);
            this.dtFrom.Text = startDate.ToString(currentFormat);
            this.dtTo.Text = endDate.ToString(currentFormat);

            Session["startDate"] = this.startDate;
            Session["endDate"] = this.endDate;
        }
        protected void llbPrevWeek_Click(object sender, EventArgs e)
        {
            this.startDate = startDate.AddDays(-7);
            this.endDate = startDate.AddDays(6);
            this.dtFrom.Text = startDate.ToString(currentFormat);
            this.dtTo.Text = endDate.ToString(currentFormat);

            Session["startDate"] = this.startDate;
            Session["endDate"] = this.endDate;
        }
        protected void llbNextWeek_Click(object sender, EventArgs e)
        {
            this.startDate = startDate.AddDays(7);
            this.endDate = startDate.AddDays(6);
            this.dtFrom.Text = startDate.ToString(currentFormat);
            this.dtTo.Text = endDate.ToString(currentFormat);

            Session["startDate"] = this.startDate;
            Session["endDate"] = this.endDate;
        }


        #region Method
        private void initial()
        {
            switch (rbPeriod.SelectedIndex)
            {
                case 0:
                    currentFormat = "dddd d MMMMyyyy";

                    this.lbFrom.Visible = true;
                    this.lbFrom.Text = "วัน";
                    this.lbTo.Visible = false;
                    this.dtFrom.Visible = false;

                    this.llbNextWeek.Visible = false;
                    this.llbPrevWeek.Visible = false;
                    this.llbPrev10.Visible = false;
                    this.llbNext10.Visible = false;

                    this.dtFrom.Visible = true;
                    this.dtTo.Visible = false;

                    var _with1 = this.dtFrom;
                    _with1.Enabled = true;
                    _with1.Text = DateTime.Now.ToString(currentFormat);

                    var _with2 = this.dtTo;
                    _with2.Enabled = true;
                    _with2.Text = DateTime.Now.ToString(currentFormat);
                    break;
                case 1:
                    currentFormat = "dddd d MMMMyyyy";
                    this.lbFrom.Visible = true;
                    this.lbFrom.Text = "เริ่ม";
                    this.lbTo.Visible = true;
                    this.lbTo.Text = "ถึง";

                    this.llbNextWeek.Visible = true;
                    this.llbPrevWeek.Visible = true;
                    this.llbPrev10.Visible = true;
                    this.llbNext10.Visible = true;

                    var _with3 = this.dtFrom;
                    _with3.Enabled = true;
                    _with3.Visible = true;
                    _with3.Text = DateTime.Now.ToString(currentFormat);

                    var _with4 = this.dtTo;
                    _with4.Enabled = true;
                    _with4.Visible = true;
                    _with4.Text = DateTime.Now.ToString(currentFormat);


                    this.startDate = new System.DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);
                    //วันเริ่มต้นของสัปดาห์
                    this.startDate = startDate.AddDays(firstDayOfWeek - DateTime.Today.DayOfWeek);
                    this.endDate = startDate.AddDays(6);
                    this.dtFrom.Text = startDate.ToString(currentFormat);
                    this.dtTo.Text = endDate.ToString(currentFormat);
                    break;
                case 2:
                    currentFormat = "MMMM yyyy";
                    this.lbFrom.Visible = true;
                    this.lbFrom.Text = "เดือน";
                    this.lbTo.Visible = false;

                    this.llbNextWeek.Visible = false;
                    this.llbPrevWeek.Visible = false;
                    this.llbNext10.Visible = false;
                    this.llbPrev10.Visible = false;

                    this.dtFrom.Visible = true;
                    this.dtTo.Visible = false;

                    var _with5 = this.dtFrom;
                    _with5.Enabled = true;
                    _with5.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, 0).ToString(currentFormat);
                    break;
                case 3:
                    currentFormat = "yyyy";
                    this.lbFrom.Visible = true;
                    this.lbFrom.Text = "ปี";
                    this.lbTo.Visible = false;
                    this.llbNextWeek.Visible = false;
                    this.llbPrevWeek.Visible = false;
                    this.llbPrev10.Visible = false;
                    this.llbNext10.Visible = false;

                    this.dtFrom.Visible = true;
                    this.dtTo.Visible = false;

                    var _with6 = this.dtFrom;
                    _with6.Enabled = true;
                    _with6.Text = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0, 0).ToString(currentFormat);
                    break;
                case 4:
                    currentFormat = "dddd d MMMM yyyy  เวลา HH:mm";
                    this.lbFrom.Visible = true;
                    this.lbFrom.Text = "เริ่ม";
                    this.lbTo.Visible = true;
                    this.lbTo.Text = "ถึง";
                    this.llbNextWeek.Visible = false;
                    this.llbPrevWeek.Visible = false;
                    this.llbPrev10.Visible = false;
                    this.llbNext10.Visible = false;

                    this.dtFrom.Visible = true;
                    this.dtTo.Visible = true;

                    var _with7 = this.dtFrom;
                    _with7.Enabled = true;
                    _with7.Text = DateTime.Now.ToString(currentFormat);

                    var _with8 = this.dtTo;
                    _with8.Enabled = true;
                    _with8.Text = DateTime.Now.ToString(currentFormat);

                    break;
                default:
                    break;
            }

            CalendarExtender1.Format = currentFormat;
            CalendarExtender2.Format = currentFormat;
        }


        public void Summarize()
        {

            if (rbPeriod.SelectedIndex == 0)
            {
                SummaryType = "วันที่ " + this.startDate.ToString("d MMMM yyyy");
            }
            else if (rbPeriod.SelectedIndex == 1)
            {
                SummaryType = "สัปดาห์ (" + this.startDate.ToString("d MMMM yyyy") + " ถึง " + this.endDate.ToString("d MMMM yyyy") + ")";
            }
            else if (rbPeriod.SelectedIndex == 2)
            {
                SummaryType = "เดือน " + this.startDate.ToString("MMMM yyyy");
            }
            else if (rbPeriod.SelectedIndex == 3)
            {
                SummaryType = "ปี " + this.startDate.ToString("yyyy");
            }
            else if (rbPeriod.SelectedIndex == 4)
            {
                if (!this.dtFrom.Text.Equals(DateTime.Now.ToString(currentFormat)))
                {
                    hasStartDate = true;
                }
                else
                {
                    hasStartDate = false;
                }

                if (!this.dtTo.Text.Equals(DateTime.Now.ToString(currentFormat)))
                {
                    hasEndDate = true;
                }
                else
                {
                    hasEndDate = false;
                }

                if (hasStartDate)
                {
                    Param_TimeBegin = startDate.ToString("d MMM yyyy   HH:mm:ss");
                    if (hasEndDate)
                    {
                        Param_TimeEnd = endDate.ToString("d MMM yyyy   HH:mm:ss");
                        Param_FilterTitle = "ตั้งแต่ " + this.dtFrom.Text + " ถึง " + this.dtTo.Text;
                    }
                    else
                    {
                        Param_FilterTitle = "ตั้งแต่ " + this.dtFrom.Text + " ถึง รายการสุดท้าย";
                        Param_TimeEnd = "ไม่ระบุ";
                    }
                }
                else
                {
                    Param_TimeBegin = "ไม่ระบุ";
                    if (hasEndDate)
                    {
                        Param_FilterTitle = "ตั้งแต่รายการแรก ถึง " + this.dtTo.Text;
                        Param_TimeEnd = endDate.ToString("d MMM yyyy   HH:mm:ss");
                    }
                    else
                    {
                        Param_FilterTitle = "ข้อมูลทั้งหมด";
                        Param_TimeEnd = "ไม่ระบุ";
                    }
                }
            }

            StationID = ddlStation.SelectedValue;
            Param_StationName = GlobalConfiguration.StationNamePrefix+""+ ddlStation.SelectedItem.Text;

            if (this.startDate.ToString("yyyyMMdd").Equals("00010101"))
            {
                this.startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            }
            else
            {

            }
            if (this.endDate.ToString("yyyyMMdd").Equals("00010101"))
            {
                this.endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }

            if (!this.dtFrom.Text.Equals(DateTime.Now.ToString(currentFormat)))
            {
                Param_TimeBegin = startDate.ToString("d MMM yyyy   HH:mm:ss");
                hasStartDate = true;
                if (!this.dtTo.Text.Equals(DateTime.Now.ToString(currentFormat)))
                {
                    hasEndDate = true;
                    Param_TimeEnd = endDate.ToString("d MMM yyyy   HH:mm:ss");
                    Param_FilterTitle = "ตั้งแต่ " + this.dtFrom.Text + " ถึง " + this.dtTo.Text;
                }
                else
                {
                    hasEndDate = false;
                    Param_TimeEnd = "ไม่ระบุ";
                    Param_FilterTitle = "ตั้งแต่ " + this.dtFrom.Text + " ถึง รายการสุดท้าย";
                }
            }
            else
            {
                Param_TimeBegin = "ไม่ระบุ";
                hasStartDate = false;
                if (!this.dtFrom.Text.Equals(DateTime.Now.ToString(currentFormat)))
                {
                    hasStartDate = true;
                    Param_TimeEnd = endDate.ToString("d MMM yyyy   HH:mm:ss");
                    Param_FilterTitle = "ตั้งแต่รายการแรก ถึง " + this.dtTo.Text;
                }
                else
                {
                    hasStartDate = false;
                    Param_TimeEnd = "ไม่ระบุ";
                    Param_FilterTitle = "ข้อมูลทั้งหมด";
                }
            }
            //Param_WeightType = cbDetailsBy.SelectedItem.Text;
            Console.WriteLine("");
        }


        public void clear()
        {

            firstDayOfWeek = DayOfWeek.Monday;
            hasStartDate = true;
            hasEndDate = true;



            //startDate { get; set; }
            //pendDate { get; set; }

            Param_StationName = string.Empty;
            Param_UserFullName = string.Empty;
            Param_WeightType = string.Empty;
            Param_TimeBegin = string.Empty;
            Param_TimeEnd = string.Empty;
            Param_FilterTitle = string.Empty;

            SummaryType = string.Empty;
            IsWeightOver = string.Empty;
            UserID = string.Empty;
            StationID = string.Empty;
            list.Clear();

            ddlStation.SelectedIndex = -1;
            dtFrom.Text = string.Empty;
            dtTo.Text = string.Empty;

        }

        public void setTitle(string title)
        {
            lbTitle.Text = title;
        }

        #endregion

        protected void dtFrom_TextChanged(object sender, EventArgs e)
        {
            this.startDate = Convert.ToDateTime(dtFrom.Text);
            this.endDate = Convert.ToDateTime(dtTo.Text);
            Session["startDate"] = this.startDate;
            Session["endDate"] = this.endDate;
        }

        protected void dtTo_TextChanged(object sender, EventArgs e)
        {
            this.startDate = Convert.ToDateTime(dtFrom.Text);
            this.endDate = Convert.ToDateTime(dtTo.Text);
            Session["startDate"] = this.startDate;
            Session["endDate"] = this.endDate;
        }

        public bool HasStartDate()
        {
            return this.hasStartDate;
        }
        public bool HasEndDate()
        {
            return this.hasEndDate;
        }

    }
}