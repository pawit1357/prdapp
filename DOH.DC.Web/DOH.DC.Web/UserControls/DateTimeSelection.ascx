﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateTimeSelection.ascx.cs"
    Inherits="DOH.DC.Web.UserControl.DateTimeSelection" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="well">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#divSearch" data-toggle="tab">
            <asp:Label ID="lbTitle" runat="server" Text=""></asp:Label></a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane active in" id="divSearch">
            <table class="table-search">
                <tr>
                    <td>
                        <label>
                            สถานี</label>
                    </td>
                    <td width="300px" style="width: 150px">
                        <asp:DropDownList ID="ddlStation" runat="server" CssClass="input-xlarge" DataTextField="StationName"
                            DataValueField="StationID" AppendDataBoundItems="true" TabIndex="1" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlStation_SelectedIndexChanged" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbFrom0" runat="server" Text="ช่วงเวลา"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rbPeriod" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rbPeriod_SelectedIndexChanged"
                            RepeatDirection="Horizontal" Width="529px">
                            <asp:ListItem Selected="True" Value="0">วัน</asp:ListItem>
                            <asp:ListItem Value="1">สัปดาห์</asp:ListItem>
                            <asp:ListItem Value="2">เดือน</asp:ListItem>
                            <asp:ListItem Value="3">ปี</asp:ListItem>
                            <asp:ListItem Value="4" Selected="True">เลือกเอง</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbFrom" runat="server" Text="วัน"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="dtFrom" runat="server" AutoPostBack="True" 
                            ontextchanged="dtFrom_TextChanged"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="dtFrom">
                        </asp:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbTo" runat="server" Text="ถึง"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="dtTo" runat="server" AutoPostBack="True" 
                            ontextchanged="dtTo_TextChanged"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="dtTo">
                        </asp:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:LinkButton ID="llbPrev10" runat="server" OnClick="llbPrev10_Click">&lt;&lt; 10</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="llbPrevWeek" runat="server" OnClick="llbPrevWeek_Click">ก่อนหน้า</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="llbNextWeek" runat="server" OnClick="llbNextWeek_Click">ถัดไป</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="llbNext10" runat="server" OnClick="llbNext10_Click">10 &gt;&gt;</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="สรุปข้อมูลแยกเป็น"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cbDetailsBy" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            เฉพาะรถที่มีน้ำหนัก</label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rbWeight" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="0">เกิน</asp:ListItem>
                            <asp:ListItem Selected="True" Value="1">ไม่เกิน</asp:ListItem>
                            <asp:ListItem Value="2">ทั้งหมด</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="checkBox_SelectUser" runat="server" AutoPostBack="True" OnCheckedChanged="checkBox_SelectUser_CheckedChanged" />
                        <label>
                            เฉพาะรายการที่บันทึกด้วย</label>
                    </td>
                    <td>
                        <asp:DropDownList ID="comboBox_User" runat="server" CssClass="input-xlarge" DataTextField="FullName"
                            DataValueField="UserID" AppendDataBoundItems="true" TabIndex="1" AutoPostBack="True"
                            Enabled="False" />
                    </td>
                </tr>
            </table>
<%--            <div class="submit">
                <asp:LinkButton ID="btnOK" runat="server" CssClass="btn" TabIndex="10" OnClick="btnOK_Click"><i class="icon-search"></i> OK</asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn" Text="Clear" TabIndex="11"
                    OnClick="btnCancel_Click" />
            </div>--%>
        </div>
    </div>
</div>
