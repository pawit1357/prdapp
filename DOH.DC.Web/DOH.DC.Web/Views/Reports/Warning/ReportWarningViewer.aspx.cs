﻿using System;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.IO;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Reports.Wim
{
    public partial class ReportWarningViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");

            if (!IsPostBack)
            {
                RenderReport();
            } 
        }

        private void RenderReport()
        {
            String ReportName = Session["reportSource"].ToString();
            ReportDataSource rptDataSource = (ReportDataSource)Session["rptDataSource"];
            ReportParameterCollection reportParameters = (ReportParameterCollection)Session["reportParameters"];

            // Reset report properties. 
            ReportViewer1.Height = Unit.Parse("100%");
            ReportViewer1.Width = Unit.Parse("100%");
            //ReportViewer1.CssClass = "table";
            // Clear out any previous datasources. 
            ReportViewer1.LocalReport.DataSources.Clear();
            //Set report mode for local processing. 
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            //Validate report source 
            var rptPath = Server.MapPath("~/ReportObjects/Warning/" + ReportName);
            if (!File.Exists(rptPath))
                return;
            //Set report path 
            this.ReportViewer1.LocalReport.ReportPath = rptPath;


            ReportViewer1.LocalReport.SetParameters(reportParameters);


            // Load the dataSource. 
            var dsmems = ReportViewer1.LocalReport.GetDataSourceNames();
            ReportViewer1.LocalReport.DataSources.Add(rptDataSource);
            // Refresh the ReportViewer 
            ReportViewer1.LocalReport.Refresh();
        }
    }
}