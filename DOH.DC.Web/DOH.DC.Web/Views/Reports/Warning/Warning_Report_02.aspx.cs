﻿using System;
using DOH.DC.Dao;
using System.Data;
using Microsoft.Reporting.WebForms;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Reports.Warning
{
    public partial class Warning_Report_02 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");

            if (!IsPostBack)
            {
                dtpDateTimeSelection.setTitle("ปริมาณรถ แยกตามประเภทการเตือน และช่วงเวลาต่าง ๆ ");
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {

            DataFilter filter = new DataFilter();
            dtpDateTimeSelection.Summarize();

            filter.hasStarDate = dtpDateTimeSelection.HasStartDate();
            filter.hasEndDate = dtpDateTimeSelection.HasEndDate();
            filter.startDate = dtpDateTimeSelection.startDate;
            filter.endDate = dtpDateTimeSelection.endDate;
            filter.dateTimeText = dtpDateTimeSelection.Param_FilterTitle;
            filter.summaryType = dtpDateTimeSelection.Param_WeightType;

            filter.stationID = Convert.ToInt16(dtpDateTimeSelection.StationID);
            filter.stationName = dtpDateTimeSelection.Param_StationName;

            GetReport(Convert.ToInt16(hwimid.Value), filter);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            dtpDateTimeSelection.clear();
        }
        void ShowReport(string reportSource, DataTable dataSource, ReportParameterCollection reportParameters)
        {
            if (dataSource.Rows.Count > 0)
            {

                var rptPath = Server.MapPath("~/ReportObjects/Warning/" + reportSource);


                ReportDataSource rptDataSource = new ReportDataSource("DataSet1", dataSource);

                Session["reportSource"] = reportSource;
                Session["rptDataSource"] = rptDataSource;
                Session["reportParameters"] = reportParameters;

                Response.Redirect("ReportWarningViewer.aspx");
            }
            else
            {
                label_status.Text = "ไม่พบข้อมูล";
            }
        }


        public void GetReport(int id, DataFilter filter)
        {
            try
            {
                if (id == 101)
                {
                    ShowReport_WARN_101(filter);
                }
                else if (id == 102)
                {
                    ShowReport_WARN_102(filter);
                }
                else if (id == 103)
                {
                    ShowReport_WARN_103(filter);
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("ERROR Getting Report\r\n\r\n" + ex.Message);
            }
        }


        void ShowReport_WARN_101(DataFilter filter)
        {
            string report = "REP_WARN_101.rdlc";

            string sql = WARNSQLText.GetQuery_REP_WARN_101(filter.startDate, filter.endDate, filter.stationID);
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WARN_102(DataFilter filter)
        {
            string report = string.Empty;
            string sql = string.Empty;

            if (filter.summaryType == "ชั่วโมง")
            {
                report = "REP_WARN_1021.rdlc";
                sql = WARNSQLText.GetQuery_REP_WARN_102(filter.startDate, filter.endDate, filter.stationID, "hh");
            }
            else if (filter.summaryType == "วัน")
            {
                report = "REP_WARN_1022.rdlc";
                sql = WARNSQLText.GetQuery_REP_WARN_102(filter.startDate, filter.endDate, filter.stationID, "dd");
            }
            else if (filter.summaryType == "เดือน")
            {
                report = "REP_WARN_1023.rdlc";
                sql = WARNSQLText.GetQuery_REP_WARN_102(filter.startDate, filter.endDate, filter.stationID, "mm");
            }
            else if (filter.summaryType == "ปี")
            {
                report = "REP_WARN_1024.rdlc";
                sql = WARNSQLText.GetQuery_REP_WARN_102(filter.startDate, filter.endDate, filter.stationID, "yyyy");
            }

            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WARN_103(DataFilter filter)
        {
            string report = "REP_WARN_103.rdlc";

            string sql = WARNSQLText.GetQuery_REP_WARN_103(filter.startDate, filter.endDate, filter.stationID, filter.WarningType);
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

    }
}