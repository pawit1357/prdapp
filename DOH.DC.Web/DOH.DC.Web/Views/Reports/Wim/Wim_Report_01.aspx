﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Wim_Report_01.aspx.cs" Inherits="DOH.DC.Web.Views.Reports.Wim.Wim_Report_01" %>

<%@ Register Src="~/UserControls/DateTimeSelection_2.ascx" TagPrefix="asp" TagName="DateTimeSelection" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hwimid" runat="server" />
            <asp:DateTimeSelection runat="server" ID="dtpDateTimeSelection" CssStyle="input-mlarge"
                FormatType="Date" />
            <asp:Label ID="label_status" runat="server" Text=""></asp:Label>
            <div class="submit">
                <asp:LinkButton ID="btnOK" runat="server" CssClass="btn" TabIndex="10" OnClick="btnOK_Click"><i class="icon-search"></i> OK</asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn" Text="Clear" TabIndex="11"
                    OnClick="btnCancel_Click" />
            </div>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
