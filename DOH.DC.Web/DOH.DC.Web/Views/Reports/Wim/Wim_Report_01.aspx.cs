﻿using System;
using DOH.DC.Dao;
using System.Data;
using Microsoft.Reporting.WebForms;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Reports.Wim
{
    public partial class Wim_Report_01 : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");

            if (!IsPostBack)
            {
                int id = Convert.ToInt16(Request.Params["id"]);
                hwimid.Value = id+"";
                dtpDateTimeSelection.setTitle("WIM - " + Util.getReportTitle(id));               
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {

            DataFilter filter = new DataFilter();
            dtpDateTimeSelection.Summarize();
           
            filter.hasStarDate = dtpDateTimeSelection.HasStartDate();
            filter.hasEndDate = dtpDateTimeSelection.HasEndDate();
            filter.startDate = dtpDateTimeSelection.startDate;
            filter.endDate = dtpDateTimeSelection.endDate;
            filter.dateTimeText = dtpDateTimeSelection.Param_FilterTitle;
            //filter.summaryType = dtpDateTimeSelection.Param_WeightType;

            filter.stationID = Convert.ToInt16(dtpDateTimeSelection.StationID);
            filter.stationName = dtpDateTimeSelection.Param_StationName;

            GetReport(Convert.ToInt16(hwimid.Value), filter);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            dtpDateTimeSelection.clear();
        }

        public void GetReport(int id, DataFilter filter)
        {
            try
            {
                if (id == 101)
                {
                    ShowReport_WIM_101(filter);
                }
                else if (id == 102)
                {
                    ShowReport_WIM_102(filter);
                }
                else if (id == 103)
                {
                    ShowReport_WIM_103(filter);
                }
                else if (id == 104)
                {
                    ShowReport_WIM_104(filter);
                }
                else if (id == 201)
                {
                    ShowReport_WIM_201(filter);
                }
                else if (id == 202)
                {
                    ShowReport_WIM_202(filter);
                }
                else if (id == 301)
                {
                    ShowReport_WIM_301(filter);
                }
                else if (id == 302)
                {
                    ShowReport_WIM_302(filter);
                }
                else if (id == 303)
                {
                    ShowReport_WIM_303(filter);
                }
                else if (id == 304)
                {
                    ShowReport_WIM_304(filter);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("ERROR Getting Report\r\n\r\n" + ex.Message);
            }

        }

        void ShowReport(string reportSource, DataTable dataSource, ReportParameterCollection reportParameters)
        {
            if (dataSource.Rows.Count > 0)
            {

                var rptPath = Server.MapPath("~/ReportObjects//" + reportSource);


                ReportDataSource rptDataSource = new ReportDataSource("DataSet1", dataSource);

                Session["reportSource"] = reportSource;
                Session["rptDataSource"] = rptDataSource;
                Session["reportParameters"] = reportParameters;

                Response.Redirect("ReportWimViewer.aspx");
            }
            else {
                label_status.Text = "ไม่พบข้อมูล";
            }

        }

        void ShowReport_WIM_101(DataFilter filter)
        {
            string report = "REP_WIM_101.rdlc";
          
            string sql = WIMSQLText.GetQuery_REP_WIM_101(filter.startDate, filter.endDate, filter.stationID);
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WIM_102(DataFilter filter)
        {
            string report = "REP_WIM_102.rdlc";

            string sql = WIMSQLText.GetQuery_REP_WIM_102(filter.startDate, filter.endDate, filter.stationID);
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WIM_103(DataFilter filter)
        {
            string report = string.Empty;
            string sql = string.Empty;

            if (filter.summaryType == "ชั่วโมง")
            {
                report = "REP_WIM_1031.rdlc";
                sql = WIMSQLText.GetQuery_REP_WIM_1031(filter.startDate, filter.endDate, filter.stationID);
            }
            else if (filter.summaryType == "วัน")
            {
                report = "REP_WIM_1032.rdlc";
                sql = WIMSQLText.GetQuery_REP_WIM_1032(filter.startDate, filter.endDate, filter.stationID);
            }
            else if (filter.summaryType == "เดือน")
            {
                report = "REP_WIM_1033.rdlc";
                sql = WIMSQLText.GetQuery_REP_WIM_1033(filter.startDate, filter.endDate, filter.stationID);
            }
            else if (filter.summaryType == "ปี")
            {
                report = "REP_WIM_1034.rdlc";
                sql = WIMSQLText.GetQuery_REP_WIM_1034(filter.startDate, filter.endDate, filter.stationID);
            }


            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WIM_104(DataFilter filter)
        {
            string report = "REP_WIM_104.rdlc";

            string sql = WIMSQLText.GetQuery_REP_WIM_104(filter.startDate, filter.endDate, filter.stationID);
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WIM_201(DataFilter filter)
        {
            string report = "REP_WIM_201.rdlc";

            string sql = WIMSQLText.GetQuery_REP_WIM_201(filter.startDate, filter.endDate, filter.stationID);
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WIM_202(DataFilter filter)
        {
            string report = string.Empty;
            string sql = string.Empty;

            if (filter.summaryType == "ชั่วโมง")
            {
                report = "REP_WIM_2021.rdlc";
                sql = WIMSQLText.GetQuery_REP_WIM_2021(filter.startDate, filter.endDate, filter.stationID);
            }
            else if (filter.summaryType == "วัน")
            {
                report = "REP_WIM_2022.rdlc";
                sql = WIMSQLText.GetQuery_REP_WIM_2022(filter.startDate, filter.endDate, filter.stationID);
            }
            else if (filter.summaryType == "เดือน")
            {
                report = "REP_WIM_2023.rdlc";
                sql = WIMSQLText.GetQuery_REP_WIM_2023(filter.startDate, filter.endDate, filter.stationID);
            }
            else if (filter.summaryType == "ปี")
            {
                report = "REP_WIM_2024.rdlc";
                sql = WIMSQLText.GetQuery_REP_WIM_2024(filter.startDate, filter.endDate, filter.stationID);
            }
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WIM_301(DataFilter filter)
        {
            string report = "REP_WIM_301.rdlc";

            string sql = WIMSQLText.GetQuery_REP_WIM_30X(filter.startDate, filter.endDate, filter.stationID);
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WIM_302(DataFilter filter)
        {
            string report = "REP_WIM_302.rdlc";

            string sql = WIMSQLText.GetQuery_REP_WIM_30X(filter.startDate, filter.endDate, filter.stationID);
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WIM_303(DataFilter filter)
        {
            string report = "REP_WIM_303.rdlc";

            string sql = WIMSQLText.GetQuery_REP_WIM_30X(filter.startDate, filter.endDate, filter.stationID);
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }

        void ShowReport_WIM_304(DataFilter filter)
        {
            string report = "REP_WIM_304.rdlc";

            string sql = WIMSQLText.GetQuery_REP_WIM_30X(filter.startDate, filter.endDate, filter.stationID);
            DataTable table = Util.GetDataTable(sql);

            ReportParameterCollection reportParameters = new ReportParameterCollection();
            string subtitle = filter.dateTimeText + " " + filter.stationName;
            reportParameters.Add(new ReportParameter("Subtitle", subtitle));

            ShowReport(report, table, reportParameters);
        }
    }
}