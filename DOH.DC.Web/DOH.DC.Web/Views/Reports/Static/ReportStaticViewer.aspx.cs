﻿using System;
using CrystalDecisions.CrystalReports.Engine;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Reports.Static
{
    public partial class ReportStaticViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");

            try
            {
                ReportDocument rptDoc = (ReportDocument)Session["RptDoc"];
                if (rptDoc != null)
                {            

                    CrystalReportViewer1.ReportSource = rptDoc;
                    CrystalReportViewer1.DataBind();

                }
                else
                {
                    //logger.Debug("rptDoc is null.");
                }
            }
            catch (Exception ex)
            {
                //logger.Debug(ex.Message);
                //logger.Error(ex.StackTrace);
            }
        }
    }
}