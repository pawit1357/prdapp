﻿using System;
using System.Web.UI;
using System.Data;
using DOH.DC.Dao;
using CrystalDecisions.CrystalReports.Engine;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Reports.Static
{
    public partial class Static_Report_04 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");

            if (!Page.IsPostBack)
            {
                initial();
            }
        }

        private void initial()
        {
            dtpDateTimeSelection.setTitle("STATIC SCALE - รายงานสรุป");
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            dtpDateTimeSelection.Summarize();

            DataTable weighingRecord = new DataTable();

            if (dtpDateTimeSelection.HasStartDate())
            {
                if (dtpDateTimeSelection.HasEndDate())
                {
                    weighingRecord = WDCWeighingDataOLDSchema.GetWeighingRecord_By_DateRange(dtpDateTimeSelection.startDate, dtpDateTimeSelection.endDate, dtpDateTimeSelection.StationID);
                }
                else
                {
                    weighingRecord = WDCWeighingDataOLDSchema.GetWeighingRecord_By_StartDate(dtpDateTimeSelection.startDate, dtpDateTimeSelection.StationID);
                }
            }
            else
            {
                if (dtpDateTimeSelection.HasEndDate())
                {
                    weighingRecord = WDCWeighingDataOLDSchema.GetWeighingRecord_By_EndDate(dtpDateTimeSelection.endDate, dtpDateTimeSelection.StationID);
                }
                else
                {
                    weighingRecord = WDCWeighingDataOLDSchema.Weighing.DataTable;
                }
            }
            //dtpDateTimeSelection.Param_WeightType

            if (weighingRecord.Rows.Count == 0 || dtpDateTimeSelection.SummaryType == string.Empty)
            {
                label_status.Text = "ไม่พบข้อมูล";
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/ReportObjects/Static/RPT_Summary.rpt"));

                report.SetDataSource(weighingRecord);
                report.SetParameterValue("SiteName", GlobalConfiguration.StationNamePrefix + "" + dtpDateTimeSelection.Param_StationName);
                report.SetParameterValue("FilteredTime", dtpDateTimeSelection.SummaryType);

                
                // Set Report Hidden Area
                if (dtpDateTimeSelection.SummaryType == "ปี")
                {
                    report.ReportDefinition.Sections["ReportFooterA1"].SectionFormat.EnableSuppress = false;
                    report.ReportDefinition.Sections["ReportFooterA2"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA3"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA4"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA5"].SectionFormat.EnableSuppress = true;

                    report.ReportDefinition.Sections["ReportFooterB1"].SectionFormat.EnableSuppress = false;
                    report.ReportDefinition.Sections["ReportFooterB2"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB3"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB4"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB5"].SectionFormat.EnableSuppress = true;
                }
                else if (dtpDateTimeSelection.SummaryType == "เดือน")
                {
                    report.ReportDefinition.Sections["ReportFooterA1"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA2"].SectionFormat.EnableSuppress = false;
                    report.ReportDefinition.Sections["ReportFooterA3"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA4"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA5"].SectionFormat.EnableSuppress = true;

                    report.ReportDefinition.Sections["ReportFooterB1"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB2"].SectionFormat.EnableSuppress = false;
                    report.ReportDefinition.Sections["ReportFooterB3"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB4"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB5"].SectionFormat.EnableSuppress = true;
                }
                else if (dtpDateTimeSelection.SummaryType == "วัน")
                {
                    report.ReportDefinition.Sections["ReportFooterA1"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA2"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA3"].SectionFormat.EnableSuppress = false;
                    report.ReportDefinition.Sections["ReportFooterA4"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA5"].SectionFormat.EnableSuppress = true;

                    report.ReportDefinition.Sections["ReportFooterB1"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB2"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB3"].SectionFormat.EnableSuppress = false;
                    report.ReportDefinition.Sections["ReportFooterB4"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB5"].SectionFormat.EnableSuppress = true;

                }
                else if (dtpDateTimeSelection.SummaryType == "ชั่วโมง")
                {
                    report.ReportDefinition.Sections["ReportFooterA1"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA2"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA3"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA4"].SectionFormat.EnableSuppress = false;
                    report.ReportDefinition.Sections["ReportFooterA5"].SectionFormat.EnableSuppress = true;

                    report.ReportDefinition.Sections["ReportFooterB1"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB2"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB3"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB4"].SectionFormat.EnableSuppress = false;
                    report.ReportDefinition.Sections["ReportFooterB5"].SectionFormat.EnableSuppress = true;

                }
                else if (dtpDateTimeSelection.SummaryType == "นาที")
                {
                    report.ReportDefinition.Sections["ReportFooterA1"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA2"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA3"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA4"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterA5"].SectionFormat.EnableSuppress = false;

                    report.ReportDefinition.Sections["ReportFooterB1"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB2"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB3"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB4"].SectionFormat.EnableSuppress = true;
                    report.ReportDefinition.Sections["ReportFooterB5"].SectionFormat.EnableSuppress = false;

                }    
         
                Session["RptDoc"] = report;
                Response.Redirect("ReportStaticViewer.aspx");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            dtpDateTimeSelection.clear();
        }
    }
}