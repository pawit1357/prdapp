﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.DC.Dao;
using System.Data;
using DOH.DC.Utils;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace DOH.DC.Web.Views.Reports.Static
{
    public partial class Static_Report_01 : System.Web.UI.Page
    {

        DataTable weighingRecord = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");

            if (!Page.IsPostBack)
            {
                initial();
            }
        }

        private void initial()
        {
            dtpDateTimeSelection.setTitle("STATIC SCALE - รายงานรถเข้าด่านชั่งรายคัน");
        }


        protected void btnOK_Click(object sender, EventArgs e)
        {
            dtpDateTimeSelection.Summarize();

            try
            {
                weighingRecord = WDCWeighingDataOLDSchema.GetWeighingView_By_Variables(dtpDateTimeSelection.startDate,
                    dtpDateTimeSelection.endDate, dtpDateTimeSelection.IsWeightOver, dtpDateTimeSelection.UserID, dtpDateTimeSelection.StationID);

            }
            catch (Exception)
            {
                // MessageBox.Show("ERROR Loading data for Datalist report\r\n\r\n" + ex.Message);
            }

            if (weighingRecord.Rows.Count == 0)
            {
                //isToClose = false;
                label_status.Text = "ไม่พบข้อมูล";

            }
            else
            {
                Session["SearchResult"] = weighingRecord;
                //Show Gridview
                gvResult.DataSource = weighingRecord;
                gvResult.DataBind();

            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            dtpDateTimeSelection.clear();
        }

        protected void gvResult_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            CommandNameEnum command = (CommandNameEnum)Enum.Parse(typeof(CommandNameEnum), e.CommandName, true);
            switch (command)
            {
                case CommandNameEnum.View:

                    dtpDateTimeSelection.Summarize();

                    long fldTransID = Convert.ToInt64(e.CommandArgument.ToString());


                    DataTable weighingRecord = WDCWeighingDataOLDSchema.GetWeighingView_By_ENFID(fldTransID);


                    string imgURL = GlobalConfiguration.ImageWebPath;

                    DataRow weighingRow = weighingRecord.Rows[0];
                    // Set TruckClass Image
                    try
                    {

                        string vc = string.Empty;
                        vc = ((string)weighingRow["fldTruckClass"]).ToString();
                        String vImgPath = String.Format(GlobalConfiguration.vclassImagePath, vc);
                        weighingRow["fldClassImage"] = Function.getImageByte(vImgPath);
                    }
                    catch { }

                    String fldLPRImagePath = (string)weighingRow["fldLPRImagePath"];
                    if (!String.IsNullOrEmpty(Path.GetExtension(fldLPRImagePath)))
                    {
                        string[] imgArgs = fldLPRImagePath.Split('-');
                        fldLPRImagePath = Path.Combine(String.Format(GlobalConfiguration.imageFilePath, imgArgs[1], imgArgs[2].Substring(0, 4), imgArgs[2].Substring(4, 2), imgArgs[2].Substring(6, 2), imgArgs[0].ToUpper()), fldLPRImagePath);
                    }
                    else
                    {
                        fldLPRImagePath = AppDomain.CurrentDomain.BaseDirectory + "\\images\\inf.jpg";
                    }
                    // Set Truck Image
                    String fldTruckImagePath = (string)weighingRow["fldTruckImagePath"];
                    if (!String.IsNullOrEmpty(Path.GetExtension(fldTruckImagePath)))
                    {
                        string[] imgArgs = fldTruckImagePath.Split('-');
                        fldTruckImagePath = Path.Combine(String.Format(GlobalConfiguration.imageFilePath, imgArgs[1], imgArgs[2].Substring(0, 4), imgArgs[2].Substring(4, 2), imgArgs[2].Substring(6, 2), imgArgs[0].ToUpper()), fldTruckImagePath);
                    }
                    else
                    {
                        fldTruckImagePath = AppDomain.CurrentDomain.BaseDirectory + "\\images\\inf.jpg";
                    }
                    weighingRow["fldTruckImage"] = Function.getImageByte(fldTruckImagePath);


                    // Set Comply Method
                    if (weighingRow["fldComplyMethod"].ToString() == "0")
                    {
                        weighingRow["fldComplyMethod"] = "GVW";
                    }
                    else if (weighingRow["fldComplyMethod"].ToString() == "1")
                    {
                        weighingRow["fldComplyMethod"] = "AGL";
                    }

                    // Set Save Method
                    if (weighingRow["fldSaveMethod"].ToString() == "0")
                    {
                        weighingRow["fldSaveMethod"] = "Manual";
                    }
                    else if (weighingRow["fldSaveMethod"].ToString() == "1")
                    {
                        weighingRow["fldSaveMethod"] = "Auto";
                    }

                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/ReportObjects/Static/RPT_TruckDetails.rpt"));
                    report.SetDataSource(weighingRecord);
                    report.SetParameterValue("SiteName", dtpDateTimeSelection.Param_StationName);
                    report.SetParameterValue("LPRImagePath", fldLPRImagePath);
                    report.SetParameterValue("WIMWeight", (int)weighingRow["fldGVW_Weight_WIM"]);




                    Session["RptDoc"] = report;
                    Response.Redirect("ReportStaticViewer.aspx");
                    break;
            }

        }

        protected void gvResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex < 0) return;
            GridView gv = (GridView)sender;
            gv.DataSource = Session["SearchResult"];
            gv.PageIndex = e.NewPageIndex;
            gv.DataBind();
        }


    }
}