﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using DOH.DC.Dao;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Reports.Static
{
    public partial class Static_Report_03 : System.Web.UI.Page
    {
        List<ShiftItem> list = new List<ShiftItem>();
        private class ShiftItem
        {
            public int ShiftID { get; set; }
            public string ShiftDescription { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");

            if (!Page.IsPostBack)
            {
                initial();
            }
        }

        private void initial()
        {

            this.dtDataDate.Text = DateTime.Now.ToString("dddd d MMMMyyyy");

            StationDao dao = new StationDao();

            ddlStation.Items.Clear();
            ddlStation.DataSource = dao.getAll();
            ddlStation.DataBind();

            DataTable table = WDCSiteParam.GetTable_ShiftView();
            foreach (DataRow r in table.Rows)
            {
                ShiftItem st = new ShiftItem();
                st.ShiftID = (int)r["ShiftID"];
                st.ShiftDescription = (string)r["ShiftDescription"];
                list.Add(st);
            }
            comboBox_Shift.Items.Clear();
            comboBox_Shift.DataSource = list;
            comboBox_Shift.DataBind();

            lbTitle.Text = "STATIC SCALE - รายงานรถเข้าด่านชั่งรายผลัด";
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            int shiftID = Convert.ToInt16(comboBox_Shift.SelectedValue);

            DataTable sourceTable = WDCWeighingDataOLDSchema.GetWeighingView_By_Date_PeriodID(Convert.ToDateTime(this.dtDataDate.Text), shiftID, ddlStation.SelectedValue);
            if (sourceTable.Rows.Count > 0)
            {

                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/ReportObjects/Static/RPT_ByPeriod.rpt"));

                int n1 = 0;
                int n2 = 0;
                foreach (DataRow r in sourceTable.Rows)
                {
                    // Overrided
                    r["fldWeightGroup1"] = r["fldGVW_Weight_WIM"];

                    if (r["fldIsOverWeight"].ToString().StartsWith("Y"))
                    {
                        n1 = n1 + 1;
                    }
                    else
                    {
                        n2 = n2 + 1;
                    }
                }

                string userFirstName = string.Empty;
                string userLastName = string.Empty;

                DataTable userview = WDCSiteParam.GetTable_UserView(ddlStation.SelectedValue);

                foreach (DataRow row in userview.Rows)
                {
                    if ((int)row["ShiftID"] == Convert.ToInt16(comboBox_Shift.SelectedValue))
                    {
                        userFirstName = (string)row["FirstName"];
                        userLastName = (string)row["LastName"];
                        break;
                    }
                }

                DateTime d1 = (DateTime)sourceTable.Rows[0]["fldTransDateTime"];
                DateTime d2 = (DateTime)sourceTable.Rows[sourceTable.Rows.Count - 1]["fldTransDateTime"];

                report.SetDataSource(sourceTable);
                report.SetParameterValue("SiteName", GlobalConfiguration.StationNamePrefix + " " + ddlStation.SelectedItem.Text);
                report.SetParameterValue("UserFirstName", userFirstName);
                report.SetParameterValue("UserLastName", userLastName);
                report.SetParameterValue("TimeBegin", d1);
                report.SetParameterValue("TimeEnd", d2);
                report.SetParameterValue("TruckCount", n1 + n2);
                report.SetParameterValue("OverWeightTruck", n1);
                report.SetParameterValue("NotOverWeightTruck", n2);
                report.SetParameterValue("FormatedPeriodID", comboBox_Shift.SelectedValue);

                Session["RptDoc"] = report;
                Response.Redirect("ReportStaticViewer.aspx");
            }
            else
            {
                label_status.Text = "ไม่พบข้อมูล";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            initial();
        }
    }
}