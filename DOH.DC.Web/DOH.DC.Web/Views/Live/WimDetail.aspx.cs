﻿using System;
using System.Web;
using System.Web.UI;
using DOH.DC.Dao;
using DOH.DC.Model;
using System.IO;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Live
{
    public partial class WimDetail : System.Web.UI.Page
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(WimDetail));

        protected View_WIM model
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                String id = Request.QueryString["WIMID"];
                model = WimDao.getById(id);
                if (model != null)
                {
                    /*
                    String imgPath = "";
                    if (!String.IsNullOrEmpty(Path.GetExtension(model.Image01Name)))
                    {
                        string[] imgArgs = model.Image01Name.ToString().Split('-');
                        String ipAddr = (String)HttpContext.Current.Session["RequestIP"];


                        if (ipAddr.Substring(0, 12).Equals("192.168.100."))
                        {
                            imgPath = String.Format(GlobalConfiguration.imageWebPathLocal, imgArgs[1], imgArgs[2].Substring(0, 4), imgArgs[2].Substring(4, 2), imgArgs[2].Substring(6, 2), imgArgs[0].ToUpper(), model.Image01Name);
                        }
                        else
                        {
                            imgPath = String.Format(GlobalConfiguration.ImageWebPath, imgArgs[1], imgArgs[2].Substring(0, 4), imgArgs[2].Substring(4, 2), imgArgs[2].Substring(6, 2), imgArgs[0].ToUpper(), model.Image01Name);
                        }
                        
                    }
                    else
                    {
                        imgPath = "http://" + HttpContext.Current.Request.Url.Authority + "/dc/images/inf.jpg";
                    }
                    */
                    //if (File.Exists("http://61.19.97.185/wim/images/inf.jpg"))
                    //{
                    //    Console.WriteLine("");
                    //}


                    String imgPath = Function.GetImageURL(model.Image01Name);
                    String img1Folder = Function.GetImageFolder(model.Image01Name) + "\\" + model.Image01Name;

                    if (File.Exists(img1Folder))
                    {
                        model.Image01Name = imgPath;
                    }
                    else
                    {
                        model.Image01Name = @"http://61.19.97.185/wim/images/inf.jpg";
                    }

                    String img2Path = Function.GetImageURL(Path.GetFileNameWithoutExtension(imgPath).Substring(0, Path.GetFileNameWithoutExtension(imgPath).Length - 1) + "9" + Path.GetExtension(imgPath));
                    String img2Folder = Function.GetImageFolder(img2Path) + "\\" + img2Path;
                    if (File.Exists(img1Folder))
                    {
                        model.Image02Name = img2Folder;
                    }
                    else
                    {
                        model.Image02Name = @"http://61.19.97.185/wim/images/inf.jpg";
                    }

                   
                    model.Image02Name = img2Path;
                    model.LicensePlateImageName = model.Image01Name;
                    model.classImg = "http://" + HttpContext.Current.Request.Url.Authority + "/wim/images/vclass/class" + model.VehicleClass + ".jpg";

                    l_wimid.Text = model.WIMID + "";
                    l_Station.Text = model.StationName;
                    l_Date.Text = model.TimeStamp.ToShortDateString() + "";
                    l_Time.Text = model.TimeStamp.ToShortTimeString() + "";
                    l_lane.Text = model.Lane + "";
                    l_VehicleNumber.Text = model.VehicleNumber + "";
                    l_VehicleClass.Text = model.VehicleClass + "";
                    l_AxleCount.Text = model.AxleCount + "";
                    l_Speed.Text = model.Speed + "";


                    l_wimGvw.Text = Function.customNumberFormat(model.GVW + "");
                    l_MaxGvw.Text = Function.customNumberFormat(model.MaxGVW + "");
                    l_Length.Text = model.Length + "";
                    l_FrontOver.Text = model.FrontOverHang + "";
                    l_RearOver.Text = model.RearOverHang + "";
                    l_esal.Text = model.ESAL + "";
                    l_error.Text = model.Error + "";
                    l_sortDecision.Text = Enum.Parse(typeof(VRSortDecisionCode), model.SortDecision.ToString()).ToString();
                    l_Status.Text = model.StatusCode + "";

                    model.isOver = (model.GVW > model.MaxGVW) ? true : false;

                    lbSep1.Text = Function.customNumberFormat(model.Axle01Seperation.ToString());
                    lbSep2.Text = Function.customNumberFormat(model.Axle02Seperation.ToString());
                    lbSep3.Text = Function.customNumberFormat(model.Axle03Seperation.ToString());
                    lbSep4.Text = Function.customNumberFormat(model.Axle04Seperation.ToString());
                    lbSep5.Text = Function.customNumberFormat(model.Axle05Seperation.ToString());
                    lbSep6.Text = Function.customNumberFormat(model.Axle06Seperation.ToString());
                    lbSep7.Text = Function.customNumberFormat(model.Axle07Seperation.ToString());

                    lbGroup1.Text = Function.customNumberFormat(model.Axle01Group.ToString());
                    lbGroup2.Text = Function.customNumberFormat(model.Axle02Group.ToString());
                    lbGroup3.Text = Function.customNumberFormat(model.Axle03Group.ToString());
                    lbGroup4.Text = Function.customNumberFormat(model.Axle04Group.ToString());
                    lbGroup5.Text = Function.customNumberFormat(model.Axle05Group.ToString());
                    lbGroup6.Text = Function.customNumberFormat(model.Axle06Group.ToString());
                    lbGroup7.Text = Function.customNumberFormat(model.Axle07Group.ToString());

                    lbWeight1.Text = Function.customNumberFormat(model.Axle01Weight.ToString());
                    lbWeight2.Text = Function.customNumberFormat(model.Axle02Weight.ToString());
                    lbWeight3.Text = Function.customNumberFormat(model.Axle03Weight.ToString());
                    lbWeight4.Text = Function.customNumberFormat(model.Axle04Weight.ToString());
                    lbWeight5.Text = Function.customNumberFormat(model.Axle05Weight.ToString());
                    lbWeight6.Text = Function.customNumberFormat(model.Axle06Weight.ToString());
                    lbWeight7.Text = Function.customNumberFormat(model.Axle07Weight.ToString());


                }
                else
                {
                    logger.Debug("WIMID IS NULL.");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

        }
    }
}