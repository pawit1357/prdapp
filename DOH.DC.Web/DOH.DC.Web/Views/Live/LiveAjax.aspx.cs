﻿using System;
using System.Web;
using DOH.DC.Dao;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.Services;
using System.Text;
using DOH.DC.Utils;
using System.IO;

namespace DOH.DC.Web.Views.Live
{
    public partial class LiveAjax : System.Web.UI.Page
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(LiveAjax));

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod()]
        public static string getWimLane1Live()
        {
            return getWimByLane(new DataFilters { lane = "0" });
        }
        [WebMethod()]
        public static string getWimLane2Live()
        {
            return getWimByLane(new DataFilters { lane = "1" });
        }

        private static String getWimByLane(DataFilters df)
        {
            String stationID = (String)HttpContext.Current.Session["stationID"];
            String sql = "Select Top 30 WIMID,TimeStamp,GVW,MaxGVW,StationName,StatusCode,Image01Name,VehicleNumber,Lane,VehicleClass,SortDecision from View_WIM Where Lane='" + df.lane + "'" +
                ((!String.IsNullOrEmpty(stationID)) ? " and StationID='" + stationID + "'" : "") +
                " order by TimeStamp desc";

            StringBuilder div = new StringBuilder();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DohConStr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(sql.ToString(), connection))
                {
                    command.Notification = null;

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    int running = 1;


                    while (dr.Read())
                    {
                        div.Append("<table style=\"background-color:Gray\" width=\"516px\" border=\"0\"><tr>");
                        div.Append("<td bgcolor=\"#" + ((Convert.ToInt32(dr["GVW"].ToString()) > Convert.ToInt32(dr["MaxGVW"].ToString())) ? "FF0000" : "42D756") + "\"> </td>");

                        //img 1
                        div.Append("<td>");
                        String imgPath = dr["Image01Name"].ToString();
                        String img1Folder = Function.GetImageFolder(imgPath) + "\\" + imgPath;
                        if (File.Exists(img1Folder))
                        {
                            div.Append("<a href=\"" + Function.GetImageURL(imgPath) + "\" class=\"pic" + (running + 1) + "  cboxElement\"  height=\"352\" width=\"228\" title=\"Img Front\">");
                            div.Append("<img src=\"" + Function.GetImageURL(imgPath) + "\"  height=\"55\" width=\"80\">");
                            div.Append("</a>");
                        }
                        else
                        {
                            div.Append("<a href=\"http://61.19.97.185/wim/images/inf.jpg\" class=\"pic" + (running + 1) + "  cboxElement\"  height=\"352\" width=\"228\" title=\"Img Front\">");
                            div.Append("<img src=\"http://61.19.97.185/wim/images/inf.jpg\"  height=\"55\" width=\"80\">");
                            div.Append("</a>");
                        }
                        div.Append("</td>");


                        //wim-02-20140927-062559-42538-0.jpg
                        div.Append("<td>");
                        if (!imgPath.Equals(""))
                        {
                            String img2Path = Function.GetImageURL(Path.GetFileNameWithoutExtension(imgPath).Substring(0, Path.GetFileNameWithoutExtension(imgPath).Length - 1) + "9" + Path.GetExtension(imgPath));
                            String img2Folder = Function.GetImageFolder(img2Path) + "\\" + img2Path;
                            if (File.Exists(img2Folder))
                            {
                                div.Append("<a href=\"" + Function.GetImageURL(img2Path) + "\" class=\"pic" + (running + 1) + "  cboxElement\"  height=\"352\" width=\"228\" title=\"Img Front\">");
                                div.Append("<img src=\"" + Function.GetImageURL(img2Path) + "\"  height=\"55\" width=\"80\">");
                                div.Append("</a>");
                            }
                            else
                            {
                                div.Append("<a href=\"http://61.19.97.185/wim/images/inf.jpg\" class=\"pic" + (running + 1) + "  cboxElement\"  height=\"352\" width=\"228\" title=\"Img Front\">");
                                div.Append("<img src=\"http://61.19.97.185/wim/images/inf.jpg\"  height=\"55\" width=\"80\">");
                                div.Append("</a>");
                            }
                            div.Append("</td>");
                        }
                        //if (!imgPath.Equals(""))
                        //{
                        //    String img2Path = Function.GetImageURL(Path.GetFileNameWithoutExtension(imgPath).Substring(0, Path.GetFileNameWithoutExtension(imgPath).Length - 1) + "9" + Path.GetExtension(imgPath));

                        //    div.Append("<td><a href=\"" + img2Path + "\" class=\"pic" + (running + 1) + "  cboxElement\"  height=\"352\" width=\"228\" title=\"Img Front\">");
                        //    div.Append("<img src=\"" + img2Path + "\"  height=\"55\" width=\"80\">");
                        //}

                        //div.Append("</a></td>");
                        div.Append("<td>");

                        //div.Append("<table id=\"Wim" + running + "\" onclick=\"window.location ='WimDetail.aspx?WIMID=" + dr["WIMID"] + "'\" border=\"0\">");
                        div.Append("<table id=\"Wim" + running + "\"  width=\"400px\"  onclick=\"window.open('WimDetail.aspx?WIMID=" + dr["WIMID"] + "', '_blank', 'toolbar=0,location=0,menubar=0,height=420,width=860,resizable=false')\" >");
                        div.Append("<tr><td>" + dr["StationName"] + "</td><td>" + dr["TimeStamp"] + "</td></tr>");
                        div.Append("<tr><td>(" + dr["VehicleNumber"] + ")</td><td>Sort: " + Enum.Parse(typeof(VRSortDecisionCode), dr["SortDecision"].ToString()).ToString() + "</td></tr>");
                        div.Append("<tr><td>Lane: " + (Convert.ToInt16(dr["Lane"]) + 1) + "</td><td>Max: " + Function.customNumberFormat(dr["MaxGvw"].ToString()) + "</td></tr>");
                        div.Append("<tr><td>Class: " + dr["VehicleClass"] + "</td><td>GVW: " + Function.customNumberFormat(dr["Gvw"].ToString()) + "</td></tr>");

                        div.Append("</table>");


                        div.Append("</td></tr>");
                        div.Append("</table><br>");
                        running++;
                    }

                    dr.Close();
                }
            }
            return div.ToString();
        }


    }
}