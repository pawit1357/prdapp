﻿using System;
using System.Web;
using System.Web.UI;
using DOH.DC.Dao;
using DOH.DC.Model;
using System.IO;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Live
{
    public partial class WarningDetail : System.Web.UI.Page
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(WarningDetail));

        protected RadarDetection model
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                String id = Request.QueryString["RefID"];
                String lane = Request.QueryString["type"];
                model = WarningDao.getById(id);
                if (model != null)
                {

                    switch (lane)
                    {
                        case "1":
                            model.BorderColor = "#FFFF00";
                            break;
                        case "2":
                            model.BorderColor = "#FF9900";
                            break;
                        case "3":
                            model.BorderColor = "#FF0000";
                            break;
                        case "4":
                            model.BorderColor = "#660099";
                            break;
                        default:
                            model.BorderColor = "#FFFFFF";
                            break;
                    }

                    String imgPath = "";
                    if (!String.IsNullOrEmpty(Path.GetExtension(model.ImageName)))
                    {
                        string[] imgArgs = model.ImageName.ToString().Split('-');
                        String ipAddr = (String)HttpContext.Current.Session["RequestIP"];


                        if (ipAddr.Substring(0, 12).Equals("192.168.100."))
                        {
                            imgPath = String.Format(GlobalConfiguration.imageWebPathLocal, imgArgs[1], imgArgs[2].Substring(0, 4), imgArgs[2].Substring(4, 2), imgArgs[2].Substring(6, 2), imgArgs[0].ToUpper(), model.ImageName);
                        }
                        else
                        {
                            imgPath = String.Format(GlobalConfiguration.ImageWebPath, imgArgs[1], imgArgs[2].Substring(0, 4), imgArgs[2].Substring(4, 2), imgArgs[2].Substring(6, 2), imgArgs[0].ToUpper(), model.ImageName);
                        }
                        
                    }
                    else
                    {
                        imgPath = "http://" + HttpContext.Current.Request.Url.Authority + "/dc/images/inf.jpg";
                    }
                    model.ImageName = imgPath;
                    //model.LicensePlateImageName = model.Image01Name;
                    model.classImg = "http://" + HttpContext.Current.Request.Url.Authority + "/dc/images/vclass/class" + model.Class + ".jpg";

                    l_wimid.Text = model.RSID + "";
                    l_Station.Text = model.StationName + "";
                    l_Date.Text = model.TimeStamp.ToShortDateString() + "";
                    l_Time.Text = model.TimeStamp.ToShortTimeString() + "";
                    l_lane.Text = model.SeqNumber + "";
                    l_VehicleNumber.Text = model.LaneID + "";


                    l_wimGvw.Text = model.Range + "";
                    l_MaxGvw.Text = model.Duration + "";
                    l_Length.Text = model.Speed + "";
                    l_FrontOver.Text = model.Class + "";
                    l_RearOver.Text = model.Length + "";
                    l_esal.Text = model.MinLength + "";
                }
                else
                {
                    logger.Debug("WIMID IS NULL.");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

        }
    }
}