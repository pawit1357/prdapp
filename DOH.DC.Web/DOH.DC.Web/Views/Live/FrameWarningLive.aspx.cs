﻿using System;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Live
{
    public partial class FrameWarningLive : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");
        }
    }
}