﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrameWarningLive.aspx.cs"
    Inherits="DOH.DC.Web.Views.Live.FrameWarningLive" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../Styles/colorbox.css" />
    <script type="text/javascript" src="../../Scripts/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Scripts/LiveWarning.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery.colorbox.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".pic1").colorbox({ rel: 'pic1' });
        });
    </script>
</head>
<body style="background-color: #949494">
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    [1] AVOIDING
                </td>
                <td>
                    [2] RUNNING-SCALE
                </td>
                <td>
                    [3] OVER-WEIGHT
                </td>
                <td>
                    [4] BARRIER-GATE-OPENED
                </td>
            </tr>
            <tr>
                <td style="width: 340px; height: 500px; overflow-y: scroll;">
                    <div id="tbReport1">
                    </div>
                </td>
                <td style="width: 340px; height: 500px; overflow-y: scroll;">
                    <div id="tbReport2">
                    </div>
                </td>
                <td style="width: 340px; height: 500px; overflow-y: scroll;">
                    <div id="tbReport3">
                    </div>
                </td>
                <td style="width: 340px; height: 500px; overflow-y: scroll;">
                    <div id="tbReport4">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
