﻿using System;
using DOH.DC.Utils;
using System.Web;

namespace DOH.DC.Web.Views.Live
{
    public partial class FrameLive : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");


                lbStation.Text = (String)HttpContext.Current.Session["stationName"];
                if (lbStation.Text.Equals(""))
                {
                    lbStation.Text = "All Station";
                }


        }

    }
}