﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="StaticDetail.aspx.cs"
    Inherits="DOH.DC.Web.Views.Live.StaticDetail" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head id="Head1" runat="server">
    <title>- กรมทางหลวง -</title>
    <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, chrome=1" />
    <link rel="Stylesheet" href="/dc/Styles/Input.css" />
    <link rel="Stylesheet" href="/dc/Styles/Main.css" />
    <link rel="Stylesheet" href="/dc/Scripts/jquery-ui-1.10.3/themes/base/jquery-ui.css" />
    <link rel="Stylesheet" href="/dc/Styles/jquery.dropdown.css" />
    <link rel="stylesheet" href="/dc/Styles/colorbox.css" />
    <script type="text/javascript" src="/dc/Scripts/jquery-ui-1.10.3/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="/dc/Scripts/jquery-ui-1.10.3/ui/jquery-ui.js"></script>
    <script type="text/javascript" src="/dc/Scripts/jquery.dropdown.js"></script>
    <script type="text/javascript" src="/dc/Scripts/jquery.colorbox.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".pic1").colorbox({ rel: 'pic1' });
        });
    </script>
</head>
<body>
    <fieldset>
        <% if (model != null)
           { %>
        <table>
            <tr>
                <%
           if (model.IsOverWeight.Equals("Y"))
           { 
                %>
                <td bgcolor="#FF0000">
                    &nbsp;
                </td>
                <%
           }               
                %>
                <td width="15%" style="padding: 10px;">
                    <a href="<%= model.LPR_ImageName %>" class="pic1" title="Img Front">
                        <img alt="" border="0" src="<%= model.LPR_ImageName%>" width="200" /></a><br />
                    <br />
                    <a href="<%= model.LPR_ImageName %>" class="pic1" title="Img Front">
                        <img alt="" border="0" src="<%= model.LPR_ImageName%>" width="200" /></a>
                </td>
                <td valign="top">
                    <table width="100%" class="tbl1">
                        <tr>
                            <th class="style9">
                                &nbsp;
                            </th>
                            <th class="style3">
                                &nbsp;
                            </th>
                            <th class="style9">
                                &nbsp;
                            </th>
                            <th class="style11" style="text-align: left">
                                &nbsp;
                            </th>
                        </tr>
                        <tr>
                            <td class="style7">
                                <strong>ENFID</strong>
                            </td>
                            <td class="style3">
                                <asp:Label ID="l_enfid" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="style9">
                                <strong>WIMID</strong>
                            </td>
                            <td class="style3">
                                <asp:Label ID="l_wimid" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style7">
                                <strong style="text-align: center">Station</strong>
                            </td>
                            <td class="style3">
                                <asp:Label ID="l_Station" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="style9">
                                <strong>WIM Gvw</strong>
                            </td>
                            <td class="style3">
                                <asp:Label ID="l_WimGvw" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style7">
                                <strong style="text-align: center">Date</strong>
                            </td>
                            <td class="style3">
                                <asp:Label ID="l_Date" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="ui-priority-primary">
                                Static GVW
                            </td>
                            <td class="style3">
                                <asp:Label ID="l_StaticGvw" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style7">
                                <strong style="text-align: center">Time</strong>
                            </td>
                            <td class="style3">
                                <asp:Label ID="l_Time" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="ui-priority-primary">
                                Max GVW
                            </td>
                            <td class="style3">
                                <asp:Label ID="l_MaxGvw" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style8">
                                <strong style="text-align: center">Seq Number</strong>
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_SeqNumber" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="ui-priority-primary">
                                Status
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_Status" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style8">
                                <b>User
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_User" runat="server" Text="-"></asp:Label>
                                </b>
                            </td>
                            <td class="ui-priority-primary">
                                Barrier
                            </td>
                            <td class="ui-priority-primary">
                                <asp:Label ID="l_Barrier" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style8">
                                <strong style="text-align: center">Vehicle Class</strong>
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_VehicleClass" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="ui-priority-primary">
                                Goods
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_good" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style8">
                                <strong style="text-align: center">LP Number</strong>
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_LpNumber" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="ui-priority-primary">
                                Departed
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_Departed" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style8">
                                <strong style="text-align: center">Province</strong>
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_Province" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="ui-priority-primary">
                                Destination
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_Destination" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style8">
                                <strong style="text-align: center">Comply Method</strong>
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_ComplyMethod" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="ui-priority-primary">
                                Notes
                            </td>
                            <td class="style4">
                                <asp:Label ID="l_Notes" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style8">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="style10">
                                &nbsp;
                            </td>
                            <td class="number">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="style2">
                                <img alt="" border="0" src="<%= model.classImg%>" width="190" />
                            </td>
                            <td class="style2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                &nbsp;&nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/pdf.png"
                                    OnClick="ImageButton1_Click" Style="width: 16px" Visible="False" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <%}
           else
           { %>
        Not found data.
        <%} %>
    </fieldset>
</body>
</html>
