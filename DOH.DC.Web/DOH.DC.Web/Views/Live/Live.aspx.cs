﻿using System;
using DOH.DC.Utils;
using System.Web;

namespace DOH.DC.Web.Views.Live
{
    public partial class Live : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");

            this.myIframe.Attributes["src"] = "FrameLive.aspx";
        }
    }
}