﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOH.DC.Utils
{
    public class GlobalConfiguration
    {

        public static String ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["DohConStr"].ConnectionString;
            }
        }

        public static byte[] getLogo()
        {
            return Function.getImageByte(System.Web.Hosting.HostingEnvironment.MapPath("/dc/images/logo.jpg"));
        }
        public static String ImageWebPath
        {
            get { return ConfigurationManager.AppSettings["imageWebPath"]; }
        }
        public static String imageWebPathLocal
        {
            get { return ConfigurationManager.AppSettings["imageWebPathLocal"]; }
        }
        public static String imageFilePath
        {
            get { return ConfigurationManager.AppSettings["imageFilePath"]; }
        }
        public static String vclassImagePath
        {
            get { return ConfigurationManager.AppSettings["vclassImagePath"]; }
        }
        public static int StationID
        {
            get { return Convert.ToInt16(ConfigurationManager.AppSettings["StationID"]); }
        }
        public static String StationNamePrefix
        {
            get { return ConfigurationManager.AppSettings["StationNamePrefix"]; }
        }
        
        public static string getDefaultPage()
        {
            return "/dc/Default.aspx";
        }
    }
}
