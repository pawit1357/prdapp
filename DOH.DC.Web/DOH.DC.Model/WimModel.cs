﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DOH.DC.Model
{
    public class WimModel
    {
        public String imgpath { get; set; }
        public int wimId { get; set; }
        public String StationName { get; set; }
        public DateTime TimeStamp { get; set; }
        public int VehicleNumber { get; set; }
        public int Lane { get; set; }
        public int VehicleClass { get; set; }
        public String SortDecision { get; set; }
        public String MaxGvw { get; set; }
        public String Gvw { get; set; }
    }
}
