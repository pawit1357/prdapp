﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;
using DCC.Utility;
using DCC.DAL;
using System.Text;
using System.IO;


namespace DCC.VIEW
{
    public partial class Default : System.Web.UI.Page
    {
        //private String formName = "Default.aspx";
        private List<CListView> lists = new List<CListView>();
        private List<CListView> listsRef = new List<CListView>();

        protected void Page_Load(object sender, EventArgs e)
        {
            TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {
                    //set user detail
                    Navigator1.setDetail(DALUser.getUserDetail(user));
                    MyMenu1.setMenu(user);
                    Session["filePos"] = null;

                    #region "แสดง RootPath ของ User"
                    switch (user.USER_GROUP_ID)
                    {
                        case "001":
                        case "002":
                            addPath(Util.getDocumentPath());
                            break;
                        case "003":
                        case "004":
                            addPath(Util.getDocumentPath());
                            addPath(DALUser.getRootPath(user.USER_ID));
                            break;
                    }
                    #endregion

                    //แสดงปุ่ม
                    docEnableButton(true, true, false);

                    //แสดง Path
                    gvFolderList(getPath(false));
                    //แสดงเอกสารที่เกี่ยวข้อง
                    gvFolderList1(Util.getDocumentPath());
                    
                }
                else
                {
                    Response.Redirect("~/View/Login.aspx");
                }
            }

            #region "POPUP"
            CMD_UPLOAD.Attributes.Add("onclick", "callpopup('pages/DocManage/FrmUploadDoc.aspx')");
            CMD_NEW_FOLDER.Attributes.Add("onclick", "callpopup('pages/DocManage/FrmNewFolder.aspx')");
            CMD_REF.Attributes.Add("onclick", "callpopup('pages/DocManage/FrmSearch.aspx')");
            ImageButton2.Attributes.Add("onclick", "callpopup('pages/DocManage/FrmSearchDoc.aspx')");
            #endregion

            #region "เปิด/ปิด เอกสารที่เกี่ยวข้อง"
            if (Session["TBL_USERS"] == null)
            {
                Response.Redirect("~/View/Login.aspx");
            }
            switch (user.USER_GROUP_ID)
            {
                case "001":
                case "002":
                    Label5.Visible = false;
                    break;
                case "003":
                case "004":
                    if (GridView3.Rows.Count > 0)
                    {
                        Label5.Visible = true;
                    }
                    else
                    {
                        Label5.Visible = false;
                    }
                    break;
            }

            #endregion

        } // end page_load

        #region "Treeview Lists"
        private String getUserDoc(TBL_USERS user, Boolean bOwnerDoc, Boolean bDeptDoc, Boolean bOtherDoc, Boolean bRefDoc)
        {
            StringBuilder sb = new StringBuilder();
            List<String> doc = new List<String>(); ;
            using (DCCDBEntities context = new DCCDBEntities())
            {

                #region "รายการที่เป็นไฟล์ของตัวเอง"
                if (bOwnerDoc)
                {
                    List<TBL_DOCUMENT> docs = context.TBL_DOCUMENT.Where("it.[DOC_OWNER]='" + user.USER_ID + "'").ToList();
                    if (docs.Count > 0)
                    {
                        foreach (TBL_DOCUMENT tbldoc in docs)
                        {
                            doc.Add("" + tbldoc.DOC_FILE_NAME + "");
                        }
                    }
                }
                #endregion
                #region "รายการที่เป็นไฟล์ในแผนก"
                if (bDeptDoc)
                {
                    TBL_USERS_PROFILE uProfile = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID]='" + user.USER_ID + "'").First();
                    if (uProfile != null)
                    {
                        //หาไฟล์ภายใต้แผนกนี้
                        TBL_M_DEPARTMENT dept = context.TBL_M_DEPARTMENT.Where("it.[DEPARTMENT_ID]='" + uProfile.PROFILE_DEPARTMENT_ID + "'").First();
                        String depNAME = dept.DEPARTMENT_FOLDER.Split('/')[1];//
                        List<TBL_DOCUMENT> docs1 = context.TBL_DOCUMENT.ToList();
                        if (docs1.Count > 0)
                        {
                            foreach (TBL_DOCUMENT tbldoc in docs1)
                            {
                                String cDepName = tbldoc.DOC_FILE_NAME.Split('/')[2];
                                if (depNAME.Equals(cDepName))
                                {
                                    doc.Add("" + tbldoc.DOC_FILE_NAME + "");
                                }
                            }
                        }
                    }
                }
                #endregion
                #region "ไฟล์แผนกอื่น"
                if (bOtherDoc)
                {
                    List<TBL_DOCUMENT> docs1 = context.TBL_DOCUMENT.ToList();

                    if (docs1.Count > 0)
                    {
                        foreach (TBL_DOCUMENT tbldoc in docs1)
                        {
                            doc.Add("" + tbldoc.DOC_FILE_NAME + "");
                        }
                    }
                }
                #endregion
                #region "รายการที่เป็นไฟล์ที่เกี่ยวข้อง"
                if (bRefDoc)
                {
                    List<TBL_DOCUMENT_REF> docs2 = context.TBL_DOCUMENT_REF.Where("it.[REF_USER_ID]='" + user.USER_ID + "'").ToList();
                    if (docs2.Count > 0)
                    {
                        foreach (TBL_DOCUMENT_REF tbldoc in docs2)
                        {
                            List<TBL_DOCUMENT> docs1 = context.TBL_DOCUMENT.Where("it.[DOC_ID]='" + tbldoc.REF_DOC_ID + "'").ToList();
                            foreach (TBL_DOCUMENT dd in docs1)
                            {
                                doc.Add("" + dd.DOC_FILE_NAME + "");
                            }
                        }
                    }
                }
                #endregion

                #region "เอกสารที่ดูได้"
                List<String> docCanView = Util.distinct(doc);
                foreach (String s in docCanView)
                {
                    sb.Append(s);
                    sb.Append(',');
                }
                #endregion
            }

            return (sb.ToString().Length > 0) ? sb.ToString().Substring(0, sb.ToString().Length - 1) : "";
        }

        private void gvFolderList(String _folder)
        {

            DirectoryInfo rootDir = new DirectoryInfo(Server.MapPath(_folder));
            if (!rootDir.Exists)
            {
                Response.Redirect("~/View/");
            }
            //กำหนดสิทธิ์ในการดูเอกสาร
            TBL_USERS user = ((TBL_USERS)Session["TBL_USERS"]);
            String path = "";

            #region "แสดง Folder"
            DirectoryInfo[] SubDirectories = rootDir.GetDirectories();
            foreach (DirectoryInfo d in SubDirectories)
            {
                CListView clv = new CListView();
                clv.Type = "Folder";
                clv.Name = d.Name;
                clv.Title = "";
                clv.Modified = d.CreationTime.ToShortDateString();
                path = "~/" + d.Name + "/";
                switch (user.USER_GROUP_ID)
                {
                    case "001"://Admin
                    case "002"://Super
                        lists.Add(clv);
                        break;
                    case "003"://Owner  
                    case "004"://User
                        //ดึงข้อมูลเอกสารในแผนก                        
                        string fDir = (_folder + "/" + d.Name).Split('/')[2];
                        string uDir = DALUser.getUserDepFolder(user).Replace('/', ' ').Trim();
                        if (fDir.Equals(uDir))
                        {
                            lists.Add(clv);
                        }
                        //ดึงเอกสารที่เกี่ยวข้อง
                        String docs = getUserDoc(user, false, false, false, true);
                        if (docs.Length > 0)
                        {
                            if (docs.Contains(d.Name))
                            {
                                lists.Add(clv);
                            }
                        }
                        else
                        {
                        }
                        break;
                }
            }
            #endregion

            #region "แสดงไฟล์ใน Folder"
            FileInfo[] fis = rootDir.GetFiles();
            foreach (FileInfo f in fis)
            {
                CListView clv = new CListView();
                clv.Type = f.Extension;
                clv.Name = f.Name;
                clv.Title = "";
                clv.Modified = f.LastAccessTime.ToString();
                
                using(DCCDBEntities context= new DCCDBEntities())
                {
                    List<TBL_DOCUMENT> docModifiedBy = context.TBL_DOCUMENT.Where("it.[DOC_FILE_NAME]='" + getPath(false) + "/" + f.Name + "'").ToList();
                    if (docModifiedBy.Count > 0)
                    {
                        List<TBL_USERS_PROFILE> profiles = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID]='"+docModifiedBy[0].DOC_MODIFIED_BY+"'").ToList();
                        if (profiles.Count > 0)
                        {
                            clv.ModifiedBy =  profiles[0].PROFILE_NAME + "  " + profiles[0].PROFILE_SURNAME;
                        }                        
                    }
                }
                switch (user.USER_GROUP_ID)
                {
                    case "001"://Admin
                    case "002"://Super
                        lists.Add(clv);
                        break;
                    case "003"://Owner  
                    case "004"://User
                        //ดึงข้อมูลเอกสาร
                        String docs = getUserDoc(user, true, true, false, true);
                        if (docs.Length > 0)
                        {
                            if (docs.Contains(f.Name))
                            {
                                lists.Add(clv);
                            }
                        }
                        break;
                }
            }
            #endregion

            #region "นำข้อมูลใส่ใน tmp table"
            using (DCCDBEntities context = new DCCDBEntities())
            {

                List<TBL_FILEMANAGER> mdDelete = context.TBL_FILEMANAGER.Where("it.[FLAG]='0'").ToList();
                if (mdDelete.Count > 0)
                {
                    foreach (TBL_FILEMANAGER tmp in mdDelete)
                    {
                        context.DeleteObject(tmp);
                    }
                    //context.SaveChanges();
                }
                //insert
                foreach (CListView lv in lists)
                {
                    TBL_FILEMANAGER manager = new TBL_FILEMANAGER();
                    manager.Type = lv.Type;
                    manager.Name = lv.Name;
                    if (!lv.Modified.Equals(""))
                    {
                        manager.Modified = Convert.ToDateTime(lv.Modified);
                    }
                    manager.ModifiedBy = lv.ModifiedBy;
                    manager.USER_ID = user.USER_ID;
                    manager.SessionID = Util.TheSessionId();// Context.Request.ServerVariables["REMOTE_ADDR"];
                    manager.FLAG = "0";
                    context.TBL_FILEMANAGER.AddObject(manager);
                }
                if (context.SaveChanges() > 0)
                {

                }
                else
                {

                }
            }
            //แสดงเฉพาะ Folder ของ user ที่ login เข้าระบบ
            EntityDataSource1.AutoGenerateWhereClause = false;
            EntityDataSource1.Where = "it.[USER_ID]='" + user.USER_ID + "' AND it.[SessionID]='" + Util.TheSessionId() + "' AND it.[FLAG]='0'";
            #endregion

            #region "ล้าง session "
            //ล้าง session ของการค้นหาข้อมูล
            if (Session["findDoc"] != null)
            {
                Session["findDoc"] = null;
            }
            #endregion
        }

        //หาเอกสารที่เกี่ยวข้อง
        private void gvFolderList1(String _folder)
        {
            DirectoryInfo rootDir = new DirectoryInfo(Server.MapPath(_folder));
            //กำหนดสิทธิ์ในการดูเอกสาร
            TBL_USERS user = ((TBL_USERS)Session["TBL_USERS"]);
            #region "แสดง Folder"
            DirectoryInfo[] SubDirectories = rootDir.GetDirectories();
            foreach (DirectoryInfo d in SubDirectories)
            {
                #region "แสดงไฟล์ใน Folder"
                FileInfo[] fis = d.GetFiles();
                foreach (FileInfo f in fis)
                {
                    CListView clv = new CListView();
                    clv.Type = f.Extension;
                    clv.Name = _folder+"/"+d.Name+"/"+f.Name;
                    clv.Title = "";
                    clv.Modified = f.LastAccessTime.ToShortDateString();
                    using (DCCDBEntities context = new DCCDBEntities())
                    {
                        List<TBL_DOCUMENT> docModifiedBy = context.TBL_DOCUMENT.Where("it.[DOC_FILE_NAME]='" + getPath(false) + "/" + f.Name + "'").ToList();
                        if (docModifiedBy.Count > 0)
                        {
                            List<TBL_USERS_PROFILE> profiles = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID]='" + docModifiedBy[0].DOC_MODIFIED_BY + "'").ToList();
                            if (profiles.Count > 0)
                            {
                                clv.ModifiedBy =  profiles[0].PROFILE_NAME + "  " + profiles[0].PROFILE_SURNAME;
                            }
                        }
                    }
                    switch (user.USER_GROUP_ID)
                    {
                        case "003"://Owner  
                        case "004"://User
                            //ดึงเอกสารที่เกี่ยวข้องจะต้องไม่เป็นเอกสารในแผนก
                            String[] docs = getUserDoc(user, false, false, false, true).Split(',');
                            if (docs.Length > 0)
                            {
                                for (int i = 0; i < docs.Length; i++)
                                {
                                    if (docs[i].Equals(clv.Name))
                                    {
                                        listsRef.Add(clv);
                                    }
                                }
                            }
                            else
                            {
                            }
                            break;
                    }
                }
            }
            Console.WriteLine();
            #endregion

            #endregion
            #region "นำข้อมูลใส่ใน tmp table"
            using (DCCDBEntities context = new DCCDBEntities())
            {

                List<TBL_FILEMANAGER> mdDelete = context.TBL_FILEMANAGER.Where("it.[FLAG]='1'").ToList();
                if (mdDelete.Count > 0)
                {
                    foreach (TBL_FILEMANAGER tmp in mdDelete)
                    {
                        context.DeleteObject(tmp);
                    }
                    //context.SaveChanges();
                }
                //insert
                foreach (CListView lv in listsRef)
                {
                    TBL_FILEMANAGER manager = new TBL_FILEMANAGER();
                    manager.Type = lv.Type;
                    manager.Name = lv.Name;
                    if (!lv.Modified.Equals(""))
                    {
                        manager.Modified = Convert.ToDateTime(lv.Modified);
                    }
                    manager.ModifiedBy = lv.ModifiedBy;
                    manager.USER_ID = user.USER_ID;
                    manager.SessionID = Util.TheSessionId();// Context.Request.ServerVariables["REMOTE_ADDR"];
                    manager.FLAG = "1";
                    context.TBL_FILEMANAGER.AddObject(manager);
                }
                if (context.SaveChanges() > 0)
                {

                }
                else
                {

                }
            }
            //แสดงเฉพาะ Folder ของ user ที่ login เข้าระบบ
            EntityDataSource2.AutoGenerateWhereClause = false;
            EntityDataSource2.Where = "it.[USER_ID]='" + user.USER_ID + "' AND it.[SessionID]='" + Util.TheSessionId() + "' AND it.[FLAG]='1'";
            #endregion

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lName = (Label)e.Row.FindControl("Label1");
                Label lType = (Label)e.Row.FindControl("Label2");
                LinkButton lbName = (LinkButton)e.Row.FindControl("LbName");
                HyperLink hl = (HyperLink)e.Row.FindControl("HyperLink2");
                ImageButton iDelete = (ImageButton)e.Row.FindControl("ImageButton1");
                ImageButton lSelect = (ImageButton)e.Row.FindControl("ImageButton2");
                LinkButton lSelect1 = (LinkButton)e.Row.FindControl("LinkButton1");
                
                
                //เพิ่ม Event ตรวจสอบการลบ                
                iDelete.Attributes.Add("onclick", "return confirm('" + MyMessage.CONFIRM_DELETE + "');");
                TBL_USERS user = ((TBL_USERS)Session["TBL_USERS"]);

                #region "ตรวจสอบประเภทไฟล์"
                String type = lType.Text;
                String name = lName.Text;

                switch (type)
                {
                    case "Folder"://Folder
                        lbName.Visible = true;
                        lbName.Text = name;
                        hl.Visible = false;
                        hl.Text = name;
                        lSelect.ImageUrl = "~/VIEW/images/ico_folder.gif";
                        #region "กำหนดสิทธิ์ในการลบโฟลเดอร์"
                        switch (user.USER_GROUP_ID)
                        {
                            case "001":
                            case "002":
                                break;
                            case "003"://กรณีที่เป็น leader,user ให้ลบโฟลเดอร์หลักไม่ได้
                            case "004":
                                if (Label1.Text.Split('/').Length == 2)
                                {
                                    iDelete.Visible = false;
                                }
                                else
                                {
                                    iDelete.Visible = true;
                                }
                                break;
                        }
                        #endregion
                        break;
                    case "UP"://UP
                        lbName.Visible = true;
                        lbName.Text = name;
                        hl.Visible = false;
                        hl.Text = "ย้อนกลับ";
                        lSelect.ImageUrl = "~/VIEW/images/data_out.gif";
                        lSelect.Visible = true;
                        iDelete.Visible = false;
                        break;
                    default://File
                        lbName.Visible = false;
                        hl.Visible = true;
                        hl.Text = name;
                        hl.NavigateUrl = "~/VIEW/pages/DocManage/FrmDownload.aspx?doc_path=" + getPath(false) + "/" + hl.Text;
                        //hl.Target = "_blank";
                        // "แสดง icon ตามไฟล์ type"
                        lSelect.ImageUrl = getIcon(type);

                        #region "กำหนดสิทธิ์ในการลบเอกสาร"
                        String docs = "";
                        switch (user.USER_GROUP_ID)
                        {
                            case "001"://Admin                                
                                break;
                            case "002"://Super
                                docs = getUserDoc(user, true, true, false, false);
                                if (!docs.Contains(name))
                                {
                                    iDelete.Visible = false;
                                }
                                else
                                {
                                    iDelete.Visible = true;
                                }
                                break;
                            case "003"://Owner  

                                docs = getUserDoc(user, true, true, false, false);
                                if (!docs.Contains(name))
                                {
                                    iDelete.Visible = false;
                                }
                                else
                                {
                                    iDelete.Visible = true;
                                }

                                break;
                            case "004"://User
                                docs = getUserDoc(user, true, false, false, false);
                                if (!docs.Contains(name))
                                {
                                    if (Label1.Text.Split('/').Length == 2)
                                    {
                                        iDelete.Visible = false;
                                    }
                                    else
                                    {
                                        iDelete.Visible = false;
                                    }
                                }
                                else
                                {
                                    if (Label1.Text.Split('/').Length == 2)
                                    {
                                        iDelete.Visible = false;
                                    }
                                    else
                                    {
                                        iDelete.Visible = true;
                                    }
                                }
                                break;
                        }
                        #endregion

                        #region "กำหนดสิทธิ์ในการแก้ไขเอกสาร"
                        using (DCCDBEntities context = new DCCDBEntities())
                        {
                            List<String> cl = new List<string>();
                            List<TBL_DOCUMENT> checkList = context.TBL_DOCUMENT.Where("it.[DOC_FILE_NAME] LIKE '" + getPath(false) + "/" + name.Split('.')[0] + "%'").ToList();
                            if (checkList.Count > 0)
                            {
                                for (int i = 0; i < checkList.Count-1; i++)
                                {
                                    cl.Add(checkList[i].DOC_FILE_NAME);
                                }
                            }
                            //ตรวจสอบสิทธิ์ในการแก้ไขเอกสาร,อัพโหลดเอกสาร
                            switch (user.USER_GROUP_ID)
                            {
                                case "001"://Admin
                                    break;
                                case "002"://Super
                                    docs = getUserDoc(user, true, true, false, true);
                                    //~/Document/Administrator/AO_ SenderCode (user confirm) - REV.00.doc
                                    if (!docs.Contains(name))
                                    {
                                        lSelect.Enabled = false;
                                        lSelect1.Enabled = false;
                                    }
                                    else
                                    {
                                        lSelect.Enabled = true;
                                        lSelect1.Enabled = true;
                                    }
                                    break;
                                case "003"://Owner  
                                    docs = getUserDoc(user, true, true, false, false);
                                    if (!docs.Contains(name))
                                    {
                                        lSelect.Enabled = false;
                                        lSelect1.Enabled = false;
                                    }
                                    else
                                    {
                                        lSelect.Enabled = true;
                                        lSelect1.Enabled = true;
                                    }
                                    break;
                                case "004"://User
                                    //ดึงข้อมูลเอกสาร
                                    docs = getUserDoc(user, true, true, false, false);
                                    if (!docs.Contains(name))
                                    {
                                        lSelect.Enabled = false;
                                        lSelect1.Enabled = false;
                                    }
                                    else
                                    {
                                        lSelect.Enabled = true;
                                        lSelect1.Enabled = true;
                                    }
                                    break;
                            }
                            #region "ให้แก้ไขได้เฉพาะ REV ล่าสุด"
                            foreach (String s in cl)
                            {
                                if ((getPath(false) + "/"+name).Equals(s))
                                {
                                    lSelect.Enabled = false;
                                    lSelect1.Enabled = false;
                                }
                            }
                            #endregion
                        }
                        #endregion

                        break;
                }
                #endregion
            }

            #region "ซ่อน Column"
            #endregion
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            String msg = Path.GetExtension(getPath(false));
            TBL_USERS user = ((TBL_USERS)Session["TBL_USERS"]);

            String type = ((Label)GridView2.SelectedRow.FindControl("Label2")).Text;
            String name = ((Label)GridView2.SelectedRow.FindControl("Label1")).Text;
            switch (type)
            {
                case "Folder":
                    #region "Folder"
                    //ตรวจสอบว่ามีการเลือกไฟล์ไว้ก่อนหรือไม่ถ้าเลือกให้ remove ออกจาก List
                    if (msg.Length>0)
                    {
                        removePath();
                    }
                    addPath("/" + name);
                    gvFolderList(getPath(false));
                    CMD_UPLOAD.Text = "อัพโหลดเอกสาร";

                    #region "กำหนดสิทธิ์ในการใช้งานปุ่ม"
                    switch (user.USER_GROUP_ID)
                    {
                        case "001"://Admin
                        case "002"://Super
                            docEnableButton(true, true, false);
                            break;
                        case "003"://Owner  
                        case "004"://User
                            //ดึงข้อมูลเอกสาร
                            string curFolder = getPath(false).Split('/')[2];
                            string fpm = getUserDoc(user, true, true, false, false);
                            if (fpm.Length > 0)
                            {
                                String[] folderPermission = fpm.Split('/');
                                if (curFolder.Equals(folderPermission[2]))
                                {
                                    docEnableButton(true, true, false);
                                }
                                else
                                {
                                    docEnableButton(false, false, false);
                                }
                            }
                            else
                            {
                                docEnableButton(true, true, false);
                            }
                            break;
                    }
                    #endregion
                    #endregion
                    break;
                case "UP":
                    #region "UP"
                    //ไม่ให้ กด up เกินกว่า Root เอกสาร
                    if (Session["filePos"] != null)
                    {
                        List<String> lists = (List<String>)Session["filePos"];
                        if (lists.Count > 1)
                        {
                            if (msg.Length > 0)
                            {
                                //ถ้าไฟล์มีนามสกุลแสดงว่าเป็นการเลือกไฟล์อยู่จะต้องถอย 2 ครั้งเพื่อให้อยู่ที่ Root
                                removePath();
                                removePath();
                            }
                            else
                            {
                                removePath();
                            }
                            gvFolderList(getPath(false));
                        }
                    }
                    //ถ้าเป็น Root ให้ enable false การ upload และ new folder
                    if (getPath(false).Split('/').Length == 2)
                    {
                        docEnableButton(false, false, false);
                    }
                    else
                    {
                        docEnableButton(true, true, false);
                    }
                    CMD_UPLOAD.Text = "อัพโหลดเอกสาร";
                    #endregion
                    break;
                default:
                    #region "File"
                    //ไฟล์
                    if (msg.Length > 0)
                    {
                        //ถ้าไฟล์มีนามสกุลแสดงว่าเป็นการเลือกไฟล์อยู่จะต้องถอย 1 ครั้งเพื่อให้อยู่ที่ Root
                        removePath();
                    }
                    docEnableButton(true, true, true);
                    CMD_UPLOAD.Text = "แก้ไขไฟล์";
                    addPath("/" + name);
                    #endregion
                    break;
            }
        }
        #endregion

        #region "จัดการเรื่องตำแหน่งไฟล์"
        private String getPath(bool bSkipRoot)
        {   // เอาโฟลเดอร์มาต่อกันเป็น path
            String msg = "";
            if (Session["filePos"] != null)
            {
                int index = 0;
                List<String> filePos = (List<String>)Session["filePos"];
                foreach (String s in filePos)
                {
                    //ถ้าเป็น True จะไม่เอา Root Folder
                    if (!(bSkipRoot && index == 0))
                        msg += s;
                    //if (bSkipRoot)
                    //{
                    //    if (index == 0)
                    //    {

                    //    }
                    //    else
                    //    {
                    //        msg += s;
                    //    }
                    //}
                    //else
                    //{
                    //    msg += s;
                    //}

                    index++;
                }
            }
            return msg;
        }

        private void addPath(String _path)
        {
            //จัดการแสดง path on list file.
            List<String> filePos = null;
            if (Session["filePos"] != null)
            {
                filePos = (List<String>)Session["filePos"];
            }
            else
            {
                filePos = new List<string>();
            }
            filePos.Add(_path);
            Session["filePos"] = filePos;
            Label1.Text = getPath(false);//Add
            //ตัด doc Root ออกก่อนแสดงผล
            LPath.Text = Label1.Text.Replace(Util.getDocumentPath(),"").Trim();
        }

        private void removePath()
        {
            List<String> filePos = null;
            if (Session["filePos"] != null)
            {
                filePos = (List<String>)Session["filePos"];
                filePos.RemoveAt(filePos.Count - 1);
            }
            Label1.Text = getPath(false);//Add
            LPath.Text = Label1.Text.Replace(Util.getDocumentPath(), "").Trim();
        }
        
        private void removePathAll()
        {
            List<String> filePos = null;
            if (Session["filePos"] != null)
            {
                
                filePos = (List<String>)Session["filePos"];
                int size = filePos.Count;
                for (int i = 0; i < size; i++)
                {
                    filePos.RemoveAt(0);
                }
                Console.WriteLine();
            }
            Label1.Text = getPath(false);//Add
            LPath.Text = Label1.Text.Replace(Util.getDocumentPath(), "").Trim();
        }
        #endregion

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (Session["findDoc"] != null)
            {
                removePathAll();
                String yy = getPath(false);
                String findDoc = (String)Session["findDoc"];
                String[] tmp = findDoc.Replace(Path.GetFileName(findDoc), "").Trim().Split('/');
                addPath("~/" + tmp[1]);
                for (int i = 2; i < tmp.Length - 1; i++)
                {
                    addPath("/" + tmp[i]);
                }
                String xx = getPath(false);
                gvFolderList(getPath(false));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        private TBL_DOCUMENT getDocID(String _fn)
        {
            TBL_DOCUMENT doc = null;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                List<TBL_DOCUMENT> docs = context.TBL_DOCUMENT.Where("it.[DOC_FILE_NAME]='" + _fn + "'").ToList();
                if (docs.Count > 0)
                {
                    doc = docs[0];
                }
            }
            return doc;
        }

        private void docEnableButton(bool bUpload, bool bNewFolder, bool bRef)
        {
            CMD_UPLOAD.Enabled = bUpload;
            CMD_NEW_FOLDER.Enabled = bNewFolder;
            CMD_REF.Enabled = bRef;
        }

        protected void GridView2_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            String fn = getPath(false) + "/" + LName.Text;
            switch (LType.Text)
            {
                case "Folder":
                    #region "ลบโฟลเดอร์"
                    if (Util.deleteDir(Server.MapPath(fn)))
                    {
                        using (DCCDBEntities context = new DCCDBEntities())
                        {
                            TBL_M_DEPARTMENT dep = context.TBL_M_DEPARTMENT.Where("it.[DEPARTMENT_FOLDER]='" + (getPath(true) + "/" + LName.Text) + "'").First();
                            if (dep != null)
                            {
                                TBL_M_DEPARTMENT depD = context.TBL_M_DEPARTMENT.Where("it.[DEPARTMENT_FOLDER_ID]=" + dep.DEPARTMENT_FOLDER_ID + "").First();
                                context.DeleteObject(depD);
                                if (context.SaveChanges() > 0)
                                {
                                    //update logs
                                    // ###### Add Document Event ######
                                    if (EventLog.Logs(((TBL_USERS)Session["TBL_USERS"]).USER_ID, dep.DEPARTMENT_FOLDER_ID + "", fn, "DF"))
                                    {
                                        //Response.Write("write log success.");
                                    }
                                    ShowMsg(MyMessage.DELETE_SUCCESS);
                                    //Response.Write(Util.ShowMessage(MyMessage.DELETE_SUCCESS, formName));
                                }
                                else
                                {
                                    ShowMsg(MyMessage.CANNOT_DELETE);
                                    //Response.Write(Util.ShowMessage(MyMessage.CANNOT_DELETE,formName));
                                }
                            }
                        }
                    }
                    else
                    {
                        ShowMsg(MyMessage.CANNOT_DELETE);
                        //Response.Write(Util.ShowMessage(MyMessage.CANNOT_DELETE, formName));
                    }
                    #endregion
                    break;
                case "UP":
                    break;
                default:
                    #region "ลบไฟล์"
                    String msg = Path.GetExtension(getPath(false));
                    //ตรวจสอบว่ามีการเลือกไฟล์ไว้ก่อนหรือไม่ถ้าเลือกให้ remove ออกจาก List
                    if (msg.Length > 0)
                    {
                        removePath();
                    }
                    fn = getPath(false) + "/" + LName.Text;
TBL_DOCUMENT doc = getDocID(fn);
if (Util.isFileExist(Server.MapPath(fn)))
{
    Util.delete(Server.MapPath(fn));
    using (DCCDBEntities context = new DCCDBEntities())
    {
        //ลบข้อมูลใน DocRef ออกก่อน                               
        List<TBL_DOCUMENT_REF> docRefDelete = context.TBL_DOCUMENT_REF.Where("it.[REF_DOC_ID] = '" + doc.DOC_ID + "' AND it.[DOC_PATH]='" + fn + "'").ToList();
        if (docRefDelete.Count > 0)
        {
            foreach (TBL_DOCUMENT_REF tmp in docRefDelete)
            {
                context.DeleteObject(tmp);
            }
        }
        //ลบข้อมูลใน doc
        TBL_DOCUMENT docDelete = context.TBL_DOCUMENT.Where("it.[DOC_ID]='" + doc.DOC_ID + "'  AND it.[DOC_FILE_NAME]='" + fn + "'").First();
        if (docDelete != null)
        {
            context.DeleteObject(docDelete);
        }
        if (context.SaveChanges() > 0)
        {
            // ###### Add Document Event ######
            if (EventLog.Logs(((TBL_USERS)Session["TBL_USERS"]).USER_ID, doc.DOC_ID, fn, "D"))
            {
                //Response.Write("write log success.");
            }
            ShowMsg(MyMessage.DELETE_SUCCESS);
            //Response.Write(Util.ShowMessage(MyMessage.DELETE_SUCCESS, formName));
        }
                            else
                            {
                                ShowMsg(MyMessage.DELETE_FAILURE);
                                //Response.Write(Util.ShowMessage(MyMessage.DELETE_FAILURE, formName));
                            }
                        }
                    }
                    #endregion
                    break;
            }
            //แสดงตำแหน่ง folder ปัจจุบัน
            gvFolderList(getPath(false));
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            //ปุ่มย้อนกลับ
            TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
            String msg = Path.GetExtension(getPath(false));
            #region "ตรวจสอบไม่ให้กดปุ่ม Back เกิน Root ของเอกสาร"
            if (Session["filePos"] != null)
            {
                List<String> lists = (List<String>)Session["filePos"];
                //Path ต้องมากกว่า 1 เพราะ ถ้าเป็น 1 จะหมายถึง Root Folder
                if (lists.Count > 1)
                {
                    //ถ้าปัจจุบันมีการเลือกเป็นไฟล์เอกสารไว้
                    if (msg.Length > 0)
                    {
                        //ถ้าไฟล์มีนามสกุลแสดงว่าเป็นการเลือกไฟล์อยู่จะต้องถอย 2 ครั้งเพื่อให้อยู่ที่ Root
                        removePath();
                        //การแสดงปุ่มควบคุม
                        docEnableButton(true, true, false); 
                    }
                    else
                    {
                        //ถ้าปัจจุบันมีการเลือกเป็น Foder ไว้
                        #region "ตรวจสอบการกดปุ่ม ย้อนกลับ"
                        
                        if (Session["TBL_USERS"] != null)
                        {
                            switch (user.USER_GROUP_ID)
                            { 
                                case "001":
                                case "002":
                                    //Admin และ Superuser จะกดปุ่มย้อนกลับได้ก็ต่อเมื่ออยู่ใน Sub Foder ที่มากกว่า 1 ชั้น
                                    removePath();
                                    //การแสดงปุ่มควบคุม
                                    switch (lists.Count)
                                    {
                                        case 1:
                                            //ถ้าเป็น Root path
                                            docEnableButton(true, true, false);
                                            break;
                                        default:
                                            //ถ้าเป็น Sup Folder
                                            docEnableButton(true, true, true);
                                            break;
                                    }                              
                                    break;
                                case "003":
                                case "004":
                                    //user และ owner จะกดปุ่มย้อนกลับได้ก็ต่อเมื่ออยู่ใน Sub Foder ที่มากกว่า 2 ชั้น
                                    if (lists.Count > 2)
                                    {
                                        removePath();
                                    }
                                    //การแสดงปุ่มควบคุม
                                    docEnableButton(true, true, false); 
                                    break;
                            }
                        }                        
                        #endregion
                    }
                    //update Gridview
                    gvFolderList(getPath(false));
                }
            }
            #endregion
            //เมื่อกดปุ่ม ย้อนกลับ ให้ล้าง Index ที่เคยเลือกไว้ใน Gridview
            GridView2.SelectedIndex = -1;
            CMD_UPLOAD.Text = "อัพโหลดเอกสาร";
        }

        protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lName = (Label)e.Row.FindControl("Label7");
                Label lType = (Label)e.Row.FindControl("Label6");

                Image iType = (Image)e.Row.FindControl("Image4");
                HyperLink hl = (HyperLink)e.Row.FindControl("HyperLink4");
                #region "ตรวจสอบประเภทไฟล์"
                String type = lType.Text;
                String name = lName.Text;

                switch (type)
                {
                    case "Folder"://Folder
                        hl.Text = name;
                        iType.ImageUrl = "~/VIEW/images/ico_folder.gif";
                        break;
                    case "UP"://UP
                        hl.Text = "ย้อนกลับ";
                        iType.ImageUrl = "~/VIEW/images/data_out.png";
                        iType.Visible = true;
                        break;
                    default://File
                        hl.Text = Path.GetFileName(name);
                        hl.NavigateUrl = "~/VIEW/pages/DocManage/FrmDownload.aspx?doc_path=" + name;
                        //hl.Target = "_blank";
                        // "แสดง icon ตามไฟล์ type"
                        iType.ImageUrl = getIcon(type);
                        #endregion
                        break;
                }
            }
        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            LType.Text = ((Label)GridView2.Rows[e.RowIndex].FindControl("Label2")).Text;
            LName.Text = ((HyperLink)GridView2.Rows[e.RowIndex].FindControl("HyperLink2")).Text;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            if (Session["findDoc"] != null)
            {
                removePathAll();
                String yy = getPath(false);
                String findDoc = (String)Session["findDoc"];
                if (findDoc != null)
                {
                    String[] tmp = findDoc.Replace(Path.GetFileName(findDoc), "").Trim().Split('/');
                    addPath("~/" + tmp[1]);
                    for (int i = 2; i < tmp.Length - 1; i++)
                    {
                        addPath("/" + tmp[i]);
                    }
                    gvFolderList(getPath(false));
                }
            }
            else
            {
                String path = getPath(false);
                if (Path.HasExtension(path))
                {
                    removePath();
                }
                gvFolderList(getPath(false));
                

                #region "กำหนดสิทธิ์ในการใช้งานปุ่ม"
                CMD_UPLOAD.Text = "อัพโหลดเอกสาร";
                TBL_USERS user = ((TBL_USERS)Session["TBL_USERS"]);
                switch (user.USER_GROUP_ID)
                {
                    case "001"://Admin
                    case "002"://Super
                        docEnableButton(true, true, false);
                        break;
                    case "003"://Owner  
                    case "004"://User
                        //ดึงข้อมูลเอกสาร
                        string curFolder = getPath(false).Split('/')[2];
                        string fpm = getUserDoc(user, true, true, false, false);
                        if (fpm.Length > 0)
                        {
                            String[] folderPermission = fpm.Split('/');
                            if (curFolder.Equals(folderPermission[2]))
                            {
                                docEnableButton(true, true, false);
                            }
                            else
                            {
                                docEnableButton(false, false, false);
                            }
                        }
                        else
                        {
                            docEnableButton(true, true, false);
                        }
                        break;
                }
                #endregion
                //Response.Redirect("Default.aspx");
            }
        }

        private void ShowMsg(String msg)
        {
            System.Text.StringBuilder strBuilder = new System.Text.StringBuilder();
            strBuilder.Append("<script>");
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append("alert('" + msg + "');");
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append("</" + "script>");

            if (!Page.ClientScript.IsClientScriptBlockRegistered(""))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "script", strBuilder.ToString());
            }
        }

        private String getIcon(String fType)
        {
            String fileIcon = "";
            switch (fType)
            {
                case ".docx":
                case ".doc":
                    fileIcon = "~/VIEW/images/icon_doc.gif";
                    break;
                case ".xlsx":
                case ".xls":
                    fileIcon = "~/VIEW/images/icon_xls.gif";
                    break;
                case ".pdf":
                    fileIcon = "~/VIEW/images/icon_pdf.gif";
                    break;
                default:
                    fileIcon = "~/VIEW/images/icon_generic.gif";
                    break;
            }
            return fileIcon;
        }
    }
}