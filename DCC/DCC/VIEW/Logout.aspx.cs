﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;
using DCC.DAL;

namespace DCC
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ACTION"] != null)
            {
                Session["ACTION"] = null;
            }
            if (Session["TBL_USERS"] != null)
            {
                TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
                DALUser.userLogOut(user.USER_ID);
                Session["TBL_USERS"] = null;
                Session.RemoveAll();
                Session.Abandon();
            }

            Response.Redirect("Login.aspx");

        }
    }
}