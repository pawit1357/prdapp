﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;

namespace DCC.VIEW
{
    public partial class Navigator : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void setDetail(String detail)
        {
            L_USER_DETAIL.Text = detail;
        }
        public String getUserDetail()
        {
            return L_USER_DETAIL.Text;
        }

        public Boolean viewPagePermission(TBL_USERS _user,String pageGroup)
        {
            Boolean bHave = false;
            if (_user.USER_GROUP_ID.Equals(pageGroup))
            {
                bHave = true;
            }
            return bHave;
        }
    }
}