﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;

namespace DCC
{
    public partial class MyMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void setMenu(TBL_USERS user)
        {            
            switch (user.USER_GROUP_ID)
            {
                case "001"://1.	Administrators = QMR
                    Menu1.Visible = true;
                    Menu2.Visible = false;
                    Menu3.Visible = false;
                    Menu4.Visible = false;
                    break;
                case "002"://2.	Super user = Director
                    Menu1.Visible = false;
                    Menu2.Visible = true;
                    Menu3.Visible = false;
                    Menu4.Visible = false;
                    break;
                case "003"://3.	Owner group = Manager
                    Menu1.Visible = false;
                    Menu2.Visible = false;
                    Menu3.Visible = true;
                    Menu4.Visible = false;
                    break;
                case "004"://4.	User = Employee
                    Menu1.Visible = false;
                    Menu2.Visible = false;
                    Menu3.Visible = false;
                    Menu4.Visible = true;
                    break;
            }
        }

    }
}