﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DCC.VIEW.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" > 

<head runat="server">
     <title>Document Control Center</title>
   <%--<link href="StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">

    .font_default
    {
	    font-family: Tahoma;
        font-size:12px;
        font-weight:normal;
        color:black;
        text-decoration:none;
    }
    .style2
    {
        font-family: Tahoma;
    }
    .forgtpwd
    {
	    font-family: Tahoma;
        font-size: 12px;
        font-weight: normal;
        color: #af0000;
        text-decoration:none;
    }
    </style>
    <script type="text/javascript" language="javascript">
        window.history.forward(-1);

//        window.onbeforeunload = confirmExit;
//        var aClick = false;
//        function confirmExit(e) {
//            if (document.all) e = event;
//            if (!e) e = window.event;
//            if (e) {
//                if (aClick == false && (e.target == document || e.clientX < 0 || e.clientY < 0)) {
//                    //return "You have attempted to leave this page. Are you sure you want to exit this page?";
//                    return "โปรดออกจากระบบด้วยการกดปุ่ม ออกจากระบบ";
//                }
//            }
//        }
    </script>
    </head>
<body>

    <form id="form1" runat="server">
    <center>
    <div class="accordionHeader" style="width: 484px"><img alt="" src="images/login_header.png" width="484" /></div>


        <%--<img alt="" src="images/spacer.gif" width="10" />--%>
        <table id="Table4" border="0" cellpadding="0" cellspacing="0" 
            
            style="background-image: url('images/Login.png'); width: 482px; height: 321px;">
            <tr>
                <td colspan="4">
                    <%--<img alt="" height="10" src="images/spacer.gif" width="10" />--%></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td align="left" colspan="2" 
                    style="color: White; background-repeat: repeat-x; background-position: left bottom;" 
                    width="193">
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 15px; background-repeat: repeat-y; background-position: left top;">
                    <%--<img alt="" height="15" src="images/spacer.gif" width="15" />--%></td>
                <td align="center" colspan="2">
                    <asp:Panel ID="LoginPanel" runat="server" Visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="193px" align="right">
                            <tr>
                                <td align="right" style="width:56px; font-size:12px; padding: 2px;">
                                    ชื่อผู้ใช้&nbsp;:&nbsp;</td>
                                <td align="left" style="width:120px; padding: 2px;">
                                    <asp:TextBox ID="txtUsername" runat="server" Width="120px" BorderStyle="Solid" 
                                        BorderWidth="1px"></asp:TextBox><br />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="width:56px; font-size:12px; padding: 2px;" class="style2">
                                    รหัสผ่าน&nbsp;:&nbsp;</td>
                                <td align="left" style="width:120px; padding: 2px;">
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="120px" 
                                        BorderStyle="Solid" BorderWidth="1px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="padding: 5px;">
                                    <asp:Button ID="btnLogin" runat="server" onclick="Button1_Click" 
                                        Text="เข้าสู่ระบบ" BorderStyle="Solid" BorderWidth="1px" Height="23px" 
                                        Width="70px" />
                                <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:HyperLink ID="HyperLink2" runat="server" CssClass="forgtpwd" 
                                     NavigateUrl="~/VIEW/pages/User/ForgotPassword.aspx">ลืมรหัสผ่าน</asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <%--<asp:Panel ID="welcomePanel" runat="server" Visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="193px" align="right">
                            <tr>
                                <td align="center">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px;">
                                    ยินดีต้อนรับ&nbsp;คุณ&nbsp;<asp:Literal ID="UserName" runat="server" Text=" "></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnLogout" runat="server" Text="ออกจากระบบ" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>--%>
                </td>
                <td style="background-repeat: repeat-y; background-position: right top;">
                    <img alt="" height="15" src="images/spacer.gif" width="15" /></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td colspan="2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                    <img alt="" height="10" src="images/spacer.gif" width="10" /></td>
            </tr>
        </table>
    </center>
    <p>
        &nbsp;</p>
    </form>
</body>
</html>
