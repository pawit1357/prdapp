﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;
using DCC.DAL;
using DCC.Utility;
using System.Text;

namespace DCC.VIEW.pages.Master
{
    public partial class MTitle : System.Web.UI.Page
    {
        private ACTION dbAction = ACTION.INSERT;
        private String formName = "MTitle.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {
                    //set user detail
                    TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
                    Navigator1.setDetail(DALUser.getUserDetail(user));
                    MyMenu1.setMenu(user);
                    //check page permission
                    if (!Navigator1.viewPagePermission(user, "001"))
                    {
                        Response.Redirect("~/VIEW/Default.aspx");
                    }
                    //Set initial action
                    Session["ACTION"] = ACTION.INSERT;
                    using (DCCDBEntities context = new DCCDBEntities())
                    {
                        //Auto ID
                        List<TBL_TITLE> dep = context.TBL_TITLE.ToList();
                        if (dep.Count > 0)
                        {
                            TXT_ID.Text = (Convert.ToInt16(dep[dep.Count - 1].TITLE_ID) + 1).ToString("000");
                        }
                        else
                        {
                            TXT_ID.Text = "001";
                        }                    
                    }
                }
                else
                {
                    Response.Redirect("~/View/Logout.aspx");
                }
                //CMD_INSERT.Enabled = false;
            }            
            if (GridView1.Rows.Count == 0)
            {
                LWARN.Text = MyMessage.SEARCH_NOT_FOUND;
            }
        }

        protected void CMD_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            switch (button.ID)
            {
                case "CMD_INSERT":
                    dbAction = (ACTION)Session["ACTION"];

                    using (DCCDBEntities context = new DCCDBEntities())
                    {
                        switch (dbAction)
                        {
                            //case ACTION.ADD:
                            //    #region "เพิ่ม"
                            //    TXT_ID.Focus();
                            //    TXT_ID.Enabled = true;
                            //    CMD_INSERT.Enabled = true;
                            //    CMD_INSERT.Text = "บันทึก";
                            //    Session["ACTION"] = ACTION.INSERT;
                            //    //Auto ID
                            //    List<TBL_TITLE> dep = context.TBL_TITLE.ToList();
                            //    if (dep.Count > 0)
                            //    {
                            //        TXT_ID.Text = (Convert.ToInt16(dep[dep.Count - 1].TITLE_ID) + 1).ToString("000");
                            //    }
                            //    else
                            //    {
                            //        TXT_ID.Text = "001";
                            //    }
                            //    #endregion
                            //    break;
                            case ACTION.INSERT:
                                if (validate())
                                {
                                    if (!isDataExist())
                                    {
                                        #region "บันทึก"
                                        TBL_TITLE title = new TBL_TITLE()
                                        {
                                            TITLE_ID = TXT_ID.Text,
                                            TITLE_NAME = TXT_NAME.Text
                                        };
                                        context.TBL_TITLE.AddObject(title);
                                        if (context.SaveChanges() > 0)
                                        {
                                            Response.Write(Util.ShowMessage(MyMessage.INSERT_SUCCESS, formName));
                                        }
                                        else
                                        {
                                            Response.Write(Util.ShowMessage(MyMessage.INSERT_FAILURE, formName));
                                        }
                                        #endregion
                                        initial();
                                    }
                                }
                                break;

                            case ACTION.UPDATE:
                                if (validate())
                                {
                                    if (!isDataExist())
                                    {
                                        #region "แก้ไข"
                                        TBL_TITLE title1 = context.TBL_TITLE.Where("it.[TITLE_ID]='" + TXT_ID.Text + "'").First();
                                        title1.TITLE_NAME = TXT_NAME.Text;
                                        if (context.SaveChanges() > 0)
                                        {
                                            Response.Write(Util.ShowMessage(MyMessage.UPDATE_SUCCESS, formName));
                                        }
                                        else
                                        {
                                            Response.Write(Util.ShowMessage(MyMessage.UPDATE_FAILURE, formName));
                                        }
                                        #endregion
                                        initial();
                                    }
                                }
                                break;
                        }
                    }                                                                       
                    break;
                case "CMD_SEARCH":
                    StringBuilder condition = new StringBuilder();
                    if (!TXT_ID.Text.Equals(""))
                    {
                        condition.Append(" it.[TITLE_ID] == '" + TXT_ID.Text + "'");
                        condition.Append(" AND");
                    }
                    if (!TXT_NAME.Text.Equals(""))
                    {
                        condition.Append(" it.[TITLE_NAME] LIKE '" + TXT_NAME.Text + "'");
                        condition.Append(" AND");
                    }
                    if (condition.ToString().Length > 0)
                    {
                        EntityDataSource1.AutoGenerateWhereClause = false;
                        EntityDataSource1.Where = condition.ToString().Substring(0, condition.ToString().Length - 3);
                    }
                    else
                    {
                        Response.Redirect(formName);
                    }
                    break;
                case "CMD_CANCEL":
                    initial();
                    Response.Redirect(formName);
                    break;
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            TXT_ID.Enabled = true;
            CMD_INSERT.Enabled = true;

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["ACTION"] = ACTION.UPDATE;
            //Set label name
            CMD_INSERT.Text = MyMessage.BUTTON_UPDATE;
            CMD_INSERT.Enabled = true;
            //Disable ID field
            TXT_ID.Enabled = false;
            //Set update Detail
            TXT_ID.Text = GridView1.SelectedRow.Cells[0].Text;
            TXT_NAME.Text = GridView1.SelectedRow.Cells[1].Text;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton button = (ImageButton)e.Row.FindControl("ImageButton1");
                button.Attributes.Add("onclick", "return confirm('" + MyMessage.CONFIRM_DELETE + "');");
            }
        }

        private void initial()
        {
            CMD_INSERT.Text = MyMessage.BUTTON_INSERT;
            TXT_ID.Enabled = true;
            TXT_ID.Text = "";
            TXT_NAME.Text = "";
            LWARN.Text = "";
            TXT_ID.Focus();
        }
        private bool validate()
        {
            bool bSucces = true;
            StringBuilder msg = new StringBuilder();
            if (TXT_ID.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลรหัส");
                msg.Append("\\n");
            }
            if (TXT_NAME.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลชื่อ");
                msg.Append("\\n");
            }
            if (msg.ToString().Length > 0)
            {
                bSucces = false;
                CMD_INSERT.Enabled = true;
                Response.Write(Util.ShowMessage(msg.ToString(),"MTitle.aspx"));
            }            
            return bSucces;
        }
        private bool isDataExist()
        {
            bool bExist = false;
            using (DCCDBEntities context = new DCCDBEntities())
            {

                List<TBL_TITLE> lists = context.TBL_TITLE.ToList();
                foreach (TBL_TITLE tmp in lists)
                {
                    if (tmp.TITLE_NAME.Trim().Equals(TXT_NAME.Text))
                    {
                        bExist = true;
                        CMD_INSERT.Enabled = true;
                        Response.Write(Util.ShowMessage(MyMessage.DATA_EXIST,"MTitle.aspx"));
                        break;
                    }
                }
            }
            return bExist;
        }

        protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            initial();
            Response.Redirect(formName);
        }
    }
}