﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;
using DCC.DAL;
using DCC.Utility;
using System.Text;

namespace DCC.VIEW.pages.Master
{
    public partial class MGroup : System.Web.UI.Page
    {
        private ACTION dbAction = ACTION.INSERT;
        private String formName = "MGroup.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {
                    //set user detail
                    TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
                    Navigator1.setDetail(DALUser.getUserDetail(user));
                    MyMenu1.setMenu(user);
                    //check page permission
                    if (!Navigator1.viewPagePermission(user, "001"))
                    {
                        Response.Redirect("~/VIEW/Default.aspx");
                    }
                    //Set initial action
                    Session["ACTION"] = ACTION.INSERT;
                    //Auto ID
                    using (DCCDBEntities context = new DCCDBEntities())
                    {
                        List<TBL_M_GROUP> dep = context.TBL_M_GROUP.ToList();
                        if (dep.Count > 0)
                        {
                            TXT_ID.Text = (Convert.ToInt16(dep[dep.Count - 1].GROUP_ID) + 1).ToString("000");
                        }
                        else
                        {
                            TXT_ID.Text = "001";
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/View/Logout.aspx");
                }
                CMD_INSERT.Enabled = true;
                CMD_INSERT.Text = "เพิ่ม";
            }
           
            if (GridView1.Rows.Count == 0)
            {
                LWARN.Text = MyMessage.SEARCH_NOT_FOUND;
            }
        }

        protected void CMD_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            switch (button.ID)
            {
                case "CMD_INSERT":
                    dbAction = (ACTION)Session["ACTION"];

                            using (DCCDBEntities context = new DCCDBEntities())
                            {
                                switch (dbAction)
                                {
                                    //case ACTION.ADD:
                                    //    #region "เพิ่ม"
                                    //    TXT_ID.Enabled = true;
                                    //    CMD_INSERT.Enabled = true;

                                    //    //Auto ID
                                    //    List<TBL_M_GROUP> dep = context.TBL_M_GROUP.ToList();
                                    //    if (dep.Count > 0)
                                    //    {
                                    //        TXT_ID.Text = (Convert.ToInt16(dep[dep.Count - 1].GROUP_ID) + 1).ToString("000");
                                    //    }
                                    //    else {
                                    //        TXT_ID.Text = "001";
                                    //    }
                                    //    CMD_INSERT.Text = "บันทึก";
                                    //    Session["ACTION"] = ACTION.INSERT;
                                    //    #endregion
                                        //break;
                                    case ACTION.INSERT:
                                        #region "บันทึก"
                                        if (!isDataExist() && validate())
                                        {
                                            TBL_M_GROUP grp = new TBL_M_GROUP();
                                            grp.GROUP_ID = TXT_ID.Text;
                                            grp.GROUP_NAME = TXT_NAME.Text;
                                            //grp.P_INSERT = CheckBox1.Checked;
                                            //grp.P_UPDATE = CheckBox2.Checked;
                                            //grp.P_DELETE = CheckBox3.Checked;
                                            //grp.P_VIEW_DOC = CheckBox4.Checked;
                                            //grp.P_VIEW_DOC_IN = CheckBox5.Checked;
                                            context.TBL_M_GROUP.AddObject(grp);
                                            if (context.SaveChanges() > 0)
                                            {
                                                Response.Write(Util.ShowMessage(MyMessage.INSERT_SUCCESS, formName));
                                            }
                                            else
                                            {
                                                Response.Write(Util.ShowMessage(MyMessage.INSERT_FAILURE, formName));
                                            }
                                            initial();
                                        }
                                        #endregion
                                        break;
                                    case ACTION.UPDATE:
                                        #region "แก้ไข"
                                        if (!isDataExist() && validate())
                                        {
                                            TBL_M_GROUP grp1 = context.TBL_M_GROUP.First(d => d.GROUP_ID == TXT_ID.Text);
                                            grp1.GROUP_ID = TXT_ID.Text;
                                            grp1.GROUP_NAME = TXT_NAME.Text;
                                            //grp1.P_INSERT = CheckBox1.Checked;
                                            //grp1.P_UPDATE = CheckBox2.Checked;
                                            //grp1.P_DELETE = CheckBox3.Checked;
                                            //grp1.P_VIEW_DOC = CheckBox4.Checked;
                                            //grp1.P_VIEW_DOC_IN = CheckBox5.Checked;
                                            if (context.SaveChanges() > 0)
                                            {
                                                Response.Write(Util.ShowMessage(MyMessage.UPDATE_SUCCESS, formName));
                                            }
                                            else
                                            {
                                                Response.Write(Util.ShowMessage(MyMessage.UPDATE_FAILURE, formName));
                                            }
                                            initial();
                                        }
                                        #endregion
                                        break;                                
                            }                                                   
                    }
                    break;
                case "CMD_SEARCH":
                    #region "ค้นหา"
                    StringBuilder condition = new StringBuilder();
                    if (!TXT_ID.Text.Equals(""))
                    {
                        condition.Append(" it.[GROUP_ID] == '" + TXT_ID.Text + "'");
                        condition.Append(" AND");
                    }
                    if (!TXT_NAME.Text.Equals(""))
                    {
                        condition.Append(" it.[GROUP_NAME] LIKE '" + TXT_NAME.Text + "'");
                        condition.Append(" AND");
                    }
                    if (condition.ToString().Length > 0)
                    {
                        EntityDataSource1.AutoGenerateWhereClause = false;
                        EntityDataSource1.Where = condition.ToString().Substring(0, condition.ToString().Length - 3);
                    }
                    else
                    {
                        Response.Redirect(formName);
                    }
                    #endregion
                    break;
                case "CMD_CANCEL":
                    initial();
                    Response.Redirect(formName);
                    break;
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            TXT_ID.Enabled = true;
            CMD_INSERT.Enabled = true;
            //Auto ID
            using (DCCDBEntities context = new DCCDBEntities())
            {
                List<TBL_M_GROUP> dep = context.TBL_M_GROUP.ToList();
                if (dep.Count > 0)
                {
                    TXT_ID.Text = (Convert.ToInt16(dep[dep.Count - 1].GROUP_ID) + 1).ToString("000");
                }
                else {
                    TXT_ID.Text = "001";
                }
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["ACTION"] = ACTION.UPDATE;
            //Set label name
            CMD_INSERT.Text = MyMessage.BUTTON_UPDATE;
            CMD_INSERT.Enabled = true;

            //Set update Detail
            using (DCCDBEntities context = new DCCDBEntities())
            {
                TBL_M_GROUP tmp = context.TBL_M_GROUP.Where("it.[GROUP_ID] = '" + GridView1.SelectedRow.Cells[0].Text + "'").First();
                TXT_ID.Text = tmp.GROUP_ID;
                TXT_NAME.Text = tmp.GROUP_NAME;
                //CheckBox1.Checked = Convert.ToBoolean(tmp.P_INSERT);
                //CheckBox2.Checked = Convert.ToBoolean(tmp.P_DELETE);
                //CheckBox3.Checked = Convert.ToBoolean(tmp.P_UPDATE);
                //CheckBox4.Checked = Convert.ToBoolean(tmp.P_VIEW_DOC);
                //CheckBox5.Checked = Convert.ToBoolean(tmp.P_VIEW_DOC_IN);
            }

        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                ImageButton button = (ImageButton)e.Row.FindControl("ImageButton1");
                button.Attributes.Add("onclick", "return confirm('" + MyMessage.CONFIRM_DELETE + "');");
            }
        }
        private void initial()
        {
            CMD_INSERT.Text = MyMessage.BUTTON_INSERT;
            TXT_ID.Enabled = true;
            TXT_ID.Text = "";
            TXT_NAME.Text = "";
            CheckBox1.Checked = false;
            CheckBox2.Checked = false;
            CheckBox3.Checked = false;
            CheckBox4.Checked = false;
            CheckBox5.Checked = false;
            TXT_ID.Focus();
        }
        private bool validate()
        {
            bool bSucces = true;
            StringBuilder msg = new StringBuilder();
            if (TXT_ID.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลรหัส");
                msg.Append("\\n");
            }
            if (TXT_NAME.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลชื่อ");
                msg.Append("\\n");
            }
            if (msg.ToString().Length > 0)
            {
                bSucces = false;
                CMD_INSERT.Enabled = true;
                Response.Write(Util.ShowMessage(msg.ToString(),"MGroup.aspx"));
            }
            return bSucces;
        }
        private bool isDataExist()
        {
            bool bExist = false;
            using (DCCDBEntities context = new DCCDBEntities())
            {

                List<TBL_M_GROUP> lists = context.TBL_M_GROUP.ToList();
                foreach (TBL_M_GROUP tmp in lists)
                {
                    if (tmp.GROUP_NAME.Trim().Equals(TXT_NAME.Text))
                    {
                        bExist = true;
                        CMD_INSERT.Enabled = true;
                        Response.Write(Util.ShowMessage(MyMessage.DATA_EXIST,"MGroup.aspx"));
                        break;
                    }
                }
            }
            return bExist;
        }
        protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            initial();
            Response.Redirect(formName);
        }
    }
}