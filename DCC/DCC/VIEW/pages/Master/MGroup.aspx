﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VIEW/theme/theme1.Master" AutoEventWireup="true" CodeBehind="MGroup.aspx.cs" Inherits="DCC.VIEW.pages.Master.MGroup" %>
<%@ Register src="../../MyMenu.ascx" tagname="MyMenu" tagprefix="uc2" %>
<%@ Register src="../../Navigator.ascx" tagname="Navigator" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
        <table class="style4">
            <tr>
                <td>   
                    &nbsp;</td>
            </tr>
            <tr>
             
                <td>   
                <div class="box_header">ตั้งค่ากลุ่ม</div>
                <div class="box_data">                                  
                    <table class="style4">
                        <tr>
                            <td>
                                        &nbsp;</td>
                            <td>
                                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                        <asp:Label ID="Label18" runat="server" Text="รหัส"></asp:Label>
                                    </td>
                            <td>
                                        <asp:TextBox ID="TXT_ID" runat="server" CssClass="font_textbox" 
                                            CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                        ControlToValidate="TXT_ID" ErrorMessage="ยังไม่ได้ป้อนรหัส" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                    </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                        <asp:Label ID="Label19" runat="server" Text="ชื่อ"></asp:Label>
                                    </td>
                            <td>
                                        <asp:TextBox ID="TXT_NAME" runat="server" CssClass="font_textbox" 
                                            CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                        ControlToValidate="TXT_NAME" ErrorMessage="ยังไม่ได้ป้อนชื่อ" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                    </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                        <asp:CheckBox ID="CheckBox1" runat="server" CssClass="font_default4" 
                                            Text="Insert" Visible="False" />
                                        <asp:CheckBox ID="CheckBox2" runat="server" CssClass="font_default4" 
                                            Text="Update" Visible="False" />
                                        <asp:CheckBox ID="CheckBox3" runat="server" CssClass="font_default4" 
                                            Text="Delete" Visible="False" />
                                        <asp:CheckBox ID="CheckBox4" runat="server" CssClass="font_default4" 
                                            Text="View Document All" Visible="False" />
                                        <asp:CheckBox ID="CheckBox5" runat="server" CssClass="font_default4" 
                                            Text="View Document Internal" Visible="False" />
                                    </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td style="text-align: right">
                                    <asp:Button ID="CMD_SEARCH" runat="server" ForeColor="#006600" 
                                        onclick="CMD_Click" Text="ค้นหา" Visible="False" />
                                    </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>              
                </div>  
                </td>
            </tr>
            <tr>
                <td class="style3">
                                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" style="text-align: right">
                                    <asp:Button ID="CMD_INSERT" runat="server" ForeColor="#006600" 
                                        onclick="CMD_Click" Text="เพิ่ม" />
                                    <asp:Button ID="CMD_CANCEL" runat="server" CausesValidation="False" 
                                        ForeColor="#006600" onclick="CMD_Click" Text="ยกเลิก" />
                                    </td>
            </tr>
            <tr>
                <td align="center">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                                CellPadding="4" DataKeyNames="GROUP_ID" DataSourceID="EntityDataSource1" 
                                ForeColor="Black" GridLines="Vertical" AllowPaging="True" 
                                onselectedindexchanged="GridView1_SelectedIndexChanged" 
                                            onrowdatabound="GridView1_RowDataBound" 
                                            onrowdeleted="GridView1_RowDeleted">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="GROUP_ID" HeaderText="รหัส" ReadOnly="True" 
                                        SortExpression="GROUP_ID" />
                                    <asp:BoundField DataField="GROUP_NAME" HeaderText="ชื่อ" 
                                        SortExpression="GROUP_NAME" />
                                    <asp:CheckBoxField DataField="P_INSERT" HeaderText="เพิ่ม" 
                                        SortExpression="P_INSERT" Visible="False" />
                                    <asp:CheckBoxField DataField="P_DELETE" HeaderText="ลบ" 
                                        SortExpression="P_DELETE" ShowHeader="False" Visible="False" />
                                    <asp:CheckBoxField DataField="P_UPDATE" HeaderText="แก้ไข" 
                                        SortExpression="P_UPDATE" ShowHeader="False" Visible="False" />
                                    <asp:CheckBoxField DataField="P_VIEW_DOC" HeaderText="ดูเอกสารทั้งหมด" 
                                        SortExpression="P_VIEW_DOC" ShowHeader="False" Visible="False" />
                                    <asp:CheckBoxField DataField="P_VIEW_DOC_IN" HeaderText="ดูเอกสารที่เกี่ยวข้อง" 
                                        SortExpression="P_VIEW_DOC_IN" ShowHeader="False" Visible="False" />
                                    <asp:CommandField ButtonType="Image" 
                                        SelectImageUrl="~/VIEW/images/button_edit.gif" ShowSelectButton="True" />
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" 
                                                CommandName="Delete" ImageUrl="~/VIEW/images/button_remove.gif" Text="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#CCCC99" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <RowStyle BackColor="#F7F7DE" />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                <SortedAscendingHeaderStyle BackColor="#848384" />
                                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                <SortedDescendingHeaderStyle BackColor="#575357" />
                            </asp:GridView>
                                    </td>
            </tr>
        </table>
                                        <asp:Label ID="LWARN" runat="server"></asp:Label>
                            <asp:EntityDataSource ID="EntityDataSource1" runat="server" 
                                ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                EnableDelete="True" EnableFlattening="False" EnableInsert="True" AutoGenerateWhereClause="True"
                                EnableUpdate="True" EntitySetName="TBL_M_GROUP" 
                                EntityTypeFilter="TBL_M_GROUP">
                            </asp:EntityDataSource>

                                    <br />
        <table class="style1">
            <tr>
                <td colspan="2" class="style2">
                    </td>
            </tr>
            <tr>
                <td style="width: 45px">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder4">
    <uc2:Navigator ID="Navigator1" runat="server" />
</asp:Content>


<asp:Content ID="Content4" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    <uc2:MyMenu ID="MyMenu1" runat="server" />
</asp:Content>








<asp:Content ID="Content5" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style2
        {
            height: 18px;
        }
        .style3
        {
            width: 610px;
            text-align: right;
        }
        .style4
        {
            width: 711px;
        }
    </style>
</asp:Content>









