﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmSearchDoc.aspx.cs" Inherits="DCC.VIEW.pages.DocManage.FrmSearchDoc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Document Control Center</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
.font_default
{
	font-family: "MS Sans Serif";
    font-size:11px;
    font-weight:normal;
    color:black;
    text-decoration:none;
            text-align: center;
        }
        </style>
    <link href="../../style/structure.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="box_header">ค้นหาเอกสาร</div>
    <div class="box_data">
    <table class="style1">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: right">
                                                                                            <asp:Label ID="Label32" runat="server" CssClass="font_default" 
                                                                                                
                        Text="ชื่อเอกสาร"></asp:Label>
                                                                                        </td>
            <td style="text-align: left">
                <asp:TextBox ID="TXT_FN" runat="server"></asp:TextBox>
&nbsp;<asp:Button ID="CMD_SEARCH" runat="server" Text="ค้นหา" onclick="CMD_SEARCH_Click" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center" colspan="2">
                                                                                            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                                                                                                BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" 
                                                                                                CellPadding="4" DataKeyNames="DOC_AUTO_ID,DOC_ID" 
                                                                                                DataSourceID="EntityDataSource1" style="text-align: left" 
                                                                                                onselectedindexchanged="GridView2_SelectedIndexChanged" 
                                                                                                onrowdatabound="GridView2_RowDataBound">
                                                                                                <Columns>
                                                                                                    <asp:BoundField DataField="DOC_AUTO_ID" HeaderText="DOC_AUTO_ID" 
                                                                                                        ReadOnly="True" SortExpression="DOC_AUTO_ID" Visible="False" />
                                                                                                    <asp:BoundField DataField="DOC_ID" HeaderText="รหัสเอกสาร" ReadOnly="True" 
                                                                                                        SortExpression="DOC_ID" />
                                                                                                    <asp:TemplateField HeaderText="ชื่อเอกสาร" SortExpression="DOC_FILE_NAME">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("DOC_FILE_NAME") %>' 
                                                                                                                Visible="False"></asp:Label>
                                                                                                            <asp:HyperLink ID="HyperLink1" runat="server">HyperLink</asp:HyperLink>
                                                                                                        </ItemTemplate>
                                                                                                        <EditItemTemplate>
                                                                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("DOC_FILE_NAME") %>'></asp:TextBox>
                                                                                                        </EditItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="DOC_DESCRIPT" HeaderText="DOC_DESCRIPT" 
                                                                                                        SortExpression="DOC_DESCRIPT" Visible="False" />
                                                                                                    <asp:BoundField DataField="DOC_OWNER" HeaderText="DOC_OWNER" 
                                                                                                        SortExpression="DOC_OWNER" Visible="False" />
                                                                                                    <asp:CommandField ShowSelectButton="True" />
                                                                                                </Columns>
                                                                                                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                                                                                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                                                                                                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                                                                                                <RowStyle BackColor="White" ForeColor="#003399" />
                                                                                                <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                                                                                <SortedAscendingCellStyle BackColor="#EDF6F6" />
                                                                                                <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                                                                                                <SortedDescendingCellStyle BackColor="#D6DFDF" />
                                                                                                <SortedDescendingHeaderStyle BackColor="#002876" />
                                                                                            </asp:GridView>
                                                                                            <asp:Label ID="Label33" runat="server" ForeColor="#FF3300"></asp:Label>
                                                                                            <asp:EntityDataSource ID="EntityDataSource1" runat="server" 
                                                                                                ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                                                                                AutoGenerateWhereClause="true"
                                                                                                EnableFlattening="False" EntitySetName="TBL_DOCUMENT" 
                                                                                                EntityTypeFilter="TBL_DOCUMENT">
                                                                                            </asp:EntityDataSource>
                                                                                        </td>
        </tr>
        <tr>
            <td style="text-align: center" colspan="2">
                                                                                        <asp:Button ID="CMD_CANCEL" runat="server" Text="ยกเลิก" 
                                                                                            onclick="CMD_CANCEL_Click" />
                                                                                        </td>
        </tr>
        <tr align="center">
            <td style="text-align: center" >
                                                                                        &nbsp;</td>
        </tr>
    </table>    
    </div>
    </form>
</body>
</html>
