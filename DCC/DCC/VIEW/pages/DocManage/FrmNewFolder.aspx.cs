﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.Utility;
using DCC.MODEL;
using DCC.DAL;
using System.IO;

namespace DCC.VIEW
{
    public partial class FrmNewFolder : System.Web.UI.Page
    {
        private String formName = "FrmNewFolder.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TBL_USERS"] != null)
            {
                EntityDataSource2.AutoGenerateWhereClause = false;
                EntityDataSource2.Select = "it.[DEPARTMENT_ID], it.[DEPARTMENT_NAME]";
                EntityDataSource2.GroupBy = "it.[DEPARTMENT_ID],it.[PARENT_ID],it.[DEPARTMENT_NAME]";

                if (Session["filePos"] != null)
                {
                    List<String> lists = (List<String>)Session["filePos"];
                    if (lists.Count > 1)
                    {
                        //อยู่ใน Sub folder
                        //ดึง ID และ แผนกเก่ามา
                        using (DCCDBEntities context = new DCCDBEntities())
                        {
                            //ตรวจสอบว่ามีการเลือกไฟล์ไว้ก่อนหรือไม่ถ้าเลือกให้ remove ออกจาก List
                            String msg = Path.GetExtension(getPath(false));
                            if (msg.Length>0)
                            {
                                removePath();
                            }
                            List<TBL_M_DEPARTMENT> deps = context.TBL_M_DEPARTMENT.Where("it.[DEPARTMENT_FOLDER]='" + getPath(true) + "'").ToList();
                            if (deps.Count>0)
                            {
                                TBL_M_DEPARTMENT dep = deps[0];

                                TXT_ID.Text = dep.DEPARTMENT_ID;
                                DDL_DEPT.SelectedValue = dep.PARENT_ID;
                                TXT_DEPT_NAME.Text = dep.DEPARTMENT_NAME;
                                TXT_ID.Enabled = false;
                                DDL_DEPT.Enabled = false;
                                TXT_DEPT_NAME.Enabled = false;
                            }
                        }
                    }
                    else
                    {
                        if (getPath(false).Split('/').Length == 2)
                        {
                            //ถ้า Path = 2 แสดงว่าอยู่ที่ Root Folder ไม่ต้องเลือกแผนก
                            TXT_ID.Text = getAutoID();//Gen ID
                        }
                    }
                }
            }
            else {
                Response.Redirect("~/View/Logout.aspx");
            }
        }

        protected void CMD_OK_Click(object sender, EventArgs e)
        {
            if (Session["filePos"] != null)
            {
                String fd = getPath(false) + "/" + TXT_FOLDER_NAME.Text;
                if (Util.createDir(Server.MapPath(fd)))
                {
                    using (DCCDBEntities context = new DCCDBEntities())
                    {
                        TBL_M_DEPARTMENT dept = new TBL_M_DEPARTMENT()
                        {
                            DEPARTMENT_ID = TXT_ID.Text,
                            PARENT_ID = DDL_DEPT.SelectedValue,
                            DEPARTMENT_NAME = TXT_DEPT_NAME.Text,
                            DEPARTMENT_FOLDER = getPath(true) + "/" + TXT_FOLDER_NAME.Text
                        };
                        context.TBL_M_DEPARTMENT.AddObject(dept);
                        if (context.SaveChanges() > 0)
                        {
                            //update logs
                            // ###### Add Document Event ######
                            if (EventLog.Logs(((TBL_USERS)Session["TBL_USERS"]).USER_ID, TXT_ID.Text, fd, "NF"))
                            {
                                //Response.Write("write log success.");
                            }
                            //Response.Write(Util.ShowMessage(MyMessage.INSERT_SUCCESS, formName));
                            //refresh หน้าหลัก

                            RefreshOpener(Page, "ContentPlaceHolder1_ContentPlaceHolder3_Button3", MyMessage.INSERT_SUCCESS);
                            //RefreshOpener(Page, "ContentPlaceHolder1$ContentPlaceHolder3$ImageButton2", MyMessage.INSERT_SUCCESS);
                        }
                        else
                        {
                            Response.Write(Util.ShowMessage(MyMessage.INSERT_FAILURE, formName));
                        }
                    }
                }
                else {
                    Response.Write(Util.ShowMessage(MyMessage.CANNOT_CREATE_DIR, formName));
                }
            }
        }

        private String getPath(bool bSkipRoot)
        {
            String msg = "";
            if (Session["filePos"] != null)
            {
                int index = 0;
                List<String> filePos = (List<String>)Session["filePos"];
                foreach (String s in filePos)
                {
                    //ถ้าเป็น True จะไม่เอา Root Folder
                    if (bSkipRoot)
                    {
                        if (index == 0)
                        {
                           
                        }
                        else
                        {
                            msg += s;
                        }
                    }
                    else
                    {
                        msg += s;
                    }

                    index++;
                }
            }
            return msg;
        }
        private String getAutoID()
        {
            String id = "001";
            using (DCCDBEntities context = new DCCDBEntities())
            {
                List<TBL_M_DEPARTMENT> dep = context.TBL_M_DEPARTMENT.ToList();
                if (dep.Count > 0)
                {
                    id = (Convert.ToInt16(dep[dep.Count - 1].DEPARTMENT_ID) + 1).ToString("000");
                }
                else
                {
                    id = "001";
                }
            }
            return id;
        }

        public void RefreshOpener(Page pPage, string strButtonName, string msg)
        {
            System.Text.StringBuilder strBuilder = new System.Text.StringBuilder();
            strBuilder.Append("<script>");
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append("alert('" + msg + "');");
            strBuilder.Append("window.opener.document.getElementById('" + strButtonName + "').click();window.close();");
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append("</" + "script>");

            if (!pPage.ClientScript.IsClientScriptBlockRegistered(strBuilder.ToString()))
            {
                pPage.ClientScript.RegisterClientScriptBlock(this.GetType(),"script", strBuilder.ToString());
            }
        }
        private void removePath()
        {
            List<String> filePos = null;
            if (Session["filePos"] != null)
            {
                filePos = (List<String>)Session["filePos"];
                filePos.RemoveAt(filePos.Count - 1);
            }
            //Label1.Text = getPath(false);//Add
        }
    }
}