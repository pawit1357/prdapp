﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;
using DCC.Utility;
using System.IO;
using DCC.DAL;
using System.Text;

namespace DCC.VIEW
{
    public partial class FrmUploadDoc : System.Web.UI.Page
    {
        private String formName = "FrmUploadDoc.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
                if (Session["TBL_USERS"] != null)
                {
                    if (Session["filePos"] != null)
                    {
                        List<String> lists = (List<String>)Session["filePos"];
                        if (lists.Count > 0)
                        {
                            //Status
                            String fn = Path.GetExtension(getPath());
                            if (fn.Length > 0)
                            {
                                TBL_DOCUMENT doc = getDocID(getPath());
                                TXT_DOC_ID.Text = doc.DOC_ID;
                                TXT_DESCRIPTION.Text = doc.DOC_DESCRIPT;
                                LUpload.Text = doc.DOC_FILE_NAME;
                                LDocName.Text = Path.GetFileName(doc.DOC_FILE_NAME);
                                RadioButtonList1.SelectedIndex = 1;
                                using (DCCDBEntities context = new DCCDBEntities())
                                {
                                    List<TBL_DOCUMENT> docs = context.TBL_DOCUMENT.Where("it.[DOC_FILE_NAME]='" + doc.DOC_FILE_NAME + "'").ToList();
                                    if (docs.Count > 0)
                                    {
                                        TXT_REVISE.Text = Convert.ToString(docs[0].DOC_REV+1);
                                        TXT_REVISE.Enabled = false;
                                    }
                                }
                            }
                            else
                            {
                                LUpload.Text = getPath();
                                RadioButtonList1.SelectedIndex = 0;
                                TXT_DOC_ID.Focus();
                            }

                        }
                    }

                }
                else {
                    Response.Redirect("~/View/Login.aspx");
                }
            }
            ////
        }

        protected void CMD_SAVE_Click(object sender, EventArgs e)
        {
            if (Session["filePos"] != null)
            {
                //path
                String fn = getPath() + "/" + FileUpload1.FileName;

                //udpate document
                String msg = "";
                String flag = "";
                using (DCCDBEntities context = new DCCDBEntities())
                {
                    switch (RadioButtonList1.SelectedIndex)
                    {
                        case 0://INSERT
                            #region "บันทึกข้อมูล"
                            if (!Util.isFileExist(Server.MapPath(getPath() + "\\" + Path.GetFileNameWithoutExtension(fn) + " - REV."+Convert.ToInt16(TXT_REVISE.Text).ToString("00")+"" + Path.GetExtension(fn))))
                            {
                                if (uploadFile(Server.MapPath(getPath() + "\\" + Path.GetFileNameWithoutExtension(fn) + " - REV."+Convert.ToInt16(TXT_REVISE.Text).ToString("00")+"" + Path.GetExtension(fn))))
                                {
                                    TBL_DOCUMENT doc = new TBL_DOCUMENT();
                                    doc.DOC_REV = Convert.ToInt16(TXT_REVISE.Text);
                                    doc.DOC_ID = TXT_DOC_ID.Text;
                                    fn = getPath() + "/" + Path.GetFileNameWithoutExtension(fn) + " - REV." + Convert.ToInt16(TXT_REVISE.Text).ToString("00") + "" + Path.GetExtension(fn);
                                    doc.DOC_FILE_NAME = fn;
                                    doc.DOC_DESCRIPT = TXT_DESCRIPTION.Text;
                                    doc.DOC_OWNER = ((TBL_USERS)Session["TBL_USERS"]).USER_ID;
                                    context.TBL_DOCUMENT.AddObject(doc);
                                    msg = "บันทึกข้อมูล";
                                }
                                else
                                {
                                    Response.Write(Util.ShowMessage("ไม่สามารถอัพโหลดเอกสารได้", formName));
                                }
                            }
                            else
                            {
                                Response.Write(Util.ShowMessage(MyMessage.FILE_IS_EXIST, formName));
                            }
                            #endregion
                            flag = "I";
                            break;
                        case 1://UPDATE
                            #region "แก้ไขข้อมูล"
                            removePath();

                            if (FileUpload1.FileName.Equals(""))
                            {
                                //เป็นการ update ข้อมูล ไม่ได้ update ไฟล์
                                fn = LUpload.Text;
                            }
                            else
                            {
                                #region "Control Version"
                                //ตรวจสอบถ้ามีไฟล์เก่าอยู่ให้ลบไฟล์เก่าออก  
                                if (Util.isFileExist(Server.MapPath(LUpload.Text)))
                                {
                                    //Util.delete(Server.MapPath(LUpload.Text));
                                    fn = getPath() + "/" + Util.getFileLastVersion(Server.MapPath(getPath()), LUpload.Text);
                                }
                                //fn = getPath() + "/" + FileUpload1.FileName;
                                #endregion
                                //update ไฟล์
                                if (uploadFile(Server.MapPath(fn)))
                                {
                                }
                                else
                                {
                                    Response.Write(Util.ShowMessage(MyMessage.UPDATE_FAILURE, formName));
                                }
                            }
                            if (!String.IsNullOrEmpty(FileUpload1.FileName))
                            {
                                TBL_DOCUMENT doc1 = new TBL_DOCUMENT();
                                doc1.DOC_REV = Convert.ToInt16(TXT_REVISE.Text);
                                doc1.DOC_ID = TXT_DOC_ID.Text;
                                doc1.DOC_FILE_NAME = fn;
                                doc1.DOC_DESCRIPT = TXT_DESCRIPTION.Text;
                                //doc1.DOC_OWNER = ((TBL_USERS)Session["TBL_USERS"]).USER_ID;
                                doc1.DOC_MODIFIED_BY = ((TBL_USERS)Session["TBL_USERS"]).USER_ID;
                                context.TBL_DOCUMENT.AddObject(doc1);
                            }
                            else
                            {
                                //หาเจ้าของเอกสาร
                                TBL_DOCUMENT docOwner = context.TBL_DOCUMENT.Where("it.[DOC_ID]='" + TXT_DOC_ID.Text + "'").First();
                                //เพิ่มข้อมูลใหม่
                                TBL_DOCUMENT docU = context.TBL_DOCUMENT.Where("it.[DOC_ID]='" + TXT_DOC_ID.Text + "' AND it.[DOC_OWNER]='" + docOwner.DOC_OWNER + "'").First();
                                docU.DOC_REV = Convert.ToInt16(TXT_REVISE.Text);
                                docU.DOC_ID = TXT_DOC_ID.Text;
                                docU.DOC_DESCRIPT = TXT_DESCRIPTION.Text;
                                docU.DOC_FILE_NAME = fn;
                                docU.DOC_MODIFIED_BY = ((TBL_USERS)Session["TBL_USERS"]).USER_ID;
                            }
                            msg = "แก้ไขข้อมูล";
                            #endregion
                            flag = "U";
                            break;
                    }
                    //ผลลัพธ์การทำรายการ
                    if (context.SaveChanges() > 0)
                    {
                        //update logs
                        // ###### Add Document Event ######
                        if (EventLog.Logs(((TBL_USERS)Session["TBL_USERS"]).USER_ID, TXT_DOC_ID.Text, fn, flag))
                        {
                            //Response.Write("write log success.");
                        }
                        RefreshOpener(Page, "ContentPlaceHolder1_ContentPlaceHolder3_Button3", msg + "เรียบร้อยแล้ว");
                        //RefreshOpener(Page, "ctl00$ContentPlaceHolder1$ContentPlaceHolder3$Button2", msg + "เรียบร้อยแล้ว");
                    }
                    else
                    {
                        Response.Write(Util.ShowMessageAndClose("เกิดข้อผิดพลาดในการ" + msg, formName));
                    }
                }
            }
        }

        #region "Fucntion"
        private Boolean uploadFile(String path)
        {
            Boolean bSuccess = false;
            if (FileUpload1.HasFile)
            {
                //string fileExt = Path.GetExtension(file.FileName);

                //if (fileExt == ".mp3")
                //{
                try
                {
                    FileUpload1.SaveAs(path);                    
                    //Label1.Text = "File name: " +FileUpload1.PostedFile.FileName + "<br>" + FileUpload1.PostedFile.ContentLength + " kb<br>" +"Content type: " + FileUpload1.PostedFile.ContentType;
                    bSuccess = true;
                }
                catch (Exception ex)
                {
                    //Label1.Text = "ERROR: " + ex.Message.ToString();
                    bSuccess = false;
                    throw ex;
                }
                //}
                //else
                //{
                //    Label1.Text = "Only .mp3 files allowed!";
                //}
            }
            else
            {
                bSuccess = false;
                //Label1.Text = "You have not specified a file.";
            }
            return bSuccess;
        }

        private String getPath()
        {
            String msg = "";
            if (Session["filePos"] != null)
            {
                List<String> filePos = (List<String>)Session["filePos"];
                foreach (String s in filePos)
                {
                    msg += s;
                }
            }
            return msg;
        }

        private void removePath()
        {
            List<String> filePos = null;
            if (Session["filePos"] != null)
            {
                filePos = (List<String>)Session["filePos"];
                filePos.RemoveAt(filePos.Count - 1);
            }
        }

        private TBL_DOCUMENT getDocID(String _fn)
        {
            TBL_DOCUMENT doc = null;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                doc = context.TBL_DOCUMENT.Where("it.[DOC_FILE_NAME]='" + _fn + "'").First();
            }
            return doc;
        }

        public void RefreshOpener(Page pPage, string strButtonName, string msg)
        {
            System.Text.StringBuilder strBuilder = new System.Text.StringBuilder();
            strBuilder.Append("<script>");
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append("alert('" + msg + "');");
            strBuilder.Append("window.opener.document.getElementById('" + strButtonName + "').click();window.close();");
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append("</" + "script>");
            if (!pPage.ClientScript.IsClientScriptBlockRegistered(strBuilder.ToString()))
            {
                pPage.ClientScript.RegisterClientScriptBlock(this.GetType(), "script", strBuilder.ToString());
            }
        }
        #endregion




     
    }
}