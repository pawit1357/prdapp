﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;
using DCC.Utility;
using DCC.DAL;

namespace DCC.VIEW
{
    public partial class FrmDownload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
                if (Session["TBL_USERS"] != null)
                {
                    String path = Request.Params["doc_path"].ToString();
                    if (path != null)
                    {
                        // ###### Add Document Event ######
                        if (EventLog.Logs(((TBL_USERS)Session["TBL_USERS"]).USER_ID, getDocID(path).DOC_ID, path, "L"))
                        {
                            Response.Redirect(path);
                        }                       
                    }
                }
                else
                {
                    Response.Redirect("~/View/Login.aspx");
                }
            }
        }

        private TBL_DOCUMENT getDocID(String _fn)
        {
            TBL_DOCUMENT doc = null;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                doc = context.TBL_DOCUMENT.Where("it.[DOC_FILE_NAME]='" + _fn + "'").First();
            }
            return doc;
        }
    }
}