﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmUploadDoc.aspx.cs" Inherits="DCC.VIEW.FrmUploadDoc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Document Control Center</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
.font_textbox
{
	font-family: "MS Sans Serif";
    font-size:16px;
    font-weight:normal;
    color:blue;
    text-decoration:none;
	margin-left: 3px;
}
.font_default
{
	font-family: "MS Sans Serif";
    font-size:11px;
    font-weight:normal;
    color:white;
    text-decoration:none;
    }
        .style2
        {
            height: 23px;
        }
        .style3
        {
            text-align: left;
        }
    </style>
    <link href="../../style/structure.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form2" runat="server">
    <div>
    
    <div class="box_header">อัพโหลดเอกสาร</div>
    <div class="box_data">
        <table class="style1">
            <tr>
                <td style="text-align: right">
                                                <asp:Label ID="Label29" runat="server" 
                                                    Text="รหัสเอกสาร"></asp:Label>
                                            </td>
                <td style="text-align: left">
                                                <asp:TextBox ID="TXT_DOC_ID" runat="server" CssClass="font_textbox" 
                                                    CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                        ControlToValidate="TXT_DOC_ID" ErrorMessage="ยังไม่ได้ป้อนรหัสเอกสาร" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                                                &nbsp;</td>
                <td style="text-align: left">
                                                  <asp:Label ID="LDocName" runat="server"></asp:Label>
                                                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                                                <asp:Label ID="Label28" runat="server" 
                                                    Text="ไฟล์เอกสาร"></asp:Label>
                                            </td>
                <td class="style3">
                                                  &nbsp;<asp:FileUpload ID="FileUpload1" runat="server"/>
                                                  <asp:Label ID="LUpload" runat="server" Visible="False"></asp:Label>
                                                  </td>
            </tr>
            <tr>
                <td style="text-align: right">
                                                <asp:Label ID="Label18" runat="server" 
                                                    Text="คำอธิบาย"></asp:Label>
                                            </td>
                <td style="text-align: left">
                    <asp:TextBox ID="TXT_DESCRIPTION" runat="server" CssClass="font_textbox" 
                                                    Height="96px" TextMode="MultiLine" 
                        Width="334px" CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                        ControlToValidate="TXT_DESCRIPTION" ErrorMessage="ยังไม่ได้ป้อนคำอธิบาย" 
                                                        SetFocusOnError="True" 
                        style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                                                <asp:Label ID="Label30" runat="server" 
                                                    Text="Revise"></asp:Label>
                                            </td>
                <td style="text-align: left">
                                                <asp:TextBox ID="TXT_REVISE" runat="server" CssClass="font_textbox" 
                                                    CausesValidation="True">0</asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                        ControlToValidate="TXT_REVISE" ErrorMessage="ยังไม่ได้ป้อน Revise" 
                                                        SetFocusOnError="True" 
                        style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
            </tr>
            <tr>
                <td class="style2">
                </td>
                <td class="style2">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" Enabled="False" 
                        RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="0">เพิ่ม</asp:ListItem>
                        <asp:ListItem Value="1">แก้ไข</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td style="text-align: left">
                    <asp:Button ID="CMD_SAVE" runat="server" onclick="CMD_SAVE_Click" 
                        Text="บันทึก" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div>
    </div>
    </form>
</body>
</html>
