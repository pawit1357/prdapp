﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;
using System.IO;
using DCC.DAL;
using DCC.Utility;

namespace DCC.VIEW.pages.Qmr
{
    public partial class FrmSearch : System.Web.UI.Page
    {
        private String formName = "FrmSearch.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {
                    if (Session["filePos"] != null)
                    {
                        List<String> lists = (List<String>)Session["filePos"];
                        using (DCCDBEntities context = new DCCDBEntities())
                        {
                            //หารหัสเอกสาร
                            TBL_DOCUMENT doc = context.TBL_DOCUMENT.Where("it.[DOC_FILE_NAME]='"+getPath(false)+"'").First();
                            //หารายชื่อผู้เกี่ยวข้องกับเอกสารนี้
                            if (doc != null)
                            {
                                List<TBL_DOCUMENT_REF> docRefs = context.TBL_DOCUMENT_REF.Where("it.[REF_DOC_ID]='" + doc.DOC_ID + "'").ToList();
                                foreach (TBL_DOCUMENT_REF df in docRefs)
                                {

                                    ListBox1.Items.Add(df.REF_USER_ID + " " + DALUser.getUsers(df.REF_USER_ID).PROFILE_NAME + "  " + DALUser.getUsers(df.REF_USER_ID).PROFILE_SURNAME);
                                }
                            }                            
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/View/Logout.aspx");
                }
            }
        }



        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //using (DCCDBEntities context = new DCCDBEntities())
                //{
                //    TBL_M_DEPARTMENT dept = context.TBL_M_DEPARTMENT.Where("it.[DEPARTMENT_ID]='" + e.Row.Cells[5].Text + "'").First();
                //    if (dept != null)
                //    {
                //        e.Row.Cells[5].Text = dept.DEPARTMENT_NAME;
                //    }
                //}
            }
        }

        protected void DDL_DEP_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (DCCDBEntities context = new DCCDBEntities())
            {
                EntityDataSource4.AutoGenerateWhereClause = false;
                EntityDataSource4.Where = "it.[PROFILE_DEPARTMENT_ID]='"+DDL_DEP.SelectedValue+"'";
            }
        }

        //Remove
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            for (int i = 0; i < ListBox1.Items.Count; i++)
            {
                if (ListBox1.Items[i].Selected)
                {
                    ListBox1.Items.Remove(ListBox1.Items[i]);
                }
            }
        }
        //Add
        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {

            foreach (GridViewRow gvr in GridView1.Rows)
            {

                CheckBox cb = (CheckBox)gvr.FindControl("CheckBox1");
                if (cb.Checked == true)
                {

                    String value = gvr.Cells[0].Text + " " + gvr.Cells[2].Text + "  " + gvr.Cells[3].Text;
                    if (!isDuplicate(value))
                    {
                        ListBox1.Items.Add(value);
                        cb.Checked = false;
                    }
                }
            }
        }
        //Cancel
        protected void Button3_Click(object sender, EventArgs e)
        {           
            Response.Redirect("FrmSearch.aspx");
        }
        //OK
        protected void Button4_Click(object sender, EventArgs e)
        {
            //บันทึอข้อมูลผู้เกี่ยวข้อง
            using (DCCDBEntities context = new DCCDBEntities())
            {
                //หารหัสเอกสาร
                TBL_DOCUMENT doc = context.TBL_DOCUMENT.Where("it.[DOC_FILE_NAME]='" + getPath(false) + "'").First();

                //ลบผู้เกี่ยวข้องก่อนหน้าออกหมด
                List<TBL_DOCUMENT_REF> deleteDocRefs = context.TBL_DOCUMENT_REF.Where("it.[REF_DOC_ID]='" + doc.DOC_ID + "'").ToList();
                foreach (TBL_DOCUMENT_REF df in deleteDocRefs)
                {
                    context.DeleteObject(df);
                }

                //เพิ่มผู้เกี่ยวข้องกับเอกสาร
                List<String> lists = new List<string>();
                for (int i = 0; i < ListBox1.Items.Count; i++)
                {
                    TBL_DOCUMENT_REF docRef = new TBL_DOCUMENT_REF();
                    docRef.REF_DOC_ID = doc.DOC_ID;
                    docRef.REF_USER_ID = ListBox1.Items[i].Value.Split(' ')[0];
                    docRef.DOC_PATH = getPath(false);
                    context.TBL_DOCUMENT_REF.AddObject(docRef);
                }
                if (context.SaveChanges() > 0)
                {
                    //update logs
                    // ###### Add Document Event ######
                    if (EventLog.Logs(((TBL_USERS)Session["TBL_USERS"]).USER_ID, doc.DOC_ID, getPath(false), "IR"))
                    {
                        //Response.Write("write log success.");
                    }
                    RefreshOpener(Page, "ContentPlaceHolder1_ContentPlaceHolder3_Button3", "บันทึกข้อมูลผู้เกี่ยวข้องเรียบร้อยแล้ว");
                    //RefreshOpener(Page, "ctl00$ContentPlaceHolder1$ContentPlaceHolder3$Button2", "บันทึกข้อมูลผู้เกี่ยวข้องเรียบร้อยแล้ว");
                }
                else
                {
                    Response.Write(Util.ShowMessageAndClose("เกิดข้อผิดพลาดในการบันทึกข้อมูลผู้เกี่ยวข้อง", formName));
                }

            }
            //Close page

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GetData", "<Script>window.close()</Script>");
        }

        //ตรวจสอบไม่ให้ เพิ่มข้อมูลซ้ำเข้า ListBox
        public bool isDuplicate(String item)
        {
            bool bDup = false;
            for (int i = 0; i < ListBox1.Items.Count; i++)
            {
                if (item.Equals(ListBox1.Items[i].Text))
                {
                    bDup = true;
                    break;
                }
            }
            return bDup;
        }

        private String getPath(bool bSkipRoot)
        {
            String msg = "";
            if (Session["filePos"] != null)
            {
                int index = 0;
                List<String> filePos = (List<String>)Session["filePos"];
                foreach (String s in filePos)
                {
                    //ถ้าเป็น True จะไม่เอา Root Folder
                    if (bSkipRoot)
                    {
                        if (index == 0)
                        {

                        }
                        else
                        {
                            msg += s;
                        }
                    }
                    else
                    {
                        msg += s;
                    }

                    index++;
                }
            }
            return msg;
        }
    
        //ตรวจสอบว่ามีผุ้เกี่ยวข้องกับเอกสารไฟล์นี้อยู่ก่อนหรือไม่
        private bool isDocRef(String docID,String userID)
        {
            bool bTrue = false;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                List<TBL_DOCUMENT_REF> lists = context.TBL_DOCUMENT_REF.Where("it.[REF_DOC_ID]='" + docID + "' AND it.[REF_USER_ID]='" + userID + "'").ToList();
                if (lists.Count > 0)
                {
                    bTrue = true;
                }
            }
            return bTrue;
        }

        public void RefreshOpener(Page pPage, string strButtonName, string msg)
        {
            System.Text.StringBuilder strBuilder = new System.Text.StringBuilder();
            strBuilder.Append("<script>");
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append("alert('" + msg + "');");
            strBuilder.Append("window.opener.document.getElementById('" + strButtonName + "').click();window.close();");
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append("</" + "script>");
            if (!pPage.ClientScript.IsClientScriptBlockRegistered(strBuilder.ToString()))
            {
                pPage.ClientScript.RegisterClientScriptBlock(this.GetType(), "script", strBuilder.ToString());
            }
        }
    }
}