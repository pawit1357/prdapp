﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VIEW/theme/theme1.Master" AutoEventWireup="true" CodeBehind="ManageUser.aspx.cs" Inherits="DCC.VIEW.pages.Admin.ManageUser" %>
<%@ Register src="../../Navigator.ascx" tagname="Navigator" tagprefix="uc1" %>
<%@ Register src="../../MyMenu.ascx" tagname="MyMenu" tagprefix="uc2" %>
<%@ Register src="../../Navigator.ascx" tagname="Navigator" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../../style/structure.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type = "text/javascript">
        function checkForm() {
            if (form.TXT_ID.value == "") { 
                alert("Username not set"); 
                return false; 
            } 
//        if (form.form_password.value=="") { 
//        alert("Password not set"); 
//        return false; 
//        } 
//        if (form.form_password2.value=="") { 
//        alert("Password not set"); 
//        return false; 
//        } 
//        if (form.form_email.value=="") { 
//        alert("E-Mail not set"); 
//        return false; 
//        } 
//        if (form.form_password.value!==form.form_password2.value) { 
//        alert("Passwords do not match!"); 
//        return false; 
//        } 
        }  
    </script>
    <table class="style1">
        <tr>
            <td>
                                        <div class="box_header">
                                            <asp:Label ID="Label5" runat="server" Text="ข้อมูลส่วนตัว"></asp:Label>
                                        </div>
                                        <div class="box_data">
                                        <asp:Panel ID="Panel2" runat="server">
                                            <table class="style1">
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        <asp:Label ID="Label31" runat="server" Text="รหัสบุคลากร"></asp:Label>
                                                    </td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="TXT_ID" runat="server" CssClass="font_textbox" 
                                                            CausesValidation="True"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                            ControlToValidate="TXT_ID" ErrorMessage="ยังไม่ได้ป้อนรหัสบุคลากร" 
                                                            SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">
                                                        <asp:Label ID="Label29" runat="server" Text="คำนำหน้า"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DDL_TITLE" runat="server" CssClass="font_dropdown" 
                                                        DataSourceID="EntityDataSource3" DataTextField="TITLE_NAME" 
                                                        DataValueField="TITLE_ID">
                                                        </asp:DropDownList>
                                                        <asp:EntityDataSource ID="EntityDataSource3" runat="server" 
                                                        ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                                        EnableFlattening="False" EntitySetName="TBL_TITLE" EntityTypeFilter="TBL_TITLE" 
                                                        Select="it.[TITLE_ID], it.[TITLE_NAME]">
                                                        </asp:EntityDataSource>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        <asp:Label ID="Label1" runat="server" Text="ชื่อ"></asp:Label>
                                                    </td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="TXT_NAME" runat="server" CssClass="font_textbox" 
                                                            CausesValidation="True"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                            ControlToValidate="TXT_NAME" ErrorMessage="ยังไม่ได้ป้อนชื่อ" 
                                                            SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        <asp:Label ID="Label2" runat="server" Text="นามสกุล"></asp:Label>
                                                    </td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="TXT_SURNAME" runat="server" CssClass="font_textbox" 
                                                            CausesValidation="True"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                            ControlToValidate="TXT_SURNAME" ErrorMessage="ยังไม่ได้ป้อนนามสกุล" 
                                                            SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        <asp:Label ID="Label3" runat="server" Text="อีเมล์"></asp:Label>
                                                    </td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="TXT_EMAIL" runat="server" CssClass="font_textbox" 
                                                            CausesValidation="True"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                            ControlToValidate="TXT_EMAIL" ErrorMessage="ยังไม่ได้ป้อนอีเมล์" 
                                                            style="color: #FF9933"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        <asp:Label ID="Label4" runat="server" Text="แผนก"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DDL_DEPT" runat="server" CssClass="font_dropdown" 
                                            DataSourceID="EntityDataSource2" DataTextField="DEPARTMENT_NAME" 
                                            DataValueField="DEPARTMENT_ID">
                                                        </asp:DropDownList>
                                                        <asp:EntityDataSource ID="EntityDataSource2" runat="server" 
                                                        ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                                        AutoGenerateWhereClause = "true"
                                                        EnableFlattening="False" EntitySetName="TBL_M_DEPARTMENT" 
                                                        Select="it.[DEPARTMENT_ID], it.[DEPARTMENT_NAME]">
                                                        </asp:EntityDataSource>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align: right;">
                                                        <asp:Button ID="CMD_SEARCH" runat="server" ForeColor="Black" 
                                                            onclick="CMD_Click" Text="ค้นหา" Visible="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        </div>
                                        <div class="box_header">
                                            <asp:Label ID="Label39" runat="server" Text="ข้อมูลรหัสผู้ใช้"></asp:Label>
                                        </div>
                                        <div class="box_data">
                                        <asp:Panel ID="Panel3" runat="server">
                                            <table class="style1">
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        <asp:Label ID="Label19" runat="server" 
                                                                        Text="รหัสผู้ใช้"></asp:Label>
                                                    </td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="TXT_USERNAME" runat="server" CssClass="font_textbox" 
                                                            CausesValidation="True"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                            ControlToValidate="TXT_USERNAME" ErrorMessage="ยังไม่ได้ป้อนรหัสผู้ใช้" 
                                                            SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        <asp:Label ID="Label20" runat="server" Text="รหัสผ่าน"></asp:Label>
                                                    </td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="TXT_PASSWORD" runat="server" CssClass="font_textbox" 
                                                                        TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                                            ControlToValidate="TXT_PASSWORD" ErrorMessage="ยังไม่ได้ป้อนรหัสผ่าน" 
                                                            SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                                            ControlToCompare="TXT_PASSWORD" ControlToValidate="TXT_CONFIRM_PASS" 
                                                            ErrorMessage="ยืนยันรหัสผ่านไม่ตรงกัน" SetFocusOnError="True"></asp:CompareValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        <asp:Label ID="Label28" runat="server" 
                                                                        Text="ยืนยันรหัสผ่าน"></asp:Label>
                                                    </td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="TXT_CONFIRM_PASS" runat="server" CssClass="font_textbox" 
                                                                        TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                                            ControlToValidate="TXT_CONFIRM_PASS" ErrorMessage="ยังไม่ได้ป้อนยืนยันรหัสผ่าน" 
                                                            SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        <asp:Label ID="Label21" runat="server" Text="กลุ่มผู้ใช้"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DDL_GROUP" runat="server" CssClass="font_dropdown" 
                                                                        DataSourceID="EntityDataSource1" DataTextField="GROUP_NAME" 
                                                                        DataValueField="GROUP_ID">
                                                        </asp:DropDownList>
                                                        <asp:EntityDataSource ID="EntityDataSource1" runat="server" 
                                                                        ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                                                        EnableFlattening="False" EntitySetName="TBL_M_GROUP" 
                                                                        EntityTypeFilter="TBL_M_GROUP" 
                                                            Select="it.[GROUP_ID], it.[GROUP_NAME]">
                                                        </asp:EntityDataSource>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        <asp:Label ID="Label30" runat="server" Text="สถานะ"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DDL_STATUS" runat="server" CssClass="font_dropdown">
                                                            <asp:ListItem Value="A">Active</asp:ListItem>
                                                            <asp:ListItem Value="I">InActive</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 110px; text-align: right;">
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        </div>
                                    <br />
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                                    <asp:Button ID="CMD_INSERT" runat="server" ForeColor="Black" 
                                        onclick="CMD_Click" Text="  เพิ่ม  " />
                                    <asp:Button ID="CMD_CANCEL" runat="server" CausesValidation="False" 
                                        ForeColor="Black" onclick="CMD_Click" Text="ยกเลิก" />
                            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" 
                                        BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                                        DataKeyNames="PROFILE_USER_ID" DataSourceID="EntityDataSource4" 
                                        ForeColor="Black" GridLines="Vertical" 
                                        onselectedindexchanged="GridView1_SelectedIndexChanged" 
                                        onrowdatabound="GridView1_RowDataBound" 
                                        onrowdeleted="GridView1_RowDeleted" AllowSorting="True">
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:BoundField DataField="PROFILE_USER_ID" HeaderText="รหัส" 
                                                ReadOnly="True" SortExpression="PROFILE_USER_ID" />
                                            <asp:BoundField DataField="PROFILE_TITLE_ID" HeaderText="PROFILE_TITLE_ID" 
                                                SortExpression="PROFILE_TITLE_ID" Visible="False" />
                                            <asp:BoundField DataField="PROFILE_NAME" HeaderText="ชื่อ" 
                                                SortExpression="PROFILE_NAME" />
                                            <asp:BoundField DataField="PROFILE_SURNAME" HeaderText="นามสกุล" 
                                                SortExpression="PROFILE_SURNAME" />
                                            <asp:BoundField DataField="PROFILE_EMAIL" HeaderText="อีเมล์" 
                                                SortExpression="PROFILE_EMAIL" />
                                            <asp:BoundField DataField="PROFILE_DEPARTMENT_ID" 
                                                HeaderText="แผนก" SortExpression="PROFILE_DEPARTMENT_ID" />
                                            <asp:BoundField DataField="PROFILE_USER_ID" HeaderText="กลุ่มผู้ใช้" />
                                            <asp:TemplateField ShowHeader="False" HeaderText="แก้ไข/ลบ">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" 
                                                        CommandName="Select" ImageUrl="~/VIEW/images/button_edit.gif" Text="Select" />
                                                    <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" 
                                                        CommandName="Delete" ImageUrl="~/VIEW/images/button_remove.gif" Text="Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                        <RowStyle BackColor="#F7F7DE" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                    </td>
        </tr>
        <tr>
            <td style="text-align: center">
                                    <asp:Label ID="LWARN" runat="server"></asp:Label>
                                </td>
        </tr>
        <tr>
            <td style="text-align: center">
                                    <asp:EntityDataSource ID="EntityDataSource4" runat="server" 
                                        ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                        EnableDelete="True" EnableFlattening="False" EnableInsert="True" 
                                        EnableUpdate="True" EntitySetName="TBL_USERS_PROFILE" AutoGenerateWhereClause="true"
                                        EntityTypeFilter="TBL_USERS_PROFILE">
                                    </asp:EntityDataSource>
                                </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder4">
    <uc2:Navigator ID="Navigator1" runat="server" />
</asp:Content>


<asp:Content ID="Content4" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    <uc2:MyMenu ID="MyMenu1" runat="server" />
</asp:Content>



<asp:Content ID="Content5" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style2
        {
            text-align: left;
        }
        .style3
        {
            width: 110px;
            height: 59px;
        }
        .style4
        {
            height: 59px;
        }
    </style>
</asp:Content>




