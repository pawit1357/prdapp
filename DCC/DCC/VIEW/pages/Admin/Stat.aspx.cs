﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;
using DCC.Utility;
using DCC.DAL;
using System.Data;

namespace DCC.VIEW.pages.Admin
{
    public partial class Stat : System.Web.UI.Page
    {
        private DataTable dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {
                    //set user detail
                    Navigator1.setDetail(DALUser.getUserDetail(user));
                    MyMenu1.setMenu(user);
                    //Set Show current event log
                    String curDate = Util.toDate(Util.getDate());
                    //check page permission
                    if (!Navigator1.viewPagePermission(user, "001"))
                    {
                        Response.Redirect("~/VIEW/Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/View/Login.aspx");
                }
            }
            #region "ตารางรายละเอียดการชำระเงิน"
            //initial gridview
            if (this.Session["xtable"] == null)
            {
                dt = new DataTable();
                dt.Columns.Add(new DataColumn("รหัสผู้ใช้", typeof(string)));
                dt.Columns.Add(new DataColumn("เพิ่ม", typeof(string)));
                dt.Columns.Add(new DataColumn("ลบ", typeof(string)));
                dt.Columns.Add(new DataColumn("แก้ไข", typeof(string)));
                dt.Columns.Add(new DataColumn("ดาวโหลด", typeof(string)));
                this.Session["xtable"] = dt;
            }
            else
            {
                dt = (DataTable)this.Session["xtable"];
            }
            #endregion
        }

        protected void RCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (RCondition.SelectedIndex)
            {
                case 0:
                    TXT_BEGIN.Enabled = false;
                    TXT_END.Enabled = false;
                    break;
                case 1:
                    TXT_BEGIN.Enabled = true;
                    TXT_END.Enabled = true;
                    break;
            }
        }

        protected void CMD_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            List<CStat> stats = null;
            switch (button.ID)
            {
                case "CMD_SEARCH":
                    switch (RCondition.SelectedIndex)
                    {
                        case 0:
                            //Show report all
                            stats = DALSTAT.getStatAll();
                            break;
                        case 1:
                            HiddenField1.Value = Util.toDate(TXT_BEGIN.Text);
                            HiddenField2.Value = Util.toDate(TXT_END.Text);
                            stats = DALSTAT.getStatByDate(HiddenField1.Value, HiddenField2.Value);
                            break;
                    }
                    foreach (CStat c in stats)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = c.Userid;
                        dr[1] = c.I;
                        dr[2] = c.D;
                        dr[3] = c.U;
                        dr[4] = c.L;
                        dt.Rows.Add(dr);
                    }
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    break;
                case "CMD_CANCEL":
                    Response.Redirect("Stat.aspx");
                    break;
            }
        }
    }
}