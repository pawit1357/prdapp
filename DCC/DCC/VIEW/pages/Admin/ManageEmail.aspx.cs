﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;
using DCC.VIEW.pages.Master;
using DCC.DAL;
using DCC.Utility;
using System.Text;

namespace DCC.VIEW.pages.Admin
{
    public partial class ManageEmail : System.Web.UI.Page
    {
        private ACTION dbAction = ACTION.UPDATE;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {
                    //set user detail
                    TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
                    Navigator1.setDetail(DALUser.getUserDetail(user));
                    MyMenu1.setMenu(user);
                    //Set initial action
                    Session["ACTION"] = ACTION.INSERT;
                    //check page permission
                    if (!Navigator1.viewPagePermission(user, "001"))
                    {
                        Response.Redirect("~/VIEW/Default.aspx");
                    }

                    using (DCCDBEntities context = new DCCDBEntities())
                    {
                        foreach (var tbl in context.TBL_EMAIL)
                        {
                            HEMAIL_ID.Value = tbl.EMAIL_ID + "";
                            TXT_EMAIL_ACCOUNT.Text = tbl.EMAIL_ACCOUNT;
                            TXT_PASSWORD.Text = tbl.EMAIL_PASSWORD;
                            TXT_CONFIRM_PASSWORD.Text = "";
                            TXT_HOST.Text = tbl.EMAIL_HOST;
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/View/Logout.aspx");
                }
            }

        }

        protected void CMD_INSERT_Click(object sender, EventArgs e)
        {
            String select = "";
            Button button = (Button)sender;
            select = button.Text;

            switch (select)
            {
                case "บันทึก":
                    //if (validate())
                    //{
                        using (DCCDBEntities context = new DCCDBEntities())
                        {

                            TBL_EMAIL email = new TBL_EMAIL()
                            {
                                EMAIL_ID = Convert.ToInt16(HEMAIL_ID.Value),
                                EMAIL_ACCOUNT = TXT_EMAIL_ACCOUNT.Text,
                                EMAIL_PASSWORD = TXT_PASSWORD.Text,
                                EMAIL_HOST = TXT_HOST.Text
                            };

                            switch (dbAction)
                            {
                                case ACTION.UPDATE:
                                    context.TBL_EMAIL.Attach(email);
                                    var entry = context.ObjectStateManager.GetObjectStateEntry(email);
                                    entry.SetModifiedProperty("EMAIL_ACCOUNT");
                                    entry.SetModifiedProperty("EMAIL_PASSWORD");
                                    entry.SetModifiedProperty("EMAIL_HOST");
                                    if (context.SaveChanges() > 0)
                                    {
                                        Response.Write(Util.ShowMessage(MyMessage.INSERT_SUCCESS, "../../Default.aspx"));
                                    }
                                    else
                                    {
                                        Response.Redirect("~/VIEW/Default.aspx");
                                    }
                                    break;
                            }
                        }
                    //}
                    break;
                case "ยกเลิก":
                    Response.Redirect("~/VIEW/Default.aspx");
                    break;
            }
        }
        private bool validate()
        {
            bool bSucces = true;
            StringBuilder msg = new StringBuilder();
            if (TXT_EMAIL_ACCOUNT.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อน Email");
                msg.Append("\\n");
            }
            if (TXT_PASSWORD.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลรหัสผ่าน");
                msg.Append("\\n");
            }
            if (TXT_CONFIRM_PASSWORD.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลยืนยันรหัสผ่าน");
                msg.Append("\\n");
            }
            if (!TXT_PASSWORD.Text.Equals(TXT_CONFIRM_PASSWORD.Text))
            {
                msg.Append("รหัสผ่านไม่ตรงกัน");
                msg.Append("\\n");
            }
            if (TXT_HOST.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลโฮสต์");
                msg.Append("\\n");
            }

            if (msg.ToString().Length > 0)
            {
                bSucces = false;
                Response.Write(Util.ShowMessage(msg.ToString(),"ManageEmail.aspx"));
            }
            return bSucces;
        }
    }
}