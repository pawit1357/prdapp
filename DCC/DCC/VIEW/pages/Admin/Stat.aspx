﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VIEW/theme/theme1.Master" AutoEventWireup="true" CodeBehind="Stat.aspx.cs" Inherits="DCC.VIEW.pages.Admin.Stat" %>
<%@ Register src="../../MyMenu.ascx" tagname="MyMenu" tagprefix="uc1" %>
<%@ Register src="../../Navigator.ascx" tagname="Navigator" tagprefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../scripts/popcalendar.js" type="text/javascript"></script>
    <link href="../../style/structure.css" rel="stylesheet" type="text/css" />
    <table class="style1">
    <tr>
        <td>
        <div class="box_header">
                                            <asp:Label ID="Label5" runat="server" 
                Text="สถิติการใช้งาน"></asp:Label>
                                        </div>
        <div class="box_data">
            <table style="width: 100%">
                <tr>
                    <td style="width: 213px; text-align: right">
                                                &nbsp;</td>
                    <td>
                                                &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 213px; text-align: right">
                                                <asp:Label ID="Label34" runat="server" 
                                                    Text="เงื่อนไขในการค้นหา"></asp:Label>
                                            </td>
                    <td>
                                                <asp:RadioButtonList ID="RCondition" runat="server" 
                                                    CssClass="font_default4" RepeatDirection="Horizontal" AutoPostBack="True" 
                                                    onselectedindexchanged="RCondition_SelectedIndexChanged">
                                                    <asp:ListItem Selected="True" Value="0">ทั้งหมด</asp:ListItem>
                                                    <asp:ListItem Value="1">ตามช่วงวัน</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                </tr>
                <tr>
                    <td style="width: 213px; text-align: right">
                                                <asp:Label ID="Label32" runat="server" 
                                                    Text="วันที่เริ่มต้น"></asp:Label>
                                            </td>
                    <td>
                                                <asp:TextBox styleId="sel2" ID="TXT_BEGIN" runat="server"></asp:TextBox>
                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                                                                                                    <script language='javascript' type="text/javascript" >
	<!--
                                                                                                                                                        if (!document.layers) {
                                                                                                                                                            document.write("<img src='../../images/cal.gif' style='CURSOR:hand;'onclick='popUpCalendar(this, form1.ctl00$ContentPlaceHolder1$TXT_BEGIN,\"dd/mm/yyyy\")'>")
                                                                                                                                                        }
	//-->
	</script></td>
                </tr>
                <tr>
                    <td style="width: 213px; text-align: right">
                                                <asp:Label ID="Label33" runat="server" 
                                                    Text="วันที่สิ้นสุด"></asp:Label>
                                            </td>
                    <td>
                                                <asp:TextBox ID="TXT_END" runat="server"></asp:TextBox>
                                                <asp:HiddenField ID="HiddenField2" runat="server" />
                                                     <script language='javascript' type="text/javascript" >
	<!--
                                                         if (!document.layers) {
                                                             document.write("<img src='../../images/cal.gif' style='CURSOR:hand;'onclick='popUpCalendar(this, form1.ctl00$ContentPlaceHolder1$TXT_END,\"dd/mm/yyyy\")'>")
                                                         }
	//-->
	</script>    </td>
                </tr>
                <tr>
                    <td style="width: 213px">
                        &nbsp;</td>
                    <td style="text-align: right">
                                                <asp:Button ID="CMD_SEARCH" runat="server" ForeColor="#006600" 
                                                    onclick="CMD_Click" Text="ค้นหา"/>
                                                <asp:Button ID="CMD_CANCEL" runat="server" ForeColor="#006600" 
                                                    onclick="CMD_Click" Text="ยกเลิก" />
                                            </td>
                </tr>
            </table>
        </div>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridView1" runat="server" BackColor="White" 
                BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                HorizontalAlign="Center">
                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                <RowStyle BackColor="White" ForeColor="#003399" />
                <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                <SortedAscendingCellStyle BackColor="#EDF6F6" />
                <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                <SortedDescendingCellStyle BackColor="#D6DFDF" />
                <SortedDescendingHeaderStyle BackColor="#002876" />
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
</table>
</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder4">
    <uc2:Navigator ID="Navigator1" runat="server" />
</asp:Content>

<asp:Content ID="Content4" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    <uc1:MyMenu ID="MyMenu1" runat="server" />
</asp:Content>


