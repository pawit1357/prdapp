﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DCC.MODEL;
using System.IO;

namespace DCC.Utility
{
    public class EventLog
    {
        public static bool Logs(String userId,String docID,String docName,string status)
        {
            Boolean bSuccess = false;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                TBL_DOCUMENT_EVENT docEvent = new TBL_DOCUMENT_EVENT();
                docEvent.EVENT_USER_ID = userId;
                docEvent.EVENT_DOC_ID = docID;
                docEvent.EVENT_DOC_NAME = docName;
                docEvent.EVENT_STATUS = status;
                docEvent.EVENT_DATE = Util.toDate2(DateTime.Now);
                context.TBL_DOCUMENT_EVENT.AddObject(docEvent);
                if (context.SaveChanges() > 0)
                {
                    bSuccess = true;

                    TBL_EMAIL email = context.TBL_EMAIL.First();
                    List<String> docUsers = getDocUser(docName);
                    foreach (String s in docUsers)
                    {
                        List<TBL_USERS_PROFILE> uPro = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID]='" + s + "'").ToList();
                            if (uPro.Count > 0)
                            {
                                Email.sendMAIL(uPro[0].PROFILE_EMAIL, email.EMAIL_ACCOUNT, // TO , FROM
                                    "เรียนคุณ " + uPro[0].PROFILE_USER_ID + " " + uPro[0].PROFILE_NAME + "  " + uPro[0].PROFILE_SURNAME, //subject
                                    "มีการ" + Util.getStatus(status) + " " + Path.GetFileName(docName), //body
                                    email.EMAIL_ACCOUNT, email.EMAIL_PASSWORD, email.EMAIL_HOST, null);
                            }                        
                    }
                }
            }
            return bSuccess;
        }

        private static List<String> getDocUser(String docName)
        {
            List<String> tmp = new List<string>();
            using (DCCDBEntities context = new DCCDBEntities())
            {
                //ส่งตามผู้เกี่ยวข้องเอกสาร
                List<TBL_DOCUMENT_REF> docRefs = context.TBL_DOCUMENT_REF.Where("it.[DOC_PATH]='" + docName + "'").ToList();
                if (docRefs.Count > 0)
                {
                    //TBL_EMAIL email = context.TBL_EMAIL.First();
                    foreach (TBL_DOCUMENT_REF docRef in docRefs)
                    {
                        //ส่งหาเอกสารให้คนอื่นในแผนก
                        //String docOwner = DAL.DALUser.getUserDepFolder(docRef.REF_USER_ID);
                        //if (docName.Contains(docOwner))
                        //{
                        //tmp.Add(docOwner);
                        //}
                        tmp.Add(docRef.REF_USER_ID);
                    }
                }
                //-----------------------------------------------------
                //การส่งอีเมล์ ตามเอกสารในแผนก
                List<TBL_USERS> users = context.TBL_USERS.ToList();
                if (users.Count > 0)
                {
                    //TBL_EMAIL email = context.TBL_EMAIL.First();
                    foreach (TBL_USERS user in users)
                    {
                        //ส่งหาเอกสารให้คนอื่นในแผนก
                        String docOwner = DAL.DALUser.getUserDepFolder(user);
                        if (docName.Contains(docOwner))
                        {
                            tmp.Add(user.USER_ID);
                        }
                    }
                }
            }

            List<String> docCanView = Util.distinct(tmp);
            return docCanView;
        }
        //ดึกเอกสารที่เกี่ยวข้อง
        //private static List<String> getDocUser(String docName)
        //{
        //    List<String> tmp = new List<string>();
        //    using (DCCDBEntities context = new DCCDBEntities())
        //    {
        //        //ส่งตามผู้เกี่ยวข้องเอกสาร
        //        List<TBL_DOCUMENT_REF> docRefs = context.TBL_DOCUMENT_REF.ToList();
        //        if (docRefs.Count > 0)
        //        {
        //            TBL_EMAIL email = context.TBL_EMAIL.First();
        //            foreach (TBL_DOCUMENT_REF docRef in docRefs)
        //            {
        //                //ส่งหาเอกสารให้คนอื่นในแผนก
        //                String docOwner = DAL.DALUser.getUserDepFolder(docRef.REF_USER_ID);
        //                if (docName.Contains(docOwner))
        //                {
        //                    tmp.Add(docOwner);
        //                }
        //            }
        //        }
        //        //-----------------------------------------------------
        //        //การส่งอีเมล์ ตามเอกสารในแผนก
        //        List<TBL_USERS> users = context.TBL_USERS.ToList();
        //        if (users.Count > 0)
        //        {
        //            TBL_EMAIL email = context.TBL_EMAIL.First();
        //            foreach (TBL_USERS user in users)
        //            {
        //                //ส่งหาเอกสารให้คนอื่นในแผนก
        //                String docOwner = DAL.DALUser.getUserDepFolder(user);
        //                if (docName.Contains(docOwner))
        //                {
        //                    tmp.Add(user.USER_ID);
        //                }
        //            }
        //        }
        //    }

        //    List<String> docCanView = Util.distinct(tmp);
        //    return docCanView;
        //}
    }
}