﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Text;
using System.Configuration;
using System.Web.SessionState;
using DCC.MODEL;

namespace DCC.Utility
{

    public class Util
    {
        //dd/MM/yyyy --> yyyyMMdd
        public static String toDate(String date)
        {
            String tmp = "";
            if (date.Length > 0)
            {
                String day = date.Substring(0, 2);
                String month = date.Substring(3, 2);
                String year = date.Substring(6, 4);
                tmp = year + month + day;
            }
            else { 
            
            }
            return tmp;
        }
        //yyyyMMdd --> dd/MM/yyyy
        public static String toDate1(String date)
        {
            String day = date.Substring(6, 2);
            String month = date.Substring(4, 2);
            String year = date.Substring(0, 4);
            return day + "/" + month + "/" + year;
        }
        public static String toDate2(DateTime dt)
        {
            return dt.Year + dt.Month.ToString("00") + dt.Day.ToString("00") + dt.Hour.ToString("00") + dt.Minute.ToString("00");
        }
        public static String toDate3(DateTime dt)
        {
            return dt.Year + dt.Month.ToString("00") + dt.Day.ToString("00");
        }
        public static String toDate4(String date)
        {
            if (!date.Equals("&nbsp;"))
            {
                String day = date.Substring(6, 2);
                String month = date.Substring(4, 2);
                String year = date.Substring(0, 4);
                String Hour = date.Substring(8, 2);
                String minute = date.Substring(10, 2);
                return day + "/" + month + "/" + year + " " + Hour + ":" + minute;
            }
            else {
                return date;
            }
            
        }
        
        public static bool isFileExist(String curFile)
        {
            bool isExist = File.Exists(curFile);
            return isExist;
        }
        public static bool createDir(String drive, String folder)
        {
            bool bSuccess = false;
            try
            {
                Directory.CreateDirectory(drive + "\\" + folder);
                bSuccess = true;
            }
            catch (IOException io)
            {
                bSuccess = false;
                throw io;
            }
            return bSuccess;
        }
        public static bool createDir(String folder)
        {
            bool bSuccess = false;
            try
            {
                Directory.CreateDirectory(folder);
                bSuccess = true;
            }
            catch (IOException io)
            {
                bSuccess = false;
                throw io;
            }
            return bSuccess;
        }
        public static Boolean renameDir(String _drive, String _folder, String _srcDrive, String _srcFolder)
        {
            bool bSuccess = false;
            try
            {
                string destFolder = _drive + _folder;
                string srcFolder = _srcDrive + _srcFolder;
                Directory.Move(srcFolder, destFolder);
                bSuccess = true;
            }
            catch (Exception ex)
            {
                bSuccess = true;
                throw ex;
            }
            return bSuccess;
        }
        public static void deleteDir(String drive, String folder)
        {
            try
            {
                Directory.Delete(drive + folder);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool deleteDir(String folder)
        {
            bool bSuccess = false;
            try
            {
                Directory.Delete(folder);
                bSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return bSuccess;
        }
        public static bool delete(String path)
        {
            bool bSuccess = false;
            try
            {
                File.Delete(path);
                bSuccess = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bSuccess;
        }

        public static String getFileLastVersion(String path,String file)
        {
            String filename = Path.GetFileName(file);
            String cvName = "";
            List<String> tmp = new List<string>();
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] fInfos = di.GetFiles();
            foreach (FileInfo fi in fInfos)
            {
                if (fi.FullName.Contains(filename))
                {
                    tmp.Add(Path.GetFileNameWithoutExtension(fi.Name));
                }
            }
            int max = 0;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                List<TBL_DOCUMENT> docs = context.TBL_DOCUMENT.Where("it.[DOC_FILE_NAME]='"+file+"'").ToList();
                if (docs.Count > 0)
                {
                    max = (int)docs[0].DOC_REV;
                    max++;

                    String[] nameSplit = Path.GetFileNameWithoutExtension(file).Split('.');
                    String fnameTrue = "";
                    #region "ป้องกันชื่อไฟล์มี จุด มากกว่า 1 ตัว"
                    int countSplit = nameSplit.Length - 1;
                    for (int i = 0; i < countSplit; i++)
                    {
                        fnameTrue += nameSplit[i] + ".";
                    }
                    #endregion
                    cvName = fnameTrue + max.ToString("00") + Path.GetExtension(file);

                }
                else {
                    cvName = Path.GetFileNameWithoutExtension(file) + " - REV." + max.ToString("00") + Path.GetExtension(file);
                }
            }
           
            return cvName;
        }
        //public static String ShowMessage(string message)
        //{

        //    return "<script type=\"text/javascript\">alert('" + message + "');</script>";
        //}

        public static String ShowMessage(string message, string url)
        {
            return "<script type=\"text/javascript\">alert('" + message + "');window.location='" + url + "'; </script>";
        }
        public static String ShowMessageAndClose(string message)
        {
            return "<script type=\"text/javascript\">alert('" + message + "');</script>";
        }
        public static String ShowMessageAndClose(string message, string url)
        {
            return "<script type=\"text/javascript\">alert('" + message + "');window.location='" + url + "'; </script>";
        }
        public static String getDate()
        {
            return DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
        }

        public static bool isEmail(string inputEmail)
        {
            return Regex.IsMatch(inputEmail,
               @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
               @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"); ;
        }

        public static String getStatus(String _status)
        {
            String status = "";
            switch (_status.Trim())
            { 
                case "I":
                    status = "อัพโหลด";
                    break;
                case "D":
                    status = "ลบ";
                    break;
                case "U":
                    status = "แกไข";
                    break;
                case "L":
                    status = "ดาวห์โหลด";
                    break;
                case "NF":
                    status = "สร้างแฟ้ม";
                    break;
                case "DF":
                    status = "ลบแฟ้ม";
                    break;
                case "IR":
                    status = "เพิ่มผู้เกี่ยวข้อง";
                    break;
            }
            return status;
        }
        public static List<String> distinct(List<String> data)
        {
            List<String> myStringList = new List<string>();
            foreach (string s in data)
            {
                if (!myStringList.Contains(s))
                {
                    myStringList.Add(s);
                }
            }
            return myStringList;
        }

        public static String getDocumentPath()
        {
            //คืนค่า doc root ที่ set ไว้ใน web.config
            return ConfigurationManager.AppSettings["docPath"].ToString();
        }
        public static string TheSessionId()
        {
            HttpSessionState ss = HttpContext.Current.Session;
            HttpContext.Current.Session["APPSESSION"] = "APPSESSION";
            //HttpContext.Current.Response.Write(ss.SessionID);
            return ss.SessionID;
        }
    
    }
}
