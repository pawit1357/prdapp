﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCC.MODEL
{
    public class CStat
    {
        private string docid;

        public string Docid
        {
            get { return docid; }
            set { docid = value; }
        }
        private string userid;

        public string Userid
        {
            get { return userid; }
            set { userid = value; }
        }
        private int i;

        public int I
        {
            get { return i; }
            set { i = value; }
        }
        private int d;

        public int D
        {
            get { return d; }
            set { d = value; }
        }
        private int u;

        public int U
        {
            get { return u; }
            set { u = value; }
        }
        private int l;

        public int L
        {
            get { return l; }
            set { l = value; }
        }
    }
}